function main(containerName) {
    //----------------------------------------------------------------------------------------------
    var canvasWidth = document.getElementById(containerName).offsetWidth;
    var canvasHeight = document.getElementById(containerName).offsetHeight;
    var frameRate = 24;

    console.log("containerName : ",containerName);
    console.log("offsetheight : ",document.getElementById(containerName).offsetHeight);
	
    var ua = navigator.userAgent.toLowerCase();
    var checks = Boolean(ua.match(/android/)) ||
            Boolean(ua.match(/ipod/)) ||
            Boolean(ua.match(/iphone/)) ||
            Boolean(ua.match(/ipad/)) ||
            Boolean(ua.match(/tablet/)) ||
            Boolean(ua.match(/tablet pc/))

    var touchable = checks && (typeof (document.ontouchstart) != 'undefined');
    var container = document.getElementById(containerName);
    var canvas = document.createElement("canvas");
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    canvas.style.position = "absolute";
    canvas.style.left = "0px";
    canvas.style.top = "0px";
    canvas.style.zIndex = 0;

    var ctx = canvas.getContext("2d");
    container.appendChild(canvas);
    //----------------------------------------------------------------------------------------------
    var totalChildren = [];
    var eventListenerMouseDown = [];
    var eventListenerMouseMove = [];
    var eventListenerMouseUp = [];
    var eventListenerMouseRollOver = [];
    var eventListenerMouseRollOut = [];
    var eventListenerText = [];

    var mouseX = 0;
    var mouseY = 0;
    var mouseDragX = 0;
    var mouseDragY = 0;
    var objectDrag = null
    var Mouse = {IsDown: false, hide: false};

    if (touchable) {
        canvas.addEventListener('touchstart', MouseDown, false);
        canvas.addEventListener('touchmove', MouseMove, false);
        canvas.addEventListener('touchend', MouseUp, false);
    } else {
        canvas.addEventListener('mousedown', MouseDown, false);
        canvas.addEventListener('mousemove', MouseMove, false);
        canvas.addEventListener('mouseup', MouseUp, false);
    }

    var focustext = null;
    var stage = new MovieClip();
    stage.width = canvasWidth;
    stage.height = canvasHeight;
    stage.level = 1;
    setInterval(UpdateAllEvent, 1000 / frameRate);

    function MouseDown(e) {
        Mouse.IsDown = true;
        if (!e)
            var e = window.event;
        var eP = e;
        if (touchable && e.touches.length > 1)
            return;
        if (touchable)
            e = e.changedTouches[0];
        mouseX = (e.pageX);
        mouseY = (e.pageY);

        for (var i = 0; i < eventListenerMouseDown.length; i++) {
            var obj = eventListenerMouseDown[i];
            if (obj.hitTestPoint(mouseX, mouseY, false) && obj.IsDown && obj._visible) {
                obj.eventDown({target: obj, currentTarget: obj, event: 'mouseDown', type: 'mouseDown'});
                eP.preventDefault();
                eP.stopPropagation();
                break;
            }
        }
    }
    function MouseMove(e) {
        if (!e)
            var e = window.event;
        var eP = e;
        if (touchable && e.touches.length > 1)
            return;
        if (touchable)
            e = e.changedTouches[0];
        mouseX = (e.pageX);
        mouseY = (e.pageY);

        if (objectDrag != null) {
            var dxMouse = mouseX - objectDrag.parent._x;
            var dyMouse = mouseY - objectDrag.parent._y;
            var aglMouse = (Math.atan2(dyMouse, dxMouse) / Math.PI * 180) - objectDrag.parent._rotation;
            var disMouse = Math.sqrt((dxMouse * dxMouse) + (dyMouse * dyMouse));
            objectDrag.parent.mouseX = (Math.cos(aglMouse * Math.PI / 180) * disMouse) / objectDrag.parent._scaleX;
            objectDrag.parent.mouseY = (Math.sin(aglMouse * Math.PI / 180) * disMouse) / objectDrag.parent._scaleY;
            objectDrag.x = objectDrag.parent.mouseX - mouseDragX;
            objectDrag.y = objectDrag.parent.mouseY - mouseDragY;
        }
        for (var i = 0; i < eventListenerMouseMove.length; i++) {
            var obj = eventListenerMouseMove[i];
            if (obj.IsMove) {
                obj.eventMove({target: obj, currentTarget: obj, event: 'mouseMove', type: 'mouseMove'});
                break;
            }
        }
    }
    function MouseUp(e) {
        Mouse.IsDown = false;
        if (!e)
            var e = window.event;
        var eP = e;
        if (touchable && e.touches.length > 1)
            return;
        if (touchable)
            e = e.changedTouches[0];
        mouseX = (e.pageX);
        mouseY = (e.pageY);

        for (var i = 0; i < eventListenerMouseUp.length; i++) {
            var obj = eventListenerMouseUp[i];
            if (obj.hitTestPoint(mouseX, mouseY, false) && obj.IsUp && obj._visible) {
                obj.eventUp({target: obj, currentTarget: obj, event: 'mouseUp', type: 'mouseUp'});
                break;
            }
        }
    }
    function MovieClip() {
        var _movieClip = {
            symbol: 'MovieClip',
            image: null,
            name: '',
            mouseX: 0,
            mouseY: 0,
            shape: false,
            loop: false,
            mask: null,
            _mask: null,
            maskTarget: null,
            level: 0,
            visible: true,
            x: 0,
            y: 0,
            rotation: 0,
            alpha: 1,
            scaleX: 1,
            scaleY: 1,
            lastWidth: 0,
            lastHeight: 0,
            width: 0,
            height: 0,
            centerX: 0,
            centerY: 0,
            _level: 0,
            _visible: true,
            _x: 0,
            _y: 0,
            _rotation: 0,
            _alpha: 1,
            _scaleX: 1,
            _scaleY: 1,
            _width: 0,
            _height: 0,
            _centerX: 0,
            _centerY: 0,
            parent: null,
            frames: [],
            frameX: 0,
            frameY: 0,
            frameWidth: 0,
            frameHeight: 0,
            eventAction: [],
            currentFrame: 1,
            totalFrames: 1,
            animation: false,
            IsHit: false,
            IsDown: false,
            IsUp: false,
            IsMove: false,
            IsEnterFrame: false,
            eventDown: function (e) {},
            eventUp: function (e) {},
            eventMove: function (e) {},
            eventEnterFrame: function (e) {},
            numChildren: [],
            numGraphic: [],
            graphicsFillColor: '',
            graphicsStrokeColor: '',
            graphicsStrokeSize: 1,
            graphics: {
                movieClip: null,
                clear: function () {
                    this.movieClip.numGraphic = [];
                    this.movieClip.graphicsFillColor = '';
                    this.movieClip.graphicsStrokeColor = '';
                    this.movieClip.graphicsStrokeSize = 1;
                },
                beginFill: function (color) {
                    this.movieClip.graphicsFillColor = color;
                },
                lineStyle: function (size, color) {
                    this.movieClip.graphicsStrokeColor = color;
                    this.movieClip.graphicsStrokeSize = size;
                },
                moveTo: function (x, y) {
                    this.movieClip.numGraphic.push({lineWidth: this.movieClip.graphicsStrokeSize, fillStyle: this.movieClip.graphicsFillColor, strokeStyle: this.movieClip.graphicsStrokeColor, graphic: [{x: x, y: y}]});
                },
                curveTo: function (cx, cy, x, y) {
                    this.movieClip.numGraphic[this.movieClip.numGraphic.length - 1].graphic.push({type: 'curve', cx: cx, cy: cy, x: x, y: y});
                },
                lineTo: function (x, y) {
                    this.movieClip.numGraphic[this.movieClip.numGraphic.length - 1].graphic.push({type: 'line', x: x, y: y});
                },
                drawRect: function (x, y, w, h) {
                    this.movieClip.numGraphic = [{lineWidth: this.movieClip.graphicsStrokeSize, fillStyle: this.movieClip.graphicsFillColor, strokeStyle: this.movieClip.graphicsStrokeColor, graphic: [{x: x, y: y}]}];
                    this.movieClip.numGraphic[this.movieClip.numGraphic.length - 1].graphic.push({type: 'line', x: w - x, y: y});
                    this.movieClip.numGraphic[this.movieClip.numGraphic.length - 1].graphic.push({type: 'line', x: w - x, y: h - y});
                    this.movieClip.numGraphic[this.movieClip.numGraphic.length - 1].graphic.push({type: 'line', x: w - x, y: y});
                },
                endFill: function () {

                }
            },
            updateAnimation: function () {
                for (var i = 0; i < this.numChildren.length; i++) {
                    this.numChildren[i].drawAnimation();
                }
            },
            drawAnimation: function () {
                if (this.IsEnterFrame) {
                    this.eventEnterFrame({target: this});
                }
                if (this.animation) {
                    for (var k = 0; k < this.eventAction.length; k++) {
                        if (this.currentFrame == this.eventAction[k].frame) {
                            this.eventAction[k].fnc({target: this});
                        }
                    }
                    this.currentFrame += 1;
                    if (this.currentFrame == this.frames.length + 1) {
                        if (this.loop) {
                            this.currentFrame = 1;
                        } else {
                            this.currentFrame = this.frames.length;
                        }
                    }
                }
                if (this.numChildren.length != 0) {
                    this.updateAnimation();
                }
            },
            updateDraw: function () {
                for (var i = 0; i < this.numChildren.length; i++) {
                    this.numChildren[i].draw();
                }
            },
            draw: function () {
                if (this.lastWidth != this.width || this.lastHeight != this.height) {
                    this.scaleX = (Math.sin(this.rotation * Math.PI / 180) * this.height - (Math.cos(this.rotation * Math.PI / 180) * this.width)) / ((Math.sin(this.rotation * Math.PI / 180) * Math.sin(this.rotation * Math.PI / 180) * this.frameWidth) - (Math.cos(this.rotation * Math.PI / 180) * Math.cos(this.rotation * Math.PI / 180) * this.frameWidth));
                    this.scaleY = ((Math.sin(this.rotation * Math.PI / 180) * this.frameWidth * this.width) - (Math.cos(this.rotation * Math.PI / 180) * this.frameWidth * this.height)) / ((Math.sin(this.rotation * Math.PI / 180) * Math.sin(this.rotation * Math.PI / 180) * this.frameWidth * this.frameHeight) - (this.frameWidth * Math.cos(this.rotation * Math.PI / 180) * Math.cos(this.rotation * Math.PI / 180) * this.frameHeight));
                }
                this._alpha = this.parent._alpha * this.alpha;
                this._visible = this.visible;
                if (this.parent._visible == false) {
                    this._visible = false;
                }
                if (this._visible == false) {
                    this._alpha = 0;
                }
                this._level = (this.parent._level * 10) + this.level;
                this._rotation = this.parent._rotation + this.rotation;
                this._scaleX = this.parent._scaleX * this.scaleX;
                this._scaleY = this.parent._scaleY * this.scaleY;
                this.width = Math.abs(Math.cos(this.rotation * Math.PI / 180) * this.frameWidth * this.scaleX) + Math.abs(Math.sin(this.rotation * Math.PI / 180) * this.frameHeight * this.scaleY);
                this.height = Math.abs(Math.sin(this.rotation * Math.PI / 180) * this.frameWidth * this.scaleX) + Math.abs(Math.cos(this.rotation * Math.PI / 180) * this.frameHeight * this.scaleY);
                this.lastWidth = this.width;
                this.lastHeight = this.height;
                this._width = Math.abs(Math.cos(this._rotation * Math.PI / 180) * this.frameWidth * this._scaleX) + Math.abs(Math.sin(this._rotation * Math.PI / 180) * this.frameHeight * this._scaleY);
                this._height = Math.abs(Math.sin(this._rotation * Math.PI / 180) * this.frameWidth * this._scaleX) + Math.abs(Math.cos(this._rotation * Math.PI / 180) * this.frameHeight * this._scaleY);
                var agl = this.parent._rotation + (Math.atan2(this.y, this.x) / Math.PI * 180);
                var dis = Math.sqrt((this.x * this.x) + (this.y * this.y));
                this._x = this.parent._x + (Math.cos(agl * Math.PI / 180) * dis * this.parent._scaleX);
                this._y = this.parent._y + (Math.sin(agl * Math.PI / 180) * dis * this.parent._scaleY);
                var dxMouse = mouseX - this._x;
                var dyMouse = mouseY - this._y;
                var aglMouse = (Math.atan2(dyMouse, dxMouse) / Math.PI * 180) - this._rotation;
                var disMouse = Math.sqrt((dxMouse * dxMouse) + (dyMouse * dyMouse));
                this.mouseX = (Math.cos(aglMouse * Math.PI / 180) * disMouse) / this._scaleX;
                this.mouseY = (Math.sin(aglMouse * Math.PI / 180) * disMouse) / this._scaleY;
                ctx.save();
                ctx.translate(this._x, this._y);
                ctx.rotate(this._rotation * Math.PI / 180);
                ctx.globalAlpha = this._alpha;
                ctx.scale(this._scaleX, this._scaleY);
                if (this.image != null) {
                    if (this.frames.length != 0) {
                        this.getSize();
                        if (this.frameWidth != 0 && this.frameHeight != 0) {
                            ctx.drawImage(this.image, this.frameX, this.frameY, this.frameWidth, this.frameHeight, this.frameCenterX, this.frameCenterY, this.frameWidth, this.frameHeight);
                        }
                    } else {
                        ctx.drawImage(this.image, this.centerX, this.centerY);
                    }
                }
                if (this.numGraphic.length != 0) {
                    for (var i = 0; i < this.numGraphic.length; i++) {
                        ctx.beginPath();

                        for (var j = 0; j < this.numGraphic[i].graphic.length; j++) {
                            if (j == 0) {
                                ctx.moveTo(this.numGraphic[i].graphic[j].x, this.numGraphic[i].graphic[j].y);
                            } else {
                                if (this.numGraphic[i].graphic[j].type == 'line') {
                                    ctx.lineTo(this.numGraphic[i].graphic[j].x, this.numGraphic[i].graphic[j].y);
                                }
                                if (this.numGraphic[i].graphic[j].type == 'curve') {
                                    ctx.quadraticCurveTo(this.numGraphic[i].graphic[j].cx, this.numGraphic[i].graphic[j].cy, this.numGraphic[i].graphic[j].x, this.numGraphic[i].graphic[j].y);
                                }
                            }
                        }
                        if (this.numGraphic[i].fillStyle != '') {
                            ctx.closePath();
                            ctx.fillStyle = this.numGraphic[i].fillStyle;
                            ctx.fill();
                        }
                        ctx.lineWidth = this.numGraphic[i].lineWidth;
                        if (this.numGraphic[i].strokeStyle != '') {
                            ctx.strokeStyle = this.numGraphic[i].strokeStyle;
                        }
                        if (this.numGraphic[i].fillStyle == '' && this.numGraphic[i].strokeStyle == '') {
                            ctx.strokeStyle = '#000000';
                        }
                        ctx.stroke();
                    }
                }
                ctx.restore();
                if (this.numChildren.length != 0) {
                    this.updateDraw();
                }
            },
            setSize: function (w, h) {
                this.lastWidth = w;
                this.lastHeight = h;
                this.frameWidth = w;
                this.frameHeight = h;
                this.width = w;
                this.height = h;
            },
            getSize: function () {
                if (this.frames.length != 0) {
                    this.frameX = this.frames[this.currentFrame - 1][0];
                    this.frameY = this.frames[this.currentFrame - 1][1];
                    this.frameWidth = this.frames[this.currentFrame - 1][2];
                    this.frameHeight = this.frames[this.currentFrame - 1][3];
                    this.frameCenterX = this.frames[this.currentFrame - 1][4];
                    this.frameCenterY = this.frames[this.currentFrame - 1][5];
                    this.width = Math.abs(Math.cos(this.rotation * Math.PI / 180) * this.frameWidth * this.scaleX) + Math.abs(Math.sin(this.rotation * Math.PI / 180) * this.frameHeight * this.scaleY);
                    this.height = Math.abs(Math.sin(this.rotation * Math.PI / 180) * this.frameWidth * this.scaleX) + Math.abs(Math.cos(this.rotation * Math.PI / 180) * this.frameHeight * this.scaleY);
                    this.lastWidth = this.width;
                    this.lastHeight = this.height;
                }
            },
            setCenter: function (x, y) {
                this.centerX = x;
                this.centerY = y;
            },
            setImage: function (source) {
                this.image = source;
                this.lastWidth = source.width;
                this.lastHeight = source.height;
                this.frameWidth = source.width;
                this.frameHeight = source.height;
                this.width = source.width;
                this.height = source.height;
            },
            addChild: function (obj) {
                obj.parent = this;
                this.numChildren.push(obj);
                obj.level = this.numChildren.length;
            },
            removeChild: function (obj) {
                var ary = [];
                var level = 0;
                for (var i = 0; i < this.numChildren.length; i++) {
                    if (this.numChildren[i] != obj) {
                        level += 1;
                        this.numChildren[i].level = level;
                        ary.push(this.numChildren[i]);
                    }
                }
                this.numChildren = ary;
            },
            removeAllChild: function (obj) {
                var ary = [];
                var level = 0;
                for (var i = 0; i < this.numChildren.length; i++) {
                    this.numChildren[i] = null;
                }
                this.numChildren = ary;
            },
            addEventListener: function (evt, fnc) {
                if (evt == 'mouseDown') {
                    this.IsDown = true;
                    this.eventDown = fnc;
                    eventListenerMouseDown.push(this);
                    eventListenerMouseDown = shortDepthLevel(eventListenerMouseDown);
                }
                if (evt == 'mouseUp') {
                    this.IsUp = true;
                    this.eventUp = fnc;
                    eventListenerMouseUp.push(this);
                    eventListenerMouseUp = shortDepthLevel(eventListenerMouseUp);
                }
                if (evt == 'mouseMove') {
                    this.IsMove = true;
                    this.eventMove = fnc;
                    eventListenerMouseMove.push(this);
                    eventListenerMouseMove = shortDepthLevel(eventListenerMouseMove);
                }
                if (evt == 'enterFrame') {
                    this.IsEnterFrame = true;
                    this.eventEnterFrame = fnc;
                }
            },
            removeEventListener: function (evt, fnc) {
                if (evt == 'mouseDown') {
                    this.IsDown = false;
                    this.eventDown = null;
                    eventListenerMouseDown = removeArray(this, eventListenerMouseDown);
                    eventListenerMouseDown = shortDepthLevel(eventListenerMouseDown);
                }
                if (evt == 'mouseUp') {
                    this.IsUp = false;
                    this.eventUp = null;
                    eventListenerMouseUp = removeArray(this, eventListenerMouseUp);
                    eventListenerMouseUp = shortDepthLevel(eventListenerMouseUp);
                }
                if (evt == 'mouseMove') {
                    this.IsMove = false;
                    this.eventMove = null;
                    eventListenerMouseMove = removeArray(this, eventListenerMouseMove);
                    eventListenerMouseMove = shortDepthLevel(eventListenerMouseMove);
                }
                if (evt == 'enterFrame') {
                    this.IsEnterFrame = false;
                    this.eventEnterFrame = null;
                }
            },
            startDrag: function () {
                var dxMouse = mouseX - this.x;
                var dyMouse = mouseY - this.y;

                var aglMouse = (Math.atan2(dyMouse, dxMouse) / Math.PI * 180) - this.parent._rotation;
                var disMouse = Math.sqrt((dxMouse * dxMouse) + (dyMouse * dyMouse));

                mouseDragX = (Math.cos(aglMouse * Math.PI / 180) * disMouse) / this.parent._scaleX;
                mouseDragY = (Math.sin(aglMouse * Math.PI / 180) * disMouse) / this.parent._scaleY;

                objectDrag = this;
            },
            stopDrag: function () {
                objectDrag = null;
            },
            prevFrame: function () {
                this.animation = false;
                if (this.currentFrame > 1) {
                    this.currentFrame -= 1;
                    this.getSize();
                }
            },
            nextFrame: function () {
                this.animation = false;
                if (this.currentFrame < this.frames.length) {
                    this.currentFrame += 1;
                    this.getSize();
                }
            },
            play: function () {
                this.animation = true;
                this.getSize();
            },
            stop: function () {
                this.animation = false;
                this.getSize();
            },
            gotoAndPlay: function (no) {
                this.currentFrame = no;
                this.animation = true;
                this.getSize();
            },
            gotoAndStop: function (no) {
                this.currentFrame = no;
                this.animation = false;
                this.getSize();
            },
            hitTestPoint: function (x, y, shape) {
                var bool = false;
                if (this._x < x && this._y < y && this._x + this._width > x && this._y + this._height > y) {
                    bool = true;
                }
                return bool;
            },
            hitTestObject: function (obj) {
                var bool = false;
                if (bool == false) {
                    bool = this.hitTestPoint(obj._x + obj.centerX, obj._y + obj.centerY, false);
                }
                if (bool == false) {
                    bool = this.hitTestPoint(obj._x + obj.centerX + obj._width, obj._y + obj.centerY, false);
                }
                if (bool == false) {
                    bool = this.hitTestPoint(obj._x + obj.centerX + obj._width, obj._y + obj.centerY + obj._height, false);
                }
                if (bool == false) {
                    bool = this.hitTestPoint(obj._x + obj.centerX, obj._y + obj.centerY + obj._height, false);
                }
                if (bool == false) {
                    bool = obj.hitTestPoint(this._x + this.centerX, this._y + this.centerY, false);
                }
                if (bool == false) {
                    bool = obj.hitTestPoint(this._x + this.centerX + this._width, this._y + this.centerY, false);
                }
                if (bool == false) {
                    bool = obj.hitTestPoint(this._x + this.centerX + this._width, this._y + this.centerY + this._height, false);
                }
                if (bool == false) {
                    bool = obj.hitTestPoint(this._x + this.centerX, this._y + this.centerY + this._height, false);
                }
                return bool;
            }
        }

        _movieClip.graphics.movieClip = _movieClip;
        totalChildren.push(_movieClip);
        return _movieClip;
    }

    function TextField() {
        var _text = {
            symbol: 'Text',
            color: '#0239d3',
            type: 'dynamic',
            font: 'Open Sans',
            size: 10,
            align: 'center',
            fontWeight: 'regular',
            autoResize: false,
            multiline: false,
            maxChars: 10,
            text: '',
            level: 0,
            border: false,
            visible: true,
            x: 0,
            y: 0,
            rotation: 0,
            alpha: 1,
            scaleX: 1,
            scaleY: 1,
            lastWidth: 0,
            lastHeight: 0,
            width: 10,
            height: 10,
            lastText: 'null',
            lastFornt: '',
            lastSize: 0,
            _text: '',
            _level: 0,
            _visible: true,
            _x: 0,
            _y: 0,
            _rotation: 0,
            _alpha: 1,
            _scaleX: 1,
            _scaleY: 1,
            _width: 10,
            _height: 10,
            frameWidth: 0,
            frameHeight: 0,
            parent: null,
            maxValue: 1000000000000,
            IsHit: false,
            IsDown: false,
            IsUp: false,
            eventDown: function (e) {},
            eventUp: function (e) {},
            numChildren: [],
            drawAnimation: function () {

            },
            draw: function () {
                this._alpha = this.parent._alpha * this.alpha;
                this._visible = this.visible;
                if (this.parent._visible == false) {
                    this._visible = false;
                }
                if (this._visible == false) {
                    this._alpha = 0;
                }
                this._level = (this.parent._level * 10) + this.level;
                this._rotation = this.parent._rotation + this.rotation;
                this._scaleX = this.parent._scaleX * this.scaleX;
                this._scaleY = this.parent._scaleY * this.scaleY;
                var agl = this.parent._rotation + (Math.atan2(this.y, this.x) / Math.PI * 180);
                var dis = Math.sqrt((this.x * this.x) + (this.y * this.y));
                this._x = this.parent._x + (Math.cos(agl * Math.PI / 180) * dis) * this.parent._scaleX;
                this._y = this.parent._y + (Math.sin(agl * Math.PI / 180) * dis) * this.parent._scaleY;
                var dxMouse = mouseX - this._x;
                var dyMouse = mouseY - this._y;
                var aglMouse = (Math.atan2(dyMouse, dxMouse) / Math.PI * 180) - this._rotation;
                var disMouse = Math.sqrt((dxMouse * dxMouse) + (dyMouse * dyMouse));
                this.mouseX = (Math.cos(aglMouse * Math.PI / 180) * disMouse) / this._scaleX;
                this.mouseY = (Math.sin(aglMouse * Math.PI / 180) * disMouse) / this._scaleY;
                if (Number(this.text) > this.maxValue) {
                    this.text = String(this.maxValue);
                }
                this._text = this.text;
                var pad = 0;
                if (this.align == 'right') {
                    pad = this.width - this.frameWidth;
                }
                if (this.align == 'center') {
                    pad = this.width / 2;
                }
                ctx.save();
                ctx.translate(this._x + pad, this._y);
                ctx.rotate(this._rotation * Math.PI / 180);
                ctx.globalAlpha = this._alpha;
                ctx.scale(this._scaleX, this._scaleY);

                ctx.fillStyle = this.color;
                ctx.font = this.size + 'px ' + this.font;
                ctx.textAlign = this.align;
                ctx.textBaseline = 'top';
                ctx.fillText(this._text, 0, 0);
                ctx.restore();
            },
            addEventListener: function (evt, fnc) {
                if (evt == 'mouseDown') {
                    this.IsDown = true;
                    this.eventDown = fnc;
                    eventListenerMouseDown.push(this);
                    eventListenerMouseDown = shortDepthLevel(eventListenerMouseDown);
                }
                if (evt == 'mouseUp') {
                    this.IsUp = true;
                    this.eventUp = fnc;
                    eventListenerMouseUp.push(this);
                    eventListenerMouseUp = shortDepthLevel(eventListenerMouseUp);
                }
            },
            removeEventListener: function (evt, fnc) {
                if (evt == 'mouseDown') {
                    this.IsDown = false;
                    this.eventDown = null;
                    eventListenerMouseDown = removeArray(this, eventListenerMouseDown);
                    eventListenerMouseDown = shortDepthLevel(eventListenerMouseDown);
                }
                if (evt == 'mouseUp') {
                    this.IsUp = false;
                    this.eventUp = null;
                    eventListenerMouseUp = removeArray(this, eventListenerMouseUp);
                    eventListenerMouseUp = shortDepthLevel(eventListenerMouseUp);
                }
            },
            hitTestPoint: function (x, y, shape) {
                var bool = false;
                if (this._x - 7 < x && this._y - 15 < y && this._x - 7 + 53 > x && this._y - 15 + 44 > y) {
                    bool = true;
                }
                return bool;
            }
        }
        eventListenerText.push(_text);
        return _text;
    }
    
    function removeArray(obj, ary) {
        var temp = [];
        for (var i = 0; i < ary.length; i++) {
            if (obj != ary[i]) {
                temp.push(ary[i]);
            }
        }
        return temp;
    }
    function shortDepthLevel(ary) {
        var temp = {};
        for (var i = 0; i < ary.length; i++) {
            for (var j = 0; j < ary.length; j++) {
                if (ary[i]._level > ary[j]._level) {
                    temp = ary[i];
                    ary[i] = ary[j];
                    ary[j] = temp;
                }
            }
        }
        return ary;
    }
    function UpdateAllEvent() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.save();
        ctx.beginPath();
        ctx.rect(0, 0, canvasWidth, canvasHeight);
        ctx.fillStyle = '#FFFFFF';
        ctx.fill();
        ctx.restore();
        stage.mouseX = mouseX;
        stage.mouseY = mouseY;
        if (stage.IsEnterFrame) {
            stage.eventEnterFrame({target: stage});
        }
        stage.updateAnimation();
        stage.updateDraw();
    }

    //------------------------------------------------------------------------------------------

    var vertsArray = [];
    var facesArray = [];
    var posArray = [];
    var maxArray = {x: 100, y: 500, z: 500};
    var minArray = {x: -100, y: -500, z: -500};

    var fLen = -1000000;
    var rotateX = 0;
    var rotateY = 0;
    var rotateZ = 0;

    var stageWidth = stage.width;
    var stageHeight = stage.height;

    var mouseDrag = {x: 0, y: 0};
    var firstDrag = {x: 0, y: 0};
    var veloDrag = {x: 0, y: 0};
    var velocity = {x: 0, y: 0};
    var startDrag = false;

    var shCube = new MovieClip();
    stage.addChild(shCube);

    var infoPos = new MovieClip();
    stage.addChild(infoPos);

    var infoBox = new MovieClip();
    infoBox.graphics.beginFill("#FFFFFF");
    infoBox.graphics.lineStyle(1, "#CCCCCC");
    infoBox.graphics.moveTo(-70, -10);
    infoBox.graphics.lineTo(70, -10);
    infoBox.graphics.lineTo(70, -80);
    infoBox.graphics.lineTo(-70, -80);
    infoBox.graphics.endFill();
    infoPos.addChild(infoBox);

    var textX = new TextField();
    infoPos.addChild(textX);
    var textY = new TextField();
    infoPos.addChild(textY);
    var textZ = new TextField();
    infoPos.addChild(textZ);

    textX.y = -70;
    textX.text = "0000";

    textY.y = -50;
    textY.text = "0000";

    textZ.y = -30;
    textZ.text = "0000";

    var zoom = 1;
    var magnify = 1;
    var facesColors = ["#FF0000", "#00FF00", "#0000FF", "#9900FF", "#FF9900", "#00FFFF"];
    var numberArray = [[[-20, 0, -10], [20, 0, -10], [20, 0, 10], [-20, 0, 10], [-20, 0, -10]],
        [[-20, 0, -10], [-20, 0, 0], [20, 0, 0], [20, 0, -10], [20, 0, 10]],
        [[-20, 0, -10], [-20, 0, 10], [0, 0, 10], [20, 0, -10], [20, 0, 10]],
        [[-20, 0, -10], [-20, 0, 10], [0, 0, 10], [0, 0, -10], [0, 0, 10], [20, 0, 10], [20, 0, -10]],
        [[-20, 0, -10], [0, 0, -10], [0, 0, 10], [-20, 0, 10], [0, 0, 10], [20, 0, 10]],
        [[-20, 0, 10], [-20, 0, -10], [0, 0, -10], [0, 0, 10], [20, 0, 10], [20, 0, -10]],
        [[-20, 0, 10], [-20, 0, -10], [0, 0, -10], [0, 0, 10], [20, 0, 10], [20, 0, -10], [0, 0, -10]],
        [[-20, 0, -10], [-20, 0, 10], [20, 0, 10]],
        [[0, 0, 10], [-20, 0, 10], [-20, 0, -10], [0, 0, -10], [0, 0, 10], [20, 0, 10], [20, 0, -10], [0, 0, -10]],
        [[0, 0, 10], [-20, 0, 10], [-20, 0, -10], [0, 0, -10], [0, 0, 10], [20, 0, 10], [20, 0, -10]]];

    shCube.x = stageWidth / 2;
    shCube.y = stageHeight / 2;

    stage._width = canvasWidth;
    stage._height = canvasHeight;
    stage._x = 0;
    stage._y = 0;

    this.addColor = function (ary) {
        facesColors = ary;
    }

    this.drawGraphic = function (ary) {
        var i
        var j
        for (i = 0; i < ary.length; i++) {
            for (j = 0; j < ary[i].length; j++) {
                vertsArray.push(ary[i][j]);

                if (j != ary[i].length - 1) {
                    facesArray.push({graphic: [vertsArray.length - 1, vertsArray.length], style: 1000000000 + i, hiden: false, select: false, graphno: i, partno: -1});
                }
            }
        }
        // buat selinder
        for (i = 0; i < ary.length; i++) {
            for (j = 0; j < ary[i].length; j++) {

                if (j != ary[i].length - 1) {
                    var r0 = ary[i][j][3] / 2;
                    var r1 = r0 * 0.7;
                    var t0 = ary[i][j][3] / 2;
                    var t1 = t0 * 0.7;

                    var n = ary[i][j];
                    var n0 = ary[i][j];
                    var n1 = ary[i][j + 1];
                    var n2 = ary[i][j + 1];
                    if (j != 0) {
                        n = ary[i][j - 1];
                    }
                    if (j != ary[i].length - 2) {
                        n2 = ary[i][j + 2];
                    }

                    /*
                     7
                     _
                     8 /   \ 6
                     1 |     | 5
                     2 \ _ / 4
                     3
                     
                     15
                     _
                     16 /   \ 14
                     9 |     | 13
                     10 \ _ / 12
                     11
                     */
                    var r = [[0, 0, -r0], [0, -r1, -r1], [0, -r0, 0], [0, -r1, r1], [0, 0, r0], [0, r1, r1], [0, r0, 0], [0, r1, -r1]];
                    var t = [[0, 0, -t0], [0, -t1, -t1], [0, -t0, 0], [0, -t1, r1], [0, 0, t0], [0, t1, t1], [0, t0, 0], [0, t1, -t1]];

                    var rotZ = -(Math.atan2(n0[2] - n[2], n0[0] - n[0]));
                    var rotY = (Math.atan2(n0[1] - n[1], n0[0] - n[0]));
                    var rotZ0 = -(Math.atan2(n1[2] - n0[2], n1[0] - n0[0]));
                    var rotY0 = (Math.atan2(n1[1] - n0[1], n1[0] - n0[0]));
                    var rotZ1 = -(Math.atan2(n2[2] - n1[2], n2[0] - n1[0]));
                    var rotY1 = (Math.atan2(n2[1] - n1[1], n2[0] - n1[0]));

                    var k;
                    for (k = 0; k < r.length; k++) {
                        r[k] = setRotationY(r[k][0], r[k][1], r[k][2], (rotZ0 + rotZ) / 2);
                        r[k] = setRotationZ(r[k][0], r[k][1], r[k][2], (rotY0 + rotY) / 2);
                    }

                    for (k = 0; k < r.length; k++) {
                        if (j != ary[i].length - 2) {
                            t[k] = setRotationY(t[k][0], t[k][1], t[k][2], (rotZ1 + rotZ0) / 2);
                            t[k] = setRotationZ(t[k][0], t[k][1], t[k][2], (rotY1 + rotY0) / 2);
                        } else {
                            t[k] = setRotationY(t[k][0], t[k][1], t[k][2], rotZ0);
                            t[k] = setRotationZ(t[k][0], t[k][1], t[k][2], rotY0);
                        }
                    }

                    var sl = [
                        [n0[0] + r[0][0], n0[1] + r[0][1], n0[2] + r[0][2]], // A
                        [n0[0] + r[1][0], n0[1] + r[1][1], n0[2] + r[1][2]], // B
                        [n0[0] + r[2][0], n0[1] + r[2][1], n0[2] + r[2][2]], // C
                        [n0[0] + r[3][0], n0[1] + r[3][1], n0[2] + r[3][2]], // D
                        [n0[0] + r[4][0], n0[1] + r[4][1], n0[2] + r[4][2]], // E
                        [n0[0] + r[5][0], n0[1] + r[5][1], n0[2] + r[5][2]], // F
                        [n0[0] + r[6][0], n0[1] + r[6][1], n0[2] + r[6][2]], // G
                        [n0[0] + r[7][0], n0[1] + r[7][1], n0[2] + r[7][2]], // H

                        [n1[0] + t[0][0], n1[1] + t[0][1], n1[2] + t[0][2]], // A
                        [n1[0] + t[1][0], n1[1] + t[1][1], n1[2] + t[1][2]], // B
                        [n1[0] + t[2][0], n1[1] + t[2][1], n1[2] + t[2][2]], // C
                        [n1[0] + t[3][0], n1[1] + t[3][1], n1[2] + t[3][2]], // D
                        [n1[0] + t[4][0], n1[1] + t[4][1], n1[2] + t[4][2]], // E
                        [n1[0] + t[5][0], n1[1] + t[5][1], n1[2] + t[5][2]], // F
                        [n1[0] + t[6][0], n1[1] + t[6][1], n1[2] + t[6][2]], // G
                        [n1[0] + t[7][0], n1[1] + t[7][1], n1[2] + t[7][2]]  // H
                    ]
                    for (var k = 0; k < sl.length; k++) {
                        vertsArray.push(sl[k]);
                    }
                    //for (var l=0; l<r.length; l++) {

                    //}
                    facesArray.push({graphic: [vertsArray.length - 1, vertsArray.length - 2, vertsArray.length - 10, vertsArray.length - 9], style: -100000000, hiden: false, select: false, graphno: i, partno: j});
                    facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 3, vertsArray.length - 11, vertsArray.length - 10], style: -100000000, hiden: false, select: false, graphno: i, partno: j});
                    facesArray.push({graphic: [vertsArray.length - 3, vertsArray.length - 4, vertsArray.length - 12, vertsArray.length - 11], style: -100000000, hiden: false, select: false, graphno: i, partno: j});
                    facesArray.push({graphic: [vertsArray.length - 4, vertsArray.length - 5, vertsArray.length - 13, vertsArray.length - 12], style: -100000000, hiden: false, select: false, graphno: i, partno: j});
                    facesArray.push({graphic: [vertsArray.length - 5, vertsArray.length - 6, vertsArray.length - 14, vertsArray.length - 13], style: -100000000, hiden: false, select: false, graphno: i, partno: j});
                    facesArray.push({graphic: [vertsArray.length - 6, vertsArray.length - 7, vertsArray.length - 15, vertsArray.length - 14], style: -100000000, hiden: false, select: false, graphno: i, partno: j});
                    facesArray.push({graphic: [vertsArray.length - 7, vertsArray.length - 8, vertsArray.length - 16, vertsArray.length - 15], style: -100000000, hiden: false, select: false, graphno: i, partno: j});
                    facesArray.push({graphic: [vertsArray.length - 8, vertsArray.length - 1, vertsArray.length - 9, vertsArray.length - 16], style: -100000000, hiden: false, select: false, graphno: i, partno: j});
                }
            }
        }
        generateCoordinat();
        renderView();
        stage.addEventListener("enterFrame", loop);
        stage.addEventListener("mouseDown", mouse_down);
        stage.addEventListener("mouseMove", mouse_move);
        stage.addEventListener("mouseUp", mouse_up);
    }

    this.updateGraphic = function (no, bool) {
        for (var k = 0; k < facesArray.length; k++) {
            if (no == facesArray[k].graphno) {
                facesArray[k].hiden = bool;
            }
        }
    }

    this.setView = function (txt) {
        if (txt == "front") {
            rotateX = 0;
            rotateY = -90;
            rotateZ = 0;
        }
        if (txt == "left") {
            rotateX = 90;
            rotateY = -90;
            rotateZ = 0;
        }
        if (txt == "top") {
            rotateX = 0;
            rotateY = 0;
            rotateZ = 0;
        }
        if (txt == "perspective") {
            rotateX = 45;
            rotateY = -65;
            rotateZ = 0;
        }
        magnify = 1;
    }

    this.zoomIn = function () {
        magnify += 0.05;
    }

    this.zoomOut = function () {
        magnify -= 0.05;
    }

    this.select = function (no, no2) {
        for (var k = 0; k < facesArray.length; k++) {
            facesArray[k].select = false;
            if (no == facesArray[k].graphno && no2 == facesArray[k].partno) {
                facesArray[k].select = true;
            }
        }
    }

    function updatInfo() {
        if (startDrag == false) {
            infoPos.x = mouseX;
            infoPos.y = mouseY;
            infoPos.visible = false;
            for (var m = 0; m < posArray.length; m++) {
                if (posArray[m] != null) {
                    if (Math.abs((posArray[m][0] + shCube.x) - mouseX) < 5 && Math.abs((posArray[m][1] + shCube.y) - mouseY) < 5) {
                        infoPos.visible = true;
                        textX.text = "Depth : " + posArray[m][2];
                        textY.text = "EW : " + posArray[m][3];
                        textZ.text = "NS : " + posArray[m][4];
                        break;
                    }
                }
            }
        }
    }

    function loop(e) {
        velocity.x /= 1.25;
        velocity.y /= 1.25;
        if (!startDrag) {
            rotateX += velocity.x;
            rotateY += velocity.y;
        }
        renderView();
        updatInfo();
    }

    function mouse_down(e) {
        mouseDrag.x = mouseX;
        firstDrag.x = rotateX;
        veloDrag.x = mouseX;

        mouseDrag.y = mouseY;
        firstDrag.y = rotateY;
        veloDrag.y = mouseY;

        startDrag = true;
    }

    function mouse_move(e) {
        if (startDrag) {
            rotateX = firstDrag.x + mouseX - mouseDrag.x;
            velocity.x = mouseX - veloDrag.x;
            veloDrag.x = mouseX;

            rotateY = firstDrag.y + mouseY - mouseDrag.y;
            velocity.y = mouseY - veloDrag.y;
            veloDrag.y = mouseY;
        }
    }

    function mouse_up(e) {
        startDrag = false;
    }

    function generateCoordinat() {
        for (var i = 0; i < vertsArray.length; i++) {
            if (vertsArray[i][0] > maxArray.x) {
                maxArray.x = vertsArray[i][0];
            }
            if (vertsArray[i][1] > maxArray.y) {
                maxArray.y = vertsArray[i][1];
            }
            if (vertsArray[i][2] > maxArray.z) {
                maxArray.z = vertsArray[i][2];
            }
            if (vertsArray[i][0] < minArray.x) {
                minArray.x = vertsArray[i][0];
            }
            if (vertsArray[i][1] < minArray.y) {
                minArray.y = vertsArray[i][1];
            }
            if (vertsArray[i][2] < minArray.z) {
                minArray.z = vertsArray[i][2];
            }
        }

        var spar = 100;
        var txtZoom = 1;
        if (maxArray.x >= 10000) {
            spar = 1000;
            txtZoom = 4;
        }

        minArray.x = Math.floor(minArray.x / spar) * spar;
        minArray.y = Math.floor(minArray.y / spar) * spar;
        minArray.z = Math.floor(minArray.z / spar) * spar;
        maxArray.x = Math.ceil(maxArray.x / spar) * spar;
        maxArray.y = Math.ceil(maxArray.y / spar) * spar;
        maxArray.z = Math.ceil(maxArray.z / spar) * spar;

        zoom = stageHeight / ((maxArray.x - minArray.x) + ((maxArray.y - minArray.y) / 2));

        for (var j = Math.round(minArray.x / spar); j < Math.round(maxArray.x / spar) + 1; j++) {
            if (j != Math.round(maxArray.x / spar) && j != Math.round(minArray.x / spar)) {
                addNumber(j * spar, 0, 0, j * spar, minArray.y, maxArray.z + 100, txtZoom);
            }

            vertsArray.push([j * spar, minArray.y, minArray.z]);
            vertsArray.push([j * spar, minArray.y, maxArray.z]);
            facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: j, hiden: false, select: false, graphno: -2, partno: -1});

            vertsArray.push([j * spar, minArray.y, minArray.z]);
            vertsArray.push([j * spar, maxArray.y, minArray.z]);
            facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: j, hiden: false, select: false, graphno: -2, partno: -1});
        }

        for (var k = Math.round(minArray.y / spar); k < Math.round(maxArray.y / spar) + 1; k++) {
            if (k != Math.round(maxArray.y / spar) && k != Math.round(minArray.y / spar)) {
                addNumber(k * spar, 90, 0, maxArray.x, k * spar, maxArray.z + 100, txtZoom);
            }

            vertsArray.push([minArray.x, k * spar, minArray.z]);
            vertsArray.push([maxArray.x, k * spar, minArray.z]);
            facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: k, hiden: false, select: false, graphno: -2, partno: -1});

            vertsArray.push([maxArray.x, k * spar, minArray.z]);
            vertsArray.push([maxArray.x, k * spar, maxArray.z]);
            facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: k, hiden: false, select: false, graphno: -2, partno: -1});
        }

        for (var l = Math.round(minArray.z / spar); l < Math.round(maxArray.z / spar) + 1; l++) {
            if (l != Math.round(maxArray.z / spar) && l != Math.round(minArray.z / spar)) {
                addNumber(l * spar, 90, -90, maxArray.x, maxArray.y + 100, l * spar, txtZoom);
            }

            vertsArray.push([minArray.x, minArray.y, l * spar]);
            vertsArray.push([maxArray.x, minArray.y, l * spar]);
            facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: l, hiden: false, select: false, graphno: -2, partno: -1});

            vertsArray.push([maxArray.x, minArray.y, l * spar]);
            vertsArray.push([maxArray.x, maxArray.y, l * spar]);
            facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: l, hiden: false, select: false, graphno: -2, partno: -1});
        }

        vertsArray.push([maxArray.x, maxArray.y + 400, 200]);
        vertsArray.push([maxArray.x, maxArray.y + 400, -200]);
        facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: 0, hiden: false, select: false, graphno: -1, partno: -1});

        vertsArray.push([maxArray.x, maxArray.y + 300, 0]);
        vertsArray.push([maxArray.x, maxArray.y + 500, 0]);
        facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: 0, hiden: false, select: false, graphno: -1, partno: -1});

        vertsArray.push([maxArray.x, maxArray.y + 400, 200]);
        vertsArray.push([maxArray.x, maxArray.y + 450, 100]);
        facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: 0, hiden: false, select: false, graphno: -1, partno: -1});

        vertsArray.push([maxArray.x, maxArray.y + 400, 200]);
        vertsArray.push([maxArray.x, maxArray.y + 350, 100]);
        facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: 0, hiden: false, select: false, graphno: -1, partno: -1});

        vertsArray.push([0, minArray.y, minArray.z]);
        vertsArray.push([0, minArray.y, maxArray.z]);
        vertsArray.push([0, maxArray.y, maxArray.z]);
        vertsArray.push([0, maxArray.y, minArray.z]);
        facesArray.push({graphic: [vertsArray.length - 4, vertsArray.length - 3, vertsArray.length - 2, vertsArray.length - 1], style: -1000000000, hiden: false, select: false, graphno: -3, partno: -1});
    }

    function addNumber(val, rotZ, rotY, tx, ty, tz, zm) {

        var digit = 0;
        var txt = Math.abs(val).toString();

        if (val < 0) {
            var ary0 = [0, 0, -10];
            var ary1 = [0, 0, 10];
            ary0 = setRotationZ(ary0[0], ary0[1], ary0[2], rotZ * Math.PI / 180);
            ary0 = setRotationX(ary0[0], ary0[1], ary0[2], rotY * Math.PI / 180);
            ary1 = setRotationZ(ary1[0], ary1[1], ary1[2], rotZ * Math.PI / 180);
            ary1 = setRotationX(ary1[0], ary1[1], ary1[2], rotY * Math.PI / 180);
            vertsArray.push([ary0[0] + tx, ary0[1] + ty, ary0[2] + tz]);
            vertsArray.push([ary1[0] + tx, ary1[1] + ty, ary1[2] + tz]);
            facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: 0, hiden: false, select: false, graphno: -1, partno: -1});
            digit += 40 * zm;
        }

        for (var k = 0; k < txt.length; k++) {
            var no = Number(txt.charAt(k));
            for (var i = 0; i < numberArray[no].length; i++) {
                var ary = [];
                ary[0] = numberArray[no][i][0] * zm;
                ary[1] = numberArray[no][i][1] * zm;
                ary[2] = numberArray[no][i][2] * zm;
                ary = setRotationZ(ary[0], ary[1], ary[2], rotZ * Math.PI / 180);
                ary = setRotationX(ary[0], ary[1], ary[2], rotY * Math.PI / 180);

                if (rotY == -90) {
                    vertsArray.push([ary[0] + tx, ary[1] + ty + digit, ary[2] + tz]);
                } else {
                    vertsArray.push([ary[0] + tx, ary[1] + ty, ary[2] + tz + digit]);
                }
                if (i != 0) {
                    facesArray.push({graphic: [vertsArray.length - 2, vertsArray.length - 1], style: 0, hiden: false, select: false, graphno: -1, partno: -1});
                }
            }

            digit += 40 * zm;
        }
    }

    function renderView() {
        var dispArray = [];
        var distArray = [];
        var vertsNewArray = [];
        var midPoint = [];
        var squarePoint = [(maxArray.x - minArray.x) / 2, (maxArray.y + minArray.y) / 2, (maxArray.z + minArray.z) / 2];

        var dist;
        var curFace;
        var curFaceLen;

        shCube.graphics.clear();

        var zooming = zoom * magnify;

        for (var i = 0; i < vertsArray.length; i++) {
            vertsNewArray[i] = setPointRotationArbitrary((vertsArray[i][0] - squarePoint[0]) * zooming, (vertsArray[i][1] - squarePoint[1]) * zooming, (vertsArray[i][2] - squarePoint[2]) * zooming, rotateX, rotateY, rotateZ);
            vertsNewArray[i] = setRotationY(vertsNewArray[i][0], vertsNewArray[i][1], vertsNewArray[i][2], 0);
            vertsNewArray[i] = setRotationX(vertsNewArray[i][0], vertsNewArray[i][1], vertsNewArray[i][2], 180 * Math.PI / 180);
        }

        for (var k = 0; k < facesArray.length; k++) {
            curFaceLen = facesArray[k].graphic.length;
            midPoint[0] = 0;
            midPoint[1] = 0;
            midPoint[2] = 0;

            for (var l = 0; l < curFaceLen; l++) {
                midPoint[0] += vertsNewArray[facesArray[k].graphic[l]][0];
                midPoint[1] += vertsNewArray[facesArray[k].graphic[l]][1];
                midPoint[2] += vertsNewArray[facesArray[k].graphic[l]][2];
            }

            midPoint[0] /= curFaceLen;
            midPoint[1] /= curFaceLen;
            midPoint[2] /= curFaceLen;

            dist = Math.sqrt(Math.pow(fLen - midPoint[0], 2) + Math.pow(midPoint[1], 2) + Math.pow(midPoint[2], 2));
            distArray[k] = [dist, k];
        }

        distArray.sort(byDist);
        posArray = [];

        for (var m = 0; m < vertsArray.length; m++) {
            dispArray[m] = [fLen / (fLen - vertsNewArray[m][0]) * vertsNewArray[m][1], -fLen / (fLen - vertsNewArray[m][0]) * vertsNewArray[m][2], vertsArray[m][0], vertsArray[m][1], vertsArray[m][2]];
        }

        for (var n = 0; n < facesArray.length; n++) {
            curFace = distArray[n][1];
            curFaceLen = facesArray[curFace].graphic.length;
            posArray[facesArray[curFace].graphic[1]] = [-1000000, -1000000, 0, 0, 0];

            if (facesArray[curFace].hiden == false) {
                if (facesArray[curFace].graphno == -3) {
                    shCube.graphics.beginFill("rgba(0, 153, 0, 0.05)");
                    shCube.graphics.lineStyle(1, "rgba(0, 153, 0, 0.05)");
                } else if (facesArray[curFace].graphno == -1) {
                    //shCube.graphics.beginFill("rgba(0, 0, 0, 0)");
                    shCube.graphics.lineStyle(1, "rgba(0, 0, 0, 1)");
                } else if (facesArray[curFace].graphno == -2) {
                    shCube.graphics.lineStyle(1, "#CCCCCC");
                } else if (facesArray[curFace].graphno >= 0) {
                    if (facesArray[curFace].partno == -1) {
                        shCube.graphics.lineStyle(2, facesColors[facesArray[curFace].graphno]);
                    } else {
                        var rgb = hexToRgb(facesColors[facesArray[curFace].graphno]);
                        if (facesArray[curFace].select == false) {
                            shCube.graphics.beginFill("rgba(" + rgb.r + ", " + rgb.g + ", " + rgb.b + ", 0.1)");
                        } else {
                            shCube.graphics.beginFill("rgba(" + rgb.r + ", " + rgb.g + ", " + rgb.b + ", 1)");
                        }
                        shCube.graphics.lineStyle(1, "rgba(0, 0, 0, 0.0)");
                    }
                }
                shCube.graphics.moveTo(dispArray[facesArray[curFace].graphic[0]][0], dispArray[facesArray[curFace].graphic[0]][1]);
                for (var o = 1; o < facesArray[curFace].graphic.length; o++) {
                    shCube.graphics.lineTo(dispArray[facesArray[curFace].graphic[o]][0], dispArray[facesArray[curFace].graphic[o]][1]);
                }
                shCube.graphics.endFill();

                if (facesArray[curFace].graphno >= 0 && facesArray[curFace].partno == -1) {
                    posArray[facesArray[curFace].graphic[1]] = [dispArray[facesArray[curFace].graphic[1]][0], dispArray[facesArray[curFace].graphic[1]][1], dispArray[facesArray[curFace].graphic[1]][2], dispArray[facesArray[curFace].graphic[1]][3], dispArray[facesArray[curFace].graphic[1]][4]];

                    shCube.graphics.lineStyle(4, facesColors[facesArray[curFace].graphno]);
                    shCube.graphics.moveTo(dispArray[facesArray[curFace].graphic[1]][0] - 2, dispArray[facesArray[curFace].graphic[1]][1]);
                    shCube.graphics.lineTo(dispArray[facesArray[curFace].graphic[1]][0] + 2, dispArray[facesArray[curFace].graphic[1]][1]);
                    shCube.graphics.endFill();
                }
            }
        }
    }

    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    function byDist(v, w) {
        if (v[0] > w[0]) {
            return -1;
        } else if (v[0] < w[0]) {
            return 1;
        } else {
            return 0;
        }
    }

    function setPointRotationArbitrary(tx, ty, tz, rx, ry, rz) {
        var newCoords = [];
        newCoords[0] = tx;
        newCoords[1] = ty;
        newCoords[2] = tz;
        var n0 = [1, 0, 0];
        var n1 = [0, 1, 0];
        var n2 = [0, 0, 1];
        newCoords = setRotationArbitrary(newCoords[0], newCoords[1], newCoords[2], n2[0], n2[1], n2[2], rz * Math.PI / 180);
        n0 = setRotationArbitrary(n0[0], n0[1], n0[2], 0, 0, 1, (rz) * Math.PI / 180);
        n1 = setRotationArbitrary(n1[0], n1[1], n1[2], 0, 0, 1, (rz) * Math.PI / 180);
        newCoords = setRotationArbitrary(newCoords[0], newCoords[1], newCoords[2], n1[0], n1[1], n1[2], ry * Math.PI / 180);
        n0 = setRotationArbitrary(n0[0], n0[1], n0[2], n1[0], n1[1], n1[2], (ry) * Math.PI / 180);
        newCoords = setRotationArbitrary(newCoords[0], newCoords[1], newCoords[2], n0[0], n0[1], n0[2], rx * Math.PI / 180);

        return newCoords;
    }

    function setRotationArbitrary(tx, ty, tz, ux, uy, uz, r) {
        var newCoords = [];
        newCoords[0] = (tx * (Math.cos(r) + (ux * ux) * (1 - Math.cos(r)))) + (ty * (ux * uy * (1 - Math.cos(r)) - uz * Math.sin(r))) + (tz * (ux * uz * (1 - Math.cos(r)) + uy * Math.sin(r)));
        newCoords[1] = (tx * (uy * ux * (1 - Math.cos(r)) + uz * Math.sin(r))) + (ty * (Math.cos(r) + (uy * uy) * (1 - Math.cos(r)))) + (tz * (uy * uz * (1 - Math.cos(r)) - ux * Math.sin(r)));
        newCoords[2] = (tx * (uz * ux * (1 - Math.cos(r)) - uy * Math.sin(r))) + (ty * (uz * uy * (1 - Math.cos(r)) + ux * Math.sin(r))) + (tz * (Math.cos(r) + (uz * uz) * (1 - Math.cos(r))));
        return newCoords;
    }

    function setRotationZ(tx, ty, tz, r) {
        var newCoords = [];
        newCoords[0] = (tx * Math.cos(r) - ty * Math.sin(r));
        newCoords[1] = (tx * Math.sin(r) + ty * Math.cos(r));
        newCoords[2] = (tz);
        return newCoords;
    }

    function setRotationY(tx, ty, tz, r) {
        var newCoords = [];
        newCoords[0] = (tx * Math.cos(r) + tz * Math.sin(r));
        newCoords[1] = (ty);
        newCoords[2] = (-tx * Math.sin(r) + tz * Math.cos(r));
        return newCoords;
    }

    function setRotationX(tx, ty, tz, r) {
        var newCoords = [];
        newCoords[0] = (tx);
        newCoords[1] = (ty * Math.cos(r) - tz * Math.sin(r));
        newCoords[2] = (ty * Math.sin(r) + tz * Math.cos(r));
        return newCoords;
    }

}


