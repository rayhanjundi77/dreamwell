﻿
function dateFormat(date) {
    return moment(date).format('MMM DD, YYYY');;
}
function timeFormat(date) {
    return moment(date).format('HH:mm');;
}

function autoNumericEditable(editable) {
    console.log($(editable.input.$input));
    $(editable.input.$input).autoNumeric('init', {
        aSep: ',',
        aDec: '.',
        aForm: true,
        vMax: '999999999999999999',
        vMin: '0'
    });
}

function autoNumericWithoutDecimal(element) {
    $(element).autoNumeric('destroy');
    $(element).autoNumeric('init', {
        aSep: ',',
        aForm: false,
        vMax: '999999999999999999',
        vMin: '0',
    });
}

//function autoNumeric(element) {
//    $(element).autoNumeric('destroy');
//    $(element).autoNumeric('init', {
//        aSep: ',',
//        aDec: '.',
//        aForm: false,
//        vMax: '999999999999999999.99',
//        vMin: '0',
//    });
//}

//Handlebars.registerHelper('base10XorMin9', function (n) {
//    return ToBase10XorMin9(n);
//});
//Handlebars.registerHelper('base10XorMin6', function (n) {
//    return ToBase10XorMin6(n);
//});
Handlebars.registerHelper('thousandSeparator', function (n) {
    if (n == null || n == "")
        return "";
    else
        return (thousandSeparator(n));
});
Handlebars.registerHelper('thousandSeparatorWithoutComma', function (n) {
    if (n == null || n == "")
        return "";
    else
        return (thousandSeparatorWithoutComma(n));
});
Handlebars.registerHelper('thousandSeparatorDecimal', function (n) {
    if (n == null || n == "")
        return "";
    else
        return (thousandSeparatorFromValueWithComma(n));
});

Handlebars.registerHelper('datePickerFormat', function (date) {
    return moment(date).format('MM/DD/YYYY');
});
Handlebars.registerHelper('dateFormat', function (date) {
    return dateFormat(date);
});
Handlebars.registerHelper('timeFormat', function (date) {
    return timeFormat(date);
});
Handlebars.registerHelper('diffDays', function (date1, date2) {
    var diff = moment(date2).diff(moment(date1), 'days', true); 
    return diff.toFixed(2);
});

function thousandSeparator(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return (x1 + x2) + ".00";
}
function thousandSeparatorDecimal(nStr) {
    nStr = (nStr) + '';//Math.ceil(nStr);;
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '.00';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function thousandSeparatorFromValueWithComma(nStr) {
    nStr = Math.ceil(nStr) + '';//Math.ceil(nStr);;
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function thousandSeparatorWithoutComma(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return (x1 + x2);
}

// Add method to prototype. this allows you to use this function on numbers and strings directly
//Number.prototype.commarize = commarize;
//String.prototype.commarize = commarize;


(function ($) {
    var basePath = '',
        apiBasePath = '',
        currAuthentication = '',
        tokenData = [];


    // Create jQuery body object
    var $body = '.modal-content',
        // Use a tags with 'class="modalTrigger"' as the triggers
        $modalTriggers = $('button.modalTrigger'),

        // Trigger event handler
        openModal = function (evt) {

            var $trigger = $(this),                  // Trigger jQuery object
                modalPath = $trigger.data('href'),       // Modal path is href of trigger
                modalTarget = $trigger.data('target'), // Modal target 
                modalToggle = $trigger.data('toggle'),
                modalOptions = $trigger.data('options'),
                EnableShowModal = $trigger.attr('onShowModal') || 'false',
                $newModal,                               // Declare modal variable
                removeModal = function (evt) {            // Remove modal handler
                    $newModal.off('hidden.bs.modal');  // Turn off 'hide' event
                    $newModal.find($body).html(''); // Remove modal from DOM
                    //$trigger.button('reset');
                },
                showModal = function (data) {             // Ajax complete event handler
                    $trigger.button('reset');
                    $newModal = $(modalTarget).last();    // Modal jQuery object
                    $newModal.find($body).html('');
                    $newModal.find($body).append(data);                // Add to DOM

                    var _modal = function () {

                        return {
                            show: function () {
                                var options = { show: true };
                                options = $.extend({}, options, {
                                    backdrop: "static", //remove ability to close modal with click
                                    keyboard: false, //remove option to close with keyboard
                                    show: true //Display loader!
                                });
                                $newModal.modal(options);           // Showtime!
                                $newModal.on('hidden.bs.modal', removeModal); // Remove modal from DOM on hide
                            }
                        };
                    }();

                    //if (EnableShowModal=='true') {
                    //    $trigger.trigger("onShowModal", _modal);
                    //} else {  }

                    _modal.show();





                };

            if (modalToggle == 'modal-remote') {
                $trigger.button('loading');
                $.get(modalPath, showModal);             // Ajax request
            }
            else {
                console.log('data-toogle is not modal-remote');
            }

            evt.preventDefault();                   // Prevent default a tag behavior
        };

    $(document).on("click", 'button.modalTrigger', openModal); // Add event handlers on body 


    var coreHelper = {
        modalDialog: {
            openModal: function (paramOptions) {
                var
                    //$trigger = options.target,                  // Trigger jQuery object
                    modalPath = paramOptions.href,       // Modal path is href of trigger
                    modalTarget = paramOptions.target, // Modal target 
                    modalToggle = paramOptions.toggle,
                    //modalOptions = $trigger.data('options'),
                    //EnableShowModal = $trigger.attr('onShowModal') || 'false',
                    $newModal,                               // Declare modal variable
                    removeModal = function (evt) {            // Remove modal handler
                        $newModal.off('hidden.bs.modal');  // Turn off 'hide' event
                        $newModal.find($body).html(''); // Remove modal from DOM
                        //$trigger.button('reset');
                    },
                    showModal = function (data) {             // Ajax complete event handler
                        //$trigger.button('reset');
                        $newModal = $(modalTarget).last();    // Modal jQuery object
                        $newModal.find($body).html('');
                        $newModal.find($body).append(data);                // Add to DOM

                        var _modal = function () {

                            return {
                                show: function () {
                                    var options = { show: true };
                                    options = $.extend({}, options, {
                                        backdrop: "static", //remove ability to close modal with click
                                        keyboard: false, //remove option to close with keyboard
                                        show: true //Display loader!
                                    });
                                    $newModal.modal(options);           // Showtime!
                                    $newModal.on('hidden.bs.modal', removeModal); // Remove modal from DOM on hide
                                }
                            };
                        }();

                        //if (EnableShowModal=='true') {
                        //    $trigger.trigger("onShowModal", _modal);
                        //} else {  }

                        _modal.show();





                    };

                if (modalToggle == 'modal-remote') {
                    //$trigger.button('loading');
                    $.get(modalPath, showModal);             // Ajax request
                }
                else {
                    console.error('data-toogle is not modal-remote');
                }


            }
        },
        excel: {
            columnIndexToRangeName: function (index) {
                var dividend = index + 1;
                var name = '';
                var modulo;
                while (dividend > 0) {
                    modulo = (dividend - 1) % 26;
                    name = String.fromCharCode(65 + modulo) + name;
                    dividend = Math.round((dividend - modulo) / 26);
                }
                return name;
            }
        },
        getCheckedValues: function (wrapper) {
            var arr = [],
                $wrapper = $(wrapper);

            $.each($wrapper.find("input:checked"), function (k, v) {
                var $el = $(v),
                    value = $el.val();

                arr.push(value);
            });

            return arr;
        },
        setQueryStringParameter: function (name, value, loc = location) {
            const params = new URLSearchParams(loc.search);
            params.set(name, value);
            window.history.replaceState({}, "", decodeURIComponent(`${loc.pathname}?${params}`));
        },
        controlCheckboxValue: function () {
            $("input:checkbox").on("change", function () {
                $(this).val($(this).is(':checked') ? true : false);
            });
        },
        loadScript: function (url, callback) {
            var script = document.createElement("script")
            script.type = "text/javascript";

            if (script.readyState) {  //IE
                script.onreadystatechange = function () {
                    if (script.readyState == "loaded" ||
                        script.readyState == "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else {  //Others
                script.onload = function () {
                    callback();
                };
            }
            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
        },
        setBasePath: function (path) {
            basePath = path;
            this.setBasePath = null;
        },
        setApiBasePath: function (path) {
            apiBasePath = path;
            this.setApiBasePath = null;
        },
        resolveApi: function (url) {
            return url.replace('~', apiBasePath);
        },
        resolve: function (url) {
            return url.replace('~', basePath);
        },
        setTokenData: function (payload) {
            tokenData = payload;
            sessionStorage.setItem("marvel.userSessionKey", tokenData);
        },
        getTokenData: function () {
            tokenData = tokenData || sessionStorage.getItem("marvel.userSessionKey");
            return tokenData;
        },
        getJqueryObject: function (obj) {
            return (typeof obj === 'string') ? $('#' + obj) : (obj.jquery ? obj : $(obj));
        },
        toDefault: function (data, defaultValue) {
            return typeof data === 'undefined' || data === null || data === '' ? defaultValue : data;
        },
        traverse: function (data, childField, callback) {
            var stack = [data];
            var n;

            while (stack.length > 0) {
                n = stack.pop();
                callback(n);

                if (!n[childField]) {
                    continue;
                }

                for (var i = n[childField].length - 1; i >= 0; i--) {
                    stack.push(n[childField][i]);
                }
            }
        },
        close: function (el, parEl) {
            $(el).closest(parEl).remove();
        },
        onError: function (el, args) {
            var $obj = $.helper.getJqueryObject(el),
                modelState = args.responseJSON.ModelState,
                validationMessageTmpl = kendo.template($("<script type='text/kendo-template' id='message'><div class='k-widget k-tooltip k-tooltip-validation k-invalid-msg field-validation-error' style='margin: 0.5em; display: block; ' data-for='#=field#' data-valmsg-for='#=field#' id='#=field#_validationMessage'><span class='k-icon k-i-warning'></span>#=message# <span class='k-icon k-i-close-circle' style='cursor: pointer' onclick='$.helper.close(this, \".k-widget\");'></span><div class='k-callout k-callout-n'></div></div></script>").html());

            coreHelper.clearError($obj);

            function showMessage(container, name, errors) {
                var wrapper = container.find('span[data-valmsg-for=' + name + '],span[data-val-msg-for=' + name + ']');

                wrapper.parent().append(validationMessageTmpl({ field: name, message: errors[0] }));
            }

            for (var key in modelState) {
                var arr = modelState[key];

                showMessage($obj, key.substr(key.indexOf('.') + 1), arr);
            }
        }
    };

    var controlHelper = {
        form: {
            clear: function (form, excludes) {
                excludes = typeof excludes === 'undefined' ? [] : excludes;
                var $el = coreHelper.getJqueryObject(form);

                $el.find('input, select, textarea').each(function (e) {
                    var obj = $(this),
                        defaultValue = obj.data("defaultValue") || "",
                        includeFilter = typeof obj.data("clearFilter") === 'undefined' ? true : obj.data("clearFilter");

                    if ($.inArray(obj[0].name, excludes) == -1 && includeFilter) {

                        if (obj[0].tagName == 'SELECT') {
                            if (obj.hasClass('select2') || obj.hasClass('select2-hidden-accessible')) {
                                obj.val(null);
                                obj.trigger('change');
                            } else {
                                obj[0].selectedIndex = 0;
                                obj.trigger('change');
                            }
                        } else {
                            obj.val(defaultValue);
                        }
                    }
                });
            },
            fill: function (form, viewData, excludes) {
                if (typeof viewData === 'undefined' || viewData == null) return;
                excludes = typeof excludes === 'undefined' ? [] : excludes;
                var $el = coreHelper.getJqueryObject(form);
                var formEls = $el.find('input, select, textarea, label');
                var self = this,
                    tempObj = {},
                    type = '',
                    field = '';

                $.each(formEls, function (key, obj) {
                    try {
                        if ($.inArray(obj.id, excludes) == -1 || $.inArray(obj.name, excludes) == -1) {
                            tempObj = $(obj);
                            field = typeof tempObj.attr('data-field') !== 'undefined' ? tempObj.data('field') : null;
                            //type = obj.type || typeof obj;
                            type = obj.type !== undefined ? obj.type : tempObj.attr('data-objecttype') !== 'undefined' ? tempObj.data('objecttype') : null;
                            //only attribute data-field broo
                            if (field == null) return;

                            //console.log('type : ' + type);
                            //console.log(tempObj.attr('data-objecttype') !== 'undefined' ? tempObj.data('objecttype') : null);

                            switch (type) {
                                case 'radio':
                                    console.log(obj.value);
                                    if (String(obj.value) == String(viewData[field])) {
                                        obj.checked = true;
                                    }

                                    break;
                                case 'select-one':

                                    /*Single Selection*/
                                    console.log(tempObj);
                                    var formattedValue = coreHelper.toDefault(viewData[field], '');
                                    var fieldText = typeof tempObj.attr('data-text') !== 'undefined' ? tempObj.data('text') : null;
                                    var formattedText = coreHelper.toDefault(viewData[fieldText], '');
                                    //tempObj.val({
                                    //    id: formattedValue,
                                    //    text: formattedText
                                    //}).trigger('change.select2');
                                    tempObj.select2("trigger", "select", {
                                        data: { id: formattedValue, text: formattedText }
                                    });
                                    break;
                                case 'hidden':
                                case 'text':
                                case 'number':
                                case 'email':
                                case 'textarea':
                                    var formattedValue = coreHelper.toDefault(viewData[field], '');
                                    if (tempObj.hasClass("date-picker")) {
                                        var format = tempObj.data("format") || "mm/dd/yyyy";
                                        //formattedValue = moment(formattedValue).format("yyyy/MM/dd");
                                        var resultFormat = moment(viewData[field]).format("MM/DD/YYYY");
                                        tempObj.datepicker('setDate', resultFormat);
                                        break;
                                    }
                                    else {
                                        switch (tempObj.data("inputmask")) {
                                            case "'alias': 'currency'":
                                                formattedValue = thousandSeparatorWithoutComma(formattedValue);
                                                break;
                                        }
                                    }
                                    tempObj.val(formattedValue);
                                    break;
                                case 'label':
                                    var formattedValue = coreHelper.toDefault(viewData[field], '');
                                    if (tempObj.hasClass("date-picker")) {

                                        var format = tempObj.data("format") || "MM/DD/YYYY";
                                        formattedValue = moment(formattedValue).format(format)
                                    }
                                    tempObj.text(formattedValue);
                                    break;
                                case 'select2':
                                    alert();
                                    break;
                                //default:
                                //    tempObj.html(formattedValue);
                                //    break;

                            }
                        }
                    } catch (e) {
                        
                    }

                    
                });
            },
            fillmultiple: function (form, viewData, excludes) {
                if (typeof viewData === 'undefined' || viewData == null) return;
                excludes = typeof excludes === 'undefined' ? [] : excludes;
                var $el = coreHelper.getJqueryObject(form);
                var formEls = $el.find('input, select, textarea, label');
                var self = this,
                    tempObj = {},
                    type = '',
                    field = '';

                $.each(formEls, function (key, obj) {
                    if ($.inArray(obj.id, excludes) == -1 || $.inArray(obj.name, excludes) == -1) {
                        tempObj = $(obj);
                        field = typeof tempObj.attr('data-field') !== 'undefined' ? tempObj.data('field') : null;
                        //type = obj.type || typeof obj;
                        type = obj.type !== undefined ? obj.type : tempObj.attr('data-objecttype') !== 'undefined' ? tempObj.data('objecttype') : null;
                        //only attribute data-field broo
                        if (field == null) return;

                        switch (type) {
                            case 'radio':
                                console.log(obj.value);
                                if (String(obj.value) == String(viewData[field])) {
                                    obj.checked = true;
                                }
                                break;

                            case 'select-one':
                                /*Single Selection*/
                                var formattedValue = coreHelper.toDefault(viewData[field], '');
                                var fieldText = typeof tempObj.attr('data-text') !== 'undefined' ? tempObj.data('text') : null;
                                var formattedText = coreHelper.toDefault(viewData[fieldText], '');
                                tempObj.select2("trigger", "select", {
                                    data: { id: formattedValue, text: formattedText }
                                });
                                break;
                            case 'hidden':
                            case 'text':
                            case 'email':
                            case 'textarea':
                                var formattedValue = coreHelper.toDefault(viewData[field], '');
                                if (tempObj.hasClass("date-picker")) {
                                    var format = tempObj.data("format") || "MM/dd/yyyy";
                                    formattedValue = moment(formattedValue).format(format)
                                    tempObj.datepicker('setDate', formattedValue);
                                    break;
                                }
                                tempObj.val(formattedValue);
                                break;
                            case 'label':
                                var formattedValue = coreHelper.toDefault(viewData[field], '');
                                if (tempObj.hasClass("date-picker")) {

                                    var format = tempObj.data("format") || "MM/DD/YYYY";
                                    formattedValue = moment(formattedValue).format(format)
                                }
                                tempObj.text(formattedValue);
                                break;
                        }
                    }
                });
            }
        }
    };

    var KendoHelper = {
        kendoUI: {
            notification: {

            },
            multiSelect: {
                setValue: function (el, value) {
                    if (value == null) return;
                    var t = $(el).data("kendoMultiSelect");
                    t.dataSource.filter({}); //clear applied filter before setting value
                    t.value(value);
                }
            },
            dropdownlist: {
                build: function () {

                },
                setValue: function (el, value) {
                    if (value.id == null) return;
                    var t = $(el).data("kendoDropDownList");
                    t.dataSource.add(value);
                    t.value(value.id);
                }
            },
            combobox: {
                build: function (el, options) {
                    var defaultOptions = {
                        placeholder: "Choose ...",
                        dataTextField: "text",
                        dataValueField: "id",
                        filter: "contains",
                        autoBind: false,
                        dataSource: {
                            type: "json",
                            serverFiltering: true,
                            transport: {
                                read: {
                                    url: options.options.url || '/',
                                    contentType: 'application/json',
                                    dataType: 'json',
                                    type: 'POST',
                                    cache: false
                                },
                                parameterMap: function (data, operation) {
                                    var mapRequest = data;
                                    return JSON.stringify(mapRequest);
                                }
                            },
                            schema: {
                                model: {
                                    id: "id",
                                    children: "items"
                                },
                                data: "items"
                            }

                        }
                    }
                },
                setValue: function (el, value) {
                    if (value.id == null) return;
                    var t = $(el).data("kendoComboBox");
                    t.dataSource.add(value);
                    t.value(value.id);
                }
            },
            dropDownTree: {
                setValue: function (el, value) {
                    var t = $(el).data("kendoDropDownTree");
                    t.dataSource.add(value);
                    t.value(value.id);
                }
            },
            grid: function (el, options) {
                var defaultOptions = {
                    dataSource: {
                        transport: {
                            read: {
                                url: options.options.url || '/',
                                contentType: 'application/json',
                                dataType: 'json',
                                type: 'POST',
                                cache: false
                            },
                            parameterMap: function (data, operation) {
                                if (operation !== "read" && data) {
                                    //options.options.parameterMap(data, operation);
                                    console.log("paramdata");
                                    console.log(data);
                                    //console.log(Array.isArray(data.model))
                                    return JSON.stringify(data);
                                }
                                //if (operation !== "read" && data.models) {
                                //    console.log(data)
                                //    console.log(data.models)
                                //    return { models: kendo.stringify(data.models) };
                                //}

                                //if (operation !== "read" && options.models) {
                                //    return { models: kendo.stringify(options.models) };
                                //}

                                if (operation == "read") {
                                    var mapRequest = data;
                                    var extractOption = { columns: [], search: options.options.search || { value: "", regex: false } };
                                    $.each(options.columns, function (i, v) {
                                        extractOption.columns.push(
                                            {
                                                field: v.field,
                                                title: v.title,
                                                aggregates: v.aggregates,
                                                searchable: v.searchable || false,
                                                sortable: v.sortable || true
                                            });
                                    });
                                    mapRequest = $.extend(true, mapRequest, extractOption);
                                    return JSON.stringify(mapRequest);
                                }
                            }
                        },
                        schema: {
                            model: {
                                id: "id",
                                fields: {
                                    id: { editable: true, nullable: true }
                                }
                            },
                            data: "data",
                            total: "recordsTotal"
                        },
                        pageSize: 5,
                        serverPaging: true,
                        serverSorting: true
                    },
                    groupable: true,
                    sortable: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 5
                    },
                    columns: [],
                    marvelCheckboxSelectable: {
                        enable: true,
                        listCheckedTemp: options.marvelCheckboxSelectable.listCheckedTemp || []
                    }
                };

                defaultOptions = $.extend(true, defaultOptions, options);
                //console.log(defaultOptions);
                if (defaultOptions.marvelCheckboxSelectable.enable) {

                    defaultOptions.columns.splice(0, 0, {
                        selectable: true, width: "50px"
                    });
                }
                $(el).kendoGrid(defaultOptions);

                if (defaultOptions.marvelCheckboxSelectable.enable) {
                    $(el).data('kendoGrid').bind("change", function (e, arg) {
                        var grid = e.sender;
                        var items = grid.items();
                        items.each(function (idx, row) {
                            var idValue = grid.dataItem(row).get(options.schema.model.id);
                            var isChecked = row.className.indexOf("k-state-selected") >= 0;
                            if (isChecked) {
                                if (defaultOptions.marvelCheckboxSelectable.listCheckedTemp.indexOf(idValue) < 0) {
                                    defaultOptions.marvelCheckboxSelectable.listCheckedTemp.push(idValue);
                                }
                            }
                            else {
                                if (defaultOptions.marvelCheckboxSelectable.listCheckedTemp.indexOf(idValue) >= 0) {
                                    defaultOptions.marvelCheckboxSelectable.listCheckedTemp.splice(defaultOptions.marvelCheckboxSelectable.listCheckedTemp.indexOf(idValue), 1);
                                }
                            }
                        });
                    });

                    $(el).data('kendoGrid').bind("dataBound", function (e) {
                        var grid = e.sender;

                        grid.wrapper.find(".k-grouping-row .k-icon").on("click", function () {
                            $.helper.kendoUI.resizeGrid();
                        });


                        var items = grid.items();
                        var itemsToSelect = [];
                        items.each(function (idx, row) {
                            var dataItem = grid.dataItem(row);
                            if (defaultOptions.marvelCheckboxSelectable.listCheckedTemp.indexOf(dataItem[options.schema.model.id]) >= 0) {
                                itemsToSelect.push(row);
                            }
                        });
                        e.sender.select(itemsToSelect);
                    });
                }



            },
            //resizeGrid: function (grid) {
            //    setTimeout(function () {
            //        var lockedContent = grid.wrapper.children(".k-grid-content-locked")
            //        var content = grid.wrapper.children(".k-grid-content");

            //        grid.wrapper.height("");
            //        lockedContent.height("");
            //        content.height("");

            //        grid.wrapper.height(grid.wrapper.height());

            //        grid.resize();
            //    });
            //}
            resizeGrid: function (options) {
                if (options === null || options === undefined) {
                    options = {
                        size: 0.55,
                        gridContentCss: ".k-grid-content",
                        gridLockedContentCss: ".k-grid-content-locked",
                        gridsToResize: []
                    };
                }


                var windowHeight = $(window).height() * options.size;

                if (options.gridsToResize !== null && options.gridsToResize.length > 0) {
                    options.gridsToResize.forEach(function (item) {
                        var gridContent = $('#' + item + ' > ' + options.gridContentCss);


                        var lockedContent = $('#' + item + ' > ' + options.gridLockedContentCss);

                        gridContent.height(windowHeight);

                        if (lockedContent !== null && lockedContent !== undefined) {
                            lockedContent.height(windowHeight);

                        }
                    });
                }
                else {
                    var gridContent = $(options.gridContentCss);
                    var lockedContent = $(options.gridLockedContentCss);

                    gridContent.height(windowHeight);

                    if (lockedContent !== null && lockedContent !== undefined) {
                        lockedContent.height(windowHeight);

                    }
                }

                $(window).resize(function () {
                    $.helper.kendoUI.resizeGrid(options);
                });

            }
        }
    };

    var pluginHelper = {
        noty: {
            error: function (title, msg, timer = 20000) {
                $.niftyNoty({
                    type: 'warning',
                    container: 'floating',
                    html: '<h4 class="alert-title">' + title + '</h4><p class="alert-message">' + msg + '</p>',
                    closeBtn: true,
                    floating: {
                        position: 'top-right',
                        animationIn: 'bounceInRight',
                        animationOut: 'bounceOutRight'
                    },
                    focus: true,
                    timer: timer
                });
            },
            success: function (title, msg, timer = 3000) {
                $.niftyNoty({
                    type: 'success',
                    container: 'floating',
                    html: '<h4 class="alert-title">' + title + '</h4><p class="alert-message">' + msg + '</p>',
                    closeBtn: true,
                    floating: {
                        position: 'top-right',
                        animationIn: 'bounceInRight',
                        animationOut: 'bounceOutRight'
                    },
                    focus: true,
                    timer: timer
                });
            },
            show: function (type, title, msg, timer = 3000) {
                $.niftyNoty({
                    type: type,
                    container: 'floating',
                    html: '<h4 class="alert-title">' + title + '</h4><p class="alert-message">' + msg + '</p>',
                    closeBtn: true,
                    floating: {
                        position: 'top-right',
                        animationIn: 'bounceInRight',
                        animationOut: 'bounceOutRight'
                    },
                    focus: true,
                    timer: timer
                });
            }
        }

    };

    $.helper = $.extend({}, controlHelper, coreHelper, KendoHelper, pluginHelper);



    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || null);
            } else {
                o[this.name] = this.value || null;
            }
        });
        return o;
    };


    $.fn.cmserializeToJSON = function () {
        var obj = {};
        /* Iterate over every input. */
        var data = this.serializeArray();
        console.log(data);
        data.map(function (data, i) {
            if (data.name.indexOf(".") > 0) {
                var prop = data.name.split(".");
                var key = prop[0]
                var val = prop[1]
                if (val.indexOf("[") > 0) {
                    var index = parseInt(val.match(/\d+/), 10);
                    if (!Array.isArray(obj[key])) {
                        obj[key] = []
                    }
                    if (typeof obj[key][index] != "object") {
                        obj[key][index] = {}
                    }
                    val = val.substr(0, val.length - 3);
                    obj[key][index][val] = data.value;
                } else {
                    if (typeof obj[key] != "object") {
                        obj[key] = {}
                    }
                    obj[key][val] = data.value;
                }
            } else {
                if (data.value == undefined || data.value == "")
                    obj[data.name] = null;
                else
                    obj[data.name] = data.value;
            }
        });

        /* Stringify the object and return it. */
        return JSON.stringify(obj, null, 4);
    };


    Handlebars.registerHelper('compare', function (lvalue, operator, rvalue, options) {

        var operators, result;

        if (arguments.length < 3) {
            throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
        }

        if (options === undefined) {
            options = rvalue;
            rvalue = operator;
            operator = "===";
        }

        operators = {
            '==': function (l, r) { return l == r; },
            '===': function (l, r) { return l === r; },
            '!=': function (l, r) { return l != r; },
            '!==': function (l, r) { return l !== r; },
            '<': function (l, r) { return l < r; },
            '>': function (l, r) { return l > r; },
            '<=': function (l, r) { return l <= r; },
            '>=': function (l, r) { return l >= r; },
            'typeof': function (l, r) { return typeof l == r; },
            '%': function (l, r) { return l % r == 0; }
        };

        if (!operators[operator]) {
            throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
        }

        result = operators[operator](lvalue, rvalue);

        if (result) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }

    });

}(jQuery));