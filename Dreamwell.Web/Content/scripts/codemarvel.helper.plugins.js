﻿(function ($) {


    //function Marvel() {

    //    function easyUI() {
    //        // Create a new copy of jQuery using sub()
    //        var plugin = jQuery.sub(),
    //            treeGrid = function () {
    //                // Extend that copy with the new plugin methods
    //                plugin.fn.extend({
    //                    treeGrid: function () {
    //                        console.log('TEST TREE GRID PLUGIN');
    //                    }

    //                });

    //                // Add our plugin to the original jQuery
    //                jQuery.fn.marvel.easyUI.treeGrid = function () {
    //                    this.addClass("plugin");

    //                    // Make sure our plugin returns our special plugin version of jQuery
    //                    return plugin(this);
    //                };
    //            }


    //        return {
    //            treeGrid:treeGrid()

    //        }

    //    };


    //    return {
    //        register: {
    //            easyUI: {
    //                treeGrid: easyUI.treeGrid
    //            }
    //        }
    //    }
    //};

    ////Register 
    //Marvel.register.easyUI.treeGrid();

    //$.fn.cmTreeGrid = function (options) {
    //    console.log(options);
    //    console.log("test plugin");
    //    return this;

    //};



    $.fn.extend(
        {
            cmComboTree: function (paramOptions) {
                var options = {
                    url: "/list",
                    method: "POST",
                    searchTarget: '',
                    base: {
                        //idField: 'id',
                        //treeField: 'description',
                        multiSort: true,
                        filterBtnIconCls: 'icon-filter',
                        remoteFilter: true,
                        rownumbers: true,
                        multiSort: true,
                        width: "100%",
                        //fitColumns: true,
                        striped: true,
                        filterColumns: [],
                        orders: [],
                        collapsible: true,

                    }
                }
                options = $.extend(true, options, paramOptions);

                var defaultOptions = {

                    //url: 'get_data.php',

                    //columns: [[
                    //    { field: 'description', width: 180 },
                    //    { field: 'persons', title: 'Persons', width: 60, align: 'right' },
                    //    { field: 'begin', title: 'Begin Date', width: 80 },
                    //    { field: 'end', title: 'End Date', width: 80 }
                    //]]

                    //loader: function (param, success, error) {
                    //    //console.log('tree param');
                    //    //console.log(param);
                    //    var opts = $(this).combotree('options');
                    //    console.log('tree option');
                    //    console.log(opts);
                    //    //var filterRules = [];
                    //    //if (!options.url) return false;
                    //    ////if (param.filterRules != undefined) {
                    //    ////    filterRules = jQuery.parseJSON(param.filterRules);
                    //    ////}



                    //    //sort click print 
                    //    //opts.sortName: "description,uom_code"
                    //    //opts.sortOrder: "asc,asc"
                    //    var requestBody = {
                    //        //Orders: opts.orders,
                    //        //FilterColumns: opts.generalFilterColumns,
                    //        //filterRules: filterRules,
                    //        //Search: $(options.searchTarget).val() || '',
                    //        //ReloadByParentId: param.id

                    //    };

                    //    console.log(requestBody);

                    //    $.ajax({
                    //        type: "POST",
                    //        url: options.url,
                    //        contentType: "application/json; charset=utf-8",
                    //        data: JSON.stringify(requestBody),
                    //        dataType: 'json',
                    //        success: function (response) {
                    //            console.log(response);
                    //            success(response.data.children);
                    //        },
                    //        error: function () {
                    //            console.log('err');
                    //            error.apply(this, arguments);
                    //        }
                    //    });


                    //},
                    //pageSize: 10,
                    //pageList: [10, 20, 30, 40, 50, 100],
                }
                defaultOptions = $.extend(true, defaultOptions, options.base);

                this.combotree(defaultOptions);

                return this;

            },
            cmTreeGrid: function (paramOptions) {
                var options = {
                    url: "/list",
                    method: "POST",
                    searchTarget: '',
                    base: {
                        //idField: 'id',
                        //treeField: 'description',
                        multiSort: true,
                        filterBtnIconCls: 'icon-filter',
                        remoteFilter: true,
                        rownumbers: true,
                        multiSort: true,
                        width: "100%",
                        //fitColumns: true,
                        striped: true,
                        filterColumns: [],
                        orders: [],
                        collapsible: true,

                    }
                }
                options = $.extend(true, options, paramOptions);

                var defaultOptions = {

                    //url: 'get_data.php',

                    //columns: [[
                    //    { field: 'description', width: 180 },
                    //    { field: 'persons', title: 'Persons', width: 60, align: 'right' },
                    //    { field: 'begin', title: 'Begin Date', width: 80 },
                    //    { field: 'end', title: 'End Date', width: 80 }
                    //]]

                    loader: function (param, success, error) {
                        console.log('tree param');
                        console.log(param);
                        var opts = $(this).treegrid('options');
                        console.log('tree option');
                        console.log(opts);
                        var filterRules = [];
                        if (!options.url) return false;
                        //if (param.filterRules != undefined) {
                        //    filterRules = jQuery.parseJSON(param.filterRules);
                        //}



                        //sort click print 
                        //opts.sortName: "description,uom_code"
                        //opts.sortOrder: "asc,asc"
                        var requestBody = {
                            Orders: opts.orders,
                            FilterColumns: opts.generalFilterColumns,
                            //filterRules: filterRules,
                            Search: $(options.searchTarget).val() || '',
                            ReloadByParentId: param.id

                        };

                        console.log(requestBody);

                        $.ajax({
                            type: "POST",
                            url: options.url,
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify(requestBody),
                            dataType: 'json',
                            success: function (response) {
                                console.log(response);
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err');
                                error.apply(this, arguments);
                            }
                        });


                    },
                    pageSize: 10,
                    pageList: [10, 20, 30, 40, 50, 100],
                }
                defaultOptions = $.extend(true, defaultOptions, options.base);
              
                this.treegrid(defaultOptions);

                return this;

            },
            modalDialog: {
                modal: function (options) {
                    //var $trigger = $(this);
                    var defOptions = {
                        modalPath: "",
                        modalTarget: "",
                        modalToggle: "modal-remote",
                        show: true
                    };
                    defOptions = $.extend(true, defOptions, options);
                    console.log(defOptions);


                    var $newModal,                               // Declare modal variable
                        $body = '.modal-content',
                        removeModal = function (evt) {            // Remove modal handler
                            $newModal.off('hidden.bs.modal');  // Turn off 'hide' event
                            $newModal.find($body).html(''); // Remove modal from DOM
                            //$trigger.button('reset');
                        },
                        showModal = function (data) {             // Ajax complete event handler
                            //$trigger.button('reset');
                            $newModal = $(defOptions.modalTarget).last();    // Modal jQuery object
                            $newModal.find($body).html('');
                            $newModal.find($body).append(data);                // Add to DOM

                            var _modal = function () {

                                return {
                                    show: function () {
                                        var opt = { show: true };
                                        opt = $.extend({}, opt, {
                                            backdrop: "static", //remove ability to close modal with click
                                            keyboard: false, //remove option to close with keyboard
                                            show: true //Display loader!
                                        });
                                        $newModal.modal(opt);           // Showtime!
                                        $newModal.on('hidden.bs.modal', removeModal); // Remove modal from DOM on hide
                                    }
                                };
                            }();

                            //if (EnableShowModal=='true') {
                            //    $trigger.trigger("onShowModal", _modal);
                            //} else {  }

                            _modal.show();


                        };


                    if (defOptions.modalToggle == 'modal-remote') {
                        //$trigger.button('loading');
                        $.get(defOptions.modalPath, showModal);             // Ajax request
                    }
                    else {
                        console.log('data-toogle is not modal-remote');
                    }
                }
            },
            cmRoleObj: function (fn) {
                var e = this;
                var dataValue = 0;
                var MAX_ENTITY_COUNT = 10000;
                var ACCESS_NOACCESS = 0;
                var ACCESS_OWNRECORD = 1;
                var ACCESS_BUSINESSUNIT = MAX_ENTITY_COUNT;
                var ACCESS_PARENTCHILD = MAX_ENTITY_COUNT * ACCESS_BUSINESSUNIT;
                var ACCESS_ORGANIZATION = MAX_ENTITY_COUNT * ACCESS_PARENTCHILD;

                switch (e.attr('data-value')) {
                    case "ACCESS_NOACCESS":
                        dataValue = ACCESS_NOACCESS;
                        break;
                    case "ACCESS_OWNRECORD":
                        dataValue = ACCESS_OWNRECORD;
                        break;
                    case "ACCESS_BUSINESSUNIT":
                        dataValue = ACCESS_BUSINESSUNIT;
                        break;
                    case "ACCESS_PARENTCHILD":
                        dataValue = ACCESS_PARENTCHILD;
                        break;
                    case "ACCESS_ORGANIZATION":
                        dataValue = ACCESS_ORGANIZATION;
                        break;
                    default:
                        dataValue = ACCESS_NOACCESS;
                        break;
                }

                var crud_access_options =
                    [
                        { id: ACCESS_NOACCESS, text: 'No Access', html: 'ten' },
                        { id: ACCESS_OWNRECORD, text: 'Own Record', html: 'twentyfive' },
                        { id: ACCESS_BUSINESSUNIT, text: 'Current Organization', html: 'fifty' },
                        { id: ACCESS_PARENTCHILD, text: 'Current & Sub Organization', html: 'seventyfive' },
                        { id: ACCESS_ORGANIZATION, text: 'Full Access', html: 'onehundred' }
                    ];

                var _getNextKey = function (currVal) {
                    var nextIDX = 0;


                    var nRes = false;
                    crud_access_options.map(function (item, index) {
                        if (item.id == currVal) {
                            nRes = true;
                            nextIDX = index + 1;
                        }

                        return item;
                    });

                    if (nRes) {
                        if (!(nextIDX < crud_access_options.length)) {
                            nextIDX = 0;
                        }
                    }


                    return nextIDX;
                }

                if (dataValue > 1) {
                    $(e).each(function () {
                        var m = this;

                        var _init = document.createElement("a");
                        var currVal = dataValue;
                        var nextIDX = _getNextKey(currVal);
                        nextIDX = (nextIDX > 0 ? nextIDX - 1 : nextIDX);

                        var pieID = Math.floor((Math.random() * 100000) + 1);
                        do {
                            if ($('input[data-cmpie="' + pieID + '"]').length > 0) {
                                pieID = Math.floor((Math.random() * 100000) + 1);
                            }
                            else {
                                break;
                            }
                        } while (true)

                        $(m).attr('data-cmpie', pieID);

                        $(_init).attr({
                            "href": "javascript:void(0);",
                            "title": crud_access_options[nextIDX].text,
                            "data-idx": pieID,
                            "data-value": crud_access_options[nextIDX].id
                        }).html('<pie class="' + crud_access_options[nextIDX].html + '"></pie>');

                        $(m).after(_init);
                        $(m).val(crud_access_options[nextIDX].id);
                        $(m).hide();
                    });
                }
                else {
                    if (dataValue > 0) {
                        var m = e;

                        var _init = document.createElement("a");
                        var currVal = dataValue;
                        var nextIDX = _getNextKey(currVal);
                        nextIDX = (nextIDX > 0 ? nextIDX - 1 : nextIDX);

                        var pieID = Math.floor((Math.random() * 100000) + 1);
                        do {
                            if ($('input[data-cmpie="' + pieID + '"]').length > 0) {
                                pieID = Math.floor((Math.random() * 100000) + 1);
                            }
                            else {
                                break;
                            }
                        } while (true)

                        $(m).attr('data-cmpie', pieID);

                        $(_init).attr({
                            "href": "javascript:void(0);",
                            "title": crud_access_options[nextIDX].text,
                            "data-idx": pieID,
                            "data-value": crud_access_options[nextIDX].id
                        }).html('<pie class="' + crud_access_options[nextIDX].html + '"></pie>');

                        $(m).after(_init);
                        $(m).val(crud_access_options[nextIDX].id);

                        $(m).hide();
                    }
                }
            },
            cmRole: function (fn) {
                var e = this;

                var MAX_ENTITY_COUNT = 10000;
                var ACCESS_NOACCESS = 0;
                var ACCESS_OWNRECORD = 1;
                var ACCESS_BUSINESSUNIT = MAX_ENTITY_COUNT;
                var ACCESS_PARENTCHILD = MAX_ENTITY_COUNT * ACCESS_BUSINESSUNIT;
                var ACCESS_ORGANIZATION = MAX_ENTITY_COUNT * ACCESS_PARENTCHILD;
                var crud_access_options =
                    [
                        { id: ACCESS_NOACCESS, text: 'No Access', html: 'ten' },
                        { id: ACCESS_OWNRECORD, text: 'Own Record', html: 'twentyfive' },
                        { id: ACCESS_BUSINESSUNIT, text: 'Current Organization', html: 'fifty' },
                        { id: ACCESS_PARENTCHILD, text: 'Current & Sub Organization', html: 'seventyfive' },
                        { id: ACCESS_ORGANIZATION, text: 'Full Access', html: 'onehundred' }
                    ];
                var _getNextKey = function (currVal) {
                    var nextIDX = 0;

                    if (currVal.trim() != '') {
                        var nRes = false;
                        crud_access_options.map(function (item, index) {
                            if (item.id == currVal) {
                                nRes = true;
                                nextIDX = index + 1;
                            }

                            return item;
                        });
                        if (nRes) {
                            if ((nextIDX > crud_access_options.length)) {
                                nextIDX = 0;
                            }
                        }
                    }

                    return nextIDX;
                }

                if ($(e).length > 1) {
                    $(e).each(function () {
                        var m = this;
                        var _init = document.createElement("button");
                        var currVal = $(m).val();
                        var nextIDX = _getNextKey(currVal);
                        nextIDX = (nextIDX > 0 ? nextIDX - 1 : nextIDX);

                        var pieID = Math.floor((Math.random() * 100000) + 1);
                        do {
                            if ($('input[data-cmpie="' + pieID + '"]').length > 0) {
                                pieID = Math.floor((Math.random() * 100000) + 1);
                            }
                            else {
                                break;
                            }
                        } while (true)

                        $(m).attr('data-cmpie', pieID);
                        $(_init).attr({
                            "title": crud_access_options[nextIDX].text,
                            "data-idx": pieID,
                            "data-value": crud_access_options[nextIDX].id,
                            "class": "btn btn-sm btn-icon btn-circle"
                        }).html('<pie class="' + crud_access_options[nextIDX].html + '"></pie>');

                        $(m).after(_init);
                        $(m).val(crud_access_options[nextIDX].id);

                        $(_init).off('click').on('click', function () {
                            var EcurrVal = $(this).attr('data-value');
                            var EpieID = $(this).attr('data-idx');
                            var EnextIDX = _getNextKey(EcurrVal);
                            EnextIDX = (EnextIDX == crud_access_options.length ? 0 : EnextIDX);
                            $(this).attr({
                                "data-value": crud_access_options[EnextIDX].id,
                                "title": crud_access_options[EnextIDX].text,
                            }).find('pie').attr({
                                'class': crud_access_options[EnextIDX].html
                            });

                            $('input[data-cmpie="' + EpieID + '"]').val(crud_access_options[EnextIDX].id);

                            if (fn) {
                                fn(m);
                            }
                        });

                        $(m).hide();
                    });
                }
                else {
                    if ($(e).length > 0) {
                        var m = e;

                        var _init = document.createElement("a");
                        var currVal = $(m).val();
                        var nextIDX = _getNextKey(currVal);
                        nextIDX = (nextIDX > 0 ? nextIDX - 1 : nextIDX);

                        var pieID = Math.floor((Math.random() * 100000) + 1);
                        do {
                            if ($('input[data-cmpie="' + pieID + '"]').length > 0) {
                                pieID = Math.floor((Math.random() * 100000) + 1);
                            }
                            else {
                                break;
                            }
                        } while (true)

                        $(m).attr('data-cmpie', pieID);

                        $(_init).attr({
                            "href": "javascript:void(0);",
                            "title": crud_access_options[nextIDX].text,
                            "data-idx": pieID,
                            "data-value": crud_access_options[nextIDX].id
                        }).html('<pie class="' + crud_access_options[nextIDX].html + '"></pie>');

                        $(m).after(_init);
                        $(m).val(crud_access_options[nextIDX].id);

                        $(_init).off('click').on('click', function () {
                            var EcurrVal = $(this).attr('data-value');
                            var EpieID = $(this).attr('data-idx');
                            var EnextIDX = _getNextKey(EcurrVal);
                            EnextIDX = (EnextIDX == crud_access_options.length ? 0 : EnextIDX);
                            $(this).attr({
                                "data-value": crud_access_options[EnextIDX].id,
                                "title": crud_access_options[EnextIDX].text
                            }).find('pie').attr({
                                'class': crud_access_options[EnextIDX].html
                            });

                            $('input[data-cmpie="' + EpieID + '"]').val(crud_access_options[EnextIDX].id);

                            if (fn) {
                                fn(m);
                            }
                        });

                        $(m).hide();
                    }
                }
            },
            cmSelect2: function (options) {
                var e = this;
                var dt_options = {
                    url: '',
                    filter: function (params) {
                        return {
                            logic: "OR",
                            filters: []
                        }
                    },
                    result: {
                        id: '',
                        text: ''
                    },
                    options: {
                        placeholder: "Choose"
                    }
                };
                dt_options = $.extend(dt_options, options);
                if (options.url != '') {

                    var select2Options = {
                        //width: "100%",
                        ajax: {
                            method: 'POST',
                            url: options.url,
                            dataType: 'json',
                            contentType: 'application/json',
                            delay: 1000,
                            data: function (params) {
                                var filters = options.filters(params);
                                var requestParameter = {
                                    pageSize: 100,
                                    filter: {
                                        logic: "OR",
                                        filters: filters
                                    }
                                }
                                return JSON.stringify(requestParameter);
                            },
                            processResults: function (data, page) {

                                var result = { results: [], more: false };
                                if (data && data.items) {
                                    result.more = data.totalPages > data.currentPage;
                                    $.each(data.items, function () {
                                        var i = this;
                                        var res = options.result;

                                        var obj = {};
                                        Object.keys(res).map(function (key, index) {
                                            obj[key] = i[res[key]];
                                        });

                                        result.results.push(obj);
                                    });
                                };
                                return result;
                            },
                            cache: true
                        },
                        escapeMarkup: function (markup) {
                            return markup;
                        }, // let our custom formatter work
                        minimumInputLength: 0
                    };
                    select2Options = $.extend(true, select2Options, dt_options.options);

                    if ($(e).length > 1) {
                        $(e).each(function () {
                            var m = this;
                            $(m).select2(select2Options);
                            return m;
                        });
                    }
                    else {
                        $(e).select2(select2Options);
                        return e;
                    }
                }
            },
            cmSetLookup: function (Id, Text) {
                var t = this;
                if (Id == '' || Id == null) return;
                $(t).append($("<option/>", {
                    value: Id,
                    text: Text,
                    selected: true
                }));
                $(t).val(Id);
            },
            cmDataTable: function (options, initComplete) {
                var e = this;
                var dt_options = {
                    processing: false,
                    serverSide: true,
                    stateSave: false,
                    searchDelay: 500,
                    responsive: true,
                    lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
                    pageLength: 10,
                    destroy: true,
                    ajax: {
                        url: '',
                        type: "POST",
                        contentType: "application/json",
                        data: function (d) {
                            console.log(d);
                            return d;
                        },
                        success: function (data) {
                            console.log(data);
                            callback(data);
                            // Do whatever you want.
                        }
                    },
                    multipleSelection: true,
                    order: [],
                    //language: {
                    //    //aria: {
                    //    //    sortAscending: ": activate to sort column ascending",
                    //    //    sortDescending: ": activate to sort column descending"
                    //    //},
                    //    emptyTable: "No data available in table",
                    //    info: "Showing _START_ to _END_ of _TOTAL_ records",
                    //    infoEmpty: "No records found",
                    //    infoFiltered: "(filtered from _MAX_ total records)",
                    //    lengthMenu: "Show _MENU_",
                    //    search: "Search:",
                    //    zeroRecords: "No matching records found",
                    //    paginate: {
                    //        previous: '<i class="demo-psi-arrow-left"></i>',
                    //        next: '<i class="demo-psi-arrow-right"></i>'
                    //    }
                    //},
                    language: {
                        /* change the default text for 'next' and 'previous' with icons */
                        paginate: {
                            previous: "<i class='fal fa-chevron-left'></i>",
                            next: "<i class='fal fa-chevron-right'></i>"
                        },
                        processing: '<div class="d-flex align-items-center justify-content-center fs-lg"><div class="spinner-border spinner-border-sm text-primary mr-2" role="status"><span class="sr-only"> Loading...</span></div> Processing...</div>',
                        /* replace the default search lable text with a nice icon */
                        search: '<div class="input-group-text d-inline-flex width-3 align-items-center justify-content-center border-bottom-right-radius-0 border-top-right-radius-0 border-right-0"><i class="fal fa-search"></i></div>',
                        /* add search filter */
                        searchPlaceholder: "Search",
                        /* change text for zero records */
                        zeroRecords: "No records to display"
                    },
                    autoWidth: true,
                    initComplete: function (settings, json) {
                        initComplete(e, settings, json);
                    }
                }



                var options = $.extend(dt_options, options);
                console.log(JSON.stringify(options));
                return e.dataTable(options);

            },


            disableTab: function (tabTarget) {
                var e = this;

                e.find('a[data-toggle="tab"]').each(function (tabIndex, tabControl) {
                    //console.log(tabIndex)    
                    $.each(tabTarget, function (e, i) {
                        if (i == tabIndex) {
                            $(tabControl).addClass('hide');
                        }
                    })
                });
                return this;
            },
            enableTab: function (tabTarget) {
                var e = this;
                e.find('a[data-toggle="tab"]').each(function (tabIndex, tabControl) {
                    $.each(tabTarget, function (e, i) {
                        if (i == tabIndex) {
                            $(tabControl).removeClass('hide');
                        }
                    })
                });
                return this;
            },
            cmValidate: function (paramOptions) {
                var e = this;
                var error = $('.alert-danger', e);
                var success = $('.alert-success', e);

                var options = {
                    onsubmit: false,
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "",  // validate all fields including form hidden input
                    messages: {
                        select_multi: {
                            maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                            minlength: jQuery.validator.format("At least {0} items must be selected")
                        }
                    },
                    invalidHandler: function (event, validator) { //display error alert on form submit              
                        success.hide();
                        error.show();
                        App.scrollTo(error, -200);
                    },

                    errorPlacement: function (error, element) { // render error placement for each input type
                        var cont = $(element).parent('.input-group');
                        if (cont.size() > 0) {
                            cont.after(error);
                        } else {
                            element.after(error);
                        }
                    },

                    highlight: function (element) { // hightlight error inputs

                        $(element)
                            .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },

                    unhighlight: function (element) { // revert the change done by hightlight
                        $(element)
                            .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                            .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },

                    submitHandler: function (form) {
                        success.show();
                        error.hide();
                    }
                };
                options["rules"] = (paramOptions.rules == undefined ? {} : paramOptions.rules);
                e.validate(options);
                e.on('err.field.fv', function (e, data) {
                    // data.element --> The field element

                    var $tabPane = data.element.parents('.tab-pane'),
                        tabId = $tabPane.attr('id');

                    $('a[href="#' + tabId + '"][data-toggle="tab"]')
                        .parent()
                        .find('i')
                        .removeClass('fa-check')
                        .addClass('fa-times');
                })

                // Called when a field is valid
                e.on('success.field.fv', function (e, data) {
                    // data.fv      --> The FormValidation instance
                    // data.element --> The field element

                    var $tabPane = data.element.parents('.tab-pane'),
                        tabId = $tabPane.attr('id'),
                        $icon = $('a[href="#' + tabId + '"][data-toggle="tab"]')
                            .parent()
                            .find('i')
                            .removeClass('fa-check fa-times');

                    // Check if all fields in tab are valid
                    var isValidTab = data.fv.isValidContainer($tabPane);
                    if (isValidTab !== null) {
                        $icon.addClass(isValidTab ? 'fa-check' : 'fa-times');
                    }
                });
            },
            genericTable: function (url, options) {
                var e = this;
                //App.blockUI({
                //    boxed: true
                //});
                var pages;
                var startIndex;
                var endIndex;
                var draw = options.draw || 1;
                var GroupRow = [];
                console.log('draw position : ' + draw);
                startIndex = (draw - 1) * options.pageLength;

                for (var c = 0; c < options.columns.length; c++) {
                    var column = options.columns[c];
                    column.name = column.data;
                    column.search = {
                        value: "",
                        regex: false
                    }
                    column.searchable = column.searchable || true;
                    column.orderable = column.orderable || true;
                }
                var data = {
                    draw: draw,
                    start: startIndex,
                    length: options.pageLength,
                    pageLength: options.pageLength,
                    destroy: true,
                    processing: true,
                    serverSide: true,
                    columns: options.columns,
                    order: options.order,
                    search: {
                        value: "",
                        regex: false
                    }
                }

                $.post(url, data, function (result) {
                    if (result) {
                        endIndex = draw * options.pageLength > result.data.length ?
                            result.data.length - 1 : draw * options.pageLength - 1;

                        // Show pager row if there is more than one page
                        var pager = '';
                        pages = Math.floor(result.recordsTotal / options.pageLength);
                        if (pages < result.recordsTotal / options.pageLength) {
                            pages += 1;
                        }
                        e.find('.pagination-info').html('<span class="pagination-info"> ' + ((draw * options.pageLength) - options.pageLength + 1) + ' to ' + (draw * options.pageLength) + ' of ' + result.recordsTotal + '&nbsp;&nbsp;&nbsp;</span>');
                        $(e.selector + ' tr:gt(0)').remove();

                        for (var i = 0; i < result.data.length; i++) {
                            var d = result.data[i];
                            var row = '<tr>';
                            row += '<td class="inbox-small-cells">';
                            row += '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">';
                            row += '<input type="checkbox" class="mail-checkbox" value="1" />';
                            row += '<span></span>';
                            row += '</label>';
                            row += '</td>';


                            for (var c = 0; c < options.columns.length; c++) {
                                var column = options.columns[c];
                                if (typeof column.render == "function") {
                                    row += '<td>' + column.render(d[column.data], d) + '</td>';
                                } else {
                                    row += '<td class="view-message">' + d[column.data] + '</td>';
                                }

                            }
                            row += '</tr>';
                            $(e.selector).append(row);
                        }


                        //Generate Pager                        
                        if (pages > 1) {
                            e.find('.page-previous').unbind("click");
                            e.find('.page-previous').click(function (ee) {
                                ee.preventDefault();
                                if (draw < 1) return;
                                var pNum = (draw - 1);
                                options["draw"] = pNum;
                                $(e.selector).genericTable(url, options);

                            });

                            e.find('.page-next').unbind("click");
                            e.find('.page-next').click(function (ee) {
                                ee.preventDefault();
                                if (draw < 1 || draw >= pages) return;
                                var pNum = (draw + 1);
                                options["draw"] = pNum;
                                $(e.selector).genericTable(url, options);
                            });
                        }




                    }
                });

            }
        });
})(jQuery);