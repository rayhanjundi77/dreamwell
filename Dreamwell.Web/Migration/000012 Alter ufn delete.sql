USE [dreamwell_revise_2021]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_can_delete]    Script Date: 12/15/2021 6:52:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		MR.DOTNET
-- Create date: 2017-03-22
-- last change date: 2019-04-02
-- Description:	Get user access on record based on roles v.1.3.7
-- =============================================
alter FUNCTION [dbo].[ufn_can_delete]
(
	@application_user_id nvarchar(50), 
	@application_entity_id nvarchar(50),
	@record_id nvarchar(50),
	@owner_id nvarchar(50)
)
RETURNS bit
AS
BEGIN

	DECLARE @max_roles_count bigint;
	DECLARE @access_own_record bigint;
	DECLARE @access_local_unit bigint;
	DECLARE @access_parent_child bigint;
	DECLARE @access_organization bigint;	
	DECLARE @access_value bigint;
	DECLARE @access_value_team bigint;
	DECLARE @manager_id nvarchar(50);
	DECLARE @child_unit_id nvarchar(50);
	DECLARE @business_unit_id nvarchar(50);
	DECLARE @team_id nvarchar(50);

	DECLARE @result bit;
	SET @result = 0;

	-- Check if user is system administrator
	SELECT @result = COALESCE(is_sysadmin, 0) 
	FROM application_user
	WHERE id = @application_user_id;
	IF @result > 0 RETURN @result;

	-- User can delete own record
	IF @owner_id = @application_user_id SET @result = 1; 
	IF @result > 0 RETURN @result;

	-- Direct manager can delete record
	SELECT @manager_id = manager_id, @child_unit_id = business_unit_id 
	FROM dbo.application_user 
	WHERE id = @owner_id;

	IF @manager_id = @application_user_id SET @result = 1; 
	IF @result > 0 RETURN @result;

	SELECT @max_roles_count = dbo.ufn_get_max_roles_count();
	SET @access_own_record = 1;
	SET @access_local_unit = @access_own_record * @max_roles_count;
	SET @access_parent_child = @access_local_unit * @max_roles_count;
	SET @access_organization = @access_parent_child * @max_roles_count;

	-- Get individual role access value
	SELECT @access_value = SUM(COALESCE(r2.access_delete, 0)) 
	FROM dbo.user_role r
	INNER JOIN role_access r2 ON r2.application_role_id = r.application_role_id
		AND r2.application_entity_id = @application_entity_id
	WHERE r.application_user_id = @application_user_id		
		AND r.is_active = 1;

	-- Get team role access value
	SELECT @access_value_team = SUM(COALESCE(r2.access_delete, 0)) 
	FROM dbo.team_role r
	INNER JOIN role_access r2 ON r2.application_role_id = r.application_role_id
		AND r2.application_entity_id = @application_entity_id
	WHERE r.team_id IN ( 
		SELECT team_id FROM team_member WHERE application_user_id = @application_user_id
		UNION
		SELECT id FROM team WHERE team_leader = @application_user_id
	) AND r.is_active = 1;

	-- Select higher role access value
	IF @access_value_team > @access_value SET @access_value = @access_value_team;

	-- User with root / organization level can delete record
	IF @access_value >= @access_organization SET @result = 1;
	IF @result > 0 RETURN @result;

	-- Check owner team
	SELECT @team_id = r.team_id 
	FROM dbo.team_role r
	INNER JOIN role_access r2 ON r2.application_role_id = r.application_role_id
		AND r2.application_entity_id = @application_entity_id
	WHERE r.team_id IN ( 
		SELECT team_id FROM team_member WHERE application_user_id = @application_user_id
		UNION
		SELECT id FROM team WHERE team_leader = @application_user_id
	) AND r.is_active = 1;

	IF @owner_id = @team_id
	BEGIN
		SET @result = 1;
		IF @result > 0 RETURN @result;
	END;

	-- Check parent-child access	
	SELECT @business_unit_id = business_unit_id 
	FROM dbo.application_user 
	WHERE id = @application_user_id;

	DECLARE @is_child_unit int;
	SET @is_child_unit = 0;
	IF @access_value >= @access_parent_child
	BEGIN		
		-- Check if user's business unit is parent unit of owner's
		WITH unit_child AS
		(
			SELECT id
			FROM dbo.business_unit WHERE parent_unit = @business_unit_id
			UNION ALL
			SELECT b.id FROM dbo.business_unit b 
			INNER JOIN unit_child ON b.parent_unit = unit_child.id
		)
		SELECT @is_child_unit = COUNT(*) 
		FROM unit_child
		WHERE unit_child.id = @child_unit_id
		OPTION(MAXRECURSION 32767);

		IF @is_child_unit > 0 
		BEGIN
			SET @result = 1;
			IF @result > 0 RETURN @result;
		END;
	END;

	-- Check if user and record owner in same business unit
	IF @access_value >= @access_local_unit AND @business_unit_id = @child_unit_id
	BEGIN
		SET @result = 1;
		IF @result > 0 RETURN @result;
	END;

	RETURN 1;
END

GO


