alter VIEW [dbo].[vw_application_user]
AS
SELECT
  r.id,
  r.created_by,
  r.created_on,
  r.modified_by,
  r.modified_on,
  r.approved_by,
  r.approved_on,
  r.is_active,
  r.is_locked,
  r.is_default,
  r.owner_id,
  r.app_username,
  r.first_name,
  r.last_name,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(r.last_name), N''), ' ', ISNULL(r.first_name, N'')))) AS app_fullname,
  r.manager_id,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u4.last_name), N''), ' ', ISNULL(u4.first_name, N'')))) AS manager_name,
  r.is_sysadmin,
  r.email,
  r.phone,
  r.is_ldap,
  r.gender,
  r.organization_id,
  o1.organization_name,
  r.business_unit_id,
  b.unit_name,
  r.primary_team_id,
  t1.team_name,
  t1.team_leader,
  fl.filepath,

  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,
  t.team_name  AS record_owning_team
FROM dbo.application_user AS r
LEFT OUTER JOIN dbo.organization AS o1
  ON r.organization_id = o1.id
LEFT OUTER JOIN dbo.business_unit AS b
  ON r.business_unit_id = b.id
LEFT OUTER JOIN dbo.team AS t1
  ON r.primary_team_id = t1.id

LEFT OUTER JOIN dbo.application_user AS u4
  ON r.manager_id = u4.id
LEFT OUTER JOIN dbo.application_user AS u0
  ON r.created_by = u0.id
LEFT OUTER JOIN dbo.application_user AS u1
  ON r.modified_by = u1.id
LEFT OUTER JOIN dbo.application_user AS u2
  ON r.approved_by = u2.id
LEFT OUTER JOIN dbo.application_user AS u3
  ON r.owner_id = u3.id
LEFT OUTER JOIN dbo.team AS t
  ON r.owner_id = t.id
  LEFT OUTER JOIN dbo.filemaster AS fl
  ON r.id = fl.record_id

GO


