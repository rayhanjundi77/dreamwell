
ALTER VIEW [dbo].[vw_drilling]
AS

SELECT
r.id,
r.created_by,
r.created_on,
r.modified_by,
r.modified_on,
r.approved_by,
r.approved_on,
r.submitted_by,
r.submitted_on,
r.is_active,
r.is_locked,
r.is_default,
r.owner_id,
r.organization_id,
o1.organization_name AS organization_name,
c.country_name,
r.well_id,
w.well_name,
w.well_classification,
w.well_type,
w.latitude,
w.longitude,
w.spud_date,
w.target_direction,
f.id AS field_id,
f.field_name AS field_name,
r.drilling_date,
r.event,
dc.contractor_name,
r.report_no,
w.rig_rating,
r.rig_rating_uom,
w.rig_type,
r.water_depth,
r.water_depth_uom,
w.rkb_elevation,
r.rkb_elevation_uom,
w.rt_to_seabed,
r.rt_to_seabed_uom,
w.rig_heading,
r.release_date,
a.id as afe_id,
rg.name as rig_name,
a.afe_no,
w.afe_cost,
r.afe_cost_currency_id,
daily.daily_cost,
r.daily_mud_cost,
r.daily_mud_cost_currency_id,
r.cummulative_cost,
r.cummulative_cost_currency_id,
r.cummulative_mud_cost,
r.cummulative_mud_cost_currency_id,
w.planned_td,
r.planned_td_uom,
w.planned_days,
r.planned_days_uom,
r.dol,
r.dol_uom,
r.dfs,
r.dfs_uom,
--r.current_hole_size,
--r.current_hole_size_uom,
COALESCE(d.previous_depth_md, 0) AS previous_depth_md,
r.previous_depth_md_uom,
COALESCE(d.previous_depth_tvd, 0) AS previous_depth_tvd,
r.previous_depth_tvd_uom,
COALESCE(r.current_depth_md, 0) AS current_depth_md,
r.current_depth_md_uom,
COALESCE(r.current_depth_tvd, 0) AS current_depth_tvd,
r.current_depth_tvd_uom,
r.hsse_incident_acident,
r.hsse_environtmental_spills,
r.hsse_safety_alert_received,
r.hsse_proactive_safety,
r.hsse_near_miss_report,
r.hsse_exercise,
r.hsse_social_issues,
r.hsse_safety_meeting,
r.hsse_stop_cards,
r.hsse_tofs,
r.hsse_jsa,
r.hsse_inductions,
r.hsse_audits,
r.hsse_days_since_lti,
r.hsse_tbop_press,
r.hsse_tbop_func,
r.hsse_dkick,
r.hsse_dstrip,
r.hsse_dfire,
r.hsse_dmis_pers,
r.hsse_aband_rig,
r.hsse_dh2s,
r.hsse_description,
r.hsse_incident_description,
r.hsse_type,
r.hsse_lta,
r.hsse_h2stest,
r.hsse_mtg,
r.hsse_kick_trip,
r.hsse_kick_drill,

r.mudlogging_wob_min,
r.mudlogging_wob_max,
r.mudlogging_wob_uom,
r.mudlogging_rpm_min,
r.mudlogging_rpm_max,
r.mudlogging_rpm_uom,
r.mudlogging_dhrpm_min,
r.mudlogging_dhrpm_max,
r.mudlogging_dhrpm_uom,
r.mudlogging_torque_min,
r.mudlogging_torque_max,
r.mudlogging_torque_uom,
r.mudlogging_flowrate_min,
r.mudlogging_flowrate_max,
r.mudlogging_flowrate_uom,
r.mudlogging_spp_min,
r.mudlogging_spp_max,
r.mudlogging_spp_uom,
r.mudlogging_spm_min,
r.mudlogging_spm_max,
r.mudlogging_spm_uom,
r.mudlogging_spmpress_min,
r.mudlogging_spmpress_max,
r.mudlogging_spmpress_uom,
r.mudlogging_avg_rop_min,
r.mudlogging_avg_rop_max,
r.mudlogging_avg_rop_uom,

r.weather_general,
r.weather_wind_speed,
r.weather_wind_direction,
r.weather_temperature,
r.weather_temperature_low,
r.weather_visibility,
r.weather_cloud,
r.weather_barometer,
r.weather_wave,
r.weather_wave_period,
r.weather_wave_direction,
r.weather_height,
r.weather_current_speed,
r.weather_pitch,
r.weather_roll,
r.weather_heave,
r.weather_comments,
r.weather_road_condition,
r.weather_chill_factor,
cmcc.cummulative_cost_count,

r.operation_data_period,
r.operation_data_early,
r.operation_data_planned,
aps.approval_level,
aps.approval_rejected_level,
aps.approval_status,

  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u4.last_name), N''), ' ', ISNULL(u4.first_name, N''))))  AS record_submitted_by,
  t.team_name  AS record_owning_team
FROM dbo.drilling AS r
OUTER APPLY (
  SELECT TOP 1 COALESCE(d.current_depth_tvd, 0) AS previous_depth_tvd, COALESCE(d.current_depth_md, 0) AS previous_depth_md 
  FROM drilling d 
  WHERE d.well_id = r.well_id AND d.drilling_date < r.drilling_date
  ORDER BY d.drilling_date DESC
  ) AS d
OUTER APPLY (SELECT daily_cost FROM [dbo].[udf_get_daily_cost](r.id)) AS daily
OUTER APPLY (SELECT cummulative_cost_count FROM [dbo].[udf_get_cummulative_cost_count](r.well_id,r.drilling_date)) AS cmcc
OUTER APPLY (SELECT * FROM [dbo].[udf_get_approval_status](r.id)) AS aps
LEFT OUTER JOIN dbo.well AS w
  ON r.well_id = w.id
LEFT OUTER JOIN dbo.field AS f
  ON w.field_id = f.id
LEFT OUTER JOIN dbo.country AS c
  ON w.country_id = c.id
LEFT OUTER JOIN dbo.drilling_contractor AS dc
  ON w.drilling_contractor_id = dc.id
LEFT OUTER JOIN dbo.afe AS a
  ON a.well_id = w.id
LEFT OUTER JOIN dbo.rig AS rg
  ON w.rig_id = rg.id
LEFT OUTER JOIN dbo.organization AS o1
  ON r.organization_id = o1.id
LEFT OUTER JOIN dbo.application_user AS u0
  ON r.created_by = u0.id
LEFT OUTER JOIN dbo.application_user AS u1
  ON r.modified_by = u1.id
LEFT OUTER JOIN dbo.application_user AS u2
  ON r.approved_by = u2.id
LEFT OUTER JOIN dbo.application_user AS u3
  ON r.owner_id = u3.id
LEFT OUTER JOIN dbo.application_user AS u4
  ON r.submitted_by = u4.id
LEFT OUTER JOIN dbo.team AS t
  ON r.owner_id = t.id

GO


