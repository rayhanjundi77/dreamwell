USE [dreamwell_revise_2021]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_get_cummulative_cost]    Script Date: 11/24/2021 10:17:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [dbo].[udf_get_usage_by_afe]('c7d2f894-0b31-479a-9da1-ad724863c8c7', 'fea214bc-55c9-4b63-9185-170b9eeca001')
CREATE FUNCTION [dbo].[udf_get_cummulative_cost_count]
(
	@well_id nvarchar(50),
	@drilling_date datetime

)
RETURNS @T table( 
	cummulative_cost_count float
)
	
AS
BEGIN
	DECLARE 
		@cummulative_cost_count float;

		SELECT  @cummulative_cost_count = COALESCE(SUM(cd.actual_price / cd.unit * r.unit), 0)
                                                FROM dbo.daily_cost AS r
                                                LEFT OUTER JOIN dbo.afe_contract AS a
                                                  ON r.afe_contract_id = a.id
                                                LEFT OUTER JOIN dbo.drilling AS d
                                                  ON r.drilling_id = d.id
                                                LEFT OUTER JOIN dbo.contract_detail AS cd
                                                  ON a.contract_detail_id = cd.id
                                                WHERE d.well_id = @well_id AND d.drilling_date <= @drilling_date

	INSERT INTO @T(cummulative_cost_count) VALUES (COALESCE(@cummulative_cost_count, 0));

	RETURN 
END


