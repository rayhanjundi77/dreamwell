
alter VIEW [dbo].[vw_drilling_hazard]
AS
SELECT
  r.id,
  r.idx,
  r.created_by,
  r.created_on,
  r.modified_by,
  r.modified_on,
  r.approved_by,
  r.approved_on,
  r.is_active,
  r.is_locked,
  r.is_default,
  r.owner_id,
  r.organization_id,
  o1.organization_name AS parent_organization_name,
  r.well_id,
  r.depth,
  r.description,

  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,
  t.team_name  AS record_owning_team
FROM dbo.drilling_hazard AS r

LEFT OUTER JOIN dbo.organization AS o1
  ON r.organization_id = o1.id
LEFT OUTER JOIN dbo.application_user AS u0
  ON r.created_by = u0.id
LEFT OUTER JOIN dbo.application_user AS u1
  ON r.modified_by = u1.id
LEFT OUTER JOIN dbo.application_user AS u2
  ON r.approved_by = u2.id
LEFT OUTER JOIN dbo.application_user AS u3
  ON r.owner_id = u3.id
LEFT OUTER JOIN dbo.team AS t
  ON r.owner_id = t.id

GO


