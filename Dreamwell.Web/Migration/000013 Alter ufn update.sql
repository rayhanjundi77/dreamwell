USE [dreamwell_revise_2021]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_can_update]    Script Date: 12/15/2021 6:53:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		MR.DOTNET
-- Create date: 2017-03-22
-- last change date: 2019-04-02
-- Description:	Get user access on record based on roles v.1.3.7
-- =============================================
alter FUNCTION [dbo].[ufn_can_update]
(
	@application_user_id nvarchar(50), 
	@application_entity_id nvarchar(50),
	@record_id nvarchar(50),
	@owner_id nvarchar(50)
)
RETURNS bit
AS
BEGIN

	DECLARE @max_roles_count bigint;
	DECLARE @access_own_record bigint;
	DECLARE @access_local_unit bigint;
	DECLARE @access_parent_child bigint;
	DECLARE @access_organization bigint;	
	DECLARE @access_value bigint;
	DECLARE @access_value_team bigint;
	DECLARE @manager_id nvarchar(50);
	DECLARE @child_unit_id nvarchar(50);
	DECLARE @business_unit_id nvarchar(50);
	DECLARE @team_id nvarchar(50);

	DECLARE @result bit;
	SET @result = 0;

	-- Check if user is system administrator
	SELECT @result = COALESCE(is_sysadmin, 0) 
	FROM application_user
	WHERE id = @application_user_id;
	IF @result > 0 RETURN @result;

	-- User can update own record
	IF @owner_id = @application_user_id SET @result = 1; 
	IF @result > 0 RETURN @result;

	-- User can update own Team record 
	--IF EXISTS (SELECT * FROM dbo.team_member WHERE team_id = @owner_id AND application_user_id=@application_user_id) SET @result = 1;
	--IF @result > 0 RETURN @result;


	-- Direct manager can update record
	SELECT @manager_id = manager_id, @child_unit_id = business_unit_id 
	FROM dbo.application_user 
	WHERE id = @owner_id;

	IF @manager_id = @application_user_id SET @result = 1; 
	IF @result > 0 RETURN @result;

	SELECT @max_roles_count = dbo.ufn_get_max_roles_count();
	SET @access_own_record = 1;
	SET @access_local_unit = @access_own_record * @max_roles_count;
	SET @access_parent_child = @access_local_unit * @max_roles_count;
	SET @access_organization = @access_parent_child * @max_roles_count;

	-- Get individual role access value
	SELECT @access_value = SUM(COALESCE(r2.access_update, 0)) 
	FROM dbo.user_role r
	INNER JOIN role_access r2 ON r2.application_role_id = r.application_role_id
		AND r2.application_entity_id = @application_entity_id
	WHERE r.application_user_id = @application_user_id
		AND r.is_active = 1;

	-- Get team role access value
	SELECT @access_value_team = SUM(COALESCE(r2.access_update, 0)) 
	FROM dbo.team_role r
	INNER JOIN role_access r2 ON r2.application_role_id = r.application_role_id
		AND r2.application_entity_id = @application_entity_id
	WHERE r.team_id IN ( 
		SELECT team_id FROM team_member WHERE application_user_id = @application_user_id
		UNION
		SELECT id FROM team WHERE team_leader = @application_user_id
	) AND r.is_active = 1;

		-- Select higher role access value
	IF @access_value_team > @access_value 
	SET @access_value = @access_value_team;

	-- Compare record ownership (own record)
	IF COALESCE(@access_value, 0) < COALESCE(@access_local_unit,0)	BEGIN
		-- User can update own record
		IF @owner_id = @application_user_id SET @result = 1; 
		IF COALESCE(@result, 0) > 0 RETURN @result;
	END;


	-- Compare record ownership (own Team record)
	IF COALESCE(@access_value_team, 0) > 0 AND 
		COALESCE(@access_value_team, 0) < COALESCE(@access_local_unit,0)	
	BEGIN
		SELECT @result = COALESCE(COUNT(t.id), 0) FROM team t
		INNER JOIN team_member tm ON tm.team_id = t.id
		WHERE t.id = @owner_id
		AND ( tm.application_user_id = @application_user_id 
		OR t.team_leader =  @application_user_id );
		
		IF COALESCE(@result, 0) > 0 RETURN @result;
	END;


	-- Compare record ownership (local unit)
	IF COALESCE(@access_value, 0) >=  COALESCE(@access_local_unit,0)
		AND COALESCE(@access_value, 0) < @access_parent_child
	BEGIN
		-- User can read own team record, including default team of business unit
		SELECT @result = COALESCE(COUNT(t.id), 0) FROM team t
		INNER JOIN team_member tm ON tm.team_id = t.id
		WHERE t.id = @owner_id
		AND ( tm.application_user_id = @application_user_id 
			OR t.team_leader =  @application_user_id );

		IF COALESCE(@result, 0) > 0 RETURN @result;
	END;

-- Compare record ownership (parent-child business unit)
	-- Modified by : Dono
	-- Modified on : 2020-03-28
	IF COALESCE(@access_value, 0) >= @access_parent_child
		AND COALESCE(@access_value, 0) < @access_organization
	BEGIN
		DECLARE @owning_team_id nvarchar(50);
		DECLARE @owning_business_unit nvarchar(50);
		DECLARE @user_team_id nvarchar(50);
		DECLARE @user_business_unit nvarchar(50);

		SELECT @owning_team_id = id FROM team WHERE id = @owner_id;
		IF @owning_team_id IS NULL
		BEGIN
			SELECT @owning_team_id = bu.default_team 
			FROM business_unit bu
			INNER JOIN application_user au ON au.business_unit_id = bu.id
			WHERE au.id = @owner_id;
		END;
		
		SELECT @owning_business_unit = bu.id 
		FROM business_unit bu
		WHERE bu.default_team = @owning_team_id;
				
		SELECT @user_team_id = bu.default_team,
			@user_business_unit = bu.id
		FROM business_unit bu
		INNER JOIN application_user au ON au.business_unit_id = bu.id
		WHERE au.id = @application_user_id;

		IF @owning_business_unit IS NOT NULL AND @user_business_unit IS NOT NULL
		BEGIN			
			IF @owning_business_unit = @user_business_unit 
			BEGIN
				SET @result = 1;
				RETURN @result;
			END;

			DECLARE @parent_business_unit nvarchar(50);
			SELECT @parent_business_unit = bu.parent_unit
			FROM business_unit bu
			WHERE bu.id = @owning_business_unit;

			WHILE(@parent_business_unit IS NOT NULL )
			BEGIN
				SET @owning_business_unit = @parent_business_unit;

				IF @user_business_unit = @owning_business_unit
				BEGIN
					SET @result = 1;
					RETURN @result;
				END;

				SELECT @parent_business_unit = bu.parent_unit
				FROM business_unit bu
				WHERE bu.id = @owning_business_unit;
			END;
		END;

	END;

	

	-- User with root / organization level can read record
	IF @access_value >= @access_organization SET @result = 1;
	IF COALESCE(@result, 0) > 0 RETURN @result;

	RETURN 1;
END

GO


