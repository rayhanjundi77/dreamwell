USE [dreamwell_revise_2021]
GO

/****** Object:  UserDefinedFunction [dbo].[udf_get_usage_by_contract]    Script Date: 11/16/2021 7:06:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--SELECT * FROM [dbo].[udf_get_usage_by_contract]('a094cde6-7310-4028-8a23-aa41679c1386', 'fea214bc-55c9-4b63-9185-170b9eeca001')
alter FUNCTION [dbo].[udf_get_usage_by_contract]
(
	@afe_contract_id nvarchar(50),
	@material_id nvarchar(50)
)
RETURNS @T table( 
	total_unit float,
	total_price float
)
	
AS
BEGIN
	DECLARE 
		@total_unit float,
		@total_price float;

	SELECT @total_unit = SUM(r.unit), @total_price = SUM((COALESCE(cd.unit_price,0) * cd.current_rate_value) * COALESCE(r.unit,0))
	FROM daily_cost r
	INNER JOIN afe_contract ac ON ac.id = r.afe_contract_id
	INNER JOIN contract_detail cd ON cd.id = ac.contract_detail_id
	--WHERE r.afe_id = @afe_id
	WHERE ac.id = @afe_contract_id AND cd.material_id = @material_id 

	INSERT INTO @T(total_unit, total_price) VALUES (COALESCE(@total_unit, 0), COALESCE(@total_price, 0));

	RETURN 
END



GO


