USE [dreamwell_revise_2021]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_can_create]    Script Date: 12/15/2021 6:51:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		MR.DOTNET
-- Create date: 2017-03-22
-- last change date: 2019-04-02
-- Description:	Get user access on record based on roles v.1.3.7
-- =============================================
alter FUNCTION [dbo].[ufn_can_create]
(
	@application_user_id nvarchar(50), 
	@application_entity_id nvarchar(50)
)
RETURNS bit
AS
BEGIN
	/*
	DECLARE @max_roles_count bigint;
	DECLARE @access_own_record bigint;
	DECLARE @access_local_unit bigint;
	DECLARE @access_parent_child bigint;
	DECLARE @access_organization bigint;	
	*/

	DECLARE @access_value bigint;
	DECLARE @access_value_team bigint;
	DECLARE @result bit;

	SET @result = 0;

	/*
	SELECT @max_roles_count = dbo.ufn_get_max_roles_count();
	SET @access_own_record = 1;
	SET @access_local_unit = @access_own_record * @max_roles_count;
	SET @access_parent_child = @access_local_unit * @max_roles_count;
	SET @access_organization = @access_parent_child * @max_roles_count;
	*/

	-- Check if user is system administrator
	SELECT @result = COALESCE(is_sysadmin, 0) 
	FROM application_user
	WHERE id = @application_user_id;
	IF @result > 0 RETURN @result;

	-- Get individual role access value
	SELECT @access_value = COALESCE(SUM(r2.access_create), 0) 
	FROM dbo.user_role r
	INNER JOIN dbo.role_access r2 ON r2.application_role_id = r.application_role_id
		AND r2.application_entity_id = @application_entity_id
	WHERE r.application_user_id = @application_user_id
		AND r.is_active = 1;
	
	-- Get team role access value
	SELECT @access_value_team = COALESCE(SUM(r2.access_create), 0) 
	FROM dbo.team_role r
	INNER JOIN role_access r2 ON r2.application_role_id = r.application_role_id
		AND r2.application_entity_id = @application_entity_id
	WHERE r.team_id IN ( 
		SELECT team_id FROM team_member WHERE application_user_id = @application_user_id
		UNION
		SELECT id FROM team WHERE team_leader = @application_user_id
	) AND r.is_active = 1;

	-- Select higher role access value
	IF @access_value_team > @access_value SET @access_value = @access_value_team;
	IF @access_value > 0 SET @result = 1;

	RETURN 1;
END



GO


