ALTER TABLE contract_detail ALTER COLUMN current_rate_value float;
ALTER TABLE contract_detail ALTER COLUMN unit float;
ALTER TABLE contract_detail ALTER COLUMN unit_price float;
ALTER TABLE contract_detail ALTER COLUMN total_price float;
ALTER TABLE contract_detail ALTER COLUMN actual_price float;
ALTER TABLE contract_detail ALTER COLUMN remaining_unit float;
ALTER TABLE daily_cost ALTER COLUMN unit float;
ALTER TABLE afe_contract ALTER COLUMN unit float;