USE [dreamwell_revise_2021]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_get_daily_cost]    Script Date: 11/16/2021 6:42:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [dbo].[udf_get_usage_by_afe]('c7d2f894-0b31-479a-9da1-ad724863c8c7', 'fea214bc-55c9-4b63-9185-170b9eeca001')
ALTER FUNCTION [dbo].[udf_get_daily_cost]
(
	@drilling_id nvarchar(50)
)
RETURNS @T table( 
	daily_cost float
)
	
AS
BEGIN
	DECLARE 
		@daily_cost float;

		SELECT @daily_cost = COALESCE(SUM(cd.actual_price / cd.unit * r.unit), 0)
		FROM dbo.daily_cost AS r
		LEFT OUTER JOIN dbo.afe_contract AS a
			ON r.afe_contract_id = a.id
		LEFT OUTER JOIN dbo.contract_detail AS cd
			ON a.contract_detail_id = cd.id
		WHERE r.drilling_id = @drilling_id

	INSERT INTO @T(daily_cost) VALUES (COALESCE(@daily_cost, 0));

	RETURN 
END


