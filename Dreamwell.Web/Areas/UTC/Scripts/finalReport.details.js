﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $('form[id=welldetails]');
    var $formDetail = $('#frm-well_final_report');

    var pageFunction = function () {
        console.log($recordId.val());
        if ($recordId.val() != "") {
            $.get($.helper.resolveApi('~/core/WellFinalReport/' + $recordId.val() + '/detail'), function (r) {
                console.log(r);
                if (r.status.success) {
                    loadWellDetail(r.data.well_id);
                    getfileLampiran();
                    $.helper.form.fill($formDetail, r.data);
                    initSummerNote(r.data);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var initSummerNote = function (data) {
            $('#summary-bab-iii').summernote({
                placeholder: 'Type your summary of BAB III',
                tabsize: 2,
                height: 200,
                callbacks: {
                    onImageUpload: function (files) {
                        uploadFile(files[0], $(this));
                    }
                },

            }).summernote('code', data.bab_3);

            $('#summary-bab-iv').summernote({
                placeholder: 'Type your summary of BAB IV',
                tabsize: 2,
                height: 200,
                callbacks: {
                    onImageUpload: function (files) {
                        uploadFile(files[0], $(this));
                    }
                }
            }).summernote('code', data.bab_4);;

            function uploadFile(file, obj) {
                console.log(obj);
                var formData = new FormData();
                //var file = $('#files')[0];
                //console.log(file.files[0]);
                formData.append('file', file);

                $.ajax({
                    url: $.helper.resolveApi("~/core/wellfinalreport/upload?id=" + $recordId.val()),
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (r) {
                        console.log(r);
                        if (r.status.success) {
                            toastr.success("Success to upload");
                            //var html = `<img src="` + $.helper.resolveApi("~/Core/GetFile/MinIO/GetImage?d=" + r.data) + `" class="mr-3" style="width:50px">`;
                            var image = $('<img>').attr('src', $.helper.resolveApi("~/Core/GetFile/MinIO/GetImage?d=" + r.data));
                            obj.summernote("insertNode", image[0]);
                            //$("#imagee").html(html);
                            //--well_final_report/{wellId}/{filename}{}
                        }
                    },
                    error: function () {
                        toastr.error("Failed to upload");
                    }
                });
                return;
            }
        }


        function delay(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

        async function uploadLampiran() {
            var recordId = $('#recordId').val();

            var files = [
                { file: $('#lampiranA')[0].files[0], type: 'a' },
                { file: $('#lampiranB')[0].files[0], type: 'b' },
                { file: $('#lampiranC')[0].files[0], type: 'c' },
                { file: $('#lampiranD')[0].files[0], type: 'd' }
            ];

            for (const item of files) {
                if (item.file) {
                    var formData = new FormData();
                    formData.append('postedFile', item.file);
                    formData.append('recordId', recordId);
                    formData.append('columnTarget', 'filemaster_id');


                    try {
                        await $.ajax({
                            url: $.helper.resolveApi("~/Core/wellfinalreport/uploadLampiran?recordId=" + recordId), // Gunakan recordId dalam URL
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (r) {
                                toastr.success("File uploaded successfully");

                                /*if (r.success) {
                                    console.log("File uploaded successfully");
                                } else {
                                    console.log(r)
                                    toastr.error(r.message);
                                }*/
                            },
                            error: function (xhr, status, error) {
                                console.log("Upload failed: ", xhr.responseText);
                                toastr.error("Error: " + xhr.responseText);
                            }
                        });

                        // Jeda waktu 1 detik (1000 ms) antara setiap upload
                        await delay(100);
                    } catch (error) {
                        console.error("Error uploading file:", error);
                    }
                }
            }
        }

        var getfileLampiran = function () {
            var recordId = $('#recordId').val();
            $.get($.helper.resolveApi('~/core/filemaster/' + recordId + '/detail'), function (r) {
                console.log('adasaa', r);
                $('#isiLampiranA').val(r.data[0].name);
                $('#isiLampiranB').val(r.data[1].name);
                $('#isiLampiranC').val(r.data[2].name);
                $('#isiLampiranD').val(r.data[3].name);
            }).fail(function (r) {
                console.log(r);
            });
        }
        var loadWellDetail = function (well_id) {
            $.get($.helper.resolveApi('~/core/Well/' + well_id + '/detail'), function (r) {
                console.log("Well Details");
                console.log(r);
                if (r.status.success) {
                    $.helper.form.fill($form, r.data);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                console.log(r);
            });
        };  

        // window.arrwellcheckbox = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18"];
        window.arrwellcheckbox = ["0"];
        var setArrCheckBox = function() {
            window.arrwellcheckbox = ["0"];
            $("input:checkbox[name='welltype[]']:checked").each(function(){
                window.arrwellcheckbox.push($(this).val());
            });

            console.log("window.arrwellcheckbox----------------------");
            console.log(window.arrwellcheckbox);
        };      

        $('.checkAll').change(function() {
            window.arrwellcheckbox = ["0"];
            if(this.checked) {
                $(".wellcheckbox").prop("checked",true);
            }else{
                $(".wellcheckbox").prop("checked",false);
            } 
            setArrCheckBox();      
        });

        $('.wellcheckbox').change(function() {            
            setArrCheckBox();      
        });

            $(".btnDownloadTest").click(function () {
                var url = $(this).attr("data-url");
                window.open(url);
            });

        $(".btnDownload").click(function(argument) {
            var url = $(this).attr("data-url");
            var param = "";
            for (var i = 0; i < window.arrwellcheckbox.length; i++) {
                var ck = window.arrwellcheckbox[i];
                param = param + "&section="+ck;
            }
            //console.log("ASIAP")
            window.open(url + param);

        });

        $('#btn-saveLampiran').click(function (event) {
            event.preventDefault();
            //var btnLampiran = $(this);
            //btnLampiran.button('loading');
            uploadLampiran()
            //console.log("ajeeeeeegg")
        });
        $('#btn-save').click(function (event) {
            var data = $formDetail.serializeToJSON();
            var btn = $(this);
            var isvalidate = $formDetail[0].checkValidity();

            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');

                var babIII = $('#summary-bab-iii').summernote('code');
                data.bab_3 = babIII;
                var babIV = $('#summary-bab-iv').summernote('code');
                data.bab_4 = babIV;

                console.log(data);
                //uploadLampiran()
                $.post($.helper.resolveApi('~/core/WellFinalReport/save'), data, function (r) {
                    console.log(r);
                    if (r.status.success) {
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
                toastr.error("Form is not valid, please check the inputs.");
            }
            $formDetail.addClass('was-validated');
        });

        return {
            init: function () {
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
});

