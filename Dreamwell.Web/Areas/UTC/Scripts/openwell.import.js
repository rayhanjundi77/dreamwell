﻿$(document).ready(function () {
    'use strict';


    var isFoundOpenWell = false;
    var well_id;
    var getoldDDR;
    $('#inputOpenWell').on("change", function () {
        getXMLOpenWell() 
    });

    $('#inputOpenWellbore').on("change", function () {
        getXMLOpenWellbore()
    });


    $('#inputOpenWellSurvey').on("change", function () {
        getXMLSurveyOpenWell()
    });


    $('#inputOpenWellFluids').on("change", function () {
        getXMLFluidsOpenWell()
    });

    $('#inputopenwellDDR').on("change", function () {
        getXMLDDROpenWell()
    });

    function getXMLOpenWell() {
        var wellll = $("#inputOpenWell").val();

        console.log("well " + wellll);
        if (wellll == '') {
            xmlDocOpenWell = null;
            isFoundOpenWell = false;
            getOldDDROpenWell = null;
            var dvTable = $("#dvTableOpenWell");
            dvTable.children().remove();
            toastr.error("Please select well file");
        }
        else {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xml)$/;

            if (regex.test($("#inputOpenWell").val().toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var xmlLocal = $.parseXML(e.target.result);
                        xmlDocOpenWell = e.target.result;
                        var customers = $(xmlLocal).find("well");

                        console.log("data " + customers.length);
                        var dvTable = $("#dvTableOpenWell");
                        dvTable.children().remove();
                        if (customers.length == null || customers.length == 0) {
                            $("#inputOpenWell").val('');
                            isFoundOpenWell = false;
                            getOldDDROpenWell = null;
                            toastr.error("Tag name Well  is not found");
                            xmlDocOpenWell = null;
                            return;
                        }
                        //Create a HTML Table element.
                        var table = $("<table class='table table - bordered table - hover table - striped w - 100' />");
                        table[0].border = "1";


                    //    console.log("find tag value well name " + $(customers[0]).find('nameWell').text());
                    //    $("#well_name").val($(customers[0]).find('nameWell').text());
                    //    $("#afe_number").val($(customers[0]).find('numAFE:first').text());
                    //    getWellByName($(customers[0]).find('nameWell').text(), customers);
                        $("#well_name_openwell").val($(customers[0]).find('nameWell').text());
                        getWellByName($(customers[0]).find('nameWell').text(), customers);

                        //Add the header row.
                        var row = $(table[0].insertRow(-1));
                        customers.eq(0).children().each(function () {
                            var headerCell = $("<th/>");
                            headerCell.html(this.nodeName);
                            row.append(headerCell);
                        });

                        //Add the data rows.
                        $(customers).each(function () {
                            row = $(table[0].insertRow(-1));
                            $(this).children().each(function () {
                                var cell = $("<td />");
                                cell.html($(this).text());
                                row.append(cell);
                            });
                        });


                        dvTable.html("");
                        dvTable.append(table);
                        $('#loadingModal').modal('hide');
                    }
                    reader.readAsText($("#inputOpenWell")[0].files[0]);
                } else {
                   
                    toastr.error("This browser does not support HTML5.");
                }
            } else {
              

                toastr.error("Please upload a valid WITSML file.");
            }

        }



    }


    function getXMLOpenWellbore() {
        var wellll = $("#inputOpenWellbore").val();

        console.log("wellbore file " + wellll);
        if (wellll == '') {
            xmlDocOpenWellbore = null;
            isFoundOpenWell = false;
            var dvTable = $("#dvTableOpenWellBore");
            dvTable.children().remove();
            toastr.error("Please select wellbore file");
        }
        else {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xml)$/;

            if (regex.test($("#inputOpenWellbore").val().toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var xmlLocal = $.parseXML(e.target.result);
                        xmlDocOpenWellbore = e.target.result;
                        var customers = $(xmlLocal).find("wellbore");

                        console.log("data " + customers.length);
                        var dvTable = $("#dvTableOpenWellBore");
                        dvTable.children().remove();
                        if (customers.length == null || customers.length == 0) {
                            $("#inputOpenWellbore").val('');
                            isFoundOpenWell = false;
                            xmlDocOpenWellbore = null;
                            toastr.error("Tag name Wellbore  is not found");

                            return;
                        }
                        //Create a HTML Table element.
                        var table = $("<table class='table table - bordered table - hover table - striped w - 100' />");
                        table[0].border = "1";


                        //    console.log("find tag value well name " + $(customers[0]).find('nameWell').text());
                        //    $("#well_name").val($(customers[0]).find('nameWell').text());
                        //    $("#afe_number").val($(customers[0]).find('numAFE:first').text());
                        //    getWellByName($(customers[0]).find('nameWell').text(), customers);
                        getWellByName($(customers[0]).find('nameWell').text(), customers);
                        //Add the header row.
                        var row = $(table[0].insertRow(-1));
                        customers.eq(0).children().each(function () {
                            var headerCell = $("<th/>");
                            headerCell.html(this.nodeName);
                            row.append(headerCell);
                        });

                        //Add the data rows.
                        $(customers).each(function () {
                            row = $(table[0].insertRow(-1));
                            $(this).children().each(function () {
                                var cell = $("<td />");
                                cell.html($(this).text());
                                row.append(cell);
                            });
                        });


                        dvTable.html("");
                        dvTable.append(table);
                        $('#loadingModal').modal('hide');
                    }
                    reader.readAsText($("#inputOpenWellbore")[0].files[0]);
                } else {

                    toastr.error("This browser does not support HTML5.");
                }
            } else {


                toastr.error("Please upload a valid WITSML file.");
            }

        }
    }
    function getXMLSurveyOpenWell() {
        var wellll = $("#inputOpenWellSurvey").val();

        console.log("survey file " + wellll);
        if (wellll == '') {
            xmlDocOpenWellSurvey = null;
            var dvTable = $("#dvTableOpenWellSurvey");
            dvTable.children().remove();
            toastr.error("Please select survey file");
        }
        else {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xml)$/;

            if (regex.test($("#inputOpenWellSurvey").val().toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var xmlLocal = $.parseXML(e.target.result);
                        xmlDocOpenWellSurvey = e.target.result;
                        var customers = $(xmlLocal).find("trajectory");

                        console.log("data " + customers.length);
                        var dvTable = $("#dvTableOpenWellSurvey");
                        dvTable.children().remove();
                        if (customers.length == null || customers.length == 0) {
                            $("#inputOpenWellSurvey").val('');
                            xmlDocOpenWellSurvey = null;
                            isFoundOpenWell = false;
                            toastr.error("Tag name Survey  is not found");
                        
                            return;
                        }
                        //Create a HTML Table element.
                        var table = $("<table class='table table - bordered table - hover table - striped w - 100' />");
                        table[0].border = "1";


                        //    console.log("find tag value well name " + $(customers[0]).find('nameWell').text());
                        //    $("#well_name").val($(customers[0]).find('nameWell').text());
                        //    $("#afe_number").val($(customers[0]).find('numAFE:first').text());
                        //    getWellByName($(customers[0]).find('nameWell').text(), customers);
                        getWellByName($(customers[0]).find('nameWell').text(), customers);
                        //Add the header row.
                        var row = $(table[0].insertRow(-1));
                        customers.eq(0).children().each(function () {
                            var headerCell = $("<th/>");
                            headerCell.html(this.nodeName);
                            row.append(headerCell);
                        });

                        //Add the data rows.
                        $(customers).each(function () {
                            row = $(table[0].insertRow(-1));
                            $(this).children().each(function () {
                                var cell = $("<td />");
                                cell.html($(this).text());
                                row.append(cell);
                            });
                        });


                        dvTable.html("");
                        dvTable.append(table);
                        $('#loadingModal').modal('hide');
                    }
                    reader.readAsText($("#inputOpenWellSurvey")[0].files[0]);
                } else {
                   
                    toastr.error("This browser does not support HTML5.");
                }
            } else {
           

                toastr.error("Please upload a valid WITSML file.");
            }

        }

    }



    function getXMLFluidsOpenWell() {

        var wellll = $("#inputOpenWellFluids").val();

        console.log("survey file " + wellll);
        if (wellll == '') {
            xmlDocOpenWellFluids = null;
            var dvTable = $("#dvTableOpenWellFluids");
            dvTable.children().remove();
            toastr.error("Please select Fluids file");
        }
        else {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xml)$/;

            if (regex.test($("#inputOpenWellFluids").val().toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var xmlLocal = $.parseXML(e.target.result);
                        xmlDocOpenWellFluids = e.target.result;
                        var customers = $(xmlLocal).find("fluidsReport");

                        console.log("data " + customers.length);
                        var dvTable = $("#dvTableOpenWellFluids");
                        dvTable.children().remove();
                        if (customers.length == null || customers.length == 0) {
                            $("#inputOpenWellFluids").val('');
                            xmlDocOpenWellFluids = null;
                            isFoundOpenWell = false;
                            toastr.error("Tag name Fluid is not found");

                            return;
                        }
                        //Create a HTML Table element.
                        var table = $("<table class='table table - bordered table - hover table - striped w - 100' />");
                        table[0].border = "1";


                        //    console.log("find tag value well name " + $(customers[0]).find('nameWell').text());
                        //    $("#well_name").val($(customers[0]).find('nameWell').text());
                        //    $("#afe_number").val($(customers[0]).find('numAFE:first').text());
                        //    getWellByName($(customers[0]).find('nameWell').text(), customers);
                        getWellByName($(customers[0]).find('nameWell').text(), customers);
                        //Add the header row.
                        var row = $(table[0].insertRow(-1));
                        customers.eq(0).children().each(function () {
                            var headerCell = $("<th/>");
                            headerCell.html(this.nodeName);
                            row.append(headerCell);
                        });

                        //Add the data rows.
                        $(customers).each(function () {
                            row = $(table[0].insertRow(-1));
                            $(this).children().each(function () {
                                var cell = $("<td />");
                                cell.html($(this).text());
                                row.append(cell);
                            });
                        });


                        dvTable.html("");
                        dvTable.append(table);
                        $('#loadingModal').modal('hide');
                    }
                    reader.readAsText($("#inputOpenWellFluids")[0].files[0]);
                } else {

                    toastr.error("This browser does not support HTML5.");
                }
            } else {


                toastr.error("Please upload a valid WITSML file.");
            }

        }
    }


    function getXMLDDROpenWell() {

        var wellll = $("#inputopenwellDDR").val();

        console.log("inputopenwellDDR file " + wellll);
        if (wellll == '') {
            xmlDocOpenWellDDR = null;
            var dvTable = $("#dvTableOpenWellDDR");
            dvTable.children().remove();
            toastr.error("Please select ddr file");
        }
        else {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xml)$/;

            if (regex.test($("#inputopenwellDDR").val().toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var xmlLocal = $.parseXML(e.target.result);
                        xmlDocOpenWellDDR = e.target.result;
                      //  console.log("target result " + e.target.result);
                       // CD_WELLBORE_FORMATION
                        var customers = $(xmlLocal).find("DM_REPORT_JOURNAL");
                        var wellname = $(xmlLocal).find("CD_WELL");
                        var afe = $(xmlLocal).find("DM_AFE");
                        console.log("data " + customers.length);



                   

                        var dvTable = $("#dvTableOpenWellDDR");
                        dvTable.children().remove();
                        if (customers.length == null || customers.length == 0) {
                            $("#inputopenwellDDR").val('');
                            xmlDocOpenWellDDR = null;
                            isFoundOpenWell = false;
                            toastr.error("Tag name ddr  is not found");

                            return;
                        }
                        //Create a HTML Table element.
                        var table = $("<table class='table table - bordered table - hover table - striped w - 100' />");
                        table[0].border = "1";


                       

                        console.log("find tag value well name " + $(wellname).attr('well_legal_name'));

                        var spudate_normal = $(wellname).attr('spud_date');
                        var convert_spudate = spudate_normal.match(/'([^']+)'/)[1];
                        console.log("try to get the spudate " + convert_spudate);
                        $("#well_name_openwell").val($(wellname).attr('well_legal_name'));
                        $("#afe_number_openwell").val($(afe).attr('afe_no'));
                        $("#duration_afe_openwell").val($(afe).attr('estimated_days'));
                        $("#typeSubmitOpenWell").val('ddrxml');
                        
                        //    getWellByName($(customers[0]).find('nameWell').text(), customers);
                        getWellByNameImportDDRXML($(wellname).attr('well_legal_name'), customers);
                        console.log("create table header " + Number($(afe).attr('afe_total')).toPrecision());
                        //Add the header row.
                        var row = $(table[0].insertRow(-1));
                      //  customers.eq(0).children().each(function () {
                        //    console.log("kustomer get the data " + this.nodeName);
                        //    var headerCell = $("<th/>");
                        //    headerCell.html(this.nodeName);
                          //  console.log("header cell " + this.nodeName);
                          //  row.append(headerCell);
                        //});

                        for (var i = 0; i < customers[0].attributes.length; i++) {
                            var attr = customers[0].attributes[i];
                            // alert(attr.name + " = " + attr.value);

                         //   console.log("header attribute " + attr.name + " value " + attr.value);

                            var headerCell = $("<th/>");
                            headerCell.html(attr.name);
                            row.append(headerCell);
                        }



                        //Add the data rows.
                    //    $(customers).each(function () {
                      //      row = $(table[0].insertRow(-1));
                       //     $(this).children().each(function () {
                         //       var cell = $("<td />");
                           //     cell.html($(this).text());
                           //     console.log("kolom cell " + $(this).text());
                           //     row.append(cell);
                           // });
                       // });


                        //sorted by event code DRL

                        var DDRInformation = [];
                        for (var j = 0; j < customers.length; j++) {
                           
                            var elements = customers[j];


                            //console.log("get attribute event code " + elements.getAttribute('event_code'));
                           // console.log("get attribute report alias " + elements.getAttribute('report_alias'));

                            if (elements.getAttribute('event_code') == 'DRL' && elements.getAttribute('report_alias') == 'DDR') {

                                DDRInformation.push(elements);
                            }
                        }

                        console.log("data hasil sorting DDR " + DDRInformation.length);

                        console.log("test orunt " + DDRInformation);

                        for (var j = 0; j < DDRInformation.length; j++) {
                            row = $(table[0].insertRow(-1));
                            var elements = DDRInformation[j];
                            //console.log("get attribute report alias " + elements.getAttribute('report_no') + " get date " + elements.getAttribute('date_report'));
                            for (var i = 0; i < elements.attributes.length; i++) {
                                var attr = elements.attributes[i];
                                var cell = $("<td />");
                                cell.html(attr.value);
                                row.append(cell);
                            }
                        }
                     



                        dvTable.html("");
                        dvTable.append(table);
                        $('#loadingModal').modal('hide');
                    }
                    reader.readAsText($("#inputopenwellDDR")[0].files[0]);
                } else {

                    toastr.error("This browser does not support HTML5.");
                }
            } else {


                toastr.error("Please upload a valid WITSML file.");
            }

        }
    }

    function getWellByNameImportDDRXML(wellname, dataDDR) {
        // lookup ? wellId = " + $wellId.val()
        //lookupWellField
        $.ajax({
            url: $.helper.resolveApi('~/core/well/' + wellname + '/lookupwellname'),
            type: 'GET',
            error: function (request, error) {
                console.log(arguments);
                toastr.error("Error called the data or Server internal error.");
                isFoundOpenWell = false;

            },
            success: function (r) {
                console.log(r.data);

                if (r.data.length > 0) {

                    isFoundOpenWell = true;
                    var idwell = r.data[0].id;
                    well_id = idwell;
                    var spuddate = r.data[0].spud_date;
                    console.log("data well id  " + idwell + " nilai spud date " + spuddate);

                    getDDRbyWellID(idwell, dataDDR);
                    if (r.data.length > 0) {
                        Swal.fire({
                            title: "",
                            text: "The Well found with same data.",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonText: "Yes"
                        }).then(function (result) {
                            if (result.value) {
                                console.log("update data");
                            } else {
                                console.log("create new data");
                            }
                        });
                    }
                    else {
                        isFoundOpenWell = false;
                        Swal.fire({
                            title: "",
                            text: "Well is not found, Please import from XML Well File.",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonText: "Yes"
                        }).then(function (result) {
                            if (result.value) {
                                console.log("update data");
                            } else {
                                console.log("create new data");
                            }
                        });
                    }
                }
                else {
                    isFoundOpenWell = false;
                    Swal.fire({
                        title: "",
                        text: "Well is not found, Please import from WITSML Well File.",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            console.log("update data");
                        } else {
                            console.log("create new data");
                        }
                    });
                }


            }
        });
    }

    function getWellByName(wellname, dataDDR) {
        // lookup ? wellId = " + $wellId.val()
        //GetViewPerPage
        $.ajax({
            url: $.helper.resolveApi('~/core/well/' + wellname + '/lookupwellname'),
            type: 'GET',
            error: function (request, error) {
                console.log(arguments);
                toastr.error("Error called the data or Server internal error.");
                isFoundOpenWell = false;
             
            },
            success: function (r) {
                console.log(r.data);

                if (r.data.length > 0) {

                    isFoundOpenWell = true;
                    var idwell = r.data[0].id;
                    well_id = idwell;
                    var spuddate = r.data[0].spud_date;
                    console.log("data well id  " + idwell + " nilai spud date " + spuddate);

                    getDDRbyWellID(idwell, dataDDR);
                    if (r.data.length > 0) {
                        Swal.fire({
                            title: "",
                            text: "The Well found with same data.",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonText: "Yes"
                        }).then(function (result) {
                            if (result.value) {
                                console.log("update data");
                            } else {
                                console.log("create new data");
                            }
                        });
                    }
                    else {
                        isFoundOpenWell = false;
                        Swal.fire({
                            title: "",
                            text: "Well is not found, Please import from WITSML Well File.",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonText: "Yes"
                        }).then(function (result) {
                            if (result.value) {
                                console.log("update data");
                            } else {
                                console.log("create new data");
                            }
                        });
                    }
                }
                else {
                    isFoundOpenWell = false;
                    Swal.fire({
                        title: "",
                        text: "Well is not found, Please import from WITSML Well File.",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            console.log("update data");
                        } else {
                            console.log("create new data");
                        }
                    });
                }


            }
        });
    }



    $('#submitOpenWell').click(function (event) {

     
        var wellll = $("#inputOpenWell").val();
        console.log("well from tap " + wellll);
        if (wellll == '') {
            toastr.error("Please select well file");
        }
        else {

            $('#newModalOpenWell').modal('show');
            $("#xmlRecordOpenWell").val(xmlDocOpenWell);
        }
    })

    $('#submitOpenWellbore').click(function (event) {


        var wellll = $("#inputOpenWellbore").val();
        console.log("wellbore from tap " + wellll);
        if (wellll == '') {
            toastr.error("Please select wellbore file");
        }
        else {


            if (isFoundOpenWell) {
                var wellboreInfor = $(xmlDocOpenWellbore).find("wellbore");
                var nameWell = $(wellboreInfor[0]).find('nameWell:first').text();
                var mdKickoff = $(wellboreInfor[0]).find('mdKickoff:first').text();
                var tvdKickoff = $(wellboreInfor[0]).find('tvdKickoff:first').text();
                var mdPlanned = $(wellboreInfor[0]).find('mdPlanned:first').text();
                var tvdPlanned = $(wellboreInfor[0]).find('tvdPlanned:first').text();
                Swal.fire(
                    {
                        title: "",
                        text: "Are you sure want to replace wellbore data with well " + nameWell +"?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            // btn.button('loading');

                            submitGeneralWellInfo(mdKickoff, tvdKickoff,mdPlanned, tvdPlanned);
                        }
                    });

            }
            else {
                toastr.error("Well is not found, Please import from WITSML Well File.");
            }


        }
    })

    var submitGeneralWellInfo = function ( mdKickoff, tvdKickoff, mdPlanned, tvdPlanned ) {

        $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
        var data = new Object();
        data.kick_of_point = mdKickoff;
        data.kop_depth = tvdKickoff;
        data.datum_elevation = mdPlanned;
        data.well_head_elevation = tvdPlanned;
        data.id = well_id;

        $.post($.helper.resolveApi('~/core/Well/save'), data, function (r) {
            if (r.status.success) {
                console.log(r);
                $("#inputOpenWellbore").val('');

                var dvTable = $("#dvTableOpenWellBore");
                dvTable.children().remove();
                toastr.success(r.status.message);
            } else {
                toastr.error(r.status.message)
            }


            $('#loadingModal').modal('hide');
        }, 'json').fail(function (r) {

            toastr.error(r.statusText);
            $('#loadingModal').modal('hide');
        });
    }



    // fluid file
    $('#submitOpenWellFluids').click(function (event) {


        var wellll = $("#inputOpenWellFluids").val();
        console.log("fluid from tap " + wellll);
        if (wellll == '') {
            toastr.error("Please select Fluid file");
        }
        else {


            if (isFoundOpenWell) {
             

                Swal.fire(
                    {
                        title: "",
                        text: "Are you sure want to replace  all fluids data?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            // btn.button('loading');

                            submitAllDDR();
                        }
                    });

            }
            else {
                toastr.error("Well is not found, Please import from WITSML Well File.");
            }


        }
    })

    //survey file
    $('#submitOpenWellSurvey').click(function (event) {


        var wellll = $("#inputOpenWellSurvey").val();
        console.log("survey file from tap " + wellll);
        if (wellll == '') {
            toastr.error("Please select Survey file");
        }
        else {


            if (isFoundOpenWell) {
                var wellboreInfor = $(xmlDocOpenWellbore).find("wellbore");
                var nameWell = $(wellboreInfor[0]).find('nameWell:first').text();
            
                Swal.fire(
                    {
                        title: "",
                        text: "Are you sure want to replace wellbore data with well " + nameWell + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            // btn.button('loading');

                            submitGeneralWellInfo(mdKickoff, tvdKickoff, mdPlanned, tvdPlanned);
                        }
                    });

            }
            else {
                toastr.error("Well is not found, Please import from WITSML Well File.");
            }


        }
    })



    //submit DDR XML 
    $('#submitOpenWellDdr').click(function (event) {


        var wellll = $("#inputopenwellDDR").val();
        console.log("DDR XML file from tap " + wellll);
        if (wellll == '') {
            toastr.error("Please select DDR file");
        }
        else {


            if (isFoundOpenWell) {
                var wellboreInfor = $(xmlDocOpenWellbore).find("wellbore");
                var nameWell = $(wellboreInfor[0]).find('nameWell:first').text();

                Swal.fire(
                    {
                        title: "",
                        text: "Are you sure want to replace wellbore data with well " + nameWell + "?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            // btn.button('loading');

                            submitGeneralWellInfo(mdKickoff, tvdKickoff, mdPlanned, tvdPlanned);
                        }
                    });

            }
            else {
                $('#newModalOpenWell').modal('show');
                $("#xmlRecordOpenWell").val(xmlDocOpenWellDDR);
            }


        }
    })


    //get DDR File
    var getDDRbyWellID = function (id, dataDDR) {
        $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
        //   url: $.helper.resolveApi('~/core/drilling/getListDrilling/' + $well_id + "/" + start_date + "/" + end_date),
        $.get($.helper.resolveApi('~/core/Drilling/getByWellId/' + id), function (r) {
            if (r.status.success) {
                console.log(r);

                if (r.data.length == dataDDR.length) {

                    toastr.info("Total DDR is still same");
                    getoldDDR = r.data;
                }
                else if (r.data.length < dataDDR.length) {

                    var counterDDR = dataDDR.length - r.data.length;
                    getoldDDR = r.data;
                    toastr.info("There are " + counterDDR + " new DDR found in import file");

                }
                else {
                    toastr.info("There are " + counterDDR + " new DDR found in import file");
                }
            } else {
                toastr.error(r.status.message);
            }
            $('#loadingModal').modal('hide');
            $('.loading-detail').hide();
        }).fail(function (r) {
            console.log(r);
            toastr.error(r.status.message);
        });
    }

    //submit DDR
    var submitAllDDR = function () {

        var dataDDR = $(xmlDocOpenWellFluids).find("fluidsReport");


        if (getoldDDR.length == 0) {

            for (var i = 0; i < dataDDR.length; i++) {
                var drillingdate = $(dataDDR[i]).find('dTim:first').text();
                var contvertdrillingdate = moment(drillingdate).utc().format('MM/DD/YYYY');

                // console.log("drriing date conver " + contvertdrillingdate + "  well name nya adalah " + wellname);
                //console.log("well name " + wellname + " drilling date " + d + "currend drilling date" + drillingdate + "hasil convert biasa " + contvertdrillingdate);

                submitDDR(well_id,  contvertdrillingdate);
            }

        }
        else {
            for (var i = 0; i < dataDDR.length; i++) {

                var drillingdate = $(dataDDR[i]).find('dTim:first').text();
                var contvertdrillingdate = moment(drillingdate).utc().format('MM/DD/YYYY');
                submitDDR(well_id, contvertdrillingdate);
            

            }
        }
      

    }
    var submitDDR = function (idWell,  drillingDate) {

        $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
        var data = new Object();
        data.drilling_date = drillingDate;
        data.well_id = idWell;
        $.post($.helper.resolveApi('~/core/drilling/newdrilling'), data, function (r) {
            if (r.status.success) {
                console.log(r);
                toastr.success(r.status.message);
                //  window.location = $.helper.resolve("/core/drilling/detail?id=") + r.data.recordId + "&well_id=" + $well_id + "&isnew=true";

            } else {
                toastr.error(r.status.message)
            }

            $("#inputOpenWellFluids").val('');

            var dvTable = $("#dvTableOpenWellFluids");
            dvTable.children().remove();
            $('#loadingModal').modal('hide');
        }, 'json').fail(function (r) {

            toastr.error(r.statusText);
            $('#loadingModal').modal('hide');
        });
    }

});
