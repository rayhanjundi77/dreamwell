﻿$(document).ready(function () {
    'use strict';
    var $btnFilter = $("#btnFilter");

    var $filterType = 0;
    var $aphId = "";


    var $tableInfoNPT = $("#tableInfoNPT");
    var $framenpt = $("#framenpt");
    var $framenptyear = $("#framenptyear");
    var $frametablenptyear = $("#frametablenptyear");

    var $pageLoading = $('#page-loading-content');
    var $dt_nptdetail = $('#dt_nptdetail');
    var $dt_nptdetailyear = $('#dt_nptdetailyear');


    var pageFunction = function () {
        var APHLookup = function () {
            $('#btn-business-unit-lookup').click(function () {
                var btn = $(this);
                btn.button('loading');
                $("#field-lookup").button('reset');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/businessunit/Lookup',
                    success: function (data) {
                        btn.button('reset');
                        //$("#field-lookup").button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "",
                            callback: function (e) {
                                //console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {
                            $box.modal('hide');
                            $aphId = event.node.id;
                            //console.log($aphId);
                            $("#formFilter").removeClass("d-none");
                            $("#btn-business-unit-lookup").text("" + event.node.text);
                        });
                    }
                });
            });
        }

        $("#filteryear").select2({
            tags: true,
            tokenSeparators: [',', ' ']
        })

        var HidePanel = function () {
            $(".panel-report").hide();
            $tableInfoNPT.hide();
            $framenpt.hide();
            $framenptyear.hide();
            $frametablenptyear.hide();
        }

        $btnFilter.on('click', function () {
            var isValid = true;

            HidePanel();

            var $filteryear = $("#filteryear").val();

            if ($filteryear.length == 0) {
                toastr.error("Please select Year to show details");
                isValid = false;
            }

            if (!isValid)
                return;
            else {
                loaddata($aphId, $filteryear);
            }
        });


        var loaddata = function (aphId, ayear) {

            var data = {
                recordId: aphId,
                ayear: ayear
            };

            loadNPTGrid(data);
            loadNPTYearGrid(data);
        };

        var loadNPTGrid = function (vparam) {
            var dt = $dt_nptdetail.dataTable({
                destroy: true,
                responsive: true,
                serverSide: false,
                ajax: function (data, callback, settings) {
                    $.ajax({
                        url: $.helper.resolveApi("~/UTC/ServicePerformance/PTNPT/Details"),
                        type: 'POST',
                        data: vparam,
                        success: function (r) {
                            if (r.status.success) {
                                callback({ data: r.data.data });
                                if (r.data.data.length > 0) {
                                    $tableInfoNPT.show();
                                    $framenpt.show();
                                    LoadNPTChart(r.data.data);
                                    LoadInfo(r.data.datadesc);
                                }

                            }
                        }
                    });
                },
                sDom: "",
                order: [],
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "description",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    },
                    {
                        data: "hours",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    }
                ],
                initComplete: function (settings, json) {

                }
            }, function (e, settings, json) {
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        };

        var LoadInfo = function (infodata) {
            $("#totalnpt").html(infodata.npt_in_day);
            $("#totalday").html(infodata.opr_in_day);
            $("#hourbyday").html(infodata.npt_by_totalday + " %");
        };

        var LoadNPTChart = function (vdata) {
            var datachart = [];
            var _data = [];

            if (vdata.length > 0) {
                $.each(vdata, function (index, value) {
                    _data = {
                        name: value.description + ', ' + value.hours,
                        y: value.hours
                    };

                    datachart.push(_data);
                });
            }

            var setChart = {
                renderTo: 'npt-chart',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
            };

            var chart = new Highcharts.Chart({
                chart: setChart,
                title: {
                    text: 'Non Productive Time'
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f} Hrs</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f}%'
                        }
                    }
                },
                tooltip: {
                    pointFormat: '<b>Total NPT: {point.y:.2f} Hrs</b><br/>',
                },
                series: [{
                    data: datachart
                }],
                credits: {
                    enabled: false
                },
            });
        };

        var loadNPTYearGrid = function (vparam) {
            var dt = $dt_nptdetailyear.dataTable({
                destroy: true,
                responsive: true,
                serverSide: false,
                ajax: function (data, callback, settings) {
                    $.ajax({
                        url: $.helper.resolveApi("~/UTC/ServicePerformance/PTNPT/DetailsYear"),
                        type: 'POST',
                        data: vparam,
                        success: function (r) {
                            console.log(r);
                            if (r.status.success) {
                                console.log(r.data);
                                $frametablenptyear.show();
                                callback({ data: r.data.listdatadesc });
                                if (r.data.listNPT.length > 0 || r.data.hoursPerYear.length > 0) {
                                    $framenptyear.show();
                                    LoadNPTChartYear(r.data.listNPT, r.data.hoursPerYear);
                                }
                            }
                        }
                    });
                },
                sDom: "",
                order: [],
                columns: [
                    {
                        data: "syear",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    },
                    {
                        data: "opr_in_day",
                        orderable: false,
                        searchable: false,
                        class: "text-center"
                    },
                    {
                        data: "npt_in_day",
                        orderable: false,
                        searchable: false,
                        class: "text-center"
                    },
                    {
                        data: "npt_by_totalday",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            return row.npt_by_totalday + ' %';
                        }

                    }
                ],
                initComplete: function (settings, json) {

                }
            }, function (e, settings, json) {
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        };

        var LoadNPTChartYear = function (listNPTdata, hoursPerYeardata) {

            var syear = '';
            if (hoursPerYeardata.length > 0) {
                for (var i = 0; i < hoursPerYeardata.length; i++) {
                    if (i == 0) {
                        syear += '' + hoursPerYeardata[i].name;
                    }
                    else {
                        syear += ', ' + hoursPerYeardata[i].name;
                    }
                }
            }

            var setChart = [];
            var setPlot = [];
            setChart = {
                renderTo: 'nptyear',
                type: 'column',
            };

            var chart = new Highcharts.Chart({
                chart: setChart,
                title: {
                    text: 'NPT ' + syear
                },
                xAxis: {
                    categories: listNPTdata,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Hours'
                    }
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} Hours</b><br/>',
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                //series: datachart,
                series: hoursPerYeardata,
                credits: {
                    enabled: false
                }
            });

            //var chart = new Highcharts.Chart({
            //    chart: setChart,
            //    title: {
            //        text: 'NPT'
            //    },
            //    xAxis: {
            //        categories: [
            //            'Jan',
            //            'Feb',
            //            'Mar',
            //            'Apr',
            //            'May',
            //            'Jun',
            //            'Jul',
            //            'Aug',
            //            'Sep',
            //            'Oct',
            //            'Nov',
            //            'Dec'
            //        ],
            //        crosshair: true
            //    },
            //    yAxis: {
            //        min: 0,
            //        title: {
            //            text: 'Hours'
            //        }
            //    },
            //    tooltip: {
            //        pointFormat: '{series.name}: <b>{point.y} Hours</b><br/>',
            //    },
            //    plotOptions: {
            //        column: {
            //            pointPadding: 0.2,
            //            borderWidth: 0
            //        }
            //    },
            //    series: [{
            //        name: 'Tokyo',
            //        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

            //    }, {
            //        name: 'New York',
            //        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

            //    }, {
            //        name: 'London',
            //        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

            //    }, {
            //        name: 'Berlin',
            //        data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

            //    }],
            //    credits: {
            //        enabled: false
            //    }
            //});

            //var chart = new Highcharts.Chart({
            //    chart: setChart,
            //    title: {
            //        text: 'Depth Benchmark'
            //    },
            //    subtitle: {
            //        text: ''
            //    },
            //    yAxis: {
            //        min: 0,
            //        title: {
            //            text: 'Depth',
            //            align: 'middle'
            //        },
            //        labels: {
            //            overflow: 'justify'
            //        }
            //    },
            //    xAxis: {
            //        categories: wellName,
            //        title: {
            //            text: null
            //        }
            //    },
            //    legend: {
            //        align: 'center',
            //        verticalAlign: 'bottom',
            //        x: 0,
            //        y: 10
            //    },
            //    plotOptions: setPlot,
            //    tooltip: {
            //        pointFormat: '{series.name}: <b>{point.y} ' + data.uom_select + '</b><br/>',
            //    },
            //    series: [
            //        {
            //            name: "Plan",
            //            data: plan
            //        },
            //        {
            //            name: "Actual",
            //            data: actual
            //        }
            //    ],
            //    credits: {
            //        enabled: false
            //    },

            //});
        }

        return {
            init: function () {
                APHLookup();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
});

