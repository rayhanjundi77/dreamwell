﻿(function ($) {
    'use strict';
    var $dt_listAfe = $('#dt_listAfe');
    var $pageLoading = $('#page-loading-content');
 
    var pageFunction = function () {
        var $recordId = $('input[id=recordId]');


        var loadDataTable = function () {
            var dt = $dt_listAfe.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/afe/dataTableByUnit?bussinessUnitID=" + $recordId.val()),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "afe_no",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return `<a href="` + $.helper.resolve("/core/Afe/ManageDetail?id=") + row.id + `" class="text-bold text-info fw-700">` + row.afe_no + `</a>`;
                            }
                            return data;
                        }
                    },
                    { data: "well_name" },
                    { data: "field_name" },
                    { data: "unit_name" },
                    {
                        data: "duration",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return `<div>` + thousandSeparatorWithoutComma(row.duration) + `</div>`;
                            }
                            return data;
                        }
                    },
                    //{
                    //    data: "id",
                    //    orderable: true,
                    //    searchable: true,
                    //    class: "text-center",
                    //    render: function (data, type, row) {
                    //        if (type === 'display') {
                    //            var output = ``;
                    //            if (row.submitted_by != null && row.submitted_on != null) {
                    //                output = `<span class="badge badge-success pt-1 pb-1 pl-3 pr-3">Submitted</span>`;
                    //            } else {
                    //                output = `<span class="badge badge-warning pt-1 pb-1 pl-3 pr-3">Draft</span>`;
                    //            }
                    //            return output;
                    //        }
                    //        return data;
                    //    }
                    //},
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            console.log(row);
                            if (type === 'display') {
                                var url = $.helper.resolveApi('~/core/AfePlan/download?id=' + row.id + '&x-Token=' + $('meta[name=x-token]').attr("content"));
                                var output =
                                    `<div class ="btn-group" data-id= "` + row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/core/afe/detail?id=") + row.id + `" class="modalTrigger btn btn-info btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#afeModal">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-warning btn-xs btn-warning waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                    <a href="`+ url + `" class="btn btn-default btn-xs btn-default waves-effect waves-themed row-download"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        Download
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    },
                    {
                        data: "duration",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return ``;
                            }
                            return data;
                        }
                    },
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/afe/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dt_listAfe.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                    //$(this).on('click', '.row-download', function () {
                    //    window.location.href = $.helper.resolveApi("~/core/AfePlan/download");
                    //});
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 