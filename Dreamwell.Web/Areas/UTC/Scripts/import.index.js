﻿$(document).ready(function () {
    'use strict';

    var isFoundSameWell = false;
    var getoldDDR;
    var submitwell = $('#submitwell');
    var choosefilewell = $('#choosefilewell');
	var submitrig = $('#submitrig');
	var submittrajectory = $('#submittrajectory');

    var fileWell = $("#inputWell");
	var fileRig = $("#inputRig");
	var fileTrajectory = $("#inputTrajectoryStation");

	//var inputWell = $("#inputWell").val();
	//var inputRig = $.trim($("#inputRig").val());
	//var inputTrajectoryStation = $.trim($("#inputTrajectoryStation").val());

	//disable
	submitrig.prop('disabled', true);
	submittrajectory.prop('disabled', true);
	fileRig.prop('disabled', true);
    fileTrajectory.prop('disabled', true);


   

    $('#inputWell').on("change", function () { getXMLWell(); });

	submitwell.on('click', function () {
        console.log("tap submit well ");
        var btn = $(this);
        var wellll = $("#inputWell").val();
        console.log("well from tap " + wellll);
        if (wellll == '') {
            toastr.error("Please select well file");
        }
        else {
            if (isFoundSameWell) {

                Swal.fire(
                    {
                        title: "",
                        text: "Are you sure want to replace all data?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            // btn.button('loading');
                            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
                            submitAllDDR();
                        }
                    });
           
            }
            else {
                $('#newWellModal').modal('show');
                $("#xmlRecord").val(xmlDoc);
            }

   
        
        }
	});


    function getXMLWell() {
        var wellll = $("#inputWell").val();
     
        console.log("well " + wellll);
        if (wellll == '') {
            xmlDoc = null;
            isFoundSameWell = false;
            getoldDDR = null;
            var dvTable = $("#dvTable");
            dvTable.children().remove();
            toastr.error("Please select well file");
        }
        else {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xml)$/;

            if (regex.test($("#inputWell").val().toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var xmlLocal = $.parseXML(e.target.result);
                        xmlDoc = e.target.result;
                        var customers = $(xmlLocal).find("opsReport");

                        console.log("data " + customers.length);
                        var dvTable = $("#dvTable");
                        dvTable.children().remove();
                        if (customers.length == null || customers.length == 0) {
                            $("#inputWell").val('');
                            isFoundSameWell = false;
                            getoldDDR = null;
                            toastr.error("Tag name Ops Report  is not found");
                            xmlDoc = null;
                            return;
                        }
                        //Create a HTML Table element.
                        var table = $("<table class='table table - bordered table - hover table - striped w - 100' />");
                        table[0].border = "1";


                   //     $.each(customers, function (i, item) {

                     //       console.log("i disini adalah " + i);
                       //     console.log("item disini adalah " + item);
                      //      $(this).find('nameWell').text();
                      //      var wellname = $(this).find('nameWell').text();
                          
                      //  });
                       
                        console.log("find tag value well name " + $(customers[0]).find('nameWell').text());
                        $("#well_name").val($(customers[0]).find('nameWell').text());
                        $("#afe_number").val($(customers[0]).find('numAFE:first').text());
                        getWellByName($(customers[0]).find('nameWell').text(), customers);

                        //Add the header row.
                        var row = $(table[0].insertRow(-1));
                        customers.eq(0).children().each(function () {
                            var headerCell = $("<th/>");
                            headerCell.html(this.nodeName);
                            row.append(headerCell);
                        });

                        //Add the data rows.
                        $(customers).each(function () {
                            row = $(table[0].insertRow(-1));
                            $(this).children().each(function () {
                                var cell = $("<td />");
                                cell.html($(this).text());
                                row.append(cell);
                            });
                        });

                      
                        dvTable.html("");
                        dvTable.append(table);
                        $('#loadingModal').modal('hide');
                    }
                    reader.readAsText($("#inputWell")[0].files[0]);
                } else {
                    xmlDoc = null;
                    isFoundSameWell = false;
                    toastr.error("This browser does not support HTML5.");
                }
            } else {
                xmlDoc = null; isFoundSameWell = false;

                toastr.error("Please upload a valid WITSML file.");
            }
         
        }


  
    }


    function getWellByName(wellname,dataDDR) {
        // lookup ? wellId = " + $wellId.val()
        //lookupWellField
        $.ajax({
            url: $.helper.resolveApi('~/core/well/' + wellname + '/lookupwellname'),
            type: 'GET',
            error: function (request, error) {
                console.log(arguments);
                toastr.error("Error called the data or Server internal error.");
                isFoundSameWell = false;
                getoldDDR = null;
            },
            success: function (r) {
                console.log(r.data);

                if (r.data.length > 0 ) {

                    isFoundSameWell = true;
                    var idwell = r.data[0].id;
                    var spuddate = r.data[0].spud_date;
                    console.log("data well id  " + idwell + " nilai spud date " + spuddate);
                    getDDRbyWellID(idwell, dataDDR);
                    if (r.data.length > 0) {
                        Swal.fire({
                            title: "",
                            text: "The Well found with same data.",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonText: "Yes"
                        }).then(function (result) {
                            if (result.value) {
                                console.log("update data");
                            } else {
                                console.log("create new data");
                            }
                        });
                    }
                    else {

                    }
                }

               
            }
        });
    }


    var getDDRbyWellID = function (id,dataDDR) {
        $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
     //   url: $.helper.resolveApi('~/core/drilling/getListDrilling/' + $well_id + "/" + start_date + "/" + end_date),
        $.get($.helper.resolveApi('~/core/Drilling/getByWellId/' + id), function (r) {
            if (r.status.success) {
                console.log(r);

                if (r.data.length == dataDDR.length) {

                    toastr.info("Total DDR is still same");
                    getoldDDR = r.data;
                }
                else if (r.data.length < dataDDR.length) {

                    var counterDDR = dataDDR.length - r.data.length;
                    getoldDDR = r.data;
                    toastr.info("There are " + counterDDR + " new DDR found in import file");

                }
                else {

                }
            } else {
                toastr.error(r.status.message);
            }
            $('#loadingModal').modal('hide');
            $('.loading-detail').hide();
        }).fail(function (r) {
            console.log(r);
            toastr.error(r.status.message);
        });
    }


    var populateDataDDR = function (dataDDR) {


    }

    var submitAllDDR = function () {
        var dataDDR = $(xmlDoc).find("opsReport");

        for (var i = 0; i < dataDDR.length; i++) {

            var drillingdate = $(dataDDR[i]).find('dTim:first').text();
            var contvertdrillingdate = moment(drillingdate).utc().format('MM/DD/YYYY');

            if (getoldDDR.length > 0) {

                for (var j = 0; j < getoldDDR.length; j++) {

                    var old_drillingdate = getoldDDR[j].drilling_date;

                    var convert_old_drilling_date = moment(old_drillingdate).utc().format('MM/DD/YYYY');

                    if (convert_old_drilling_date == contvertdrillingdate) {


                        saveDrillingOperationData(getoldDDR[j], dataDDR[i]);
                    }

                }

            }

        }

    }


    var saveDrillingOperationData = function (oldDDR , newDDR) {

        var obj = new Object();
        obj.id = oldDDR.id; 
        obj.well_id = oldDDR.well_id;
        obj.operation_data_period = $(newDDR).find('sum24Hr:first').text();
        obj.operation_data_early = $(newDDR).find('statusCurrent:first').text();
        obj.operation_data_planned = $(newDDR).find('forecast24Hr:first').text();
       // obj.cummulative_cost = $(newDDR).find('costDay:first').text();
        //convert object to json string
        var string = JSON.stringify(obj);
        //convert string to Json Object
        var dataOperation = JSON.parse(string);

        $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
        $.post($.helper.resolveApi('~/core/drilling/save'), dataOperation, function (r) {
            if (r.status.success) {
         
                toastr.success(r.status.message);

                savePersonelData(oldDDR, newDDR);
            } else {
                toastr.error(r.status.message);
            }
            $('#loadingModal').modal('hide');
        }, 'json').fail(function (r) {
           
            toastr.error(r.statusText);
            $('#loadingModal').modal('hide');
        });

    }

    var savePersonelData = function (oldDDR, newDDR)
    {
      

        var getPersonelDatas = $(newDDR).find('personnel');

        var myDataPersonal = [];
        console.log("get total personal data" + getPersonelDatas.length);
        if (getPersonelDatas.length > 0) {

            for (var i = 0; i < getPersonelDatas.length; i++) {

                var obj = new Object();
                obj.drilling_id = oldDDR.id;
                obj.job_personel_id = "10df2941-abf3-48c2-a0c6-c374a70d044c";
                obj.personel = $(getPersonelDatas[i]).find('typeService:first').text();
                obj.company = $(getPersonelDatas[i]).find('company:first').text();
                obj.pax = $(getPersonelDatas[i]).find('numPeople:first').text();
         
                myDataPersonal.push(obj);
             }
        }
        var string = JSON.stringify(myDataPersonal);
        console.log("data personal json " + string);
        var dataOperasional = JSON.parse(string);
        $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
        $.ajax({
            url: $.helper.resolveApi('~/core/drillingpersonel/saveAll?drillingId=' + oldDDR.id),
            type: "POST",
            data: string,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                //console.log(r);
                if (r.status.success) {
                    toastr.success("Data Personal Updated");
                
                } else {
                    toastr.error(r.status.message);
                }
                $('#loadingModal').modal('hide');
            }
        });

    }

    var saveFluidsData = function (oldDDR, newDDR) {


        var getFluidsData = $(newDDR).find('fluid');

        var myDataFluids = [];
        console.log("get total length fluids" + getFluidsData.length);
        if (getFluidsData.length > 0) {

            for (var i = 0; i < getFluidsData.length; i++) {

                var obj = new Object();
                obj.drilling_id = oldDDR.id;
                obj.job_personel_id = "10df2941-abf3-48c2-a0c6-c374a70d044c";
                obj.personel = $(getPersonelDatas[i]).find('typeService:first').text();
                obj.company = $(getPersonelDatas[i]).find('company:first').text();
                obj.pax = $(getPersonelDatas[i]).find('numPeople:first').text();

                myDataPersonal.push(obj);
            }
        }
        var string = JSON.stringify(myDataPersonal);
        console.log("data personal json " + string);
        var dataOperasional = JSON.parse(string);
        $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
        $.ajax({
            url: $.helper.resolveApi('~/core/drillingpersonel/saveAll?drillingId=' + oldDDR.id),
            type: "POST",
            data: string,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                //console.log(r);
                if (r.status.success) {
                    toastr.success("Data Personal Updated");

                } else {
                    toastr.error(r.status.message);
                }
                $('#loadingModal').modal('hide');
            }
        });

    }
    var getDrillingPersonelId = function (recordId) {
        $.get($.helper.resolveApi('~/core/drillingpersonel/' + recordId + '/getByDrillingId'), function (r) {
            if (r.status.success) {
                if (r.data.length > 0) {
           
                }
            }
         
        }).fail(function (r) {
            //console.log(r);
        }).done(function () {

        });
    }

    var submitAllDDRV2 = function () {
        var dataDDR = $(xmlDoc).find("opsReport");
        var dvTable = $("#dvTable");
        dvTable.children().remove();

        //Create a HTML Table element.
        var table = $("<table class='table table - bordered table - hover table - striped w - 100' id= 'ddrviewer' />");
        table[0].border = "1";


        var row = $(table[0].insertRow(-1));

        var headerCell = $("<tr><th>No</th> <th> <input type='checkbox'name='checkall' value='Check All' checked></th> " +
            " <th>DDR Date From File</th> <th>DDR Date From Application</th><th>Action</th></tr > ");
        row.append(headerCell);


        var childRow;
        // input check box make number, date DDR from file , date from old DDR , submit button per cell
        var number = 0;
        for (var i = 0; i < dataDDR.length; i++) {

            var drillingdate = $(dataDDR[i]).find('dTim:first').text();
            var contvertdrillingdate = moment(drillingdate).utc().format('MM/DD/YYYY');

            number = number + 1;

            childRow = $("<tr><td>" + number + "</td> <td> <input type='checkbox' name='check[]' value='' checked></td> <td>" + contvertdrillingdate + "</td> <th>123</td><td><button type='button' class='btn btn - info btn - xs ml - auto ml - sm - 0 waves - effect waves - themed btnClassInput' id='submitCheck'> <span class='fal fa - save mr - 1'></span>Submit</button></td> </tr>");

            row.append(childRow);

        }

        dvTable.html("");
        dvTable.append(table);
    }
    function add_fields() {
        document.getElementById("myTable").insertRow(-1).innerHTML = '<tr><td><textarea name ="Question" placeholder="Question" th: field = "${questionAnswerSet.question}" id="question" style = "resize: none; width:100%;"></textarea></td><td><textarea name="Answer" placeholder ="Answer" th: field = "${questionAnswerSet.answer}" id="answer" style="resize:none;width: 100%;"></textarea></td ></tr>';
    }
});
