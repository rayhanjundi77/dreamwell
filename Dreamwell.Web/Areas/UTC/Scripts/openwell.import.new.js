﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordIDOpenwell]');
    var $form = $("#formNewOpenWell")
    var $test = 0

    // var well_name = $('#well_name');
    //   $('#well_name').attr('disabled', true);
    //$("#afe_number").attr("disabled", true);
    // well_name.prop('disabled', true)

    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    }

    //-- L

    var pageFunction = function () {
        var wellTypeLookup = function () {
            $("#well_status_openwell").val('');
            $("#well_status_openwell").select2({
                dropdownParent: $form,
                placeholder: "Select a Well Type",
                allowClear: true,
            });

            $("select[id='well_status_openwell']").on('change', function () {
                if ($(this).val() == 0) {
                    $(".row-parent").show();
                    $("#parent_openwell").val('');
                } else {
                    $(".row-parent").hide();
                    //$("#parent_well").val('');
                }
                loadWell($(this).val());
            });
        }

        var loadWell = function (status) {
            if (status != null) {
                if (status == 0) {
                    $("#parent_openwell").prop('disabled', 'disabled');
                } else {
                    $("#parent_openwell").removeAttr('disabled');
                    $("#parent_openwell").cmSelect2({
                        url: $.helper.resolveApi('~/core/well/lookup'),
                        result: {
                            id: 'id',
                            text: 'well_name'
                        },
                        filters: function (params) {
                            console.log(params);
                            return [{
                                field: "well_name",
                                operator: "contains",
                                value: params.term || '',
                            }];
                        },
                        options: {
                            destroy: true,
                            dropdownParent: $form,
                            placeholder: "Select a Well",
                            allowClear: true,
                        }
                    });

                    $("#parent_openwell").on('select2:select', function (e) {
                        var id = e.target.value;
                        getAphField(id);
                    });
                }
            }
        }

        var loadAPH = function (initValue) {
            // console.log('load APH');
            easyloader.load('combotree', function () {        // load the specified module
                $('#business_unit_id_openwell').combotree({
                    textField: 'text',
                    value: initValue,
                    loader: function (param, success, error) {
                        $.ajax({
                            type: "POST",
                            url: $.helper.resolveApi("~/core/BusinessUnit/combotree"),
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({}),
                            dataType: 'json',
                            success: function (response) {
                                console.log('data APH sukses');
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err datanya');
                                error.apply(this, arguments);
                            }
                        });
                    },
                    onSelect: function (node) {
                        console.log("Node:: ");
                        console.log(node);
                        loadFields([''], node.id);
                        //loadField(node.id, []);
                    }
                });
            })
        }
        loadAPH(['']);




        var loadFields = function (initValue, businessUnitId) {
            easyloader.load('combotree', function () {        // load the specified module
                $('#field_id_openwell').combotree({
                    textField: 'text',
                    value: initValue,
                    loader: function (param, success, error) {
                        $.ajax({
                            type: "POST",
                            url: $.helper.resolveApi("~/core/Asset/GetFieldNode?countryId=&businessUnitId=" + businessUnitId),
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({}),
                            dataType: 'json',
                            success: function (response) {
                                console.log(response);
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err');
                                error.apply(this, arguments);
                            }
                        });
                    },
                    onSelect: function (node) {
                        console.log("Node:: ");
                        console.log(node);
                        //loadField(node.id, []);
                    }
                });
            })
        }


        loadAPH(['']);

        var getAphField = function (id) {
            if (id != null) {
                $.get($.helper.resolveApi('~/core/well/' + id + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#business_unit_id_openwell').combotree({ value: r.data.business_unit_id });
                        $('#field_id_openwell').combotree({ value: r.data.field_id });
                    }
                });
            }
        }


        $('#btn-save-openWell').click(function (event) {
            //var obj = JSON.parse(text);

          
            //   console.log("data value xml doc " + getXMLDOC);
        

            var afe_number = $('#afe_number_openwell').val();

            var well_name = $('#well_name_openwell').val();
            var well_status = $('#well_status_openwell').val();
            var duration_afe = $('#duration_afe_openwell').val();
            var business_unit_id = $('#business_unit_id_openwell').val();
            var field_id = $('#field_id_openwell').val();

            var obj = new Object();
            obj.id = null;
            obj.well_status = well_status;
            obj.well_name = well_name;
            obj.business_unit_id = business_unit_id;
            obj.field_id = field_id;
            //convert object to json string
            var string = JSON.stringify(obj);
            //convert string to Json Object
            var dataJSONWELL = JSON.parse(string);


            // var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                Swal.fire(
                    {
                        title: "",
                        text: "Are you sure want to import a new Well?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            // btn.button('loading');
                            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
                            $.post($.helper.resolveApi('~/core/well/new'), dataJSONWELL, function (r) {
                                if (r.status.success) {
                                   
                                    var objAFE = new Object();
                                    objAFE.id = null;
                                    objAFE.afe_no = afe_number;
                                    objAFE.well_id = r.data.recordId;
                                    objAFE.duration = duration_afe;
                                    objAFE.business_unit_id = business_unit_id;
                                    objAFE.field_id = field_id;
                                    var stringafe = JSON.stringify(objAFE);
                                    var dataJSONAFE = JSON.parse(stringafe);
                                    submitAFE(r.data.recordId, dataJSONAFE);

                                } else {
                                    toastr.error(r.status.message)
                                }
                                $('#loadingModal').modal('hide');
                                //btn.button('reset');
                                $("#newModalOpenWell").modal('hide');
                            }, 'json').fail(function (r) {
                                // btn.button('reset');
                                toastr.error(r.statusText);
                            });
                        }
                    });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        var submitAFE = function (wellid, datajson) {

            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            $.post($.helper.resolveApi('~/core/afe/save'), datajson, function (r) {
                if (r.status.success) {
                   
                    var getType = $("#typeSubmitOpenWell").val();

                    if (getType == 'ddrxml') {

                        var xmlLocal = $("#xmlRecordOpenWell").val();
                    

                       // var reportDDRJURNAL = $(xmlLocal).find("DM_REPORT_JOURNAL");
                        var wellData = $(xmlLocal).find("CD_WELL");

                        var spudate_normal = $(wellData).attr('spud_date');
                        var convert_spudate = spudate_normal.match(/'([^']+)'/)[1];
                        submitGeneralWellDDRINFO(wellid, moment(convert_spudate).format('MM/DD/YYYY'), xmlLocal);
                    }
                    else {
                        var xmlLocal = $("#xmlRecordOpenWell").val();
                        var getXMLDOC = $(xmlLocal).find("well");
                        console.log("spud date " + $(getXMLDOC[0]).find('dTimSpud:first').text());
                        var minimumDate = $(getXMLDOC[0]).find('dTimSpud:first').text();
                        submitGeneralWellInfo(wellid, moment(minimumDate).format('MM/DD/YYYY'), getXMLDOC);


                    }
                  
                    toastr.success(r.status.message);

                } else {
                    //toastr.error(r.status.message)
                }

            }, 'json').fail(function (r) {

                toastr.error(r.statusText);
            });
            $('#loadingModal').modal('hide');
        }

        var submitGeneralWellDDRINFO = function (well_id, spudDate, dataDDR) {

            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var data = new Object();
            data.spud_date = spudDate;
            data.id = well_id;

            var wellData = $(dataDDR).find("CD_WELL");
            var afe = $(dataDDR).find("DM_AFE");
            console.log("data DDR " + $(wellData).attr('geo_latitude'));
            data.latitude = $(wellData).attr('geo_latitude');
            data.longitude = $(wellData).attr('geo_longitude');



            if ($(wellData).attr('is_offshore') == 'N') {

                data.environment = "offshore";
            }
            else {
                data.environment = "onshore";
            }

            data.water_depth = $(wellData).attr('water_depth');
            data.afe_cost = Number($(afe).attr('afe_total')).toPrecision();



            $.post($.helper.resolveApi('~/core/Well/save'), data, function (r) {
                if (r.status.success) {
                    console.log(r);

                    submitAllDDR(well_id);
                  
                } else {
                    toastr.error(r.status.message)
                }


                $('#loadingModal').modal('hide');
            }, 'json').fail(function (r) {

                toastr.error(r.statusText);
                $('#loadingModal').modal('hide');
            });
        }


        //submit DDR
        var submitAllDDR = function (well_id) {

            var xmlLocal = $("#xmlRecordOpenWell").val();


            // var reportDDRJURNAL = $(xmlLocal).find("DM_REPORT_JOURNAL");
            var wellData = $(xmlLocal).find("CD_WELL");

            var spudate_normal = $(wellData).attr('spud_date');
            var convert_spudate = spudate_normal.match(/'([^']+)'/)[1];
            var dataDDR = $(xmlLocal).find("DM_REPORT_JOURNAL");

            if (dataDDR.length == 0) {
            }
            else {
                for (var i = 0; i < dataDDR.length; i++) {


                    var elements = dataDDR[i];
                    if (elements.getAttribute('event_code') == 'DRL' && elements.getAttribute('report_alias') == 'DDR') {

                        var drillingdate = elements.getAttribute('date_report');
                        var convertdate = drillingdate.match(/'([^']+)'/)[1];
                        var contvertdrillingdate = moment(convertdate).utc().format('MM/DD/YYYY');
                        submitDDR(well_id, contvertdrillingdate);
                    }
                   


                }
            }


        }
        var submitDDR = function (idWell, drillingDate) {

            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var data = new Object();
            data.drilling_date = drillingDate;
            data.well_id = idWell;
            $.post($.helper.resolveApi('~/core/drilling/newdrilling'), data, function (r) {
                if (r.status.success) {
                    console.log(r);
                    toastr.success(r.status.message);
                    //  window.location = $.helper.resolve("/core/drilling/detail?id=") + r.data.recordId + "&well_id=" + $well_id + "&isnew=true";

                } else {
                    toastr.error(r.status.message)
                }

                $("#inputopenwellDDR").val('');

                var dvTable = $("#dvTableOpenWellDDR");
                dvTable.children().remove();
                $('#loadingModal').modal('hide');
            }, 'json').fail(function (r) {

                toastr.error(r.statusText);
                $('#loadingModal').modal('hide');
            });
        }



        var submitGeneralWellInfo = function (well_id, spudDate, dataDDR) {

            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var data = new Object();
            data.spud_date = spudDate;
            data.id = well_id;
            console.log("data DDR " + $(dataDDR[0]).find('groundElevation:first').text());
            data.ground_elevation = $(dataDDR[0]).find('groundElevation:first').text(); 

            var locations = $(dataDDR[0]).find('location:first');

            console.log("data location " + $(dataDDR[0]).find('location:first'));

            var latitude = $(locations).find('latitude:first').text();
            console.log("data latitude " + latitude);
            var longitudde = $(locations).find('longitude:first').text();
            console.log("data longitude " + longitudde);
            data.latitude = latitude;
            data.longitude = longitudde;


            $.post($.helper.resolveApi('~/core/Well/save'), data, function (r) {
                if (r.status.success) {
                    console.log(r);
                    $("#inputOpenWell").val('');
                    
                    var dvTable = $("#dvTableOpenWell");
                    dvTable.children().remove();
                } else {
                    toastr.error(r.status.message)
                }


                $('#loadingModal').modal('hide');
            }, 'json').fail(function (r) {

                toastr.error(r.statusText);
                $('#loadingModal').modal('hide');
            });
        }


        return {
            init: function () {
                wellTypeLookup();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();

    });


});