﻿(function ($) {
    'use strict';
    var $dt_listAfe = $('#dt_listAfe');
    var $pageLoading = $('#page-loading-content');

    var pageFunction = function () {
        var $recordId = $('input[id=recordId]');


        var loadDataTable = function () {
            var dt = $dt_listAfe.cmDataTable({
                pageLength: 5,
                serverSide: false,
                ajax: {
                    url: $.helper.resolveApi("~/core/afe/rkapPlanField?bussinessUnitID=" + $recordId.val()),
                    type: "POST",
                   
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "bussines_unit_id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "field_name",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `<a href ="#" href="#" onClick="window.open('/UTC/APH/Dashboard/DashboardFieldDetail?id=` + row.field_id + `', '_blank')" class="text-bold text-info fw-700">` + row.field_name + `</a>`;
                                return output;

                            }
                            return data;
                        }, width: "10%"
                    }, 
                    { data: "total_well" },
                    { data: "plan_price" },
                    {
                        data: "field_id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `<button type="button"  class="btn btn-primary" data-target="#rkapDetailModel" data-toggle="modal" id ="` + row.field_id + `" onclick ="ActualPriceRekap(this.id)"><i class="fa fa-eye"></i></button>`;
                                return output;

                            }
                            return data;
                        }, width: "10%"
                    },
                    {
                        data: "bussines_unit_id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.bussines_unit_id + `" >
                                    <a class ="row-edit btn btn-xs btn-info btn-hover-info fa fa-external-link add-tooltip" href="#" onClick="window.open('/UTC/APH/Dashboard/DashboardFieldDetail?id=`+ row.bussines_unit_id + `', '_blank')"
                                        data-original-title="Detail" data-container="body">
                                    </a>
                       
                                </div>`;
                                return output;
                            }
                            return data;
                        }, width: "10%"
                    }
  
                ],
                initComplete: function (settings, json) {
                  
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 