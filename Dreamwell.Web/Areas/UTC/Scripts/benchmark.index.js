﻿$(document).ready(function () {
    'use strict';
    var $btnFilter = $("#btnFilter");
    var $arrwellbitdata = [];

    var $filterType = 0;
    var $chartType = 0;
    var $aphId = $("input[name=aph_id]").val();
    var $fieldId = "";

    var $benchmarktype = 0;

    var $btnView = $("#btnView");
    var $dt_listrop = $('#dt_listrop');
    var $pageLoading = $('.page-loading-content');
    var $bitcount = 0;

    var $btn_bit_record = $('#bit_record');

    var $dt_listbitrop = $('#dt_listbitrop');


    var pageFunction = function () {
        $(".filter-year").val('');
        $(".filter-year").select2({
            //dropdownParent: $form,
            placeholder: "Select Year",
        });

        var ChartTypeLookup = function (selectedId) {
            if (selectedId == "") {
                $(".chart-type").val('');
            } else {
                $(".chart-type").val(selectedId);
            }
            $(".chart-type").select2({
                //dropdownParent: $form,
                placeholder: "Select Chart Type",
                allowClear: true
            });
        }

        if ($aphId != '') {
            $("#field-lookup").button('reset');
            $("#field_id").val('');
            $("#formFilter").removeClass("d-none");
        }


        var APHLookup = function () {
            $('#btn-business-unit-lookup').click(function () {
                var btn = $(this);
                btn.button('loading');
                $("#field-lookup").button('reset');
                $("#field_id").val('');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/businessunit/Lookup',
                    success: function (data) {
                        btn.button('reset');
                        //$("#field-lookup").button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "",
                            callback: function (e) {
                                console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {
                            $box.modal('hide');
                            $aphId = event.node.id;
                            $("#formFilter").removeClass("d-none");
                            $("#btn-business-unit-lookup").text("" + event.node.text);
                        });
                    }
                });
            });
        }
        $('#field-lookup').click(function () {
            var element = $(this);
            element.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/Asset/LookupFieldByBusinessUnit?id=' + $aphId,
                success: function (data) {
                    element.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "",
                        callback: function (e) {
                            console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o, event) {

                        if (event.node.parent == '#') {
                            alert('Please double click field only');
                            return;
                        } else {
                            $box.modal('hide');
                        }
                        $fieldId = event.node.id;
                        element.html(" " + event.node.text);

                        getWellByField($fieldId);
                    });
                }
            });
        });

        var uomSelect = function () {
            $("#uom_select").cmSelect2({
                url: $.helper.resolveApi('~/core/uom/lookup/uomLength'),
                result: {
                    id: 'uom_code',
                    text: 'uom_code'
                },
                filters: function (params) {
                    return [{
                        field: "uom_code",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "Select a Unit of Measurement",
                    allowClear: true
                }
            });
        }

        var getWellByField = function (fieldid) {
            var templateScript = $("#well-template").html();
            var template = Handlebars.compile(templateScript);

            $.get($.helper.resolveApi('~/core/well/' + fieldid + '/detailByField'), function (r) {
                //console.log(r);
                if (r.status.success) {
                    $("#groupWell").show();
                    if (r.data.length > 0) {
                        if ($("#field-" + r.data[0].field_id).length == 0) {
                            $("#listWell").append(template({ data: r.data }));
                            $('.remove-field').on('click', function () {
                                $("#field-" + $(this).data("fieldid")).remove();
                            });
                            $('.select-all').on('click', function () {
                                $("#listWell").find(".custom-control-input").prop("checked", true);
                            });
                        } else {
                            toastr.error("Field is Available on the list.");
                        }
                    } else {
                        $("#field-lookup").button('reset');
                        toastr.error("No Well Available on this Field.");
                    }
                } else {
                    $("#groupWell").hide();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        $("select[name='uom_code[]']").on('change', function () {
            getPlanVsActualDepth($(this).val());
        });

        async function getPlanVsActualDepth(uom) {

            $("#depth-benchmark").show();
            var aryWell = [];
            //var aryuomSelect = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });

            if (uom == undefined) {
                var data = new Object();
                data.well_id = new Object();
                data.well_id = aryWell;
                data.field_id = $fieldId;
                data.uom_select = ["ft"];
            }
            else {
                var data = new Object();
                data.well_id = new Object();
                data.well_id = aryWell;
                data.field_id = $fieldId;
                data.uom_select = uom;
            }


            if ($chartType == 3) {
                var html = `<div class="text-center">Not available to showing Depth Benchmark with Pie Chart</div>`
                $("#depth-chart").html(html);
                return;
            }

            await $.post($.helper.resolveApi("~/UTC/Benchmark/getPlanVsActualDepth"), data, function (r) {
                //var chart = new Highcharts.Chart();
                if (r.status.success && r.data.length > 0) {
                    var plan = [];
                    var actual = [];
                    var wellName = [];

                    $.each(r.data, function (index, value) {
                        plan.push(value.planned_td);
                        actual.push(value.actual_depth);
                        wellName.push(value.well_name);
                    });
                    var setChart = [];
                    var setPlot = [];
                    if ($chartType == 1) {
                        setChart = {
                            renderTo: 'depth-chart',
                            //margin: [50, 10, 60, 65],
                            type: 'column',
                            //height: 400,
                        };

                        setPlot = {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                },
                            }
                        };
                    } else if ($chartType == 2) {
                        setChart = {
                            renderTo: 'depth-chart',
                            type: '',
                        };

                        setPlot = {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                            }
                        };
                    }

                    var chart = new Highcharts.Chart({
                        chart: setChart,
                        title: {
                            text: 'Depth Benchmark'
                        },
                        subtitle: {
                            text: ''
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Depth',
                                align: 'middle'
                            },
                            labels: {
                                overflow: 'justify'
                            }
                        },
                        xAxis: {
                            categories: wellName,
                            title: {
                                text: null
                            }
                        },
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            x: 0,
                            y: 10
                        },
                        plotOptions: setPlot,
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.y} ' + data.uom_select + '</b><br/>',
                        },
                        series: [
                            {
                                name: "Plan",
                                data: plan
                            },
                            {
                                name: "Actual",
                                data: actual
                            }
                        ],
                        credits: {
                            enabled: false
                        },

                    });
                } else if (r.status.success && r.data.length <= 0) {
                    $("#depth-bar").html("No Record Available");
                } else {
                    toastr.error(r.data.message)
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        async function getTotalWell() {
            $("#totalwell-benchmark").show();
            var aryWellType = [];

            $.each($("input[name='welltype[]']:checked"), function () {
                aryWellType.push($(this).val());
            });

            if ($(".filter-year").val() == "") {
                toastr.error("Please choose year to show.");
                return;
            }

            var data = new Object();
            data.well_id = new Object();
            data.well_year = $(".filter-year").val().sort();
            data.business_unit_id = $aphId;
            data.field_id = $fieldId;
            data.well_status = aryWellType;

            //if ($chartType == 3) {
            //    var html = `<div class="text-center">Not available to showing Drilled Days Benchmark with Pie Chart</div>`
            //    $("#drilledDays-chart").html(html);
            //    return;
            //}

            await $.post($.helper.resolveApi("~/UTC/Benchmark/getTotalWell"), data, function (r) {
                if (r.status.success) {
                    if (r.data == null) {
                        $("#totalwell-benchmark .row-data").hide();
                        $("#totalwell-benchmark .no-data").show();
                        return;
                    } else if (r.data.length > 0) {
                        $("#totalwell-benchmark .row-data").show();
                        $("#totalwell-benchmark .no-data").hide();
                    }

                    var putraData = [];
                    var totalwell = 0;
                    if (!isNaN(r.status.message)) {
                        totalwell = parseInt(r.status.message);
                    }

                    $.each(r.data, function (index, value) {
                        $.each(value.field, function (index, valuefield) {
                            if (putraData.length == 0) {
                                putraData.push({
                                    name: valuefield.field_name,
                                    data: []
                                });
                            } else {
                                var isExist = false;
                                for (var i = 0; i < putraData.length; i++) {
                                    if (valuefield.field_name == putraData[i].name) {
                                        isExist = true;
                                    }
                                }
                                if (!isExist) {
                                    putraData.push({
                                        name: valuefield.field_name,
                                        data: []
                                    });
                                }
                            }
                        });
                    });

                    $.each(r.data, function (index, value) {
                        $.each(value.field, function (index, valuefield) {
                            var aryWell = {
                                x: value.year,
                                y: valuefield.total_well
                            }

                            for (var i = 0; i < putraData.length; i++) {
                                if (valuefield.field_name == putraData[i].name) {
                                    putraData[i].data.push(aryWell);
                                }
                            }
                        });
                    });

                    //console.log("aryData Row");
                    //console.log(putraData);

                    var setChart = [];
                    var setPlot = [];
                    if ($chartType == 1) {
                        setChart = {
                            renderTo: 'totalwell-chart',
                            type: 'column',
                        };

                        setPlot = {
                            //series: a,
                            //column: {
                            //    // shared options for all column series
                            //}
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        };
                    } else if ($chartType == 2) {
                        setChart = {
                            renderTo: 'drilledDays-chart',
                            type: '',
                        };

                        setPlot = {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                            }
                        };
                    }

                    var chart = new Highcharts.Chart({
                        chart: setChart,
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: 'Total Sumur ' + totalwell,
                            align: "right",
                            x: -100
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: ''
                            },
                            stackLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                }
                            }
                        },
                        legend: {
                            align: 'right',
                            //x: -70,
                            verticalAlign: 'middle',
                            layout: 'vertical',
                            y: 20,
                            //floating: true,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            shadow: false
                        },
                        tooltip: {
                            formatter: function () {
                                return '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '<br/>' +
                                    'Total: ' + this.point.stackTotal;
                            }
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                            }
                        },
                        series: putraData,
                        credits: {
                            enabled: false
                        },

                    });
                }
            }, 'json').fail(function (r) {
                console.log("ini data well",r);
            });
        };

        async function getPlanVsActualDrilledDays() {
            $("#drilledDays-benchmark").show();
            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });
            var data = new Object();
            data.well_id = new Object();
            data.well_id = aryWell;
            data.field_id = $fieldId;

            if ($chartType == 3) {
                var html = `<div class="text-center">Not available to showing Drilled Days Benchmark with Pie Chart</div>`
                $("#drilledDays-chart").html(html);
                return;
            }

            await $.post($.helper.resolveApi("~/UTC/Benchmark/getPlanVsActualDrilledDays"), data, function (r) {
                if (r.status.success) {
                    var plan = [];
                    var actual = [];
                    var wellName = [];

                    $.each(r.data, function (index, value) {
                        plan.push(value.planned_days);
                        actual.push(value.actual_days);
                        wellName.push(value.well_name);
                    });

                    var setChart = [];
                    var setPlot = [];
                    if ($chartType == 1) {
                        setChart = {
                            renderTo: 'drilledDays-chart',
                            type: 'column',
                        };

                        setPlot = {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                },
                            }
                        };
                    } else if ($chartType == 2) {
                        setChart = {
                            renderTo: 'drilledDays-chart',
                            type: '',
                        };

                        setPlot = {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                            }
                        };
                    }

                    var chart = new Highcharts.Chart({
                        chart: setChart,
                        title: {
                            text: 'Drilled Days Benchmark'
                        },
                        subtitle: {
                            text: ''
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Depth',
                                align: 'middle'
                            },
                            labels: {
                                overflow: 'justify'
                            }
                        },
                        xAxis: {
                            categories: wellName,
                            title: {
                                text: null
                            }
                        },
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            x: 0,
                            y: 10
                        },
                        plotOptions: setPlot,
                        tooltip: {
                            pointFormat: '{series.name}: <b>{point.y:.2f}</b><br/>',
                        },
                        series: [
                            {
                                name: "Plan",
                                data: plan
                            },
                            {
                                name: "Actual",
                                data: actual
                            }
                        ],
                        credits: {
                            enabled: false
                        },

                    });
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        async function getTotalRig() {
            $("#rig-benchmark").show();
            var aryWellType = [];

            $.each($("input[name='welltype[]']:checked"), function () {
                aryWellType.push($(this).val());
            });

            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });

            var data = new Object();
            data.well_id = aryWell;
            data.business_unit_id = $aphId;
            data.field_id = $fieldId;
            data.well_status = aryWellType;

            if ($chartType == 3) {
                var html = `<div class="text-center">Not available to showing Drilled Days Benchmark with Pie Chart</div>`
                $("#rig-chart").html(html);
                return;
            }

            await $.post($.helper.resolveApi("~/UTC/Benchmark/getRigFiltering"), data, function (r) {
                if (r.status.success) {

                    if (r.data == null || r.data.length == 0) {
                        $("#rig-benchmark .row-data").hide();
                        $("#rig-benchmark .no-data").show();
                        return;
                    } else if (r.data.length > 0) {
                        $("#rig-benchmark .row-data").show();
                        $("#rig-benchmark .no-data").hide();
                    }

                    var xAxisCategories = [];
                    var plannedDays = [];
                    var movingDays = [];
                    var actualDays = [];
                    var dol = [];
                    //-- add Array Field
                    $.each(r.data, function (index, value) {
                        console.log('value');
                        console.log('ini dol',value.dol);
                        xAxisCategories.push(value.contractor_name);
                        plannedDays.push(value.planned_days);
                        actualDays.push(value.actual_days);
                        dol.push(value.dol);
                        if (value.planned_days > value.actual_days)
                            movingDays.push(value.planned_days - value.actual_days);
                        else
                            movingDays.push(0);


                        console.log(movingDays, value.actual_days - value.planned_days, value.actual_days, value.planned_days);
                    });
                    $('#rig-chart').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Rig Utilizationasional'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            categories: xAxisCategories,
                            crosshair: true
                        },
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            x: 0,
                            y: 10,
                            shadow: false,
                        },
                        yAxis: {
                            labels: {
                                format: '{value} days',
                                style: {
                                    //color: Highcharts.getOptions().colors[0]
                                }
                            },
                            title: {
                                text: 'Days',
                                style: {
                                    //color: Highcharts.getOptions().colors[0]
                                }
                            },
                        },
                        tooltip: {
                            shared: true,
                            formatter: function () {
                                var contract_name = this.x;
                                var result = "<b>" + this.x + "</b><br />";
                                $.each(this.points, function (index, value) {
                                    result += "<br /><b>" + value.series.name + ":<b/> " + value.y + " days";
                                });

                                result += "<br/><br/><br />";
                                $.each(r.data, function (index, value) {
                                    if (value.contractor_name == contract_name) {
                                        //if (value.planned_days > 0)
                                        //    result += "<br/><span><b>planned days:</b> " + value.planned_days + "days </span>";
                                        if (value.total_well > 0)
                                            result += "<br/><span><b>total well:</b> " + value.total_well + "</span>";
                                        if (value.original_well > 0)
                                            result += "<br/><span><b>original well:</b> " + value.original_well + "</span>";
                                        if (value.side_track > 0)
                                            result += "<br/><span><b>side track:</b> " + value.side_track + "</span>";
                                        if (value.work_over > 0)
                                            result += "<br/><span><b>work over:</b> " + value.work_over + "</span>";
                                        if (value.well_services > 0)
                                            result += "<br/><span><b>well services:</b> " + value.well_services + "</span>";
                                    }
                                });


                                return result;
                            }
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: 'Days On Location',
                            data: dol,
                            color: '#00C7FF',

                        }, {
                            name: 'Operation Days',
                            data: actualDays,
                            color: '#ff0000',
                        }]
                    });

                    //$('#rig-chart').highcharts({
                    //    chart: {
                    //        type: 'column'
                    //    },
                    //    title: {
                    //        text: 'Rig Utilization'
                    //    },
                    //    subtitle: {
                    //        text: ''
                    //    },
                    //    legend: {
                    //        align: 'center',
                    //        verticalAlign: 'bottom',
                    //        x: 0,
                    //        y: 10
                    //    },
                    //    yAxis: {

                    //        labels: {
                    //            format: '{value} days',
                    //            style: {
                    //                //color: Highcharts.getOptions().colors[0]
                    //            }
                    //        },
                    //        title: {
                    //            text: 'Days',
                    //            style: {
                    //                //color: Highcharts.getOptions().colors[0]
                    //            }
                    //        },
                    //    },
                    //    xAxis: {
                    //        categories: xAxisCategories,
                    //        crosshair: true,
                    //        labels: {
                    //            style: {
                    //                fontSize: '10px',
                    //                fontFamily: 'Verdana, sans-serif'
                    //            }
                    //        }
                    //    },
                    //    credits: {
                    //        enabled: false
                    //    },
                    //    legend: {
                    //        shadow: false,
                    //        //reversed: true
                    //    },
                    //    tooltip: {
                    //        shared: true,
                    //        formatter: function () {
                    //            //console.log(this);
                    //            var contract_name = this.x;
                    //            var result = "<b>" + this.x + "</b><br />";
                    //            $.each(this.points, function (index, value) {
                    //                result += "<br /><b>" + value.series.name + ":<b/> " + value.y + " days";
                    //            });

                    //            result += "<br/><br/><br />";
                    //            $.each(r.data, function (index, value) {
                    //                //console.log(value.contractor_name + " - " + contract_name);
                    //                if (value.contractor_name == contract_name) {
                    //                    if (value.planned_days > 0)
                    //                        result += "<br/><span><b>Planned Days:</b> " + value.planned_days + "Days </span>";
                    //                    if (value.total_well > 0)
                    //                        result += "<br/><span><b>Total Well:</b> " + value.total_well + "</span>";
                    //                    if (value.original_well > 0)
                    //                        result += "<br/><span><b>Original Well:</b> " + value.original_well + "</span>";
                    //                    if (value.side_track > 0)
                    //                        result += "<br/><span><b>Side Track:</b> " + value.side_track + "</span>";
                    //                    if (value.work_over > 0)
                    //                        result += "<br/><span><b>Work Over:</b> " + value.work_over + "</span>";
                    //                    if (value.well_services > 0)
                    //                        result += "<br/><span><b>Well Services:</b> " + value.well_services + "</span>";
                    //                }
                    //            });


                    //            return result;
                    //            //return '<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '<br/>' +
                    //            //    'Total: ' + this.point.stackTotal;
                    //        }
                    //    },
                    //    plotOptions: {
                    //        //column: {

                    //        //}
                    //        column: {
                    //            stacking: 'normal',
                    //            grouping: false,
                    //            shadow: false,
                    //            borderWidth: 0
                    //        }
                    //    },
                    //    //series: [
                    //    //    {
                    //    //        name: 'Moving Days',
                    //    //        asd: "123",
                    //    //        color: '#FF0000',
                    //    //        data: movingDays,
                    //    //        pointWidth: 100,
                    //    //        pointPlacement: 0
                    //    //    },
                    //    //    {
                    //    //        name: 'Actual Days',
                    //    //        asd: "321",
                    //    //        color: '#00C7FF',
                    //    //        data: actualDays,
                    //    //        pointWidth: 100,
                    //    //        pointPlacement: 0
                    //    //    }]
                    //    series: [
                    //        {
                    //            name: 'Operation Days',
                    //            color: '#FF0000',
                    //            data: [10,8],
                    //            pointWidth: 100,
                    //            pointPlacement: 0
                    //        },
                    //        {
                    //            name: 'Days On Location',
                    //            color: '#00C7FF',
                    //            data: [5,4],
                    //            pointWidth: 100,
                    //            pointPlacement: 0
                    //        }]
                    //});
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        async function getPlanVsActual() {
            $("#planVsActual-benchmark").show();
            var aryWellType = [];

            $.each($("input[name='welltype[]']:checked"), function () {
                aryWellType.push($(this).val());
            });

            var arywellId = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                arywellId.push($(this).val());
            });

            var data = new Object();
            data.well_id = arywellId;
            data.business_unit_id = $aphId;
            data.field_id = $fieldId;
            data.well_status = aryWellType;


            await $.post($.helper.resolveApi("~/UTC/Benchmark/getPlanVsCostFiltering"), data, function (r) {

                if (r.status.success) {
                    if (r.data == null || r.data.length == 0) {
                        $("#planVsActual-benchmark .row-data").hide();
                        $("#planVsActual-benchmark .no-data").show();
                        return;
                    } else if (r.data.length > 0) {
                        $("#planVsActual-benchmark .row-data").show();
                        $("#planVsActual-benchmark .no-data").hide();
                    }

                    var putraData = [];
                    //-- add Array Field
                    $.each(r.data, function (index, value) {
                        if (putraData.length == 0) {
                            putraData.push({
                                name: value.field_name,
                                data: []
                            });
                        } else {
                            var isExist = false;
                            for (var i = 0; i < putraData.length; i++) {
                                if (value.field_name == putraData[i].name) {
                                    isExist = true;
                                }
                            }
                            if (!isExist) {
                                putraData.push({
                                    name: value.field_name,
                                    data: []
                                });
                            }
                        }
                    });


                    var aryColor = [];
                    for (var i = 0; i < 100; i++) {
                        aryColor.push("#" + Math.floor(Math.random() * 16777215).toString(16));
                    }

                    //-- Grouping Data
                    $.each(r.data, function (index, value) {
                        for (var i = 0; i < putraData.length; i++) {
                            if (value.field_name == putraData[i].name) {
                                if (putraData[i].data.length == 0) {
                                    putraData[i].data.push(value.planned_md);
                                    putraData[i].data.push(value.actual_md);
                                    putraData[i].data.push(value.planned_days);
                                    putraData[i].data.push(value.actual_days);
                                    putraData[i].data.push(value.planned_cost);
                                    putraData[i].data.push(value.actual_cost);
                                } else {
                                    putraData[i].data[0] = putraData[i].data[0] + value.planned_md;
                                    putraData[i].data[1] = putraData[i].data[1] + value.actual_md;
                                    putraData[i].data[2] = putraData[i].data[2] + value.planned_days;
                                    putraData[i].data[3] = putraData[i].data[3] + value.actual_days;
                                    putraData[i].data[4] = putraData[i].data[4] + value.planned_cost;
                                    putraData[i].data[5] = putraData[i].data[5] + value.actual_cost;
                                }
                            }
                        }
                    });


                    var series = [];
                    var indexcolor = 0;
                    $.each(putraData, function (index, value) {
                        series.push(
                            {
                                name: 'Planned',
                                data: [
                                    [0, value.data[0]]
                                ],
                                color: '#457b9d',
                                yAxis: 0,
                                stack: 0
                            }, {
                                name: 'Actual',
                                data: [
                                    [0, value.data[1]]
                                ],
                                color: '#1d3557',
                                yAxis: 0,
                        }, {
                                name: 'Planned',
                                data: [
                                    [1, value.data[2]]
                                ],
                                color: '#f4a261',
                                yAxis: 1,
                                stack: 0
                        }, {
                                name: 'Actual',
                                data: [
                                    [1, value.data[3]]
                                ],
                                color: '#e76f51',
                                yAxis: 1,
                        }, {
                                name: 'Planned',
                                data: [
                                    [2, value.data[4]]
                                ],
                                color: '#76c893',
                                yAxis: 2,
                                stack: 0
                        }, {
                            name: 'Actual',
                            data: [
                                [2, value.data[5]]
                            ],
                                color: '#52b69a',
                                yAxis: 2,
                        }
                        );
                        indexcolor++;
                    });

                    var chart = {
                        type: 'column'
                    };

                    var title = {
                        text: putraData[0].name
                    };
                    var xAxis = {
                        categories: ["Depth", "Days", "Cost"]
                    };
                    var yAxis = [
                        {
                            labels: {
                                format: '{value} ft',
                                style: {
                                    color: Highcharts.getOptions().colors[2]
                                }
                            },
                            title: {
                                text: 'Depth',
                                style: {
                                    color: Highcharts.getOptions().colors[2]
                                }
                            },
                        },
                        {
                            title: {
                                text: 'Days',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value} days',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            }
                        },
                        {
                            title: {
                                text: 'COST (USD)',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            labels: {
                                format: 'USD {value}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            opposite: true
                        }
                    ];
                    var legend = {
                        enabled : false,
                        align: 'center',
                        verticalAlign: 'bottom',
                        layout: 'horizontal',
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                        borderColor: '#CCC',
                        borderWidth: 1,
                        shadow: false
                    };
                    var tooltip = {
                        formatter: function () {
                            return '<b>' + this.x + '</b><br/>' +
                                this.series.name + ': ' + this.y
                                /*+ '<br/>' +
                                'Total: ' + this.point.stackTotal;*/
                        }
                    };
                    var plotOptions = {
                        column: {
                            stacking: 'normal'
                        }
                    };
                    var credits = {
                        enabled: false
                    };

                    var json = {};
                    json.chart = chart;
                    json.title = title;
                    json.xAxis = xAxis;
                    json.yAxis = yAxis;
                    json.legend = legend;
                    json.tooltip = tooltip;
                    json.plotOptions = plotOptions;
                    json.credits = credits;
                    json.series = series;

                    console.log(json)

                    $('#planVsActual-chart').highcharts(json);
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        async function getCummulativeCost() {
            $("#costPerDepth-benchmark").show();
            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });
            var data = new Object();
            data.well_id = new Object();
            data.well_id = aryWell;
            data.field_id = $fieldId;


            if ($chartType == 3) {
                var html = `<div class="text-center">Not available to showing Cummulative Cost Benchmark with Pie Chart</div>`
                $("#cummulativeCost-chart").html(html);
                return;
            }

            await $.post($.helper.resolveApi("~/UTC/Benchmark/getCummulativeCost"), data, function (r) {
                if (r.status.success) {
                    var plan = [];
                    var actual = [];
                    var wellName = [];

                    if (r.data.length > 0) {

                        $.each(r.data, function (index, value) {
                            plan.push(value.afe_cost);
                            actual.push(value.total_cost);
                            wellName.push(value.well_name);
                        });
                        var setChart = [];
                        var setPlot = [];
                        if ($chartType == 1) {
                            setChart = {
                                renderTo: 'cummulativeCost-chart',
                                //margin: [50, 10, 60, 65],
                                type: 'column',
                                //height: 400,
                            };

                            setPlot = {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                }
                            };
                        } else if ($chartType == 2) {
                            setChart = {
                                renderTo: 'cummulativeCost-chart',
                                type: '',
                            };

                            setPlot = {
                                line: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                }
                            };
                        }

                        var chart = new Highcharts.Chart({
                            chart: setChart,
                            title: {
                                text: 'Cummulative Cost Benchmark'
                            },
                            subtitle: {
                                text: ''
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Cost ($)',
                                    align: 'middle'
                                },
                                labels: {
                                    overflow: 'justify'
                                }
                            },
                            xAxis: {
                                categories: wellName,
                                title: {
                                    text: null
                                }
                            },
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 10
                            },
                            plotOptions: setPlot,
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.y:.2f}</b><br/>',
                            },
                            series: [
                                {
                                    name: "Plan",
                                    data: plan
                                },
                                {
                                    name: "Actual",
                                    data: actual
                                }
                            ],
                            credits: {
                                enabled: false
                            },
                        });
                    } else {
                        $("#cummulativeCost-bar").html("No Record Available");
                    }
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        async function GetIadc() {
            $("#iadc-benchmark").show();
            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });
            var data = new Object();
            data.well_id = new Object();
            data.well_id = aryWell;
            data.field_id = $fieldId;

            if ($chartType == 2) {
                var html = `<div class="text-center">Not available to showing PT/PT Benchmark with Line Chart</div>`
                $("#iadc-chart").html(html);
                return;
            }

            await $.post($.helper.resolveApi("~/UTC/Benchmark/GetWellIadc"), data, function (r) {
                if (r.status.success) {
                    var aryPT = [];
                    var aryNPT = [];
                    var aryPercentagePT = [];
                    var aryPercentageNPT = [];
                    var wellName = [];
                    if (r.data.length > 0) {
                        if ($chartType == 1) {
                            $.each(r.data, function (index, value) {
                                wellName.push(value.well_name);
                                if (parseInt(value.pt_interval_hours) + parseInt(value.npt_interval_hours) > 0) {
                                    var percentage_pt = (value.pt_interval_hours / (value.pt_interval_hours + value.npt_interval_hours)) * 100;
                                    var percentage_npt = (value.npt_interval_hours / (value.pt_interval_hours + value.npt_interval_hours)) * 100;
                                    aryPercentagePT.push(parseFloat(percentage_pt));
                                    aryPercentageNPT.push(parseFloat(percentage_npt));

                                    aryPT.push(parseFloat(value.pt_interval_hours));
                                    aryNPT.push(parseFloat(value.npt_interval_hours));
                                } else {
                                    aryPercentagePT.push(0);
                                    aryPercentageNPT.push(0);

                                    aryPT.push(parseFloat(0));
                                    aryNPT.push(parseFloat(0));
                                }
                            });

                            var chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'iadc-chart',
                                    margin: [50, 10, 60, 65],
                                    type: 'column',
                                    height: 400,
                                },
                                title: {
                                    text: 'PT / NPT'
                                },
                                xAxis: {
                                    categories: wellName
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: ''
                                    },
                                    stackLabels: {
                                        enabled: false,
                                        style: {
                                            fontWeight: 'bold',
                                        }
                                    }
                                },
                                series: [
                                    {
                                        name: "PT",
                                        wells: wellName,
                                        hours: aryPT,
                                        data: aryPercentagePT
                                    },
                                    {
                                        name: "NPT",
                                        wells: wellName,
                                        hours: aryNPT,
                                        data: aryPercentageNPT
                                    }
                                ],
                                tooltip: {
                                    formatter: function () {
                                        var point = this.point;
                                        point.y2 = this.series.options.data[point.index];

                                        return '<b>' + this.series.options.wells[point.index] + '<b/><br/><b>' + this.series.options.name + '<b/><br/>Percentage: ' + Highcharts.numberFormat(point.y2, 2) + '%<br/>Hours: ' + this.series.options.hours[point.index];
                                    }
                                },
                                plotOptions: {
                                    column: {
                                        stacking: 'normal',
                                    }
                                },
                                credits: {
                                    enabled: false
                                },
                            });
                        } else if ($chartType == 3) {
                            var _well = [];
                            $.each(r.data, function (index, value) {
                                _well = {
                                    name: value.well_name,
                                    y: value.npt_interval_hours
                                };

                                wellName.push(_well);
                            });

                            var setChart = {
                                renderTo: 'iadc-chart',
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie',
                            };

                            var chart = new Highcharts.Chart({
                                chart: setChart,
                                title: {
                                    text: 'NPT'
                                },
                                tooltip: {
                                    pointFormat: '<b>{point.percentage:.1f} Hrs</b>'
                                },
                                accessibility: {
                                    point: {
                                        valueSuffix: '%'
                                    }
                                },
                                plotOptions: {
                                    pie: {
                                        allowPointSelect: true,
                                        cursor: 'pointer',
                                        dataLabels: {
                                            enabled: true,
                                            format: '<b>{point.name}</b>: {point.percentage:.1f}%'
                                        }
                                    }
                                },
                                tooltip: {
                                    pointFormat: '<b>Total NPT: {point.y:.2f} Hrs</b><br/>',
                                },
                                series: [{
                                    data: wellName
                                }],
                                credits: {
                                    enabled: false
                                },
                            });
                        }
                    } else {
                        $("#ptAndNptHours-bar").html("No Record Available");
                    }
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        async function getRopData() {
            $("#listWellBit").html("");

            $("#chkBitsize").hide();
            $("#cbBitsize").hide();
            $('#dtrop').hide();


            var $ViewType = $('.rop-type').val();

            if ($ViewType == 1) {
                $("#chkBitsize").show();
                $('#dtrop').show();

                getWellBit();
            }
            else if ($ViewType == 2) {
                $('.ropgroup').show();
                $("#cbBitsize").show();
                $('#dtrop').hide();

                var aryWell = [];
                $.each($("input[name='wellId[]']:checked"), function () {
                    aryWell.push($(this).val());
                });
                bitSizeSelect(aryWell);
            }


            //getRopGrid();
            //getRopChart();
        }

        async function getRopChart() {
            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });
            var data = new Object();
            data.well_id = new Object();
            data.well_id = aryWell;
            data.field_id = $fieldId;

            await $.post($.helper.resolveApi("~/UTC/Benchmark/getRop"), data, function (r) {
                if (r.status.success) {
                    $("#rop-benchmark").show();
                    var coloumns = [];
                    var rop = ["ROP"];
                    var wellName = [];
                    if (r.data.length > 0) {
                        $.each(r.data, function (index, value) {
                            rop.push(value.rop);
                            wellName.push(value.well_name);
                        });

                        //coloumns.push(rop);

                        //console.log(coloumns);
                        c3.generate(
                            {
                                bindto: "#rop-bar",
                                data:
                                {
                                    columns: coloumns,
                                    type: 'bar',
                                    groups: [
                                        ['ROP']
                                    ]
                                },
                                grid: {
                                    y: {
                                        lines: [{ value: 0 }]
                                    }
                                },
                                axis: {
                                    x: {
                                        type: 'category',
                                        categories: wellName
                                    }
                                }
                            });
                    } else {
                        $("#rop-bar").html("No Record Available");
                    }
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        async function getCostPerFeet() {
            $("#costperfeet-benchmark").show();
            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });
            var data = new Object();
            data.well_id = new Object();
            data.well_id = aryWell;
            data.field_id = $fieldId;


            if ($chartType == 3) {
                var html = `<div class="text-center">Not available to showing Cost Per Feet Benchmark with Pie Chart</div>`
                $("#costperfeet-chart").html(html);
                return;
            }

            await $.post($.helper.resolveApi("~/UTC/Benchmark/getCostPerDepth"), data, function (r) {
                if (r.status.success) {
                    //console.log(r);

                    var plan = [];
                    var actual = [];
                    var wellName = [];
                    if (r.data.length > 0) {

                        $.each(r.data, function (index, value) {
                            if (parseInt(value.afe_cost) > 0 || parseInt(value.afe_cost) > 0) {
                                plan.push(parseInt(value.afe_cost) / parseInt(value.planned_td));
                            } else {
                                plan.push(0);
                            }
                            if (parseInt(value.total_cost) > 0 || parseInt(value.measure_depth) > 0) {
                                actual.push(parseInt(value.total_cost) / parseInt(value.measure_depth));
                            } else {
                                actual.push(0);
                            }
                            wellName.push(value.well_name);
                        });
                        var setChart = [];
                        var setPlot = [];
                        if ($chartType == 1) {
                            setChart = {
                                renderTo: 'costperfeet-chart',
                                //margin: [50, 10, 60, 65],
                                type: 'column',
                                //height: 400,
                            };

                            setPlot = {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                }
                            };
                        } else if ($chartType == 2) {
                            setChart = {
                                renderTo: 'costperfeet-chart',
                                type: '',
                            };

                            setPlot = {
                                line: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                }
                            };
                        }


                        var chart = new Highcharts.Chart({
                            chart: setChart,
                            title: {
                                text: 'Cost Per Feet Benchmark'
                            },
                            subtitle: {
                                text: ''
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Cost/ft',
                                    align: 'middle'
                                },
                                labels: {
                                    overflow: 'justify'
                                }
                            },
                            xAxis: {
                                categories: wellName,
                                title: {
                                    text: null
                                }
                            },
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 10
                            },
                            plotOptions: setPlot,
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.y:.2f}</b><br/>',
                            },
                            series: [
                                {
                                    name: "Plan",
                                    data: plan
                                },
                                {
                                    name: "Actual",
                                    data: actual
                                }
                            ],
                            credits: {
                                enabled: false
                            },
                        });

                    } else {
                        $("#wob-bar").html("No Record Available");
                    }
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        $("input[name='benchmarktype[]']").change(function () {
            $benchmarktype = $(this).val();

            if ($(this).val() == "7") {
                $("#filtering-field").addClass("d-none");
                $("#wellYear").removeClass("dp-none");
            }
            else {
                $("#filtering-field").removeClass("d-none");
                $("#wellYear").addClass("dp-none");
            }

            benchmarktypeChange($(this).val());
        });

        var benchmarktypeChange = function (ival) {
            $('#wellChartType').show();
            $('.ropgroup').hide();
            $('#RopType').hide();
            $("#ropbit-benchmark").hide();
            $("#dtbitrop").hide();

            if (ival == 5) {
                $('#wellChartType').hide();
                $('#RopType').show();
            }
        }

        $btnFilter.on('click', function () {
            $bitcount = 0;

            $("#ropbit-benchmark").hide();
            $("#dtbitrop").hide();

            var arywellId = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                arywellId.push($(this).val());
            });

            bit_record(arywellId);

            var isValid = true;
            $chartType = $('.chart-type').val();

            if ($benchmarktype != 5) {
                if ($chartType == null) {
                    toastr.error("Please choose Chart Type for details");
                    isValid = false;
                }
            }

            if (!$fieldId && $fieldId == '' && $benchmarktype != 7) {
                toastr.error("Please choose Field for details");
                isValid = false;
            }

            if (!$aphId && $aphId == '') {
                toastr.error("Please choose APH for details");
                isValid = false;
            }

            $(".panel-report").hide();

            var totalChecked = $('input[name="benchmarktype[]"]:checked').length;
            if (totalChecked <= 0) {
                toastr.error("Please select parameter to show details");
                isValid = false;
            }

            if (!isValid)
                return;

            if (totalChecked > 0) {
                $.each($("input[name='benchmarktype[]']:checked"), function () {
                    $filterType = $(this).val();
                    if ($filterType == 1)
                        getPlanVsActualDepth();
                    else if ($filterType == 2)
                        getPlanVsActualDrilledDays();
                    else if ($filterType == 3)
                        getCummulativeCost();
                    else if ($filterType == 4)
                        GetIadc();
                    else if ($filterType == 5)
                        getRopData();
                    else if ($filterType == 6)
                        getCostPerFeet();
                    else if ($filterType == 7)
                        getTotalWell();
                    else if ($filterType == 8)
                        getTotalRig();
                    else if ($filterType == 9)
                        getPlanVsActual();
                });
            } else {
                getPlanVsActualDepth();
                getPlanVsActualDrilledDays();
                getCummulativeCost();
                GetIadc();
                getRopData();
                getCostPerFeet();
            }
        });

        var generateNewArrBitSize = function(xs) {
            var key = 'bit_size';
            return xs.reduce(function(rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
            }, {});
        };


        var getWellBit = function () {
            var templateScript = $("#wellbit-template").html();
            var template = Handlebars.compile(templateScript);

            //var alist = [];
            //for (var i = 0; i < 5; i++) {
            //    var listdata = {
            //        id: i
            //    };
            //    alist.push(listdata);
            //}
            //if (alist.length > 0) {
            //    $("#listWellBit").append(template({ data: alist }));
            //}

            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });


            $arrwellbitdata = [];
            if (aryWell.length > 0) {
                for (var i = 0; i < aryWell.length; i++) {
                    $arrwellbitdata[i] = [];
                    $.get($.helper.resolveApi('~/UTC/Benchmark/getWellBit/' + aryWell[i]), function (r) {
                        if (r.status.success) {
                            console.log(r.data);
                            if (r.data.databit.length > 0) {
                                $bitcount++;
                                var arrNewBit = [];
                                var arrBitSize = [];
                                var arrData = [];
                                // ubah bentuk array jadi uniq dan hapus titik di bit size untuk selector
                                for (var t = 0; t < r.data.databit.length; t++) {
                                    var databit = r.data.databit[t];
                                    if(arrBitSize.some(BitSize => BitSize.bit_size === databit.bit_size)){
                                        var objData = {
                                            'id':databit.id,
                                            'bit_size':databit.bit_size,
                                            'bit_sizeStr':databit.bit_size.toString().replace('.',''),
                                        }
                                        arrData.push(objData);
                                    }else{
                                        var objData = {
                                            'id':databit.id,
                                            'bit_size':databit.bit_size,
                                            'bit_sizeStr':databit.bit_size.toString().replace('.',''),
                                        }
                                        arrData.push(objData);

                                        var obj = {
                                            'id':databit.id,
                                            'bit_size':databit.bit_size,
                                            'bit_sizeStr':databit.bit_size.toString().replace('.',''),
                                        }
                                        arrBitSize.push(obj);
                                    }
                                }
                                $("#listWellBit").append(template({ arrbit: arrBitSize, data: arrData, welldata: r.data.datawell }));
                            }
                        }

                        if ($bitcount > 0) {
                            $('.ropgroup').show();
                        }
                        else {
                            $('.ropgroup').hide();
                        }
                    });
                }
            } else {
                $('.ropgroup').hide();
            }
        }


        $btnView.on('click', function () {
            var aryWellBit = [];
            var aryBitSize = [];

            // $.each($("input[name='wellbitId[]']:checked"), function () {
            //     aryWellBit.push($(this).val());
            // });

            $.each($("input[name='wellbitId[]']:checked"), function () {
                var bitsize = $(this).val();
                console.log('.selectedbitsize-'+bitsize);
                $(this).parent().parent().find('.selectedbitsize-'+bitsize).each(function( index ) {
                    console.log($(this).val());
                    aryWellBit.push($(this).val());
                });
            });

            console.log(aryWellBit)

            var arywelltype = [];
            $.each($("input[name='welltype[]']:checked"), function () {
                arywelltype.push($(this).val());
            });

            var arywellId = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                arywellId.push($(this).val());
            });

            var $filteryear = $("#filteryear").val();

            var $ViewType = $('.rop-type').val();



            if ($ViewType == 1) {
                var data = {
                    business_unit_id: $aphId,
                    field_id: $fieldId,
                    well_id: aryWellBit,
                    well_status: arywelltype,
                    well_year: $filteryear
                };

                getRopGrid(data);

            }
            else if ($ViewType == 2) {
                aryBitSize.push($(".ListBitSize").val());

                var data = {
                    business_unit_id: $aphId,
                    field_id: $fieldId,
                    well_id: arywellId,
                    well_status: arywelltype,
                    well_year: $filteryear,
                    bit_size: aryBitSize
                };

                LoadRopGrafik(data);
                getWellBitRopGrid(data);
            }
        });

        //async function getRopGrid1(vdata) {
        //    $pageLoading.loading('start');
        //    var dt = $dt_listrop.cmDataTable({
        //        pageLength: 10,
        //        ajax: function (data, callback, settings) {
        //            $.ajax({
        //                url: $.helper.resolveApi("~/UTC/Benchmark/getWellRopData1"),
        //                type: 'POST',
        //                data: vdata,
        //                success: function (r) {
        //                    if (r.status.success) {
        //                        callback({ data: r.data });
        //                    }
        //                }
        //            });
        //        },
        //        columns: [
        //            {
        //                data: "id",
        //                orderable: false,
        //                searchable: false,
        //                class: "text-center",
        //                render: function (data, type, row, meta) {
        //                    return meta.row + meta.settings._iDisplayStart + 1;
        //                }
        //            },
        //            { data: "well_name" },
        //            { data: "bit_size" },
        //            { data: "mudlogging_wob_min" },
        //            { data: "mudlogging_wob_max" },
        //            { data: "mudlogging_rpm_min" },
        //            { data: "mudlogging_rpm_max" },
        //            { data: "mudlogging_flowrate_min" },
        //            { data: "mudlogging_flowrate_max" },
        //            { data: "dg_footage" },
        //            { data: "mudlogging_hkld" },
        //            { data: "mudlogging_mwi" },
        //            { data: "mudlogging_mwo" },
        //            { data: "depth_in" },
        //            { data: "depth_out" },
        //            { data: "duration" },
        //            { data: "dg_rop_overall" }
        //        ],
        //        initComplete: function (settings, json) {
        //        }
        //    }, function (e, settings, json) {
        //        var $table = e; // table selector 
        //    });   
        //    dt.on('processing.dt', function (e, settings, processing) {
        //        $pageLoading.loading('stop');
        //        if (processing) {
        //            $pageLoading.loading('start');
        //        } else {
        //            $pageLoading.loading('stop')
        //        }
        //    })
        //}

        var getRopGrid = function (vparam) {
            var dt = $dt_listrop.dataTable({
                destroy: true,
                responsive: true,
                serverSide: false,
                ajax: function (data, callback, settings) {
                    $.ajax({
                        url: $.helper.resolveApi("~/UTC/Benchmark/getWellRopData"),
                        type: 'POST',
                        data: vparam,
                        success: function (r) {
                            if (r.status.success) {
                                callback({ data: r.data });
                            }
                        }
                    });
                },
                sDom: "",
                order: [],
                paging: false,
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "well_name" },
                    { data: "bit_size" },
                    { data: "mudlogging_wob_min" },
                    { data: "mudlogging_wob_max" },
                    { data: "mudlogging_rpm_min" },
                    { data: "mudlogging_rpm_max" },
                    { data: "mudlogging_flowrate_min" },
                    { data: "mudlogging_flowrate_max" },
                    { data: "dg_footage" },
                    { data: "mudlogging_hkld" },
                    { data: "mudlogging_mwi" },
                    { data: "mudlogging_mwo" },
                    { data: "depth_in" },
                    { data: "depth_out" },
                    { data: "duration" },
                    { data: "dg_rop_overall" }
                ],
                initComplete: function (settings, json) {

                }
            }, function (e, settings, json) {
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        };

        var RopTypeLookup = function (selectedId) {
            if (selectedId == "") {
                $(".rop-type").val('');
            } else {
                $(".rop-type").val(selectedId);
            }
            $(".rop-type").select2({
                placeholder: "Select View Type",
                allowClear: true
            });
        }

        var BitSizeLookup = function (selectedId) {
            if (selectedId == "") {
                $(".ListBitSize").val('');
            } else {
                $(".ListBitSize").val(selectedId);
            }
            $(".ListBitSize").select2({
                placeholder: "Select a Bit Sizee",
                allowClear: true
            });
        };

        var bitSizeSelect = function (vdata) {
            $('#bitsize_select').empty().trigger("change");
            var vparam = {
                well_id: vdata
            }

            $.ajax({
                url: $.helper.resolveApi("~/UTC/Benchmark/getMultiWellBit"),
                type: 'POST',
                data: vparam,
                success: function (r) {
                    if (r.status.success) {
                        if (r.data.databit.length > 0) {
                            for (var i = 0; i < r.data.databit.length; i++) {
                                var newOption = new Option(r.data.databit[i].bit_size, r.data.databit[i].bit_size, false, false);
                                $('#bitsize_select').append(newOption).trigger('change');
                            }
                        }

                        BitSizeLookup();
                    }
                }
            });
        }


        var LoadRopGrafik = function (vparam) {
            $.ajax({
                url: $.helper.resolveApi("~/UTC/Benchmark/getWellBitRopData"),
                type: 'POST',
                data: vparam,
                success: function (r) {
                    if (r.status.success) {
                        $("#ropbit-benchmark").show();
                        if (r.data.length > 0) {
                            LoadRopBitChart('ropbit-chart', r.data);
                        }
                        else {
                            $("#ropbit-chart").html('<div style="text-align: center; padding-top: 150px;"><b>No Data Available</b></div>');
                        }
                    }
                }
            });
        }

        var LoadRopBitChart = function (chartname, vdata) {
            var setChart = {
                renderTo: chartname,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            };

            console.log(vdata);

            //var chartData = [];
            //$.each(vdata, function (key, value) {
            //    chartData.push([key, value.depth]);

            //    var _actualCost = [key, value.daily_cost];
            //    actualCost.push(_actualCost);
            //})

            var ddata = [];

            //for (var i = 0; i < 3; i++) {
            //    var nseries = {
            //        name: 'Well ' + i + '',
            //        data: []
            //    }
            //    ddata.push(nseries);

            //    for (var j = 0; j < 2; j++) {
            //        var arydata = {
            //            x: j,
            //            y: (j + 1) * 3
            //        }

            //        ddata[i].data.push(arydata);
            //    }
            //}

            var ii = 1;

            for (var i = 0; i < vdata.length; i++) {
                var nseries = {
                    name: vdata[i].well_name,
                    data: []
                }
                ddata.push(nseries);

                for (var j = 0; j < 2; j++) {
                    var vx = 0;
                    var vy = 0;

                    if (j == 0) {
                        vx = 0;
                        if (vdata[i].depth_out) {
                            vy = vdata[i].depth_in;
                        }
                    }
                    else {
                        if (vdata[i].duration) {
                            vx = vdata[i].duration;
                        }
                        if (vdata[i].depth_out) {
                            vy = vdata[i].depth_out;
                        }

                        //vx = vx + j + i;
                        //vy = (vy + (ii * (i + 1))) * 10;
                    }

                    var arydata = {
                        x: vx,
                        y: vy
                    }

                    ddata[i].data.push(arydata);
                    ii++;
                }
            }

            var chart = new Highcharts.Chart({
                chart: setChart,
                title: {
                    text: ''
                },
                xAxis: {
                    min: 0,
                    labels: {
                        format: '{value} h'
                    },
                    title: {
                        text: 'Duration'
                    }
                },
                yAxis: {
                    reversed: true,
                    min: 0,
                    title: {
                        text: 'Depth'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    //x: -70,
                    verticalAlign: 'middle',
                    layout: 'vertical',
                    y: 20,
                    //floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    formatter: function () {
                        return '<b>Duration : ' + this.x + ' Hours</b><br/>Well Name : ' + this.series.name + '<br/>Depth : ' + this.y + ' ft<br/>';
                    }
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                    }
                },
                series: ddata,
                credits: {
                    enabled: false
                },

            });

            //var chart = new Highcharts.Chart({
            //    chart: setChart,
            //    title: {
            //        text: ''
            //    },
            //    xAxis: [
            //        {
            //        //categories: [
            //        //    'Jan',
            //        //    'Feb',
            //        //    'Mar',
            //        //    'Apr',
            //        //    'May',
            //        //    'Jun',
            //        //    'Jul',
            //        //    'Aug',
            //        //    'Sep',
            //        //    'Oct',
            //        //    'Nov',
            //        //    'Dec'
            //        //],
            //        crosshair: true,
            //        labels: {
            //            rotation: -45
            //        }
            //    }],
            //    yAxis: {
            //        min: 0,
            //        title: {
            //            text: 'Rainfall (mm)'
            //        }
            //    },
            //    accessibility: {
            //        point: {
            //            valueSuffix: '%'
            //        }
            //    },
            //    series: [{
            //        name: 'Tokyo',
            //        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

            //    }, {
            //        name: 'New York',
            //        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

            //    }, {
            //        name: 'London',
            //        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

            //    }, {
            //        name: 'Berlin',
            //        data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

            //    }],
            //    credits: {
            //        enabled: false
            //    }
            //});
        };


        var getWellBitRopGrid = function (vparam) {
            $("#dtbitrop").show();
            var dt = $dt_listbitrop.dataTable({
                destroy: true,
                responsive: true,
                serverSide: false,
                ajax: function (data, callback, settings) {
                    $.ajax({
                        url: $.helper.resolveApi("~/UTC/Benchmark/getWellBitRopData"),
                        type: 'POST',
                        data: vparam,
                        success: function (r) {
                            if (r.status.success) {
                                callback({ data: r.data });
                            }
                        }
                    });
                },
                sDom: "",
                order: [],
                paging: false,
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "well_name" },
                    { data: "bit_size" },
                    { data: "mudlogging_wob_min" },
                    { data: "mudlogging_wob_max" },
                    { data: "mudlogging_rpm_min" },
                    { data: "mudlogging_rpm_max" },
                    { data: "mudlogging_flowrate_min" },
                    { data: "mudlogging_flowrate_max" },
                    { data: "dg_footage" },
                    { data: "mudlogging_hkld" },
                    { data: "mudlogging_mwi" },
                    { data: "mudlogging_mwo" },
                    { data: "depth_in" },
                    { data: "depth_out" },
                    { data: "duration" },
                    { data: "dg_rop_overall" }
                ],
                initComplete: function (settings, json) {

                }
            }, function (e, settings, json) {
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        };

        var bit_record = function (wellid) {
            $btn_bit_record.on('click', function () {
                window.open($.helper.resolve("/utc/APH/BitRecord/Download?wellId=" + wellid), '_blank');
            });
        }




        return {
            init: function () {
                APHLookup();
                uomSelect();
                ChartTypeLookup();
                RopTypeLookup();
                BitSizeLookup();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
});