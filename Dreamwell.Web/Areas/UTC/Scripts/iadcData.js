﻿(function ($) {
    'use strict';
    var $dt_listIadc = $('#dt_listIadc');
    var $pageLoading = $('#page-loading-content');

    var pageFunction = function () {
        var loadDataTable = function () {
            var dt = $dt_listIadc.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/iadc/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "iadc_code" },
                    { data: "description" },
                    {
                        data: "type",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            console.log(row);
                            if (type === 'display') {
                                if (row.type == "1")
                                    return "<span>PT</span>";
                                else if (row.type == "2")
                                    return "<span>NPT</span>";
                            }
                            return data;
                        }
                    },
                    {
                        data: "created_on",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            console.log(row);
                            if (type === 'display') {
                                return moment(row.created_on).format('MMM DD, YYYY');
                            }
                            return data;
                        }
                    },
                ],
                initComplete: function (settings, json) {
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 