﻿$(document).ready(function () {
    'use strict';
    var $recordId = $("#wellId");

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                var loading = `<div class="spinner-border" role="status" style="width: 4rem; height: 4rem;">
                                    <span class="sr-only">Loading...</span>
                               </div><div class="mt-4"></div>`;
                $("#stuckPipe").html(loading);

                $.get($.helper.resolveApi('~/UTC/SingleWell/GetDrillingStuckPipe/' + $recordId.val()), function myfunction(r) {
                    console.log('GetDrillingStuckPipe');
                    console.log(r);
                    if (r.status.success && r.data.length > 0) {
                        var aryStuckPipe = [];
                        var date = [];
                        $.each(r.data, function (key, value) {
                            date.push(moment(value.drilling_date).format('DD MMM'));
                            //if (parseInt(value.total_hours_iadc) <= 0) {
                            //    aryStuckPipe.push(0);
                            //} else {
                            //    var percentage_pipe = (parseFloat(value.total_hours_iadc) / (parseFloat(value.total_hours))) * 100;
                            //    aryStuckPipe.push(percentage_pipe);
                            //}
                            aryStuckPipe.push(value.total_hours);
                        })


                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'stuckPipe',
                                margin: [50, 10, 60, 65],
                                type: 'column',
                                height: 400,
                            },
                            title: {
                                text: 'Stuck Pipe'
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                categories: date,
                                title: {
                                    text: null
                                },
                                labels: {
                                    rotation: -45,
                                    style: {
                                        fontSize: '10px',
                                        fontFamily: 'Verdana, sans-serif'
                                    }
                                }
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Percentage',
                                    align: 'middle'
                                },
                                labels: {
                                    overflow: 'justify'
                                }
                            },
                            series: [
                                {
                                    name: "Stuck Pipe",
                                    data: aryStuckPipe
                                }
                            ],
                            tooltip: {
                                formatter: function () {
                                    var point = this.point;
                                    point.y2 = this.series.options.data[point.index];

                                    return '<b>' + this.series.options.name + '<b/><br/>Usage: ' + Highcharts.numberFormat(point.y2, 2) + ' Hrs';
                                }
                            },
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    }
                                }
                            },
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 10
                            },
                            credits: {
                                enabled: false
                            },
                        });
                    }
                });
            } else {
                
            }
        };

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});