﻿$(document).ready(function () {
    window.depthuom = "";
    'use strict';
    var $wellId = $("input[id=id]");
    var $formWellDDR = $("#form-wellDDR");

    var $formDrillingOperation = $("#formDrillingOperation");
    var $formHSSE = $("#formHSSE");
    var $formdrillingBHA = $("#form-drillingBHA");
    var $formDrillingBit = $("#formDrillingBit");
    var $formMudloggingData = $("#formMudloggingData");
    //var $formDrillingDeviation = $("#formDrillingDeviation");
    var $form_mud1 = $("#form-mud1");
    var $form_mud2 = $("#form-mud2");
    var $form_mud3 = $("#form-mud3");
    var $formTotalVolume = $("#form-totalVolume");
    var $formChemicalUse = $("#form-chemicalUse");
    var $formDrillingWeather = $('#formDrillingWeather');
    //var $date = $("#datepicker-3");

    var isShow = false;
    var $btnShowTrajectory = $("#btnShowTrajectory");
    var plan_depth = 0;
    var ObjectMapper = [];
    //alert(date.val());
    $("#well_bha_id").cmSelect2({
        url: $.helper.resolveApi('~/core/wellbha/lookupByWell/' + $wellId.val()),
        result: {
            id: 'id',
            text: 'bha_no'
        },
        filters: function (params) {
            return [{
                field: "bha_no",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            placeholder: "",
            allowClear: true,
            templateResult: function (repo) {
                if (repo.loading) {
                    return repo.text;
                }
                return "<span class='fw-700'>BHA No: " + repo.text + "</span>";
            },
            templateSelection: function (data, container) {
                if (data.text == "")
                    return "Please Select a BHA";
                else
                    return "<span class='fw-700'>BHA No: " + data.text + "</span>";
            },
        }

    });

    var pageFunction = function () {

        var GetWellObjectUomMap = function () {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $wellId.val()), function (r) {
               // console.log("ini adlaah unit uom");
               // console.log(r.data);
                if (r.status.success && r.data.length > 0) {
                    $("form#formDrilling :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("form#formOperationActivities :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#form-drillingHoleAndCasing :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("form#form-drillingBHA :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#form-drillingBhaComponent :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("form#form-drillingBIT :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#tableDrillingDeviation :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("#tableActualInformation :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("#tableWellDDR :input.uom-definition").each(function () {
                        console.log("check table well ddr " + $(this).data("objectname"));
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("#form-drillingBhaBit :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });

                    $("#formDrillingWeather :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });

                   
                    //$("#formOperationActivities :input.uom-definition").each(function () {
                    //    var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                    //    var elementId = $(this)[0].id;
                    //    if (objectName != undefined) {
                    //        $.each(r.data, function (key, value) {
                    //            if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                    //                $("#" + elementId).val(value.uom_code);
                    //            }
                    //        });
                    //    }
                    //})
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }


        var lodaDate = function (dt) {
            console.log('Initializing Datepicker with date:', dt);
            var data = moment(dt).format('YYYY-MM-DD');
            $('#datepicker-3').datepicker(
                {
                    autoclose: true,
                    todayBtn: "linked",
                    format: 'yyyy-mm-dd',
                    orientation: "bottom left",
                    //defaultDate:date,
                    //todayHighlight: true,
                }
            );
            $('#datepicker-3').datepicker('setDate', data);

            $('#datepicker-3').datepicker().on('change', function (e) {
                wellDDR($(this).val());
                //drillingOperationActivity($(this).val());
                drillOperation($(this).val());
            })
        }



        Handlebars.registerHelper('setType', function (type) {
            if (type == 1) {
                return "PT";
            } else if (type == "2") {
                return "NPT";
            }
        });

        Handlebars.registerHelper('setIadcName', function (iadc_code, iadc_description, type) {
            if (type == 1) {
                return iadc_code + " - " + iadc_description + " (PT)";
            } else if (type == "2") {
                return iadc_code + " - " + iadc_description + " (NPT)";
            }
        });

        Handlebars.registerHelper('printHoleCasingType', function (type, id) {
            var html = "";

            html += "<select class=\"form-control hole-casing-tipe\" id='hole-casing-tipe-" + id + "' name=\"lot_fit[]\">";
            if (type == 1) {
                html += `<option value="1" selected="true">LOT</option>
                          <option value="2">FIT</option>`;
            } else if (type == 2) {
                html += `<option value="1">LOT</option>
                          <option value="2" selected="true">FIT</option>`;
            } else {
                html += `<option value="1">LOT</option>
                          <option value="2">FIT</option>`;
            }
            html += "</select>";
            html += "<div class=\"invalid-feedback\">Lot or Fit type cannot be empty</div>";
            return new Handlebars.SafeString(html);
        });

        var wellBhaLookUp = function (e) {
            //var $element = e;
            //if (e == null)
            var $element = $(".wellBhaLookup");
            $element.cmSelect2({
                url: $.helper.resolveApi('~/core/wellbha/lookupByWell/' + $wellId.val()),
                result: {
                    id: 'id',
                    text: 'bha_no'
                },
                filters: function (params) {
                    return [{
                        field: "bha_no",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "",
                    allowClear: true,
                    templateResult: function (repo) {
                        if (repo.loading) {
                            return repo.text;
                        }
                        return "<span class='fw-700'>No: " + repo.text + "</span>";
                    },
                    templateSelection: function (data, container) {
                        if (data.text == "")
                            return "Choose BHA";
                        else
                            return "<span class='fw-700'>No: " + data.text + "</span>";
                    },
                }
            });
        }

        var wellBitLookUp = function (e) {
            //var $element = e;
            //if (e == null)
            var $element = $(".wellBitLookup");
            $element.cmSelect2({
                url: $.helper.resolveApi('~/core/wellbit/lookupByWell/' + $wellId.val()),
                result: {
                    id: 'id',
                    text: 'bit_number',
                    bit_size: 'bit_size'
                },
                filters: function (params) {
                    return [{
                        field: "bit_number",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "",
                    allowClear: true,
                    templateResult: function (repo) {
                        if (repo.loading) {
                            return repo.text;
                        }
                        return "<span class='fw-700'>No: " + repo.text + "</span>";
                    },
                    templateSelection: function (data, container) {
                        if (data.text == "")
                            return "Choose BIT";
                        else
                            return "<span class='fw-700'>No: " + data.text + "</span>";
                    },
                }
            });
        }



        var chart = new main("trajectory3d");

        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }
        var arrayDepth = [];
        var arrayDays = [];
        var arrayCost = [];

        async function getActualDepth() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/WellData/chart/actualDepth?wellId=" + $wellId.val()),
                    type: 'GET'
                });
                //console.log(result.data);
                return result.data;
            } catch (error) {
                //console.error(error);
            }
        }

        async function getActualCost() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/WellData/chart/actualCost?wellId=" + $wellId.val()),
                    type: 'GET'
                });

                return result.data;
            } catch (error) {
                //console.error(error);
            }
        }

        async function getPlanningDepth() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/WellData/chart/tvd/planningDepth?wellId=" + $wellId.val()),
                    type: 'GET'
                });

                return result.data;
            } catch (error) {
                //console.error(error);
            }
        }

        async function getPlanningCost() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/WellData/chart/tvd/planningCost?wellId=" + $wellId.val()),
                    type: 'GET'
                });

                return result.data;
            } catch (error) {
                //console.error(error);
            }
        }

        function GetUomForChart() {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $wellId.val()), function (r) {
                if (r.status.success && r.data.length > 0) {
                    $.each(r.data, function (key, value) {
                        if ('tvd depth' == (value.object_name || '').toLowerCase().trim()) {
                            window.depthuom = value.uom_code;
                        }
                    });
                }

                chartTVD();
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        function chartTVD() {
            if ($wellId.val() !== '') {
                console.log('asasasa')
                $.get($.helper.resolveApi('~/UTC/SingleWell/GetTataWaktuPengeboranNew/' + $wellId.val()), function (r) {
                    console.log('GetTataWaktuPengeboranNew');
                    // console.log(r);
                    if (r.status.success) {
                        var date = [];
                        var days = [];
                        var actualDepth = [];
                        var dailyCost = [];
                        var tvdDepth = [];
                        var cumCost = [];
                        var arrtvdDepth = [];
                        var arrcumCost = [];
                        var drillingActivity = [];
                        var cost = 0;
                        $.each(r.data.report_drilling, function (key, value) {
                            date.push(moment(value.drilling_date).format('YYYY-MM-DD'));
                            days.push(value.dfs == 1 ? 1 : value.dfs);
                            var _actualDepth = [value.dfs, value.current_depth_md == null ? 0 : value.current_depth_md];
                            actualDepth.push(_actualDepth);
                            cost += value.daily_cost;
                            cumCost.push(cost);
                            //console.log("ini cumulative",cumCost)
                            // var _actualCost = [value.dfs, value.daily_cost == null ? 0 : value.daily_cost];
                            // dailyCost.push(_actualCost);
                            // console.log("ini data actual depth", _actualDepth);
                            if (value.operation_activity.length > 0) {
                                $.each(value.operation_activity, function (keyDOA, valueDOA) {
                                    drillingActivity.push(valueDOA.description);
                                    //console.log("ini",drillingActivity)
                                });

                            }
                        });

                        $.each(r.data.tvd_plan, function (key, value) {
                            var _planningDepth = [value.days, value.depth];
                            arrtvdDepth.push(_planningDepth);

                            var _planningCost = [value.days, value.cumm_cost];
                            arrcumCost.push(_planningCost);
                        })

                        $.each(r.data.drilling_data, function (key, value) {
                            var _actualCost = [value.cummulative_cost_count == null ? 0 : value.cummulative_cost_count];
                            dailyCost.push(_actualCost);
                            //console.log("ini", dailyCost)
                        })
                        //dailyCost.push(dailyCost[dailyCost.length - 1]);
                        $('#tvdchart').highcharts({
                            title: {
                                text: ''
                            },

                            subtitle: {
                                text: ''
                            },
                            credits: {
                                enabled: false
                            },
                            yAxis: [{ // Primary yAxis
                                floor: 0,
                                ceiling: 10000,
                                labels: {
                                    format: '{value} ' + window.depthuom,
                                    style: {
                                        color: Highcharts.getOptions().colors[1]
                                    }
                                },
                                title: {
                                    text: 'Depth',
                                    style: {
                                        color: Highcharts.getOptions().colors[1]
                                    }
                                },
                                reversed: true
                            }, { // Secondary yAxis
                                floor: 0,
                                title: {
                                    text: 'Cost',
                                    style: {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                                },
                                labels: {
                                    format: '{value} USD',
                                    style: {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                                },
                                opposite: true
                                //reversed: true
                            }],
                            xAxis: {
                                floor: 0,
                                ceiling: 10000,
                                labels: {
                                    format: '{value}',
                                    step: 0
                                },
                                title: {
                                    text: 'Days'
                                },
                                accessibility: {
                                    rangeDescription: 'Range: 0 to 200'
                                }
                            },

                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            },
                            tooltip: {
                                //pointFormatter: function () {
                                //    var seriesNameConverter = {
                                //        'Seriesonename': 'Series One Name',
                                //        'Seriestwoname': 'Series Two Name'
                                //    };

                                //    return '<span style="color:{point.color}">\u25CF</span> '
                                //        + seriesNameConverter[this.series.name] + ': <b>' + this.y + '</b><br/>';

                                //    console.log(this.series.name);
                                //},
                                formatter: function () {
                                    var html = '<span style="font-size:10px"><b>' + this.series.name + '</b></span><br/>';
                                    if (this.series.name == "Actual Depth") {
                                        html += '<span style="font-size:10px">' + this.key + ' Days</span><br/>';
                                    } else if (this.series.name == "Planning Depth") {
                                        html += '<span style="font-size:10px">' + this.key + ' Days</span><br/>';
                                        html += '<span style="font-size:10px">' + this.y + ' ' + window.depthuom + '</span><br/>';
                                    } else if (this.series.name == "Planning Cost") {
                                        html += '<span style="font-size:10px">' + this.y.toFixed(2) + '$</span><br/>';
                                    }
                                    var keyInt = this.key - 1;
                                    if (r.data.drilling_data[keyInt] && this.series.name != "Planning Depth" && this.series.name != "Planning Cost") {
                                        //if (r.data.drilling_data[this.key].cummulative_cost_count != null) {
                                        //    var cummulative_cost_count = r.data.drilling_data[this.key].cummulative_cost_count.toFixed(2);
                                        //} else {
                                        //    var cummulative_cost_count = "-";
                                        //}
                                        //html += '<span style="font-size:10px">' + cummulative_cost_count + ' $</span><br/>';
                                        html += '<span style="font-size:10px">' + cumCost[keyInt].toFixed(2) + ' $</span><br/>';
                                        html += '<span style="font-size:10px">' + r.data.drilling_data[keyInt].current_depth_md + ' ' + window.depthuom + '</span><br/>';

                                    }

                                    if (this.series.name == "Actual Depth")
                                        html += '<span style="font-size:10px"><b>Drilling Operation Activity</b></span><br/><br/>';

                                    if (r.data.report_drilling[this.key]) {
                                        if (r.data.report_drilling[this.key].operation_activity.length > 0 && this.series.name == "Actual Depth") {
                                            html += '<span style="font-size:10px">' + r.data.drilling_data[this.key].operation_data_period + '</span><br/><br/>';
                                        }
                                    }

                                    return html;
                                }

                            },
                            plotOptions: {
                                series: {

                                    label: {
                                        connectorAllowed: false
                                    },
                                    pointStart: 0,
                                }
                            },

                            series: [{
                                name: 'Actual Depth',
                                data: actualDepth,

                            },
                            {   
                                name: 'Cummulative Cost',
                                yAxis: 1,
                                data: cumCost
                            },
                            {
                                name: 'Planning Depth',
                                data: arrtvdDepth
                            },
                            {
                                name: 'Planning Cost',
                                yAxis: 1,
                                data: arrcumCost
                            }],

                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 1000
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }
                        });
                    }
                });
            } else {

            }
        }
        GetUomForChart();


        var aryProjection = [];
        var deviationWell = [];
        var deviationDrilling = [];
        var loadWellDeviation = function () {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            $.get($.helper.resolveApi('~/core/WellDeviation/' + $wellId.val() + '/detailByWellId'), function (r) {
                //console.log("Load 3D");
                //console.log(r);
                if (r.status.success && r.data.length > 0) {
                    $.each(r.data, function (key, value) {
                        var point = [value.tvd, value.e_w, value.n_s, 200];
                        deviationWell.push(point);
                    });
                    //console.log(depth);
                    //chart3d("", deviationWell);
                    getDrillingDeviationByWellIdEventClick();
                }
                $('#loadingModal').modal('hide');
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };


        function chart3d(useDiam, plan, actual) {
            isShow = true;
            var chart = new main("trajectory3d");
            aryProjection.push(plan);
            var diam = 0;
            chart.addColor(["#0073B7", "#DA0425", "#BBD86F", "#9400D3", "#008B8B", "#00FF00", "#0000FF", "#FF8C00", "#000000", "#FF69B4", "#00F5FF"]);
            if (useDiam !== 'undefined' && useDiam === true) {
                diam = 700;
            }

            //var ary = [];
            //ary[0] = [[0.00, 0.00, 0.00, diam], [500.00, 0.00, 0.00, diam], [1000.00, 200, 200, diam], [1500.00, 200, 200, diam]];
            //var actual = [[0.00, 0.00, 500.00, 100], [500.00, 500.00, 0.00, 50], [1000.00, 500, 200, 50], [1500.00, 500, 200, 50]];
            aryProjection.push(actual);

            //console.log(ary[0]);
            chart.drawGraphic(aryProjection);
            chart.setView("left");
        }

        var wellProfile = function () {
            //alert(drillingId);
            var templateScript = $("#wellProfileDetail-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/well/' + $wellId.val() + '/detail'), function (r) {

                if (r.data) {
                    $("#tableWellDetailProfile > tbody").html(template(r.data));
                    console.log('profile');
                    console.log(r.data.spud_date);
                    plan_depth = r.data.planned_td;
                    lodaDate(r.data.spud_date);
                    //datepickerFormat($('.date-picker'));
                    //iadcLookUp();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };
        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

        $('#scriptSelector').change(function () {
            var selectedScript = $(this).val();
            $('.describe').html('');

            if (selectedScript === 'actual-section') {
                loadWellDeviation();
                actualWell();
            } else if (selectedScript === 'plan-section') {
                loadWellDeviation();
                displayAll();

            } else if (selectedScript === 'all-section') {
                displayAllWell();
            } else {
                console.log("Nothing Selected")
            }
        });

        var displayAllWell = function () {
            loadWellDeviation();
            displayAll();
            //planWell();
        }


        var actualWell = function () {
            var getTypeData = function () {
                return $.get($.helper.resolveApi('~/core/well/' + $wellId.val() + '/detail'))
                    .then(response => response.status.success ? response.data : []);
            }

            $.when(getTypeData()).done(function (dataType) {
                if (dataType && dataType.well_type) {
                    if (dataType.well_type === 'Horizontal' || dataType.well_type === 'J-type') {
                        horizontalActualWell();
                    } else if (dataType.well_type === 'Vertical' || dataType.well_type === 'Directional') {
                        verticalActualWell();
                    } else {
                        console.log('Invalid data type');
                    }
                } else {
                    console.error('Invalid data or missing "type" property');
                }
            });
        };


        var horizontalActualWell = function () {
            var dataActual = [];

            function getCoordinateActual() {
                return $.get($.helper.resolveApi('~/core/drillingdeviation/' + $wellId.val() + '/listByWellId'))
                    .then(response => response.status.success ? response.data : []);
            }

            function getDescriptionActual() {
                return $.get($.helper.resolveApi('~/core/drillingoperationactivity/getByDrillingWellIdas/' + $wellId.val()))
                    .then(response => response.status.success ? response.data : []);
            }

            var actualImage = function (dataA, dataB) {
                var imagedata = [];
                var defaultWidthAct = 120;
                var minvalueAct = 10;

                for (const itemA of dataA) {
                    const sudutA = itemA.inclination;
                    const kedalamanA = itemA.measured_depth;
                    const imgdata = {
                        sudut: sudutA,
                        kedalaman: kedalamanA,
                        widht: defaultWidthAct
                    };

                    const matchingDataB = dataB.find(itemB => {
                        return itemA.measured_depth != null &&
                            itemB.hole_depth != null &&
                            itemA.measured_depth.toString().substring(0, 2) === itemB.hole_val.toString().substring(0, 2);
                    });

                    if (matchingDataB) {
                        var dynamic = defaultWidthAct -= minvalueAct;
                        imgdata.dynamic = dynamic;
                    }
                    imagedata.push(imgdata);
                }

                return imagedata;
            };



            var actualDesc = function (dataA, dataB) {
                var descData = [];
                for (const itemA of dataA) {
                    const matchingDataB = dataB.find(itemB => {
                        if (itemA && itemA.measured_depth != null) {
                            const kedalamanA = itemA.measured_depth.toString().substring(0, 2);
                            if (itemB.hole_depth != null) {
                                const duaAngkaDepan = itemB.hole_val.toString().substring(0, 2);
                                return kedalamanA === duaAngkaDepan;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    });

                    if (matchingDataB) {
                        var dataAAdded = {
                            nama: matchingDataB.hole_name,
                            depth: matchingDataB.hole_depth,
                            indexHC: dataB.indexOf(matchingDataB),
                            arrayPipa: []
                        };

                        var currentDataPlan = {
                            arrayData: []
                        };

                        var dataTerakhirAct = false;

                        for (var k = 0; k < dataA.length; k++) {
                            currentDataPlan.arrayData.push({
                                y: dataA[k].measured_depth,
                                x: dataA[k].inclination,
                            });

                            if (dataA[k].measured_depth.toString().substring(0, 2) === matchingDataB.hole_depth.toString().substring(0, 2)) {
                                currentDataPlan.arrayData.push({
                                    y: dataA[k].measured_depth,
                                    x: dataA[k].inclination,
                                    nama: matchingDataB.hole_name,
                                    casing: matchingDataB.casing_name,
                                    depth_line: dataTerakhirAct = true
                                });
                                break;
                            }
                        }

                        dataAAdded.arrayPipa.push(currentDataPlan);
                        descData.unshift(dataAAdded);
                    }
                }

                return descData;
            };

            $.when(getCoordinateActual(), getDescriptionActual())
                .done(function (dataA, dataB) {
                    var imageDataAct = actualImage(dataA, dataB);
                    var actualDesct = actualDesc(dataA, dataB);
                    renderHoriActual({ imageDataAct, actualDesct });
                })
                .fail(function (errorActual) {
                    console.error("Error fetching data from Actual Well API:", errorActual);
                });
        };


        var verticalActualWell = function () {
            var processData = function () {
                return $.get($.helper.resolveApi('~/core/drillingoperationactivity/getByDrillingWellIdas/' + $wellId.val()))
                    .then(response => response.status.success ? response.data : []);
            };

            var calculateTotalHole = function (dataV) {
                var totalLoop = 0;
                var dataActual = [];

                dataV.sort(function (a, b) {
                    return a.hole_depth - b.hole_depth;
                });


                for (const item of dataV) {
                    const hole_depth = item.hole_depth;
                    const hole_depth_divided = hole_depth / 10;
                    totalLoop += hole_depth_divided;
                    const totalHole = totalLoop;

                    const dataVerAdd = {
                        totalHole: totalHole,
                        hole_depth_divided: hole_depth_divided
                    };

                    dataActual.push(dataVerAdd);
                }

                dataActual.sort(function (c, d) {
                    return d.totalHole - c.totalHole;
                });

                return dataActual;
            };

            var dynamicWidthAct = function (dataV) {
                var defaultWidthAct = 130;
                var minvalueAct = 10;

                var resultArray = dataV.map(function (item) {
                    const hole_depth = item.hole_depth;
                    const hole_depth_divided = hole_depth / 10;

                    var dynamic = defaultWidthAct -= minvalueAct

                    return {
                        name: item.hole_name,
                        depth: item.hole_depth,
                        hole_type: item.hole_type,
                        hole_depth_divided: hole_depth_divided,
                        widthDynamicAct: dynamic
                    };
                });

                return resultArray
            };

            $.when(processData())
                .done(function (dataV) {
                    var totalHoleArray = calculateTotalHole(dataV);
                    var widhtDynamic = dynamicWidthAct(dataV);
                    renderVartiActual({ totalHoleArray, widhtDynamic });
                })
                .fail(function (error) {
                    console.error(error);
                });
        };

        var renderHoriActual = function (dataActual) {
            var modifiedTemplateScript = $("#actualHorizontal-template").html();
            var modifiedTemplate = Handlebars.compile(modifiedTemplateScript);
            $("#pipe-drill > .row").html(modifiedTemplate(dataActual));
            maskingMoney();
        };

        var renderVartiActual = function (dataArrays) {
            var modifiedTemplateScript = $("#actualVertical-template").html();
            var modifiedTemplate = Handlebars.compile(modifiedTemplateScript);
            $("#pipe-drill > .row").html(modifiedTemplate(dataArrays));
            maskingMoney();
        };

        var displayAll = function () {
            var getTypeData = function () {
                return $.get($.helper.resolveApi('~/core/well/' + $wellId.val() + '/detail'))
                    .then(response => response.status.success ? response.data : []);
            }

            $.when(getTypeData()).done(function (dataType) {
                if (dataType && dataType.well_type) {
                    if (dataType.well_type === 'Horizontal') {
                        horizontalPlanWell();
                        console.log('Data type is horizontal');
                    } else if (dataType.well_type === 'Vertical' || dataType.well_type === 'Directional') {
                        vertcalPlanWell();
                        console.log('Data type is vertical');
                    } else {
                        console.log('Invalid data type');
                    }
                } else {
                    console.error('Invalid data or missing "type" property');
                }
            });
        }

        var horizontalPlanWell = function () {
            var daftarTampli = [];
            var xStart = -10;

            var getCordinatePlan = function () {
                return $.get($.helper.resolveApi('~/core/WellDeviation/' + $wellId.val() + '/detailByWellId'))
                    .then(response => response.status.success ? response.data : []);
            };

            var getDescription = function () {
                return $.get($.helper.resolveApi('~/core/wellholeandcasing/' + $wellId.val() + '/getAllByWellIdnew'))
                    .then(response => response.status.success ? response.data : []);
            };

            var getDrillHazzard = function () {
                return $.get($.helper.resolveApi('~/core/DrillingHazard/' + $wellId.val() + '/detailbywellid'))
                    .then(response => response.status.success ? response.data : []);
            }

            $.when(getCordinatePlan(), getDescription(), getDrillHazzard())
                .done(function (dataA, dataB, dataC) {
                    var xStart = 0;
                    var daftarTampli = [];
                    var currentBaseWidth = 120;

                    for (const itemA of dataA) {
                        const sudutA = itemA.inclination;
                        const kedalamanA = itemA.measured_depth;
                        let baseWidth = currentBaseWidth;
                        const longLine = dataA.indexOf(itemA) * 10;
                        const totalLoop = dataB.length;

                        const dataAAdded = { sudut: sudutA, kedalaman: kedalamanA, longline: longLine, widthDynamic: baseWidth, totalLoops: totalLoop, arrayPipa: [] };

                        const matchingDataB = dataB.find(itemB =>
                            kedalamanA.toString().substring(0, 3) === itemB.hole_depth.toString().substring(0, 3)
                        );

                        if (matchingDataB) {
                            dataAAdded.nama = matchingDataB.hole_name;
                            dataAAdded.depth = matchingDataB.hole_depth;
                            dataAAdded.indexHC = dataB.indexOf(matchingDataB);
                            dataAAdded.startX = xStart + 10;
                            currentBaseWidth -= 13.5;

                            xStart += 10;

                            var currentDataPlan = {
                                arrayData: []
                            };

                            var dataTerakhir = false;

                            for (var k = 0; k < dataA.length; k++) {
                                currentDataPlan.arrayData.push({
                                    y: dataA[k].measured_depth,
                                    x: dataA[k].inclination,

                                });

                                if (dataA[k].measured_depth.toString().substring(0, 3) === matchingDataB.hole_depth.toString().substring(0, 3)) {
                                    currentDataPlan.arrayData.push({
                                        y: dataA[k].measured_depth,
                                        x: dataA[k].inclination,
                                        depth_line: dataTerakhir = true
                                    })
                                    break;
                                }

                            }

                            dataAAdded.arrayPipa.push(currentDataPlan);
                        }

                        const matchingDataH = dataC.find(itemC =>
                            kedalamanA.toString().substring(0, 3) === itemC.depth.toString().substring(0, 3)
                        );

                        if (matchingDataH) {
                            dataAAdded.dhDepth = matchingDataH.depth;
                            dataAAdded.dhDesc = matchingDataH.description;
                            dataAAdded.indexDH = dataC.indexOf(matchingDataH);
                        }
                        daftarTampli.push(dataAAdded);
                    }
                    renderHorizontalPlanData(daftarTampli);

                })
                .fail(function (error) {
                    console.error(error);
                });

        };

        var vertcalPlanWell = function () {
            var totalHoleDepthDivided = 0;
            var verticalData = [];

            var getVeriticalData = function () {
                return $.get($.helper.resolveApi('~/core/wellholeandcasing/' + $wellId.val() + '/getAllByWellSchemanticId'))
                    .then(response => {
                        //console.log("asa",response)
                        if (response.status.success) {
                            var data = response.data;
                            console.log("ini plan", data);  // Log the data to the console
                            return data;
                        } else {
                            console.error("Failed to ajeeg vertical data. Error:", response.status.message);
                            return [];
                        }
                    })
                    .fail(function (error) {
                        console.error("Failed to retrieve vertical data. Error:", error.statusText);
                        return [];
                    });
            };

            var getVerticalDrillHazzard = function () {
                return $.get($.helper.resolveApi('~/core/DrillingHazard/' + $wellId.val() + '/detailbywellid'))
                    .then(response => response.status.success ? response.data : []);
            }

            $.when(getVeriticalData(), getVerticalDrillHazzard())
                .done(function (dataV, dataVDH) {

                    //console.log("asdawdwdadwad",dataV, dataVDH)

                    var defaultWidthPlan = 130;
                    var minvaluePlan = 10;
                    for (const item of dataV) {
                        const hole_name = item.hole_name;
                        const hole_depth = item.hole_depth;
                        const hole_type = item.hole_type;
                        const hole_depth_divided = item.hole_depth / 10;

                        defaultWidthPlan -= minvaluePlan
                        const dynamicWidth = defaultWidthPlan;


                        const maxHoleDepth = Math.max(...dataV.map(item => item.hole_depth / 10));
                        const lineHole = item.hole_depth / 10;
                        totalHoleDepthDivided += lineHole;
                        const totalHole = totalHoleDepthDivided;

                        const dataVerAdd = {
                            nama: hole_name,
                            depth: hole_depth,
                            hole_type: hole_type,
                            hole_depth_divided: hole_depth_divided,
                            widthDynamic: dynamicWidth,
                            maxHoleDepth: maxHoleDepth,
                            lineHole: lineHole,
                            totalHole: totalHole
                        };

                        const matchingDataHV = dataVDH.find(itemC =>
                            hole_depth.toString().substring(0, 3) === itemC.depth.toString().substring(0, 3)
                        );

                        if (matchingDataHV) {
                            dataVerAdd.dhDepth = matchingDataHV.depth;
                            dataVerAdd.dhDesc = matchingDataHV.description;
                            dataVerAdd.indexDH = dataVDH.indexOf(matchingDataHV);
                        }

                        verticalData.push(dataVerAdd)

                    }
                    renderVerticalPlanData(verticalData);
                })
                .fail(function (error) {
                    console.error(error);
                });
        };

        var renderHorizontalPlanData = function (verticalData) {
            var modifiedTemplateScript = $("#horizontal-template").html();
            var modifiedTemplate = Handlebars.compile(modifiedTemplateScript);
            $("#pipe-drill > .row").html(modifiedTemplate({ dataCoordinate: verticalData }));
            maskingMoney();
        };

        var renderVerticalPlanData = function (verticalData) {
            var modifiedTemplateScript = $("#vertical-template").html();
            var modifiedTemplate = Handlebars.compile(modifiedTemplateScript);
            $("#pipe-drill > .row").html(modifiedTemplate({ dataCoordinate: verticalData }));
            maskingMoney();
        };

        var drillOperation = function (date) {
            $.get($.helper.resolveApi('~/UTC/WellData/getByWellId/' + $wellId.val() + '/' + date + ''), function (r) {
                console.log(r);
                if (r.data) {
                    $.each(r.data, function (key, value) {
                        $("#type" + value.type).val(value.description);
                    })
                }
            });
        }

        var drillingOperationActivity = function (id) {
            var templateScript = $("#readHoleAndCasing-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/utc/WellData/getByDrillingId/' + id + '/' + $wellId.val()), function (r) {
                if (r.data) {
                    $("#formOperationActivities").html(template({ data: r.data }));

                    //$("#formOperationActivities :input.uom-definition").each(function () {
                    //    var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                    //    var elementId = $(this)[0].id;
                    //    if (objectName != undefined) {
                    //        $.each(ObjectMapper, function (key, value) {
                    //            if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                    //                $("#" + elementId).val("(" + value.uom_code + ")");
                    //            }
                    //        });
                    //    }
                    //});

                    $.each(r.data, function (key, value) {
                        //holeAndCasingLookup(value.id);
                        //lotFitLookup(value.id, false);

                        $("#cloneOperationActivies-" + value.id).on('click', function () {
                            cloneOperationActivities(value.id);
                        });
                    });

                    //datepickerFormat($('.date-picker'));
                    //iadcLookUp();

                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowOperation').remove();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {

            }).done(function () {

            });
        }


        var getDrillingBhaBit = function (id) {
            var templateScript = $("#readBhaBit-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillingBhaBit/getDetail/' + id), function (r) {
                console.log('DrillingBhaBit');
                console.log(r);
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#form-drillingBhaBit").html(template({ data: r.data }));

                        //GetWellObjectUomMap();

                        $("#form-drillingBhaBit .numeric-money").inputmask({
                            digits: 2,
                            greedy: true,
                            definitions: {
                                '*': {
                                    validator: "[0-9]"
                                }
                            },
                            rightAlign: false
                        });

                        $(".depth-out").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".depth-in").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".duration-bit").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });

                        $.each(r.data, function (key, value) {
                            wellBhaLookUp(value.well_bha_id);
                            wellBitLookUp(value.well_bit_id);

                        });


                    } else {
                        //cloneMudloggingData();
                    }

                    $('.row-delete').on('click', function () {
                        $(this).closest('.row-data').remove();
                    });
                }
            }).fail(function (r) {
            }).done(function () {
            });
        }

        var getDrillingBhaBit__ASD = function (id) {
            var templateScript = $("#readBhaBit-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillingBhaBit/getDetail/' + id), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#form-drillingBhaBit").html(template({ data: r.data }));

                        //GetWellObjectUomMap(r.data);

                        $("#form-drillingBhaBit .numeric-money").inputmask({
                            digits: 2,
                            greedy: true,
                            definitions: {
                                '*': {
                                    validator: "[0-9]"
                                }
                            },
                            rightAlign: false
                        });

                        $(".depth-out").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".depth-in").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".duration-bit").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });

                        $.each(r.data, function (key, value) {
                            wellBhaLookUp(value.well_bha_id);
                            wellBitLookUp(value.well_bit_id);

                        });


                    } else {
                        //cloneMudloggingData();
                    }

                    $('.row-delete').on('click', function () {
                        $(this).closest('.row-data').remove();
                    });
                }
            }).fail(function (r) {
            }).done(function () {
            });
        }

        //var drillingBha = function () {
        //    $.get($.helper.resolveApi('~/core/DrillingBha/' + $wellId.val() + '/detailByWellId'), function (r) {
        //        if (r.status.success) {
        //            $.helper.form.fill($formdrillingBHA, r.data);
        //            //drillingBhaComponent(r.data.id);
        //            //drillingBhaBit(r.data.id);
        //        }
        //        $('.loading-detail').hide()
        //    }).fail(function (r) {

        //    }).done(function () {

        //    });
        //}
        var gettotalBha = function (recordId) {
            // Mendapatkan template Handlebars
            var templateScript = $("#totalBha-template").html();
            // Mengkompilasi template Handlebars menjadi fungsi
            var template = Handlebars.compile(templateScript);

            $.get($.helper.resolveApi('~/core/wellbhacomponent/GetByWellBha/' + recordId), function (r) {
                // Mengatasi respons dari panggilan AJAX
                if (r.status.success) {
                    var totalBha = 0;
                    if (r.data && Array.isArray(r.data)) {
                        r.data.forEach(function (item) {
                            totalBha += item.length;
                        });
                    }
                    $("#BhaComponentContent").show();

                    // Menyiapkan objek JavaScript dengan totalBha untuk dimasukkan ke dalam template
                    var data = { totalBha: totalBha };

                    // Menghasilkan HTML dengan memasukkan data ke dalam template Handlebars
                    var html = template(data);

                    // Memasukkan HTML yang dihasilkan ke dalam elemen HTML yang diinginkan
                    $("#tableBhaComponent > tfoot").html(html);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                // Penanganan jika panggilan gagal
            }).done(function () {
                // Tindakan yang diambil setelah panggilan selesai, meskipun berhasil atau gagal
            });
        };

        var drillingBhaComponent = function (recordId) {
            var templateScript = $("#bhaComponent-template").html();
            var template = Handlebars.compile(templateScript);

            $.get($.helper.resolveApi('~/core/wellbhacomponent/GetByWellBha/' + recordId), function (r) {
                console.log(r);
                if (r.status.success) {
                    $("#BhaComponentContent").show();
                    $("#tableBhaComponent > tbody").html(template(r));

                    // Menghitung total dari properti 'leg'
                    var totalLeg = 0;
                    if (r.data && Array.isArray(r.data)) {
                        r.data.forEach(function (item) {
                            totalLeg += item.length;
                        });
                    }
                    //console.log("Total leg:", totalLeg);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                // Penanganan jika panggilan gagal
            }).done(function () {
                // Tindakan yang diambil setelah panggilan selesai, meskipun berhasil atau gagal
            });
        };



        var getActualLithology = function (recordId) {
            console.log(recordId);
            var templateScript = $("#ActualLithology-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillFormationsActual/' + recordId + '/detailbywellidNoRole'), function (r) {
                if (r.status.success) {
                    console.log("ini aAL", r.data)
                    $("#tableActualInformation > tbody").html(template({ data: r.data }));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        // TAB Daily Cost
        var drillingDeviationDt = function (recordId) {
            var templateScript = $("#drillingDeviation-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillingDeviation/' + recordId + '/detailByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingDeviation > tbody").html(template(r));
                }
            })
        };

        var getDrillingMud = function (recordId, mudType) {
            $.get($.helper.resolveApi('~/utc/welldata/getmud?drillingId=' + recordId + '&dataMudType=' + mudType), function (r) {
                if (r.status.success) {
                    if (mudType == "mud_1") {
                        $.helper.form.fill($form_mud1, r.data);
                        $("#form-mud1 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                    if (mudType == "mud_2") {
                        $.helper.form.fill($form_mud2, r.data);
                        $("#form-mud2 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                    if (mudType == "mud_3") {
                        $.helper.form.fill($form_mud3, r.data);
                        $("#form-mud3 #mud_time").val(timeFormat(r.data.mud_time));
                    }

                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingMudPit = function (recordId) {
            var templateScript = $("#readDrillingPit-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingmudpit/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingPit > tbody").html(template({ data: r.data }));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingTotalVolume = function (recordId) {
            $.get($.helper.resolveApi('~/core/drillingtotalvolume/' + recordId + '/detail'), function (r) {

                if (r.status.success) {
                    $.helper.form.fill($formTotalVolume, r.data);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };
        

        var getChemicalUse = function (recordId) {
            var templateScript = $("#readDrillingCUSED-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingchemicalused/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                   // console.log("dawdaidwidjwidwajd",r.data)
                    $("#tableDrillingCused > tbody").html(template({ data: r.data }));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingBulk = function (recordId, drilling_date) {
            var date = moment(drilling_date).format('YYYY-MM-DD')
            var templateScript = $("#drillingBulk-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingbulk/' + recordId + '/' + $wellId.val() + '/' + date + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableBulk > tbody").html(template({ data: r.data }));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingTransportById = function (recordId) {
            var templateScript = $("#readTransport-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingtransport/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableTransport > tbody").html(template({ data: r.data }));
                    //datepickerFormat($('.date-picker'));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingPersonelId = function (recordId) {
            var templateScript = $("#readDrillingPersonel-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingpersonel/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingPersonel > tbody").html(template({ data: r.data }));

                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getPlanVsCost = function () {
            var templateScript = $("#planVsCost-tempalte").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/TVDPlan/' + $wellId.val() + '/detailbywellid'), function (r) {
                console.log(r);
                if (r.status.success) {
                    $("#tablePlanVsCost > tbody").html(template({ data: r.data }));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

       

        //var getBhaBit = function (id) {
        //    var templateScript = $("#readBhaBit-template").html();
        //    var template = Handlebars.compile(templateScript);
        //    $.get($.helper.resolveApi('~/utc/welldata/getBhaBit/' + id), function (r) {
        //        if (r.status.success) {
        //            $("#tableBhaBit > tbody").html(template({ data: r.data }));

        //            $.each(r.data, function (key, value) {
        //                wellBhaLookUp(value.well_bha_id);
        //                wellBitLookUp(value.well_bit_id);
        //            });

        //            $('.row-delete').on('click', function () {
        //                $(this).closest('.rowBhaBit').remove();
        //            });
        //        }
        //    }).fail(function (r) {
        //    }).done(function () {
        //    });
        //}

        //var date_select = "";
        var wellDDR = function (date) {

            var Dates = new Date(date);
            var newdate = new Date(Dates);
            newdate.setDate(newdate.getDate() + 1);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1;
            if (mm < 10) mm = '0' + mm;
            var yyyy = newdate.getFullYear();
            var dateEnd = yyyy + '-' + mm + '-' + dd;
            var templateScript = $("#wellData-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drilling/getListDrilling/' + $wellId.val() + '/' + date + '/' + dateEnd + ''), function (r) {
                console.log('---');
                console.log(r);


                if (r.status.success && r.data.length > 0) {
                    r.data[0].spud_date = moment(r.data[0].spud_date).format('YYYY-MM-DD');
                    r.data[0].created_on = moment(date).format('YYYY-MM-DD');

                    var rn = r.data[0].report_no + 1;
                    $('input[name="report_no"]').val("#" + rn)

                    $("#tableWellDDR > tbody").html(template(r.data[0]));
                    $.helper.form.fill($formDrillingOperation, r.data[0]);
                    $.helper.form.fill($formHSSE, r.data[0]);
                    if (r.data[0].hsse_tbop_press === null) {
                        $('input[data-field="hsse_tbop_press"]').val('-')
                    }
                    if (r.data[0].hsse_tbop_func === null) {
                        $('input[data-field="hsse_tbop_func"]').val('-')
                    }
                    if (r.data[0].hsse_dkick === null) {
                        $('input[data-field="hsse_dkick"]').val('-')
                    }
                    if (r.data[0].hsse_dstrip === null) {
                        $('input[data-field="hsse_dstrip"]').val('-')
                    }
                    if (r.data[0].hsse_dfire === null) {
                        $('input[data-field="hsse_dfire"]').val('-')
                    }
                    if (r.data[0].hsse_dmis_pers === null) {
                        $('input[data-field="hsse_dmis_pers"]').val('-')
                    }
                    if (r.data[0].hsse_aband_rig === null) {
                        $('input[data-field="hsse_aband_rig"]').val('-')
                    }
                    if (r.data[0].hsse_dh2s === null) {
                        $('input[data-field="hsse_dh2s"]').val('-')
                    }
                    $.helper.form.fill($formMudloggingData, r.data[0]);
                    $.helper.form.fill($formDrillingWeather, r.data[0]);
                    drillingDeviationDt((r.data[0].id));
                    getDrillingMud((r.data[0].id), 'mud_1');
                    getDrillingMud((r.data[0].id), 'mud_2');
                    getDrillingMud((r.data[0].id), 'mud_3');
                    getDrillingTotalVolume((r.data[0].id));
                    getChemicalUse((r.data[0].id));
                    getActualLithology((r.data[0].well_id));
                    getDrillingMudPit((r.data[0].id));
                    getDrillingBulk((r.data[0].id), (r.data[0].drilling_date));
                    getDrillingTransportById((r.data[0].id));
                    getDrillingPersonelId((r.data[0].id));
                    drillingOperationActivity((r.data[0].id));
                    //getBhaBit((r.data[0].id));
                    GetWellObjectUomMap((r.data[0].wel_id));
                    getDrillingBhaBit((r.data[0].id));

                    $("#js_demo_accordion-4 > .content").show();
                    $("#js_demo_accordion-4 > .content-alert").hide();
                } else {
                    $("#js_demo_accordion-4 > .content-alert").show();
                    $("#js_demo_accordion-4 > .content").hide();
                    $("#js_demo_accordion-4 > .content-alert").text("No drilling data available on this date.");;
                }
                //console.log('act');
                //console.log(r.data[0].id);
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        // TAB BHA DETAILS
        var getDrillingDeviationByWellId = function () {
            var templateScript = $("#drillingDeviationBha-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingdeviation/' + $wellId.val() + '/listByWellId'), function (r) {
                console.log("drillingdeviation");
                console.log(r);
                if (r.status.success && r.data.length > 0) {
                    $("#tableDeviationData > tbody").html(template({ data: r.data }));
                    maskingMoney();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var getDrillingDeviationByWellIdEventClick = function () {
            $.get($.helper.resolveApi('~/core/drillingdeviation/' + $wellId.val() + '/listByWellId'), function (r) {
                //console.log(r);
                if (r.status.success && r.data.length > 0) {
                    $.each(r.data, function (key, value) {
                        var point = [value.tvd, value.e_w, value.n_s, 200];
                        deviationDrilling.push(point);
                    });
                }

                chart3d("", deviationWell, deviationDrilling);
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        //var getChildTree = function (data, hierarchy) {
        //    var space = 15 * hierarchy;
        //    data.forEach(item => {
        //        if (item.children.length > 0 || item.Currency == null) {
        //            var body = `<tr>
        //                <td><span style="padding-left: ${space}px">${item.Description}</span></td>
        //                <td></td>
        //                <td></td>
        //            </tr>`;

        //            $("#tableDailyCost > tbody").append(body);
        //            getChildTree(item.children, hierarchy + 1);
        //            //console.log(item.Description + hierarchy)
        //            //console.log("jajaja", hierarchy)
        //        } else {
        //            var body = `<tr>
        //                <td><span style="padding-left: ${space}px">${item.Description}</span></td>
        //                <td>${item.Currency} ${parseFloat(item.total_price_plan).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
        //                <td>${item.Currency} ${parseFloat(item.total_price_actual).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
        //            </tr>`;

        //            $("#tableDailyCost > tbody").append(body);
        //        }
        //    });
        //}
        //async function getCostByWellId() {
        //    $.get($.helper.resolveApi('~utc/WellData/' + $wellId.val() + '/dailyCost'), function (r) {
        //        var data = r.data;
        //        var data1 = data.children;
        //        //console.log("daily", data1);
        //        getChildTree(data.children, 1);
        //    });
        //}
        var getChildTree = function (data, hierarchy) {
            var space = 15 * hierarchy;
            var html = '';
            var totalPlan = 0;
            var totalActual = 0;

            data.forEach(item => {
                if (item.children.length > 0 || item.Currency == null) {
                    html += `<tr>
                <td><span style="padding-left: ${space}px">${item.Description}</span></td>
                <td></td>
                <td></td>
            </tr>`;

                    var childResult = getChildTree(item.children, hierarchy + 1);
                    html += childResult.html;
                    totalPlan += childResult.totalPlan;
                    totalActual += childResult.totalActual;
                } else {
                    var planCost = parseFloat(item.total_price_plan || 0);
                    var actualCost = parseFloat(item.total_price_actual || 0);

                    totalPlan += planCost;
                    totalActual += actualCost;

                    html += `<tr>
                <td><span style="padding-left: ${space}px">${item.Description}</span></td>
                <td>${item.Currency} ${planCost.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                <td>${item.Currency} ${actualCost.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
            </tr>`;
                }
            });

            // Return HTML and the calculated totals for this parent
            return { html, totalPlan, totalActual };
        }

        async function getCostByWellId() {
            try {
                const response = await $.get($.helper.resolveApi('~utc/WellData/' + $wellId.val() + '/dailyCost'));
                const data = response.data;

                $("#tableDailyCost > tbody").empty();

                // Ambil parent dengan deskripsi 'TANGIBLE COST'
                let tangibleParent = data.children.find(item => item.Description.toLowerCase() === 'tangible cost');

                // Pastikan parent Tangible Cost ditemukan dan memiliki children
                if (tangibleParent && tangibleParent.children) {
                    // Tampilkan parent 'TANGIBLE COST' di tabel
                    $("#tableDailyCost > tbody").append(`
                <tr>
                    <td><strong>${tangibleParent.Description}</strong></td>
                    <td></td>
                    <td></td>
                </tr>
            `);

                    // Proses dan tampilkan child items di bawah 'TANGIBLE COST'
                    const tangibleResult = getChildTree(tangibleParent.children, 1);

                    // Format hasil total biaya untuk Tangible
                    let totalPlanTangibleFormatted = tangibleResult.totalPlan.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    let totalActualTangibleFormatted = tangibleResult.totalActual.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                    // Tampilkan child items dan total Tangible Cost di tabel
                    $("#tableDailyCost > tbody").append(tangibleResult.html);
                    var totalRowTangible = `
            <tr>
                <td><strong>TOTAL TANGIBLE COST</strong></td>
                <td><strong>$ ${totalPlanTangibleFormatted}</strong></td>
                <td><strong>$ ${totalActualTangibleFormatted}</strong></td>
            </tr>`;
                    $("#tableDailyCost > tbody").append(totalRowTangible);
                }

                // Proses Intangible Cost jika diperlukan
                let intangibleParent = data.children.find(item => item.Description.toLowerCase() === 'intangible cost');

                if (intangibleParent && intangibleParent.children) {
                    // Tampilkan parent 'INTANGIBLE COST' di tabel
                    $("#tableDailyCost > tbody").append(`
                <tr>
                    <td><strong>${intangibleParent.Description}</strong></td>
                    <td></td>
                    <td></td>
                </tr>
            `);

                    // Proses dan tampilkan child items di bawah 'INTANGIBLE COST'
                    const intangibleResult = getChildTree(intangibleParent.children, 1);

                    // Format hasil total biaya untuk Intangible
                    let totalPlanIntangibleFormatted = intangibleResult.totalPlan.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    let totalActualIntangibleFormatted = intangibleResult.totalActual.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                    // Tampilkan child items dan total Intangible Cost di tabel
                    $("#tableDailyCost > tbody").append(intangibleResult.html);
                    var totalRowIntangible = `
            <tr>
                <td><strong>TOTAL INTANGIBLE COST</strong></td>
                <td><strong>$ ${totalPlanIntangibleFormatted}</strong></td>
                <td><strong>$ ${totalActualIntangibleFormatted}</strong></td>
            </tr>`;
                    $("#tableDailyCost > tbody").append(totalRowIntangible);
                }

            } catch (error) {
                console.error('Error fetching cost data:', error);
                $("#tableDailyCost > tbody").append('<tr><td colspan="3">Error fetching data. Please try again later.</td></tr>');
            }
        }


        $("#well_bha_id").on('select2:select', function (e) {
            var id = e.target.value;
            $("#BhaComponentContent").hide();
            wellBitLookupByWellBha(id);
            $('#well_bit_id').val(null).trigger('change');
        });

        var wellBitLookupByWellBha = function (wellBhaId) {
            $("#well_bit_id").cmSelect2({
                url: $.helper.resolveApi('~/utc/welldata/LookupByWellBha/' + wellBhaId),
                result: {
                    id: 'id',
                    text: 'bit_number'
                },
                filters: function (params) {
                    return [{
                        field: "bit_number",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "",
                    allowClear: true,
                    templateResult: function (repo) {
                        if (repo.loading) {
                            return repo.text;
                        }
                        return "<span class='fw-700'>BIT No: " + repo.text + "</span>";
                    },
                    templateSelection: function (data, container) {
                        if (data.text == "")
                            return "Please Select a BIT";
                        else {
                            return "<span class='fw-700'>BIT No: " + data.text + "</span>";
                        }
                    },
                }
            });

            $("#well_bit_id").on('select2:select', function (e) {
                //console.log(e.params.data);
                //getBhaComponent(wellBhaId, e.params.data);
                drillingBhaComponent(wellBhaId);
                gettotalBha(wellBhaId)
            });
        }

        $btnShowTrajectory.on('click', function () {
            if (!isShow)
                showTrajectory();
        });
        var showTrajectory = function () {
            loadWellDeviation();
        }

        function getMaxData(idData, dataArray) {
            var dates = [];
            for (var i = 0; i < dataArray.length; i++) {

                var dataWell = dataArray[i];

                for (var j = 0; j < dataWell.data.length; j++) {

                    var dataw = dataWell.data[j];
                    if (dataw.well_hole_and_casing_id == idData) {


                        dates.push(dataw);
                    }
                }

            }

            dates.sort(function (a, b) {
                var date1 = (new Date(moment(a.created_on).format('YYYYMMDDHHmm')));
                var date2 = (new Date(moment(b.created_on).format('YYYYMMDDHHmm')));
                return date1 - date2;
            });
            var highestDate = dates[dates.length - 1];
            var lowestDate = dates[0];
            return highestDate;
        }
        
        async function getWellHoleAndCasingPlanActual(wellHoleAndaCasingPlan) {
            holeAndCasing = wellHoleAndaCasingPlan;
            try {
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi("~/Core/DrillingOperationActivity/getByWellIdNew?wellId=" + $wellId.val()),

                    success: function (r) {

                        if (r.data.length > 0) {

                            var actualLatestHoleAndCasing = []
                            for (var i = 0; i < wellHoleAndaCasingPlan.length; i++) {

                                var idHoleAndCasing = wellHoleAndaCasingPlan[i].id;
                                var resultData = getMaxData(idHoleAndCasing, r.data);

                                actualLatestHoleAndCasing.push(resultData);
                            }


                            var top = 0;
                            var width = 120;
                            var left = 0;
                            var heightNew = 0;
                            var heightHole = 0
                            var tophole = 0

                            for (var i = 0; i < wellHoleAndaCasingPlan.length; i++) {

                                var holeDepth = i == 0 ? wellHoleAndaCasingPlan[0].hole_depth : wellHoleAndaCasingPlan[i].hole_depth - wellHoleAndaCasingPlan[i - 1].hole_depth;
                                var _height = (parseFloat(holeDepth) / parseFloat(plan_depth)) * 710;

                                if (i == 0) {
                                    heightHole = parseFloat(wellHoleAndaCasingPlan[i].hole_depth);
                                    tophole = 0;
                                } else {
                                    heightHole = parseFloat(wellHoleAndaCasingPlan[i].hole_depth);
                                    tophole = parseFloat(wellHoleAndaCasingPlan[i - 1].hole_depth);
                                }

                                heightNew = heightHole;

                                if (wellHoleAndaCasingPlan[i].actual_depth != null) {
                                    if (actualLatestHoleAndCasing[i].depth > heightNew) {
                                        heightNew = actualLatestHoleAndCasing[i].depth;
                                    }
                                }

                                //var html = ` <div class="drill-hole"
                                //                style="height: ` + (heightNew - parseFloat(tophole)) + `px; width: ` + width + `px;
                                //                top: ` + tophole + `px;  position: absolute;
                                //                left: ` + left + `px; background-color: #ffeb99;">`;
                                var html = ` <div class="drill-hole"
                                                style="height: ` + (heightNew - parseFloat(tophole)) + `px; width: ` + width + `px;
                                                background-color: #ffeb99; border-left: 3px solid #8f8f8f; border-right: 3px solid #8f8f8f; display: flex;">`;
                                var marginLabel = parseFloat(wellHoleAndaCasingPlan[i].hole_depth) < 1000 ? -70 : -95;

                                var rightSegitiga = ((width / 2) + 12) * -1;
                                var leftSegitiga2 = 0;
                                if (i > 2) {
                                    leftSegitiga2 = -95;
                                }
                                else {
                                    leftSegitiga2 = width * -1;
                                }

                                if (heightHole <= 50) {
                                    leftSegitiga2 = ((width / 2) + 14) * -1;
                                }

                                html += `<span class="fw-700" style="position: relative; top: ` + (heightHole - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; left: ` + marginLabel + `px; height: 18px;width:50px;">` + wellHoleAndaCasingPlan[i].hole_depth + ` ft</span>`;
                                if (wellHoleAndaCasingPlan[i].actual_depth != null) {
                                    html += `   <img src="` + $.helper.resolve("/Content/img/segitiga.png") + `" class="" style="position: relative; top: ` + (parseFloat(actualLatestHoleAndCasing[i].depth) - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; right: ` + rightSegitiga + `px; height: 18px;">`;
                                    html += `   <span class="fw-700" style="position: relative; top: ` + (parseFloat(actualLatestHoleAndCasing[i].depth) - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; right: ` + marginLabel + `px; height: 18px;">` + actualLatestHoleAndCasing[i].depth + ` ft</span>`;
                                }
                                //html += `   <div style="width:27px; position: absolute; top: ` + (heightHole - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; left: ` + leftSegitiga2 +`px;background-color: silver; opacity: 0.5"></div>`;
                                html += `<img src="` + $.helper.resolve("/Content/img/segitiga2.png") + `" class="" style="position: relative; top: ` + (heightHole - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; left: ` + leftSegitiga2 + `px; height: 18px;" id="tooltip` + i + `" title="` + wellHoleAndaCasingPlan[i].hole_depth + `ft, Hole ` + wellHoleAndaCasingPlan[i].hole_name + `in, Casing ` + wellHoleAndaCasingPlan[i].casing_name + `">`;


                                html += `</div>`;
                                $(".drill-body").append(html);
                                top = _height + top;
                                width -= 15;
                                left += 7.5;
                            }

                            if (heightNew > window.heightBackgroundGrey) {
                                window.heightBackgroundGrey = heightNew;
                            }
                        }
                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }
        
        async function getWellHoleAndCasingPlan() {
            try {
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi("~/Core/WellHoleAndCasing/" + $wellId.val() + "/getAllByWellId"),

                    success: function (r) {
                        if (r.data.length > 0) {
                            getWellHoleAndCasingPlanActual(r.data);
                        }
                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }
        return {
            init: function () {
                wellProfile();

                getPlanVsCost();

                getDrillingDeviationByWellId();

                getCostByWellId();

                loadWellDeviation();

                actualWell();

                displayAll();

                getWellHoleAndCasingPlan();

                //wellData();

                //getActualWell();

                // planWell();

            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });


});