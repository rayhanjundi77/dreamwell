﻿$(document).ready(function () {

    // Leason Learned
    {
        'use strict';
        var $btnPreview = $("#btnPreview");
        var $btnAnalyze = $("#btnAnalyze");
        var $btnAnalyzeDetails = $("#btnDetails");

        var $aphId = $('input[name=aph_id]').val();
        var $fieldId = "";
        var $iadcId = "";
        var $uom = "";
        var data = new Object();

        var pageFunction = function () {

            if ($aphId != '') {
                $fieldId = "";
                $("#field-section").removeClass("d-none");
            }


            var APHLookup = function () {
                $('#btn-business-unit-lookup').click(function () {
                    var btn = $(this);
                    btn.button('loading');
                    $("#field-lookup").button('reset');
                    $("#field_id").val('');
                    jQuery.ajax({
                        type: 'POST',
                        url: '/core/businessunit/Lookup',
                        success: function (data) {
                            btn.button('reset');
                            //$("#field-lookup").button('reset');
                            var $box = bootbox.dialog({
                                message: data,
                                title: "",
                                callback: function (e) {
                                    console.log(e);
                                }
                            });
                            $box.on("onSelected", function (o, event) {
                                $box.modal('hide');
                                $aphId = event.node.id;
                                $fieldId = "";
                                $("#field-section").removeClass("d-none");
                                $("#btn-business-unit-lookup").text("" + event.node.text);
                            });
                        }
                    });
                });
            }


            $('#field-lookup').click(function () {
                FieldLookup();
            });
            var FieldLookup = function () {
                var element = $("#field-lookup");
                element.button('loading');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/Asset/LookupFieldByBusinessUnit?id=' + $aphId,
                    success: function (data) {
                        element.button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "",
                            callback: function (e) {
                                console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {

                            if (event.node.parent == '#') {
                                alert('Please double click field only');
                                return;
                            } else {
                                $box.modal('hide');
                            }
                            $fieldId = event.node.id;
                            element.html(" " + event.node.text);

                            getWellByField($fieldId);
                        });
                    }
                });
            }

            var getWellByField = function (fieldId) {
                $("#well-section").removeClass("d-none");
                var templateScript = $("#well-template").html();
                var template = Handlebars.compile(templateScript);
                $.get($.helper.resolveApi('~/core/well/' + fieldId + '/detailByField'), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        if (r.data.length > 0) {
                            $("#listWell").html(template({ data: r.data }));
                            $('.select-all').on('click', function () {
                                $("#listWell").find(".custom-control-input").prop("checked", true);
                            });
                        } else {
                            $("#field-lookup").button('reset');
                            toastr.error("No Well Available on this Field.");
                        }
                    } else {
                        $("#groupWell").hide();
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }

            var iadcLookUp = function () {
                $('.iadc-lookup').click(function () {
                    var element = $(this);
                    element.button('loading');
                    jQuery.ajax({
                        type: 'POST',
                        url: '/core/Iadc/Lookup?isNPTOnly=true',
                        success: function (data) {
                            element.button('reset');
                            var $box = bootbox.dialog({
                                message: data,
                                title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                                callback: function (e) {
                                    //console.log('IADC');
                                    //console.log(e);
                                    //console.log(e);
                                }
                            });
                            $box.on("onSelected", function (o, event) {
                                $box.modal('hide');
                                if (event.node.original.type == "1") {
                                    toastr.error("Please choose Keyword with NPT type");
                                    $box.modal('show');
                                } else {
                                    element.html(event.node.text);
                                    $iadcId = event.node.id;
                                    $box.modal('hide');
                                }
                            });
                        }
                    });
                });
            }

            var validation = function () {

            }

            $btnPreview.on('click', function () {
                $("#preview-section").show();
                $("#analyze-section").hide();
                $("#analyzeDetail-section").hide();
                getAnalyze(true);
            });

            $btnAnalyze.on('click', function () {
                $("#preview-section").hide();
                $("#analyze-section").show();
                $("#analyzeDetail-section").hide();
                getAnalyze(false);
            });

            Handlebars.registerHelper('number', function (value) {
                return parseInt(value) + 1;
            });

            var uomSelect = function () {
                $("#uom_select").cmSelect2({
                    url: $.helper.resolveApi('~/core/uom/lookup/uomLength'),
                    result: {
                        id: 'uom_code',
                        text: 'uom_code'
                    },
                    filters: function (params) {
                        return [{
                            field: "uom_code",
                            operator: "contains",
                            value: params.term || '',
                        }];
                    },
                    options: {
                        placeholder: "Select a Unit of Measurement",
                        allowClear: true
                    }
                });

                $("#uom_select").on('select2:select', function (e) {
                    $uom = e.params.data.text;
                });
            }

            var getAnalyze = function (isPreview) {
                var isValid = true;
                if ($iadcId == "") {
                    isValid = false;
                    toastr.error("Please choose a Keyword First.");
                }
                if ($fieldId == "") {
                    isValid = false;
                    toastr.error("Please choose a Field First.");
                }
                if ($aphId == "") {
                    isValid = false;
                    toastr.error("Please choose an APH First.");
                }
                if ($aphId == "") {
                    isValid = false;
                    toastr.error("Please choose an UOM First.");
                }

                if (!isValid)
                    return;

                var templateScript = $("#preview-template").html();
                var template = Handlebars.compile(templateScript);

                var aryWell = [];
                $.each($("input[name='wellId[]']:checked"), function () {
                    aryWell.push($(this).val());
                });

                if (aryWell.length <= 0) {
                    toastr.error("Please choose a Well First.");
                    return;
                }

                data.well_id = new Object();
                data.well_id = aryWell;
                data.field_id = $fieldId;
                data.iadc_id = $iadcId;
                data.uom_select = $uom;

                $.post($.helper.resolveApi("~/UTC/LeasonLearned/GetAnalyze"), data, function (r) {
                    if (r.status.success) {
                        console.log('GetAnalyze');
                        console.log(r);
                        if (isPreview) {
                            $("#tablePreview > tbody").html(template({ data: r.data }));
                        } else {
                            var actual_cost = [];
                            var actual_depth = [];
                            var interval_npt = [];
                            var well_name = [];

                            if (r.data.length > 0) {
                                $.each(r.data, function (index, value) {
                                    actual_cost.push(value.actual_cost);
                                    actual_depth.push(value.actual_depth);
                                    interval_npt.push(value.interval);
                                    well_name.push(value.well_name);


                                    //actual_cost.push(1000);
                                    //actual_depth.push(value.actual_depth * 1.2);
                                    //interval_npt.push(30);
                                    //well_name.push(value.well_name + "CC");
                                });
                            }


                            var chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'analyze-preview',
                                    //margin: [50, 10, 60, 65],
                                    type: 'column',
                                    //height: 400,
                                },
                                title: {
                                    text: ''
                                },
                                subtitle: {
                                    text: ''
                                },
                                yAxis: [
                                    { // Primary yAxis
                                        floor: 0,
                                        //ceiling: 10000,
                                        title: {
                                            text: 'Footage (' + $uom + ')',
                                            style: {
                                                color: Highcharts.getOptions().colors[1]
                                            }
                                        },
                                        labels: {
                                            format: '{value}',
                                            style: {
                                                color: Highcharts.getOptions().colors[1]
                                            }
                                        },
                                        //reversed: true
                                    },
                                    { // Secondary yAxis
                                        floor: 0,
                                        title: {
                                            text: 'Cost (USD)',
                                            style: {
                                                color: Highcharts.getOptions().colors[0]
                                            }
                                        },
                                        labels: {
                                            format: '{value}',
                                            style: {
                                                color: Highcharts.getOptions().colors[0]
                                            }
                                        },
                                        //accessibility: {
                                        //    rangeDescription: 'Range: 1 to 99999'
                                        //},
                                        min: 0,
                                        opposite: true,
                                        //reversed: false
                                    },

                                    { // Secondary yAxis
                                        floor: 0,
                                        title: {
                                            text: 'NPT (Hours)',
                                            style: {
                                                color: Highcharts.getOptions().colors[0]
                                            }
                                        },
                                        labels: {
                                            format: '{value}',
                                            style: {
                                                color: Highcharts.getOptions().colors[0]
                                            }
                                        },
                                        //accessibility: {
                                        //    rangeDescription: 'Range: 1 to 99999'
                                        //},
                                        min: 0,
                                        opposite: true,
                                        //reversed: false
                                    }
                                ],
                                xAxis: {
                                    categories: well_name,
                                    title: {
                                        text: null
                                    }
                                },
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    x: 0,
                                    y: 10
                                },
                                plotOptions: {
                                    bar: {
                                        dataLabels: {
                                            enabled: true
                                        },
                                    }
                                },
                                //tooltip: {
                                //    pointFormat: '{series.name}: <b>{point.y} ' + data.uom_select + '</b><br/>',
                                //},
                                series:
                                    [
                                        {
                                            name: 'footage',
                                            marker: {
                                                symbol: 'diamond'
                                            },
                                            data: actual_depth
                                        },
                                        {
                                            name: 'Cost',
                                            type: 'line',
                                            marker: {
                                                symbol: 'diamond'
                                            },
                                            yAxis: 1,
                                            data: actual_cost
                                        },
                                        {
                                            name: 'NPT (Hours)',
                                            type: 'line',
                                            marker: {
                                                symbol: 'diamond'
                                            },
                                            yAxis: 2,
                                            data: interval_npt
                                        }
                                    ],
                                credits: {
                                    enabled: false
                                },

                            });
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            };

            $btnAnalyzeDetails.on('click', function () {
                $("#analyzeDetail-section").show();
                getAnalyzeDetails();
            });

            var getAnalyzeDetails = function () {
                $.post($.helper.resolveApi("~/UTC/LeasonLearned/GetAnalyzeDetail"), data, function (r) {
                    if (r.status.success) {
                        var well_name = [];

                        var aryTimeVsDepth = [];
                        var aryTimeVsCost = [];

                        var drilling = [];
                        var time_versus_cost = [];
                        var dtArrV = [];

                        if (r.data.length > 0) {
                            $.each(r.data, function (index, value) {
                                $.each(value.drilling, function (index, valuedetails) {
                                    aryTimeVsDepth.push([valuedetails.current_depth_md]);
                                });

                                $.each(value.time_versus_cost, function (index, valuedetails) {
                                    aryTimeVsCost.push([valuedetails.usage_percentage]);
                                });

                                $.each(value.drilling, function (index, val) {
                                    well_name.push([val.well_name]);
                                });

                                $.each(value.drilling, function (index, val) {
                                    drilling.push(val);
                                });

                                var aryTimeVsDepthNew = [];
                                var aryTimeVsCostNew = [];
                                var nameTvD = "";
                                var nameTvC = "";
                                $.each(value.drilling, function (index, valuedetails) {
                                    aryTimeVsDepthNew.push(valuedetails.current_depth_md);
                                    nameTvD = valuedetails.well_name;
                                    nameTvC = valuedetails.well_name;
                                });


                                $.each(value.time_versus_cost, function (index, valuedetails) {
                                    aryTimeVsCostNew.push(Math.round(valuedetails.usage_percentage * 100) / 100);
                                });
                                //name: 'Rainfall',
                                //    type: 'column',
                                //        yAxis: 1,
                                //            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
                                //                tooltip: {
                                //    valueSuffix: ' mm'
                                //}
                                var objD = {
                                    name: "Time VS Depth " + nameTvD,
                                    data: aryTimeVsDepthNew,
                                    visible: true,
                                    tooltip: {
                                        valueSuffix: ' ' + $uom
                                    },
                                    yAxis: 0

                                }
                                dtArrV.push(objD);

                                var objC = {
                                    name: "Time VS Cost " + nameTvC,
                                    data: aryTimeVsCostNew,
                                    visible: true,
                                    tooltip: {
                                        valueSuffix: ' %'
                                    },
                                    yAxis: 1
                                }
                                dtArrV.push(objC);
                            });
                        }

                        var group_to_values = drilling.reduce(function (obj, item) {
                            obj[item.well_name] = obj[item.well_name] || [];
                            obj[item.well_name].push(item.current_depth_md);
                            return obj;
                        }, {});

                        var groups = Object.keys(group_to_values).map(function (key) {
                            return { well_name: key, current_depth_md: group_to_values[key] };
                        });
                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'analyzedetail-preview',
                            },
                            title: {
                                text: ''
                            },

                            subtitle: {
                                text: ''
                            },

                            yAxis: [
                                { // Primary yAxis
                                    // floor: 0,
                                    // ceiling: 10000,
                                    title: {
                                        text: 'Kedalaman (MKU)',
                                        style: {
                                            color: Highcharts.getOptions().colors[1]
                                        }
                                    },
                                    labels: {
                                        format: '{value} ' + $uom,
                                        style: {
                                            color: Highcharts.getOptions().colors[1]
                                        }
                                    },
                                    reversed: true
                                },
                                { // Secondary yAxis
                                    // floor: 0,
                                    title: {
                                        text: 'Realisasi Biaya',
                                        style: {
                                            color: Highcharts.getOptions().colors[0]
                                        }
                                    },
                                    labels: {
                                        format: '{value}%',
                                        style: {
                                            color: Highcharts.getOptions().colors[0]
                                        }
                                    },
                                    //accessibility: {
                                    //    rangeDescription: 'Range: 1 to 99999'
                                    //},
                                    //min: 0,
                                    opposite: true,
                                    //max: 200
                                    //reversed: false
                                },
                            ],

                            xAxis: {
                                accessibility: {
                                    rangeDescription: 'Time vs Depth & Time vs Cost'
                                }
                            },

                            tooltip: {
                                //pointFormat: '{series.name}: <b>{point.y} ' + data.uom_select + '</b><br/>',
                                pointFormat: '{series.name}: <b>{point.y} </b><br/>',
                                //shared: true
                            },

                            legend: {
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'middle'
                            },

                            plotOptions: {
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    },
                                    pointStart: 1
                                }
                            },

                            series: dtArrV,

                            responsive: {
                                rules: [{
                                    condition: {
                                        // maxWidth: 500
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }

                        });
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            };

            return {
                init: function () {
                    APHLookup();
                    iadcLookUp();
                    uomSelect();
                }
            }
        }();

    }

    // Drilling Performance
    {
        var $btnFilter = $("#btnFilter");
        var $framedp = $("#framedp");
        var $framematerial = $("#framematerial");
        var $dt_dp = $('#dt_dp');

        var $filterType = 0;
        var $aphId_dp = $('input[name=aph_id_dp]').val();


        var $pageLoading = $('#page-loading-content');

        var pageFunction_dp = function () {
            if ($aphId_dp != '') {
                $("#formFilter").removeClass("d-none");
            }

            var APHLookup = function () {
                $('#btn-business-unit-lookup_dp').click(function () {
                    var btn = $(this);
                    btn.button('loading');
                    jQuery.ajax({
                        type: 'POST',
                        url: '/core/businessunit/Lookup',
                        success: function (data) {
                            btn.button('reset');
                            var $box = bootbox.dialog({
                                message: data,
                                title: "",
                                callback: function (e) {
                                }
                            });
                            $box.on("onSelected", function (o, event) {
                                $box.modal('hide');
                                $aphId_dp = event.node.id;
                                $("#formFilter").removeClass("d-none");
                                $("#btn-business-unit-lookup_dp").text("" + event.node.text);
                            });
                        }
                    });
                });
            }

            var dataYear = [];
            var currentYear = new Date().getFullYear();
            var year = 2011;
            while (currentYear >= year) {
                dataYear.push(currentYear);
                currentYear--;
            }
            $("#filteryear").select2({
                tags: true,
                tokenSeparators: [',', ' '],
                data: dataYear
            })

            var HidePanel = function () {
                $(".panel-report").hide();
                $framedp.hide();
                $framematerial.hide();
            }

            $btnFilter.on('click', function () {
                var isValid = true;

                HidePanel();

                var $filteryear = $("#filteryear").val();

                if ($filteryear.length == 0) {
                    toastr.error("Please select Year to show details");
                    isValid = false;
                }

                if (!isValid)
                    return;
                else {
                    loaddata($aphId_dp, $filteryear);
                }
            });


            var loaddata = function (aphId, ayear) {

                var data = {
                    recordId: aphId,
                    ayear: ayear
                };

                loadDPGrid(data);
                loadMaterialGrid(data);
            };

            var loadDPGrid = function (vparam) {
                var no = 0;
                var dt = $dt_dp.DataTable({
                    destroy: true,
                    paging: false,
                    responsive: true,
                    serverSide: false,
                    ajax: function (data, callback, settings) {
                        $.ajax({
                            url: $.helper.resolveApi("~/UTC/ServicePerformance/WellDrillingPerformance"),
                            type: 'POST',
                            data: vparam,
                            success: function (r) {
                                if (r.status.success) {
                                    console.log(r);
                                    callback({ data: r.data });
                                    if (r.data.length > 0) {
                                        $framedp.show();
                                    }
                                }
                            }
                        });
                    },
                    sDom: "",
                    order: [[1, 'asc']],
                    columns: [
                        {
                            data: "asset_code",
                            orderable: false,
                            searchable: false,
                            class: "text-center",
                            render: function (data, type, row, meta) {
                                if (row["well_name"] == undefined) {
                                    no = no + 1;
                                    return no;
                                }
                                return "";
                            }
                        },
                        {
                            data: "field_name",
                            orderable: false,
                            searchable: false,
                            class: "text-center"
                        },
                        {
                            data: "pt_hr",
                            orderable: false,
                            searchable: false,
                            class: "text-center"
                        },
                        {
                            data: "npt_hr",
                            orderable: false,
                            searchable: false,
                            class: "text-center"
                        },
                        {
                            data: "cummulative_cost_plan",
                            orderable: false,
                            searchable: false,
                            class: "text-center"
                        },
                        {
                            data: "cummulative_cost_actual",
                            orderable: false,
                            searchable: false,
                            class: "text-center"
                        },
                        {
                            data: "depth_plan",
                            orderable: false,
                            searchable: false,
                            class: "text-center"
                        },
                        {
                            data: "depth_actual",
                            orderable: false,
                            searchable: false,
                            class: "text-center"
                        }
                    ],
                    initComplete: function (settings, json) {
                        dt.rows().every(function () {
                            var data = this.data();
                            var wellList = data["well_list"];
                            if (wellList) {

                            }
                            for (var i = 0; i < wellList.length; i++) {
                                var item = wellList[i];
                                item["field_name"] = item["well_name"];
                                this.row.add(item).draw();
                            }
                        });
                    }
                }, function (e, settings, json) {
                });

                dt.on('processing.dt', function (e, settings, processing) {
                    $pageLoading.loading('start');
                    if (processing) {
                        $pageLoading.loading('start');
                    } else {
                        $pageLoading.loading('stop')
                    }
                });

                dt.on('order.dt', function (e, settings, processing) {
                    var number = 0;
                    dt.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                        if (cell.innerHTML.length > 0) {
                            cell.innerHTML = i + 1 - number;
                        } else {
                            number++;
                        }
                    });
                });
            };

            var loadMaterialGrid = function (vparam) {
                var templateScript = $("#material-template").html();
                var template = Handlebars.compile(templateScript);

                $.ajax({
                    url: $.helper.resolveApi("~/UTC/ServicePerformance/MaterialUssagePerformance"),
                    type: 'POST',
                    data: vparam,
                    success: function (r) {
                        //console.log(r.data);
                        if (r.status.success) {
                            if (r.data.fields.length > 0 || r.data.pivotData.length > 0) {
                                $framematerial.show();
                                $("#listmaterial").html(template({ fields: r.data.fields, pivotData: r.data.pivotData }));
                            }
                        }
                    }
                });
            };
            return {
                init: function () {
                    APHLookup();
                }
            }
        }();
    }

    $(document).ready(function () {
        pageFunction.init();
        pageFunction_dp.init();
    });
});

