﻿$(document).ready(function () {
    'use strict';
    var $recordId = $("#wellId");

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                var loading = `<div class="spinner-border" role="status" style="width: 4rem; height: 4rem;">
                                    <span class="sr-only">Loading...</span>
                               </div><div class="mt-4"></div>`;
                $("#operationHours").html(loading);

                $.get($.helper.resolveApi('~/UTC/SingleWell/GetWellIadc/' + $recordId.val()), function (r) {
                    if (r.status.success) {
                        var aryPercentagePT = [];
                        var aryPercentageNPT = [];
                        var date = [];
                        if (r.data.length > 0) {
                            $.each(r.data, function (index, value) {
                                date.push(moment(value.drilling_date).format('DD MMM'));
                                var percentage_pt = 0.0;
                                var percentage_npt = 0.0;
                                if (value.total_hours > 0) {
                                    if (value.total_hours_pt > 0) {
                                        //percentage_pt = (parseFloat(value.total_hours_pt) / (parseFloat(value.total_hours))) * 100;
                                        percentage_pt = value.total_hours_pt;
                                    }
                                    if (value.total_hours_npt > 0) {
                                        //percentage_npt = (parseFloat(value.total_hours_npt) / (parseFloat(value.total_hours))) * 100;
                                        percentage_npt = value.total_hours_npt;
                                    }
                                }

                                aryPercentagePT.push(percentage_pt);
                                aryPercentageNPT.push(percentage_npt);
                            });
                            $('#operationHours').highcharts({
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: 'Operation Hours'
                                },
                                xAxis: {
                                    categories: date,
                                    title: {
                                        text: null
                                    },
                                    labels: {
                                        rotation: -45,
                                        style: {
                                            fontSize: '10px',
                                            fontFamily: 'Verdana, sans-serif'
                                        }
                                    }
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'Operation Hours'
                                    },
                                    stackLabels: {
                                        enabled: false,
                                        style: {
                                            fontWeight: 'bold',
                                        }
                                    }
                                },
                                tooltip: {
                                    formatter: function () {
                                        var point = this.point;
                                        point.y2 = this.series.options.data[point.index];

                                        return '<b>' + this.series.options.name + '<b/><br/>Usage: ' + Highcharts.numberFormat(point.y2, 2) + ' Hrs';
                                    }
                                },
                                plotOptions: {
                                    column: {
                                        stacking: 'normal',
                                        dataLabels: {
                                            enabled: false
                                        }
                                    }
                                },
                                series: [{
                                    name: "PT",
                                    data: aryPercentagePT
                                },
                                {
                                    name: "NPT",
                                    data: aryPercentageNPT
                                }]
                            });
                        } else {
                            $("#ptAndNptHours-bar").html("No Record Available");
                        }



                        return;
                        $.each(r.data, function (key, value) {
                            console.log(value);
                            //date.push(moment(value.drilling_date).format('DD MMM'));
                            if (parseInt(value.total_pt) + parseInt(value.total_npt) > 0) {
                                var percentage_pt = (value.total_pt / (value.total_pt + value.total_npt)) * 100;
                                var percentage_npt = (value.total_npt / (value.total_pt + value.total_npt)) * 100;
                                aryPT.push(parseFloat(percentage_pt));
                                aryNPT.push(parseFloat(percentage_npt));
                            } else {
                                aryPT.push(0);
                                aryNPT.push(0);
                            }
                        })

                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'operationHours',
                                margin: [50, 10, 60, 65],
                                type: 'column',
                                height: 400,
                            },
                            title: {
                                text: 'Average ROP'
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                //categories: ,
                                title: {
                                    text: null
                                }
                            },
                            series: [
                                {
                                    name: "PT",
                                    data: series
                                }, {
                                    name: "NPT",
                                    data: aryNPT
                                }
                            ],
                            yAxis: {
                                min: 0,
                                title: {
                                    text: ''
                                },
                                stackLabels: {
                                    enabled: false,
                                    style: {
                                        fontWeight: 'bold',
                                    }
                                }
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.y:.2f}%</b><br/>',
                            },
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    }
                                }
                            },
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 10
                            },
                            credits: {
                                enabled: false
                            },
                        });
                    }
                });
            } else {

            }
        };

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});