﻿$(document).ready(function () {
    'use strict';
    window.depthuom = "";
    var $recordId = $("#wellId");

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                var loading = `<div class="spinner-border" role="status" style="width: 4rem; height: 4rem;">
                                    <span class="sr-only">Loading...</span>
                               </div><div class="mt-4"></div>`;
                $("#waktu-pengeboran").html(loading);

                $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $recordId.val()), function (r) {
                    if (r.status.success && r.data.length > 0) {
                        $.each(r.data, function (key, value) {
                            if ('tvd depth' == (value.object_name || '').toLowerCase().trim()) {
                                window.depthuom = value.uom_code;
                            }
                        });
                    }

                    $.get($.helper.resolveApi('~/UTC/SingleWell/GetTataWaktuPengeboranNew/' + $recordId.val()), function (r) {
                        console.log('GetTataWaktuPengeboranNew');
                        console.log(r);
                        if (r.status.success) {
                            var date = [];
                            var days = [];
                            var actualDepth = [];
                            var dailyCost = [];
                            var tvdDepth = [];
                            var cumCost = [];
                            var arrtvdDepth = [];
                            var arrcumCost = [];
                            var drillingActivity = [];
                            var cost = 0;
                            $.each(r.data.report_drilling, function (key, value) {
                                console.log(value);
                                date.push(moment(value.drilling_date).format('YYYY-MM-DD'));
                                days.push(value.dfs == 1 ? 1 : value.dfs);
                                var _actualDepth = [value.dfs, value.current_depth_md == null ? 0 : value.current_depth_md];
                                actualDepth.push(_actualDepth);
                                cost += value.daily_cost;
                                cumCost.push(cost);
                                // var _actualCost = [value.dfs, value.daily_cost == null ? 0 : value.daily_cost];
                                // dailyCost.push(_actualCost);

                                if (value.operation_activity.length > 0) {
                                    $.each(value.operation_activity, function (keyDOA, valueDOA) {
                                        drillingActivity.push(valueDOA.description);
                                    });
                                }
                            });

                            $.each(r.data.tvd_plan, function (key, value) {
                                var _planningDepth = [value.days, value.depth];
                                arrtvdDepth.push(_planningDepth);

                                var _planningCost = [value.days, value.cumm_cost];
                                arrcumCost.push(_planningCost);
                            })

                            $.each(r.data.drilling_data, function (key, value) {
                                var _actualCost = [value.cummulative_cost_count == null ? 0 : value.cummulative_cost_count];
                                dailyCost.push(_actualCost);
                            })

                            $('#waktu-pengeboran').highcharts({
                                title: {
                                    text: ''
                                },

                                subtitle: {
                                    text: ''
                                },
                                credits: {
                                    enabled: false
                                },
                                yAxis: [{ // Primary yAxis
                                    floor: 0,
                                    ceiling: 10000,
                                    labels: {
                                        format: '{value} ' + window.depthuom,
                                        style: {
                                            color: Highcharts.getOptions().colors[1]
                                        }
                                    },
                                    title: {
                                        text: 'Depth',
                                        style: {
                                            color: Highcharts.getOptions().colors[1]
                                        }
                                    },
                                    reversed: true
                                }, { // Secondary yAxis
                                    floor: 0,
                                    title: {
                                        text: 'Cost',
                                        style: {
                                            color: Highcharts.getOptions().colors[0]
                                        }
                                    },
                                    labels: {
                                        format: '{value} USD',
                                        style: {
                                            color: Highcharts.getOptions().colors[0]
                                        }
                                    },
                                    opposite: true
                                    //reversed: true
                                }],
                                xAxis: {
                                    floor: 0,
                                    ceiling: 10000,
                                    labels: {
                                        format: '{value}',
                                        step: 0
                                    },
                                    title: {
                                        text: 'Days'
                                    },
                                    accessibility: {
                                        rangeDescription: 'Range: 0 to 200'
                                    }
                                },

                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                },
                                tooltip: {
                                    //pointFormatter: function () {
                                    //    var seriesNameConverter = {
                                    //        'Seriesonename': 'Series One Name',
                                    //        'Seriestwoname': 'Series Two Name'
                                    //    };

                                    //    return '<span style="color:{point.color}">\u25CF</span> '
                                    //        + seriesNameConverter[this.series.name] + ': <b>' + this.y + '</b><br/>';

                                    //    console.log(this.series.name);
                                    //},
                                    formatter: function () {
                                        var keyInt = this.key - 1;
                                        var html = '<span style="font-size:10px"><b>' + this.series.name + '</b></span><br/>';
                                        if (this.series.name == "Actual Depth") {
                                            html += '<span style="font-size:10px">' + this.key + ' Days</span><br/>';
                                        } else if (this.series.name == "Planning Depth") {
                                            html += '<span style="font-size:10px">' + this.key + ' Days</span><br/>';
                                            html += '<span style="font-size:10px">' + this.y + ' ' + window.depthuom + '</span><br/>';
                                        } else if (this.series.name == "Planning Cost") {
                                            html += '<span style="font-size:10px">' + this.y.toFixed(2) + '$</span><br/>';
                                        }
                                        if (r.data.drilling_data[keyInt] && this.series.name != "Planning Depth" && this.series.name != "Planning Cost") {
                                            //if (r.data.drilling_data[this.key].cummulative_cost_count != null) {
                                            //    var cummulative_cost_count = r.data.drilling_data[this.key].cummulative_cost_count.toFixed(2);
                                            //} else {
                                            //    var cummulative_cost_count = "-";
                                            //}
                                            //html += '<span style="font-size:10px">' + cummulative_cost_count + ' $</span><br/>';
                                            html += '<span style="font-size:10px">' + cumCost[keyInt].toFixed(2) + ' $</span><br/>';
                                            html += '<span style="font-size:10px">' + r.data.drilling_data[keyInt].current_depth_md + ' ' + window.depthuom + '</span><br/>';

                                        }
                                        if (this.series.name == "Actual Depth")
                                            html += '<span style="font-size:10px"><b>Drilling Operation Activity</b></span><br/><br/>';

                                        if (r.data.report_drilling[keyInt]) {
                                            if (r.data.report_drilling[keyInt].operation_activity.length > 0 && this.series.name == "Actual Depth") {
                                                html += '<span style="font-size:10px">' + r.data.drilling_data[keyInt].operation_data_period + '</span><br/><br/>';
                                            }
                                        }

                                        return html;
                                    }

                                },
                                plotOptions: {
                                    series: {

                                        label: {
                                            connectorAllowed: false
                                        },
                                        pointStart: 0,
                                    }
                                },

                                series: [{
                                    name: 'Actual Depth',
                                    data: actualDepth,

                                },
                                {
                                    name: 'Cummulative Cost',
                                    yAxis: 1,
                                    data: cumCost
                                },
                                {
                                    name: 'Planning Depth',
                                    data: arrtvdDepth
                                },
                                {
                                    name: 'Planning Cost',
                                    yAxis: 1,
                                    data: arrcumCost
                                }],

                                responsive: {
                                    rules: [{
                                        condition: {
                                            maxWidth: 1000
                                        },
                                        chartOptions: {
                                            legend: {
                                                layout: 'horizontal',
                                                align: 'center',
                                                verticalAlign: 'bottom'
                                            }
                                        }
                                    }]
                                }
                            });
                        }
                    });
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });

                $.get($.helper.resolveApi('~/UTC/SingleWell/GetTataWaktuPengeboran/' + $recordId.val()), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        var planningDepth = [];
                        var planningCost = [];

                        var actualDepth = [];
                        var actualCost = [];

                        $.each(r.data.report_drilling, function (key, value) {

                            var _actualDepth = [key, value.current_depth_md];
                            actualDepth.push(_actualDepth);

                            var _actualCost = [key, value.daily_cost];
                            actualCost.push(_actualCost);
                        })

                        $.each(r.data.tvd_plan, function (key, value) {
                            var _planningDepth = [value.days, value.depth];
                            planningDepth.push(_planningDepth);

                            var _planningCost = [value.days, value.cumm_cost];
                            planningCost.push(_planningCost);
                        })
                        console.log(actualDepth);
                        // $('#waktu-pengeboran').highcharts({
                        //     title: {
                        //         text: 'Tata Waktu Pengeboran'
                        //     },

                        //     subtitle: {
                        //         text: ''
                        //     },
                        //     credits: {
                        //         enabled: false
                        //     },
                        //     yAxis: [{ // Primary yAxis
                        //         floor: 0,
                        //         ceiling: 10000,
                        //         labels: {
                        //             format: '{value} ft',
                        //             style: {
                        //                 color: Highcharts.getOptions().colors[1]
                        //             }
                        //         },
                        //         title: {
                        //             text: 'Depth',
                        //             style: {
                        //                 color: Highcharts.getOptions().colors[1]
                        //             }
                        //         },
                        //         reversed: true

                        //     }, { // Secondary yAxis
                        //         floor: 0,
                        //         title: {
                        //             text: 'Cost',
                        //             style: {
                        //                 color: Highcharts.getOptions().colors[0]
                        //             }
                        //         },
                        //         labels: {
                        //             format: '{value} USD',
                        //             style: {
                        //                 color: Highcharts.getOptions().colors[0]
                        //             }
                        //         },
                        //         //accessibility: {
                        //         //    rangeDescription: 'Range: 1 to 99999'
                        //         //},
                        //         //min: 0,
                        //         opposite: true,
                        //         //reversed: false
                        //     }],
                        //     xAxis: {
                        //         floor: 0,
                        //         ceiling: 10000,
                        //         labels: {
                        //             format: '{value}',
                        //             step: 0
                        //         },
                        //         title: {
                        //             text: 'Days'
                        //         },
                        //         accessibility: {
                        //             rangeDescription: 'Range: 0 to 99999'
                        //         },
                        //         min: 0
                        //     },
                        //     legend: {
                        //         layout: 'horizontal',
                        //         align: 'center',
                        //         verticalAlign: 'bottom'
                        //     },
                        //     tooltip: {
                        //         formatter: function (d) {
                        //             return '<b>' + this.series.name + ': </b> ' + this.y + '<br/>';
                        //         }
                        //     },
                        //     plotOptions: {
                        //         series: {
                        //             label: {
                        //                 connectorAllowed: false
                        //             },
                        //             pointStart: 0
                        //         }
                        //     },
                        //     series:
                        //         [
                        //             {
                        //                 name: 'Planning Depth',
                        //                 marker: {
                        //                     symbol: 'square'
                        //                 },
                        //                 data: planningDepth
                        //             },
                        //             {
                        //                 name: 'Actual Depth',
                        //                 marker: {
                        //                     symbol: 'diamond'
                        //                 },
                        //                 data: actualDepth
                        //             },
                        //             {
                        //                 name: 'Planning Cost',
                        //                 marker: {
                        //                     symbol: 'square'
                        //                 },
                        //                 yAxis: 1,
                        //                 data: planningCost
                        //             },
                        //             {
                        //                 name: 'Actual Cost',
                        //                 marker: {
                        //                     symbol: 'diamond'
                        //                 },
                        //                 yAxis: 1,
                        //                 data: actualCost
                        //             }
                        //         ],
                        //     responsive: {
                        //         rules: [{
                        //             condition: {
                        //                 maxWidth: 500
                        //             },
                        //             chartOptions: {
                        //                 legend: {
                        //                     layout: 'horizontal',
                        //                     align: 'center',
                        //                     verticalAlign: 'bottom'
                        //                 }
                        //             }
                        //         }]
                        //     }
                        // });
                    }
                });
            } else {

            }
        };

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});