﻿$(document).ready(function () {
    'use strict';
    var $recordId = $("#wellId");

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                var loading = `<div class="spinner-border" role="status" style="width: 4rem; height: 4rem;">
                                    <span class="sr-only">Loading...</span>
                               </div><div class="mt-4"></div>`;
                $("#trajecttorySideView").html(loading);

                $.get($.helper.resolveApi('~/UTC/SingleWell/Get2DTrajectory/' + $recordId.val()), function myfunction(r) {
                    if (r.status.success) {
                        var plan = [];
                        var actual = [];

                        if (r.data.well_deviation.length > 0) {
                            $.each(r.data.well_deviation, function (index, value) {
                                var _plan = [value.v_section, value.tvd];
                                plan.push(_plan);
                            });
                        }
                        if (r.data.drilling_deviation.length > 0) {
                            $.each(r.data.drilling_deviation, function (index, value) {
                                var _actual = [value.v_section, value.tvd];
                                var haha = _actual[1] / 10;
                                //console.log("ini sususus", haha)
                                actual.push(_actual);
                            });
                        }

                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'trajecttorySideView',
                                type: 'spline',
                                inverted: false
                            },
                            title: {
                                text: '2D Trajectory (Side View)'
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                //reversed: true,
                                floor: 0,
                                tickInterval: 200,
                                title: {
                                    text: ''
                                },
                            },
                            yAxis: {
                                floor: 0,
                                tickInterval: 100,
                                alignTicks: false,
                                reversed: true,
                                title: {
                                    text: ''
                                }
                            },
                            legend: {
                                enabled: true
                            },
                            tooltip: {
                                headerFormat: '',
                                pointFormat: '<b>V Section</b>: {point.x} mkt<br/> <b>TVD</b>: {point.y} ft'
                            },
                            plotOptions: {
                                spline: {
                                    marker: {
                                        radius: 4,
                                        lineColor: 'rgb(0, 115, 183)',
                                        lineWidth: 1
                                    }
                                }
                            },
                            series:
                                [{

                                    name: 'Plan',
                                    marker: {
                                        symbol: 'square'
                                    },
                                    data: plan
                                }, {
                                    name: 'Actual',
                                    marker: {
                                        symbol: 'diamond'
                                    },
                                    data: actual
                                }],
                            credits: {
                                enabled: false
                            },
                        });
                    }
                });
            } else {

            }
        };

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});