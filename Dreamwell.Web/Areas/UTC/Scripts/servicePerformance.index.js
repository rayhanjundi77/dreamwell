﻿$(document).ready(function () {
    'use strict';
    var $btnFilter = $("#btnFilter");

    var $filterType = 0;
    var $chartType = 0
    var $aphId = $('input[name=aph_id]').val();
    var $fieldId = "";

    var $contentView = $("#view-list");
    var $tableJobPersonel = $("#tableJobPersonel");
    var $dt_jobPersonel = $('#dt_jobPersonel');
    
    var $framenpt = $("#framenpt");
    var $framemudcostbyft = $("#framemudcostbyft");
    var $framemudcostbybbl = $("#framemudcostbybbl");


    var $pageLoading = $('#page-loading-content');
    var $dt_nptdetail = $('#dt_nptdetail');


    var pageFunction = function () {
        if ($aphId != '') {
            $("#field-lookup").button('reset');
            $("#field_id").val('');
            $("#formFilter").removeClass("d-none");
        }

        var APHLookup = function () {
            $('#btn-business-unit-lookup').click(function () {
                var btn = $(this);
                btn.button('loading');
                $("#field-lookup").button('reset');
                $("#field_id").val('');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/businessunit/Lookup',
                    success: function (data) {
                        btn.button('reset');
                        //$("#field-lookup").button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "",
                            callback: function (e) {
                                //console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {
                            $box.modal('hide');
                            $aphId = event.node.id;
                            //console.log($aphId);
                            $("#formFilter").removeClass("d-none");
                            $("#btn-business-unit-lookup").text("" + event.node.text);
                        });
                    }
                });
            });
        }
        $('#field-lookup').click(function () {
            var element = $(this);
            element.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/Asset/LookupFieldByBusinessUnit?id=' + $aphId,
                success: function (data) {
                    element.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "",
                        callback: function (e) {
                            //console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o, event) {

                        if (event.node.parent == '#') {
                            alert('Please double click field only');
                            return;
                        } else {
                            $box.modal('hide');
                        }
                        $fieldId = event.node.id;
                        element.html(" " + event.node.text);

                        getWellByField($fieldId);
                    });
                }
            });
        });

        var getWellByField = function (wellid) {
            var templateScript = $("#well-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/well/' + wellid + '/detailByField'), function (r) {
                //console.log(r);
                if (r.status.success) {
                    $("#groupWell").show();
                    if (r.data.length > 0) {
                        if ($("#field-" + r.data[0].field_id).length == 0) {
                            //$("#listWell").append(template({ data: r.data }));
                            $("#listWell").html(template({ data: r.data }));
                            $('.remove-field').on('click', function () {
                                $("#field-" + $(this).data("fieldid")).remove();
                            });
                            $('.select-all').on('click', function () {
                                $("#listWell").find(".custom-control-input").prop("checked", true);
                            });

                        } else {
                            HidePanel();
                            toastr.error("Field is Available on the list.");
                        }
                    } else {
                        $("#field-lookup").button('reset');
                        $("#listWell").empty();
                        $("#groupWell").hide();
                        HidePanel();

                        toastr.error("No Well Available on this Field.");
                    }
                } else {
                    $("#groupWell").hide();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var HidePanel = function () {
            $contentView.empty();
            $framemudcostbybbl.hide();
            $framemudcostbyft.hide();
            return;
            $(".panel-report").hide();
            $tableJobPersonel.hide();
            $framenpt.hide();
        }

        $btnFilter.on('click', function () {
            var isValid = true;

            HidePanel();

            var totalWellChecked = $('input[name="wellId[]"]:checked').length;
            if (totalWellChecked <= 0) {
                toastr.error("Please select Well to show details");
                isValid = false;
            }

            if (!isValid)
                return;

            if (totalWellChecked > 0) {
                loaddata();
            }
        });


        var loaddata = function () {
            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                var wellid = $(this).val();
                loadJobPersonel(wellid);
                aryWell.push(wellid);
            });

            var vtipe = [];
            console.log($("input[name='serviceperformance[]']:checked"));
            $.each($("input[name='serviceperformance[]']:checked"), function () {
                $filterType = $(this).val();
                if ($filterType == 1 || $filterType == 2) {
                    vtipe.push($filterType);
                }
            })
            console.log(vtipe);
            loaddataMudftbbl(vtipe, aryWell);
        };
        var loadJobPersonel = function (wellid) {
            $.get($.helper.resolveApi('~/core/well/' + wellid + '/detail'), function (r) {
                if (r.status.success) {
                    //console.log(r.data);
                    if (r.data != null) {
                        var contentId = "content-" + wellid;
                        var clone = $("#content-clone").clone();
                        clone.attr("id", "content-" + wellid).show().appendTo($contentView);

                        $("#content-" + wellid).find(".wellname").html(r.data.well_name);
                        $("#content-" + wellid).find(".contractorname").html(r.data.contractor_name);

                        loadJobPersonalGrid(wellid);
                        loadchartselected(wellid);

                    } else {
                        //HidePanel();
                        toastr.error("Well not found");
                    }
                } else {
                }
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }
        var loadchartselected = function (wellid) {
            var vtipe = [];
            $.each($("input[name='serviceperformance[]']:checked"), function () {
                $filterType = $(this).val();
                if ($filterType == 0) {
                    $framenpt.show();
                    $("#content-" + wellid).find(".framenpt").show();
                    loadNPTGrid(wellid);
                }
                else if ($filterType == 1 || $filterType == 2) {
                    vtipe.push($filterType);
                }
            });
            //loaddataMudftbbl(vtipe);

        };

        var loadJobPersonalGrid = function (wellid) {
            var dt = $("#content-" + wellid).find(".dt_jobPersonel").dataTable({
                destroy: true,
                responsive: true,
                serverSide: false,
                ajax: function (data, callback, settings) {
                    $.ajax({
                        url: $.helper.resolveApi("~/UTC/ServicePerformance/" + wellid + "/JobPersonel"),
                        type: 'POST',
                        contentType: "application/json",
                        data: function (d) {
                            return JSON.stringify(d);
                        },
                        success: function (data) {
                            callback({ data: data });
                        }
                    });
                },
                sDom: "",
                order: [],
                columns: [
                    {
                        data: "personel",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    },
                    {
                        data: "company",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    }
                ],
                initComplete: function (settings, json) {

                }
            }, function (e, settings, json) {
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        };

        var loadNPTGrid = function (wellid) {
            var dt = $("#content-"+wellid).find(".dt_nptdetail").dataTable({
                destroy: true,
                responsive: true,
                serverSide: false,
                ajax: function (data, callback, settings) {
                    $.ajax({
                        url: $.helper.resolveApi("~/UTC/ServicePerformance/NPT/Details/" + wellid),
                        type: 'POST',
                        contentType: "application/json",
                        data: function (d) {
                            return JSON.stringify(d);
                        },
                        success: function (data) {
                            if (data.status.success) {
                                callback({ data: data.data });
                                LoadNPTChart(data.data, wellid);
                            }
                        }
                    });
                },
                sDom: "",
                order: [],
                columns: [
                    {
                        data: "parent_code",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    },
                    {
                        data: "description",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            return 'NPT';
                        }
                    },
                    {
                        data: "iadc_code",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    },
                    {
                        data: "hours",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    },
                    {
                        data: "days",
                        orderable: false,
                        searchable: false,
                        class: "text-left"
                    }
                ],
                initComplete: function (settings, json) {

                }
            }, function (e, settings, json) {
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        };

        var LoadNPTChart = function (vdata, wellid) {
            var datachart = [];
            var _data = [];
            $("#content-" + wellid).find(".npt-chart").attr("id", "npt-chart-" + wellid);
            if (vdata.length > 0) {
                $.each(vdata, function (index, value) {
                    _data = {
                        name: value.description + ', ' + value.hours,
                        y: value.hours
                    };

                    datachart.push(_data);
                });
            }

            var setChart = {
                renderTo: "npt-chart-" + wellid,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
            };

            var chart = new Highcharts.Chart({
                chart: setChart,
                title: {
                    text: 'Non Productive Time'
                },
                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f} Hrs</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f}%'
                        }
                    }
                },
                tooltip: {
                    pointFormat: '<b>Total NPT: {point.y:.2f} Hrs</b><br/>',
                },
                series: [{
                    data: datachart
                }],
                credits: {
                    enabled: false
                },
            });
        };

        var loaddataMudftbbl = function (tipe, wellList) {
            $("#mudcostbyft-content").html('');
            $("#mudcostbybbl-content").html('');
            var data = { well_list: wellList }
           // console.log(tipe);
           // console.log(data);
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: $.helper.resolveApi("~/UTC/ServicePerformance/Trayek/" + $fieldId),
                success: function (r) {
                    if (r.data.length > 0) {
                        console.log("ini tarawdwadda",r.data);

                        for (var j = 0; j < tipe.length; j++) {
                            if (tipe[j] == "1") {
                                $framemudcostbyft.show();
                                var templatemudcostbyftScript = $("#mudcostbyft-template").html();
                                var templatemudcostbyft = Handlebars.compile(templatemudcostbyftScript);
                                $("#mudcostbyft-content").html(templatemudcostbyft({ data: r.data }));
                                for (var i = 0; i < r.data.length; i++) {
                                    drawchart('mudcostbyft-' + r.data[i].hole_name, r.data[i].data, 1);
                                }
                            }
                            else if (tipe[j] == "2") {
                                $framemudcostbybbl.show();
                                var templatemudcostbybblScript = $("#mudcostbybbl-template").html();
                                var templatemudcostbybbl = Handlebars.compile(templatemudcostbybblScript);
                                $("#mudcostbybbl-content").html(templatemudcostbybbl({ data: r.data }));

                                for (var i = 0; i < r.data.length; i++) {
                                    drawchart('mudcostbybbl-' + r.data[i].hole_name, r.data[i].data, 2);
                                    //console.log("ini semuanya", r.data[i].data)
                                }
                            }
                        }
                    }
                }
            });


        }

        var drawchart = function (chartname, vdataplan, filterType) {
            var plan = [];
            var actual = [];
            var wellName = [];

            var tittletextPlanYAxis = '';
            var tittletextActualYAxis = '';

            var seriesNamePlan = '';
            var seriesNameActual = '';
            var unitName = "";
            if (filterType == 1) {
                $.each(vdataplan, function (index, value) {
                    plan.push(value.plan_mudcostdepth);
                    actual.push(value.actual_mudcostdepth);
                    wellName.push(value.well_name);
                    unitName = value.uom_depth;
                });
                tittletextPlanYAxis = 'Plan Mud Cost / ' + unitName +' ($/' + unitName +')';
                tittletextActualYAxis = 'Actual Mud Cost / ' + unitName + ' ($/' + unitName +')';
                seriesNamePlan = 'PLAN MUD COST/' + unitName;
                seriesNameActual = 'ACTUAL MUD COST/' + unitName;
            }
            else if (filterType == 2) {
                $.each(vdataplan, function (index, value) {
                    plan.push(value.plan_mudcostvolume);
                    //actual.push(value.actual_mudcostvolume);
                    actual.push(value.actual_bbl_mud);
                    wellName.push(value.well_name);
                    //unitName = value.uom_volume;
                    unitName = "bbl";
                    //console.log("ini unit name", unitName)
                });
                tittletextPlanYAxis = 'Plan Mud Cost / ' + unitName + ' ($/' + unitName +')';
                tittletextActualYAxis = 'Actual Mud Cost / ' + unitName + ' ($/' + unitName +')';
                seriesNamePlan = 'PLAN MUD COST/' + unitName;
                seriesNameActual = 'ACTUAL MUD COST/' + unitName;
            }

            var setChart = {
                renderTo: chartname,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            };

            var setPlot = {
                bar: {
                    dataLabels: {
                        enabled: true
                    },
                }
            };

            var chart = new Highcharts.Chart({
                chart: setChart,
                title: {
                    text: ''
                },
                credits: {
                    enabled: false
                },
                xAxis: [{
                    categories: wellName,
                    crosshair: true,
                    labels: {
                        rotation: -45
                    }
                }],
                yAxis: [
                    { // Secondary yAxis
                        floor: 0,
                        title: {
                            text: tittletextActualYAxis,
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        accessibility: {
                            rangeDescription: 'Range: 0 to 99999'
                        },
                        min: 0,
                        opposite: true
                    },
                    { // Primary yAxis
                        floor: 0,
                        labels: {
                            format: '{value} USD',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        title: {
                            text: tittletextPlanYAxis,
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        accessibility: {
                            rangeDescription: 'Range: 0 to 99999'
                        },
                        min: 0
                    }
                ],
                tooltip: {
                    shared: true,
                },
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                },
                series: [
                    {
                        name: seriesNamePlan,
                        type: 'column',
                        yAxis: 1,
                        data: plan,
                        tooltip: {
                            valueSuffix: ' USD/' + unitName
                        }

                    },
                    {
                        name: seriesNameActual,
                        type: 'spline',
                        data: actual,
                        tooltip: {
                            valueSuffix: ' USD/' + unitName
                        }
                    }
                ]
            });
        }


        async function getmudcostbyft() {
            //let result;
            //try {
            //    result = await $.ajax({
            //        url: $.helper.resolveApi("~/UTC/SingleWell/GetIadcAnalysis/" + $recordId.val()),
            //        type: 'GET'
            //    });
            //    return result;
            //} catch (error) {
            //    console.log(error);
            //}
        }

        return {
            init: function () {
                APHLookup();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
});

