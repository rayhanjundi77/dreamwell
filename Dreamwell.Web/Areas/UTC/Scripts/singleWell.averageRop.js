﻿$(document).ready(function () {
    'use strict';
    var $recordId = $("#wellId");

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/UTC/SingleWell/GetDrillingByWell/' + $recordId.val()), function myfunction(r) {
                    console.log('GetDrillingByWell');
                    console.log(r);
                    if (r.status.success && r.data.length > 0) {
                        //var arrayDate = [];
                        //for (var i = 0; i < r.data.length; i++) {
                        //    var isFound = false;
                        //    var nextDate = moment(r.data[i].drilling_date);
                        //    var arX = [moment(nextDate).format('YYYY-MM-DD')];

                        //    arrayDate.push(arX);
                        //}
                        var series = [];
                        var date = [];
                        $.each(r.data, function (key, value) {
                            date.push(moment(value.drilling_date).format('DD MMM'));
                            series.push(value.mudlogging_avg_rop_avg == null ? 0 : value.mudlogging_avg_rop_avg);
                        })


                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'averageROP',
                                margin: [50, 10, 60, 65],
                                type: 'column',
                                height: 400,
                            },
                            title: {
                                text: 'Average ROP'
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                categories: date,
                                title: {
                                    text: null
                                },
                                labels: {
                                    rotation: -45,
                                    style: {
                                        fontSize: '10px',
                                        fontFamily: 'Verdana, sans-serif'
                                    }
                                }
                            },
                            series: [
                                {
                                    name: "ROP",
                                    data: series
                                },
                            ],
                            yAxis: {
                                min: 0,
                                title: {
                                    text: '',
                                    align: 'middle'
                                },
                                labels: {
                                    overflow: 'justify'
                                }
                            },
                            tooltip: {
                                formatter: function () {
                                    return 'ROP: ' + this.y + ' ';
                                }
                            },
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    }
                                }
                            },
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 10
                            },
                            credits: {
                                enabled: false
                            },
                        });
                    }
                });
            } else {
                
            }
        };

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});