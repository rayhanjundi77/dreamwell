﻿(function ($) {
    'use strict';
    var pageFunction = function () {
        var $recordId = $('input[id=recordId]');
        var plan_depth = 10;
        var cost_max = 0;

      //  var $wellId = $("input[id=id]");
        if ($recordId.val() != "") {
            console.log("get record id value" + $recordId.val());
            getWellDetail();
            getDrillingFormationPlan();
            getDrillingFormationActualNew();
            chartTVD();
           
        }
        else {
            console.log("kosong");
        }


        async function getWellDetail() {
            let result;
            try {
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi('~/core/Well/' + $recordId.val() + '/detail'),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (rwell) {



                        $.get($.helper.resolveApi('~/UTC/SingleWell/GetDrillingByWell/' + $recordId.val()), function myfunction(r) {
                            if (r.status.success && r.data.length > 0) {



                            }
                        })


                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                        console.log(r);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }

        async function getDrillingFormationPlan() {
            let result;
            try {
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi('~/core/DrillFormationsPlan/' + $recordId.val() + '/detailbywellid'),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (r) {
                        var zindex = 1000;
                        var planned_td = plan_depth;
                        //// console.log("getDrillingFormationPlan");
                        //// console.log("Plan Depth TD: " + plan_depth);
                        //// console.log(r);
                        //if (r.data.length > 0)
                        //    // console.log("Plan Stone: " + r.data[r.data.length - 1].depth);
                        //else
                        //    // console.log("0");

                        const _height = 750;
                        var lastY = 0;
                        var height = 0;
                        window.heightBackgroundGrey = 0;
                        var cc = 0;
                        $.each(r.data, function (index, value) {
                            //// console.log("Depth: " + value.depth)
                            //zindex -= 10;
                            var posTopRatio = parseFloat(r.data[index].depth) / parseFloat(plan_depth);
                            var posTop = 0;
                            if (index == 0)
                                height = posTopRatio * _height;
                            else {
                                //if (r.data.length - 1 == index) {
                                //    height = 10;
                                //} else {
                                //    height = (posTopRatio * _height) - lastY;
                                //}
                                height = (posTopRatio * _height);
                                posTop = (posTopRatio * _height);
                            }
                            //// console.log(
                            //    " :: Index: " + index +
                            //    " :: Ratio: " + posTopRatio +
                            //    " :: Current Y: " + lastY +
                            //    " :: Pos Top: " + posTop + "px" +
                            //    " :: Height:" + height);

                            if (cc == 0) {
                                var topLasty = 0;
                            } else {
                                var topLasty = lastY;
                            }


                            //var html = `<div class="plan-lithology" style="height: ` + height + `px; z-index: ` + zindex + `; background-image: url(` + $.helper.resolveApi("~/Core/GetFile/MinIO/GetImage?d=" + value.filemaster_id) + `);">
                            //var html = `<div class="plan-lithology" style="top: ` + lastY + `px; height: ` + height + `px;">
                            var html = `<div class="plan-lithology" style ="top: ` + topLasty + `px; width:50px; height: ` + (parseFloat(value.depth) - topLasty) + `px; 
                                                background-image: url(` + $.helper.resolve("/lithology/") + value.stone_id + ".png" + `);
                                                background-size: 50px 30px;">
                                                <div class="fw-500 text-white text-white-shadow">` + value.depth + ` ft</div>
                                            </div>`;
                            $("#section-plan-lithology").append(html);
                            window.heightBackgroundGrey = (parseFloat(value.depth)) + 2;

                            lastY = parseFloat(value.depth);
                            cc++;
                        });

                        //getWellHoleAndCasingPlan();


                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                        // // console.log(r);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }

        async function getDrillingFormationActualNew() {
            let result;
            try {
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi('~/core/DrillFormationsActual/' + $recordId.val() + '/detailbywellidNew'),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (r) {
                        // console.log("r--------------------------------------");
                        const _height = 750;
                        var lastY = 0;
                        var height = 0;
                        var heightTotal = 0;
                        var cc = 0;
                        var newArrData = r.data.sort((a, b) => (a.depth > b.depth) ? 1 : ((b.depth > a.depth) ? -1 : 0))
                        //console.log("adwddwdw",r.data);
                        //console.log("sadasdawdwdwd", newArrData);
                        $.each(newArrData, function (index, value) {
                            if (cc == 0) {
                                var topLasty = 0;
                            } else {
                                var topLasty = lastY;
                            }

                            var heightBg = (parseFloat(height) - parseFloat(lastY));
                            // console.log("value.detailObj ACTUAL LITHOLOGY-------------------------------");
                            // console.log(value.detailObj.length)
                            var html = `<div class="actual-lithology" style="top: 0px; height: ` + (parseFloat(value.depth) - topLasty) + `px; display: flex; align-items: center; position: relative;">`;
                            var row = '';
                            var rightR = 0;
                            for (var i = 0; i < value.detailObj.length; i++) {
                                var dtl = value.detailObj[i];
                                if (i == 0) {
                                    rightR = 0;
                                } else {
                                    rightR = rightR + value.detailObj[(i - 1)].percentage / 2;
                                }
                                var wdFF = dtl.percentage / 2;
                                row = row + `<div style="height: ` + (parseFloat(value.depth) - topLasty) + `px;background-image: url(` + $.helper.resolve("/lithology/") + dtl.stone_id + ".png" + `);background-size:50px 30px;right:` + rightR + `px;width:` + wdFF + `px"></div>`;
                            }

                            // Ditambahkan Untuk uom nya seperti di well detail js

                            html = html + row + `<div class="fw-500 text-black text-white-shadow" style="top :` + topLasty +`px;">` + value.depth + ` ft</div>
                            </div>`;
                            $("#section-actual-lithology").append(html);
                            lastY = height;
                            lastY = parseFloat(value.depth);
                            cc++;

                            heightTotal = heightTotal + heightBg;
                        });

                        if (heightTotal > window.heightBackgroundGrey) {
                            window.heightBackgroundGrey = heightTotal;
                        }

                        //scrolling();
                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                        // console.log(r);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }

        async function chartTVD() {
            console.log("get chart TVD");
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/UTC/SingleWell/GetTataWaktuPengeboran/' + $recordId.val()), function (r) {
                    console.log('GetTataWaktuPengeboran');
                    console.log(r);
                    if (r.status.success) {
                        var date = [];
                        var days = [];
                        var actualDepth = [];
                        var dailyCost = [];
                        var tvdDepth = [];
                        var cumCost = [];
                        var arrtvdDepth = [];
                        var arrcumCost = [];
                        var drillingActivity = [];
                        $.each(r.data.report_drilling, function (key, value) {
                            //console.log(value);
                            date.push(moment(value.drilling_date).format('YYYY-MM-DD'));
                            days.push(value.dfs == 1 ? 1 : value.dfs);
                            var _actualDepth = [value.dfs, value.current_depth_md == null ? 0 : value.current_depth_md];
                            actualDepth.push(_actualDepth);

                            var _actualCost = [value.dfs, value.cummulative_cost == null ? 0 : value.cummulative_cost];
                            dailyCost.push(_actualCost);

                            if (value.operation_activity.length > 0) {
                                $.each(value.operation_activity, function (keyDOA, valueDOA) {
                                    drillingActivity.push(valueDOA.description);
                                });
                            }
                        });

                        $.each(r.data.tvd_plan, function (key, value) {
                            var _planningDepth = [value.days, value.depth];
                            arrtvdDepth.push(_planningDepth);

                            var _planningCost = [value.days, value.cumm_cost];
                            arrcumCost.push(_planningCost);
                        })

                        $('#tvdchart').highcharts({
                            title: {
                                text: ''
                            },

                            subtitle: {
                                text: ''
                            },
                            credits: {
                                enabled: false
                            },
                            yAxis: [{ // Primary yAxis
                                floor: 0,
                                ceiling: 10000,
                                labels: {
                                    format: '{value} ft',
                                    style: {
                                        color: Highcharts.getOptions().colors[1]
                                    }
                                },
                                title: {
                                    text: 'Depth',
                                    style: {
                                        color: Highcharts.getOptions().colors[1]
                                    }
                                },
                                reversed: true
                            }, { // Secondary yAxis
                                floor: 0,
                                title: {
                                    text: 'Cost',
                                    style: {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                                },
                                labels: {
                                    format: '{value} USD',
                                    style: {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                                },
                                opposite: true
                                //reversed: true
                            }],
                            xAxis: {
                                floor: 0,
                                ceiling: 10000,
                                labels: {
                                    format: '{value}',
                                    step: 0
                                },
                                title: {
                                    text: 'Days'
                                },
                                accessibility: {
                                    rangeDescription: 'Range: 0 to 200'
                                }
                            },

                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            },
                            tooltip: {
                                //pointFormatter: function () {
                                //    var seriesNameConverter = {
                                //        'Seriesonename': 'Series One Name',
                                //        'Seriestwoname': 'Series Two Name'
                                //    };

                                //    return '<span style="color:{point.color}">\u25CF</span> '
                                //        + seriesNameConverter[this.series.name] + ': <b>' + this.y + '</b><br/>';

                                //    console.log(this.series.name);
                                //},
                                formatter: function () {
                                    //console.log(this.key);
                                    //console.log(this.x + " - " + this.y);
                                    //console.log(drillingActivity[this.key]);
                                    //return 'The value for <b>' + this.x +
                                    //    '</b> is <b>' + this.y + '</b>';


                                    var html = '<span style="font-size:10px"><b>' + this.series.name + '</b></span><br/>';
                                    html += '<span style="font-size:10px">' + r.data.report_drilling[this.key].dfs + ' Days</span><br/>';
                                    html += '<span style="font-size:10px">' + r.data.report_drilling[this.key].cummulative_cost + ' $</span><br/>';

                                    if (this.series.name == "Actual Depth")
                                        html += '<span style="font-size:10px"><b>Drilling Operation Activity</b></span><br/><br/>';

                                    if (r.data.report_drilling[this.key].operation_activity.length > 0 && this.series.name == "Actual Depth") {
                                        //console.log('yes');
                                        $.each(r.data.report_drilling[this.key].operation_activity, function (key, value) {
                                            html += '<span style="font-size:10px">' + value.description + '</span><br/><br/>';
                                        });
                                    }
                                    //html += '<table>';
                                    //html += '<tr>';
                                    //html += '<td style="color:{series.color};padding:0">Operation Day : </td>';
                                    //html += '<td style="padding:0"><b>' + this.x + ' Day</b></td>';
                                    //html += '</tr>';
                                    //html += '<tr>';
                                    //html += '<td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    //        '<td style="padding:0"><b>{point.y:.1f}</b></td>';
                                    //html += '</tr>';

                                    return html;
                                }
                                //headerFormat: '<span style="font-size:10px"><b>{series.name}</b></span><table>',
                                //pointFormat: '<tr><td style="color:{series.color};padding:0">Operation Day : </td>' +
                                //    '<td style="padding:0"><b>{point.x:.1f} Day</b></td></tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                //    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                                //footerFormat: '</table>',
                                //shared: true,
                                //useHTML: true

                            },
                            plotOptions: {
                                series: {

                                    label: {
                                        connectorAllowed: false
                                    },
                                    pointStart: 0,
                                }
                            },

                            series: [{
                                name: 'Actual Depth',
                                data: actualDepth,

                            },
                            {
                                name: 'Cummulative Cost',
                                yAxis: 1,
                                data: dailyCost
                            },
                            {
                                name: 'Planning Depth',
                                data: arrtvdDepth
                            },
                            {
                                name: 'Planning Cost',
                                yAxis: 1,
                                data: arrcumCost
                            }],

                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 1000
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }
                        });
                    }
                });
            }

            else {

            }
        }


         function wellProfile  () {
            //alert(drillingId);
             console.log("get well value id" + $recordId.val());
            var templateScript = $("#wellProfileDetail-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/well/' + $recordId.val() + '/detail'), function (r) {

                if (r.data) {
                    $("#tableWellDetailProfile > tbody").html(template(r.data));
                    console.log('profile');
                    console.log(r.data.spud_date);
                    lodaDate(r.data.spud_date);
                    //datepickerFormat($('.date-picker'));
                    //iadcLookUp();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var lodaDate = function (dt) {
            var data = moment(dt).format('YYYY-MM-DD');
            $('#datepicker-3').datepicker(
                {
                    autoclose: true,
                    todayBtn: "linked",
                    format: 'yyyy-mm-dd',
                    orientation: "bottom left",
                    //defaultDate:date,
                    //todayHighlight: true,
                }
            );
            $('#datepicker-3').datepicker('setDate', data);

            $('#datepicker-3').datepicker().on('change', function (e) {
                wellDDR($(this).val());
                //drillingOperationActivity($(this).val());
                drillOperation($(this).val());
            })
        }



        return {
            init: function () {
                wellProfile();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();

    });
}(jQuery));
