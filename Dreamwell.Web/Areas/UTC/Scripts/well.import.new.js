﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formNewWell")
    var $test = 0

   // var well_name = $('#well_name');
 //   $('#well_name').attr('disabled', true);
    //$("#afe_number").attr("disabled", true);
   // well_name.prop('disabled', true)

    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    }

    //-- L

    var pageFunction = function () {
        var wellTypeLookup = function () {
            $("#well_status").val('');
            $("#well_status").select2({
                dropdownParent: $form,
                placeholder: "Select a Well Type",
                allowClear: true,
            });

            $("select[id='well_status']").on('change', function () {
                if ($(this).val() == 0) {
                    $(".row-parent").show();
                    $("#parent_well").val('');
                } else {
                    $(".row-parent").hide();
                    //$("#parent_well").val('');
                }
                loadWell($(this).val());
            });
        }

        var loadWell = function (status) {
            if (status != null) {
                if (status == 0) {
                    $("#parent_well").prop('disabled', 'disabled');
                } else {
                    $("#parent_well").removeAttr('disabled');
                    $("#parent_well").cmSelect2({
                        url: $.helper.resolveApi('~/core/well/lookup'),
                        result: {
                            id: 'id',
                            text: 'well_name'
                        },
                        filters: function (params) {
                            console.log(params);
                            return [{
                                field: "well_name",
                                operator: "contains",
                                value: params.term || '',
                            }];
                        },
                        options: {
                            destroy: true,
                            dropdownParent: $form,
                            placeholder: "Select a Well",
                            allowClear: true,
                        }
                    });

                    $("#parent_well").on('select2:select', function (e) {
                        var id = e.target.value;
                        getAphField(id);
                    });
                }
            }
        }

        var loadAPH = function (initValue) {
           // console.log('load APH');
            easyloader.load('combotree', function () {        // load the specified module
                $('#business_unit_id').combotree({
                    textField: 'text',
                    value: initValue,
                    loader: function (param, success, error) {
                        $.ajax({
                            type: "POST",
                            url: $.helper.resolveApi("~/core/BusinessUnit/combotree"),
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({}),
                            dataType: 'json',
                            success: function (response) {
                                console.log('data APH sukses');
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err datanya');
                                error.apply(this, arguments);
                            }
                        });
                    },
                    onSelect: function (node) {
                        console.log("Node:: ");
                        console.log(node);
                        loadFields([''], node.id);
                        //loadField(node.id, []);
                    }
                });
            })
        }
        loadAPH(['']);


      

        var loadFields = function (initValue, businessUnitId) {
            easyloader.load('combotree', function () {        // load the specified module
                $('#field_id').combotree({
                    textField: 'text',
                    value: initValue,
                    loader: function (param, success, error) {
                        $.ajax({
                            type: "POST",
                            url: $.helper.resolveApi("~/core/Asset/GetFieldNode?countryId=&businessUnitId=" + businessUnitId),
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({}),
                            dataType: 'json',
                            success: function (response) {
                                console.log(response);
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err');
                                error.apply(this, arguments);
                            }
                        });
                    },
                    onSelect: function (node) {
                        console.log("Node:: ");
                        console.log(node);
                        //loadField(node.id, []);
                    }
                });
            })
        }

        
        loadAPH(['']);

        var getAphField = function (id) {
            if (id != null) {
                $.get($.helper.resolveApi('~/core/well/' + id + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#business_unit_id').combotree({ value: r.data.business_unit_id });
                        $('#field_id').combotree({ value: r.data.field_id });
                    }
                });
            }
        }


        var submitAllDDR = function (well_id, DDRXML) {
            if (DDRXML != null) {
                var dataDDR = $(DDRXML).find("opsReport");
                var dates = []; 
                for (var i = 0; i < dataDDR.length; i++) {

                    var wellname = $(dataDDR[i]).find('nameWell').text();
                    var drillingdate = $(dataDDR[i]).find('dTim:first').text();
                    var contvertdrillingdate = moment(drillingdate).format('YYYY-MM-DD');
                    dates.push(new Date(contvertdrillingdate));
                   // console.log("drriing date conver " + contvertdrillingdate + "  well name nya adalah " + wellname);
                    //console.log("well name " + wellname + " drilling date " + d + "currend drilling date" + drillingdate + "hasil convert biasa " + contvertdrillingdate);

                  //  submitDDR(well_id, "", contvertdrillingdate);
                }

                var minimumDate = new Date(Math.min.apply(null, dates)); 
              
                console.log("minimum date nya adalah " + moment(minimumDate).format('MM/DD/YYYY'));
                console.log("semua dates nya adalah ", dates);

                submitGeneralWellInfo(well_id, moment(minimumDate).format('MM/DD/YYYY'), dataDDR );

            }
        }
        var submitGeneralWellInfo = function (well_id,spudDate, dataDDR) {

            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var data = new Object();
            data.spud_date = spudDate;
            data.id = well_id;
      
            $.post($.helper.resolveApi('~/core/Well/save'), data, function (r) {
                if (r.status.success) {
                    console.log(r);
                    //  window.location = $.helper.resolve("/core/drilling/detail?id=") + r.data.recordId + "&well_id=" + $well_id + "&isnew=true";
                    for (var i = 0; i < dataDDR.length; i++) {

                        var wellname = $(dataDDR[i]).find('nameWell').text();
                        var drillingdate = $(dataDDR[i]).find('dTim:first').text();
                        var contvertdrillingdate = moment(drillingdate).utc().format('MM/DD/YYYY');
                    
                        // console.log("drriing date conver " + contvertdrillingdate + "  well name nya adalah " + wellname);
                        //console.log("well name " + wellname + " drilling date " + d + "currend drilling date" + drillingdate + "hasil convert biasa " + contvertdrillingdate);

                          submitDDR(well_id, "", contvertdrillingdate);
                    }


                    xmlDoc = null;
                    $("#inputWell").val(null);
       
                    var dvTable = $("#dvTable");
                    dvTable.children().remove();
                } else {
                    toastr.error(r.status.message)
                }

               
                $('#loadingModal').modal('hide');
            }, 'json').fail(function (r) {
              
                toastr.error(r.statusText);
                $('#loadingModal').modal('hide');
            });
		}
        var submitDDR = function (idWell, dataDDR, drillingDate) {

            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var data = new Object();
            data.drilling_date = drillingDate;
            data.well_id = idWell;
            $.post($.helper.resolveApi('~/core/drilling/newdrilling'), data, function (r) {
                if (r.status.success) {
                    console.log(r);
                  //  window.location = $.helper.resolve("/core/drilling/detail?id=") + r.data.recordId + "&well_id=" + $well_id + "&isnew=true";

                } else {
                    toastr.error(r.status.message)
                }
              
                //btn.button('reset');
                $('#loadingModal').modal('hide');
            }, 'json').fail(function (r) {
               
                toastr.error(r.statusText);
                $('#loadingModal').modal('hide');
            });
        }

        var submitAFE = function (wellid,datajson) {

            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            $.post($.helper.resolveApi('~/core/afe/save'), datajson, function (r) {
                if (r.status.success) {

                    //submit CEK DDR
                    var getXMLDOC = $("#xmlRecord").val();
                    submitAllDDR(wellid, getXMLDOC);
                    toastr.success(r.status.message);
                
                } else {
                    //toastr.error(r.status.message)
                }
               
            }, 'json').fail(function (r) {
              
                toastr.error(r.statusText);
            });
            $('#loadingModal').modal('hide');
        }

        $('#btn-save').click(function (event) {
            //var obj = JSON.parse(text);

            var getXMLDOC = $("#xmlRecord").val();
         //   console.log("data value xml doc " + getXMLDOC);
            var customers = $(getXMLDOC).find("opsReport");
            //console.log("data xml yg akan disave  " + $(customers[0]).find('nameWell').text());
            var data = $form.serializeToJSON();
            var jsonString = JSON.stringify(data);
            console.log("data json saat import new well  " + jsonString);

            var afe_number = $('#afe_number').val();

            var well_name = $('#well_name').val();
            var well_status = $('#well_status').val();
            var duration_afe = $('#duration_afe').val();
            var business_unit_id = $('#business_unit_id').val();
            var field_id = $('#field_id').val();
          //  console.log("data form afe number  " + afe_number + "well_name " + well_name + "bussiness id" + business_unit_id + "wll statue" + well_status + "field id" + field_id);
        //    { "id": null, "well_status": "0", "well_name": "test well  again", "business_unit_id": "c89293df-5745-4e5a-b0e7-fb12e2461a74", "field_id": "41aa6c9e-0af2-4f61-bc72-dceabb4dd1b8" }
           // var dataCreateWell = '{ "id": null, ' +
             //   + '"well_status": "' + well_status +'",' +
              //  + '"well_name": "' + well_name +'",' +
               // + '"business_unit_id": "' + business_unit_id +'",' +
               // + '"field_id": "' + field_id + '"}';

            var obj = new Object();
            obj.id = null;
            obj.well_status = well_status;
            obj.well_name = well_name;
            obj.business_unit_id = business_unit_id;
            obj.field_id = field_id;
            //convert object to json string
            var string = JSON.stringify(obj);
            //convert string to Json Object
            var dataJSONWELL = JSON.parse(string);

           
           // var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                Swal.fire(
                    {
                        title: "",
                        text: "Are you sure want to import a new Well?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                           // btn.button('loading');
                            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
                            $.post($.helper.resolveApi('~/core/well/new'), dataJSONWELL, function (r) {
                                if (r.status.success) {
                                  //  window.location = $.helper.resolve("/core/well/detail?id=") + r.data.recordId;
                                   
                                    //submit AFE

                                //    var dataCreatAFE = '{ "id": null, ' +
                                  //      + '"afe_no": "' + afe_number + '",' +
                                    //    + '"well_id": "' + r.data.recordId + '",' +
                                     //   + '"duration": "' + duration_afe + '",' +
                                      //  + '"business_unit_id": "' + business_unit_id + '",' +
                                       // + '"field_id": "' + field_id + '"}';
                                    var objAFE = new Object();
                                    objAFE.id = null;
                                    objAFE.afe_no = afe_number;
                                    objAFE.well_id = r.data.recordId;
                                    objAFE.duration = duration_afe;
                                    objAFE.business_unit_id = business_unit_id;
                                    objAFE.field_id = field_id;   
                                    var stringafe = JSON.stringify(objAFE);   
                                    var dataJSONAFE = JSON.parse(stringafe);
                                    submitAFE(r.data.recordId, dataJSONAFE);
                                 
                                } else {
                                    toastr.error(r.status.message)
                                }
                                $('#loadingModal').modal('hide');
                                //btn.button('reset');
                                $("#newWellModal").modal('hide');
                            }, 'json').fail(function (r) {
                               // btn.button('reset');
                                toastr.error(r.statusText);
                            });
                        }
                    });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });


        return {
            init: function () {
                wellTypeLookup();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
      
    });


});