﻿$(document).ready(function () {
    'use strict';
    var $recordId = $("#wellId");

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                var loading = `<div class="spinner-border" role="status" style="width: 4rem; height: 4rem;">
                                    <span class="sr-only">Loading...</span>
                               </div><div class="mt-4"></div>`;
                $("#trajecttoryTopView").html(loading);

                $.get($.helper.resolveApi('~/UTC/SingleWell/Get2DTrajectory/' + $recordId.val()), function myfunction(r) {

                    if (r.status.success) {
                        var plan = [];
                        var actual = [];

                        if (r.data.well_deviation.length > 0) {
                            $.each(r.data.well_deviation, function (index, value) {
                                var _plan = [value.e_w, value.n_s];
                                plan.push(_plan);
                            });
                        }
                        if (r.data.drilling_deviation.length > 0) {
                            $.each(r.data.drilling_deviation, function (index, value) {
                                var _actual = [value.e_w, value.n_s];
                                actual.push(_actual);
                            });
                        }

                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'trajecttoryTopView',
                                //type: 'spline',
                                //inverted: false
                            },
                            title: {
                                text: '2D Trajectory (Top View)'
                            },
                            subtitle: {
                                text: ''
                            },
                            xAxis: {
                                tickInterval: 200,
                                /*reversed: true,*/
                                title: {
                                    text: ''
                                },
                            },
                            yAxis: {
                                tickInterval: 200,
                                //reversed: true,
                                title: {
                                    text: ''
                                },
                            },
                            legend: {
                                enabled: true
                            },
                            tooltip: {
                                headerFormat: '',
                                pointFormat: '<b>E/W</b>: {point.x} mkt<br/> <b>N/S</b>: {point.y} ft'
                            },
                            plotOptions: {
                                spline: {
                                    marker: {
                                        radius: 4,
                                        lineColor: 'rgb(0, 115, 183)',
                                        lineWidth: 1
                                    }
                                }
                            },
                            series:
                                [{

                                    name: 'Plan',
                                    marker: {
                                        symbol: 'square'
                                    },
                                    data: plan
                                }, {
                                    name: 'Actual',
                                    marker: {
                                        symbol: 'diamond'
                                    },
                                    data: actual
                                }],
                            credits: {
                                enabled: false
                            },
                        });


                        return;
                        //var series = [];
                        //var dataWell = [];
                        //var seriesdrl = [];
                        //var dataddr = [];
                        //$.each(r.data.well_deviation, function (key, value) {
                        //    series.push(value.e_w == null ? 0 : value.e_w);
                        //    series.push(value.n_s == null ? 0 : value.n_s);
                        //})

                        //$.each(r.data.drilling_deviation, function (key, value) {
                        //    seriesdrl.push(value.e_w == null ? 0 : value.e_w);
                        //    seriesdrl.push(value.n_s == null ? 0 : value.n_s);
                        //})

                        //const matrixify = (arr, size) =>
                        //    Array.from({ length: Math.ceil(arr.length / size) }, (v, i) =>
                        //        arr.slice(i * size, i * size + size));
                        //const listWell = series;
                        //const listddr = seriesdrl;
                        //dataWell.push(matrixify(listWell, 2));
                        //dataddr.push(matrixify(listddr, 2));

                        //$('#trajecttoryTopView').highcharts({
                        //    chart: {
                        //        type: 'spline'
                        //    },
                        //    title: {
                        //        text: '2D Trajectory (Side View)'
                        //    },
                        //    yAxis: [{ // Primary yAxis
                        //        labels: {
                        //            format: '{value}',
                        //        },
                        //        title: {
                        //            text: '',
                        //        },
                        //        reversed: true
                        //    }],
                        //    xAxis: [{
                        //        labels: {
                        //            step: 0
                        //        },
                        //        reversed: true
                        //    }],
                        //    tooltip: {
                        //        headerFormat: '',
                        //        pointFormat: 'Vertical Section : {point.x} <br /> TVD : {point.y}'
                        //    },
                        //    series: [{
                        //        name: 'Program Directional Survey',
                        //        data: dataWell[0]
                        //    }, {
                        //        name: 'Actual Directional Survey',
                        //        data: dataddr[0]
                        //    }]
                        //});
                    }
                });
            } else {

            }
        };

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});