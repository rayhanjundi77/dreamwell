$(document).ready(function () {
	'use strict';

	var $btnDownload = $('#btnDownloadFile');
	var valueAppType = "";
	var exportAppType = "";
	var $aphId = "";
	var $fieldId = "";
	var controls = {
		leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
		rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
	}

	var pageFunction = function () {


		$("#well_app_type").val('');
		$("#well_app_type").select2({
			placeholder: "Select app type",
			allowClear: true
		});


		$("#export_app_type").val('');
		$("#export_app_type").select2({
			placeholder: "Select export type",
			allowClear: true
		});



		var APHLookup = function () {
			$('#btn-business-unit-lookup').click(function () {
				var btn = $(this);
				btn.button('loading');
				$("#field-lookup").button('reset');
				$("#field_id").val('');
				jQuery.ajax({
					type: 'POST',
					url: '/core/businessunit/Lookup',
					success: function (data) {
						btn.button('reset');
						//$("#field-lookup").button('reset');
						var $box = bootbox.dialog({
							message: data,
							title: "",
							callback: function (e) {
								//console.log(e);
							}
						});
						$box.on("onSelected", function (o, event) {
							$box.modal('hide');
							$aphId = event.node.id;
							//console.log($aphId);
							$("#formFilter").removeClass("d-none");
							$("#btn-business-unit-lookup").text("" + event.node.text);
						});
					}
				});
			});
		}
		$('#field-lookup').click(function () {
			var element = $(this);
			element.button('loading');
			jQuery.ajax({
				type: 'POST',
				url: '/core/Asset/LookupFieldByBusinessUnit?id=' + $aphId,
				success: function (data) {
					element.button('reset');
					var $box = bootbox.dialog({
						message: data,
						title: "",
						callback: function (e) {
							//console.log(e);
						}
					});
					$box.on("onSelected", function (o, event) {

						if (event.node.parent == '#') {
							alert('Please double click field only');
							return;
						} else {
							$box.modal('hide');
						}
						$fieldId = event.node.id;
						element.html(" " + event.node.text);

						getWellByField($fieldId);
					});
				}
			});
		});

		var getWellByField = function (wellid) {
			var templateScript = $("#well-template").html();
			var template = Handlebars.compile(templateScript);
			$.get($.helper.resolveApi('~/core/well/' + wellid + '/detailByField'), function (r) {
				//console.log(r);
				if (r.status.success) {
					$("#groupWell").show();
					if (r.data.length > 0) {
						if ($("#field-" + r.data[0].field_id).length == 0) {
							//$("#listWell").append(template({ data: r.data }));
							$("#listWell").html(template({ data: r.data }));
							$('.remove-field').on('click', function () {
								$("#field-" + $(this).data("fieldid")).remove();
							});
						} else {
							toastr.error("Field is Available on the list.");
						}
					} else {
						$("#field-lookup").button('reset');
						toastr.error("No Well Available on this Field.");
					}
				} else {
					$("#groupWell").hide();
				}
				$('.loading-detail').hide();
			}).fail(function (r) {
				//console.log(r);
			}).done(function () {

			});
		}

		$("#export_date").datepicker({
			autoclose: true,
			todayBtn: "linked",
			format: 'yyyy-mm-dd',
			orientation: "bottom left",
			todayHighlight: true,
			templates: controls
		});

		$btnDownload.on('click', function () {


			

			var isValid = true;

			//var totalWellChecked = $('input[name="wellId[]"]:checked').length;

			var totalWellChecked = [$well_id];

			var tempDataAppType = $("#well_app_type").val();

			var tempexportAppType = $("#export_app_type").val();

			// var date = $('#export_date').val();

			var date = $("#downloadDate").val();
			// if ($well_id.length <= 0) {
			// 	toastr.error("Please select  a well ");

			// 	isValid = false;
			// }

			// if (tempDataAppType == null) {

			// 	toastr.error("Please select app type ");

			// 	isValid = false;
			// }

			// if (tempexportAppType == null) {

			// 	toastr.error("Please select exort type ");

			// 	isValid = false;
			// }

			// valueAppType = tempDataAppType;
			// exportAppType = tempexportAppType;
			// console.log('nilai temp apptype : ' + tempDataAppType + ' nilai export type: ' + exportAppType);

			if (!date) {
				toastr.error("Please select date");
				isValid = false;
			}

			if (!isValid)
				return;

			getByWellId();			

			// if ($well_id.length > 0) {
			// 	console.log('check well saat didownload data ' + objectReturnWellFilter.data.well_name);


			// 	if (exportAppType == '1') {

			// 		if (valueAppType == '1') {
			// 			doExcel1();
			// 		}
			// 		else {
			// 			doExcel2();
			// 		}
			// 	}
			// 	else {
			// 		if (valueAppType == '1') {
			// 			doSaveDatPadSIM();
			// 		}
			// 		else {
					
			// 		}
				
   //              }
				




			// 	//download(date);
			// }
		});

		var download = function (date) {
			var aryWell = [$well_id];
			//$.each($("input[name='wellId[]']:checked"), function () {
			//	aryWell.push($(this).val());
			//});

			var wellid = '';
			if (aryWell.length > 0) {
				wellid = aryWell[0];
			}

			var type = $('#export_type').find(":selected").val()
			var url = $.helper.resolveApi('~utc/Exim/Download/' + date + '/' + wellid + '/' + type + '?x-Token=' + $('meta[name=x-token]').attr("content"));
			$('<a href="' + url + '" target="blank"></a>')[0].click();
			//window.location.href = $.helper.resolveApi('~utc/Exim/Download/' + date + '/' + wellid + '?x-Token=' + $('meta[name=x-token]').attr("content"));
		};

		return {
			init: function () {
				APHLookup();
			}
		}
	}();

	$(document).ready(function () {
		pageFunction.init();
	});


});
