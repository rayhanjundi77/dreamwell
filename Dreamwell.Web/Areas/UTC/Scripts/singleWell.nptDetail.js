﻿$(document).ready(function () {
    'use strict';
    var $recordId = $("#recordId");

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                var arXAnalysis = [];
                var colors = []; // Menggunakan array untuk menyimpan warna

                $.get($.helper.resolveApi("~/UTC/SingleWell/GetIadcAnalysisNpt/" + $recordId.val()), function (r) {
                    console.log("GetIadcAnalysisNPT");
                    console.log(r);
                    if (r.status.success) {
                        if (r.data.length > 0) {
                            for (var i = 0; i < r.data.length; i++) {
                                arXAnalysis.push({
                                    name: r.data[i].description,
                                    y: r.data[i].total,
                                    Time: r.data[i].interval,
                                    Type: r.data[i].type,
                                });
                                colors.push(getRandomColor()); // Menambahkan warna secara dinamis
                            }

                            Highcharts.setOptions({
                                colors: colors // Mengatur warna yang dihasilkan secara dinamis
                            });

                            // Fungsi untuk membuat grafik pie chart
                            function createPieChart() {
                                $('#trayek-analysis').highcharts({
                                    chart: {
                                        plotBackgroundColor: null,
                                        plotBorderWidth: null,
                                        plotShadow: false,
                                        type: 'pie',
                                        margin: [0, 0, 0, 0],
                                        spacingTop: 0,
                                        spacingBottom: 0,
                                        spacingLeft: 0,
                                        spacingRight: 0
                                    },
                                    title: {
                                        text: ''
                                    },
                                    tooltip: {
                                        pointFormat: '<strong>{point.Type}</strong><br><strong>{point.Time:.2f} Hrs</strong>'
                                    },
                                    plotOptions: {
                                        pie: {
                                            size: '40%',
                                            allowPointSelect: true,
                                            cursor: 'pointer',
                                            dataLabels: {
                                                enabled: true,
                                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                            }
                                        }
                                    },
                                    series: [{
                                        name: 'Brands',
                                        colorByPoint: true,
                                        data: arXAnalysis
                                    }]
                                });
                            }

                            // Fungsi untuk membuat grafik bar chart
                            function createBarChart() {
                                $('#trayek-analysis').highcharts({
                                    chart: {
                                        type: 'bar'
                                    },
                                    title: {
                                        text: ''
                                    },
                                    xAxis: {
                                        categories: arXAnalysis.map(function (item) {
                                            return item.name;
                                        })
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Total'
                                        }
                                    },
                                    series: [{
                                        name: 'Total',
                                        data: arXAnalysis.map(function (item) {
                                            return item.y;
                                        })
                                    }]
                                });
                            }

                            // Fungsi untuk menampilkan chart berdasarkan pilihan pengguna
                            function showChart() {
                                var pieChartCheckbox = $('#pie-chart-checkbox').is(':checked');
                                var barChartCheckbox = $('#bar-chart-checkbox').is(':checked');

                                $('#trayek-analysis').empty(); // Menghapus chart sebelumnya

                                if (pieChartCheckbox) {
                                    createPieChart();
                                }

                                if (barChartCheckbox) {
                                    createBarChart();
                                }
                            }

                            // Menampilkan grafik berdasarkan pilihan awal (default pie chart)
                            showChart();

                            // Event listener untuk memperbarui chart saat checkbox berubah
                            $('#pie-chart-checkbox, #bar-chart-checkbox').on('change', function () {
                                showChart();
                            });
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });

            } else {

            }
        };

        // Fungsi untuk menghasilkan warna acak
        var getRandomColor = function () {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        };

        return {
            init: function () {
                loadDetail();
            }
        }
    }();



    $(document).ready(function () {
        pageFunction.init();
    });


});