﻿(function ($) {
    'use strict';

    var $pageLoading = $('.page-loading-content');
    var $dt_well = $('#dt_well');
    var $dt_aph = $('#dt_aph');
    var bussinessUnitID = $('input[id=PrimaryBusinessUnitId]');
    var bussinessUnitName = $('input[id=PrimaryBusinessUnit]');
    var $label = {
        projectArea: $('label#total_project_area'),
        totalSumur: $('label#total_sumur'),
        total_well: $('label#total_well'),
        total_DDR: $('label#total_DDR'),
        total_DDR_approved: $('label#total_DDR_approved'),
        total_daily_cost: $('label#total_daily_cost'),
        total_contract_active: $('label#total_contract_active'),
        totalDDRUpload: $('label#total_ddr_upload'),
    };

    var map;
    var markers = [];  // we well store the markers here,


    var pageFunction = function () {
        // Total Well
        function getTotalWell() {
            $.get($.helper.resolveApi('~/core/Well/getAllNew?isadmin=' + $("#userisadmin").val()), function (r) {
                if (r.status.success) {
                    $label.total_well.text(r.data);
                    $label.totalSumur.text(r.data);

                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        // Daily Cost
        function getTotalDailyCost() {
            $.get($.helper.resolveApi('~/core/DailyCost/getAll'), function (r) {
                console.log("data cost");
                console.log(r);
                if (r.status.success) {
                    // $label.total_contract_active.text(r.data.length);
                    var count = 0;
                    for (var i = 0; i < r.data.length; ++i) {
                        var actualPrice = r.data[i].actual_price;
                        count = count + actualPrice;
                    }
                    $label.total_daily_cost.text(thousandSeparatorDecimal(count.toFixed(2)));
                }
            }, 'json').fail(function (r) {

            });
        };

        // Total Contract
        function getTotalContractActive() {
            $.get($.helper.resolveApi('~/core/Contract/getAllNew?isadmin=' + $("#userisadmin").val()), function (r) {
                if (r.status.success) {
                    $label.total_contract_active.text(r.data);
                }
            }, 'json').fail(function (r) {

            });
        };

        // Service Company
        function loadServiceCompany() {
            console.log("load service company");


            var dt = $dt_aph.cmDataTable({
                pageLength: 5,
                serverSide: false,

                ajax: {
                    url: $.helper.resolveApi("~/core/afe/rkapPlan?bussinessUnitID=" + bussinessUnitID.val()),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "bussines_unit_id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "unit_name",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `<a href ="#" href="#" onClick="window.open('/UTC/APH/Dashboard/DashboardFieldDetailV2?id=` + row.bussines_unit_id + `', '_blank')" class="text-bold text-info fw-700">` + row.unit_name + `</a>`;
                                return output;

                            }
                            return data;
                        }, width: "10%"
                    },
                    { data: "total_well" },
                    { data: "plan_price" },

                    {
                        data: "bussines_unit_id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `<button type="button"  class="btn btn-primary" data-target="#rkapDetailModel" data-toggle="modal" id ="` + row.bussines_unit_id + `" onclick ="ActualPriceRekap(this.id)"><i class="fa fa-eye"></i></button>`;
                                return output;

                            }
                            return data;
                        }, width: "10%"
                    },
                    {
                        data: "bussines_unit_id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.bussines_unit_id + `" >
                                    <a class ="row-edit btn btn-xs btn-info btn-hover-info fa fa-external-link add-tooltip" href="#" onClick="window.open('/UTC/APH/Dashboard/DashboardFieldDetail?id=`+ row.bussines_unit_id + `', '_blank')"
                                        data-original-title="Detail" data-container="body">
                                    </a>
                       
                                </div>`;
                                return output;
                            }
                            return data;
                        }, width: "10%"
                    }
                ],
                initComplete: function (settings, json) {

                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
                console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });

        }

        // Well Performance
        function loadWellPerformance() {
            console.log("load well performance");
            var dt = $dt_well.cmDataTable({
                pageLength: 5,
                ajax: {
                    url: $.helper.resolveApi("~/core/Well/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: true,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "well_name" },
                    { data: "field_name" },
                    {
                        data: "well_type"
                    },

                    {

                        data: "contractor_name"
                    },
                    {
                        data: "NPT AND PT",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `<button type="button"  class="btn btn-primary" data-target="#nptDetailModal" data-toggle="modal" id ="` + row.id + `" onclick ="Details(this.id)"><i class="fa fa-eye"></i></button>`;
                                return output;

                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: true,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.id + `" >
                                    <a class ="row-edit btn btn-xs btn-info btn-hover-info fa fa-external-link add-tooltip" href="#"
                                                onClick="window.open('/UTC/APH/Dashboard/DashboardWellInformation?id=`+ row.id + `', '_blank')"
                                        data-original-title="Detail" data-container="body">
                                    </a>
                       
                                </div>`;
                                return output;
                            }
                            return data;
                        }, width: "10%"
                    }
                ],
                initComplete: function (settings, json) {

                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
                console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        }

        // BAR PT NPT
        var stackedBarPTNPT = function (well_id) {
            $("#PTNPT > .panel").show();
            $("#PTNPT > .panel-alert").hide();

            if (well_id != '') {
                console.log('try to get npt dna pt');
                $.get($.helper.resolveApi('~/UTC/SingleWell/GetWellIadc/' + well_id), function (r) {
                    if (r.status.success) {
                        console.log(r.data);
                        var aryPercentagePT = [];
                        var aryPercentageNPT = [];
                        var date = [];
                        if (r.data.length > 0) {

                        }
                    }
                });
            }
        }

        // Total Approved
        function getTottalApproved() {
            $.get($.helper.resolveApi('~/core/approvalhistory/approvedHistory'), function (r) {
                console.log("data total ddr approved");
                console.log(r);
                if (r.status.success) {
                    $label.total_DDR_approved.text(r.data.length);
                }
            }, 'json').fail(function (r) {

            });
        }
        // Total DDR
        function getTotalDDR() {
            $.get($.helper.resolveApi('~/core/Drilling/getViewAllNew?isadmin=' + $("#userisadmin").val()), function (r) {
                console.log("data total ddr");
                console.log(r);
                if (r.status.success) {
                    // $label.total_DDR.text(r.data.length);
                    $("#total_ddr_upload").text(r.data);
                }
            }, 'json').fail(function (r) {

            });
        }

        // Total Field
        function getTotalField() {
            $.get($.helper.resolveApi('~/core/Field/getAllField'), function (r) {
                if (r.status.success) {
                    $label.projectArea.text(r.data.length);
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        }

        //google map defenition
        function initialize(well_data) {
            console.log("well data");
            console.log(well_data);
            map = new google.maps.Map(document.getElementById('map_canvas'), {
                zoom: 13,
                center: new google.maps.LatLng(-6.200000, 106.8166666),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [
                    {
                        stylers: [
                            { hue: '#ff1a00' },
                            { invert_lightness: true },
                            { saturation: -100 },
                            { lightness: 33 },
                            { gamma: 0.5 }
                        ]
                    }, {
                        featureType: 'poi.business',
                        elementType: ' labels.icon',
                        stylers: [
                            { visibility: 'on' },
                            { hue: '#fff700' },
                            { lightness: -15 },
                            { saturation: 99 }
                        ]
                    }, {
                        featureType: 'water',
                        elementType: 'geometry',
                        stylers: [
                            { color: '#2D333C' },
                            { lightness: 15 }
                        ]
                    }, {
                        featureType: 'transit.station.rail',
                        elementType: 'labels.text.stroke',
                        stylers: [
                            { visibility: 'on' },
                            { color: '#FF6666' }
                        ]
                    }, {
                        featureType: 'poi',
                        elementType: 'geometry',
                        stylers: [
                            { visibility: 'on' },
                            { lightness: -35 }
                        ]
                    }, {
                        featureType: 'road.local',
                        elementType: 'geometry.fill',
                        stylers: [
                            { visibility: 'on' },
                            { hue: '#FFD900' },
                            { lightness: 30 },
                            { saturation: 99 }
                        ]
                    }
                ]
            });

            var iconBase = 'https://i.imgur.com/';
            var icons = {
                spierings: {
                    name: 'Dreamwell Pertamina',
                    icon: 'https://pngio.com/images/png-a2498955.html'
                },
                hotel: {
                    name: 'Project Area (APH)',
                    icon: iconBase + 'fQ85Rxi.png'
                },
                gasstation: {
                    name: 'Well Station',
                    icon: iconBase + '71YrkJY.png'
                },
                trainstation: {
                    name: 'Drilling',
                    icon: iconBase + 'h1CWWIO.png'
                }
            };

            function addMarker(feature) {
                var marker = new google.maps.Marker({
                    position: feature.position,
                    icon: icons[feature.type].icon,
                    map: map
                });
                markers.push({
                    marker: marker,
                    type: feature.type
                });
            }


            var features = [];
            for (var j = 0; j < well_data.length; j++) {

                var data = well_data[j];
                var positions = {
                    position: new google.maps.LatLng(data.latitude, data.longitude),
                    type: "gasstation"
                };

                features.push(positions);
            }



            for (var i = 0, feature; feature = features[i]; i++) {
                addMarker(feature);
            }

            var legend = document.getElementById('legend');
            var i = 0;
            for (var key in icons) {
                var type = icons[key];
                var name = type.name;
                var icon = type.icon;
                var div = document.createElement('div');
                div.innerHTML = '<input checked="checked" type="checkbox" onchange="toggleType(this, event, \'' + features[i].type + '\')"><img src="' + icon + '"> ' + name;
                legend.appendChild(div);
                i++;
            }
            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
        }

        function toggleType(elm, event, type) {
            var on = elm.checked;
            for (var i = 0; i < markers.length; i++) {
                if (markers[i].type == type) {
                    markers[i].marker.setMap(on ? map : null);
                }
            }
        }

        // Chart Region
        //function loadRegion() {
        //    $.ajax({
        //        url: $.helper.resolveApi("~/utc/Dashboard/getRegion?bussinessUnitID=" + bussinessUnitID.val()),
        //        //url: $.helper.resolveApi("~/utc/Dashboard/getRegion"),
        //        type: 'POST',
        //        contentType: 'application/json',
        //        dataType: 'json',
        //        data: JSON.stringify({}),
        //        success: function (response) {

        //            if (response.status.success && Array.isArray(response.data)) {
        //                regionTable(response.data); // Display regions in table if data is valid
        //            } else {
        //                console.error('Failed to retrieve data or data is not as expected:', response.status.message || 'Invalid data');
        //            }
        //        },
        //        error: function (xhr, status, error) {
        //            console.error('Error during API call:', error);
        //        }
        //    });
        //}
        function loadRegion(businessUnitID = null) {
            // Determine URL based on the presence of businessUnitID
            const apiUrl = businessUnitID
                ? `~/utc/Dashboard/getRegion?businessUnitID=${businessUnitID}`
                : `~/utc/Dashboard/getRegion`;

            $.ajax({
                url: $.helper.resolveApi(apiUrl),
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({}),
                success: function (response) {
                    if (response.status.success && Array.isArray(response.data)) {
                        regionTable(response.data); // Display regions in table if data is valid
                    } else {
                        console.error('Failed to retrieve data or data is not as expected:', response.status.message || 'Invalid data');
                    }
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                }
            });
        }


        function regionTable(data) {
            console.log('Data passed to regionTable:', data); 
            const tbody = $('#region_table');
            tbody.empty(); 

            if (Array.isArray(data)) {
                //const filteredData = data.filter(item => item.parent_unit === null);

                data.forEach(function (item, index) {
                    console.log(`region apa aja ${index}:`, item); 

                    const formattedPlanningCost = item.planning_cost ? item.planning_cost.toLocaleString() : '0';
                    const formattedPlanningWell = item.planning_well ? item.planning_well.toLocaleString() : '0';
                    const formattedActualCost = item.actual_cost ? item.actual_cost.toLocaleString() : '0';
                    const formattedActualWell = item.actual_well ? item.actual_well.toLocaleString() : '0';

                    const rowHtml = `
                        <tr id="row-region-${index + 1}">
                            <td class="unit-name">${item.unit_name}</td>
                            <td>${formattedPlanningCost}</td>
                            <td>${formattedPlanningWell}</td>
                            <td>${formattedActualCost}</td>
                            <td>${formattedActualWell}</td>
                        </tr>
                    `;
                    tbody.append(rowHtml);

                    $(`#row-region-${index + 1} .unit-name`).click(function () {
                        $('.unit-name').removeClass('active');
                        $(this).addClass('active');
                        showRegionDetails(item.unit_name, item.business_unit_id);
                    });
                });
            } else {
                console.error('Expected an array but got:', data);
            }
        }

        // Data for PT/NPT region
        async function loadChartData(businessUnitId = null) {
            try {
                const response = await $.ajax({
                    //url: $.helper.resolveApi("~/utc/Dashboard/chartRegion"),
                    url: $.helper.resolveApi(`~/utc/Dashboard/chartRegion${businessUnitId ? `?businessUnitId=${businessUnitId}` : ''}`),
                    type: 'POST',
                    contentType: 'application/json'
                });

                console.log("API response received:", response);

                if (response.status && response.status.success) {
                    var regionData = response.data;

                    if (!regionData || regionData.length === 0) {
                        console.error('Regions data is empty or null.');
                        return;
                    }

                    console.log("Region data:", regionData);

                    var aryPercentagePT = [];
                    var aryPercentageNPT = [];
                    var categories = [];

                    // Calculate percentage of PT and NPT for each region
                    regionData.forEach(function (region, index) {
                        categories.push(region.unit_name);

                        const totalHours = region.total_hours_pt + region.total_hours_npt; // Total hours for the region
                        const ptPercentage = totalHours ? (region.total_hours_pt / totalHours) * 100 : 0;
                        const nptPercentage = totalHours ? (region.total_hours_npt / totalHours) * 100 : 0;

                        aryPercentagePT.push(ptPercentage);
                        aryPercentageNPT.push(nptPercentage);

                        console.log(`Region ${index + 1}:`, {
                            unit_name: region.unit_name,
                            ptPercentage: ptPercentage.toFixed(2) + '%',
                            nptPercentage: nptPercentage.toFixed(2) + '%'
                        });
                    });

                    // Initialize the Highcharts chart with API data
                    $("#stackedBarPTNPT1").highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: null
                        },
                        xAxis: {
                            categories: categories,
                            title: {
                                text: null
                            },
                            labels: {
                                rotation: -45,
                                style: {
                                    fontSize: '10px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            min: 0,
                            max: 100,
                            title: {
                                text: 'PT / NPT (%)'
                            },
                            labels: {
                                format: '{value}%'
                            },
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                }
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f}%</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                },
                                point: {
                                    events: {
                                        click: function () {
                                            var unitName = this.category;
                                            var parentId = regionData[this.index].business_unit_id;

                                            showRegionDetails(unitName, parentId);
                                        }
                                    }
                                },
                                cursor: 'pointer'
                            }
                        },
                        series: [{
                            name: 'PT',
                            data: aryPercentagePT.map(function (pt, index) {
                                return {
                                    y: pt,
                                    parentId: regionData[index].business_unit_id // Attach parentId to the point
                                };
                            }),
                            color: 'green'
                        }, {
                            name: 'NPT',
                            data: aryPercentageNPT.map(function (npt, index) {
                                return {
                                    y: npt,
                                    parentId: regionData[index].business_unit_id // Attach parentId to the point
                                };
                            }),
                            color: 'red'
                        }]
                    });

                    console.log("Chart initialized with PT and NPT data.");

                } else {
                    console.error('Failed to retrieve data from API:', response.status.message);
                }
            } catch (error) {
                console.error('Error during API call:', error);
            }
        }


        //async function loadChartData(businessUnitId = null) {
        //    try {
        //        const response = await $.ajax({
        //            url: $.helper.resolveApi(`~/utc/Dashboard/chartRegion${businessUnitId ? `?businessUnitId=${businessUnitId}` : ''}`),
        //            type: 'POST',
        //            contentType: 'application/json'
        //        });

        //        console.log("API response received:", response);

        //        if (response.status && response.status.success) {
        //            var regionData = response.data;

        //            if (!regionData || regionData.length === 0) {
        //                console.error('Regions data is empty or null.');
        //                return;
        //            }

        //            console.log("Region data:", regionData);

        //            var aryPercentagePT = [];
        //            var aryPercentageNPT = [];
        //            var categories = [];

        //            regionData.forEach(function (region, index) {
        //                categories.push(region.unit_name);

        //                const totalHours = region.total_hours_pt + region.total_hours_npt;
        //                const ptPercentage = totalHours ? (region.total_hours_pt / totalHours) * 100 : 0;
        //                const nptPercentage = totalHours ? (region.total_hours_npt / totalHours) * 100 : 0;

        //                aryPercentagePT.push(ptPercentage);
        //                aryPercentageNPT.push(nptPercentage);

        //                console.log(`Region ${index + 1}:`, {
        //                    unit_name: region.unit_name,
        //                    ptPercentage: ptPercentage.toFixed(2) + '%',
        //                    nptPercentage: nptPercentage.toFixed(2) + '%'
        //                });
        //            });

        //            $("#stackedBarPTNPT1").highcharts({
        //                chart: {
        //                    type: 'column'
        //                },
        //                title: {
        //                    text: 'Region'
        //                },
        //                xAxis: {
        //                    categories: categories,
        //                    title: { text: null },
        //                    labels: {
        //                        rotation: -45,
        //                        style: {
        //                            fontSize: '10px',
        //                            fontFamily: 'Verdana, sans-serif'
        //                        }
        //                    }
        //                },
        //                credits: {
        //                    enabled: false
        //                },
        //                yAxis: {
        //                    min: 0,
        //                    max: 100,
        //                    title: {
        //                        text: 'PT / NPT (%)'
        //                    },
        //                    labels: { format: '{value}%' },
        //                },
        //                tooltip: {
        //                    shared: true,
        //                    useHTML: true
        //                },
        //                series: [{
        //                    name: 'PT',
        //                    data: aryPercentagePT,
        //                    color: 'green'
        //                }, {
        //                    name: 'NPT',
        //                    data: aryPercentageNPT,
        //                    color: 'red'
        //                }]
        //            });

        //            console.log("Chart initialized with PT and NPT data.");
        //        } else {
        //            console.error('Failed to retrieve data from API:', response.status.message);
        //        }
        //    } catch (error) {
        //        console.error('Error during API call:', error);
        //    }
        //}


        // Chart Zona
        function showRegionDetails(unitName, parentId) {
            console.log("parentId passed to function: ", parentId);
            console.log("unitName passed to function: ", unitName);

            $('#regionDetailsTitle').text(`${unitName} Details`);
            $('#regionDetailsPanel').show();

            const tbody = $('#zona_table');
            tbody.html('<tr><td colspan="6">Loading zone data...</td></tr>');
            
            const apiUrl = $.helper.resolveApi('~/utc/Dashboard/getZona/' + parentId);
            //const apiUrl = $.helper.resolveApi("~/utc/Dashboard/getZona?bussinessUnitID=" + bussinessUnitID.val() + "&parentId=" + parentId);
            //console.log("API URL being called: ", apiUrl);

            // Make the API call to fetch zona data
            $.ajax({
                url: apiUrl,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({}),
                success: function (response) {
                    console.log('Response from API:', response);
                    
                    tbody.empty();
                    
                    if (response.status.success && Array.isArray(response.data)) {
                        console.log('Data retrieved successfully, populating the table...');

                        response.data.forEach((item, index) => {
                            console.log(`Item ${index}:`, item);

                            const CummulativePlan = item.cummulative_cost_plan ? roundTwoDecimals(item.cummulative_cost_plan).toLocaleString() : '0';
                            const CummulativeActual = item.cummulative_cost_actual ? roundTwoDecimals(item.cummulative_cost_actual).toLocaleString() : '0';
                            const CummulativePlanFt = item.cummulative_cost_plan_ft ? roundTwoDecimals(item.cummulative_cost_plan_ft).toLocaleString() : '0';
                            const CummulativeActualFt = item.cummulative_cost_actual_ft ? roundTwoDecimals(item.cummulative_cost_actual_ft).toLocaleString() : '0';
                            
                            const CummulativeActualFtPersen = item.cost_ft_persen ? roundTwoDecimals(item.cost_ft_persen) : '0';

                            const rowHtml = `
                                <tr id="row-zona-${item.business_unit_id}">
                                    <td class="unit-name">${item.unit_name}</td>
                                    <td>${CummulativePlan}</td>
                                    <td>${CummulativeActual}</td>
                                    <td>${CummulativePlanFt}</td>
                                    <td>${CummulativeActualFt}</td>
                                    <td>${CummulativeActualFtPersen}</td>
                                </tr>
                            `;
                            tbody.append(rowHtml);

                            // Menambahkan event handler untuk klik
                            $(`#row-zona-${item.business_unit_id} .unit-name`).click(function () {
                                $('.unit-name').removeClass('active');
                                $(this).addClass('active');

                                showZonaDetails(item.unit_name, item.business_unit_id);
                            });
                        });

                        console.log('Table successfully updated with zona data.');

                        loadChartForRegion(unitName, parentId);
                        loadPtNptChart(unitName, parentId);
                    } else {
                        console.error('Failed to retrieve data or data is not as expected:', response.status.message || 'data not valid');
                        tbody.html('<tr><td colspan="6">No zones found for this region.</td></tr>');
                    }
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                    tbody.html('<tr><td colspan="6">Error loading zone data.</td></tr>');
                }
            });
        }

        function loadChartForRegion(unitName, parentId) {
            console.log("Fetching chart data for unitName: ", unitName, " and parentId: ", parentId);

            // API URL to get chart data
            const apiUrl = $.helper.resolveApi(`~/utc/Dashboard/chartZona/` + parentId);
            
            $.ajax({
                url: apiUrl,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    console.log('Response from API:', response);

                    let depthCategories = ['Plan Depth', 'Actual Depth'];  
                    let seriesData = [];

                    if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                        console.log('Data retrieved successfully, populating chart...');
                        
                        response.data.forEach((item) => {
                            let unitSeries = {
                                name: item.unit_name,  
                                data: [item.depth_plan || 0, item.depth_actual || 0]  
                            };
                            seriesData.push(unitSeries);
                        });
                    } else {
                        console.warn(`No data available for parentId: ${parentId}, initializing empty chart.`);
                        
                        seriesData.push({
                            name: 'No Data',
                            data: [0, 0]
                        });
                    }

                    // Initialize chart with unit_name as series and depthPlanData / depthActualData as x-axis categories
                    Highcharts.chart('chartDetailRegion', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: unitName + ' Details'
                        },
                        xAxis: {
                            categories: depthCategories,  
                            title: {
                                text: null
                            },
                            labels: {
                                rotation: -45,
                                style: {
                                    fontSize: '10px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'm/ft'
                            },
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                }
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                },
                                //cursor: 'pointer'
                            }
                        },
                        series: seriesData  
                    });

                    console.log('Chart initialized.');
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                }
            });
        }

        function loadPtNptChart(unitName, parentId) {
            console.log("Fetching PT/NPT data for parentId: ", parentId);
            console.log("Fetching PT/NPT data for unitName: ", unitName);

            // API URL to get chart data
            const apiUrl = $.helper.resolveApi(`~/utc/Dashboard/chartPtNptRegion/` + parentId);

            // Make AJAX call to fetch the chart data
            $.ajax({
                url: apiUrl,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    console.log('Response from API:', response);

                    let categories = [];
                    let ptData = [];
                    let nptData = [];

                    if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                        console.log('Data retrieved successfully, populating chart...');

                        response.data.forEach((item) => {
                            const totalHours = (item.total_hours_pt || 0) + (item.total_hours_npt || 0);  // Hitung total jam PT + NPT
                            const ptPercentage = totalHours > 0 ? ((item.total_hours_pt || 0) / totalHours) * 100 : 0;
                            const nptPercentage = totalHours > 0 ? ((item.total_hours_npt || 0) / totalHours) * 100 : 0;

                            categories.push(item.unit_name);
                            ptData.push(ptPercentage);  // Masukkan dalam persen
                            nptData.push(nptPercentage);  // Masukkan dalam persen
                        });
                    } else {
                        console.warn(`No data available for parentId: ${parentId}, initializing empty chart.`);
                        // If no data, provide default empty categories and data
                        categories = ['No Data'];
                        ptData = [0];
                        nptData = [0];
                    }

                    // Initialize chart with dynamic data from API
                    Highcharts.chart('ptNptRegion', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: unitName + ' Details'
                        },
                        xAxis: {
                            categories: categories,
                            title: {
                                text: null
                            },
                            labels: {
                                rotation: -45,
                                style: {
                                    fontSize: '10px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            min: 0,
                            max: 100,
                            title: {
                                text: 'PT / NPT (%)'
                            },
                            labels: {
                                formatter: function () {
                                    return this.value + '%';  // Tampilkan dalam persen
                                }
                            },
                            stackLabels: {
                                enabled: false,
                                style: {
                                    fontWeight: 'bold',
                                }
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',  // Tampilkan persen di tooltip
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                        series: [{
                            name: 'PT (%)',
                            data: ptData,
                            color: 'green'
                        }, {
                            name: 'NPT (%)',
                            data: nptData,
                            color: 'red'
                        }]
                    });

                    console.log('Chart initialized.');
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                }
            });
        }

        // Chart Field
        function showZonaDetails(unitName, unitId) {
            console.log("unitId passed to function: ", unitId);
            console.log("unitName passed to function: ", unitName);
            
            if (!unitName) {
                $('#zonaDetailsTitle').text('Zona Details');
            } else {
                $('#zonaDetailsTitle').text(unitName + ' Details');
            }
            $('#zonaDetailsPanel').show();

            const tbody = $('#field_table');
            tbody.html('<tr><td colspan="6">Loading field data...</td></tr>');

            // Log API URL before making the call
            const apiUrl = $.helper.resolveApi('~/utc/Dashboard/getField/' + unitId);

            // Make the API call to fetch zona data
            $.ajax({
                url: apiUrl,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    console.log('Response from API:', response);
                    
                    tbody.empty();
                    
                    if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                        console.log('Data retrieved successfully, populating the table...');

                        response.data.forEach((detail, index) => {
                            console.log(`detail ${index}:`, detail);
                            
                            const CummulativePlan = detail.cummulative_cost_plan ? roundTwoDecimals(detail.cummulative_cost_plan).toLocaleString() : '0';
                            const CummulativeActual = detail.cummulative_cost_actual ? roundTwoDecimals(detail.cummulative_cost_actual).toLocaleString() : '0';
                            const CummulativePlanFt = detail.cummulative_cost_plan_ft ? roundTwoDecimals(detail.cummulative_cost_plan_ft).toLocaleString() : '0';
                            const CummulativeActualFt = detail.cummulative_cost_actual_ft ? roundTwoDecimals(detail.cummulative_cost_actual_ft).toLocaleString() : '0';
                            const CummulativeActualFtPersen = detail.cost_ft_persen ? roundTwoDecimals(detail.cost_ft_persen) : '0';

                            const rowHtml = `
                                <tr id="row-zona-${detail.field_id}">
                                    <td class="unit-name">${detail.field_name}</td>
                                    <td>${CummulativePlan}</td>
                                    <td>${CummulativeActual}</td>
                                    <td>${CummulativePlanFt}</td>
                                    <td>${CummulativeActualFt}</td>
                                    <td>${CummulativeActualFtPersen}</td>
                                </tr>
                            `;
                            tbody.append(rowHtml);
                            
                            $(`#row-zona-${detail.field_id} .unit-name`).click(function () {
                                $('.unit-name').removeClass('active');
                                $(this).addClass('active');

                                showFieldDetails(detail.field_name, detail.field_id);  
                            });
                        });

                        console.log('Table successfully updated with field data.');
                        
                        updateBarChart(unitId, unitName); 
                        updatePieChart(unitId, unitName);

                    } else {
                        console.warn(`No data available for unitId: ${unitId}, initializing empty table.`);
                        tbody.html('<tr><td colspan="6">No data available</td></tr>');
                    }
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                    tbody.html('<tr><td colspan="6">Error loading field data.</td></tr>');
                }
            });
        }

        function updateBarChart(unitId, unitName) {
            console.log("Fetching bar chart data for unitId: ", unitId);

            // API URL to get the chart data for the specified unit
            const apiUrl = $.helper.resolveApi(`~/utc/Dashboard/chartField/` + unitId);

            // Make AJAX call to fetch the chart data
            $.ajax({
                url: apiUrl,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    console.log('Response from API:', response);
                    
                    let depthCategories = ['Plan Depth', 'Actual Depth'];
                    let daysCategories = ['Planned Days', 'Actual Days'];
                    let costCategories = ['Plan Cost', 'Actual Cost'];
                    
                    let depthSeries = [];
                    let daysSeries = [];
                    let costSeries = [];
                    
                    if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                        console.log('Data retrieved successfully, initializing the chart...');
                        
                        response.data.forEach((item) => {
                            depthSeries.push({
                                name: item.field_name || 'Unknown Field',
                                data: [item.depth_plan || 0, item.depth_actual || 0]
                            });

                            daysSeries.push({
                                name: item.field_name || 'Unknown Field',
                                data: [item.planned_days || 0, item.actual_days || 0]
                            });

                            costSeries.push({
                                name: item.field_name || 'Unknown Field',
                                data: [item.plan_cost || 0, item.actual_cost || 0]
                            });
                        });
                    } else {
                        console.warn('No data available for unitId: ' + unitId);
                        
                        depthSeries.push({ name: 'No Data', data: [0, 0] });
                        daysSeries.push({ name: 'No Data', data: [0, 0] });
                        costSeries.push({ name: 'No Data', data: [0, 0] });
                    }

                    // Initialize the Highcharts for Depth chart
                    Highcharts.chart('chartDepthZona', {
                        chart: { type: 'column' },
                        title: { text: unitName + ' - Depth Details' },
                        xAxis: { categories: depthCategories },
                        yAxis: { min: 0, title: { text: 'm/ft' } },
                        series: depthSeries,
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                },
                            }
                        }
                    });

                    // Initialize the Highcharts for Days chart
                    Highcharts.chart('chartDaysZona', {
                        chart: { type: 'column' },
                        title: { text: unitName + ' - Days Details' },
                        xAxis: { categories: daysCategories },
                        yAxis: { min: 0, title: { text: 'days' } },
                        series: daysSeries,
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                },
                            }
                        }
                    });

                    // Initialize the Highcharts for Cost chart
                    Highcharts.chart('chartCostZona', {
                        chart: { type: 'column' },
                        title: { text: unitName + ' - Cost Details' },
                        xAxis: { categories: costCategories },
                        yAxis: { min: 0, title: { text: 'USD' } },
                        series: costSeries,
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                },
                            }
                        }
                    });

                    console.log('Charts initialized with dynamic or default data.');
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                    $('#chartDepthZona').html('<p>Error loading chart data.</p>');
                }
            });
        }

        async function updatePieChart(unitId, unitName) {
            console.log("Fetching pie chart data for unitId: ", unitId);

            let aryColor = [];
            let arXAnalysis = [];
            let totalDurationHours = 0;
            let htmlContent = ""; 

            try {
                const apiUrl = $.helper.resolveApi(`~/utc/Dashboard/getPieField/` + unitId);
                console.log("API URL being called: ", apiUrl);

                let response = await $.ajax({
                    url: apiUrl,
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json'
                });

                if (response.status.success && Array.isArray(response.data)) {
                    let analysis = response.data;
                    console.log('Response from PIE API:', response);

                    analysis.forEach((item) => {
                        totalDurationHours += item.total_duration_hours || 0;
                    });

                    analysis.forEach((item, index) => {
                        let color;
                        do {
                            color = '#' + Math.floor(Math.random() * 16777215).toString(16);
                        } while (color === '#ff0000');
                        aryColor.push(color);

                        let percentageManual = (item.total_duration_hours / totalDurationHours) * 100;

                        let dataPoint = {
                            name: "IADC Code " + item.iadc_code || "Unknown IADC Code",
                            y: item.total_duration_hours || 0,
                            extra: item.total_duration_hours + ' Hours'
                        };

                        arXAnalysis.push(dataPoint);
                        
                        htmlContent += `<p><strong style="color: black;">${dataPoint.name} :</strong> ${dataPoint.extra} (${percentageManual.toFixed(1)}%)</p>`;
                    });
                    
                    $('.name-pie').html(htmlContent);

                    // Initialize the Highcharts pie chart
                    Highcharts.chart('chartDetailZonaPie', {
                        chart: {
                            type: 'pie',
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            margin: [0, 0, 0, 0],
                            spacingTop: 0,
                            spacingBottom: 0,
                            spacingLeft: 0,
                            spacingRight: 0
                        },
                        title: {
                            text: 'Kumulatif NPT Bor PT ' + unitName
                        },
                        tooltip: {
                            pointFormat: '<strong>{point.name}</strong><br><strong>{point.y:.1f} Hours</strong><br><strong>{point.percentage:.1f} %</strong>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.percentage:.1f}%'
                                }
                            }
                        },
                        series: [{
                            name: 'IADC Code',
                            colorByPoint: true,
                            data: arXAnalysis
                        }]
                    });

                    getNptFieldData(unitId);

                } else {
                    console.warn("No data available or the response is not in the expected format.");
                }

            } catch (error) {
                console.error("Error fetching data for pie chart:", error);
            }
        }

        async function getNptFieldData(unitId) {
            console.log("Fetching NPT data for unitId: ", unitId);

            try {
                const nptApiUrl = $.helper.resolveApi(`~/utc/Dashboard/getNptField/` + unitId);

                let nptResponse = await $.ajax({
                    url: nptApiUrl,
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json'
                });

                console.log("Response from getNptField API:", nptResponse);

                if (nptResponse.status.success && nptResponse.data && nptResponse.data.length > 0) {
                    let nptData = nptResponse.data[0];
                    console.log("NPT Data received:", nptData);

                    let totalNptDays = parseFloat(nptData.total_npt_days || 0).toFixed(2);
                    let finishedWellbore = nptData.finished_wellbore || 0;
                    let totalOperationDays = parseFloat(nptData.total_operation_days || 0).toFixed(2);

                    $('#totalNPT').text(`${totalNptDays} days`);
                    $('#finishedWellbore').text(`${finishedWellbore} Well`);
                    $('#totalOperationDays').text(`${totalOperationDays} days`);

                    let nptPercentage = totalOperationDays > 0
                        ? ((totalNptDays / totalOperationDays) * 100).toFixed(2)
                        : "0.00";
                    $('#nptPercentage').text(`${nptPercentage}%`);
                } else {
                    console.error("No NPT data available or the response is not in the expected format.");
                    $('#totalNPT').text("0 days");
                    $('#finishedWellbore').text("0 Well");
                    $('#totalOperationDays').text("0 days");
                    $('#nptPercentage').text("0%");
                }
            } catch (error) {
                console.error("Error fetching NPT data:", error);
                $('#totalNPT').text("0 days");
                $('#finishedWellbore').text("0 Well");
                $('#totalOperationDays').text("0 days");
                $('#nptPercentage').text("0%");
            }
        }

        // Chart Well
        function showFieldDetails(fieldName, FieldId) {
            console.log("FieldId field passed to function: ", FieldId);
            console.log("FieldName field passed to function: ", fieldName);

            if (!fieldName) {
                $('#fieldDetailsTitle').text('Field Details');
            } else {
                $('#fieldDetailsTitle').text(fieldName + ' Details');
            }
            $('#fieldDetailsPanel').show();

            const tbody = $('#well_table');
            tbody.html('<tr><td colspan="6">Loading well data...</td></tr>');  // Show loading message

            // Construct API URL
            const apiUrl = $.helper.resolveApi('~/utc/Dashboard/getWell/' + FieldId);

            // Make the API call to fetch well data
            $.ajax({
                url: apiUrl,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    console.log('Response from API:', response);
                    tbody.empty();  // Clear the loading message

                    if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                        console.log('Data retrieved successfully, populating the table...');

                        response.data.forEach((detail, index) => {
                            console.log(`Detail ${index}:`, detail);

                            const CummulativePlan = detail.cummulative_cost_plan ? roundTwoDecimals(detail.cummulative_cost_plan).toLocaleString() : '0';
                            const CummulativeActual = detail.cummulative_cost_actual ? roundTwoDecimals(detail.cummulative_cost_actual).toLocaleString() : '0';
                            const CummulativePlanFt = detail.cummulative_cost_plan_ft ? roundTwoDecimals(detail.cummulative_cost_plan_ft).toLocaleString() : '0';
                            const CummulativeActualFt = detail.cummulative_cost_actual_ft ? roundTwoDecimals(detail.cummulative_cost_actual_ft).toLocaleString() : '0';
                            const CummulativeActualFtPersen = detail.cost_ft_persen ? roundTwoDecimals(detail.cost_ft_persen) : '0';

                            const rowHtml = `
                        <tr id="row-zona-${detail.field_id}">
                            <td>${detail.well_name}</td>
                            <td>${CummulativePlan}</td>
                            <td>${CummulativeActual}</td>
                            <td>${CummulativePlanFt}</td>
                            <td>${CummulativeActualFt}</td>
                            <td>${CummulativeActualFtPersen}</td>
                        </tr>
                    `;
                            tbody.append(rowHtml);

                            // Uncomment below if you want further well details
                            // $(`#row-zona-${detail.field_id}`).click(function () {
                            //    showWellDetails(detail.well_name, detail.field_id);  // Call for further well details
                            // });
                        });

                        console.log('Table successfully updated with well data.');

                        wellBarChart(fieldName, FieldId);  // Populate bar chart
                        wellPieChart(fieldName, FieldId);  // Populate pie chart

                    } else {
                        console.warn(`No data available for FieldId: ${FieldId}, initializing empty table.`);
                        tbody.html('<tr><td colspan="6">No data available</td></tr>');  // Show no data message
                    }
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                    tbody.html('<tr><td colspan="6">Error loading well data.</td></tr>');  // Show error message
                }
            });
        }

        function wellBarChart(fieldName, FieldId) {
            console.log("Fetching bar chart data for FieldId: ", FieldId);
            console.log("Fetching bar chart data for fieldName: ", fieldName);

            // API URL to get the chart data for the specified unit
            const apiUrl = $.helper.resolveApi(`~/utc/Dashboard/chartWell/` + FieldId);

            // Make AJAX call to fetch the chart data
            $.ajax({
                url: apiUrl,
                type: 'POST',
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    console.log('Response from API:', response);
                    
                    let depthCategories = ['Plan Depth', 'Actual Depth'];
                    let daysCategories = ['Planned Days', 'Actual Days'];
                    let costCategories = ['Plan Cost', 'Actual Cost'];
                    
                    let depthSeries = [];
                    let daysSeries = [];
                    let costSeries = [];
                    
                    if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                        console.log('Data retrieved successfully, initializing the chart...');
                        
                        response.data.forEach((item) => {
                            depthSeries.push({
                                name: item.well_name || 'Unknown Well',
                                data: [item.depth_plan || 0, item.depth_actual || 0]
                            });

                            daysSeries.push({
                                name: item.well_name || 'Unknown Well',
                                data: [item.planned_days || 0, item.actual_days || 0]
                            });

                            costSeries.push({
                                name: item.well_name || 'Unknown Well',
                                data: [item.plan_cost || 0, item.actual_cost || 0]
                            });
                        });
                    } else {
                        console.warn('No data available for FieldId: ' + FieldId);
                        
                        depthSeries.push({ name: 'No Data', data: [0, 0] });
                        daysSeries.push({ name: 'No Data', data: [0, 0] });
                        costSeries.push({ name: 'No Data', data: [0, 0] });
                    }

                    // Initialize the Highcharts for Depth chart
                    Highcharts.chart('chartDepthField', {
                        chart: { type: 'column' },
                        title: { text: fieldName + ' - Depth Details' },
                        xAxis: { categories: depthCategories },
                        yAxis: { min: 0, title: { text: 'm/ft' } },
                        series: depthSeries,
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                },
                            }
                        }
                    });

                    // Initialize the Highcharts for Days chart
                    Highcharts.chart('chartDaysField', {
                        chart: { type: 'column' },
                        title: { text: fieldName + ' - Days Details' },
                        xAxis: { categories: daysCategories },
                        yAxis: { min: 0, title: { text: 'days' } },
                        series: daysSeries,
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                },
                            }
                        }
                    });

                    // Initialize the Highcharts for Cost chart
                    Highcharts.chart('chartCostField', {
                        chart: { type: 'column' },
                        title: { text: fieldName + ' - Cost Details' },
                        xAxis: { categories: costCategories },
                        yAxis: { min: 0, title: { text: 'USD' } },
                        series: costSeries,
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: false
                                },
                            }
                        }
                    });

                    console.log('Charts initialized with dynamic or default data.');
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                    $('#chartDepthField').html('<p>Error loading chart data.</p>');
                }
            });
        }

        async function wellPieChart(fieldName, FieldId) {
            console.log("Fetching pie chart data for unitId: ", FieldId);

            let aryColor = [];
            let arXAnalysis = [];
            let totalDurationHours = 0;
            let htmlContent = ""; 

            try {
                const apiUrl = $.helper.resolveApi(`~/utc/Dashboard/getPieWell/` + FieldId);
                console.log("API URL being called: ", apiUrl);

                let response = await $.ajax({
                    url: apiUrl,
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json'
                });

                if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                    let analysis = response.data;
                    console.log('Response from PIE Well API:', response);

                    analysis.forEach((item) => {
                        totalDurationHours += item.total_duration_hours || 0;
                    });

                    analysis.forEach((item, index) => {
                        let color;
                        do {
                            color = '#' + Math.floor(Math.random() * 16777215).toString(16);
                        } while (color === '#ff0000');
                        aryColor.push(color);

                        let percentageManual = (item.total_duration_hours / totalDurationHours) * 100;

                        let dataPoint = {
                            name: "IADC Code " + item.iadc_code || "Unknown IADC Code",
                            y: item.total_duration_hours || 0,
                            extra: item.total_duration_hours + ' Hours'
                        };

                        arXAnalysis.push(dataPoint);
                        
                        htmlContent += `<p><strong strong style="color: black;">${dataPoint.name} :</strong> ${dataPoint.extra} (${percentageManual.toFixed(1)}%)</p>`;
                    });
                    
                    $('.namewell-pie').html(htmlContent);

                    // Initialize the Highcharts pie chart
                    Highcharts.chart('chartDetailFieldPie', {
                        chart: {
                            type: 'pie',
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            margin: [0, 0, 0, 0],
                            spacingTop: 0,
                            spacingBottom: 0,
                            spacingLeft: 0,
                            spacingRight: 0
                        },
                        title: {
                            text: 'Kumulatif NPT Bor PT ' + fieldName
                        },
                        tooltip: {
                            pointFormat: '<strong>{point.name}</strong><br><strong>{point.y:.1f} Hours</strong><br><strong>{point.percentage:.1f} %</strong>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.percentage:.1f}%'
                                }
                            }
                        },
                        series: [{
                            name: 'IADC Code',
                            colorByPoint: true,
                            data: arXAnalysis
                        }]
                    });

                    getNptWellData(FieldId);

                } else {
                    console.warn("No data available or the response is not in the expected format.");
                    
                    $('.namewell-pie').html("<p><strong>No Data Available</strong></p>");

                    // Initialize the Highcharts pie chart with a "No Data" message
                    Highcharts.chart('chartDetailFieldPie', {
                        chart: {
                            type: 'pie',
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            margin: [0, 0, 0, 0],
                            spacingTop: 0,
                            spacingBottom: 0,
                            spacingLeft: 0,
                            spacingRight: 0
                        },
                        title: {
                            text: 'Kumulatif NPT Bor PT ' + fieldName
                        },
                        tooltip: {
                            pointFormat: '<strong>{point.name}</strong><br><strong>{point.y:.1f} Hours</strong>'
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.percentage:.1f}%'
                                }
                            }
                        },
                        series: [{
                            name: 'IADC Code',
                            colorByPoint: true,
                            data: [{
                                name: 'No Data',
                                y: 100,
                                color: '#ddd'  // Grey color to represent "No Data"
                            }]
                        }]
                    });
                }

            } catch (error) {
                console.error("Error fetching data for pie chart:", error);
                
                $('.namewell-pie').html("<p><strong>No Data Available</strong></p>");

                // Initialize the Highcharts pie chart with a "No Data" message
                Highcharts.chart('chartDetailFieldPie', {
                    chart: {
                        type: 'pie',
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        margin: [0, 0, 0, 0],
                        spacingTop: 0,
                        spacingBottom: 0,
                        spacingLeft: 0,
                        spacingRight: 0
                    },
                    title: {
                        text: 'Kumulatif NPT Bor PT ' + fieldName
                    },
                    tooltip: {
                        pointFormat: '<strong>{point.name}</strong><br><strong>{point.y:.1f} Hours</strong>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '{point.percentage:.1f}%'
                            }
                        }
                    },
                    series: [{
                        name: 'IADC Code',
                        colorByPoint: true,
                        data: [{
                            name: 'No Data',
                            y: 100,
                            color: '#ddd'  // Grey color to represent "No Data"
                        }]
                    }]
                });
            }
        }

        async function getNptWellData(FieldId) {
            console.log("Fetching NPT data for FieldId: ", FieldId);

            try {
                const nptApiUrl = $.helper.resolveApi(`~/utc/Dashboard/getNptWell/` + FieldId);

                let nptResponse = await $.ajax({
                    url: nptApiUrl,
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json'
                });

                console.log("Response from getNptWell API:", nptResponse);

                if (nptResponse.status.success && nptResponse.data && nptResponse.data.length > 0) {
                    let nptData = nptResponse.data[0];  
                    console.log("NPT Data received:", nptData);

                    let totalNptDays = parseFloat(nptData.total_npt_days || 0).toFixed(2);
                    let finishedWellbore = nptData.finished_wellbore || 0;
                    let totalOperationDays = parseFloat(nptData.total_operation_days || 0).toFixed(2);

                    $('#totalNPTWell').text(`${totalNptDays} days`);
                    $('#finishedboreWell').text(`${finishedWellbore} Well`);
                    $('#totalOperationDaysWell').text(`${totalOperationDays} days`);

                    let nptPercentage = totalOperationDays > 0
                        ? ((totalNptDays / totalOperationDays) * 100).toFixed(2)
                        : "0.00";
                    $('#nptPercentageWell').text(`${nptPercentage}%`);
                } else {
                    console.error("No NPT data available or the response is not in the expected format.");
                    $('#totalNPTWell').text("0 days");
                    $('#finishedboreWell').text("0 Well");
                    $('#totalOperationDaysWell').text("0 days");
                    $('#nptPercentageWell').text("0%");
                }
            } catch (error) {
                console.error("Error fetching NPT data:", error);
                $('#totalNPTWell').text("0 days");
                $('#finishedboreWell').text("0 Well");
                $('#totalOperationDaysWell').text("0 days");
                $('#nptPercentageWell').text("0%");
            }
        }
        
        //// Function to generate random data
        //function generateRandomData() {
        //    return {
        //        exploration: Math.floor(Math.random() * 20) + 1,
        //        exploitation: Math.floor(Math.random() * 20) + 1,
        //        workover: Math.floor(Math.random() * 30) + 1
        //    };
        //}

        function roundTwoDecimals(number) {
            return Math.round(number * 100) / 100;
        }

        function generateRandomData(start, count) {
            var data = [];
            for (var i = 0; i < count; i++) {
                data.push(Math.floor(Math.random() * 100) + start);
            }
            return data;
        }

        function generateRandomPercentageData(count) {
            var data = [];
            var total = 0;
            for (var i = 0; i < count - 1; i++) {
                var value = Math.random() * 100;
                data.push(value);
                total += value;
            }
            data.push(100 - total); // Ensure the sum is 100%
            return data;
        }

        return {
            init: function () {
                //getTotalSumur();
                getTotalWell();
                getTotalField();
                getTotalDailyCost();
                getTotalContractActive();
                getTotalDDR();
                getTottalApproved();
                loadWellPerformance();
                //  stackedBarPTNPT('62430f3d-9057-46df-bb66-cd9f6ead0633');
                loadServiceCompany();
                loadRegion();
                loadChartData();
                //actidate(startDate, endDate);


            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));