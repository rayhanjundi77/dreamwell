$(document).ready(function () {
    var wellId = $('[name="well_id"]').val();
    $('.cmd').click(function () {
        // Tampilkan elemen loading
        $('#loading').show();

        const element = document.getElementById("content-fwr");

        var opt = {
            pagebreak: { mode: ['avoid-all', 'css', 'legacy'] },
            jsPDF: { unit: 'in', format: 'a4', orientation: 'portrait' }
        };

        // Proses generate PDF
        html2pdf().set(opt).from(element).save().then(function () {
            // Sembunyikan elemen loading setelah selesai
            $('#loading').hide();
        });
    });

    var loadWellData = function () {
        if (wellId !== '') {
            $.get($.helper.resolveApi('~/core/well/' + wellId + '/detail'), function (r) {
                console.log(r);
                if (r && r.data) {
                    $('span[name=bab1_field_name]').html(r.data.field_name || 'N/A');
                    $('span[name=bab1_well_name]').html(r.data.well_name || 'N/A');
                    $('span[name=bab1_well_classification]').html(r.data.well_classification || 'N/A');
                    $('span[name=bab1_well_type]').html(r.data.well_type || 'N/A');
                    $('span[name=bab1_latitude]').html(r.data.latitude || 'N/A');
                    $('span[name=bab1_longitude]').html(r.data.longitude || 'N/A');
                    $('span[name=bab1_ground_elevation]').html(r.data.ground_elevation || 'N/A');
                    $('span[name=bab1_kick_of_point]').html(r.data.kick_of_point || 'N/A');
                    $('span[name=bab1_record_created_by]').html(r.data.record_created_by || 'N/A');
                    $('span[name=bab1_rig_name]').html(r.data.rig_name || 'N/A');
                    $('span[name=bab1_spud_date]').html(r.data.spud_date ? moment(r.data.spud_date).format('DD MMM YYYY') : 'N/A');
                    $('span[name=mon_spud_date]').html(r.data.spud_date ? moment(r.data.spud_date).format('DD MMM YYYY') : 'N/A');
                    $('span[name=lbl_well_name]').html(r.data.well_name);
                    $('span[name=mon_budget]').html(r.data.afe_cost);
                    $('.well_name').text(r.data.well_name || 'N/A');
                    // Anda bisa menambahkan manipulasi DOM lainnya di sini
                } else {
                    console.error('Data tidak ditemukan');
                }
            }).fail(function (r) {
                console.error('Request failed', r);
            }).done(function () {
                console.log('Request done');
            });
        }
    };

    var getAfeData = function () {
        if (wellId !== '') {
            $.get($.helper.resolveApi('~/core/afe/' + wellId + '/detailByWell'), function (r) {
                console.log("hayu", r);
                $('span[name=bab1_afe_no]').html(r.data[0].afe_no);
                $('span[name=mon_afe_no]').html(r.data[0].afe_no);
                
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }
    }

    var loadFwrData= function () {
        console.log("sas",wellId)
        if (wellId !== '') {
            $.get($.helper.resolveApi('~/core/WellFinalReport/' + wellId + '/detailByWellId'), function (r) {
                console.log(r);
                $('span[name=lbl_drilling_engineer]').html(r.data.drilling_engineer);
                $('span[name=lbl_drilling_engineer]').html(r.data.drilling_engineer);
                $('span[name=lbl_company_man]').html(r.data.company_man);
                $('span[name=lbl_field_engineer]').html(r.data.field_engineer);
                $('span[name=lbl_drilling_manager]').html(r.data.drilling_manager);

               
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

    };

    var loadWellHoleandCasingData = function() {
        //console.log("Initiating AJAX request to fetch well hole and casing data.");
        $.get($.helper.resolveApi('~/core/WellHoleAndCasing/' + wellId + '/getAllByWellId'), function (r) {
            console.log("aaaa")
            var templateSusunanCasingScript = $("#susunanCasingTemplate").html();
            var templateSusunanCasing = Handlebars.compile(templateSusunanCasingScript);
            $('#susunanCasing').html(templateSusunanCasing({ data: r.data }));

        }).fail(function (r) {
            //console.log(r);
        }).done(function () {

        });
    }

    var getDataCostByWell = function () {
        $.get($.helper.resolveApi('~utc/WellData/' + wellId + '/dailyCost'), function (r) {
            var data = r.data;
            loadBS19(data.children, 1);
        });
    }

    var loadBS19 = function (data, hierarchy) {
        var space = 15 * hierarchy;
        data.forEach(item => {
            if (item.children.length > 0 || item.Currency == null) {
                var body = `<tr>
                        <td><span style="padding-left: ${space}px">${item.Description}</span></td>
                        <td></td>
                        <td></td>
                    </tr>`;

                $("#tableDailyCost > tbody").append(body);
                loadBS19(item.children, hierarchy + 1);
                //console.log(item.Description + hierarchy)
                //console.log("jajaja", hierarchy)
            } else {
                var body = `<tr>
                        <td><span style="padding-left: ${space}px">${item.Description}</span></td>
                        <td>${item.Currency} ${parseFloat(item.total_price_plan).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                        <td>${item.Currency} ${parseFloat(item.total_price_actual).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    </tr>`;

                $("#tableDailyCost > tbody").append(body);
            }
        });
    }

    var getDrillingData = function () {
        return $.get($.helper.resolveApi('~/core/drilling/' + wellId + '/detailbyId'))
            .then(function (response) {
                if (response) {
                    loadMonitoringData(response);
                }
                return response;
            })
            .fail(function (error) {
                console.error("Error fetching AFE data:", error);
                return null; // Mengembalikan null jika terjadi kesalahan
            })
            .done(function () {
                console.log("Request completed.");
            });
    }

    var loadMonitoringData = function (data) {
        var m = data;
        //console.log("ini ujasdasd", m.data.length)
        for (var i = 0; i < m.data.length; i++) {
            var item = m.data[i];
            //console.log("omo",item.drilling_dateyyyyy)
            var space = 20; // Sesuaikan dengan jumlah spasi yang Anda inginkan
            var body = `
            <tr>
                <td class="align-middle text-center" style="width: 5%;">${item.report_no}</td>
                <td class="align-middle text-center" style="width: 20%;">${item.drilling_date}</td>
                <td class="text-right">$ ${item.daily_cost.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                <td class="text-right">$ ... </td>
                <td class="text-right">$ ... %</td>
            </tr>`;
            $("#tableMonitoringBudget > tbody").append(body);
        }
    }

    
    
    loadWellData();
    loadFwrData();
    getAfeData();
    loadWellHoleandCasingData();
    getDataCostByWell();
    getDrillingData();
    //loadDeviationData();
    //loadTimeCostPie();

});