﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $fieldId = $('input[id=field_id]');
    var $wellId = $('input[id=well_id]');
    var bussinessUnitID = $('input[id=PrimaryBusinessUnitId]');
    var $form = $("#formDrilling");
    var $formDrillingOperation = $("#formDrillingOperation");
    var $formOperationActivities = $("#formOperationActivities");
    var $formHSSE = $("#formHSSE");
    var $formDrillingTransport = $("#formDrillingTransport");
    var $formDrillingBulk = $("#formBulkItem");
    var $formDrillingDeviation = $("#formDrillingDeviation");
    var $formMudloggingData = $("#formMudloggingData");
    var $form_mud1 = $("#form-mud1");
    var $form_mud2 = $("#form-mud2");
    var $form_mud3 = $("#form-mud3");
    var $formDrillingPit = $("#form-pit");
    var $formTotalVolume = $("#form-totalVolume");
    var $formDrillingWeather = $('#formDrillingWeather');
    var $formDrillingPersonel = $("#formDrillingPersonel");
    var $formDrillingBit = $("#formDrillingBit");
    var $formDrillingAerated = $("#formDrillingAerated");

    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    };

    var lastRowPersonel = 0;
    var lastRowPit = 0;
    var $dailyCost = 0;
    var $cummulativeCost = 0;
    var $dailyMudCost = 0;
    var $cummulativeMudCost = 0;

    var maskingMoney = function () {
        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });
    }

    var iadcLookUp = function () {
        $('.iadc-lookup').click(function () {
            var element = $(this);
            element.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/Iadc/Lookup',
                success: function (data) {
                    element.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                        callback: function (e) {
                            console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o, event) {
                        $box.modal('hide');
                        element.closest("tr").children(".activitiesCategory").find("input").val(event.node.original.category);
                        if (event.node.original.type == "1") {
                            element.closest("tr").children(".activitiesPTorNPT").find("input").val("PT");
                        } else if (event.node.original.type == "2") {
                            element.closest("tr").children(".activitiesPTorNPT").find("input").val("NPT");

                        }
                        element.siblings().val(event.node.id);
                        element.html(event.node.text);
                    });
                }
            });
        });
    };
    var jobPersonelLookUp = function (id) {
        var $element;
        if (id == null || id == "") {
            $element = $(".lookup-personel");
        } else {
            $element = $("#" + id);
        }
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/JobPersonel/lookup'),
            result: {
                id: 'id',
                text: 'job_title'
            },
            filters: function (params) {
                return [{
                    field: "job_title",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: $formDrillingPersonel,
                placeholder: "Select a Job Personel",
                allowClear: true,
                //tags: true,
                //multiple: true,
                maximumSelectionLength: 1,
            }
        });
    };

    var pitLookup = function (id) {
        var $element;
        if (id == null || id == "") {
            $element = $(".lookup-pit");
        } else {
            $element = $("#" + id);
        }
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/pit/lookup'),
            result: {
                id: 'id',
                text: 'pit_name'
            },
            filters: function (params) {
                return [{
                    field: "pit_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: $formDrillingPit,
                placeholder: "Select a Pit",
                allowClear: true,
                //tags: true,
                //multiple: true,
                maximumSelectionLength: 1,
            }
        });
    };

    var datepickerFormat = function (element) {
        element.datepicker({
            autoclose: true,
            todayBtn: "linked",
            format: 'mm/dd/yyyy',
            orientation: "bottom left",
            todayHighlight: true,
            templates: controls
        });
    };

    $("#mud_type1, #mud_type2, #mud_type3").val('');
    $("#mud_type1, #mud_type2, #mud_type3").select2({
        placeholder: "Select a Well Type",
        allowClear: true
    });


    var pageFunction = function () {

        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });

        datepickerFormat($('.date-picker'));
        autoNumericWithoutDecimal($(".numeric"));
        iadcLookUp();
        $("#cloneOperationActivies").on('click', function () {
            var templateScript = $("#rowOperationActivities-template").html();
            var template = Handlebars.compile(templateScript);
            $("#tableOperationActivities > tbody").append(template);

            $('#tableOperationActivities > tbody tr:last > .activitiesIadc > .iadc-lookup').click(function () {
                var element = $(this);
                element.button('loading');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/Iadc/Lookup',
                    success: function (data) {
                        element.button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                            callback: function (e) {
                                console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {
                            $box.modal('hide');
                            element.closest("tr").children(".activitiesCategory").find("input").val(event.node.original.category);
                            if (event.node.original.type == "1") {
                                element.closest("tr").children(".activitiesPTorNPT").find("input").val("PT");
                            } else if (event.node.original.type == "2") {
                                element.closest("tr").children(".activitiesPTorNPT").find("input").val("NPT");
                            }
                            element.siblings().val(event.node.id);
                            element.html(event.node.text);
                        });
                    }
                });
            });

            datepickerFormat($('#tableOperationActivities>tbody tr:last > .activitiesDate > .date-picker'));
        });

        $('#cloneDrillingTransport').on('click', function () {
            var templateScript = $("#rowTransport-template").html();
            var template = Handlebars.compile(templateScript);
            $("#tableTransport > tbody").append(template);

            datepickerFormat($('#tableTransport>tbody tr:last > .transportDate > .date-picker'));

        });

        $("#cloneDrillingDeviation").on('click', function () {
            var templateScript = $("#rowDrillingDeviationDaily-template").html();
            var template = Handlebars.compile(templateScript);
            $('#tableDrillingDeviation > tbody tr.no-data').remove();
            $("#tableDrillingDeviation > tbody").append(template);
        });

        $("#cloneDrillingPersonel").on('click', function () {
            var templateScript = $("#rowDrillingPersonel-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableDrillingPersonel > tbody tr.no-data').remove();
            $("#tableDrillingPersonel > tbody").append(template);

            $('#tableDrillingPersonel>tbody tr:last > .jobPersonel > select').attr("id", "job_personel_id-" + lastRowPersonel);

            jobPersonelLookUp("job_personel_id-" + lastRowPersonel);
            lastRowPersonel++;

            $('.row-delete').on('click', function () {
                console.log($(this));
                $(this).closest('.rowPersonel').remove();
            });
        });

        $("#cloneDrillingPit").on('click', function () {
            var templateScript = $("#rowDrillingPit-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableDrillingPit > tbody tr.no-data').remove();
            $("#tableDrillingPit > tbody").append(template);

            $('#tableDrillingPit>tbody tr:last > .pitSelect > select').attr("id", "pit_id-" + lastRowPit);

            pitLookup("pit_id-" + lastRowPit);
            lastRowPit++;

            $('.row-delete').on('click', function () {
                console.log($(this));
                $(this).closest('.rowRecord').remove();
            });
        });

        Handlebars.registerHelper('setType', function (type) {
            if (type == 1) {
                return "PT";
            } else if (type == "2") {
                return "NPT";
            }
        });

        Handlebars.registerHelper('setIadcName', function (iadc_code, iadc_description, type) {
            if (type == 1) {
                return iadc_code + " - " + iadc_description + " (PT)";
            } else if (type == "2") {
                return iadc_code + " - " + iadc_description + " (NPT)";
            }
        });

        Handlebars.registerHelper('printHoleCasingType', function (type, id) {
            var html = "";

            html += "<select class=\"form-control hole-casing-tipe\" id='hole-casing-tipe-" + id + "' name=\"lot_fit[]\">";
            if (type == 1) {
                html += `<option value="1" selected="true">LOT</option>
                          <option value="2">FIT</option>`;
            } else if (type == 2) {
                html += `<option value="1">LOT</option>
                          <option value="2" selected="true">FIT</option>`;
            } else {
                html += `<option value="1">LOT</option>
                          <option value="2">FIT</option>`;
            }
            html += "</select>";
            html += "<div class=\"invalid-feedback\">Lot or Fit type cannot be empty</div>";
            return new Handlebars.SafeString(html);
        });

        var GetWellObjectUomMap = function (id) {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + id), function (r) {
                if (r.status.success && r.data.length > 0) {
                    $("form#formDrilling :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("form#formOperationActivities :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("#form-drillingHoleAndCasing :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("form#form-drillingBHA :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#form-drillingBhaComponent :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("form#form-drillingBIT :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#tableDrillingDeviation :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("#form-drillingBhaBit :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    })
                    $("#formOperationActivities :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    })
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var wellBhaLookUp = function (e) {
            //var $element = e;
            //if (e == null)
            var $element = $(".wellBhaLookup");
            $element.cmSelect2({
                url: $.helper.resolveApi('~/core/wellbha/lookupByWell/' + $wellId.val()),
                result: {
                    id: 'id',
                    text: 'bha_no'
                },
                filters: function (params) {
                    return [{
                        field: "bha_no",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "",
                    allowClear: true,
                    templateResult: function (repo) {
                        if (repo.loading) {
                            return repo.text;
                        }
                        return "<span class='fw-700'>No: " + repo.text + "</span>";
                    },
                    templateSelection: function (data, container) {
                        if (data.text == "")
                            return "Choose BHA";
                        else
                            return "<span class='fw-700'>No: " + data.text + "</span>";
                    },
                }
            });
        }

        var wellBitLookUp = function (e) {
            //var $element = e;
            //if (e == null)
            var $element = $(".wellBitLookup");
            $element.cmSelect2({
                url: $.helper.resolveApi('~/core/wellbit/lookupByWell/' + $wellId.val()),
                result: {
                    id: 'id',
                    text: 'bit_number',
                    bit_size: 'bit_size'
                },
                filters: function (params) {
                    return [{
                        field: "bit_number",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "",
                    allowClear: true,
                    templateResult: function (repo) {
                        if (repo.loading) {
                            return repo.text;
                        }
                        return "<span class='fw-700'>No: " + repo.text + "</span>";
                    },
                    templateSelection: function (data, container) {
                        if (data.text == "")
                            return "Choose BIT";
                        else
                            return "<span class='fw-700'>No: " + data.text + "</span>";
                    },
                }
            });

            //$element.on('select2:select', function (response) {
            //    //console.log();
            //    var id = response.target.value;
            //    //console.log("Well Bit Selected");
            //    //console.log(response);
            //    var element = $element.closest('.row-data').find(".bit-run");

            //    getBitLastRun(response.target.value, element);
            //});
        }

        var getActivityByDrillingId = function (drillingId) {
            //alert(drillingId);
            var templateScript = $("#readOperationActivities-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingoperationactivity/' + drillingId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableOperationActivities > tbody").html(template({ data: r.data }));
                    datepickerFormat($('.date-picker'));
                    iadcLookUp();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingTransportById = function (recordId) {
            var templateScript = $("#readTransport-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingtransport/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableTransport > tbody").html(template({ data: r.data }));
                    datepickerFormat($('.date-picker'));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingBulk = function (drillingId, well_id ,drilling_date) {
            var date = moment(drilling_date).format('YYYY-MM-DD');
            var templateScript = $("#drillingBulk-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingbulk/' + drillingId + '/' + well_id + '/' + date + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableBulk > tbody").html(template({ data: r.data }));
                    //datepickerFormat($('.date-picker'));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingMud = function (drillingId, mudType) {
            $.get($.helper.resolveApi('~/utc/auditdata/getmud?drillingId=' + drillingId + '&dataMudType=' + mudType), function (r) {
                console.log('mud');
                console.log(r);
                if (r.status.success) {
                    
                    if (mudType == "mud_1") {
                        $.helper.form.fill($form_mud1, r.data);
                        $("#form-mud1 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                    if (mudType == "mud_2") {
                        $.helper.form.fill($form_mud2, r.data);
                        $("#form-mud2 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                    if (mudType == "mud_3") {
                        $.helper.form.fill($form_mud3, r.data);
                        $("#form-mud3 #mud_time").val(timeFormat(r.data.mud_time));
                    }

                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingMudPit = function (drillingId) {
            var templateScript = $("#readDrillingPit-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingmudpit/' + drillingId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingPit > tbody").html(template({ data: r.data }));

                    jobPersonelLookUp("");
                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowPersonel').remove();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingTotalVolume = function (drillingId) {
            $.get($.helper.resolveApi('~/core/drillingtotalvolume/' + drillingId + '/detail'), function (r) {

                if (r.status.success) {
                    $.helper.form.fill($formTotalVolume, r.data);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingPersonelId = function (drillingId) {
            var templateScript = $("#readDrillingPersonel-template").html();
            var template = Handlebars.compile(templateScript);
            //$.get($.helper.resolveApi('~/core/drillingpersonel/' + recordId + '/getByDrillingId'), function (r) {
            $.get($.helper.resolveApi('~/core/drillingpersonel/' + drillingId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingPersonel > tbody").html(template({ data: r.data }));

                    jobPersonelLookUp("");
                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowPersonel').remove();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingItemByDrillingId = function (drillingId) {
            var templateScript = $("#readDrillingBulkItemByDrillingId-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingbulk/' + drillingId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#drillingBulkItem > tbody").html(template({ data: r.data }));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingItemByWellId = function (well_id) {
            var templateScript = $("#readDrillingItemByWellId-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingbulk/' + well_id + '/getItemByWellId'), function (r) {
                if (r.status.success) {
                    $("#drillingBulkItem > tbody").html(template({ data: r.data }));

                    $('.item-consumed').on('change', function () {
                        var previous = $(this).closest("tr").children(".item-previous").find("input").val();
                        var consumed = $(this).closest("tr").children(".item-consumed").find("input").val();
                        var received = $(this).closest("tr").children(".item-received").find("input").val();
                        var balance = parseInt(received) + parseInt(previous) - parseInt(consumed);
                        $(this).closest("tr").children(".item-balance").find("input").val(balance)
                    });

                    $('.item-received').on('change', function () {
                        var previous = $(this).closest("tr").children(".item-previous").find("input").val();
                        var consumed = $(this).closest("tr").children(".item-consumed").find("input").val();
                        var received = $(this).closest("tr").children(".item-received").find("input").val();
                        var balance = parseInt(received) + parseInt(previous) - parseInt(consumed);
                        $(this).closest("tr").children(".item-balance").find("input").val(balance)
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingDeviationByDrillingId = function (drillingId) {
            var templateScript = $("#readDrillingDeviation-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillingDeviation/' + drillingId + '/detailByDrillingId'), function (r) {
                console.log("--");
                console.log(r);
                if (r.status.success && r.data.length > 0) {
                    $("#tableDrillingDeviation > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingChimicalUse = function (drillingId) {
            var templateScript = $("#drillingchicalUse-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingchemicalused/' + drillingId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableChemicalUsed > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };
       
        var getDrillingBit = function (drillingId) {
            $.get($.helper.resolveApi('~/core/drillingBit/' + drillingId + '/detailByDrillingId'), function (r) {
                if (r.status.success) {
                    $.helper.form.fill($formDrillingBit, r.data[0]);
                }
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        var getDrillingDeviationByDrillingId = function (drillingId) {
            var templateScript = $("#readDrillingDeviation-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillingDeviation/' + drillingId + '/detailByDrillingId'), function (r) {
                //console.log('dev');
                //console.log(r);
                if (r.status.success && r.data.length > 0) {
                    $("#tableDrillingDeviation > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var getDrillingAerated = function (drillingId) {
            $.get($.helper.resolveApi('~/core/drillingaerated/' + drillingId + '/getByDrillingId'), function (r) {
                console.log('aerated');
                console.log(r);
                if (r.status.success) {
                    $.helper.form.fill($formDrillingAerated, r.data);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var dailyCost = function (wellId, drillingid) {
            $.get($.helper.resolveApi('~/core/dailycost/GetDailyCost/' + wellId + '/' + drillingid), function (r) {
                if (r.status.success) {
                    console.log('test');
                    console.log(r);
                    $("#DailyCost").val(thousandSeparatorFromValueWithComma(r.data.daily_cost));
                    $("#cummulative_cost").val(thousandSeparatorFromValueWithComma(r.data.cummulative_cost));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {

            }).done(function () {

            })
        }

        var getActivityByDrillingId = function (recordId, welllid) {
            var templateScript = $("#readHoleAndCasing-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingoperationactivity/getByDrillingId/' + recordId + '/' + welllid), function (r) {
                if (r.status.success) {
                    console.log('opration');
                    console.log(welllid);
                    $("#formOperationActivities").html(template({ data: r.data }));
                    GetWellObjectUomMap(welllid);

                    //$.each(r.data, function (key, value) {
                    //    //holeAndCasingLookup(value.id);
                    //    //lotFitLookup(value.id, false);

                    //    $("#cloneOperationActivies-" + value.id).on('click', function () {
                    //        cloneOperationActivities(value.id);
                    //    });
                    //});


                    $("input").on("change", function () {
                        $(this).removeClass("input-danger");
                    });

                    datepickerFormat($('.date-picker'));
                    iadcLookUp();

                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowOperation').remove();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
            }).done(function () {

            });

        }

        var holeAndCasingLookup = function (id) {
            $("#holeandcasinglookup-" + id).cmSelect2({
                url: $.helper.resolveApi("~/core/wellholeandcasing/lookup?wellId=" + $wellId.val()),
                result: {
                    id: 'id',
                    text: 'hole_name',
                    casing_name: 'casing_name',
                    hole_depth: 'hole_depth',
                    casing_setting_depth: 'casing_setting_depth'
                },
                filters: function (params) {
                    return [{
                        field: "hole_name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "Select Hole and Casing",
                    allowClear: true,
                    templateResult: function (repo) {
                        return "<b>Hole:</b> " + repo.text + `"` + " <b>Casing:</b> " + repo.casing_name + `"`;
                    },
                }
            });
            $("#holeandcasinglookup-" + id).on('select2:select', function (e) {
                $("#hole-diameter-" + id).val(e.params.data.text);
                $("#hole-diameter-" + id).val(e.params.data.text);
                $("#casing-diameter-" + id).val(e.params.data.casing_name);
                $("#hole-target-" + id).val(e.params.data.hole_depth);
            });
        }

        var getDrillingBhaBit = function (id) {
            
            var templateScript = $("#readBhaBit-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillingBhaBit/getDetail/' + id), function (r) {
                console.log('aasad');
                console.log(r.data);
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#form-drillingBhaBit").html(template({ data: r.data }));

                        GetWellObjectUomMap(r.data);

                        $("#form-drillingBhaBit .numeric-money").inputmask({
                            digits: 2,
                            greedy: true,
                            definitions: {
                                '*': {
                                    validator: "[0-9]"
                                }
                            },
                            rightAlign: false
                        });

                        $(".depth-out").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".depth-in").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".duration-bit").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });

                        $.each(r.data, function (key, value) {
                            wellBhaLookUp(value.well_bha_id);
                            wellBitLookUp(value.well_bit_id);

                        });


                    } else {
                        //cloneMudloggingData();
                    }

                    $('.row-delete').on('click', function () {
                        $(this).closest('.row-data').remove();
                    });
                }
            }).fail(function (r) {
            }).done(function () {
            });
        }
        
        var loadDetail = function (drillingId) {

           
            //var $currentDepthMD = 0;
            //var $previousDepthMD = 0;
            $.get($.helper.resolveApi('~/utc/auditdata/' + drillingId + '/detail'), function (r) {
                console.log('detail');
                console.log(r);
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                        $.helper.form.fill($formDrillingWeather, r.data);
                        $.helper.form.fill($formMudloggingData, r.data);
                        $.helper.form.fill($formHSSE, r.data);
                        $.helper.form.fill($formDrillingOperation, r.data);

                        dailyCost(r.data.well_id, r.data.id);
                        getDrillingBulk(r.data.id, r.data.well_id, r.data.drilling_date);

                        
                        $.each(r.data.drilling_operation, function (key, value) {
                            $("#type" + value.type).val(value.description);
                        })

                        $("#report_no").val("#" + r.data.report_no);
                        $("#drilling_date").val(moment(r.data.drilling_date).format('MMM DD, YYYY'));

                        $("#spud_date").val(moment(r.data.spud_date).format('MMM DD, YYYY'));
                        $("#date_now").val(moment(r.data.drilling_date).format('YYYY-MM-DD'));

                        var result = r.data.current_depth_md - r.data.previous_depth_md;
                        $("#progress").val(thousandSeparatorFromValueWithComma(result));

                        $("#daily_mud_cost").val(r.data.daily_mud_cost == null ? 0 : r.data.daily_mud_cost);
                        $("#cummulative_mud_cost").val(r.data.cummulative_mud_cost == null ? 0 : r.data.cummulative_mud_cost);


                        getActivityByDrillingId(r.data.id, r.data.well_id);
                        getDrillingBhaBit(r.data.id);
                }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });

                            
                
                getDrillingTransportById(drillingId);
                
                getDrillingDeviationByDrillingId(drillingId);
                getDrillingMud(drillingId, 'mud_1');
                getDrillingMud(drillingId, 'mud_2');
                getDrillingMud(drillingId, 'mud_3');
                getDrillingMudPit(drillingId);
                getDrillingTotalVolume(drillingId);
                getDrillingPersonelId(drillingId);
                getDrillingChimicalUse(drillingId);   
                getDrillingAerated(drillingId);
                //getDrillingItemByDrillingId(drillingId);
        };

        $("input[type=text].numeric").val(function (index, value) {
            console.log(value.replace(/,/g, ''));
            //return value.trim();
        });

        //-- Function field change
        $('#daily_cost').on('change', function () {
            var newDailyCost = $(this).val().replace(/,/g, '');
            var newValue = parseInt($cummulativeCost) - parseInt($dailyCost) + parseInt(newDailyCost);
            $("#cummulative_cost").val(thousandSeparatorWithoutComma(newValue));
        });
        $('#daily_mud_cost').on('change', function () {
            var newDailyCost = $(this).val().replace(/,/g, '');
            var newValue = parseInt($cummulativeMudCost) - parseInt($dailyMudCost) + parseInt(newDailyCost);
            $("#cummulative_mud_cost").val(thousandSeparatorWithoutComma(newValue));
        });

        $(document).on('click', '.row-delete', function () {
            $(this).closest('.rowOperation').remove();
        });

        var $well_id = "";
        $("#well_id").cmSelect2({
            url: $.helper.resolveApi('~/core/well/lookup'),
            result: {
                id: 'id',
                text: 'well_name'
            },
            filters: function (params) {
                return [{
                    field: "well_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                //dropdownParent: $form,
                placeholder: "Select a Well",
                allowClear: true,
                maximumSelectionLength: 1,
            }
        });

        $("#well_id").on('select2:select', function (e) {
            var id = e.target.value;
            $well_id = id;
            getByWellId(id);
            GetWellObjectUomMap(id);
            
        });

        var getByWellId = function (id) {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            $.get($.helper.resolveApi('~/core/Well/' + id + '/detail'), function (r) {
                if (r.status.success) {
                    console.log("well detail");
                    console.log(r);
                    var calendarEl = document.getElementById('calendar');
                    calenderRender(calendarEl, r.data.spud_date);
                } else {
                    toastr.error(r.status.message);
                }
                $('#loadingModal').modal('hide');
                $('.loading-detail').hide();
            }).fail(function (r) {
                console.log(r);
            });
        }

        var todayDate = moment().startOf('day');
        var YM = todayDate.format('YYYY-MM');
        var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
        var TODAY = todayDate.format('YYYY-MM-DD');
        var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');


        document.addEventListener('DOMContentLoaded', function () {
            var calendarEl = document.getElementById('calendar');
            var event = {
                title: 'Dinner',
                start: TODAY + 'T20:00:00',
                className: "bg-danger border-danger text-white"
            };

            //calenderRender(calendarEl);
        });

        var calenderRender = function (calendarEl, spud_date) {
            $("#calendar").html("");
            var calendar = new FullCalendar.Calendar(calendarEl,
                {
                    plugins: ['dayGrid', 'list', 'timeGrid', 'interaction', 'bootstrap'],
                    themeSystem: 'bootstrap',
                    timeZone: 'local',
                    dateAlignment: "month", //week, month
                    defaultDate: spud_date,
                    buttonText:
                    {
                        today: 'today',
                        month: 'month',
                        week: 'week',
                        day: 'day',
                        list: 'list'
                    },
                    eventTimeFormat:
                    {
                        hour: 'numeric',
                        minute: '2-digit',
                        meridiem: 'short'
                    },
                    navLinks: true,
                    header:
                    {
                        left: 'prev,next today addEventButton',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
                    },
                    footer:
                    {
                        left: '',
                        center: '',
                        right: ''
                    },
                    customButtons:
                    {
                        addEventButton:
                        {
                            text: '+',
                            click: function () {
                                var dateStr = prompt('Enter a date in YYYY-MM-DD format');
                                var date = new Date(dateStr + 'T00:00:00'); // will be in local time

                                if (!isNaN(date.valueOf())) { // valid?
                                    calendar.addEvent(
                                        {
                                            title: 'dynamic event',
                                            start: date,
                                            allDay: true
                                        });
                                    alert('Great. Now, update your database...');
                                }
                                else {
                                    alert('Invalid date.');
                                }
                            }
                        }
                    },
                    height: 600,
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    selectable: true,
                    events: function (info, successCallback, failureCallback) {
                        var start_date = moment(info.start).format('YYYY-MM-DD');
                        var end_date = moment(info.end).format('YYYY-MM-DD');
                        console.log('date');
                        console.log(info);
                        $.ajax({
                            url: $.helper.resolveApi('~/UTC/auditdata/getListDrillingSubmitted/' + $well_id + "/" + start_date + "/" + end_date),
                            type: 'GET',
                            success: function (r) {
                                console.log('errr');
                                console.log(r);
                                var events = [];

                                $.each(r.data, function (key, value) {
                                    console.log(key);
                                    events.push({
                                        id: value.id,
                                        title: "Terisi",
                                        start: value.drilling_date, // will be parsed
                                        className: "bg-info border-info text-white"
                                    });
                                });
                                successCallback(events);
                            }
                        });
                    },
                    select: function (info) {
                        console.log('selected ' + info.startStr + ' to ' + info.endStr);
                    },
                    eventAfterAllRender: function (callback) {
                        alert();
                    },
                    eventRender: function (eventObj, $el) {
                        console.log($el);
                    },
                    eventDestroy: function () {

                    },
                    eventClick: function (info) {
                        loadDetail(info.event.id);
                        $("#default-example-modal-lg").modal();

                        console.log(info);
                    },
                    viewRender: function (view, element) {
                    }
                });
            calendar.render();
        };

        var $field_id = "";
        $("#field_id").cmSelect2({
            url: $.helper.resolveApi('~/core/BusinessUnitField/lookup?bussinessUnitID=' + bussinessUnitID),
            result: {
                id: 'field_id',
                text: 'field_name'
            },
            filters: function (params) {
                return [{
                    field: "field_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select a field",
                allowClear: true,
                maximumSelectionLength: 1,
            }
        });

        
        $("#field_id").on('select2:select', function (e) {
            var id = e.target.value;
            console.log("Selected field ID: ", id);
            multiTable(id);
        });

        async function multiTable(id) {
            console.log("Field ID passed to function: ", id);

            try {
                const apiUrl = $.helper.resolveApi('~/core/drilling/getLatestDrilling/' + id);
                const response = await $.ajax({
                    url: apiUrl,
                    type: 'GET',
                    contentType: 'application/json',
                    dataType: 'json'
                });

                console.log("Full API response:", response);

                const tableHeader = document.querySelector('#table-header');
                tableHeader.innerHTML = `<tr id="wellRow"><th>Well Name</th></tr>
                          <tr id="dayRow"><th>Day</th></tr>
                          <tr id="statusRow"><th>Status</th></tr>
                          <tr id="phaseRow"><th>Phase</th></tr>
                          <tr id="depthRow"><td style="font-weight: 500;">Depth</td></tr>`;

                if (response.status.success && response.data && Array.isArray(response.data) && response.data.length > 0) {
                    console.log('API response data:', response.data);

                    response.data.forEach((item, index) => {
                        const { drilling, well, drilling_operation_activity: operations, plannedTdUom, depthUom } = item;

                        addDataRow('wellRow', drilling.well_name || 'No Data');
                        addDataRow('dayRow', drilling.dfs || 'No Data');
                        addDataRow('statusRow', drilling.event || 'No Data');
                        addDataRow('phaseRow', operations && operations.length > 0 ? operations[0].category : 'No Data');

                        const depthChartRow = document.querySelector('#depthRow');
                        if (depthChartRow) {
                            const chartCell = document.createElement('td');
                            chartCell.innerHTML = `<div id="depthChart-${index}" style="width: 300px; height: 200px;"></div>`;
                            depthChartRow.appendChild(chartCell);

                            const plannedTdUomText = plannedTdUom || '';
                            const depthUomText = depthUom || '';

                            Highcharts.chart(`depthChart-${index}`, {
                                chart: {
                                    type: 'column',
                                    height: 150,
                                    width: 300
                                },
                                title: {
                                    text: null
                                },
                                xAxis: {
                                    categories: ['Depth'],
                                    labels: {
                                        enabled: true
                                    }
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: null
                                    },
                                    labels: {
                                        enabled: true
                                    }
                                },
                                legend: {
                                    enabled: true // Enable legend for other series
                                },
                                plotOptions: {
                                    column: {
                                        grouping: true, // Display columns side-by-side
                                        dataLabels: {
                                            enabled: true,
                                            formatter: function () {
                                                const uom = this.series.name === 'Planned Depth' ? plannedTdUomText : depthUomText;
                                                return `${this.y}`;
                                            },
                                            style: {
                                                fontSize: '12px'
                                            }
                                        }
                                    }
                                },
                                tooltip: {
                                    pointFormatter: function () {
                                        // Conditionally rename "Total Depth" as "Depth" in the tooltip
                                        const seriesName = this.series.name === 'Total Depth' ? 'Depth' : this.series.name;
                                        const uom = this.series.name === 'Planned Depth' ? plannedTdUomText : depthUomText;
                                        return `<span style="color:${this.color}">\u25CF</span> ${seriesName}: <b>${this.y} ${uom}</b><br/>`;
                                    }
                                },
                                series: [{
                                    name: 'Planned Depth',
                                    data: [
                                        well.planned_td || 0
                                    ],
                                    color: 'blue'
                                },
                                {
                                    name: 'Actual Depth',
                                    data: [
                                        (operations && operations.length > 0) ? operations[0].depth : 0
                                    ],
                                    color: '#ADD8E6'
                                },
                                {
                                    name: 'Total Depth',
                                    data: [
                                        (well.planned_td || 0) + ((operations && operations.length > 0) ? operations[0].depth : 0)
                                    ],
                                    color: 'white',
                                    showInLegend: false // Hide Total Depth from legend
                                }],
                                credits: { enabled: false }
                            });


                        }
                    });

                    loadPtNptChart(id);
                    loadChartStuck(id);
                } else {
                    console.warn("No data available from API or failed to fetch.");
                    fillNoData();
                }
            } catch (error) {
                console.error("Error fetching drilling data from API:", error);
                fillNoData();
            }
        }


        function addDataRow(rowId, text) {
            const row = document.querySelector(`#${rowId}`);
            if (row) {
                row.innerHTML += `<th class="text-center">${text}</th>`;
            }
        }

        function fillNoData() {
            ['wellRow', 'dayRow', 'statusRow', 'phaseRow', 'depthRow'].forEach(rowId => {
                addDataRow(rowId, 'No Data');
            });
        }

        

        function loadChartStuck(id) {
            const apiUrl = $.helper.resolveApi('~/core/drilling/getDrillingPipe/' + id);

            $.ajax({
                url: apiUrl,
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    console.log('Response from API:', response);

                    let categories = [];
                    let seriesData = [];

                    if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                        console.log('API response data:', response.data);
                        
                        response.data.forEach((item) => {
                            const wellName = item.well_name; 
                            const totalDuration = item.total_operation_duration_hours;
                            
                            if (!categories.includes(wellName)) {
                                categories.push(wellName);
                            }
                            
                            seriesData.push({
                                name: wellName,
                                y: totalDuration || 0, // Use 0 if no duration is available
                                //description: item.iadc_description || 'No description available'
                            });
                        });

                    } else {
                        console.warn('No data available for stuck pipe.');
                        categories = ['No Data'];
                        seriesData.push({
                            name: 'No Data',
                            y: 0,
                            //description: 'No description available'
                        });
                    }

                    // Initialize the chart
                    Highcharts.chart('chartStuck', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: null
                        },
                        xAxis: {
                            categories: categories,
                            title: {
                                text: null
                            },
                            labels: {
                                rotation: -45,
                                style: {
                                    fontSize: '10px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: null
                            }
                        },
                        tooltip: {
                            pointFormat: '<strong>{point.y:.2f} Hours</strong>'
                        },
                        //plotOptions: {
                        //    column: {
                        //        stacking: 'normal',
                        //        dataLabels: {
                        //            enabled: false  
                        //        }
                        //    }
                        //},
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: true,
                                    format: '{y}', // Displays the actual data value as a label
                                    style: {
                                        fontSize: '14px' // Adjust the font size as needed
                                    }
                                }
                            }
                        },
                        series: [{
                            name: 'Stuck Pipe',
                            data: seriesData
                        }]
                    });

                    console.log('Chart initialized with IADC codes and descriptions.');
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                }
            });
        }


        function loadPtNptChart(id) {
            console.log("Fetching PT/NPT data for fieldId: ", id);

            //const apiUrl = $.helper.resolveApi('~/core/drilling/getLatestDrilling/' + id);
            const apiUrl = $.helper.resolveApi('~/core/drilling/getLatestDrillingPTNPT/' + id);

            $.ajax({
                url: apiUrl,
                type: 'GET',
                contentType: 'application/json',
                dataType: 'json',
                success: function (response) {
                    console.log('Response Chart from API:', response);

                    let categories = [];
                    let ptData = [];
                    let nptData = [];

                    if (response.status.success && Array.isArray(response.data) && response.data.length > 0) {
                        response.data.forEach((item) => {
                            let totalPtHours = parseFloat(item.total_hours_pt) || 0;
                            let totalNptHours = parseFloat(item.total_hours_npt) || 0;

                            categories.push(item.well_name);
                            ptData.push(totalPtHours);
                            nptData.push(totalNptHours);
                        });

                        // Initialize the chart with the data
                        Highcharts.chart('ptNptMulti', {
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: null
                            },
                            xAxis: {
                                categories: categories
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: null
                                }
                            },
                            tooltip: {
                                pointFormat: '<strong>{point.y:.2f} Hours</strong>'
                            },
                            plotOptions: {
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: true,
                                        format: '{y}', // Displays the actual data value as a label
                                        style: {
                                            fontSize: '14px' // Adjust the font size as needed
                                        }
                                    }
                                }
                            },
                            series: [{
                                name: 'PT',
                                data: ptData,
                                color: 'green'
                            }, {
                                name: 'NPT',
                                data: nptData,
                                color: 'red'
                            }]
                        });

                        console.log('Chart initialized.');
                    } else {
                        console.warn('No data available for chart.');
                    }
                },
                error: function (xhr, status, error) {
                    console.error('Error during API call:', error);
                }
            });
        }


        return {
            init: function () {
                //$(":input").inputmask();
                //loadDetail();
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });


});