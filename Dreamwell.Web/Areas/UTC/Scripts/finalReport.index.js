﻿$("#lbl_test").text("SEBELUM DOCUMENT JAVASCRIPT");
//window.status = 'ready_to_print';
//window.status = 'PAGE_LOADED';
$(document).ready(function () {
    window.arrwellcheckbox = ["0"];
    function thousandSeparatorFromValueWithComma(nStr) {
        var x = (nStr + "").split(".");
        var x1 = x[0];
        var x2 = x.length > 1 ? "." + x[1] : ".00";
        var rgx = /(\d+)(\d{3})/;
        if (x1 == 0) {
            x1 = 0;
        } else {
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, "$1" + "," + "$2");
            }
        }
        if ((x2 + "").length == 2) return x1 + x2 + "0";
        else return x1 + x2;
    }
    Handlebars.registerHelper("thousandSeparatorDecimal", function (n) {
        if (n == null || n == "" || n == 0) return "0.00";
        else return thousandSeparatorFromValueWithComma(n);
    });

    Handlebars.registerHelper("CasingType", function (n) {
        if (n == 1) return "Conductor";
        else if (n == 2) return "Surface";
        else if (n == 3) return "Drilling Liner";
        else if (n == 4) return "Intermediate";
        else if (n == 5) return "Production Liner";
        else if (n == 6) return "Perforated Liner";
        else if (n == 7) return "Production";
    });

    var template = undefined;

    $("#lbl_test").text("TEST FROM JAVASCRIPT");
    var wellId = $("input[name=well_id]").val();

    function getDataWell() {
        return $.ajax({
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            url: urlAPI + "/core/Well/" + wellId + "/detail",
            async: false,
        }).responseText;
    }

    function getWellBit() {
        var templateScript = $("#bitReportTemplate").html();
        var template = Handlebars.compile(templateScript);
        $.ajax({
            type: "GET",
            url: urlAPI + "/UTC/SingleWell/GetBitByWellId/" + wellId,
            contentType: "application/json",
            //dataType: 'json',
            success: function (r) {
                $("#section-bitReport").html(template({ data: r.data }));
            },
            error: function () { },
        });
    }
    getWellBit();

    function getDataPlanVsActualDepth() {
        var aryWell = [];
        aryWell.push("4c1c8437-9802-481b-8258-c3eeb1d5cde0");
        var data = new Object();
        data.well_id = new Object();
        data.well_id = aryWell;
        data.field_id = "1f73d67f-e056-4c75-8215-99c1682745bf";

        return $.ajax({
            type: "POST",
            //dataType: 'json',
            contentType: "application/json",
            url: urlAPI + "/UTC/Benchmark/getPlanVsActualDepth/",
            data: JSON.stringify(data),
            async: false,
        }).responseText;
    }

    function getCostByWellId() {
        $.ajax({
            type: "GET",
            url: urlAPI + "/utc/WellData/" + wellId + "/dailyCost",
            success: function (response) {
                var data = response.data;
                var data1 = data.children;
                //console.log("daily", data1);
                getChildTree(data.children, 1);
                getSurveyActual();
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
            }
        });
    }

    getCostByWellId();

    var getChildTree = function (data, hierarchy) {
        var space = 15 * hierarchy;
        data.forEach(item => {
            if (item.children.length > 0 || item.Currency == null) {
                var body = `<tr>
                        <td><span style="padding-left: ${space}px">${item.Description}</span></td>
                        <td></td>
                        <td></td>
                    </tr>`;

                $("#tableDailyCost > tbody").append(body);
                getChildTree(item.children, hierarchy + 1);
                //console.log(item.Description + hierarchy)
                //console.log("jajaja", hierarchy)
            } else {
                var body = `<tr>
                        <td><span style="padding-left: ${space}px">${item.Description}</span></td>
                        <td>${item.Currency} ${item.total_price_plan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                        <td>${item.Currency} ${item.total_price_actual.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                    </tr>`;

                $("#tableDailyCost > tbody").append(body);
            }
        });
    }

    function getDataWellHoleAndCasing() {
        $.ajax({
            type: "GET",
            url: urlAPI + "/Core/WellHoleAndCasing/" + wellId + "/getAllByWellId",
            contentType: "application/json",
            //dataType: 'json',
            success: function (r) {
                var templateSusunanCasingScript = $(
                    "#susunanCasingTemplate"
                ).html();
                var templateSusunanCasing = Handlebars.compile(
                    templateSusunanCasingScript
                );
                $("#susunanCasing").html(
                    templateSusunanCasing({ data: r.data })
                );

                var templateHoleCasingScript = $(
                    "#holeCasingPlanTemplate"
                ).html();
                var templateHoleCasing = Handlebars.compile(
                    templateHoleCasingScript
                );
                $("#lampiran-4-holeCasingPlan").html(
                    templateHoleCasing({ data: r.data })
                );
            },
            error: function () { },
        });
    }
    getDataWellHoleAndCasing();

    function getHoleCasingActual() {
        $.ajax({
            type: "GET",
            url: urlAPI + "/UTC/SingleWell/GetHoleCasingActual/" + wellId,
            contentType: "application/json",
            //dataType: 'json',
            success: function (r) {
                var templateHoleCasingScript = $(
                    "#holeCasingActualTemplate"
                ).html();
                var templateHoleCasing = Handlebars.compile(
                    templateHoleCasingScript
                );
                $("#lampiran-4-holeCasingActual").html(
                    templateHoleCasing({ data: r.data })
                );
            },
            error: function () { },
        });
    }
    getHoleCasingActual();

    function getDrillingDeviation() {
        $.ajax({
            type: "GET",
            url: urlAPI + "/core/WellDeviation/" + wellId + "/detailByWellId",
            contentType: "application/json",
            //dataType: 'json',
            success: function (r) {
                console.log("WellDeviation");
                //console.log("INI WEL", wellId);
                GetUomForChart();
                Analysis();
                getAfeData();
                getSurveyProgram();
                //console.log(r);
                var templateScript = $("#drillingDeviationTemplate").html();
                var template = Handlebars.compile(templateScript);
                $("#lampiran-6-directionalSurvey").html(
                    template({ data: r.data })
                );
            },
            error: function () { },
        });
    }
    getDrillingDeviation();

    function getDataActivityByWellId() {
        return $.ajax({
            type: "GET",
            contentType: "application/json",
            url:
                urlAPI +
                "/Core/DrillingOperationActivity/getByWellId?wellId=" +
                wellId,
            async: false,
        }).responseText;
    }

    function getBABInformation() {
        return $.ajax({
            type: "GET",
            contentType: "application/json",
            url: urlAPI + "/Core/WellFinalReport/" + wellId + "/detailByWellId",
            async: false,
        }).responseText;
    }

    function getSurveyProgram() {
        var templateScript = $("#surveyprogram-template").html();
        var template = Handlebars.compile(templateScript);
        $.ajax({
            url: urlAPI + '/core/WellDeviation/' + wellId + '/detailByWellId',
            type: 'GET',
            dataType: 'json',
            success: function (r) {
                if (r.status.success && r.data.length > 0) {
                    $("#tblSurveyProgram > tbody").html(template({ data: r.data }));
                }
            },
            error: function (xhr, status, error) {
                // console.log(xhr);
            },
            complete: function () {
                // Callback function after request completion
            }
        });
    }




    function getMonitoringPemboran() {
        return $.ajax({
            type: "GET",
            contentType: "application/json",
            url: urlAPI + "/UTC/SingleWell/GetTataWaktuPengeboran/" + wellId,
            async: false,
        }).responseText;
    }

    function getAfeData() {
        var responseData;
        $.ajax({
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            url: urlAPI + "/core/drilling/" + wellId + "/detailbyId",
            async: false,
            success: function (data) {
                responseData = data;
                getMonitoringData(responseData);
            },
            error: function (xhr, status, error) {
                console.error("Error fetching AFE data:", error);
            }
        });
        return responseData;
    }
    function getMonitoringData(data) {
        var m = data;
        //console.log("ini ujasdasd", m.data.length)
        for (var i = 0; i < m.data.length; i++) {
            var item = m.data[i];
            //console.log("omo",item.drilling_dateyyyyy)
            var space = 20; // Sesuaikan dengan jumlah spasi yang Anda inginkan
            var body = `
            <tr>
                <td class="align-middle text-center" style="width: 5%;">${item.report_no}</td>
                <td class="align-middle text-center" style="width: 20%;">${item.drilling_date}</td>
                <td class="text-right">$ ${item.daily_cost.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</td>
                <td class="text-right">$ ... </td>
                <td class="text-right">$ ... %</td>
            </tr>`;
            $("#tableMonitoringBudget > tbody").append(body);
        }
    }

    function getDrilling() {
        $.ajax({
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            url: urlAPI + "/core/drilling/" + wellId + "/detailbyId",
            success: function (data) {
                getLoopChemical(data);
            },
            error: function (xhr, status, error) {
                console.error("Error fetching drilling data:", error);
            }
        });
    }

    getDrilling();

    function getLoopChemical(drillingData) {
        var drill = drillingData.data;
        for (i = 0; i < drill.length; i++) {
            var drillingId = drill[i].id;
            getChemical(drillingId);
        }
    }

    function getChemical(recordId) {
        $.ajax({
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            url: urlAPI + '/core/drillingchemicalused/' + recordId + '/getByDrillingId',
            success: function (r) {
                //console.log("iaiaiaiaia", r.data)
                if (r.data !== null) {
                    var body = `
                             <tr>
                             <td class="fs-13">${r.data.chemical_type !== null ? r.data.chemical_type : '-'}</td>
                             <td class="fs-13"></td>
                             <td class="fs-13"></td>
                             <td class="fs-13">${r.data.amount !== null ? r.data.amount : '-'}</td>
                             </tr>
                             `;
                    $("#tblRecapSection > tbody").append(body);
                }
            },
            error: function (xhr, status, error) {
                // console.log(xhr);
            },
            complete: function () {
                // Callback function after request completion
            }
        });
    }

    function getSurveyActual() {
        return $.ajax({
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            url: urlAPI + "/core/drilling/" + wellId + "/getFirst",
            async: false,
            success: function (data) {
                var idDrill = data.data.id;
                getDataSurveyActual(idDrill);
            },
            error: function (xhr, status, error) {
                console.error("Error fetching AFE data:", error);
            }
        });
    }

    function getDataSurveyActual(drillingId) {
        var templateScript = $("#surveyactual-template").html();
        var template = Handlebars.compile(templateScript);
        console.log("ini driling", drillingId);
        $.ajax({
            url: urlAPI + '/core/DrillingDeviation/' + drillingId + '/detailByDrillingId',
            type: 'GET',
            dataType: 'json',
            success: function (r) {
                // console.log('drillingDeviationDt');
                // console.log(r.data);
                if (r.status.success && r.data.length > 0) {
                    $("#tblSurveyActual > tbody").html(template({ data: r.data }));
                } else {
                    $("#tblSurveyActual > tbody").html(`<tr class="no-data">
                                                <td colspan="10" class="text-center">No data available. Add Row to input new data.</td>
                                            </tr>`);
                }
            },
            error: function (xhr, status, error) {
                // console.log(xhr);
            },
            complete: function () {
                // Callback function after request completion
            }
        });
    }


    function getFristDrill() {
        return $.ajax({
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            url: urlAPI + "/core/drilling/" + wellId + "/getFirst",
            async: false,

        }).responseText;
    }
    var dataMonitorBudget = JSON.parse(getFristDrill());

    if (dataMonitorBudget.status.success) {
        $("#afe_info").find("span[data-objecttype='label']").text("N/A");
        $("#afe_info").find("span[data-objecttype='label'][data-field]").each(function () {
            var field = $(this).data("field");
            if (dataMonitorBudget.data[field] !== undefined) {
                $(this).text(dataMonitorBudget.data[field]);
            }
        });
        $(".afe_no").html(dataMonitorBudget.data.afe_no);
        $("span[name=lbl_afe_no]").html(dataMonitorBudget.data.afe_no);
    }

    var dataWell = JSON.parse(getDataWell());

    if (dataWell.status.success) {
        controlHelper.form.clear($("#well_info"));
        controlHelper.form.fill($("#well_info"), dataWell.data);
        $(".well_name").html(dataWell.data.well_name);
        $("span[name=lbl_well_name]").html(dataWell.data.well_name);
    }

    var dataSurvey = JSON.parse(getDataWell());

    if (dataSurvey.status.success) {
        controlHelper.form.clear($("#survey_program"));
        controlHelper.form.fill($("#survey_program"), dataSurvey.data);
        $(".well_name").html(dataSurvey.data.well_name);
        $("span[name=well_name]").html(dataSurvey.data.well_name);
    }

    var activity = JSON.parse(getDataActivityByWellId());
    template = Handlebars.compile($("#activityTemplate").html());
    $("#drillingActivity").html(template(activity));

    var bab3Information = JSON.parse(getBABInformation());
    template = Handlebars.compile($("#bab3InformationTemplate").html());
    if (bab3Information.status.success) {
        $("#bab3Information").html(template(bab3Information.data));
    }

    var bab4Information = JSON.parse(getBABInformation());
    template = Handlebars.compile($("#bab4InformationTemplate").html());
    if (bab4Information.status.success) {
        $("#bab4Information").html(template(bab4Information.data));
    }

    var babInformation = JSON.parse(getBABInformation());
    template = Handlebars.compile($("#babInformationTemplate").html());

    if (babInformation.status.success) {
        console.log("ini pegawai")
        babInformation.data.well_name = dataWell.data.well_name;
        $("#babInformation").html(template(babInformation.data));
    }

    var dataMonitoringPemboran = JSON.parse(getMonitoringPemboran());

    if (dataMonitoringPemboran.status.success) {
        console.log("ini bor", dataMonitoringPemboran);
        var planningDepth = [];
        var planningCost = [];

        var actualDepth = [];
        var actualCost = [];

        $.each(
            dataMonitoringPemboran.data.report_drilling,
            function (key, value) {
                var _actualDepth = [value.dfs, value.current_depth_md];
                actualDepth.push(_actualDepth);

                var _actualCost = [value.dfs, value.daily_cost];
                actualCost.push(_actualCost);
            }
        );

        $.each(dataMonitoringPemboran.data.tvd_plan, function (key, value) {
            var _planningDepth = [value.days, value.depth];
            planningDepth.push(_planningDepth);

            var _planningCost = [value.days, value.cost];
            planningCost.push(_planningCost);
        });
    }

    function getBABInformation() {
        return $.ajax({
            type: "GET",
            contentType: "application/json",
            url: urlAPI + "/Core/WellFinalReport/" + wellId + "/detailByWellId",
            async: false,
        }).responseText;
    }

    function GetUomForChart() {
        return $.ajax({
            type: "GET",
            contentType: "application/json",
            url: urlAPI + "/core/wellobjectuommap/GetAllByWellId/" + wellId,
            async: false,
            success: function (r) {
                if (r.status.success && r.data.length > 0) {
                    $.each(r.data, function (key, value) {
                        if ('tvd depth' == (value.object_name || '').toLowerCase().trim()) {
                            window.depthuom = value.uom_code;
                        }
                    });
                }

                chartTVD();
                $('.loading-detail').hide();
            },
            error: function (r) {
                //console.log(r);
            }
        });
    }
    //console.log("ini wll id", wellId)

    function chartTVD() {
        if (wellId !== '') {
            return $.ajax({
                type: "GET",
                contentType: "application/json",
                url: urlAPI + "/UTC/SingleWell/GetTataWaktuPengeboranNew/" + wellId,
                async: false,
                success: function (response) {
                    if (response.status.success) {
                        var date = [];
                        var days = [];
                        var actualDepth = [];
                        var dailyCost = [];
                        var tvdDepth = [];
                        var cumCost = [];
                        var arrtvdDepth = [];
                        var arrcumCost = [];
                        var drillingActivity = [];
                        var cost = 0;

                        $.each(response.data.report_drilling, function (key, value) {
                            date.push(moment(value.drilling_date).format('YYYY-MM-DD'));
                            days.push(value.dfs == 1 ? 1 : value.dfs);
                            var _actualDepth = [value.dfs, value.current_depth_md == null ? 0 : value.current_depth_md];
                            actualDepth.push(_actualDepth);
                            cost += value.daily_cost;
                            cumCost.push(cost);

                            if (value.operation_activity.length > 0) {
                                $.each(value.operation_activity, function (keyDOA, valueDOA) {
                                    drillingActivity.push(valueDOA.description);
                                });
                            }
                        });

                        $.each(response.data.tvd_plan, function (key, value) {
                            var _planningDepth = [value.days, value.depth];
                            arrtvdDepth.push(_planningDepth);

                            var _planningCost = [value.days, value.cumm_cost];
                            arrcumCost.push(_planningCost);
                        });

                        $.each(response.data.drilling_data, function (key, value) {
                            var _actualCost = [value.cummulative_cost_count == null ? 0 : value.cummulative_cost_count];
                            dailyCost.push(_actualCost);
                        });

                        $('#tvdchart').highcharts({
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            credits: {
                                enabled: false
                            },
                            yAxis: [{ // Primary yAxis
                                floor: 0,
                                ceiling: 10000,
                                labels: {
                                    format: '{value} ' + window.depthuom,
                                    style: {
                                        color: Highcharts.getOptions().colors[1]
                                    }
                                },
                                title: {
                                    text: 'Depth',
                                    style: {
                                        color: Highcharts.getOptions().colors[1]
                                    }
                                },
                                reversed: true
                            }, { // Secondary yAxis
                                floor: 0,
                                title: {
                                    text: 'Cost',
                                    style: {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                                },
                                labels: {
                                    format: '{value} USD',
                                    style: {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                                },
                                opposite: true
                            }],
                            xAxis: {
                                floor: 0,
                                ceiling: 10000,
                                labels: {
                                    format: '{value}',
                                    step: 0
                                },
                                title: {
                                    text: 'Days'
                                },
                                accessibility: {
                                    rangeDescription: 'Range: 0 to 200'
                                }
                            },
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            },

                            plotOptions: {
                                series: {
                                    label: {
                                        connectorAllowed: false
                                    },
                                    pointStart: 0,
                                }
                            },
                            series: [{
                                name: 'Actual Depth',
                                data: actualDepth,
                            },
                            {
                                name: 'Cummulative Cost',
                                yAxis: 1,
                                data: cumCost
                            },
                            {
                                name: 'Planning Depth',
                                data: arrtvdDepth
                            },
                            {
                                name: 'Planning Cost',
                                yAxis: 1,
                                data: arrcumCost
                            }],
                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 1000
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }
                        });
                    }
                }
            });
        }
    }

    //chartTVD();


    Highcharts.chart("container", {
        title: {
            text: "Solar Employment Growth by Sector, 2010-2016",
        },

        subtitle: {
            text: "Source: thesolarfoundation.com",
        },

        yAxis: {
            title: {
                text: "Number of Employees",
            },
        },

        xAxis: {
            accessibility: {
                rangeDescription: "Range: 2010 to 2017",
            },
        },

        legend: {
            layout: "vertical",
            align: "right",
            verticalAlign: "middle",
        },

        plotOptions: {
            series: {
                animation: false,
            },
        },

        series: [
            {
                name: "Installation",
                data: [
                    43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175,
                ],
            },
            {
                name: "Manufacturing",
                data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434],
            },
            {
                name: "Sales & Distribution",
                data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387],
            },
            {
                name: "Project Development",
                data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227],
            },
            {
                name: "Other",
                data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111],
            },
        ],

        responsive: {
            rules: [
                {
                    condition: {
                        maxWidth: 500,
                    },
                    chartOptions: {
                        legend: {
                            layout: "horizontal",
                            align: "center",
                            verticalAlign: "bottom",
                        },
                    },
                },
            ],
        },
    });
    window.status = "PAGE_LOADED";
    //setTimeout(function () {
    //    window.status = 'PAGE_LOADED';

    //}, 2000);



    function getIadcAnalysis() {
        return $.ajax({
            type: "GET",
            contentType: "application/json",
            url: urlAPI + "/Core/WellFinalReport/" + wellId + "/detailByWellId",
            async: false,
        }).responseText;
    }

    // Fungsi untuk mengambil data IADC Analysis dari API
    function getDataIadcAnalysis() {
        var responseData = null;
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: urlAPI + "/UTC/SingleWell/GetIadcAnalysis/" + wellId,
            async: false, // Pemanggilan sinkron
            success: function (response) {
                responseData = response;
            },
            error: function (xhr, status, error) {
                console.error("Failed to fetch IADC Analysis data:", error);
            }
        });
        return responseData;
    }

    // Fungsi untuk melakukan analisis IADC
    function Analysis() {
        // Tampilkan panel trayek
        $(".trayek-panel").show();

        // Ambil data IADC Analysis dari API
        var data = getDataIadcAnalysis();
        // console.log("ini data ehhe", data);
        // Periksa apakah data tidak kosong
        if (data != null) {
            // Ambil template Handlebars untuk trayek
            var templateScript = $("#trayek-template").html();
            var template = Handlebars.compile(templateScript);

            // Render data ke dalam template
            $("#trayek-content").html(template(data));

            // Iterasi setiap data untuk membuat chart
            $.each(data.data, function (key, value) {
                trayekChart(value.id, value.iadcAnalysis, value.iadcAnalysisNpt);
            });
        } else {
            // Tampilkan pesan jika tidak ada data trayek
            $("#trayek-content").html(`<div class="col-md-12 pt-3 pb-2 text-center"><p class="h6">No trayek record on well available</p></div>`);
        }
    }


    function trayekChart(id, analysis, analysisnpt) {
        var obj = {};
        var arXAnalysis = [];
        var aryColor = [];
        var total = 0;
        //console.log("ini analysusus", analysis)
        for (var i = 0; i < analysis.length; i++) {
            // console.log(analysis[i].interval)
            total = total + analysis[i].interval;
        }
        console.log(total);
        var datanptCount = 0;
        if (analysisnpt != null) {
            for (var i = 0; i < analysisnpt.length; i++) {
                var datanpt = analysisnpt[i];
                datanptCount += datanpt.hours;
            }
        }
        for (var i = 0; i < analysis.length; i++) {
            obj["NPT"] = 'rgb(255, 0, 0)';

            if (obj[analysis[i].iadc_code] == "" || obj[analysis[i].iadc_code] == null) {
                if (analysis[i].iadc_code != null) {
                    obj[analysis[i].iadc_code] = "#" + Math.floor(Math.random() * 16777215).toString(16);;
                }
            }

            if (analysis[i].iadc_code == null) {
                aryColor.push(obj["NPT"]);
            } else {
                aryColor.push(obj[analysis[i].iadc_code]);
            }

            if (analysis[i].type == "NPT") {
                var time = datanptCount;
            } else {
                var time = analysis[i].interval;

            }
            //console.log(total)
            arXAnalysis.push(
                {
                    well_hole_casing_id: id,
                    name: analysis[i].description,
                    y: analysis[i].total,
                    Time: time,
                    Type: analysis[i].type,
                    total: total,
                    percentageManual: (analysis[i].interval / total) * 100,
                    // percentageManual: analysis[i].total,
                    sliced: i == 0 ? true : false,
                    selected: i == 0 ? true : false
                }
            );

        }
        //console.log('total : ', arXAnalysis);
        $('#trayek-' + id).highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                margin: [0, 0, 0, 0],
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<strong>{point.Type}</strong><br><strong>{point.Time:.1f} Hrs</strong>'
            },
            plotOptions: {
                pie: {
                    //size: '50%',

                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentageManual:.2f} % '
                    },
                    colors: aryColor
                }
            },
            series: [
                {
                    name: 'Brands',
                    stroke: '#94236a',
                    colorByPoint: true,
                    data: arXAnalysis,
                    point: {
                        events: {
                            click: function (event) {
                                if (this.Type == "NPT")
                                    //alert(this.well_hole_casing_id);
                                    $("#btnWellHoleCasing-" + this.well_hole_casing_id).click();
                            }
                        }
                    }
                },
                //series: [{
                //    data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
                //}
            ]
        });
    }


});
