﻿$(document).ready(function () {
    var pageFunction = function () {
        var wellProfile = function () {
            //alert(drillingId); // Pastikan drillingId sudah didefinisikan sebelumnya
            var templateScript = $("#wellProfileDetail-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/well/' + $wellId.val() + '/detail'), function (r) {
                if (r.data) {
                    $("#tableWellDetailProfile > tbody").html(template(r.data));
                    console.log('profile');
                    console.log(r.data.spud_date);
                    plan_depth = r.data.planned_td;
                    lodaDate(r.data.spud_date);
                    //datepickerFormat($('.date-picker'));
                    //iadcLookUp();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                // Handle failure if necessary
            }).done(function () {
                // Any additional actions after completion if necessary
            });
        };

        // Call the wellProfile function
        wellProfile();
    };

    return {
        init: function () {
            pageFunction();
        }
    };
});
