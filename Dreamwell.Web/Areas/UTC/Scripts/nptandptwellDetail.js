﻿$(document).ready(function () {
    'use strict';
    var $recordId = $("#recordId");

    var pageFunction = function () {
        console.log("nilai record id : " + $recordId);

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                var arXAnalysis = [];
                var totalNPTANDPT = [];
                var color = [];

                $.get($.helper.resolveApi("~/UTC/SingleWell/GetWellIadc/" + $recordId.val()), function (r) {
                    console.log("GetIadcAnalysisNPTandPT");
                    console.log(r);
                    if (r.status.success) {
                        if (r.data.length > 0) {
                            var totalNPT = 0;
                            var totalPT = 0;
                            for (var j = 0; j < r.data.length; j++) {
                                totalNPT = totalNPT + r.data[j].total_hours_npt;
                                totalPT = totalPT + r.data[j].total_hours_pt;
                            }
                            console.log("total npt " + totalNPT);
                            console.log("total pt " + totalPT);
                            arXAnalysis.push(
                                {
                                    name: "NPT",
                                    y: totalNPT,
                                    Type: "totalNPT",
                                }
                            );
                            color.push('#FF0000');


                            arXAnalysis.push(
                                {
                                    name: "PT",
                                    y: totalPT,
                                    Type: "totalPT",
                                }
                            );

                            color.push('#0000FF');


                            console.log("nilai ar x analysus " + arXAnalysis);

                            console.log("total colors " + color);
                       

                            $('#nptAndptchart').highcharts({
                                chart: {
                                    plotBackgroundColor: null,
                                    plotBorderWidth: null,
                                    plotShadow: false,
                                    type: 'pie',
                                    margin: [0, 0, 0, 0],
                                    spacingTop: 0,
                                    spacingBottom: 0,
                                    spacingLeft: 0,
                                    spacingRight: 0
                                },
                                title: {
                                    text: ''
                                },
                                tooltip: {
                                    pointFormat: '<strong>{point.Type}</strong><br><strong>{point.Time:.2f} Hrs</strong>'
                                },
                                plotOptions: {
                                    pie: {
                                        size: '40%',
                                        allowPointSelect: true,
                                        cursor: 'pointer',
                                        dataLabels: {
                                            enabled: true,
                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                        }
                                    }
                                },
                                series: [{
                                    name: 'NPT AND PT WELL',
                                    colorByPoint: true,
                                    data: arXAnalysis
                                }]
                            });

                            console.log("test done gak or reload");
                        }
                        else {
                            toastr.error("data npt and pt for this well is still empty");
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }

               else {

            }
        };


        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});