﻿$(document).ready(function () {
    'use strict';
    var $dt_well = $('#dt_well');
    var $pageLoading = $('#page-loading-content');

    var pageFunction = function () {

        var loadDataTable = function () {

            var dt = $dt_well.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/Well/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "field_name" },
                    { data: "well_name" },
                    { data: "well_type" },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.id + `" >
                                    <a class ="row-edit btn btn-xs btn-info btn-hover-info fa fa-pencil add-tooltip" href="/Core/Well/Detail?id=`+ row.id + `"
                                        data-original-title="Edit" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        }, width: "10%"
                    }
                ],
                initComplete: function (settings, json) {
                        $(this).on('click', '.row-edit', function () {

                            var recordId = $(this).closest('.btn-group').data('id');

                            $WellFormModal.modal('show'), $form.loading('start');
                            $.helper.form.clear($form);
                            $.get($.helper.resolveApi('~/core/Well/' + recordId + '/detail'), function (r) {

                                if (r.status.success) {
                                    $.helper.form.fill($form, r.data);
                                }
                                $form.loading('stop');
                            }).fail(function (r) {
                                toastr.error(r.statusText);
                                $form.loading('stop');
                            });

                        });
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
                console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });


});