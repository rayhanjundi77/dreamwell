﻿(function ($) {
    'use strict';
    

    var pageFunction = function () {
        var $recordId = $('input[id=recordId]');
        var $btnFinalReport = $("#btnFinalReport");
        var $form = $("#form-well-detail");
        var $bhaModal = $('#bhaModal');
        var $wellId = $('input[id=id]');
        var $formWellDDR = $("#form-wellDDR");
        var $formOperationActivities = $("#formOperationActivities");
        var $formDrillingOperation = $("#formDrillingOperation");
        var $formHSSE = $("#formHSSE");
        var $formdrillingBHA = $("#form-drillingBHA");
        var $formDrillingBit = $("#formDrillingBit");
        var $formMudloggingData = $("#formMudloggingData");
        var $formDrillingDeviation = $("#formDrillingDeviation");
        var $form_mud1 = $("#form-mud1");
        var $form_mud2 = $("#form-mud2");
        var $form_mud3 = $("#form-mud3");
        var $formTotalVolume = $("#form-totalVolume");
        var $formChemicalUse = $("#form-chemicalUse");
        var $formDrillingWeather = $('#formDrillingWeather');


        var plan_depth = 0; //planning
        var cost_max = 0;

        var total_drilling_num = 0;
        var total_size_timeline = 0;

        var plan_drilling_num = 0;
        window.heightBackgroundGrey = 0;

        var cost_min = 0;

        var over_time = false; // true or false

        // init variable
        var diameter = 0;
        var phase_depth = 0;
        var depth = 0;
        var target_depth = 0;
        var cost = 0;
        var budget = 0;
        var depth_info = '';
        var cost_info = '';
        var bha = "";
        var type = "";
        var rig = "";
        var phase = '';
        var status = "No Issue";

        // WEATHER INFO
        var weather = 'sun';
        // sun, cloud-sun, cloud-drizzle-sun, cloud-drizzle, rainy, lightning
        var wind_speed = '';
        var barometer = '';
        var wind_direction = '';
        var wave = '';
        var comment = '';
        var temperature = '';
        var wave_period = '';
        var visibility = '';
        var wave_direction = '';
        var cloud = '';
        var height = '';

        var date_data = 0;

        var drillingList;
        var plan = 0;
        var heightWell = 0;

        var dataAllObj = [];
        var holeAndCasing = [];

        if ($recordId.val() != "") {
            getWellDetail();
        }

        async function getWellDetail() {
            let result;
            try {
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi('~/core/Well/' + $recordId.val() + '/detail'),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (rwell) {
                        $.helper.form.fill($form, rwell.data);
                        plan_drilling_num = rwell.data.planned_days;

                        $.get($.helper.resolveApi('~/UTC/SingleWell/GetDrillingByWell/' + $recordId.val()), function myfunction(r) {
                            if (r.status.success && r.data.length > 0) {
                                $("#well-schematic-loading").remove();
                                $("#well-schematic-section").show();
                                //-- Drill Formation Plan
                                getDrillingFormationPlan($recordId.val());




                                $("#singleWellMain").show();
                                $("#footer-main-report").hide();

                                plan_depth = r.data[0].planned_td;
                                cost_max = r.data[0].afe_cost;

                                total_drilling_num = r.data.length;
                                plan = Math.ceil(r.data[0].planned_days);
                                var acplan = r.data.length;
                                //console.log("ini days",r.data);
                                //total_size_timeline = Math.ceil($('.timeline-track').outerHeight()) / (total_drilling_num);
                                //total_size_timeline = 600 / (total_drilling_num / 0.93);
                                total_size_timeline = 600 / plan;

                                //// console.log("Height: " + $('.timeline-track').outerHeight());

                                if (plan > 1)
                                    $("#planned_days").html(thousandSeparatorDecimal(plan) + " days");
                                else
                                    $("#planned_days").html(thousandSeparatorDecimal(plan) + " day");

                                if (plan > 1)
                                    $("#actual_days").html(thousandSeparatorDecimal(acplan) + " days");
                                else
                                    $("#actual_days").html(thousandSeparatorDecimal(acplan) + " day");

                                var spud = r.data[0].spud_date;
                                var date = new Date(r.data[0].spud_date);

                                $("#afe_cost").html("USD " + thousandSeparatorDecimal(cost_max));
                                $("#planned_td").html(thousandSeparatorDecimal(plan_depth) + " ft");



                                window.objDayDrill = {
                                    'total_drilling_num': total_drilling_num,
                                    'plan': plan,
                                    'data': r.data,
                                };




                                GetWellObjectUomMap(plan, date, r.data)
                                //RPM(plan, date, r.data);
                                //ROP(plan, date, r.data);
                                //Torsi(plan, date, r.data);
                                //AVGSpp(plan, date, r.data);
                                //flowrateGPM(plan, date, r.data);
                                stackedBarPTNPT();
                                Analysis();
                            } else {
                                $("#singleWellMain").hide();
                                $("#footer-main-report").show();
                                $("#footer-main-report > div").html("No drilling record available on this well.");

                                //$("#formModal > .panel").hide();
                                //$("#formModal > .panel-alert").show();
                                $("#depthDay > .panel-alert").show();
                                $("#depthDay > .panel").hide();
                                $("#depthDay > .panel-alert").text("No well data available on this well.");
                                $("#WOB > .panel").hide();
                                $("#WOB > .panel-alert").text("No well data available on this well.");
                                $("#DHRPM > .panel").hide();
                                $("#DHRPM > .panel-alert").text("No well data available on this well.");
                                $("#RPM > .panel").hide();
                                $("#RPM > .panel-alert").text("No well data available on this well.");
                                $("#ROP > .panel").hide();
                                $("#ROP > .panel-alert").text("No well data available on this well.");
                                $("#Torsi > .panel").hide();
                                $("#Torsi > .panel-alert").text("No well data available on this well.");
                                $("#SPP > .panel").hide();
                                $("#SPP > .panel-alert").text("No well data available on this well.");
                                $("#SPM > .panel").hide();
                                $("#SPM > .panel-alert").text("No well data available on this well.");
                                $("#SPMPRESS > .panel").hide();
                                $("#SPMPRESS > .panel-alert").text("No well data available on this well.");
                                $("#flowrate > .panel").hide();
                                $("#flowrate > .panel-alert").text("No well data available on this well.");

                                $("#PTNPT > .panel").hide();
                                $("#PTNPT > .panel-alert").text("No well data available on this well.");


                                $(".trayek-panel").hide();

                                Swal.fire({
                                    icon: 'error',
                                    text: "No drilling record available on this well.",
                                })
                            }
                        })

                        $.get($.helper.resolveApi('~/core/DrillingBhaBit/getDetailMud/' + $recordId.val()), function myfunction(r) {
                            if (r.status.success && r.data.length > 0) {
                                //GetWellObjectUomMap(plan, date, r.data)
                                WOB(r.data);
                                DHRPM(r.data);
                                RPM(r.data);
                                ROP(r.data);
                                Torsi(r.data);
                                SPM(r.data);
                                SPMPRESS(r.data);
                                AVGSpp(r.data);
                                flowrateGPM(r.data);
                                //stackedBarPTNPT();
                                //Analysis();
                            }
                        })
                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                        // console.log(r);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }

        async function getDrillingFormationPlan(id) {
            try {
                const [uomPlan] = await getUomPlanActual(); // Ambil UOM Plan
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi('~/core/DrillFormationsPlan/' + id + '/detailbywellid'),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (r) {
                        var zindex = 1000;
                        var planned_td = plan_depth;
                        const _height = 750;
                        var lastY = 0;
                        var height = 0;
                        window.heightBackgroundGrey = 0;
                        var cc = 0;
                        $.each(r.data, function (index, value) {
                            var posTopRatio = parseFloat(r.data[index].depth) / parseFloat(plan_depth);
                            var posTop = 0;
                            if (index == 0)
                                height = posTopRatio * _height;
                            else {
                                height = (posTopRatio * _height);
                                posTop = (posTopRatio * _height);
                            }

                            var topLasty = cc == 0 ? 0 : lastY;

                            // Gantikan "ft" dengan UOM yang didapatkan dari getUomPlanActual()
                            var html = `<div class="plan-lithology" style ="top: ${topLasty}px; height: ${(parseFloat(value.depth) - topLasty)}px; 
                                      background-image: url(${$.helper.resolve("/lithology/") + value.stone_id + ".png"});
                                      background-size: 50px 30px;">
                                      <div class="fw-500 text-white text-white-shadow">${value.depth} ${uomPlan}</div>
                                  </div>`;
                            $("#section-plan-lithology").append(html);
                            window.heightBackgroundGrey = (parseFloat(value.depth)) + 2;

                            lastY = parseFloat(value.depth);
                            cc++;
                        });

                        getWellHoleAndCasingPlan();
                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }


        /*
        async function getDrillingFormationActual(id) {
            let result;
            try {
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi('~/core/DrillFormationsActual/' + id + '/detailbywellidNew'),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (r) {
                        var zindex = 1000;
                        //// console.log("Plan Depth TD: " + plan_depth);
                        // console.log(r);

                        //   var totalDepthTemporary = 0;

                        // for (var i = 0; i < r.data.length; i++) {
                        //    var depthFormation = r.data[i].depth;

                        // totalDepthTemporary += depthFormation;
                        //      }
                        const _height = 750;
                        var lastY = 0;
                        var height = 0;
                        var heightTotal = 0;
                        var cc = 0;
                        $.each(r.data, function (index, value) {
                            zindex -= 10;
                            var posTopRatio = (parseFloat(value.depth) / parseFloat(plan_depth)) * 0.889;

                            //var posTopRatio = parseFloat(r.data[index].depth) / parseFloat(plan_depth);
                            var posTop = 0;
                            if (index == 0)
                                height = posTopRatio * _height;
                            else {

                                height = (posTopRatio * _height);
                                posTop = (posTopRatio * _height);
                            }

                            if (cc == 0) {
                                var topLasty = 0;
                            } else {
                                var topLasty = lastY;
                            }

                            var heightBg = (parseFloat(height) - parseFloat(lastY));
                            // console.log("heightnya adalah " + height);
                            // console.log("value.detail-------------------------------");
                            // console.log(value.detail.length)
                            var html = `<div class="actual-lithology" style="top: ` + topLasty + `px; height: ` + (parseFloat(value.depth) - topLasty) + `px; ">`;
                            var row = '';
                            var rightR = 0;
                            for (var i = 0; i < value.detail.length; i++) {
                                var dtl = value.detail[i];
                                if (i == 0) {
                                    rightR = 0;
                                } else {
                                    rightR = rightR + value.detail[(i - 1)].percentage / 2;
                                }
                                var wdFF = dtl.percentage / 2;
                                row = row + `<div style="height: ` + (parseFloat(value.depth) - topLasty) + `px;background-image: url(` + $.helper.resolve("/lithology/") + dtl.stone_id + ".png" + `);background-size:50px 30px;right:` + rightR + `px;width:` + wdFF + `px"></div>`;
                            }

                            html = html + row + `<div class="fw-500 text-white text-white-shadow">` + value.depth + ` ft</div>
                                            </div>`;
                            $("#ddddsection-actual-lithology").append(html);
                            lastY = height;
                            lastY = parseFloat(value.depth);
                            cc++;

                            heightTotal = heightTotal + heightBg;
                        });

                        if (heightTotal > window.heightBackgroundGrey) {
                            window.heightBackgroundGrey = heightTotal;
                        }

                        scrolling();
                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                        // console.log(r);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }
        */

        async function getDrillingFormationActualNew(id) {
            let result;
            try {
                //const [uomAct] = await getUomPlanActual(); 
                await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi('~/core/DrillFormationsActual/' + id + '/detailbywellidNew'),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (r) {
                        // console.log("r--------------------------------------");
                        const _height = 750;
                        var lastY = 0;
                        var height = 0;
                        var heightTotal = 0;
                        var cc = 0;
                        var newArrData = r.data.sort((a, b) => (a.depth > b.depth) ? 1 : ((b.depth > a.depth) ? -1 : 0))

                        // console.log(r.data);
                        // console.log(newArrData);
                        $.each(newArrData, function (index, value) {
                            if (cc == 0) {
                                var topLasty = 0;
                            } else {
                                var topLasty = lastY;
                            }

                            var heightBg = (parseFloat(height) - parseFloat(lastY));
                            // console.log("value.detailObj ACTUAL LITHOLOGY-------------------------------");
                            // console.log(value.detailObj.length)
                            var html = `<div class="actual-lithology" style="top: ` + topLasty + `px; height: ` + (parseFloat(value.depth) - topLasty) + `px; ">`;
                            var row = '';
                            var rightR = 0;
                            for (var i = 0; i < value.detailObj.length; i++) {
                                var dtl = value.detailObj[i];
                                if (i == 0) {
                                    rightR = 0;
                                } else {
                                    rightR = rightR + value.detailObj[(i - 1)].percentage / 2;
                                }
                                var wdFF = dtl.percentage / 2;
                                row = row + `<div style="height: ` + (parseFloat(value.depth) - topLasty) + `px;background-image: url(` + $.helper.resolve("/lithology/") + dtl.stone_id + ".png" + `);background-size:50px 30px;right:` + rightR + `px;width:` + wdFF + `px"></div>`;
                            }

                            // Ditambahkan Untuk uom nya seperti di well detail js

                            html = html + row + `<div class="fw-500 text-white text-white-shadow">` + value.depth + ` </div>
                                            </div>`;
                            $("#section-actual-lithology").append(html);
                            lastY = height;
                            lastY = parseFloat(value.depth);
                            cc++;

                            heightTotal = heightTotal + heightBg;
                        });

                        if (heightTotal > window.heightBackgroundGrey) {
                            window.heightBackgroundGrey = heightTotal;
                        }

                        scrolling();
                    },
                    error: function (r) {
                        toastr.error(r.statusText);
                        // console.log(r);
                    }
                });
            } catch (error) {
                console.error(error);
            }
        }


        var scrolling = function () {
            var positionYdrilling = 0;
            var time_report = $('<span class="" styl data-pos="' + 0 + '"></span>');
            time_report.data({
            }).css({
                top: 0
            });
            time_report.appendTo('.timeline-track');

            var dataAllObjitem = [
                {
                    "top": 0,
                    "well_id": window.objDayDrill.data[0].well_id,
                    "drilling_date": "Drag Down",
                    "event": "",
                    "drilling_contractor": "",
                    "report_no": "",
                    "afe_cost": thousandSeparatorFromValueWithComma(cost_max),
                    "afe_cost_uom": "USD",
                    "daily_cost": "$ 0",
                    "daily_budget": "$ 0",
                    "daily_cost_uom": "USD",
                    'depth_pos': 0,
                    "current_depth_md": 0,
                }];
            dataAllObj.push(dataAllObjitem);

            var total_size_timelineCC = window.heightBackgroundGrey / window.objDayDrill.total_drilling_num;
            total_size_timeline = total_size_timelineCC;
            var cumCost = 0;
            for (var i = 1; i <= window.objDayDrill.total_drilling_num; i++) {
                var index = i - 1;
                positionYdrilling += total_size_timelineCC;
                cumCost += window.objDayDrill.data[index].daily_cost;
                //if (i == total_drilling_num)
                //    // console.log("Last Position Y : " + positionYdrilling);

                var drillingDate = moment(window.objDayDrill.data[i - 1].drilling_date).toDate();
                var cost = 0;
                var costlabel = "No Drilling";
                var depth = window.objDayDrill.data[index].current_depth_md;
                cost = window.objDayDrill.data[index].daily_cost;
                //costlabel = "$ " + thousandSeparatorFromValueWithComma(window.objDayDrill.data[index].cummulative_cost);
                costlabel = "$ " + thousandSeparatorFromValueWithComma(cumCost);


                var time_report = $('<span class="timeline-item" data-pos="' + i + '"></span>');
                time_report.data({
                    'date': moment(drillingDate).format('YYYY-MM-DD'),
                    'diameter': 0,
                    'phase_depth': 0,
                    'depth': 0,
                    'target_depth': plan_depth,
                    'cost': cost,
                    'budget': 0,
                    'depth_pos': (depth / plan_depth * 100),
                    'target_pos': plan_depth,//(target_depth / plan_depth * 100),
                    'cost_pos': (cost_max == 0 ? 0 : (cost / cost_max * 100)),
                    'budget_pos': (cost_max == 0 ? 0 : (budget / cost_max * 100)),
                    'depth_info': depth_info,
                    'cost_info': cost_info,

                    'bha': bha,
                    'type': type,
                    'rig': rig,
                    'phase': phase,
                    'status': status,

                    'weather': weather,
                    'wind_speed': wind_speed,
                    'barometer': barometer,
                    'wind_direction': wind_direction,
                    'wave': wave,
                    'comment': comment,
                    'temperature': temperature,
                    'wave_period': wave_period,
                    'visibility': visibility,
                    'wave_direction': wave_direction,
                    'cloud': cloud,
                    'height': height
                }).css({
                    top: positionYdrilling
                });
                //// console.log("total_size_timeline: " + total_size_timeline + " ::  plan: " + plan + " :: i:" + i + " :: total_drilling_num: " + total_drilling_num);
                time_report.appendTo('.timeline-track');

                var daily_budget = (window.objDayDrill.total_drilling_num == i ? cost_max : (cost_max / plan) * i);

                var dataAllObjitem = [
                    {
                        "top": (total_size_timeline) * i,
                        //"top": "100",
                        "well_id": window.objDayDrill.data[index].well_id,
                        "drilling_date": moment(drillingDate).format('YYYY-MM-DD'),
                        "event": window.objDayDrill.data[index].event,
                        "drilling_contractor": window.objDayDrill.data[index].contractor_name,
                        "report_no": window.objDayDrill.data[index].report_no,
                        "afe_cost": thousandSeparatorFromValueWithComma(cost_max),
                        "afe_cost_uom": "USD",
                        "daily_cost": costlabel,
                        "daily_budget": "$ " + thousandSeparatorFromValueWithComma(daily_budget),
                        "daily_cost_uom": "USD",
                        'depth_pos': (depth / plan_depth * 100),
                        "cummulative_cost": "1588379.67",
                        "cummulative_cost_uom": "USD",
                        "current_depth_md": thousandSeparatorFromValueWithComma(depth),
                    }];

                dataAllObj.push(dataAllObjitem);
                date_data = i;
            }

            //debugger;
            // $('.dirt-wrapper').css('height', (((date_data) * total_size_timeline)) + 'px');
            $('.dirt-wrapper').css('height', window.heightBackgroundGrey + 'px');
            $('.cost-wrapper').css('height', window.heightBackgroundGrey + 'px');
            $('.time-wrapper').css('height', window.heightBackgroundGrey + 'px');


            heightWell = (date_data) * total_size_timeline;
            //$('.dirt-wrapper').css('height', '800px');
            $('.report-body .detail-body .detail-wrapper > div').css('background-size', '100% ' + ((date_data * total_size_timeline)) + 'px');
            //var planned_td_format = (planned_td * 3.2808);

            if ($('.timeline-track .timeline-item').length) {
                var phase = $('<span class="left"><span class="text"></span><i class="arrow"></i></span>');
                var target = $('<span class="right line-track"></span>');
                var depth = $('<span><span class="text"></span><i class="arrow"></i></span>');
                var budget = $('<span class="left line-track hide-info"></span>');
                var cost = $('<span class="right"></span>');

                phase.appendTo('.drill-info');
                //budget.appendTo('.cost-wrapper .detail-info');
                //cost.appendTo('.cost-wrapper .detail-info');

                generateTimeline(0);
            }


            function generateTimeline(pos) {
                getUomPlanActual().then(function (uom) {
                    var uomPln = uom[0]
                    var report = $('.timeline-track .timeline-item[data-pos="' + pos + '"]');
                    //var text_phase = 'Ø ' + $.number(report.data('diameter')) + ' <i class="fa fa-arrow-right"></i> ' + $.number(report.data('phase_depth'));
                    var text_phase_bitsize = $.number(report.data('diameter'));

                    $('.drill-info > span.left').css({
                        'top': (pos * total_size_timeline) + 'px',
                        'background-color': '#afd285',
                        'color': '#252525'
                    }).children('.text').html(text_phase_bitsize + '" @ ' + dataAllObj[pos][0].current_depth_md + uomPln);
                    //    $('.drill-info > span.right').css('top', '100%').html(text_target); 

                    $('.drill-wrapper .drill-bit').css('height', (pos * total_size_timeline + 1) + 'px');

                    $('.cost-wrapper .detail-body .detail-wrapper > div').css('height', (pos * total_size_timeline + 1) + 'px');
                    $('.cost-wrapper .detail-info > span.right').css({ 'top': (pos * total_size_timeline) + 'px' }).html(dataAllObj[pos][0].daily_cost);

                    $('.cost-wrapper .detail-info > span.left').removeClass('hide-info').css({ 'top': (pos * total_size_timeline) + 'px', 'margin-top': '-12px' }).html(dataAllObj[pos][0].daily_budget);

                    $("#timeline_drilling_date").text(dataAllObj[pos][0].drilling_date);

                    //-- Label Diatas Gambar2
                    $('.general-info.bha').children('strong').html(report.data('bha'));
                    $('.general-info.type').children('strong').html(report.data('type'));
                    $('.general-info.rig').children('strong').html(report.data('rig'));
                    $('.general-info.phase').children('strong').html(report.data('phase'));
                    $('.general-info.status').children('strong').html(report.data('status'));
                });
            }

            var detailInformation = function (array) {
                console.log("data arraaaayy", array);
                var hole = 0;
                if (array.current_depth_md) {
                    for (var i = 0; i < holeAndCasing.length; i++) {
                        var actualDepth = holeAndCasing[i].actual_depth ? holeAndCasing[i].actual_depth.toString().replace(",", "") : 0;
                        if (array.current_depth_md < parseInt(actualDepth)) {
                            hole = holeAndCasing[i].hole_name;
                            break;
                        }
                    }
                    if (hole == 0 && parseInt(holeAndCasing[holeAndCasing.length - 1].actual_depth.toString().replace(",", "")) > 0) {
                        hole = holeAndCasing[holeAndCasing.length - 1].hole_name;
                    }
                }
                getUomPlanActual().then(function (uom) {
                    var uomPln = uom[0]
                    $('.drill-info > span.left').css({
                        'top': array.top + 'px',
                        'background-color': '#afd285',
                        'color': '#252525'
                    }).children('.text').html(hole + '" @ ' + array.current_depth_md + uomPln);
                });
                if (array.current_depth_md == 0) {
                    var ffdepth = "1";
                } else {
                    var ffdepth = array.current_depth_md.replace(",", "");
                }
                var depth = parseFloat(ffdepth);
                $('.drill-wrapper .drill-bit').css('height', (depth) + 'px');


                $('.cost-wrapper .detail-body .detail-wrapper > div').css('height', (depth) + 'px');
                $('.cost-wrapper .detail-info > span.right').css({
                    'top': (depth) + 'px',
                }).html(array.daily_cost);

                $('.cost-wrapper .detail-info > span.left').removeClass('hide-info').css({
                    'top': depth + 'px',
                    'margin-top': '-12px',
                    'background-color': 'rgb(255, 227, 131)'
                }).html(array.daily_budget);
                $('.cost-wrapper .detail-info > span.left:after').css({
                    'border-left': '12px solid #FFE383'
                });
                //$('.cost-wrapper .detail-info > span.left.line-track::after').css({
                //    'border-left': '12px solid #FFE383'
                //});

                $("#timeline_drilling_date").text(array.drilling_date);
            }

            $('.report-body .timeline-thumb').draggable({
                axis: "y",
                containment: ".report-body .timeline-track",
                start: function (event, ui) {
                    //
                },
                drag: function (event, ui) {
                    var top = ui.position.top;
                    var pos = Math.round(top / total_size_timeline);
                    //$.each(dataAllObj, function (index, value) {
                    //    if (top == Math.round(value[0].top)) {
                    //        // console.log("value",value)
                    //    //// console.log(top);
                    //    //// console.log(value[0].top);
                    //        detailInformation(value[0]);
                    //    }
                    //});
                    for (var i = 0; i < dataAllObj.length; i++) {
                        if (top < dataAllObj[i][0].top) {
                            detailInformation(dataAllObj[i][0]);
                            break;
                        }
                    }

                    if (pos > date_data) {
                        if (pos - date_data > 1) {
                            pos = date_data + 1;
                        } else {
                            pos = date_data;
                        }
                    }

                    $('.time-wrapper .detail-body .detail-wrapper > div').css('height', ui.position.top + 'px');
                },
                stop: function (event, ui) {
                    var top = ui.position.top;
                    var pos = Math.round(top / total_size_timeline);

                    if (pos > date_data) {
                        if (pos - date_data > 1) {
                            pos = date_data;
                            // pos = date_data + 4;
                        } else {
                            pos = date_data;
                        }
                    }

                    //$('.time-wrapper .detail-body .detail-wrapper > div').css('height', (pos * total_size_timeline + 1) + 'px');
                    //ui.helper.css('top', (pos * total_size_timeline + 1) + 'px');
                }
            });
        }


        var cekData = function () {
            $.get($.helper.resolveApi("~/Core/DrillingOperationActivity/getByWellIdNew?wellId=" + $recordId.val())), function (r) {
                if (r.status.success) {
                    console.log("ini data wekk hahahahaha", r.data)

                }
            }
        }

        var GetWellObjectUomMap = function (plan, date, rdata) {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $recordId.val()), function (r) {
                var uom = "";
                if (r.status.success) {
                    // console.log(r.data)
                    for (var i = 0; i < r.data.length; i++) {
                        var uomdata = r.data[i];
                        var object_name = uomdata.object_name;
                        if (object_name == "TVD Depth") {
                            uom = uomdata.uom_code;
                            break;
                        }
                    }
                }
                // console.log("ini uom ges",uom)
                depthPerDay(plan, date, rdata, uom);
            })
        }

        var depthPerDay = function (plan, spud_date, data, uom) {
            $("#depthDay > .panel").show();
            $("#depthDay > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var nextDate = moment(data[i].drilling_date);
                var arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);
            }

            var series = [];
            $.each(data, function (key, value) {
                series.push(value.current_depth_md == null ? 0 : value.current_depth_md);
            })

            $('#barChart').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Depth per Day (ft)'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} ' + uom + '</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Depth',
                    data: series
                }]
            });

        }

        var WOB = function (data) {
            $("#WOB > .panel").show();
            $("#WOB > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var isFound = false;
                var nextDate = moment(data[i].drilling_date);
                var arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);

            }
            var wobMax = [];
            var wobMin = [];
            $.each(data, function (key, value) {
                wobMax.push(value.mudlogging_wob_max == null ? 0 : value.mudlogging_wob_max);
                wobMin.push(value.mudlogging_wob_min == null ? 0 : value.mudlogging_wob_min);
            })

            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartWOB').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'WOB'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: setPlot,
                series: [{
                    name: 'WOB Max',
                    data: wobMax
                }, {
                    name: 'WOB Min',
                    data: wobMin
                }]
            });

        }
        var DHRPM = function (data) {
            $("#DHRPM > .panel").show();
            $("#DHRPM > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var isFound = false;
                var nextDate = moment(data[i].drilling_date);
                var arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);

            }
            var dhrpmMax = [];
            var dhrpmMin = [];
            $.each(data, function (key, value) {
                dhrpmMax.push(value.mudlogging_dhrpm_max == null ? 0 : value.mudlogging_dhrpm_max);
                dhrpmMin.push(value.mudlogging_dhrpm_min == null ? 0 : value.mudlogging_dhrpm_min);
            })

            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartDHRPM').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'DHRPM'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: setPlot,
                series: [{
                    name: 'DHRPM Max',
                    data: dhrpmMax
                }, {
                    name: 'DHRPM Min',
                    data: dhrpmMin
                }]
            });

        }
        var RPM = function (data) {
            $("#RPM > .panel").show();
            $("#RPM > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var isFound = false;
                var nextDate = moment(data[i].drilling_date);
                var arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);

            }
            var rpmMax = [];
            var rpmMin = [];
            $.each(data, function (key, value) {
                rpmMax.push(value.mudlogging_rpm_max == null ? 0 : value.mudlogging_rpm_max);
                rpmMin.push(value.mudlogging_rpm_min == null ? 0 : value.mudlogging_rpm_min);
            })

            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartRPM').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'RPM'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: setPlot,
                series: [{
                    name: 'RPM Max',
                    data: rpmMax
                }, {
                    name: 'RPM Min',
                    data: rpmMin
                }]
            });

        }
        var ROP = function (data) {
            $("#ROP > .panel").show();
            $("#ROP > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var isFound = false;
                var nextDate = moment(data[i].drilling_date);
                var arX = [moment(nextDate).format('YYYY-MM-DD')];

                arrayDate.push(arX);
            }
            var ropMax = [];
            var ropMin = [];
            $.each(data, function (key, value) {
                ropMax.push(value.mudlogging_avg_rop_max == null ? 0 : value.mudlogging_avg_rop_max);
                ropMin.push(value.mudlogging_avg_rop_min == null ? 0 : value.mudlogging_avg_rop_min);
            })
            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartROP').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'ROP'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                //plotOptions: setPlot,
                series: [{
                    name: 'ROP Max',
                    data: ropMax
                }, {
                    name: 'ROP Min',
                    data: ropMin
                }]
            });
        }
        var SPM = function (data) {
            $("#SPM > .panel").show();
            $("#SPM > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var isFound = false;
                var nextDate = moment(data[i].drilling_date);
                var arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);

            }
            var spmMax = [];
            var spmMin = [];
            $.each(data, function (key, value) {
                spmMax.push(value.mudlogging_spm_max == null ? 0 : value.mudlogging_spm_max);
                spmMin.push(value.mudlogging_spm_min == null ? 0 : value.mudlogging_spm_min);
            })

            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartSPM').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'SPM'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: setPlot,
                series: [{
                    name: 'SPM Max',
                    data: spmMax
                }, {
                    name: 'SPM Min',
                    data: spmMin
                }]
            });

        }
        var SPMPRESS = function (data) {
            $("#SPMPRESS > .panel").show();
            $("#SPMPRESS > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var isFound = false;
                var nextDate = moment(data[i].drilling_date);
                var arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);

            }
            var spmpressMax = [];
            var spmpressMin = [];
            $.each(data, function (key, value) {
                spmpressMax.push(value.mudlogging_spmpress_max == null ? 0 : value.mudlogging_spmpress_max);
                spmpressMin.push(value.mudlogging_spmpress_min == null ? 0 : value.mudlogging_spmpress_min);
            })

            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartSPMPRESS').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'SPMPRESS'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: setPlot,
                series: [{
                    name: 'SPMPRESS Max',
                    data: spmpressMax
                }, {
                    name: 'SPMPRESS Min',
                    data: spmpressMin
                }]
            });

        }
        var Torsi = function (data) {
            $("#Torsi > .panel").show();
            $("#Torsi > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var isFound = false;
                var nextDate = moment(data[i].drilling_date);
                var arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);
            }
            var TorsiMin = [];
            var TorsiMax = [];

            $.each(data, function (key, value) {
                TorsiMin.push(value.mudlogging_torque_min == null ? 0 : value.mudlogging_torque_min);
                TorsiMax.push(value.mudlogging_torque_max == null ? 0 : value.mudlogging_torque_max);
            });

            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartTorsi').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Torsi(klbsft)'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    stackLabels: {
                        enabled: true
                    }
                },
                plotOptions: setPlot,
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                series: [{
                    name: 'Torsi Max',
                    data: TorsiMax
                }, {
                    name: 'Torsi Min',
                    data: TorsiMin
                }]
            });
        }
        var AVGSpp = function (data) {
            $("#SPP > .panel").show();
            $("#SPP > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var nextDate = moment(data[i].drilling_date);
                var arX = arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);
            }
            var sppMax = [];
            var sppMin = [];
            $.each(data, function (key, value) {
                sppMax.push(value.mudlogging_spp_max == null ? 0 : value.mudlogging_spp_max);
                sppMin.push(value.mudlogging_spp_min == null ? 0 : value.mudlogging_spp_min);
            })
            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartSPP').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Average of SPP (psi)'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: setPlot,
                series: [{
                    name: 'SPP Max',
                    data: sppMax
                }, {
                    name: 'SPP Min',
                    data: sppMin
                }]
            });
        }
        var flowrateGPM = function (data) {
            $("#flowrate > .panel").show();
            $("#flowrate > .panel-alert").hide();

            var arrayDate = [];
            for (var i = 0; i < data.length; i++) {
                var isFound = false;
                var nextDate = moment(data[i].drilling_date);

                var arX = arX = [moment(nextDate).format('YYYY-MM-DD')];
                arrayDate.push(arX);
            }

            var flowrateMax = [];
            var flowrateMin = [];
            $.each(data, function (key, value) {
                flowrateMax.push(value.mudlogging_flowrate_max == null ? 0 : value.mudlogging_flowrate_max);
                flowrateMin.push(value.mudlogging_flowrate_min == null ? 0 : value.mudlogging_flowrate_min);
            })
            var setPlot = {
                column: {
                    pointWidth: 100 / data.length,
                    groupPadding: 0.5
                }
            };

            $('#barChartFlow').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Average Flow per Day (GPM)'
                },
                subtitle: {
                    text: ''
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 10
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: arrayDate,
                    crosshair: true,
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: setPlot,
                series: [{
                    name: 'SPP Max',
                    data: flowrateMax
                }, {
                    name: 'SPP Min',
                    data: flowrateMin
                }]
            });
        }
        var stackedBarPTNPT = function () {
            $("#PTNPT > .panel").show();
            $("#PTNPT > .panel-alert").hide();

            if ($recordId.val != '') {
                $.get($.helper.resolveApi('~/UTC/SingleWell/GetWellIadc/' + $recordId.val()), function (r) {
                    if (r.status.success) {
                        var aryPercentagePT = [];
                        var aryPercentageNPT = [];
                        var date = [];
                        if (r.data.length > 0) {
                            $.each(r.data, function (index, value) {
                                date.push(moment(value.drilling_date).format('DD-MM-YYYY'));
                                var percentage_pt = 0.0;
                                var percentage_npt = 0.0;
                                if (value.total_hours > 0) {
                                    if (value.total_hours_pt > 0) {
                                        //percentage_pt = (parseFloat(value.total_hours_pt) / (parseFloat(value.total_hours))) * 100;
                                        percentage_pt = value.total_hours_pt;
                                    }
                                    if (value.total_hours_npt > 0) {
                                        //percentage_npt = (parseFloat(value.total_hours_npt) / (parseFloat(value.total_hours))) * 100;
                                        percentage_npt = value.total_hours_npt;
                                    }
                                }

                                aryPercentagePT.push(percentage_pt);
                                aryPercentageNPT.push(percentage_npt);
                            });

                            $("#stackedBarPTNPT").highcharts({
                                chart: {
                                    type: 'column'
                                },
                                title: {
                                    text: 'PT & NPT (Hours)'
                                },
                                xAxis: {
                                    categories: date,
                                    title: {
                                        text: null
                                    },
                                    labels: {
                                        rotation: -45,
                                        style: {
                                            fontSize: '10px',
                                            fontFamily: 'Verdana, sans-serif'
                                        }
                                    }
                                },
                                credits: {
                                    enabled: false
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: ''
                                    },
                                    stackLabels: {
                                        enabled: false,
                                        style: {
                                            fontWeight: 'bold',
                                        }
                                    }
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                        '<td style="padding:0"><b>{point.y:.1f} hrs</b></td></tr>',
                                    footerFormat: '</table>',
                                    shared: true,
                                    useHTML: true
                                },
                                plotOptions: {
                                    column: {
                                        stacking: 'normal',
                                        dataLabels: {
                                            enabled: false
                                        }
                                    }
                                },
                                series: [{
                                    name: 'PT',
                                    data: aryPercentagePT

                                }, {
                                    name: 'NPT',
                                    data: aryPercentageNPT

                                }]
                            });
                        }
                    }
                });
            }
        }

        var colors = ["#1dc9b7", "#2196F3", "#886ab5", "#ffc241"];

        async function getPTNPT() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/SingleWell/chart/PTNPT?wellId=" + $recordId.val()),
                    type: 'GET'
                });
                return result;
            } catch (error) {
                console.error(error);
            }
        }
        Array.prototype.insert = function (index, item) {
            this.splice(index, 0, item);
        };

        async function getActualDepth() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/WellData/chart/actualDepth?wellId=" + $recordId.val()),
                    type: 'GET'
                });
                return result.data;
            } catch (error) {
                console.error(error);
            }
        }

        async function getActualCost() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/WellData/chart/actualCost?wellId=" + $recordId.val()),
                    type: 'GET'
                });

                return result.data;
            } catch (error) {
                console.error(error);
            }
        }

        async function getPlanningDepth() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/WellData/chart/tvd/planningDepth?wellId=" + $recordId.val()),
                    type: 'GET'
                });

                return result.data;
            } catch (error) {
                console.error(error);
            }
        }

        async function getPlanningCost() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/WellData/chart/tvd/planningCost?wellId=" + $recordId.val()),
                    type: 'GET'
                });

                return result.data;
            } catch (error) {
                console.error(error);
            }
        }

        async function getMulticasingPlan(dataID) {
            try {
                const response = await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi("~/Core/wellholeandcasing/" + dataID + "/getAllHoleCasingByWellId")
                });
                console.log("INI multiplan", response.data);
                return response.data;
            } catch (error) {
                toastr.error(error.statusText);
                console.error(error);
                return null;
            }
        }

        async function getMulticasingActual(dataID) {
            try {
                const response = await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi("~/Core/DrillingHoleAndCasing/" + dataID + "/ListMulticasing")
                });
                //console.log("INI multiaktual", response.data);
                return response.data;
            } catch (error) {
                toastr.error(error.statusText);
                console.error(error);
                return null;
            }
        }

        async function getWellHoleAndCasingPlan() {
            var templateHoleScript = $("#planHoleCasing-template").html();
            var templateHole = Handlebars.compile(templateHoleScript);

            try {
                const response = await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi("~/Core/WellHoleAndCasing/" + $recordId.val() + "/getAllByWellId")
                });

                const existingIds = [];

                for (var i = 0; i < response.data.length; i++) {
                    var id = response.data[i].id;

                    // Cek apakah id sudah ada di existingIds
                    if (!existingIds.some(entry => entry.id === id)) {
                        // Panggil fungsi jika id belum ada
                        const planData = await getMulticasingPlan(id);
                        const actualData = await getMulticasingActual(id);

                        if (planData && actualData) {
                            // Ambil atribut dari planData dan actualData
                            var casingNamePlan = planData.map(item => item.casing_name);
                            var casingValPlan = planData.map(item => item.casing_setting_depth);
                            var idMultiPlan = planData.map(item => item.parent_well_hole_casing_id);

                            var casingNameAct = actualData.map(item => item.casing_name);
                            var casingValAct = actualData.map(item => item.casing_val);
                            var idMultiAct = actualData.map(item => item.well_hole_and_casing_id);

                            // Tambahkan id ke existingIds hanya jika kedua data ada
                            existingIds.push({
                                id: id,
                                casingNamePlan: casingNamePlan,
                                casingValPlan: casingValPlan,
                                idMultiPlan: idMultiPlan,
                                casingNameAct: casingNameAct,
                                casingValAct: casingValAct,
                                idMultiAct: idMultiAct,
                                multiPlan: planData,
                                multiActual: actualData
                            });
                        }
                    }
                }

                console.log("hasil hoho", existingIds);

                if (response.data.length > 0) {
                    getWellHoleAndCasingPlanActual(response.data, existingIds);
                }
            } catch (error) {
                toastr.error(error.statusText);
                console.error(error);
            }
        }



        function getMaxData(idData, dataArray) {
            var dates = [];
            for (var i = 0; i < dataArray.length; i++) {

                var dataWell = dataArray[i];

                for (var j = 0; j < dataWell.data.length; j++) {

                    var dataw = dataWell.data[j];
                    if (dataw.well_hole_and_casing_id == idData) {
                        dates.push(dataw);
                    }
                }

            }

            dates.sort(function (a, b) {
                var date1 = (new Date(moment(a.created_on).format('YYYYMMDDHHmm')));
                var date2 = (new Date(moment(b.created_on).format('YYYYMMDDHHmm')));
                return date1 - date2;
            });
            var highestDate = dates[dates.length - 1];
            var lowestDate = dates[0];


            return highestDate;

        }

        var GetUomWellPlan = function () {
            return new Promise(function (resolve, reject) {
                $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $recordId.val()), function (r) {
                    var uomPlan = "";
                    if (r.status.success) {
                        for (var i = 0; i < r.data.length; i++) {
                            var uomdata = r.data[i];
                            var object_name = uomdata.object_name;
                            if (object_name == "Hole Depth") {
                                uomPlan = uomdata.uom_code;
                                break;
                            }
                        }
                    }
                    resolve(uomPlan);
                });
            });
        }

        var GetUomWellActual = function () {
            return new Promise(function (resolve, reject) {
                $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $recordId.val()), function (r) {
                    var uom = "";
                    if (r.status.success) {
                        for (var i = 0; i < r.data.length; i++) {
                            var uomdata = r.data[i];
                            var object_name = uomdata.object_name;
                            if (object_name == "Depth") {
                                uom = uomdata.uom_code;
                                break;
                            }
                        }
                    }
                    resolve(uom);
                });
            });
        }

        var getUomPlanActual = async function () {
            try {
                var uomPlan = await GetUomWellPlan();
                var uomActual = await GetUomWellActual();
                // console.log(uomPlan, uomActual);
                return [uomPlan, uomActual];
            } catch (error) {
                console.error("Error:", error);
                return [undefined, undefined];
            }
        }

        var getDrillHazzard = async function () {
            try {
                const response = await $.get($.helper.resolveApi('~/core/DrillingHazard/' + $recordId.val() + '/detailbywellid'));
                if (response.status.success) {
                    return response.data;
                } else {
                    console.error('Error fetching data:', response.status.message); // Menampilkan pesan error ke konsol
                    return [];
                }
            } catch (error) {
                console.error('Error fetching data:', error); // Menampilkan pesan error ke konsol
                return [];
            }
        }



        async function getWellHoleAndCasingPlanActual(wellHoleAndaCasingPlan, dataMulti) {
            var existingIds = dataMulti;
            holeAndCasing = wellHoleAndaCasingPlan;
            try {
                const r = await $.ajax({
                    type: "GET",
                    url: $.helper.resolveApi("~/Core/DrillingOperationActivity/getByWellIdNew?wellId=" + $recordId.val())
                });

                if (r.data.length > 0) {
                    console.log("ini banyak id", r.data);
                    const uom = await getUomPlanActual();
                    const dhazzardArray = await getDrillHazzard();

                    var planDhazzard = [];
                    for (var i = 0; i < dhazzardArray.length; i++) {
                        var depthDH = dhazzardArray[i].depth;
                        var descripDH = dhazzardArray[i].description;
                        if (depthDH !== undefined && descripDH !== undefined) {
                            planDhazzard.push({
                                depth: depthDH,
                                description: descripDH
                            });
                        }
                    }

                    console.log(planDhazzard);

                    var uomPln = uom[0];
                    var uomAct = uom[1];

                    var actualLatestHoleAndCasing = [];
                    for (var i = 0; i < wellHoleAndaCasingPlan.length; i++) {
                        var idHoleAndCasing = wellHoleAndaCasingPlan[i].id;
                        var resultData = getMaxData(idHoleAndCasing, r.data);
                        actualLatestHoleAndCasing.push(resultData);
                    }

                    var holeCasingDepth = wellHoleAndaCasingPlan[wellHoleAndaCasingPlan.length - 1].hole_depth;
                    var top = 0;
                    var width = 120;
                    var left = 0;
                    var heightNew = 0;
                    var heightHole = 0;
                    var tophole = 0;
                    var html = '';
                    var buttonAdded = false;

                    for (var i = 0; i < wellHoleAndaCasingPlan.length; i++) {
                        var holeDepth = i == 0 ? wellHoleAndaCasingPlan[0].hole_depth : wellHoleAndaCasingPlan[i].hole_depth - wellHoleAndaCasingPlan[i - 1].hole_depth;
                        var _height = (parseFloat(holeDepth) / parseFloat(plan_depth)) * 710;

                        if (i == 0) {
                            heightHole = parseFloat(wellHoleAndaCasingPlan[i].hole_depth);
                            tophole = 0;
                        } else {
                            heightHole = parseFloat(wellHoleAndaCasingPlan[i].hole_depth);
                            tophole = parseFloat(wellHoleAndaCasingPlan[i - 1].hole_depth);
                        }

                        heightNew = heightHole;

                        if (wellHoleAndaCasingPlan[i].actual_depth != null) {
                            if (actualLatestHoleAndCasing[i].depth > heightNew) {
                                heightNew = actualLatestHoleAndCasing[i].depth;
                            }
                        }

                        html += ` <div class="drill-hole" 
                    style="height: ` + (heightHole - parseFloat(tophole)) + `px; width: ` + width + `px; 
                    top: ` + tophole + `px;  position: absolute; 
                    left: ` + left + `px; background-color: #ffeb99;">`;
                        var marginLabel = parseFloat(wellHoleAndaCasingPlan[i].hole_depth) < 1000 ? -70 : -95;
                        html += `<span class="fw-700" style="position: absolute; top: ` + (heightHole - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; left: ` + marginLabel + `px; height: 18px;">` + wellHoleAndaCasingPlan[i].hole_depth + ` ` + uomPln + `</span>`;
                        if (wellHoleAndaCasingPlan[i].hole_depth) {
                            if (!buttonAdded) {
                                html += `<a class="fw-700 btn btn-default row-bhaModal" id="bhaModalBtn" style="position: absolute; top: -15px; transform: translateY(-100%); z-index: 1; right: ` + -115 + `px; height: 18px; display: flex; align-items: center; justify-content: center;"><span class="fal fa-eye"></span> Well BHA</a>`;
                                buttonAdded = true;
                            }
                            // Additional code for each iteration if needed
                        }
                        // Menambahkan casing name dan casing val untuk multi plan
                        if (existingIds[i]) {
                            const casingPlanHtml = existingIds[i].casingNamePlan.map((name, index) => {
                                return `Casing ${name}`;
                            }).join('<br />');

                            if (casingPlanHtml) {
                                const tooltipText = existingIds[i].casingNamePlan.map((name, index) => {
                                    return `${wellHoleAndaCasingPlan[i].hole_depth} ${uomPln} Hole ${wellHoleAndaCasingPlan[i].hole_name}" Casing ${name}`;
                                }).join('\n'); // Menggunakan newline untuk tooltip

                                html += `
                                <div style="border: 1px solid #000; position: absolute; top: ${heightHole - parseFloat(tophole) + 20}px; transform: translateY(-100%); z-index: 1; left: ${marginLabel - 30}px;" title="${tooltipText}">
                                    <span class="fw-700" style="height: 18px;">
                                        ${casingPlanHtml}
                                    </span>
                                </div>`;
                            }
                        }



                        // Menambahkan planDhazzard jika ada
                        if (planDhazzard[i] && planDhazzard[i].depth != null) {
                            html += `<span class="fw-700" style="position: absolute; top: ` + (planDhazzard[i].depth - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; left: ` + (marginLabel - 50) + `px; height: 18px; color: red;" id="tooltip` + i + `" title="` + planDhazzard[i].description + `">` + planDhazzard[i].depth + ` ` + uomPln + `</span>`;
                        }

                        if (wellHoleAndaCasingPlan[i].actual_depth != null) {
                            html += `   <img src="` + $.helper.resolve("/Content/img/segitiga.png") + `" class="" style="position: absolute; top: ` + (parseFloat(actualLatestHoleAndCasing[i].depth) - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; right: -25px; height: 18px;">`;
                            html += `   <span class="fw-700" style="position: absolute; top: ` + (parseFloat(actualLatestHoleAndCasing[i].depth) - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; right: ` + marginLabel + `px; height: 18px;">` + actualLatestHoleAndCasing[i].depth + ` ` + uomAct + `</span>`;

                            // Menambahkan casing name dan casing val untuk multi actual
                            if (existingIds[i]) {
                                const casingActualHtml = existingIds[i].casingNameAct.map((name, index) => {
                                    return `Casing ${name}`;
                                }).join('<br />');

                                if (casingActualHtml) {
                                    const tooltipText = existingIds[i].casingNameAct.map((name, index) => {
                                        return `${actualLatestHoleAndCasing[i].depth} ${uomPln} Hole ${actualLatestHoleAndCasing[i].hole_name}" Casing ${name}`;
                                    }).join('\n'); // Menggunakan pemisah newline untuk tooltip

                                    html += `
                                    <div style="border: 1px solid #000; position: absolute; top: ${parseFloat(actualLatestHoleAndCasing[i].depth) - parseFloat(tophole) + 20}px; transform: translateY(-100%); z-index: 1; right: ${marginLabel - 30}px;" title="${tooltipText}">
                                        <span class="fw-700" style="height: 18px;">
                                            ${casingActualHtml}
                                        </span>
                                    </div>`;
                                }
                            }




                        }
                        html += `<img src="` + $.helper.resolve("/Content/img/segitiga2.png") + `" class="" style="position: absolute; top: ` + (heightHole - parseFloat(tophole)) + `px; transform: translateY(-100%); z-index: 1; left: -25px; height: 18px;" id="tooltip` + i + `" title="` + wellHoleAndaCasingPlan[i].hole_depth + ` ` + uomPln + `, Hole ` + wellHoleAndaCasingPlan[i].hole_name + `in, Casing ` + wellHoleAndaCasingPlan[i].casing_name + `">`;

                        html += `</div>`;
                        $(".drill-body").append(html);
                        top = _height + top;
                        width -= 15;
                        left += 7.5;
                    }

                    if (heightNew > window.heightBackgroundGrey) {
                        window.heightBackgroundGrey = heightNew;
                    }

                    $(document).on('click', '.row-bhaModal', function () {
                        //console.log("hahahajajajajajajajajaajajaja")
                        modalBha();
                    });
                } else {
                    console.log("there is no data with the well id");
                }

                getDrillingFormationActualNew($recordId.val());
            } catch (error) {
                console.error(error);
            }
        }
        Handlebars.registerHelper('HoleCasingSize', function (hole_depth) {
            // console.log("[Plan Depth TD: " + plan_depth + "]");
            var as = (parseFloat(hole_depth) / parseFloat(plan_depth)) * 720;
            return as;
        });
        async function modalBha(recordID) {
            $bhaModal.modal('show');
        }

        $("#holeandcasinglookup").cmSelect2({
            url: $.helper.resolveApi("~/core/wellholeandcasing/lookup?wellId=" + $recordId.val()),
            //url: $.helper.resolveApi('~/core/wellholeandcasing/lookupByWell/' + $recordId.val()),
            result: {
                id: 'id',
                text: 'hole_name',
                casing_name: 'casing_name',
            },
            filters: function (params) {
                console.log("Filter params:", params); // Tambahkan log untuk memeriksa params
                return [{
                    field: "hole_name",
                    operator: "contains",
                    value: params.term || ''
                }];
            },
            options: {
                placeholder: "Select a Trayek",
                allowClear: true,
                templateResult: function (repo) {
                    console.log("Template result:", repo); // Tambahkan log untuk memeriksa repo
                    if (repo.loading) {
                        return repo.text; // Menampilkan "Searching…" saat proses loading
                    }
                    return "<b>Hole:</b> " + repo.text + " <b>Casing:</b> " + repo.casing_name;
                },
                dropdownParent: $bhaModal
            }
        });

        $("#holeandcasinglookup").on('select2:select', function (e) {
            var holeAndCasingId = e.params.data.id;
            console.log("Selected Hole and Casing ID:", holeAndCasingId);
            updateWellBhaOptions(holeAndCasingId);
            $('#well_bha_id').val(null).trigger('change');
        });

        var updateWellBhaOptions = function (holeAndCasingId) {
            $("#well_bha_id").cmSelect2({
                url: $.helper.resolveApi('~/core/wellbha/lookupByWell/' + $recordId.val()),
                //url: $.helper.resolveApi('~/core/wellbha/LookupByHoleAndCasing/' + $recordId.val() + '/' + holeAndCasingId),
                result: {
                    id: 'id',
                    text: 'bha_no'
                },
                filters: function (params) {
                    return [{
                        field: "bha_no",
                        operator: "contains",
                        value: params.term || ''
                    }];
                },
                options: {
                    placeholder: "Select a Bha",
                    allowClear: true,
                    maximumSelectionLength: 2,
                    dropdownParent: $bhaModal
                }
            });
        }

        $("#well_bha_id").on('select2:select', function (e) {
            var id = e.params.data.id;
            console.log("Selected Well BHA ID:", id);
            $("#BhaComponentContent").hide();
            wellBitLookupByWellBha(id);
            $('#well_bit_id').val(null).trigger('change');
        });

        var wellBitLookupByWellBha = function (wellBhaId) {
            $("#well_bit_id").cmSelect2({
                url: $.helper.resolveApi('~/utc/welldata/LookupByWellBha/' + wellBhaId),
                result: {
                    id: 'id',
                    text: 'bit_number'
                },
                filters: function (params) {
                    return [{
                        field: "bit_number",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "Select a Bit",
                    allowClear: true,
                    maximumSelectionLength: 2,
                    dropdownParent: $bhaModal
                }
            });
            $("#well_bit_id").on('select2:select', function (e) {
                console.log("Selected Bit ID:", e.params.data.id);
                dataBhaComponent(wellBhaId);
            });
        }

        var dataBhaComponent = function (recordID) {
            console.log("Record ID for BHA Component:", recordID);
            var templateScript = $("#bhaComponentWell-template").html();
            var template = Handlebars.compile(templateScript);

            $.get($.helper.resolveApi('~/core/wellbhacomponent/GetByWellBha/' + recordID), function (r) {
                console.log("BHA Component Data:", r);
                if (r.status.success) {
                    $("#BhaComponentContent").show();
                    $("#tableBhaComponent > .well-body").html(template(r));
                }
            }).fail(function (r) {
                console.error("Failed to fetch BHA Component data:", r);
            });
        }
        async function getIadcAnalysis() {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/SingleWell/GetIadcAnalysis/" + $recordId.val()),
                    type: 'GET'
                });
                //console.log("Ini data HC IADC : ", result)
                return result;
            } catch (error) {
                // console.log(error);
            }
        }

        async function getIadcAnalysisNpt(wellId, drillingHoleCasingId) {
            let result;
            try {
                result = await $.ajax({
                    url: $.helper.resolveApi("~/UTC/SingleWell/GetIadcAnalysisnpt/" + wellId + "/" + drillingHoleCasingId),
                    type: 'GET'
                });
                return result;
            } catch (error) {
                // console.log(error);
            }
        }
        var Analysis = function () {
            $(".trayek-panel").show();
            getIadcAnalysis().then((data) => {
                console.log("ini iadc", data)
                var templateScript = $("#trayek-template").html();
                var template = Handlebars.compile(templateScript);
                if (data.data.length > 0) {
                    $("#trayek-filter-content").show();
                    $("#trayek-content").html(template(data));
                    var htmlChart = `
                        <div class="col-md-6">
                            <select class="custom-select" id="chartType" name="chartFilter[]">
                                <option value="pie">Pie</option>
                                <option value="bar">Bar</option>
                            </select>
                        </div>
                    `;
                    $("#chart-filter").append(htmlChart);

                    $.each(data.data, function (key, value) {
                        console.log("Nilai value:", value); // Tampilkan seluruh objek value
                        console.log("Nilai hole_name:", value.hole_name)
                        var html = `
                            <div class="custom-control custom-checkbox custom-control-inline pt-2">
                                <input type="checkbox" class="custom-control-input" id="trayektype${value.id}" name="trayekFiltering[]" value="${value.id}">
                                <label class="custom-control-label" for="trayektype${value.id}">Trayek : ${value.hole_name}" </label>
                            </div>
                        `;
                        $("#list-trayek").append(html);
                        trayekChart(value.id, value.iadcAnalysis, value.iadcAnalysisNpt);
                    });
                } else {
                    $("#trayek-filter-content").hide();
                    $("#trayek-content").html(`<div class="col-md-12 pt-3 pb-2 text-center"><p class="h6">No trayek record on well available</p></div>`);
                }
            });

            $("#btnFilterTrayek").on('click', function () {
                trayekFiltering();
            });
        }

        var chartFilter = function () {

        }

        var trayekFiltering = function () {
            var totalChecked = $('input[name="trayekFiltering[]"]:checked').length;

            if (totalChecked > 0) {
                $(".trayek-record").hide();
                $.each($("input[name='trayekFiltering[]']:checked"), function () {
                    $("#trayek" + $(this).val()).show();
                });
            } else {
                toastr.error("Please choose at least 1 trayek to Filter.");
            }

            var selectedChart = $('#chartType').val();
            // Lakukan sesuatu berdasarkan chart yang dipilih
            if (selectedChart === 'pie') {
                // Lakukan sesuatu untuk chart pie
            } else if (selectedChart === 'bar') {
                // Lakukan sesuatu untuk chart bar
            }
        }


        var obj = {};

        var trayekChart = function (id, analysis, analysisnpt) {
            var arXAnalysis = [];
            var aryColor = [];
            var descData = [];
            var total = 0;

            // Ensure analysis is not null
            if (analysis && Array.isArray(analysis)) {
                for (var i = 0; i < analysis.length; i++) {
                    total = total + analysis[i].interval;
                }
            }

            var datanptCount = 0;
            if (analysisnpt && Array.isArray(analysisnpt)) {
                for (var i = 0; i < analysisnpt.length; i++) {
                    var datanpt = analysisnpt[i];
                    datanptCount += datanpt.hours;
                }
            }

            if (analysis && Array.isArray(analysis)) {
                for (var i = 0; i < analysis.length; i++) {
                    obj["NPT"] = 'rgb(255, 0, 0)';

                    if (!obj[analysis[i].iadc_code]) {
                        if (analysis[i].iadc_code != null) {
                            var color;
                            do {
                                color = '#' + Math.floor(Math.random() * 16777215).toString(16);
                            } while (color === '#ff0000'); // Avoid red color
                            obj[analysis[i].iadc_code] = color;
                        }
                    }

                    aryColor.push(analysis[i].iadc_code == null ? obj["NPT"] : obj[analysis[i].iadc_code]);

                    var time = analysis[i].type == "NPT" ? datanptCount : analysis[i].interval;

                    arXAnalysis.push({
                        well_hole_casing_id: id,
                        name: analysis[i].description,
                        y: analysis[i].total,
                        Time: time,
                        Type: analysis[i].type,
                        total: total,
                        percentageManual: (analysis[i].interval / total) * 100,
                        sliced: i == 0,
                        selected: i == 0
                    });

                    descData.push(analysis[i].description);
                }
            }

            // Highcharts setup...
            $('#trayek-' + id).highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    margin: [0, 0, 0, 0],
                    spacingTop: 0,
                    spacingBottom: 0,
                    spacingLeft: 0,
                    spacingRight: 0
                },
                title: {
                    text: 'Pie Chart'
                },
                tooltip: {
                    pointFormat: '<strong>{point.Type}</strong><br><strong>{point.Time:.1f} Hrs</strong>'
                },
                plotOptions: {
                    pie: {
                        size: '65%',
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            useHTML: true, // Mengizinkan penggunaan HTML dalam format
                            format: '<span style="font-size: 7px;"><b>{point.name}</b></span>:<span style="font-size: 7.5px;">{point.percentageManual:.2f} %</span>',
                            //format: '<b>{point.name}</b><br>{point.percentageManual:.2f} % ',
                            style: {
                                width: '345px'
                            }
                        },
                        colors: aryColor
                    }
                },
                series: [{
                    name: 'Brands',
                    stroke: '#94236a',
                    colorByPoint: true,
                    data: arXAnalysis,
                    point: {
                        events: {
                            click: function (event) {
                                if (this.Type == "NPT") {
                                    $("#btnWellHoleCasing-" + this.well_hole_casing_id).click();
                                }
                            }
                        }
                    }
                }]
            });
        }
        var OverallTimeDistribution = function () {
            $('#timeDistribution').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: ' Overall Time Distribution'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [
                        '01:00',
                        '02:00',
                        '03:00',
                        '04:00',
                        '05:00',
                        '06:00',
                        '07:00',
                        '08:00',
                        '09:00',
                        '10:00',
                        '11:00',
                        '12:00'
                    ],
                    crosshair: true,
                    title: {
                        text: 'Durasi (Hours)'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Trayek'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:70px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Bor Formasi (PT)',
                    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

                }, {
                    name: 'Bor Formasi (NPT)',
                    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

                }, {
                    name: 'Trip Run (PT)',
                    data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

                }, {
                    name: 'Trip Run (NTP)',
                    data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

                }]
            });
        }

        var WiseAnalysis = function () {
            wiseAnalysisChart();
        }

        var wiseAnalysisChart = function () {
            $('#BorFormasi4').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Browser market shares in January, 2018'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'Chrome',
                        y: 61.41,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Internet Explorer',
                        y: 11.84
                    }, {
                        name: 'Firefox',
                        y: 10.85
                    }, {
                        name: 'Edge',
                        y: 4.67
                    }, {
                        name: 'Safari',
                        y: 4.18
                    }, {
                        name: 'Sogou Explorer',
                        y: 1.64
                    }, {
                        name: 'Opera',
                        y: 1.6
                    }, {
                        name: 'QQ',
                        y: 1.2
                    }, {
                        name: 'Other',
                        y: 2.61
                    }]
                }]
            });
        }

        $btnFinalReport.on('click', function () {
            $.get($.helper.resolveApi('~/core/WellFinalReport/' + $recordId.val() + '/detailByWellId'), function (r) {
                if (r.status.success) {
                    window.location = $.helper.resolve("/UTC/APH/finalreport/details?id=" + r.data.id + "&wellId=" + $recordId.val());
                    //$.helper.form.fill($form, r.data);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        });

        //$(".btn-report-ddr-pertamina").click(function () {
        //    $.get($.helper.resolveApi('~/UTC/SingleWell/report/ddr-pertamina/' + $recordId.val() ), function (r) {
        //        if (r.status.success) {
        //            // console.log(r.data);
        //        }
        //        else {
        //            Swal.fire({
        //                icon: 'error',
        //                text: "Error",
        //            })
        //        }
        //    })
        //});

        var GetWellObjectUom = function () {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $recordId.val()), function (r) {
                if (r.status.success && r.data.length > 0) {
                    $("form#formDrilling :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("form#formOperationActivities :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#form-drillingHoleAndCasing :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("form#form-drillingBHA :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#form-drillingBhaComponent :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("form#form-drillingBIT :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#tableDrillingDeviation :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("#tableActualInformation :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("#tableWellDDR :input.uom-definition").each(function () {
                        console.log("check table well ddr " + $(this).data("objectname"));
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                    $("#form-drillingBhaBit :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });

                    $("#formDrillingWeather :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        // Fungsi untuk memanggil API dan meng-update data berdasarkan tanggal
        var drillOperation = function (date) {
            $.get($.helper.resolveApi('~/UTC/WellData/getByWellId/' + $recordId.val() + '/' + date + ''), function (r) {
                console.log(r);
                if (r.data) {
                    $.each(r.data, function (key, value) {
                        $("#type" + value.type).val(value.description);
                    })
                }
            });
        }

        // GENERAL DATA
        var wellDDR = function (date) {
            console.log('wellDDR called with date:', date, 'recordId:', $recordId.val());

            var Dates = new Date(date);
            var newdate = new Date(Dates);
            newdate.setDate(newdate.getDate() + 1);
            var dd = newdate.getDate();
            var mm = newdate.getMonth() + 1;
            if (mm < 10) mm = '0' + mm;
            var yyyy = newdate.getFullYear();
            var dateEnd = yyyy + '-' + mm + '-' + dd;
            var templateScript = $("#wellData-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();

            $.get($.helper.resolveApi('~/core/drilling/getListDrilling/' + $recordId.val() + '/' + date + '/' + dateEnd), function (r) {
                console.log('API response received:', r);

                if (r.status.success && r.data.length > 0) {
                    console.log('Data found:', r.data);

                    r.data[0].spud_date = moment(r.data[0].spud_date).format('YYYY-MM-DD');
                    r.data[0].created_on = moment(date).format('YYYY-MM-DD');

                    var rn = r.data[0].report_no + 1;
                    $('input[name="report_no"]').val("#" + rn);

                    $("#tableWellDDR > tbody").html(template(r.data[0]));
                    $.helper.form.fill($formDrillingOperation, r.data[0]);
                    $.helper.form.fill($formHSSE, r.data[0]);

                    console.log('HSSE Data:', r.data[0].hsse_tbop_press, r.data[0].hsse_tbop_func, r.data[0].hsse_dkick);

                    if (r.data[0].hsse_tbop_press === null) {
                        $('input[data-field="hsse_tbop_press"]').val('-');
                    }
                    if (r.data[0].hsse_tbop_func === null) {
                        $('input[data-field="hsse_tbop_func"]').val('-');
                    }
                    if (r.data[0].hsse_dkick === null) {
                        $('input[data-field="hsse_dkick"]').val('-');
                    }
                    if (r.data[0].hsse_dstrip === null) {
                        $('input[data-field="hsse_dstrip"]').val('-');
                    }
                    if (r.data[0].hsse_dfire === null) {
                        $('input[data-field="hsse_dfire"]').val('-');
                    }
                    if (r.data[0].hsse_dmis_pers === null) {
                        $('input[data-field="hsse_dmis_pers"]').val('-');
                    }
                    if (r.data[0].hsse_aband_rig === null) {
                        $('input[data-field="hsse_aband_rig"]').val('-');
                    }
                    if (r.data[0].hsse_dh2s === null) {
                        $('input[data-field="hsse_dh2s"]').val('-');
                    }

                    $.helper.form.fill($formMudloggingData, r.data[0]);
                    $.helper.form.fill($formDrillingWeather, r.data[0]);

                    console.log('Calling other functions with ID:', r.data[0].id);
                    drillingDeviationDt((r.data[0].id));
                    getDrillingMud((r.data[0].id), 'mud_1');
                    getDrillingMud((r.data[0].id), 'mud_2');
                    getDrillingMud((r.data[0].id), 'mud_3');
                    getDrillingTotalVolume((r.data[0].id));
                    getChemicalUse((r.data[0].id));
                    getActualLithology((r.data[0].well_id));
                    getDrillingMudPit((r.data[0].id));
                    getDrillingBulk((r.data[0].id), (r.data[0].drilling_date));
                    getDrillingTransportById((r.data[0].id));
                    getDrillingPersonelId((r.data[0].id));
                    drillingOperationActivity((r.data[0].id));
                    //getBhaBit((r.data[0].id));
                    GetWellObjectUom((r.data[0].wel_id));
                    getDrillingBhaBit((r.data[0].id));

                    $("#js_demo_accordion-4 > .content").show();
                    $("#js_demo_accordion-4 > .content-alert").hide();
                } else {
                    console.log('No data available or API call was unsuccessful.');
                    $("#js_demo_accordion-4 > .content-alert").show();
                    $("#js_demo_accordion-4 > .content").hide();
                    $("#js_demo_accordion-4 > .content-alert").text("No drilling data available on this date.");
                }
            }).fail(function (r) {
                console.log('Failed to load wellDDR data:', r.statusText, r.responseText);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // DEVIATION DATA
        var drillingDeviationDt = function (recordId) {
            var templateScript = $("#drillingDeviation-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/DrillingDeviation/' + recordId + '/detailByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingDeviation > tbody").html(template(r));
                }
            }).fail(function (r) {
                console.log('Failed to load drilling deviation data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // MUD DATA
        var getDrillingMud = function (recordId, mudType) {
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/utc/welldata/getmud?drillingId=' + recordId + '&dataMudType=' + mudType), function (r) {
                if (r.status.success) {
                    if (mudType === "mud_1") {
                        $.helper.form.fill($form_mud1, r.data);
                        $("#form-mud1 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                    if (mudType === "mud_2") {
                        $.helper.form.fill($form_mud2, r.data);
                        $("#form-mud2 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                    if (mudType === "mud_3") {
                        $.helper.form.fill($form_mud3, r.data);
                        $("#form-mud3 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                }
            }).fail(function (r) {
                console.log('Failed to load mud data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // MUD DATA
        var getDrillingMudPit = function (recordId) {
            var templateScript = $("#readDrillingPit-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/drillingmudpit/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingPit > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                console.log('Failed to load drilling mud pit data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // MUD DATA
        var getDrillingTotalVolume = function (recordId) {
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/drillingtotalvolume/' + recordId + '/detail'), function (r) {
                if (r.status.success) {
                    $.helper.form.fill($formTotalVolume, r.data);
                }
            }).fail(function (r) {
                console.log('Failed to load total volume data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // MUD DATA
        var getChemicalUse = function (recordId) {
            var templateScript = $("#readDrillingCUSED-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/drillingchemicalused/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingCused > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                console.log('Failed to load chemical use data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // Actual Lithology
        var getActualLithology = function (recordId) {
            var templateScript = $("#ActualLithology-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/DrillFormationsActual/' + recordId + '/detailbywellidNoRole'), function (r) {
                if (r.status.success) {
                    $("#tableActualInformation > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                console.log('Failed to load actual lithology data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // BULK DATA
        var getDrillingBulk = function (recordId, drilling_date) {
            var date = moment(drilling_date).format('YYYY-MM-DD');
            var templateScript = $("#drillingBulk-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/drillingbulk/' + recordId + '/' + $wellId.val() + '/' + date + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableBulk > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                console.log('Failed to load drilling bulk data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // TRANSPORT
        var getDrillingTransportById = function (recordId) {
            var templateScript = $("#readTransport-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/drillingtransport/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableTransport > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                console.log('Failed to load drilling transport data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // POB
        var getDrillingPersonelId = function (recordId) {
            var templateScript = $("#readDrillingPersonel-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/drillingpersonel/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableDrillingPersonel > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                console.log('Failed to load drilling personnel data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // HOLE & CASING
        var drillingOperationActivity = function (id) {
            var templateScript = $("#readHoleAndCasing-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/utc/WellData/getByDrillingId/' + id + '/' + $recordId.val()), function (r) {
                if (r.data) {
                    $("#formOperationActivities").html(template({ data: r.data }));

                    $.each(r.data, function (key, value) {
                        $("#cloneOperationActivies-" + value.id).on('click', function () {
                            cloneOperationActivities(value.id);
                        });
                    });

                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowOperation').remove();
                    });
                }
            }).fail(function (r) {
                console.log('Failed to load drilling operation activities:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // BHA BIT
        var getDrillingBhaBit = function (id) {
            var templateScript = $("#readBhaBit-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/DrillingBhaBit/getDetail/' + id), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#form-drillingBhaBit").html(template({ data: r.data }));

                        $("#form-drillingBhaBit .numeric-money").inputmask({
                            digits: 2,
                            greedy: true,
                            definitions: {
                                '*': {
                                    validator: "[0-9]"
                                }
                            },
                            rightAlign: false
                        });

                        $(".depth-out").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".depth-in").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".duration-bit").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });

                        $.each(r.data, function (key, value) {
                            wellBhaLookUp(value.well_bha_id);
                            wellBitLookUp(value.well_bit_id);
                        });
                    }
                }
            }).fail(function (r) {
                console.log('Failed to load BHA and Bit data:', r);
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        var wellBhaLookUp = function (e) {
            var $element = $(".wellBhaLookup");
            $element.cmSelect2({
                url: $.helper.resolveApi('~/core/wellbha/lookupByWell/' + $recordId.val()),
                result: {
                    id: 'id',
                    text: 'bha_no'
                },
                filters: function (params) {
                    return [{
                        field: "bha_no",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "",
                    allowClear: true,
                    templateResult: function (repo) {
                        if (repo.loading) {
                            return repo.text;
                        }
                        return "<span class='fw-700'>No: " + repo.text + "</span>";
                    },
                    templateSelection: function (data, container) {
                        if (data.text == "")
                            return "Choose BHA";
                        else
                            return "<span class='fw-700'>No: " + data.text + "</span>";
                    },
                }
            });
        }

        var wellBitLookUp = function (e) {
            var $element = $(".wellBitLookup");
            $element.cmSelect2({
                url: $.helper.resolveApi('~/core/wellbit/lookupByWell/' + $recordId.val()),
                result: {
                    id: 'id',
                    text: 'bit_number',
                    bit_size: 'bit_size'
                },
                filters: function (params) {
                    return [{
                        field: "bit_number",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "",
                    allowClear: true,
                    templateResult: function (repo) {
                        if (repo.loading) {
                            return repo.text;
                        }
                        return "<span class='fw-700'>No: " + repo.text + "</span>";
                    },
                    templateSelection: function (data, container) {
                        if (data.text == "")
                            return "Choose BIT";
                        else
                            return "<span class='fw-700'>No: " + data.text + "</span>";
                    },
                }
            });
        }

        Handlebars.registerHelper('setType', function (type) {
            if (type == 1) {
                return "PT";
            } else if (type == "2") {
                return "NPT";
            }
        });

        Handlebars.registerHelper('setIadcName', function (iadc_code, iadc_description, type) {
            if (type == 1) {
                return iadc_code + " - " + iadc_description + " (PT)";
            } else if (type == "2") {
                return iadc_code + " - " + iadc_description + " (NPT)";
            }
        });

        Handlebars.registerHelper('printHoleCasingType', function (type, id) {
            var html = "";

            html += "<select class=\"form-control hole-casing-tipe\" id='hole-casing-tipe-" + id + "' name=\"lot_fit[]\">";
            if (type == 1) {
                html += `<option value="1" selected="true">LOT</option>
                          <option value="2">FIT</option>`;
            } else if (type == 2) {
                html += `<option value="1">LOT</option>
                          <option value="2" selected="true">FIT</option>`;
            } else {
                html += `<option value="1">LOT</option>
                          <option value="2">FIT</option>`;
            }
            html += "</select>";
            html += "<div class=\"invalid-feedback\">Lot or Fit type cannot be empty</div>";
            return new Handlebars.SafeString(html);
        });

        // Fungsi untuk menginisialisasi date-picker
        var initDatePicker = function (date) {
            // Periksa apakah ada tanggal yang tersimpan di local storage
            var savedDate = localStorage.getItem('selectedDate');
            var defaultDate = savedDate ? savedDate : moment(date).format('YYYY-MM-DD');

            $('#datepicker-3').datepicker({
                autoclose: true,
                todayBtn: "linked",
                format: 'yyyy-mm-dd',
                orientation: "bottom left",
            });
            $('#datepicker-3').datepicker('setDate', defaultDate);

            $('#datepicker-3').datepicker().on('change', function (e) {
                var selectedDate = $(this).val();
                console.log('Date changed:', selectedDate);
                localStorage.setItem('selectedDate', selectedDate);  // Simpan tanggal ke local storage

                wellDDR(selectedDate);  // Call function to handle data for selected date
                drillOperation(selectedDate);  // Another function for operations

                // Perlebar modal ketika tanggal dipilih
                if (selectedDate) {
                    $('#operationsActivies .modal-dialog').addClass('wide-modal');
                } else {
                    $('#operationsActivies .modal-dialog').removeClass('wide-modal');
                }
            });
        };

        // Fungsi untuk memuat operasi kegiatan saat modal dibuka
        var loadOperationsActivities = function (wellid) {
            var templateScript = $("#readOperationsActivies-template").html();
            var template = Handlebars.compile(templateScript);
            $('.loading-detail').show();
            $.get($.helper.resolveApi('~/core/drilling/' + wellid + '/detailbyId'), function (r) {
                console.log(r.spud_date);
                if (r.status.success && r.data) {
                    $("#tableOperationActivities").html(template({ data: r.data }));

                    $.each(r.data, function (key, value) {
                        $("#cloneOperationActivies-" + value.id).on('click', function () {
                            cloneOperationActivities(value.id);
                        });
                    });

                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowOperation').remove();
                    });
                } else {
                    $("#tableOperationActivities").html("<div class='alert alert-danger'>No data available for the selected drilling operation.</div>");
                }
            }).fail(function (r) {
                console.error("Error loading operation activities:", r);
                $("#tableOperationActivities").html("<div class='alert alert-danger'>Failed to load data.</div>");
            }).always(function () {
                $('.loading-detail').hide();
            });
        };

        // Inisialisasi modal dan panggil loadOperationsActivities ketika modal ditampilkan
        $('#operationsActivies').on('shown.bs.modal', function (e) {
            var spud_date = $(e.relatedTarget).data('spud-date');
            console.log('percobaan', spud_date);

            var wellId = $(e.relatedTarget).data('well_id');
            if (wellId) {
                loadOperationsActivities(wellId);
            } else {
                console.log("No well ID provided for operations activities.");
            }
            initDatePicker(spud_date);  // Initialize date picker with spud_date when modal is opened
        });

        // Hapus tanggal saat halaman di-refresh
        $(window).on('beforeunload', function () {
            localStorage.removeItem('selectedDate');  // Hapus tanggal yang tersimpan
        });

        return {
            init: function () {
                stackedBarPTNPT();
                OverallTimeDistribution();
                WiseAnalysis();
                cekData();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();

    });
}(jQuery));
