﻿$(document).ready(function () {
    'use strict';
    var $btnPreview = $("#btnPreview");
    var $btnAnalyze = $("#btnAnalyze");
    var $btnAnalyzeDetails = $("#btnDetails");

    var $aphId = "";
    var $fieldId = "";
    var $iadcId = "";
    var data = new Object();

    var pageFunction = function () {
        var APHLookup = function () {
            $('#btn-business-unit-lookup').click(function () {
                var btn = $(this);
                btn.button('loading');
                $("#field-lookup").button('reset');
                $("#field_id").val('');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/businessunit/Lookup',
                    success: function (data) {
                        btn.button('reset');
                        //$("#field-lookup").button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "",
                            callback: function (e) {
                                console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {
                            $box.modal('hide');
                            $aphId = event.node.id;
                            $fieldId = "";
                            $("#field-section").removeClass("d-none");
                            $("#btn-business-unit-lookup").text("" + event.node.text);
                        });
                    }
                });
            });
        }


        $('#field-lookup').click(function () {
            FieldLookup();
        });
        var FieldLookup = function () {
            var element = $("#field-lookup");
            element.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/Asset/LookupFieldByBusinessUnit?id=' + $aphId,
                success: function (data) {
                    element.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "",
                        callback: function (e) {
                            console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o, event) {

                        if (event.node.parent == '#') {
                            alert('Please double click field only');
                            return;
                        } else {
                            $box.modal('hide');
                        }
                        $fieldId = event.node.id;
                        element.html(" " + event.node.text);

                        getWellByField($fieldId);
                    });
                }
            });
        }

        var getWellByField = function (fieldId) {
            $("#well-section").removeClass("d-none");
            var templateScript = $("#well-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/well/' + fieldId + '/detailByField'), function (r) {
                console.log(r);
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#listWell").html(template({ data: r.data }));
                    } else {
                        $("#field-lookup").button('reset');
                        toastr.error("No Well Available on this Field.");
                    }
                } else {
                    $("#groupWell").hide();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var iadcLookUp = function () {
            $('.iadc-lookup').click(function () {
                var element = $(this);
                element.button('loading');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/Iadc/Lookup?isNPTOnly=true',
                    success: function (data) {
                        element.button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                            callback: function (e) {
                                //console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {
                            $box.modal('hide');
                            if (event.node.original.type == "1") {
                                toastr.error("Please choose Keyword with NPT type");
                                $box.modal('show');
                            } else {
                                element.html(event.node.text);
                                $iadcId = event.node.id;
                                $box.modal('hide');
                            }
                        });
                    }
                });
            });
        }

        var validation = function () {
        
        }

        $btnPreview.on('click', function () {
            $("#preview-section").show();
            $("#analyze-section").hide();
            $("#analyzeDetail-section").hide();
            getAnalyze(true);
        });

        $btnAnalyze.on('click', function () {
            $("#preview-section").hide();
            $("#analyze-section").show();
            $("#analyzeDetail-section").hide();
            getAnalyze(false);
        });

        Handlebars.registerHelper('number', function (value) {
            return parseInt(value) + 1;
        });

        var getAnalyze = function (isPreview) {
            var isValid = true;
            if ($iadcId == "") {
                isValid = false;
                toastr.error("Please choose a Keyword First.");
            }
            if ($fieldId == "") {
                isValid = false;
                toastr.error("Please choose a Field First.");
            }
            if ($aphId == "") {
                isValid = false;
                toastr.error("Please choose an APH First.");
            }

            if (!isValid)
                return;

            var templateScript = $("#preview-template").html();
            var template = Handlebars.compile(templateScript);

            var aryWell = [];
            $.each($("input[name='wellId[]']:checked"), function () {
                aryWell.push($(this).val());
            });

            if (aryWell.length <= 0) {
                toastr.error("Please choose a Well First.");
                return;
            }

            data.well_id = new Object();
            data.well_id = aryWell;
            data.field_id = $fieldId;
            data.iadc_id = $iadcId;
            data.uom_select = ["ft"];

            $.post($.helper.resolveApi("~/UTC/LeasonLearned/GetAnalyze"), data, function (r) {
                if (r.status.success) {
                    if (isPreview) {
                        $("#tablePreview > tbody").html(template({ data: r.data }));
                    } else {
                        var actual_cost = [];
                        var actual_depth = [];
                        var interval_npt = [];
                        var well_name = [];

                        if (r.data.length > 0) {
                            $.each(r.data, function (index, value) {
                                actual_cost.push(value.actual_cost);
                                actual_depth.push(value.actual_depth);
                                interval_npt.push(value.interval);
                                well_name.push(value.well_name);


                                //actual_cost.push(1000);
                                //actual_depth.push(value.actual_depth * 1.2);
                                //interval_npt.push(30);
                                //well_name.push(value.well_name + "CC");
                            });
                        }


                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'analyze-preview',
                                //margin: [50, 10, 60, 65],
                                type: 'column',
                                //height: 400,
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: ''
                            },
                            yAxis: [
                                { // Primary yAxis
                                    floor: 0,
                                    ceiling: 10000,
                                    title: {
                                        text: 'Footage (ft)',
                                        style: {
                                            color: Highcharts.getOptions().colors[1]
                                        }
                                    },
                                    labels: {
                                        format: '{value}',
                                        style: {
                                            color: Highcharts.getOptions().colors[1]
                                        }
                                    },
                                    //reversed: true
                                },
                                { // Secondary yAxis
                                    floor: 0,
                                    title: {
                                        text: 'Cost (USD)',
                                        style: {
                                            color: Highcharts.getOptions().colors[0]
                                        }
                                    },
                                    labels: {
                                        format: '{value}',
                                        style: {
                                            color: Highcharts.getOptions().colors[0]
                                        }
                                    },
                                    //accessibility: {
                                    //    rangeDescription: 'Range: 1 to 99999'
                                    //},
                                    min: 0,
                                    opposite: true,
                                    //reversed: false
                                },

                                { // Secondary yAxis
                                    floor: 0,
                                    title: {
                                        text: 'NPT (Hours)',
                                        style: {
                                            color: Highcharts.getOptions().colors[0]
                                        }
                                    },
                                    labels: {
                                        format: '{value}',
                                        style: {
                                            color: Highcharts.getOptions().colors[0]
                                        }
                                    },
                                    //accessibility: {
                                    //    rangeDescription: 'Range: 1 to 99999'
                                    //},
                                    min: 0,
                                    opposite: true,
                                    //reversed: false
                                }
                            ],
                            xAxis: {
                                categories: well_name,
                                title: {
                                    text: null
                                }
                            },
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 10
                            },
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                }
                            },
                            //tooltip: {
                            //    pointFormat: '{series.name}: <b>{point.y} ' + data.uom_select + '</b><br/>',
                            //},
                            series:
                                [
                                    {
                                        name: 'Footage',
                                        marker: {
                                            symbol: 'diamond'
                                        },
                                        data: actual_depth
                                    },
                                    {
                                        name: 'Cost',
                                        type: 'line',
                                        marker: {
                                            symbol: 'diamond'
                                        },
                                        yAxis: 1,
                                        data: actual_cost
                                    },
                                    {
                                        name: 'NPT (Hours)',
                                        type: 'line',
                                        marker: {
                                            symbol: 'diamond'
                                        },
                                        yAxis: 1,
                                        data: interval_npt
                                    }
                                ],
                            credits: {
                                enabled: false
                            },

                        });
                    }
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        $btnAnalyzeDetails.on('click', function () {
            $("#analyzeDetail-section").show();
            getAnalyzeDetails();
        });

        var getAnalyzeDetails = function () {
            $.post($.helper.resolveApi("~/UTC/LeasonLearned/GetAnalyzeDetail"), data, function (r) {
                if (r.status.success) {
                    console.log("GetAnalyzeDetail");
                    console.log(r);
                    var well_name = [];

                    var aryTimeVDepth = [];
                    var aryTimeVCost = [];

                    if (r.data.length > 0) {
                        $.each(r.data, function (index, value) {
                            $.each(value.drilling, function (index, valuedetails) {
                                aryTimeVDepth.push([valuedetails.dfs, valuedetails.current_depth_md]);
                            });

                            $.each(value.time_versus_cost, function (index, valuedetails) {
                                aryTimeVCost.push([valuedetails.dfs, valuedetails.usage_percentage]);
                            });
                        });
                    }


                    var chart = new Highcharts.Chart({
                        chart: {
                            renderTo: 'analyzedetail-preview',
                            //margin: [50, 10, 60, 65],
                            type: 'column',
                            //height: 400,
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        yAxis: [
                            { // Primary yAxis
                                floor: 0,
                                ceiling: 10000,
                                title: {
                                    text: 'Kedalaman (MKU)',
                                    style: {
                                        color: Highcharts.getOptions().colors[1]
                                    }
                                },
                                labels: {
                                    format: '{value}',
                                    style: {
                                        color: Highcharts.getOptions().colors[1]
                                    }
                                },
                                reversed: true
                            },
                            { // Secondary yAxis
                                floor: 0,
                                title: {
                                    text: 'Realisasi Biaya',
                                    style: {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                                },
                                labels: {
                                    format: '{value}%',
                                    style: {
                                        color: Highcharts.getOptions().colors[0]
                                    }
                                },
                                //accessibility: {
                                //    rangeDescription: 'Range: 1 to 99999'
                                //},
                                min: 0,
                                opposite: true,
                                //reversed: false
                            },
                        ],
                        xAxis: {
                            categories: well_name,
                            title: {
                                text: null
                            }
                        },
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            x: 0,
                            y: 10
                        },
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                },
                            }
                        },
                        //tooltip: {
                        //    pointFormat: '{series.name}: <b>{point.y} ' + data.uom_select + '</b><br/>',
                        //},
                        series:
                            [
                                {
                                    name: 'Time Vs Depth',
                                    type: 'line',
                                    marker: {
                                        symbol: 'diamond'
                                    },
                                    data: aryTimeVDepth
                                },
                                {
                                    name: 'Cost',
                                    type: 'line',
                                    marker: {
                                        symbol: 'diamond'
                                    },
                                    yAxis: 1,
                                    data: aryTimeVCost
                                },
                            ],
                        credits: {
                            enabled: false
                        },

                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        };

        return {
            init: function () {
                APHLookup();
                iadcLookUp();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
});

