﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.UTC.Controllers
{
    public class DashboardController : BaseControllerUTC
    {

        // GET: UTC/Dashboard
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NPTandPTWellDetail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }
        public ActionResult DashboardWellInformation([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }

        public ActionResult DashboardFieldDetail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }

        public ActionResult DashboardFieldDetailV2([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }
        public ActionResult DashboardFieldDetailByField([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }
        
    }
}