﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.UTC.Controllers
{
    public class ReportController : BaseControllerUTC
    {
        // GET: UTC/Reports
        public ActionResult SingleWell()
        {
            return View();
        }
        public ActionResult SingleWellNPTDetail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }

        public ActionResult MultiWell()
        {
            return View();
        }
        public ActionResult Benchmark()
        {
            return View();
        }

        public ActionResult ServicePerformance()
        {
            return View();
        }

        public ActionResult ServicePerformanceAPH()
        {
            return View();
        }

        public ActionResult Export()
        {
            return View();
        }

        public ActionResult ImportNewWell()
        {
            return View();
        }

        public ActionResult LeasonLearned()
        {
            return View();
        }

        public ActionResult TrajectorySideView([Bind(Prefix = "id")] string wellId)
        {
            ViewBag.wellId = wellId;
            return PartialView();
        }
        public ActionResult TrajectoryTopView([Bind(Prefix = "id")] string wellId)
        {
            ViewBag.wellId = wellId;
            return PartialView();
        }

        public ActionResult WaktuPengeboran([Bind(Prefix = "id")] string wellId)
        {
            ViewBag.wellId = wellId;
            return PartialView();
        }

        public ActionResult OperationHours([Bind(Prefix = "id")] string wellId)
        {
            ViewBag.wellId = wellId;
            return PartialView();
        }

        public ActionResult AverageROP([Bind(Prefix = "id")] string wellId)
        {
            ViewBag.wellId = wellId;
            return PartialView();
        }

        public ActionResult StuckPipe([Bind(Prefix = "id")] string wellId)
        {
            ViewBag.wellId = wellId;
            return PartialView();
        }
    }
}