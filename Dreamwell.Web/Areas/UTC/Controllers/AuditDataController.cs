﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.UTC.Controllers
{
    public class AuditDataController : BaseControllerUTC
    {
        // GET: UTC/AuditData
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail([Bind(Prefix = "well_id")] string well_id, [Bind(Prefix = "drilling_date")] string drilling_date, [Bind(Prefix = "id")] string recordId)
        {
            ViewBag.wellId = well_id;
            ViewBag.recordId = recordId;
            ViewBag.drillingDate = drilling_date;
            return View();
        }
    }
}