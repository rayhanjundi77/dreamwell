﻿using CommonTools.wkhtml;
using Dreamwell.Infrastructure.WebApplication;
using Rotativa.MVC;

using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TuesPechkin;


namespace Dreamwell.Web.Areas.UTC.Controllers
{
    public class BitRecordController : BaseControllerUTC
    {
        private static IConverter converter =
            new ThreadSafeConverter(
                new RemotingToolset<PdfToolset>(
                    new WinAnyCPUEmbeddedDeployment(
                        new TempFolderDeployment())));
        private static IConverter imageConverter =
            new ThreadSafeConverter(
                new RemotingToolset<ImageToolset>(
                    new WinAnyCPUEmbeddedDeployment(
                        new TempFolderDeployment())));
        // GET: UTC/BitRecord
        public ActionResult Index([Bind(Prefix = "wellId")] string well_id)
        {
            ViewBag.wellId = well_id;
            return View();
        }

        
        [HttpGet]
        public FileResult Download([Bind(Prefix = "wellId")] string well_id)
        {
            byte[] result = new byte[1024];
            var doc = new HtmlToPdfDocument();
            doc.GlobalSettings = new GlobalSettings()
            {
                ProduceOutline = true,
                PaperSize = PaperKind.A4, // Implicit conversion to PechkinPaperSize
                Margins =
                {
                    //All = 0.49,
                    Top = 0.49,
                    Left = 0.49,
                    Right = 0.49,
                    Bottom = 0.49,
                    Unit = Unit.Inches
                },


            };

            doc.Objects.Add(new ObjectSettings
            {
                PageUrl = $"{Dreamwell.Infrastructure.Options.DreamwellSettings.WebServerUrl}/UTC/APH/BitRecord?wellId={well_id}&X_TOKEN={userSession.Token}",

                WebSettings = {
                    EnablePlugins=true,
                    DefaultEncoding = "utf-8",
                    LoadImages=true,
                    PrintBackground=true,
                    PrintMediaType=true,
                    EnableJavascript=true,
                    EnableIntelligentShrinking = true,
                    MinimumFontSize = 12,


                    },
                LoadSettings = {
                    DebugJavascript=false,
                    WindowStatus="PAGE_LOADED",
                    StopSlowScript=false,
                    //RenderDelay=10000
                   
                    },
                CountPages = true
                //FooterSettings ={
                //    LeftText= "Page [page] of [topage]",
                //    ContentSpacing=10,
                //    FontSize = 7,
                //    }

            });

            converter.Error += Convert_Error;
            converter.Warning += Convert_Warning;
            result = converter.Convert(doc);
            return File(result, "application/pdf");
        }

        public void Convert_Warning(object sender, WarningEventArgs e)
        {
            AppLogger.Debug("Waring TuesPechkin");
            AppLogger.Error(e.WarningMessage);
            AppLogger.Error(e);
        }

        public void Convert_Error(object sender, TuesPechkin.ErrorEventArgs e)
        {
            AppLogger.Debug("Error TuesPechkin");
            AppLogger.Error(e.ErrorMessage);
            AppLogger.Error(e);
        }
    }
}