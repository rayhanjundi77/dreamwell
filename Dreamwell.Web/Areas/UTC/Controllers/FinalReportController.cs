﻿using CommonTools.wkhtml;
using Dreamwell.Infrastructure.WebApplication;
using Rotativa.MVC;

using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TuesPechkin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Specialized;
using System.Configuration;
using PuppeteerSharp;
using static Google.Protobuf.WellKnownTypes.Field.Types;
//using DinkToPdf;
//using DinkToPdf.Contracts;

namespace Dreamwell.Web.Areas.UTC.Controllers
{
    public class FinalReportController : BaseControllerUTC
    {
        private static readonly IConverter _converter = new ThreadSafeConverter(
         new RemotingToolset<PdfToolset>(
             new WinAnyCPUEmbeddedDeployment(
                 new TempFolderDeployment())));

        // GET: UTC/FinalReport
        public ActionResult Index([Bind(Prefix = "wellId")] string well_id, [Bind(Prefix = "section")] string[] section)
        {
            ViewBag.wellId = well_id;
            ViewBag.arrSection = section;
            return View();
        }

        public ActionResult IndexPreview([Bind(Prefix = "wellId")] string well_id, [Bind(Prefix = "id")] string record_Id,[Bind(Prefix = "section")] string[] section)
        {
            ViewBag.wellId = well_id;
            ViewBag.recordId = record_Id;
            ViewBag.arrSection = section;
            return View();
        }

        public ActionResult NewIndex([Bind(Prefix = "wellId")] string well_id, [Bind(Prefix = "id")] string record_Id, [Bind(Prefix = "section")] string[] section)
        {
            ViewBag.wellId = well_id;
            ViewBag.recordId = record_Id;
            ViewBag.arrSection = section;
            return View();
        }
        public ActionResult Details([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "wellId")] string wellId)
        {
            ViewBag.recordId = recordId;
            ViewBag.wellId = wellId;
            return View();
        }

        public ActionResult Download(string wellId, string[] section)
        {
            // Setup the document's global settings
            var globalSettings = new GlobalSettings
            {
                ProduceOutline = true,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10, Bottom = 10, Left = 10, Right = 10, Unit = Unit.Millimeters }
            };
            string param = "";
            //string param = string.Join("", section.Select(s => $"&section={s}"));

            foreach (string ck in section)
            {
                param = param + "&section=" + ck;
            }
            var config = ConfigurationManager.GetSection("MarvelGroup/AxaSettings") as NameValueCollection;
            var baseUrl = config["WebServerUrl"];
            // Configure settings for the PDF content object
            var objectSettings = new ObjectSettings
            {   

                // PageUrl = $"{baseUrl}/UTC/APH/FinalReport/index?wellId={wellId}", // Replace with the actual URL
                PageUrl = $"{baseUrl}/UTC/APH/FinalReport/Index?wellId={wellId}&X_TOKEN={userSession.Token}" + param,
                WebSettings = { DefaultEncoding = "utf-8" }
            };

            // Create the HTML to PDF document with global and object settings
            var document = new HtmlToPdfDocument
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            // Convert HTML page to PDF
            byte[] pdfBuffer = _converter.Convert(document);

            // Return the generated PDF file to the client
            return File(pdfBuffer, "application/pdf", "FinalRaiaiaiaiaiaieport.pdf");
        }

        public ActionResult Stream(string wellId, string[] section)
        {
            // Setup the document's global settings
            var globalSettings = new GlobalSettings
            {
                ProduceOutline = true,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10, Bottom = 10, Left = 10, Right = 10, Unit = Unit.Millimeters }
            };

            string param = "";
            foreach (string ck in section)
            {
                param += "&section=" + ck;
            }

            var config = ConfigurationManager.GetSection("MarvelGroup/AxaSettings") as NameValueCollection;
            var baseUrl = config["WebServerUrl"];

            // Configure settings for the PDF content object
            var objectSettings = new ObjectSettings
            {
                PageUrl = $"{baseUrl}/UTC/APH/FinalReport/IndexPreview?wellId={wellId}&X_TOKEN={userSession.Token}" + param,
                WebSettings = { DefaultEncoding = "utf-8" }
            };

            // Create the HTML to PDF document with global and object settings
            var document = new HtmlToPdfDocument
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            // Convert HTML page to PDF
            byte[] pdfBuffer = _converter.Convert(document);

            // Create a MemoryStream from the PDF buffer
            var pdfStream = new MemoryStream(pdfBuffer);

            // Set the content disposition to inline to attempt to view in browser
            Response.AppendHeader("Content-Disposition", "inline; filename=FinalReport.pdf");

            // Return the generated PDF stream to the client
            return new FileStreamResult(pdfStream, "application/pdf");
        }
        
        public async Task<ActionResult> StreamNew(string wellId, string[] section)
        {
            // Setup the document's global settings
            var globalSettings = new GlobalSettings
            {
                ProduceOutline = true,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10, Bottom = 10, Left = 10, Right = 10, Unit = Unit.Millimeters }
            };

            string param = "";
            foreach (string ck in section)
            {
                param += "&section=" + ck;
            }

            var config = ConfigurationManager.GetSection("MarvelGroup/AxaSettings") as NameValueCollection;
            var baseUrl = config["WebServerUrl"];
            var pageUrl = $"{baseUrl}/UTC/APH/FinalReport/IndexPreview?wellId={wellId}&X_TOKEN={userSession.Token}" + param;

            // Setup Puppeteer to render the page
            var browserFetcher = new BrowserFetcher();
            await browserFetcher.DownloadAsync();  // Use default or specific revision if needed
            var browser = await Puppeteer.LaunchAsync(new LaunchOptions { Headless = true });
            var page = await browser.NewPageAsync();
            await page.GoToAsync(pageUrl, new NavigationOptions { Timeout = 180000  , WaitUntil = new[] { WaitUntilNavigation.Networkidle0 } });
            // Get the fully rendered HTML content
            var content = await EmbedImagesAsBase64(page);
            await browser.CloseAsync();

            // Save the content to a temporary HTML file
            var tempFilePath = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".html");
            System.IO.File.WriteAllText(tempFilePath, content);

            // Configure settings for the PDF content object
            var objectSettings = new ObjectSettings
            {
                PageUrl = "file:///" + tempFilePath.Replace("\\", "/"), // Use PageUrl to point to the temporary file
                WebSettings = new WebSettings { DefaultEncoding = "utf-8" }
            };


            // Create the HTML to PDF document with global and object settings
            var document = new HtmlToPdfDocument
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            // Convert HTML page to PDF
            byte[] pdfBuffer = _converter.Convert(document);

            // Delete the temporary file
            System.IO.File.Delete(tempFilePath);

            // Create a MemoryStream from the PDF buffer
            var pdfStream = new MemoryStream(pdfBuffer);

            // Set the content disposition to inline to attempt to view in browser
            Response.AppendHeader("Content-Disposition", "inline; filename=FinalReport.pdf");

            // Return the generated PDF stream to the client
            return new FileStreamResult(pdfStream, "application/pdf");
        }

        private async Task<string> EmbedImagesAsBase64(IPage page)
        {
            // Evaluate script in the context of the page to embed images as base64
            var content = await page.GetContentAsync();
            var script = @"
                Array.from(document.images).forEach(async (img) => {
                    if (!img.src.startsWith('data:')) {
                        const response = await fetch(img.src);
                        const blob = await response.blob();
                        const reader = new FileReader();
                        reader.onload = () => {
                            img.src = reader.result;
                        };
                        reader.readAsDataURL(blob);
                    }
                });
                document.documentElement.outerHTML;";
            var embeddedContent = await page.EvaluateExpressionAsync<string>(script);
            return embeddedContent;
        }
    }
}

