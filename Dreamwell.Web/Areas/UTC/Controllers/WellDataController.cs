﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.UTC.Controllers
{
    public class WellDataController : BaseControllerUTC
    {
        // GET: UTC/WellData
        public ActionResult Index([Bind(Prefix = "field")] string fieldId, [Bind(Prefix = "name")] string fieldName)
        {
            ViewBag.fieldId = fieldId;
            ViewBag.fieldName = fieldName;
            return View();
        }
        public ActionResult Detail([Bind(Prefix = "wellId")] string wellId)
        {
            ViewBag.wellId = wellId;
            return View();
        }

        public ActionResult WellProfile([Bind(Prefix = "wellId")] string wellId)
        {
            ViewBag.wellId = wellId;
            return View();
        }
    }
}