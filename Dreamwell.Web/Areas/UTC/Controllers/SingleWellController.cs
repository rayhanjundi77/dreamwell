﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.UTC.Controllers
{
    public class SingleWellController : BaseControllerUTC
    {
        // GET: UTC/WellData
        public ActionResult Index([Bind(Prefix = "field")] string fieldId, [Bind(Prefix = "name")] string fieldName)
        {
            ViewBag.fieldId = fieldId;
            ViewBag.fieldName = fieldName;
            return View();
        }
        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }

        public ActionResult Export([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            //ConfigurationManager.AppSettings["UploadLithologyPath"].ToString()

            return View();
        }
        
        public ActionResult ExportPertamina([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;

            return View();
        }
    }
}