﻿using System.Web.Mvc;

namespace Dreamwell.Web.Areas.UTC
{
    public class UTCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "UTC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {

            context.MapRoute(
                "UTC_default",
                "UTC/{unit}/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}