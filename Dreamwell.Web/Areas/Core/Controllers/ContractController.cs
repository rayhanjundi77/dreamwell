﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class ContractController : BaseController
    {
        // GET: core/contract
        public ActionResult Index()
        {
            return View();
        }
    
        public ActionResult Detail([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "afeId")] string afeId)
        {
            ViewBag.recordId = recordId;
            ViewBag.afeId = afeId;
            return PartialView();
        }
        
        public ActionResult ManageMaterial([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "afeId")] string afeId, [Bind(Prefix = "isNewPurchase")] bool? isNewPurchase)
        {
            ViewBag.recordId = recordId;
            ViewBag.afeId = afeId;
            ViewBag.isNewPurchase = isNewPurchase;
            return PartialView();
        }
    }

}