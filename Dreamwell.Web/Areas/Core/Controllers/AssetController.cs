﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class AssetController : BaseController
    {
        // GET: Core/Asset
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LookupField()
        {
            return PartialView();
        }
        public ActionResult LookupFieldByBusinessUnit([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }
        
    }
}