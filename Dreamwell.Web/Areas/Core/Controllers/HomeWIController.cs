﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class HomeWIController : BaseController
    {
        // GET: Core/HomeWI

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DetailWellIntervesions([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }
    }
}