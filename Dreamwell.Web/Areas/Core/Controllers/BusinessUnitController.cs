﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class BusinessUnitController : BaseController
    {
        // GET: Core/BusinessUnit
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Lookup()
        {
            return PartialView();
        }

        public ActionResult UpdateLeader([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "defaultteam")] string defaultTeam)
        {
            ViewBag.recordId = recordId;
            ViewBag.defaultTeam = defaultTeam;
            return PartialView();
        }
    }
}