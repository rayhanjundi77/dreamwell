﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class AfeController : BaseController
    {
        // GET: Core/Vendor
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }

        public ActionResult ManageDetail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }

        public ActionResult ManagePlan([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "afeId")] string afeId)
        {
            ViewBag.recordId = recordId;
            ViewBag.afeId = afeId;
            return PartialView();
        }

        public ActionResult ShowContract()
        {
            return PartialView();
        }

        //public ActionResult Detail([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "afeId")] string afeId)
        //{
        //    ViewBag.recordId = recordId;
        //    ViewBag.afeId = afeId;
        //    return PartialView();
        //}


        public ActionResult ManageBudget([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "afeid")] string afeId, [Bind(Prefix = "afelineid")] string afeLineId)
        {
            ViewBag.recordId = recordId;
            ViewBag.afeId = afeId;
            ViewBag.afeLineId = afeLineId;
            return PartialView();
        }

        public ActionResult Approval()
        {
            return View();
        }

        public ActionResult Approved()
        {
            return View();
        }

        public ActionResult ApprovalDetail([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "status")] bool? status)
        {
            ViewBag.recordId = recordId;
            ViewBag.status = status;
            return View();
        }

    }

}