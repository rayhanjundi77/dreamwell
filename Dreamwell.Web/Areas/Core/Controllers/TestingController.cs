﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class TestingController : Controller
    {
        // GET: Core/Testing
        public ActionResult Index()
        {
            return View();
        }
    }
}