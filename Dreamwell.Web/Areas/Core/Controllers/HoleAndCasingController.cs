﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class HoleAndCasingController : BaseController
    {
        // GET: Core/Vendor
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }

        public ActionResult CasingCatalog()
        {
            return View();
        }

        public ActionResult CatalogDetail([Bind(Prefix = "id")] string recordId) 
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }

        public ActionResult CasingConnection()
        {
            return View();
        }

        public ActionResult ConnectionDetail([Bind(Prefix = "id")] string recordId) 
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }
    }

}