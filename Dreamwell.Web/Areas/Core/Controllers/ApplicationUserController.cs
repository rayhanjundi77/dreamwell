﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class ApplicationUserController : BaseController
    {
        // GET: Core/ApplicationUser
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }

        public ActionResult ChangePassword([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }

        public ActionResult UpdatePassword([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }
    }
}