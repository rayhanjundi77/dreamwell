﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class TeamController : BaseController
    {
        // GET: Core/Team
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }

        public ActionResult Member([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "businessUnitId")] string businessUnitId)
        {
            ViewBag.recordId = recordId;
            ViewBag.businessUnitId = businessUnitId;
            return View();
        }

        public ActionResult Role([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }
    }
}