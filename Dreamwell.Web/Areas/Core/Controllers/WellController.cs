﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class WellController : BaseController
    {
        // GET: Core/Well
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }
        public ActionResult HoleAndCasing([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "wellId")]string wellId)
        {
            ViewBag.recordId = recordId;
            ViewBag.wellId = wellId;
            return PartialView();
        }
        public ActionResult CasingNew([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "wellId")] string wellId)
        {
            ViewBag.recordId = recordId;
            ViewBag.wellId = wellId;
            return PartialView();
        }

        public ActionResult MultiCasing([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "wellId")] string wellId)
        {
            ViewBag.recordId = recordId;
            ViewBag.wellId = wellId;
            return PartialView();
        }
        public ActionResult HoleAndCasingNew([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "wellId")] string wellId)
        {
            ViewBag.recordId = recordId;
            ViewBag.wellId = wellId;
            return PartialView();
        }
        public ActionResult New()
        {
            return PartialView();
        }

        public ActionResult UnitOfMeasurementConfiguration([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }
    }
}