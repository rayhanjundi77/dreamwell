﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class IadcController : BaseController
    {
        // GET: Core/Vendor
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Lookup([Bind(Prefix = "isNPTOnly")] bool? isNPTOnly)
        {
            ViewBag.isNPTOnly = isNPTOnly;
            return PartialView();
        }
        public ActionResult History()
        {
            return View();
        }
        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }
    }

}