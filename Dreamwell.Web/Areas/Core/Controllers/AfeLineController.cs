﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class AfeLineController : BaseController
    {
        // GET: Core/Vendor
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Lookup()
        {
            return PartialView();
        }
        public ActionResult Detail([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "parentId")] string parentId, [Bind(Prefix = "parentName")] string parentName)
        {
            ViewBag.recordId = recordId;
            ViewBag.parentId = parentId;
            ViewBag.parentName = parentName;
            return PartialView();
        }
    }

}