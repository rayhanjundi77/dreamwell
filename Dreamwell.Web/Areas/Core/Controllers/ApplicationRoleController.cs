﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class ApplicationRoleController : BaseController
    {
        // GET: Core/ApplicationRole
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RoleAccess([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }

        public ActionResult Member([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }
        public ActionResult Menu([Bind(Prefix = "roleId")] string roleId, [Bind(Prefix = "roleName")] string roleName)
        {
            ViewBag.roleId = roleId;
            ViewBag.roleName = roleName;
            return View();
        }

    }
}