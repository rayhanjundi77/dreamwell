﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class DailyCostController : BaseController
    {
        // GET: Core/Vendor
        
        public ActionResult DailyCost([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "materialId")] string materialId, [Bind(Prefix = "afeId")] string afeId, [Bind(Prefix = "drillingId")] string drillingId)
        {
            //ViewBag.drillingId = recordId;
            //ViewBag.afeId = afeId;
            ViewBag.materialId = materialId;
            ViewBag.afeId = afeId;
            ViewBag.drillingId = drillingId;
            return PartialView();
        }
    }

}