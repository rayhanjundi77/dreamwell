﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class ApplicationMenuController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }
        public ActionResult Setup()
        {
            return View();
        }
        public ActionResult ManageDetail([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "categoryId")] string categoryId, [Bind(Prefix = "moduleId")] string applicationModuleId, [Bind(Prefix = "parentId")] string parentMenuId)
        {
            ViewBag.recordId = recordId;
            ViewBag.categoryId = categoryId;
            ViewBag.applicationModuleId = applicationModuleId;
            ViewBag.parentMenuId = parentMenuId;
            return PartialView();
        }
    }
}