﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class ExcelTemplateController : BaseController
    {
        // GET: Core/ExcelTemplate
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Editor([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }

        public ActionResult TableRelated([Bind(Prefix = "excel_table_map_id")] string excelTableMapId)
        {
            ViewBag.excelTableMapId = excelTableMapId;
            return PartialView();
        }

        public ActionResult ColumnMapper([Bind(Prefix = "excel_column_map_id")] string excelColumnMapId)
        {
            ViewBag.excelColumnMapId = excelColumnMapId;
            return PartialView();
        }

    }
}