﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class UomController : BaseController
    {
        // GET: Core/Uom
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }
    }
}