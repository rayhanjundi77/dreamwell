﻿using Dreamwell.Infrastructure.WebApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class ContractDetailController : BaseController
    {
        // GET: core/contract
        public ActionResult Index([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return View();
        }
      
        public ActionResult Detail([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "contractId")] string contractId)
        {
            ViewBag.recordId = recordId;
            ViewBag.contractId = contractId;
            return PartialView();
        }
    }

}