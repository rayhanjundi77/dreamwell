﻿using CommonTools.wkhtml;
using Dreamwell.Infrastructure.WebApplication;
using Rotativa.MVC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class DrillingController : BaseController
    {
        // GET: Core/Vendor
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail([Bind(Prefix = "well_id")] string well_id, [Bind(Prefix = "drilling_date")] string drilling_date, [Bind(Prefix = "id")] string recordId)
        {
            ViewBag.wellId = well_id;
            ViewBag.recordId = recordId;
            ViewBag.drillingDate = drilling_date;
            return View();
        }
     
        
        public ActionResult ApprovalDetail([Bind(Prefix = "well_id")] string well_id, [Bind(Prefix = "drilling_date")] string drilling_date, [Bind(Prefix = "id")] string recordId)
        {
            ViewBag.wellId = well_id;   
            ViewBag.recordId = recordId;
            ViewBag.drillingDate = drilling_date;
            return View();
        }

        public ActionResult ExportToPdf()
        {
            return new Rotativa.MVC.UrlAsPdf("http://localhost:58372/core/drilling/detail?id=53d1871f-fb22-4e27-a763-c223369fd21c&well_id=4d9c7b72-40af-425f-9ab2-266cd009f6ac&X_TOKEN=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InN5c19hZG1pbiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvdXNlcmRhdGEiOiJ7XCJUb2tlblwiOm51bGwsXCJBcHBVc2VySWRcIjpcImY3ZWIwZGJlLTYzOWYtZjZlMS03MjMyLWU2OTc4ZWY1MWQwYlwiLFwiQXBwVXNlcm5hbWVcIjpcInN5c19hZG1pblwiLFwiRmlyc3RuYW1lXCI6XCJTeXN0ZW1cIixcIkxhc3RuYW1lXCI6XCJBZG1pbmlzdHJhdG9yXCIsXCJFbWFpbFwiOlwiYWRtaW4ucGVydGFtaW5hQHBlcnRhbWluYS5jb21cIixcIlN5c0FkbWluXCI6dHJ1ZSxcIk9yZ2FuaXphdGlvbklkXCI6XCI5NzcwOWM1Yi1iNzQyLTY4NjktZWM4MS1kYzljMTQxMmVmYjRcIixcIk9yZ2FuaXphdGlvbk5hbWVcIjpcIlBFUlRBTUlOQVwiLFwiT3JnYW5pemF0aW9uRG9tYWluXCI6bnVsbCxcIkdlbmRlclwiOnRydWUsXCJUZWFtc1wiOm51bGwsXCJGdWxsbmFtZVwiOlwiQURNSU5JU1RSQVRPUixTeXN0ZW1cIixcIlJvbGVzXCI6bnVsbH0iLCJuYmYiOjE1ODQyMjA2ODYsImV4cCI6MTU4NDMwNzA4NiwiaWF0IjoxNTg0MjIwNjg2fQ.XrE-YnWBsdLoHtagbr476zgxI0BlGZnfi6mF-oWXX_I")
            {
                RotativaOptions = new Rotativa.Core.DriverOptions()
                {
                    CustomSwitches = "--debug-javascript --no-stop-slow-scripts --javascript-delay 50000 "
                }
            };

            //var ret = new ViewAsPdf("Detail", new
            //{
            //    id = "53d1871f-fb22-4e27-a763-c223369fd21c",
            //    well_id = "4d9c7b72-40af-425f-9ab2-266cd009f6ac",
            //    X_TOKEN = userSession.Token
            //})
            //{
            //    RotativaOptions = new Rotativa.Core.DriverOptions()
            //    {
            //        CustomSwitches = "--debug-javascript --no-stop-slow-scripts --javascript-delay 50000 ",
            //        IsJavaScriptDisabled = false
            //    },
            //    FileName = "test.pdf"
            //};
            //return ret;



            //return new ActionAsPdf("Detail", new
            //{
            //    id = "53d1871f-fb22-4e27-a763-c223369fd21c",
            //    well_id = "4d9c7b72-40af-425f-9ab2-266cd009f6ac",
            //    X_TOKEN = userSession.Token
            //})
            //{
            //    RotativaOptions = new Rotativa.Core.DriverOptions()
            //    {
            //        CustomSwitches = "--enable-javascript --debug-javascript --no-stop-slow-scripts --javascript-delay 20000 "
            //        //IsJavaScriptDisabled = false
            //    },
            //    FileName = "test.pdf"
            //};



            //MemoryStream memory = new MemoryStream();
            ////PdfDocument document = new PdfDocument() { Url = string.Format(@"{0}\",Dreamwell.Infrastructure.Options.DreamwellSettings.WebServerUrl) };
            //PdfDocument document = new PdfDocument() { Url = "http://localhost:58372/core/drilling/detail?id=53d1871f-fb22-4e27-a763-c223369fd21c&well_id=4d9c7b72-40af-425f-9ab2-266cd009f6ac" };
            //PdfOutput output = new PdfOutput() { OutputStream = memory };
            //PdfConvert.ConvertHtmlToPdf(document, output);
            //memory.Position = 0;

            //PdfConvert.ConvertHtmlToPdf(document, new PdfOutput
            //{
            //    OutputFilePath = "FinalReport.pdf"
            //});
            //return File(memory, "application/pdf", Server.UrlEncode("FinalReport.pdf"));
        }

        public ActionResult DailyCost([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "afeId")] string afeId)
        {
            ViewBag.recordId = recordId;
            ViewBag.afeId = afeId;
            return PartialView();
        }
        public ActionResult ManageUsage([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "afeId")] string afeId, [Bind(Prefix = "material_id")] string materialId)
        {
            ViewBag.recordId = recordId;
            ViewBag.afeId = afeId;
            ViewBag.materialId = materialId;
            return PartialView();
        }

        public ActionResult SetupBhaBit([Bind(Prefix = "well_id")] string wellId, [Bind(Prefix = "drilling_id")] string drillingId)
        {
            ViewBag.wellId = wellId;
            ViewBag.drillingId = drillingId;
            return PartialView();
        }

        public ActionResult CasingAddNew([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "wellId")] string wellId)
        {
            ViewBag.recordId = recordId;
            ViewBag.wellId = wellId;
            return PartialView();
        }

        public ActionResult listMulticasingDrill([Bind(Prefix = "id")] string recordId, [Bind(Prefix = "wellId")] string wellId)
        {
            ViewBag.recordId = recordId;
            ViewBag.wellId = wellId;
            return PartialView();
        }

        public ActionResult SetupLihtology([Bind(Prefix = "well_id")] string wellId, [Bind(Prefix = "drilling_id")] string drillingId, [Bind(Prefix = "drill_formation_actual_id")] string drill_formation_actual_id, [Bind(Prefix = "depth")] string depth)
        {
            ViewBag.wellId = wellId;
            ViewBag.drillingId = drillingId;
            ViewBag.drill_formation_actual_id = drill_formation_actual_id;
            ViewBag.depth = depth;
            return PartialView();
        }

        public ActionResult ReadLihtology([Bind(Prefix = "well_id")] string wellId, [Bind(Prefix = "drilling_id")] string drillingId, [Bind(Prefix = "drill_formation_actual_id")] string drill_formation_actual_id, [Bind(Prefix = "depth")] string depth)
        {
            ViewBag.wellId = wellId;
            ViewBag.drillingId = drillingId;
            ViewBag.drill_formation_actual_id = drill_formation_actual_id;
            ViewBag.depth = depth;
            return PartialView();
        }

        public ActionResult Bha([Bind(Prefix = "well_bha_id")] string wellBhaId, [Bind(Prefix = "well_id")] string wellId, [Bind(Prefix = "drilling_id")] string drillingId)
        {
            ViewBag.wellBhaId = wellBhaId;
            ViewBag.wellId = wellId;
            ViewBag.drillingId = drillingId;
            return PartialView();
        }

        public ActionResult BhaEdit([Bind(Prefix = "well_bha_id")] string wellBhaId, [Bind(Prefix = "well_id")] string wellId, [Bind(Prefix = "drilling_id")] string drillingId)
        {
            ViewBag.wellBhaId = wellBhaId;
            ViewBag.wellId = wellId;
            ViewBag.drillingId = drillingId;
            return PartialView();
        }

        public ActionResult Bit([Bind(Prefix = "well_bit_id")] string wellBitId, [Bind(Prefix = "well_id")] string wellId, [Bind(Prefix = "drilling_id")] string drillingId)
        {
            ViewBag.wellBitId = wellBitId;
            ViewBag.wellId = wellId;
            ViewBag.drillingId = drillingId;
            return PartialView();
        }

        public ActionResult BitEdit([Bind(Prefix = "well_bit_id")] string wellBitId, [Bind(Prefix = "well_id")] string wellId, [Bind(Prefix = "drilling_id")] string drillingId)
        {
            ViewBag.wellBitId = wellBitId;
            ViewBag.wellId = wellId;
            ViewBag.drillingId = drillingId;
            return PartialView();
        }

        public ActionResult Approval()
        {
            return View();
        }

        public ActionResult Approved()
        {
            return View();
        }
        public ActionResult History()
        {
            return View();
        }

        public ActionResult HistoryDetails([Bind(Prefix = "id")] string recordId)
        {
            ViewBag.recordId = recordId;
            return PartialView();
        }
    }
}