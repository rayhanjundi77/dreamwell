﻿using CommonTools.wkhtml;
using Dreamwell.Infrastructure.WebApplication;
using Rotativa.MVC;

using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TuesPechkin;
namespace Dreamwell.Web.Areas.Core.Controllers
{
    public class DTStuckPipeController : BaseController
    {

        private static IConverter converter =
   new ThreadSafeConverter(
       new RemotingToolset<PdfToolset>(
           new WinAnyCPUEmbeddedDeployment(
               new TempFolderDeployment())));

        private static IConverter imageConverter =
    new ThreadSafeConverter(
        new RemotingToolset<ImageToolset>(
            new WinAnyCPUEmbeddedDeployment(
                new TempFolderDeployment())));
        // GET: Core/DTStuckPipe
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public FileResult Download()
        {
            byte[] result = new byte[1024];
            var doc = new HtmlToPdfDocument();
            doc.GlobalSettings = new GlobalSettings()
            {
                ProduceOutline = true,
                //DocumentTitle = "Pretty Websites",
                //PageOffset = 1,
                PaperSize = PaperKind.A4, // Implicit conversion to PechkinPaperSize
                Margins =
                {
                    //All = 0.49,
                    Top = 0.49,
                    Left = 0.49,
                    Right = 0.49,
                    Bottom = 0.7,
                    Unit = Unit.Inches
                },


            };

            doc.Objects.Add(new ObjectSettings
            {
                PageUrl = $"{Dreamwell.Infrastructure.Options.DreamwellSettings.WebServerUrl}/core/DTStuckPipe",
                //PageUrl = $"http://www.google.com",

                WebSettings = {
                    EnablePlugins=true,
                    DefaultEncoding = "utf-8",
                    LoadImages=true,
                    PrintBackground=true,
                    PrintMediaType=true,
                    EnableJavascript=true,
                    EnableIntelligentShrinking = true,
                    MinimumFontSize = 16,


                    },
                LoadSettings = {
                    DebugJavascript=false,
                    WindowStatus="PAGE_LOADED",
                    StopSlowScript=false,
                    //RenderDelay=10000
                   
                    },
                CountPages = true,
                FooterSettings ={
                    LeftText= "Page [page] of [topage]",
                    ContentSpacing=10,
                    FontSize = 7,
                    }

            });

            converter.Error += Converter_Error;
            converter.Warning += Converter_Warning;
            result = converter.Convert(doc);
            //for (var i = 0; i < 5; i++)
            //{

            //    var path = Path.Combine(Path.GetTempPath(), String.Format("dreamwell_{0}.pdf", i));

            //    System.IO.File.WriteAllBytes(path, result);
            //}
            return File(result, "application/pdf");
            //return File(result, "application/pdf",string.Format("Final-Report-{0}.pdf",Guid.NewGuid().ToString("N")));
        }
        public void Converter_Warning(object sender, WarningEventArgs e)
        {
            AppLogger.Debug("Warning TuesPechkin");
            AppLogger.Error(e.WarningMessage);
            AppLogger.Error(e);
        }

        public void Converter_Error(object sender, TuesPechkin.ErrorEventArgs e)
        {
            AppLogger.Debug("Error TuesPechkin");
            AppLogger.Error(e.ErrorMessage);
            AppLogger.Error(e);
        }

    }
}