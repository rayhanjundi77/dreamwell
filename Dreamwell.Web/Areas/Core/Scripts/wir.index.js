﻿$(document).ready(function () {
    'use strict';

    var bussinessUnitID = $('input[id=PrimaryBusinessUnitId]');
    var $recordId = $('input[id=recordId]');

    var $formPotential = $('#form-potential');
    var $formFilled = $('#formFilled');
    var $formRatio = $('#formRatio');


    var btnPotential = $('#btn-save-potential');
    var btnFilled = $('#btn-save-filled');
    var btnRatio = $('#btn-save-ratio');

    $('#form-wir, #formWorkload, #formRatio, #formFilled, #panel-estimation, #panel-attachments, #potensial').hide();


    $('#well_color').on('change', function () {
        const selectedOption = $(this).find('option:selected');
        const bgColor = selectedOption.data('bg');

        $(this).css('background-color', bgColor);
    });

    $('.summernote').summernote({
        placeholder: 'Enter text here...',
        tabsize: 2,
        height: 200,
        callbacks: {
            onImageUpload: function (files) {
                console.log('Uploading file:', files[0]);
            }
        }
    });

    $('#well_id').cmSelect2({
        url: $.helper.resolveApi('~/core/wellwi/lookup'),
        result: {
            id: 'id',
            text: 'well_name'
        },
        filters: function (params) {
            return [{
                field: "well_name",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            placeholder: "Select a Well",
            allowClear: true,
            maximumSelectionLength: 1,
        }
    });

    // Event handler untuk well_id
    $('#well_id').on('select2:select', function (e) {
        const wellId = e.params.data.id; 
        const wellName = e.params.data.text; 
        console.log("Selected Well ID:", wellId);
        console.log("Selected Well Name:", wellName);

        $('#well_name').val(wellName);
        $('#well_id_wir').val(wellId);
        $('#well_id_cost').val(wellId);
        $('#well_id_potential').val(wellId);
        $('#well_id_filled').val(wellId);
        $('#well_id_ratio').val(wellId);
        $('#well_id_attach').val(wellId);
        $('#well_id_approval').val(wellId);
        
        console.log('welllll:', $('#well_id_approval').val());

        if (wellId) {
            $('#well_name').val(wellName); 
            $('#form-wir, #formWorkload, #formRatio, #formFilled, #panel-estimation, #potensial, #panel-attachments').fadeIn();

            getWellData(wellId);
            getPotentialData(wellId);
            getAttachData(wellId);
            getCostData(wellId);
            //getWorkloadWir(wellId);
            getRatioData(wellId);
            getfilledData(wellId);
            getApprovalWir(wellId);
        } else {
            $('#well_name').val('');
            $('#form-wir, #formWorkload, #formRatio, #formFilled, #panel-estimation, #potensial, #panel-attachments').fadeOut();
            $('#tableWIRWorkload tbody').empty(); 
        }
    });

    //$("#field_id").cmSelect2({
    //    url: $.helper.resolveApi('~/core/BusinessUnitField/lookup?bussinessUnitID=' + bussinessUnitID),
    //    result: {
    //        id: 'field_id',
    //        text: 'field_name'
    //    },
    //    filters: function (params) {
    //        return [{
    //            field: "field_name",
    //            operator: "contains",
    //            value: params.term || '',
    //        }];
    //    },
    //    options: {
    //        placeholder: "Select a field",
    //        allowClear: true,
    //        maximumSelectionLength: 1,
    //    }
    //});

    //$("#field_id").on('select2:select', function (e) {
    //    const fieldId = e.params.data.id;
    //    const fieldName = e.params.data.text;
    //    console.log("Selected Well ID:", fieldId);
    //    console.log("Selected Well Name:", fieldName);
    //    var selectedData = e.params.data; // Data yang dipilih dari Select2

    //    console.log("Selected Data: ", selectedData); // Debug untuk memastikan isinya
    //    console.log("Selected ID: ", selectedData.id);
    //    console.log("Selected Text: ", selectedData.text);

    //    //$("#field_name").text(selectedData.text); 
    //    $('#field_name').val(fieldName); 
    //    $('#field_id').val(selectedData.id);    
    //});


    $('#btn-saveGeneralWIR').on('click', function (e) {
        e.preventDefault();

        // Validasi form
        if (!$('#form-wir')[0].checkValidity()) {
            $('#form-wir')[0].reportValidity();
            return;
        }

        if (!$('#field_name').val()) {
            toastr.error("Field name is required.");
            return;
        }

        // Serialisasi data form
        const formData = {
            id: $('#recordId').val(),
            wir_no: $('#wir_no').val(),
            originator: $('#originator').val(),
            issues_date: $('#issues_date').val(),
            field_id: $('#field_id').val(),
            field_name: $('#field_name').val(),  
            well_id: $('#well_id_wir').val(),  
            well_name: $('#well_name').val(),  
            well_color: $('#well_color').val(),
            priority: $('#priority').val(),
            main_job_id: $('#main_job_id').val(),
            budget_ass: $('#budget_ass').val(),
            request_status: $('#request_status').val(),
            type_operation: $('#type_operation').summernote('code'),
            present_status: $('#present_status').summernote('code'),
            main_lines: $('#main_lines').summernote('code'),
            reservoir_information: $('#reservoir_information').summernote('code'),
        };

        // Tambahkan indikator loading
        $('#btn-saveGeneralWIR').prop('disabled', true).text('Saving...');
        console.log('form', formData);
        // Kirim data melalui AJAX
        $.ajax({
            url: $.helper.resolveApi('~/core/wir/save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function (response) {
                //toastr.success('Well Intervention Request saved successfully!');
                toastr.success('Save to continue to fill up form WIR');

                $('#form-wir')[0].reset(); 
                $('#form-wir, #formWorkload, #formRatio, #formFilled, #panel-estimation, #potensial, #panel-attachments ').fadeOut(); // Hide forms again
                $('#well_id').val('').trigger('change'); 
                $('#well_name').val(''); 
                $('#selected-well-id').text(''); 

                $('#type_operation').summernote('code', '');
                $('#present_status').summernote('code', '');
                $('#main_lines').summernote('code', '');
                $('#reservoir_information').summernote('code', '');

                $('#tableWIRWorkload tbody').empty();
                rowCount = 1;

                $('#btn-saveGeneralWIR').prop('disabled', false).text('Save');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while saving the record: ' + error);
                $('#btn-saveGeneralWIR').prop('disabled', false).text('Save');
            }
        });
        
    });

    var getWellData = function (wellId) {
        if (!wellId) {
            alert('Well ID is required');
            return;
        }

        $.ajax({
            url: $.helper.resolveApi('~/core/wir/listByWellId/' + wellId),
            type: 'GET',
            success: function (response) {
                if (response.status.success && response.data.length > 0) {

                    var wirData = response.data[0].wir; 
                    var wellwiData = response.data[0].wellwi; 
                    console.log("WIR Data: ", wirData);
                    console.log("Wellwi Data: ", wellwiData);

                    // Isi data `wir` ke form
                    $('#wir_no').val(wirData.wir_no || ""); // Jika null, isi string kosong
                    $('#originator').val(wirData.originator || "");
                    $('#issues_date').val(wirData.issues_date ? moment(wirData.issues_date).format('MM/DD/YYYY') : "");
                    $('#well_color').val(wirData.well_color || "");
                    $('#priority').val(wirData.priority || "");
                    $('#main_job_id').val(wirData.main_job_id || "");
                    $('#budget_ass').val(wirData.budget_ass || "");
                    $('#request_status').val(wirData.request_status || "");

                    // Summernote fields
                    $('#type_operation').summernote('code', wirData.type_operation || "");
                    $('#present_status').summernote('code', wirData.present_status || "");
                    $('#main_lines').summernote('code', wirData.main_lines || "");
                    $('#reservoir_information').summernote('code', wirData.reservoir_information || "");

                    // Isi data `wellwi` ke input field_name
                    if (wellwiData && wellwiData.length > 0) {
                        var fieldNames = wellwiData.map(well => well.field_name).join(", ");
                        console.log("Field Names: ", fieldNames);
                        $("#field_name").val(fieldNames);
                    } else {
                        console.warn("No wellwi data found.");
                        $("#field_name").val("No fields available");
                    }
                } else {
                    toastr.warn("No data found for the given Well ID.");
                    resetForm();
                }
            },
            error: function (xhr, status, error) {
                toastr.error("An error occurred while fetching data: " + error);
                console.error("Error Response: ", xhr);
                resetForm();
            }
        });
    };


    //var getWellData = function (wellId) {
    //    if (!wellId) {
    //        alert('Well ID is required');
    //        return;
    //    }

    //    $.ajax({
    //        url: $.helper.resolveApi('~/core/wir/listByWellId/' + wellId),
    //        type: 'GET',
    //        success: function (response) {
    //            if (response.status.success && response.data.length > 0) {
    //                // Ambil data pertama dari response
    //                var wellData = response.data[0];
    //                console.log("WIR Data: ", wellData);

    //                // Ambil wellwi (list vw_wellwi) dari wellData
    //                if (wellData.wellwi && wellData.wellwi.length > 0) {
    //                    // Gabungkan semua field_name dari list wellwi
    //                    var fieldNames = wellData.wellwi.map(well => well.field_name).join(", ");
    //                    console.log("Field Names: ", fieldNames);

    //                    // Tampilkan field_name di input field_name
    //                    $("#field_name").val(fieldNames);
    //                } else {
    //                    console.warn("No wellwi data found.");
    //                    $("#field_name").val("No fields available");
    //                }

    //                // Isi data lainnya ke form
    //                $('#wir_no').val(wellData.wir_no);
    //                $('#originator').val(wellData.originator);
    //                $('#issues_date').val(moment(wellData.issues_date).format('MM/DD/YYYY'));
    //                $('#well_color').val(wellData.well_color);
    //                $('#priority').val(wellData.priority);
    //                $('#main_job_id').val(wellData.main_job_id);
    //                $('#budget_ass').val(wellData.budget_ass);
    //                $('#request_status').val(wellData.request_status);

    //                // Summernote fields
    //                $('#type_operation').summernote('code', wellData.type_operation);
    //                $('#present_status').summernote('code', wellData.present_status);
    //                $('#main_lines').summernote('code', wellData.main_lines);
    //                $('#reservoir_information').summernote('code', wellData.reservoir_information);
    //            } else {
    //                toastr.warn('No data found for the given Well ID.');
    //                resetForm();
    //            }
    //        },
    //        error: function (xhr, status, error) {
    //            toastr.error('An error occurred while fetching data: ' + error);
    //            resetForm();
    //        }
    //    });
    //};

    var resetForm = function () {
        $("#well_type").val('');
        
        $('#wir_no').val('');
        $('#originator').val('');
        $('#issues_date').val('');
        $('#field_id').val('').trigger('change');
        $('#well_color').val('');
        $('#priority').val('');

        $('#main_job_id').val('');
        
        $('#budget_ass').val('');
        $('#request_status').val('');
        $('#type_operation').summernote('code', '');
        $('#present_status').summernote('code', '');
        $('#main_lines').summernote('code', '');
        $('#reservoir_information').summernote('code', '');
    }

    $('#issues_date').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true,
        todayHighlight: true,
        language: 'en'
    });

    $("#main_job_id").cmSelect2({
        url: $.helper.resolveApi('~/core/iadcwi/lookup'),
        result: {
            id: 'id',
            text: 'job_code', 
            unit_desc: 'unit_description',
            operation_description: 'operation_description',
            description: 'description'
        },
        filters: function (params) {
            return [{
                field: "unit_description",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            placeholder: "Select a Main Job",
            allowClear: true,
            maximumSelectionLength: 2,
            templateResult: function (data) {
                if (!data.id) {
                    // Placeholder
                    return data.text;
                }
                return $(`<span><strong>${data.unit_desc}</strong> - ${data.operation_description}</span>`);
            },
            templateSelection: function (data) {
                if (!data.unit_desc || !data.operation_description) {
                    return "Select a Main Job"; 
                }
                return `${data.unit_desc} - ${data.operation_description}`;
            }
        }
    });

    let selectedUnit = {};
    $("#main_job_id").on('select2:select', function (e) {
        selectedUnit = {
            id: e.params.data.id,
            unit_desc: e.params.data.unit_desc,
            op_desc: e.params.data.operation_description,
            desc: e.params.data.description
        };

        console.log("Main Job: ", selectedUnit); 
    });


    let rowCount = 1;
    $('#addRowButton').click(function () {
        const selectedIntegrity = $('#well_integrity').val();

        if (!selectedIntegrity || !selectedUnit.unit_desc) {
            alert('Please select a valid Unit and Well Integrity before adding a row.');
            return;
        }
        const newRow = `
    <tr>
        <td>${rowCount++}</td>
        <td>
            <input type="text" class="form-control" name="unit" value="${selectedUnit.unit_desc}" readonly>
        </td>
        <td>
            <input type="text" class="form-control" name="job_title" value="${selectedUnit.op_desc}" readonly>
        </td>
        <td>
            <input type="text" class="form-control" name="workload_description" value="${selectedUnit.desc}" readonly>
        </td>
        <td>
            <input type="text" class="form-control" name="well_integrity" value="${selectedIntegrity}" readonly>
        </td>
        <td>
            <button type="button" class="btn btn-danger btn-sm removeRow">
                <i class="fal fa-trash"></i>
            </button>
        </td>
    </tr>
`;
        $('#workload-body').append(newRow);
    });

    function removeRow(button) {
        $(button).closest('tr').remove();
    }

    //$('#btn-save-workload').on('click', function (e) {
    //    e.preventDefault();

    //    if ($('#unit_id').val().trim() === "") {
    //        toastr.error('Unit must be added.');
    //        return;
    //    }

    //    if ($('#tableWIRWorkload tbody tr').length === 0) {
    //        toastr.error('At least one workloadWir must be added.');
    //        return;
    //    }

    //    const formData = {
    //        well_id: $('#well_id').val().trim(), 
    //        unit: $('#unit_id').val().trim(),
    //        expenditure: $('#expenditure').val().trim(),
    //        workloadWir: [] 
    //    };

    //    $('#tableWIRWorkload tbody tr').each(function () {
    //        const workloadWir = {
    //            unit: $(this).find('td').eq(1).text(), 
    //            job_title: $(this).find('td').eq(2).find('input').val(), 
    //            workload: $(this).find('td').eq(3).find('input').val(), 
    //            well_integrity: $(this).find('td').eq(4).text() 
    //        };

    //        console.log('Workload: ', workloadWir);

    //        formData.workloadWir.push(workloadWir);
    //    });

    //    console.log('Form Data:', formData);

    //    // Kirim data melalui AJAX
    //    $.ajax({
    //        url: $.helper.resolveApi('~/core/WIRWorkload/save'),
    //        type: 'POST',
    //        contentType: 'application/json',
    //        data: JSON.stringify(formData),
    //        success: function (response) {
    //            toastr.success('Data saved successfully!');
    //        },
    //        error: function (xhr, status, error) {
    //            toastr.error('Error saving data: ' + error);
    //        }
    //    });
    //});

    //function getWorkloadWir(wellId) {
    //    // Pastikan ID well tidak kosong
    //    if (!wellId) {
    //        alert('Well ID is required');
    //        return;
    //    }

    //    $.ajax({
    //        url: $.helper.resolveApi('~/core/WIRWorkload/listByWellId/' + wellId),
    //        type: 'GET',
    //        success: function (response) {
    //            if (response.status.success && response.data.length > 0) {
    //                var workloadData = response.data[0];  

    //                $('#unit').val(workloadData.unit);
    //                $('#expenditure').val(workloadData.expenditure);
    //                $('#job_title').val(workloadData.job_title);
    //                $('#workload').val(workloadData.workload);
    //                $('#well_integrity').val(workloadData.well_integrity);
    //            } else {
    //                toastr.error('Please complete the form workload first');
    //            }
    //        },
    //        error: function (xhr, status, error) {
    //            toastr.error('An error occurred while fetching data: ' + error);
    //        }
    //    });
    //}



    //$('.success-ratio-profile').each(function () {
    //    $(this).val(""); // Set to the default disabled option
    //});

    //$('.success-ratio-profile').on('change', function () {
    //    try {
    //        // Temukan baris terkait
    //        var row = $(this).closest('tr');
    //        var successRatioCell = row.find('.status_ratio_text');

    //        console.log("Ratio :", successRatioCell);

    //        // Perbarui sel status
    //        var selectedValue = $(this).val();
    //        if (selectedValue === 'DLS 1') {
    //            successRatioCell.css({ 'background-color': 'green', 'color': 'white' }).text('High');
    //        } else if (selectedValue === 'DLS 2') {
    //            successRatioCell.css({ 'background-color': 'yellow', 'color': 'black' }).text('Medium');
    //        } else if (selectedValue === 'DLS 3') {
    //            successRatioCell.css({ 'background-color': 'red', 'color': 'white' }).text('Low');
    //        } else {
    //            successRatioCell.css({ 'background-color': '', 'color': '' }).text('');
    //        }

    //        // Simpan nilai status dan warna di elemen tersembunyi dalam baris
    //        var statusText = successRatioCell.text();
    //        var statusColor = successRatioCell.css('background-color');

    //        row.find('.status_ratio_text').val(statusText); // Simpan teks status
    //        row.find('.status_ratio_color').val(statusColor); // Simpan warna status

    //        // Debugging
    //        console.log("Selected Value:", selectedValue);
    //        console.log("Status Ratio Text:", statusText);
    //        console.log("Status Ratio Color:", statusColor);

    //    } catch (error) {
    //        console.error("An error occurred while updating Success Ratio:", error);
    //    }
    //});

    $('.success-ratio-profile').each(function () {
        $(this).val(""); // Set to the default disabled option
    });

    $('.success-ratio-profile').on('change', function () {
        try {
            var selectedValue = $(this).val();
            var successRatioCell = $(this).closest('tr').find('.well-profile');

            // Check the selected value and update the background color and text
            if (selectedValue === 'DLS 1') {
                successRatioCell.css('background-color', 'green').text('High');
            } else if (selectedValue === 'DLS 2') {
                successRatioCell.css('background-color', 'yellow').text('Medium');
            } else if (selectedValue === 'DLS 3') {
                successRatioCell.css('background-color', 'red').text('Low');
            } else {
                successRatioCell.css('background-color', '').text('');
            }

            // Tambahkan value untuk dikirim ke form atau request
            var statusText = successRatioCell.text(); // Ambil teks status (High, Medium, Low)
            var statusColor = successRatioCell.css('background-color'); // Ambil background color

            // Store the values for sending to the server
            $('#status_ratio_text').val(statusText); // Simpan teks status di input tersembunyi
            $('#status_ratio_color').val(statusColor); // Simpan warna background di input tersembunyi

        } catch (error) {
            console.error("An error occurred while updating Success Ratio:", error);
        }
    });


    $('.success-ratio-history').each(function () {
        $(this).val(""); // Set to the default disabled option
    });

    $('.success-ratio-history').on('change', function () {
        try {
            var selectedValue = $(this).val();
            var successRatioCell = $(this).closest('tr').find('.well-history');

            // Check the selected value and update the background color and text
            if (selectedValue === 'No') {
                successRatioCell.css('background-color', 'green').text('High');
                console.log("Changed to High with Green background.");
            } else if (selectedValue === 'Medium') {
                successRatioCell.css('background-color', 'yellow').text('Medium');
                console.log("Changed to Medium with Yellow background.");
            } else if (selectedValue === 'Difficult') {
                successRatioCell.css('background-color', 'red').text('Low');
                console.log("Changed to Low with Red background.");
            } else {
                successRatioCell.css('background-color', '').text('');
                console.warn("Unrecognized value selected:", selectedValue);
            }
        } catch (error) {
            console.error("An error occurred while updating Success Ratio:", error);
        }
    });

    $('#btn-save-ratio').on('click', function (e) {
        e.preventDefault();

        if (!$formRatio[0].checkValidity()) {
            $formRatio[0].reportValidity();
            return;
        }

        var data = $formRatio.serializeToJSON();
        console.log('Serialized Form Data:', data);

        const formData = {
            id: $('#recordId').val(),
            well_id: $('#well_id_ratio').val(),
            well_profile: $('#well_profile').val(),
            well_history: $('#well_history').val(),
            category_ori: $('#category_ori').val(),
            status_ori: $('#status_ori').val(),
            status_ratio: $('#status_ratio').val(),
            status_ratio_text: $('#status_ratio_text').val(),
            status_ratio_color: $('#status_ratio_color').val()
        };

        console.log('FormData Ratio:', formData);

        //btnRatio.prop('disabled', true).text('Saving...');

        $.ajax({
            url: $.helper.resolveApi('~/core/SuccessRatio/save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function (response) {
                toastr.success('Data saved successfully!');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while saving the record: ' + error);
                btnRatio.prop('disabled', false).text('Save');
            }
        });
    });

    function getRatioData(wellId) {
        // Pastikan ID well tidak kosong
        if (!wellId) {
            alert('Well ID is required');
            return;
        }

        // Kirim request GET untuk mengambil data well
        $.ajax({
            url: $.helper.resolveApi('~/core/SuccessRatio/listByWellId/' + wellId),
            type: 'GET',
            success: function (response) {
                if (response.status.success && response.data.length > 0) {
                    var successRatio = response.data[0];  // Ambil data pertama dari array response

                    // Isi form dengan data yang diambil
                    $('#category').val(successRatio.category);
                    $('#description').val(successRatio.description);
                    $('#well_profile').val(successRatio.well_profile);
                    $('#well_history').val(successRatio.well_history);
                    $('#well_id_ratio').val(successRatio.well_id);

                    toastr.success('Data loaded successfully');
                } else {
                    toastr.info('No data found for the selected Well. Please ensure the form is completed first.');
                }
            },
            error: function (xhr, status, error) {
                toastr.error('An error occurred while fetching data: ' + error);
            }
        });
    }

    // Filled
    $('.success-ratio-select').on('change', function () {
        var selectedValue = $(this).val();
        var row = $(this).closest('tr'); 

        if (selectedValue === 'High') {
            row.find('td:eq(2)').css('background-color', 'green');
        } else if (selectedValue === 'Medium') {
            row.find('td:eq(2)').css('background-color', 'yellow');
        } else if (selectedValue === 'Low') {
            row.find('td:eq(2)').css('background-color', 'red');
        }
    });

    $('.success-ratio-select').each(function () {
        $(this).val(""); 
    });

    $('.success-ratio-select').on('change', function () {
        try {
            var selectedValue = $(this).val();
            var successRatioCell = $(this).closest('tr').find('.status_ori');

            // Check the selected value and update the background color and text
            if (selectedValue === 'High') {
                successRatioCell.css('background-color', 'green').text('High');
                console.log("Changed to High with Green background.");
            } else if (selectedValue === 'Medium') {
                successRatioCell.css('background-color', 'yellow').text('Medium');
                console.log("Changed to Medium with Yellow background.");
            } else if (selectedValue === 'Low') {
                successRatioCell.css('background-color', 'red').text('Low');
                console.log("Changed to Low with Red background.");
            } else {
                successRatioCell.css('background-color', '').text('');
                console.warn("Unrecognized value selected:", selectedValue);
            }
        } catch (error) {
            console.error("An error occurred while updating Success Ratio:", error);
        }
    });

    $('#btn-save-filled').on('click', function (e) {
        e.preventDefault();

        if (!$formFilled[0].checkValidity()) {
            $formFilled[0].reportValidity();
            return;
        }

        var data = $formFilled.serializeToJSON();
        console.log('Serialized Form Data:', data);

        const formData = {
            id: $('#recordId').val(),
            status_ori: $('#status_ori').val(),
            well_id: $('#well_id_filled').val(),
        };

        console.log('FormData Filled:', formData);

        //btnFilled.prop('disabled', true).text('Saving...');

        $.ajax({
            url: $.helper.resolveApi('~/core/SuccessRatio/save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function (response) {
                toastr.success('Data saved successfully!');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while saving the record: ' + error);
                btnFilled.prop('disabled', false).text('Save');
            }
        });
    });

    function getfilledData(wellId) {
        if (!wellId) {
            alert('Well ID is required');
            return;
        }

        $.helper.form.clear($formFilled);

        $.ajax({
            url: $.helper.resolveApi('~/core/SuccessRatio/listByWellId/' + wellId),
            type: 'GET',
            success: function (response) {
                if (response.status.success && response.data.length > 0) {
                    var successRatio = response.data[0];

                    $('#status_ori').val(successRatio.status_ori);
                    $('#well_id').val(successRatio.well_id);

                    toastr.success('Data loaded successfully');
                } else {
                    toastr.info('No data found for the selected Well. Please ensure the form is completed first.');
                }
                btnFilled.button('reset');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while fetching data: ' + error);
                btnFilled.button('reset');
            }
        });
    }

    //Potential
    $('#btn-save-potential').on('click', function (e) {
        e.preventDefault();

        if (!$formPotential[0].checkValidity()) {
            $formPotential[0].reportValidity();
            return;
        }

        var data = $formPotential.serializeToJSON();
        console.log('Serialized Form Data:', data);

        const formData = {
            id: $('#recordId').val(),
            current_pot: $('#current_pot').val(),
            expected: $('#expected').val(),
            gain_gas: $('#gain_gas').val(),
            gain_oil: $('#gain_oil').val(),
            well_id: $('#well_id_potential').val(),
            gas_stakes: $('#gas_stakes').val(),
            kUSD_gas: $('#kUSD_gas').val(),
            oil_stakes: $('#oil_stakes').val(),
            kUSD_oil: $('#kUSD_oil').val(),
            prepared_by: $('#prepared_by').val(),
            department: $('#department').val(),
            geologist: $('#geologist').val(),
        };

        console.log('FormData Cost:', formData);

        //btnPotential.prop('disabled', true).text('Saving...');

        $.ajax({
            url: $.helper.resolveApi('~/core/Potential/save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function (response) {
                toastr.success('Data saved successfully!');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while saving the record: ' + error);
                btnPotential.prop('disabled', false).text('Save');
            }
        });
    });

    function getPotentialData(wellId) {
        if (!wellId) {
            alert('Well ID is required');
            return;
        }

        $.helper.form.clear($formPotential);

        $.ajax({
            url: $.helper.resolveApi('~/core/Potential/listByWellId/' + wellId),
            type: 'GET',
            success: function (response) {
                if (response.status.success && response.data.length > 0) {
                    var potentialData = response.data[0];

                    $('#current_pot').val(potentialData.current_pot);
                    $('#expected').val(potentialData.expected);
                    $('#gain_gas').val(potentialData.gain_gas);
                    $('#gain_oil').val(potentialData.gain_oil);
                    $('#well_id').val(potentialData.well_id);
                    $('#gas_stakes').val(potentialData.gas_stakes);
                    $('#kUSD_gas').val(potentialData.kUSD_gas);
                    $('#oil_stakes').val(potentialData.oil_stakes);
                    $('#kUSD_oil').val(potentialData.kUSD_oil);
                    $('#prepared_by').val(potentialData.prepared_by);
                    $('#department').val(potentialData.department);
                    $('#geologist').val(potentialData.geologist);

                    toastr.success('Data loaded successfully');
                } else {
                    toastr.info('No data found for the selected Well. Please ensure the form is completed first.');
                }
                btnPotential.button('reset');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while fetching data: ' + error);
                btnPotential.button('reset');
            }
        });
    }

    //Cost WIR
    $('#btn-save-cost').on('click', function (e) {
        e.preventDefault();

        const $form = $('#form-estimation'); 
        const btn = $('#btn-save-cost'); 

        // Validasi form
        if (!$form[0].checkValidity()) {
            $form[0].reportValidity();
            return;
        }

        // Serialize form data menjadi JSON
        var data = $form.serializeToJSON();
        console.log('Serialized Form Data:', data);

        const formData = {
            id: $('#recordId').val(),
            well_id: $('#well_id_cost').val(),
            usd: $('#usd').val(),
            duration: $('#duration').val(),
            remarks: $('#remarks').val(),
            estimated_stakes: $('#estimated_stakes').val(),
            key_1: $('#key_1').val(),
            key_2: $('#key_2').val(),
        };

        console.log('FormData Cost:', formData);

        //btn.prop('disabled', true).text('Saving...');

        $.ajax({
            url: $.helper.resolveApi('~/core/CostWir/save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function (response) {
                toastr.success('Data saved successfully!');
                //$.helper.form.clear($form);

                //// Reset tombol
                //btn.button('reset');

                //// Log hasil reset
                //console.log('Form has been reset and save button is restored.');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while saving the record: ' + error);

                // Reset tombol jika terjadi error
                btn.prop('disabled', false).text('Save');
            }
        });
    });

    function getCostData(wellId) {
        const $form = $('#form-estimation'); 
        const btn = $('#btn-save-cost'); 

        if (!wellId) {
            alert('Well ID is required');
            return;
        }

        $.helper.form.clear($form);

        $.ajax({
            url: $.helper.resolveApi('~/core/CostWir/listByWellId/' + wellId),
            type: 'GET',
            success: function (response) {
                if (response.status.success && response.data.length > 0) {
                    var costData = response.data[0]; // Ambil data pertama

                    // Isi form dengan data yang diambil
                    $('#usd').val(costData.usd);
                    $('#duration').val(costData.duration);
                    $('#remarks').val(costData.remarks);
                    $('#estimated_stakes').val(costData.estimated_stakes);
                    $('#well_id').val(costData.well_id);
                    $('#key_1').val(costData.key_1);
                    $('#key_2').val(costData.key_2);

                    toastr.success('Data loaded successfully');
                } else {
                    toastr.info('No data found for the selected Well. Please ensure the form is completed first.');
                }

                btn.button('reset');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while fetching data: ' + error);
                btn.button('reset');
            }
        });
    }


    //Attachment Files
    $('#files').on("change", function () {
        console.log("test upload attachments " + $recordId.val());
        upload($recordId.val())
    });

    function upload(wellId, rowElement) {
        var formData = new FormData();
        var file = $(rowElement).find('.file-input')[0].files[0];

        if (!file) {
            toastr.error("No file selected.");
            return;
        }

        formData.append('file', file);

        $.ajax({
            url: $.helper.resolveApi("~/Core/Attach/upload?wellId=" + wellId),
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.status && response.status.success) {
                    toastr.success(response.status.message || "File uploaded successfully.");

                    $(rowElement).find('#file_name').val(response.data[0].fileName);
                    console.log("File uploaded:", response.data[0].fileName);

                    console.log("file_name updated to:", $(rowElement).find('#file_name').val());
                } else {
                    toastr.warning(response.status.message || "File uploaded, but response format is unexpected.");
                }
            },
            error: function () {
                toastr.error("Failed to upload file.");
            }
        });
    }

    //$('#dt_Attachments').on("change", ".file-input", function () {
    //    var rowElement = $(this).closest('tr');
    //    var wellId = $('#well_id_attach').val().trim();

    //    if (!wellId) {
    //        toastr.error("Well ID is missing.");
    //        return;
    //    }

    //    upload(wellId, rowElement);
    //});

    $('#dt_Attachments').on("change", ".file-input", function () {
        var rowElement = $(this).closest('tr'); // Ambil elemen baris terkait
        var wellId = $('#well_id_attach').val().trim(); // Ambil Well ID

        if (!wellId) {
            toastr.error("Well ID is missing.");
            return;
        }

        var file = $(this)[0].files[0]; // Ambil file dari input
        if (file) {
            // Update file size
            var fileSize = (file.size / 1024).toFixed(2) + " KB"; // Konversi ke KB
            $(rowElement).find('.file-size').val(fileSize);

            // Upload file dan perbarui `file_name`
            upload(wellId, rowElement);
        } else {
            console.error("File input is empty for row:", rowElement);
        }
    });



    //$('#btn-saveAttachWIR').on('click', function (e) {
    //    e.preventDefault();

    //    let hasEmptyFileName = false;

    //    $('#dt_Attachments tbody tr').each(function () {
    //        const fileName = $(this).find('#file_name').val();
    //        console.log("Checking file_name for row:", fileName);

    //        if (!fileName || fileName.trim() === '') {
    //            hasEmptyFileName = true;
    //            return false;
    //        }
    //    });

    //    if (hasEmptyFileName) {
    //        toastr.error('Please upload a file for each attachment.');
    //        return;
    //    }

    //    // Jika semua validasi lolos, lanjutkan dengan pengiriman data
    //    const formData = {
    //        well_id: $('#well_id_attach').val().trim(),
    //        wli_comments: $('#wli_comments').val().trim(),
    //        attachments: [] // Tambahkan data lampiran
    //    };

    //    $('#dt_Attachments tbody tr').each(function () {
    //        formData.attachments.push({
    //            attach_type: $(this).find('td').eq(1).find('input').val(),
    //            file_name: $(this).find('#file_name').val(), // Pastikan ini tidak kosong
    //            description: $(this).find('td').eq(3).find('input').val(),
    //            size: $(this).find('td').eq(4).find('input').val()
    //        });
    //    });

    //    console.log('Form Data:', formData);

    //    $.ajax({
    //        url: $.helper.resolveApi('~/core/Attach/save'),
    //        type: 'POST',
    //        contentType: 'application/json',
    //        data: JSON.stringify(formData),
    //        success: function (response) {
    //            if (response.success) {
    //                toastr.success('Data saved successfully!');
    //            } else {
    //                toastr.warning('Data saved, but response format is unexpected.');
    //            }
    //        },
    //        error: function (xhr, status, error) {
    //            toastr.error('Error saving data: ' + error);
    //        }
    //    });
    //});

    $('#btn-saveAttachWIR').on('click', function (e) {
        e.preventDefault();

        let hasEmptyFileName = false;

        $('#dt_Attachments tbody tr').each(function (index) {
            const fileNameInput = $(this).find('#file_name');
            const fileName = fileNameInput.val(); // Ambil nilai dari input hidden

            // Log detail untuk debugging
            console.log(`Row ${index + 1}:`, {
                fileNameInputExists: fileNameInput.length > 0,
                fileName: fileName
            });

            if (!fileName || fileName.trim() === '') {
                hasEmptyFileName = true;
                console.error(`Row ${index + 1} has an empty or undefined file_name.`);
                return false;
            }
        });

        if (hasEmptyFileName) {
            toastr.error('Please upload a file for each attachment.');
            return;
        }

        // Jika semua validasi lolos, lanjutkan dengan pengiriman data
        const formData = {
            well_id: $('#well_id_attach').val().trim(),
            //wli_comments: $('#wli_comments').val().trim(),
            wli_comments: $('#wli_comments').summernote('code').trim(),
            attachments: []
        };

        console.log('WLI Comments:', $('#wli_comments').val());
        console.log('Form Data:', formData);

        $('#dt_Attachments tbody tr').each(function () {
            formData.attachments.push({
                attach_type: $(this).find('td').eq(1).find('input').val(),
                file_name: $(this).find('#file_name').val(), // Pastikan ini tidak kosong
                description: $(this).find('td').eq(3).find('input').val(),
                size: $(this).find('.file-size').val()
            });
        });

        console.log('Form Data:', formData);

        $.ajax({
            url: $.helper.resolveApi('~/core/Attach/save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function (response) {
                if (response.success) {
                    toastr.success('Data saved successfully!');
                } else {
                    toastr.warning('Data saved, but response format is unexpected.');
                }
            },
            error: function (xhr, status, error) {
                toastr.error('Error saving data: ' + error);
            }
        });
    });

    function getAttachData(wellId) {
        // Pastikan ID well tidak kosong
        if (!wellId) {
            alert('Well ID is required');
            return;
        }

        // Kirim request GET untuk mengambil data lampiran berdasarkan Well ID
        $.ajax({
            url: $.helper.resolveApi('~/core/Attach/listByWellId/' + wellId),
            type: 'GET',
            success: function (response) {
                if (response.status.success && response.data.length > 0) {
                    var successRatio = response.data[0];  // Ambil data pertama dari array response

                    // Isi form dengan data yang diambil
                    $('#category').val(successRatio.category);
                    $('#description').val(successRatio.description);
                    $('#well_profile').val(successRatio.well_profile);
                    $('#well_history').val(successRatio.well_history);
                    $('#well_id_ratio').val(successRatio.well_id);

                    toastr.success('Data loaded successfully');
                } else {
                    toastr.info('No data found for the selected Well. Please ensure the form is completed first.');
                }

                if (response.status.success && response.data.length > 0) {
                    var attachWir = response.data;

                    console.log('data attach: ', attachWir);

                    $('#dt_Attachments tbody').empty();

                    if (response.wli_comments) {
                        $('#wli_comments').summernote('code', response.wli_comments.trim());
                        
                    } else {
                        $('#wli_comments').summernote('code', ''); // Jika tidak ada komentar
                    }

                    attachWir.forEach(function (attachment, index) {
                        var row = `<tr data-attachment-id="${attachment.id}">
                            <td>${index + 1}</td>
                            <td><input type="text" class="form-control" value="${attachment.attach_type}" readonly></td>
                            <td><input type="text" class="form-control" value="${attachment.file_name}" readonly></td>
                            <td><input type="text" class="form-control" value="${attachment.description}" readonly></td>
                            <td><input type="text" class="form-control" value="${attachment.size}" readonly></td>
                            <td class="text-center">
                                <button type="button" class="btn btn-sm btn-danger removeRow">
                                    <i class="fal fa-trash"></i>
                                </button>
                            </td>
                        </tr>`;


                        // Tambahkan baris baru ke tbody
                        $('#dt_Attachments tbody').append(row);
                    });

                    toastr.success('Data loaded successfully');
                } else {
                    $('#wli_comments').summernote('code', '');
                    toastr.info('No data found for the selected Well. Please ensure the form is completed first.');
                }
            },
            error: function (xhr, status, error) {
                toastr.error('An error occurred while fetching data: ' + error);
            }
        });
    }

    //function getAttachData(wellId) {
    //    // Pastikan ID well tidak kosong
    //    if (!wellId) {
    //        alert('Well ID is required');
    //        return;
    //    }

    //    // Inisialisasi Summernote jika belum
    //    if (!$('#wli_comments').data('summernote')) {
    //        $('#wli_comments').summernote({
    //            height: 150, // Tinggi editor
    //            placeholder: 'Enter WLI Comments here...',
    //            toolbar: [
    //                ['style', ['bold', 'italic', 'underline', 'clear']],
    //                ['para', ['ul', 'ol', 'paragraph']],
    //                ['insert', ['link']],
    //                ['view', ['codeview']]
    //            ]
    //        });
    //    }

    //    // Kirim request GET untuk mengambil data lampiran berdasarkan Well ID
    //    $.ajax({
    //        url: $.helper.resolveApi('~/core/Attach/listByWellId/' + wellId),
    //        type: 'GET',
    //        success: function (response) {
    //            if (response.status.success && response.data.length > 0) {
    //                var attachWir = response.data;

    //                console.log('data attach: ', attachWir);

    //                $('#dt_Attachments tbody').empty();

    //                // Set nilai WLI Comments ke Summernote
    //                if (response.wli_comments) {
    //                    $('#wli_comments').summernote('code', response.wli_comments.trim());
    //                } else {
    //                    $('#wli_comments').summernote('code', ''); // Jika tidak ada komentar
    //                }

    //                // Tambahkan data lampiran ke tabel
    //                attachWir.forEach(function (attachment, index) {
    //                    var row = `<tr data-attachment-id="${attachment.id}">
    //                    <td>${index + 1}</td>
    //                    <td><input type="text" class="form-control" value="${attachment.attach_type}" readonly></td>
    //                    <td><input type="text" class="form-control" value="${attachment.file_name}" readonly></td>
    //                    <td><input type="text" class="form-control" value="${attachment.description}" readonly></td>
    //                    <td><input type="text" class="form-control" value="${attachment.size}" readonly></td>
    //                    <td class="text-center">
    //                        <button type="button" class="btn btn-sm btn-danger removeRow">
    //                            <i class="fal fa-trash"></i>
    //                        </button>
    //                    </td>
    //                </tr>`;

    //                    // Tambahkan baris baru ke tbody
    //                    $('#dt_Attachments tbody').append(row);
    //                });

    //                toastr.success('Data loaded successfully');
    //            } else {
    //                $('#wli_comments').summernote('code', '');
    //                toastr.info('No data found for the selected Well. Please ensure the form is completed first.');
    //            }
    //        },
    //        error: function (xhr, status, error) {
    //            toastr.error('An error occurred while fetching data: ' + error);
    //        }
    //    });
    //}


    $(document).on('click', '.removeRow', function () {
        var recordId = $(this).closest('tr').data('attachment-id');
        if (!recordId) {
            toastr.error("No ID found for the selected row.");
            return;
        }

        bootbox.confirm({
            message: "<p class='text-semibold text-main'>Are you sure?</p><p>You won't be able to revert this!</p>",
            buttons: {
                confirm: {
                    label: "Delete",
                    className: "btn-danger"
                },
                cancel: {
                    label: "Cancel",
                    className: "btn-secondary"
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json",
                        url: $.helper.resolveApi("~/core/Attach/delete"),
                        data: JSON.stringify([recordId]),
                        success: function (response) {
                            if (response.status.success) {
                                toastr.success("Data has been deleted.");
                                $(`tr[data-attachment-id="${recordId}"]`).remove();
                                updateRowCount();
                            } else {
                                toastr.error(response.status.message || "Failed to delete the record. Please check logs.");
                            }
                        },
                        error: function (xhr) {
                            var errorMessage = xhr.responseJSON?.status?.message || "An unknown error occurred.";
                            toastr.error("An error occurred: " + errorMessage);
                        }
                    });
                }
            }
        });
    });




    $('.btn-Save').on('click', function (e) {
        e.preventDefault();

        // Identifikasi kolom terkait
        const parentTd = $(this).closest('td');
        const isSubmit = $(this).hasClass('submit') ? '1' : '0';
        const statusText = isSubmit === '1' ? 'Submitted' : 'Approved';

        // Ambil data dari kolom terkait
        const formData = {
            name: parentTd.find('input[id$="_name"]').val(),
            department: parentTd.find('input[id$="_position"]').val(),
            well_id: $('#well_id_attach').val(),
            is_submit: isSubmit,
            status: statusText, // Status yang dikirim ke database
            date_approval: new Date().toISOString(), // Waktu dalam format ISO
        };

        console.log('form approval: ', formData);

        // Validasi input
        if (!formData.name || !formData.department) {
            alert('Please fill in all required fields.');
            return;
        }

        // Tambahkan indikator loading
        const button = $(this);
        button.prop('disabled', true).text(isSubmit === '1' ? 'Submitting...' : 'Saving...');

        // Kirim data melalui AJAX
        $.ajax({
            url: $.helper.resolveApi('~/core/wir/approvalwir/save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function (response) {
                parentTd.html(`
                    <p>${formData.department}</p>
                    <p class="${isSubmit === '1' ? 'text-primary' : 'text-success'} font-weight-bold">${statusText}</p>
                    <p class="text-muted">[ ${new Date().toLocaleString()} ]</p>
                    <p>${formData.name}</p>
                `);

                toastr.success(isSubmit === '1' ? 'Data submitted successfully!' : 'Data saved successfully!');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while saving the record: ' + error);
            },
            complete: function () {
                button.prop('disabled', false).text('Save'); // Reset tombol
            }
        });
    });

    $('.btn-success').on('click', function () {
        const parentTd = $(this).closest('td');
        const saveButton = parentTd.find('.btn-Save');
        saveButton.addClass('submit').click();
    });

    var getApprovalWir = function (wellId) {
        $.get($.helper.resolveApi('~/core/wir/' + wellId + '/detailapprovalwirbyId'), function (r) {
            if (r.status.success && r.data.length > 0) {
                console.log('Response data:', r.data);

                // Iterasi setiap item data dari API
                r.data.forEach(item => {
                    // Normalisasi nama department untuk mencocokkan ID elemen input
                    const departmentKey = item.department.toLowerCase().replace(/\s/g, '_'); // Normalisasi ID
                    const positionInput = `#${departmentKey}_position`; // ID posisi
                    const nameInput = `#${departmentKey}_name`; // ID nama
                    const parentTd = $(nameInput).closest('td'); // Elemen <td> terkait

                    // Pastikan elemen ditemukan
                    if (parentTd.length > 0) {
                        // Tentukan status berdasarkan data API
                        const isSubmit = item.is_submit ? '1' : '0';
                        const statusText = isSubmit === '1' ? 'Submitted' : 'Approved';
                        const approvalTime = item.created_on ? new Date(item.created_on).toLocaleString() : '-';

                        // Perbarui input dengan data API
                        $(positionInput).val(item.department || ''); // Isi input posisi
                        $(nameInput).val(item.name || ''); // Isi input nama

                        // Tambahkan status dan informasi waktu jika ada
                        parentTd.html(`
                        <p>${item.department || ''}</p>
                        <p class="${isSubmit === '1' ? 'text-primary' : 'text-success'} font-weight-bold">${statusText}</p>
                        <p class="text-muted">[ ${approvalTime} ]</p>
                        <p>${item.name || ''}</p>
                    `);
                    } else {
                        console.warn(`No matching input fields for department: ${item.department}`);
                    }
                });
            } else {
                console.warn('No data returned for well ID:', wellId);
            }
        }, 'json').fail(function (r) {
            // Tampilkan pesan error jika request gagal
            toastr.error(r.statusText);
            console.error('Error fetching approval data:', r);
        });
    };











});
