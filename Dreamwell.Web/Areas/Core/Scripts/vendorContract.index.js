﻿(function ($) {
    'use strict';
    var $dt_listVendorContract = $('#dt_listVendorContract');
    var $pageLoading = $('#page-loading-content');

    var pageFunction = function () {
        var loadDataTable = function () {
            var dt = $dt_listVendorContract.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/vendorcontract/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        console.log(d);
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "name" },
                    { data: "unit_name" },
                    { data: "contact_person" },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output = "";
                                console.log(row);
                                if (row.telp_1 !== null && row.telp_1 !== "") {
                                    output += `<div>` + row.telp_1 + `</div>`;
                                } 
                                if (row.telp_2 !== null && row.telp_2 !== "") {
                                    output += `<div>` + row.telp_1 + `</div>`;
                                } 
                                return output;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output =
                                `<div class ="btn-group" data-id= "` + row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/core/vendorcontract/detail?id=") + row.id + `" class="modalTrigger btn btn-primary btn-sm btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#vendorContractModal">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-danger btn-sm btn-info waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/vendor/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dt_listVendor.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 