﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formVendorContract")

    //-- Lookup
    $("#vendor_id").cmSelect2({
        url: $.helper.resolveApi('~/core/Vendor/lookup'),
        result: {
            id: 'id',
            text: 'name'
        },
        filters: function (params) {
            return [{
                field: "name",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            dropdownParent: $form,
            placeholder: "Select a Vendor",
            allowClear: true,
            maximumSelectionLength: 1,
        }
    });

    $("#business_unit_id").cmSelect2({
        url: $.helper.resolveApi('~/core/BusinessUnit/lookupParent'),
        result: {
            id: 'id',
            text: 'unit_name'
        },
        filters: function (params) {
            return [{
                field: "unit_name",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            dropdownParent: $form,
            placeholder: "Select a Company",
            allowClear: true,
            maximumSelectionLength: 1,
        }
    });

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                console.log($recordId.val());
                $.get($.helper.resolveApi('~/core/vendorcontract/' + $recordId.val() + '/detail'), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);

                        if (r.data.fl_rig_vendor) {
                            $("#fl_rig_vendor").attr("checked", "checked");
                        }
                        if (r.data.fl_active) {
                            $("#fl_active").attr("checked", "checked");
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };

        $('#btn-save').click(function (event) {
            var $dtVendor = $('#dt_listVendorContract');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                console.log(data);
                $.post($.helper.resolveApi('~/core/vendorcontract/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                        $dtVendor.DataTable().ajax.reload();
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});