﻿(function ($) {
    'use strict';

    var pagination = $('#pagination');
    var $roleContent = $('#page-role-content');

    var $formModal = $('#applicationRoleModal');
    var $form = $('#form-applicationRole');


    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };
    

    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            role_name: {
                validators: {
                    notEmpty: {
                        message: 'The role name is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION

    var pageFunction = function () {

        pagination.data('twbs-pagination.js', null);

        var templateScript = $("#role-template").html();
        var template = Handlebars.compile(templateScript);


        var pagePlugin = function (options) {
            var o = $roleContent;
            var dataSource = {
                pageSize: 100,
                page: 1,
                filter: {
                    logic: "OR",
                    filters: [
                        {
                            field: "role_name",
                            operator: "contains",
                            value: ""
                        }
                    ]
                }
            }
            options.dataSource = $.extend({}, dataSource, options.dataSource);
            var load = function (opt) {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: opt.url,
                    data: JSON.stringify(opt.dataSource),
                    success: function (data) {
                        console.log(data);
                        if (data.totalPages > 0) {
                            pagination.twbsPagination({
                                initiateStartPageClick: false,
                                totalPages: data.totalPages,
                                startPage: data.currentPage,
                                onPageClick: function (event, page) {
                                    opt.dataSource.page = page;
                                    load(opt);
                                }
                            });
                        }

                        if (data.items.length > 0) {
                            o.html(template(data));
                            $('.row-edit').click(function () {
                                var recordId = $(this).closest(".role-id").data("id");
                                console.log(recordId);
                                $.helper.form.clear($form),
                                    $.get($.helper.resolveApi('~/core/ApplicationRole/' + recordId + '/detail'), function (r) {
                                        if (r.status.success) {
                                            $.helper.form.fill($form, r.data);
                                        }
                                    }).fail(function (r) {
                                        $.helper.noty.error(r.status, r.statusText);
                                    });
                            });
                            $('.row-delete').click(function () {
                                var recordId = $(this).closest(".list-group-item").data("id");
                                console.log(recordId);

                            });

                            $('#page-role-content-no_record').hide();
                        } else {
                            $('#page-role-content-no_record').show();
                        }
                        
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            };
            load(options);
            return {
                refresh: function (opt) {
                    load(opt);
                }
            }
        };
        var reloadPlugin = function () {
            pagePlugin({
                url: $.helper.resolveApi("~/core/ApplicationRole/dataSource")
            });
        }

        $('#btn-addNew').on('click', function () {
            $.helper.form.clear($form),
                $.helper.form.fill($form, {
                    role_name: null
                });
            $formModal.modal('show');
        });

        $('#btn-save').on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/ApplicationRole/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        reloadPlugin();
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            }
            else return;
        });

        $('#btn-syncEntity').on('click', function () {
            var btn = $(this);
            btn.button('loading');
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            $.post($.helper.resolveApi('~/core/ApplicationRole/synchronizeEntityRole'), {}, function (r) {
                if (r.status.success) {
                    toastr.success("Role Entity has been synchronized ")
                } else {
                    toastr.error(r.status.message);
                }
                $('#loadingModal').modal("hide");
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            });


        });



        return {
            init: function () {
                reloadPlugin();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();

    });
}(jQuery));

