﻿(function ($) {
    'use strict';

    var $tree = $('#businessUnit-tree');
    var $businessUnitFormModal = $('#businessUnitModal');
    var $businessUnitModalUpdate = $('#businessUnitModalUpdate');
    var $form = $('#form-businessUnit');
    var $formUpdate = $('#form-businessUnitUpdate');

    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };


    //$businessUnitFormModal.niftyOverlay({
    //    iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
    //});


    var pageFunction = function () {

        // FORM VALIDATION
        // =================================================================
        $form.bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: faIcon,
            fields: {
                unit_name: {
                    validators: {
                        notEmpty: {
                            message: 'The business unit name is required.'
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            // Remove the has-success class
            $parent.removeClass('has-success');
        });
        // END FORM VALIDATION

        $formUpdate.bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: faIcon,
            fields: {
                unit_name: {
                    validators: {
                        notEmpty: {
                            message: 'The business unit name is required.'
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            // Remove the has-success class
            $parent.removeClass('has-success');
        });

        var loadTree = function () {
            $tree.jstree({
                contextmenu: {
                    select_node: false,
                    items: customMenu
                },
                core: {
                    themes: {
                        "responsive": false
                    },
                    check_callback: true,
                    data: function (obj, callback) {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: $.helper.resolveApi("~/core/BusinessUnit/tree"),
                            success: function (data) {
                                callback.call(this, data);
                            }
                        });
                    }

                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "root": {
                        "class": "css-custom"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                },
                state: { "key": "id" },
                plugins: ["dnd", "state", "types", "contextmenu"]
            });

            $tree.bind("move_node.jstree rename_node.jstree", function (e, data) {
                if (e.type == "move_node") {
                    $.get($.helper.resolveApi("~/core/BusinessUnit/order"),
                        {
                            recordId: data.node.id,
                            parentTargetId: data.parent,
                            newOrder: data.old_position < data.position ? data.position + 1 : data.position
                        }, function (data) {
                            if (data.status.success) {
                                //$tree.jstree(true).refresh();
                            }
                        });
                }
            });
        }

        function customMenu(node) {
            // The default set of all items
            var items = {
                Create: {
                    label: "Create",
                    icon: "fa fa-plus-square-o",
                    action: function (n) {
                        console.log(node.id);
                        $.helper.form.clear($form),
                            $.helper.form.fill($form, {
                                parent_unit: node.id,
                                parent_unit_name: node.text
                            });

                        $("#parentId").val(node.id);
                        $businessUnitFormModal.modal('show');
                    }
                },
                Edit: {
                    label: "Edit",
                    icon: "fa fa-pencil-square-o",
                    action: function () {
                        $businessUnitModalUpdate.modal('show'), $.helper.form.clear($formUpdate);
                        $.get($.helper.resolveApi('~/core/BusinessUnit/' + node.id + '/detail'), function (r) {
                            if (r.status.success) {
                                $.helper.form.fill($formUpdate, r.data);
                            }
                        }).fail(function (r) {
                            toastr.error(r.statusText)
                        });
                    }
                },
                Delete: {
                    label: "Delete",
                    icon: "fa fa-trash-o",
                    action: function () {
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are you sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    className: "btn-danger",
                                    label: "Confirm"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/BusinessUnit/delete"),
                                        data: JSON.stringify([node.id]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.success("Data has been deleted")
                                                $tree.jstree("refresh");
                                            } else {
                                                toastr.error(r.status.message)
                                            }
                                            b.modal('hide');
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText)
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    }
                },
                UpdateLeader: {
                    label: "Set Leader",
                    icon: "fa fa-pencil-square-o",
                    action: function () {
                        //$leaderFormModal.modal('show');
                        $.helper.modalDialog.openModal({
                            href: $.helper.resolve("/core/businessUnit/UpdateLeader?id=" + node.id + "&defaultteam=" + node.data.default_team),
                            target: '#LeaderModal',
                            toggle: 'modal-remote'
                        });
                    }
                }
            };
            return items;
        }

        $('#btn-addNew').on('click', function () {
            $.helper.form.clear($form),
                $.helper.form.fill($form, {
                    parent_unit: null,
                    parent_unit_name: null
                });
            $businessUnitFormModal.modal('show');
        });

        $('#btn-save').on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/BusinessUnit/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            $('input[name=id]').val(r.data.recordId);
                            toastr.success(r.status.message)

                            $tree.jstree("refresh");
                        } else {
                            btn.button('reset');
                            toastr.error(r.status.message)
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText)
                });

            }
            else return;
        });

        $('#btn-update').on('click', function () {
            var btn = $(this);
            var validator = $formUpdate.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $formUpdate.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/BusinessUnit/update'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            $('input[name=id]').val(r.data.recordId);
                            toastr.success(r.status.message)

                            $tree.jstree("refresh");
                        } else {
                            btn.button('reset');
                            toastr.error(r.status.message)
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText)
                });

            }
            else return;
        });

        return {
            init: function () {
                loadTree();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));

