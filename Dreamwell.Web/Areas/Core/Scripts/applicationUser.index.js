﻿(function ($) {
    'use strict';

    var pagination = $('#pagination');
    var $btnFilter = $("#btn-filter");
    var search = "1";

    var pageFunction = function () {

        pagination.data('twbs-pagination.js', null);

        var templateScript = $("#user-template").html();
        var template = Handlebars.compile(templateScript);
        //pagination.niftyOverlay({
        //    iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
        //});

        async function getFileData(filemasterid, datagender) {
            return $.ajax({
                url: $.helper.resolveApi("~Core/GetFile/" + filemasterid + "/detailImageFile"),
                type: 'GET',
                contentType: "application/json",
                success: function (r) {
                    console.log("get image file list data")
                    console.log(r);
                    var urlimageprofile = $("#urlimageprofile").val();
                    if(r.data != null){
                        $(".imgpp-"+filemasterid).attr("src",urlimageprofile+r.data.filepath);
                    }else{
                        $(".imgpp-"+filemasterid).attr("src",urlimageprofile+'user.jpg');
                    }
                },
                error: function (r) {

                },
            });

        }

        var pagePlugin = function (options) {
            var o = $('#page-user-content');
            var dataSource = {
                pageSize: 8,
                page: 1,
                filter: {
                    logic: "OR",
                    filters: [
                        {
                            field: "app_fullname",
                            operator: "contains",
                            value: options.filterByName
                        }
                    ]
                }
            }
            options.dataSource = $.extend({}, dataSource, options.dataSource);
            var load = function (opt) {
                //pagination.niftyOverlay('show');
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: opt.url,
                    data: JSON.stringify(opt.dataSource),
                    success: function (data) {

                        console.log("data user ");
                        console.log(data);                        
                        if (data.totalPages > 0) {
                            pagination.twbsPagination({
                                initiateStartPageClick: false,
                                totalPages: data.totalPages,
                                startPage: data.currentPage,
                                onPageClick: function (event, page) {
                                    opt.dataSource.page = page;
                                    load(opt);
                                }
                            });
                        }
                        if (data.items.length > 0) {
                            console.log(data);  
                            console.log(o);
                            o.html(template(data));
                            $('#page-user-content .row-delete').click(function () {
                                var e = $(this);
                                console.log(e.closest('.marvel-item').data('id'));

                                var b = bootbox.confirm({
                                    message: "<p class='text-semibold text-main'>Are you sure ?</p><p>You won't be able to revert this!</p>",
                                    buttons: {
                                        confirm: {
                                            className: "btn-danger",
                                            label: "Confirm"
                                        }
                                    },
                                    callback: function (result) {
                                        if (result) {

                                            $.ajax({
                                                type: "POST",
                                                dataType: 'json',
                                                contentType: 'application/json',
                                                url: $.helper.resolveApi("~/core/ApplicationUser/delete"),
                                                data: JSON.stringify([e.closest('.marvel-item').data('id')]),
                                                success: function (r) {
                                                    if (r.status.success) {
                                                        $.helper.noty.success("Successfully", "Data has been deleted");
                                                        load(opt);
                                                    } else {
                                                        $.helper.noty.error("Information", r.status.message);
                                                    }
                                                    b.modal('hide');
                                                },
                                                error: function (r) {
                                                    $.helper.noty.error(r.status, r.statusText);
                                                    b.modal('hide');
                                                }
                                            });

                                            return false;
                                        }
                                    },
                                    animateIn: 'bounceIn',
                                    animateOut: 'bounceOut'
                                });
                            });

                            $('#page-role-content-no_record').hide();
                        } else {
                            $('#page-role-content-no_record').show();
                        }

                        $btnFilter.button('reset');

                        setTimeout(function() {
                            for (var i = 0; i < data.items.length; i++) {
                                var userD = data.items[i];
                                getFileData(userD.id,"P");
                            }
                        }, 1000);
                        //pagination.niftyOverlay('hide');
                    },
                    error: function (err) {
                        console.error(err);
                        //pagination.niftyOverlay('hide');
                    }
                });
            };
            load(options);
            return {
                refresh: function (opt) {
                    load(opt);
                }
            }
        };

        $("#btn-filter").on('click', function () {
            var filterByName = $("#search-name").val();
            $('#page-user-content').html("");
            $btnFilter.button('loading');
            pagePlugin({
                url: $.helper.resolveApi("~/core/ApplicationUser/dataSource/"),
                totalPages: 2,
                filterByName: filterByName
            });
        })

        return {
            init: function () {
                var filterByName = $("#search-name").val();
                pagePlugin({
                    url: $.helper.resolveApi("~/core/ApplicationUser/dataSource/"),
                    totalPages: 2,
                    filterByName: filterByName
                });
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();

    });
}(jQuery));

