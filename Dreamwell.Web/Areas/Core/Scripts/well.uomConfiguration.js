﻿$(document).ready(function () {
    'use strict';

    var $wellId = $('input[id=wellId]');
    var $form = $("#form-objectUom");

    var $dt = $("#dt_wellUomMap");

    var pageFunction = function () {
        var LookupUom = function (categoryId) {
            $("#uom_id").cmSelect2({
                url: $.helper.resolveApi('~/core/Uom/lookup'),
                result: {
                    id: 'id',
                    text: 'description'
                },
                filters: function (params) {
                    return [
                        {
                            logic: "AND",
                            filters: [{
                                field: "uom_category_id",
                                operator: "eq",
                                value: categoryId,
                            },
                                //{
                                //    field: "unit_alias",
                                //    operator: "contains",
                                //    value: params.term || '',
                                //}
                            ]
                        }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select a Unit of Measurement",
                    allowClear: true,
                    maximumSelectionLength: 1,
                }
            });
        }

        var loadDataTable = function () {
            var url = $.helper.resolveApi('~/core/wellobjectuommap/dataTable/objectMapper') + "?wellId=" + $wellId.val();
            var dt = $dt.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: url,
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        if (d.search && d.search.value) {
                            console.log('Original Search Value:', d.search.value);
                            d.search.value = d.search.value.replace(/%/g, '%25'); // Encode % to %25
                            console.log('Encoded Search Value:', d.search.value);
                        }
                        console.log('Request Data:', d); // Logging request data
                        return JSON.stringify(d);
                    },
                    dataSrc: function (json) {
                        console.log('Response Data:', json); // Logging response data
                        return json.data;
                    }
                },
                columns: [
                    {
                        data: "object_name",
                        class: "pl-2",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `<div class="d-flex align-items-center flex-row">
                                                <div class="fw-500 flex-1 d-flex flex-column cursor-pointer" data-toggle="collapse" data-target="#msg-01 > .js-collapse">
                                                    <div class="fs-lg">
                                                        `+ data + `
                                                        <span class="badge badge-danger">`+ row.uom_code + `</span>
                                                    </div>
                                                    <div class="fs-nano">
                                                        `+ row.uom_category_name + `
                                                    </div>
                                                </div>
                                                <div class="collapsed-reveal">
                                                    <button class="btn-link edit-uom" data-id="`+ row.id + `">
                                                        <span class="fal fa-edit"></span>
                                                    </button>
                                                </div>
                                            </div>`;
                                return output;
                            }
                            return data;
                        },
                    },
                    {
                        data: "uom_code",
                        searchable: true, 
                        visible: false 
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.edit-uom', function () {
                        var wellObjectId = $(this).data("id");
                        $.ajax({
                            type: "GET",
                            dataType: 'json',
                            contentType: 'application/json',
                            url: $.helper.resolveApi("~/core/WellObjectUomMap/" + wellObjectId + "/detail"),
                            success: function (r) {
                                if (r.status.success) {
                                    LookupUom(r.data.uom_category_id);
                                    $("#wellObjectUomId").val(wellObjectId);
                                    $("#well-uom-configuration").show();
                                    $("#object_name").val(r.data.object_name);
                                    $("#category_name").val(r.data.uom_category_name);
                                } else {
                                    toastr.error(r.status.message);
                                }
                            },
                            error: function (r) {
                                toastr.error(r.statusText);
                            }
                        });
                        return false;
                    });

                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
                console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                if (processing) {
                } else {
                }
            });
        }


        $('#btn-saveWellUom').click(function (event) {
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();

                $.post($.helper.resolveApi('~/core/WellObjectUomMap/save'), data, function (r) {
                    if (r.status.success) {
                        loadDataTable();
                        toastr["success"](r.status.message);
                    } else {
                        toastr["error"](r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                });


            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});