﻿$(document).ready(function () {
    'use strict';
    //var $id = $('input[id=well_hole_and_casing_id]');
    var $recordId = $('#drillingId').val();
    var $wellId = $('#wellId').val();
    var $formMulticasing = $("#form-multiCasing");
    var $btnSaveMulticasing = $("#btn_save-multicasing");

   
    var maskingMoney = function () {
        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });
    };

    var getdrillingId = function (recordId) {
        console.log("sasasaswdwd", recordId)
        $.get($.helper.resolveApi('~/core/DrillingHoleAndCasing/' + recordId + '/multicasing'), function (r) {
            var dataCase = r.data[0]
            console.log("kakaka", dataCase.well_id)
            $("#id_well").val(dataCase.well_id);
            $("#well_hole_and_casing_id").val(dataCase.well_hole_and_casing_id);
            $("#multicasing").val(1);
            //$.helper.form.fill($form, r.data);    
        }).fail(function (r) {
            //console.log(r);
        }).done(function () {

        });
    }
    var loadDetail = function () {
    };

    $btnSaveMulticasing.on('click', function () {
        var data = $formMulticasing.serializeToJSON();
        console.log("Form data:", data);

        var isValidate = $formMulticasing[0].checkValidity();
        if (isValidate) {
            if (confirm("Are you sure you want to save and create a Hole and Casing?")) {
                $.post($.helper.resolveApi('~/core/DrillingHoleAndCasing/savecasing'), data, function (r) {
                    if (r.status.success) {
                        window.location.reload();
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }
                }, 'json').fail(function (r) {
                    toastr.error(r.statusText);
                });
            }
        } else {
            $form.addClass('was-validated');
            toastr.error("Please complete all form Hole and Casing fields.");
        }
    });

    var pageFunction = function () {
        return {
            init: function () {
                //casinglookup(1); // Assuming the ID is 1 for the first field
                maskingMoney();
                getdrillingId($recordId);
                loadDetail();

            }
        };
    }();

    pageFunction.init();
});
