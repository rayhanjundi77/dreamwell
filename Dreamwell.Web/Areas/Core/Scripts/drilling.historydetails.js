﻿$(document).ready(function () {
    'use strict';
    var $dt_basic = $("#dt_basic");
    var $recordId = $("#recordId");

    var pageFunction = function () {
        var loadDetails = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/drilling/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $("#well_name").text(r.data.well_name);
                        $("#drilling_date").text(moment(r.data.drilling_date).format('MMM DD, YYYY'));
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        }

        var loadDatables = function () {
            console.log($.helper.resolveApi('~/core/approvalhistory/getApprovalHistory/' + $recordId.val()));
            var dt = $dt_basic.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi('~/core/approvalhistory/getApprovalHistory/' + $recordId.val()),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }

                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            console.log(row);
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }, width: "1px"
                    },
                    {
                        data: "created_on",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            return row.description;
                        }
                    },
                    {
                        data: "created_on",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            return moment(row.created_on).format('MMM DD, YYYY');
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center pl-3 pr-3",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.approval_level == 0 && row.status == 200)
                                    return `<span class="badge badge-success p-1 pr-3 pl-3">Submitted</span>`;
                                else if (row.approval_level == 1 && row.status == 200)
                                    return `<span class="badge badge-success p-1 pr-3 pl-3">Approved by Engineer</span>`;
                                else if (row.approval_level == 1 && row.status == 3)
                                    return `<span class="badge badge-danger p-1 pr-3 pl-3">Rejected by Engineer</span>`;
                                else if (row.approval_level == 2 && row.status == 200)
                                    return `<span class="badge badge-success p-1 pr-3 pl-3">Approved by Manager</span>`;
                                else if (row.approval_level == 2 && row.status == 3)
                                    return `<span class="badge badge-danger p-1 pr-3 pl-3">Rejected by Manager</span>`;
                                else
                                    return `<span class="badge badge-danger p-1 pr-3 pl-3">[ Level: ` + row.approval_level + ` ] [ Status: ` + row.approval_status + ` ]</span>`;
                            }
                            return data;
                        },
                        width: "1px"
                    }
                ],
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                if (processing) {
                } else {
                }
            })
        }

        return {
            init: function () {
                loadDetails();
                loadDatables();
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });


});