﻿$(document).ready(function () {
    'use strict';
    var $drillingId = $('input[id=drillingId]');
    var $afeId = $('input[id=afeId]');
    var $materialId = $('input[id=materialId]');
    var $dt_ManageMaterial = $("#dt_ManageMaterial");

    var pageFunction = function () {
        $.fn.editable.defaults.url = "/post";
        $.fn.editable.defaults.mode = 'inline';

        var masking = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            $(".numeric").inputmask({
                digits: 0,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

        var initEditableMaterialUsing = function () {
            $(".editable-material-using").editable({
                url: $.helper.resolveApi("~/core/dailycost/save"),
                ajaxOptions: { contentType: 'application/json', dataType: 'json' },
                params: function (params) {
                    var amount = params.value;//.replace(/\./g, '');//.replace(/,/g, "");
                    var req = {
                        id: $("#afecontractid-" + $(this).data('afecontractid')).val(),
                        afe_id: $afeId.val(),
                        afe_contract_id: $(this).data('afecontractid'),
                        drilling_id: $drillingId.val(),
                        material_id: $materialId.val(),
                        unit: amount,
                    };
                    console.log(req);
                    return JSON.stringify(req);
                },
                type: "number",
                step: 'any',
                pk: 1,
                name: "amount",
                title: "Enter Program Budget",
                display: function (value) {
                    $("#col-using").attr("width", "100px;");
                    //var numeric = value.replace(/\./g, '');//.replace(/,/g, "");
                    //$(this).text(thousandSeparatorDecimal(value));
                },
                validate: function (value) {
                    var myStr = value.replace(/\./g, '');//.replace(/,/g, "");
                    if ($.isNumeric(myStr) == '') {
                        return 'Only numbers are allowed';
                    }
                },
                success: function (r) {
                    console.log("Daily Cost");
                    console.log(r);
                    if (r.status.success) {
                        $("#afecontractid-" + $(this).data('afecontractid')).val(r.data.recordId);
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                        return "";
                    }
                }
            }).on('shown', function (e, editable) {
                autoNumericEditable(editable);
            });
        }

        var loadDetail = function () {
            if ($drillingId.val() !== '') {
                $.get($.helper.resolveApi('~/core/drilling/' + $drillingId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        console.log('--- Load Detail --- ');
                        console.log(r);
                        //$afeId = r.data.drilling.afe_id;
                        //-- Call Datatatble

                        loadDataTable();
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };

        var loadDataTable = function () {
            var url = $.helper.resolveApi('~/core/dailycost/GetManageUsageDetail/' + $afeId.val() + '/' + $drillingId.val() + '/' + $materialId.val());
            var dt = $dt_ManageMaterial.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: url,
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "contract_no",
                        orderable: true,
                        searchable: true,
                        class: "text-left align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output = `  <span class="fw-700">` + row.material_name + `</span>
                                                <br />
                                                <span class="text-success">Contract No: `+ row.contract_no + `</span>`;
                                return output;
                            }
                            return data;
                        }
                    },
                    {
                        data: "afe_line_description",
                        orderable: true,
                        searchable: true,
                        class: "text-left align-middle pl-2",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return row.afe_line_description;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-center align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.unit == null) {
                                    return "N/A";
                                } else {
                                    return thousandSeparatorWithoutComma(Math.round(row.unit * 100) / 100);
                                }
                            }
                            return data;
                        }
                    },
                    //{
                    //    data: "id",
                    //    orderable: true,
                    //    searchable: true,
                    //    class: "text-right align-middle",
                    //    render: function (data, type, row) {
                    //        if (type === 'display') {
                    //            if (row.unit_price == null) {
                    //                return "N/A";
                    //            } else {
                    //                return row.currency_code + " " + thousandSeparatorWithoutComma(row.unit_price);
                    //            }
                    //        }
                    //        return data;
                    //    }
                    //},
                    //{
                    //    data: "id",
                    //    orderable: true,
                    //    searchable: true,
                    //    class: "text-right align-middle",
                    //    render: function (data, type, row) {
                    //        if (type === 'display') {
                    //            if (row.total_price == null) {
                    //                return "N/A";
                    //            } else {
                    //                return row.currency_code + " " + thousandSeparatorWithoutComma(row.total_price);
                    //            }
                    //        }
                    //        return data;
                    //    }
                    //},
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-right align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return "USD " + thousandSeparatorFromValueWithComma(Math.round(row.actual_price * 100) / 100);
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-center fw-700 text-success align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return Math.round(row.remaining_unit * 100) / 100;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center fw-700 align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var usage = 0;
                                var daily_cost_id = row.id == null ? "" : row.id;
                                if (row.daily_unit != null)
                                    usage = row.daily_unit;

                                var html = `<input type="hidden" id="afecontractid-` + row.afe_contract_id + `" value="` + daily_cost_id + `" />`;
                                html += `<a class="editable-material-using text-danger numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" data-dailycostid="` + daily_cost_id + `" data-afecontractid="` + row.afe_contract_id + `" data-materialid="` + row.material_id + `" data-value="` + usage + `" data-title="Enter Amount">` + thousandSeparatorDecimal(Math.round(usage * 100) / 100) + `</a>`;
                                return html;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center fw-700 align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return ``;
                            }
                            return data;
                        }
                    },
                ],
                initComplete: function (settings, json) {
                    initEditableMaterialUsing();
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});