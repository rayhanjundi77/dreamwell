﻿(function ($) {
    'use strict';
    var $dtBasic = $('#dt_list');
    var $pageLoading = $('#page-loading-content');

    var pageFunction = function () {

        async function getFileData(filemasterid, bhaid) {
            return $.ajax({
                url: $.helper.resolveApi("~Core/GetFile/" + filemasterid + "/detailImageFileById"),
                type: 'GET',
                contentType: "application/json",
                success: function (r) {
                    //console.log("get image file list data")
                    //console.log(r);
                    var urlimageprofile = $.helper.resolve("/UploadBHAPath/");
                    if(r.data != null){
                        $(".imgpp-"+filemasterid).attr("src",urlimageprofile+r.data.filepath);
                    }else{
                        $(".imgpp-"+filemasterid).attr("src",urlimageprofile+'default.jpg');
                    }
                },
                error: function (r) {

                },
            });

        }

        var loadDataTable = function () {
            var dt = $dtBasic.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/bhacomponent/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    //{
                    //    data: "id",
                    //    orderable: false,
                    //    searchable: false,
                    //    class: "text-center",
                    //    render: function (data, type, row, meta) {
                    //        return meta.row + meta.settings._iDisplayStart + 1;
                    //    }
                    //},
                    //{ data: "component_name" },
                    {
                        data: "component_name",
                        render: function (data, type, row) {
                            //alert(row.filemaster_id);
                            //console.log(row.filemaster_id);
                            if (type === 'display') {
                                var output;
                                getFileData(row.image_id, row.id)
                                output = `<div class="d-flex align-items-center">
                                                        <img class="imgpp-`+row.image_id+`" src="#" class="mr-3" style="width:50px">
                                                        <h1 class="fw-300 m-0 l-h-n">
                                                            <span class="text-contrast">`+ data + `</span>
                                                        </h1>
                                                    </div>`;

                                return output;
                            }
                            return data;
                        },
                        width: "40%"
                    },

                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output =
                                    `<div class ="btn-group" data-id= "` + row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/core/bhacomponent/detail?id=") + row.id + `" class="modalTrigger btn btn-primary btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-xs" role="status" aria-hidden="true"></span>' data-target="#bhaComponentModal">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-warning btn-xs btn-info waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/bhacomponent/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dtBasic.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 