﻿$(document).ready(function () {
    'use strict';

    var $recordId = $('input[id=recordId]');
    var $form = $("#form-Stone")

    var pageFunction = function () {

        // FORM VALIDATION FEEDBACK ICONS
        // =================================================================
        var faIcon = {
            valid: 'fal fa-check-circle fa-lg text-success',
            invalid: 'fal fa-times-circle fa-lg',
            validating: 'fal fa-refresh'
        };
        // FORM VALIDATION
        // =================================================================
        $form.bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: faIcon,
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'The Stone Name is required.'
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            // Remove the has-success class
            $parent.removeClass('has-success');
        });
    // END FORM VALIDATION


        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/Stone/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    console.log(r);
                }).done(function () {

                });
            }

        };



        //$('#btn-save').click(function (event) {
        //    var btn = $(this);
        //    var isvalidate = $form[0].checkValidity();
        //    if (isvalidate) {
        //        event.preventDefault();
        //        btn.button('loading');
        //        var data = $form.serializeToJSON();

        //        $.post($.helper.resolveApi('~/core/DrillingContractor/save'), data, function (r) {
        //            if (r.status.success) {
        //                $('input[name=id]').val(r.data.recordId);
        //                toastr["info"](r.status.message);
        //            } else {
        //                toastr["error"](r.status.message);
        //            }
        //            btn.button('reset');
        //        }, 'json').fail(function (r) {
        //            btn.button('reset');
        //        });


        //    } else {
        //        event.preventDefault();
        //        event.stopPropagation();
        //    }
        //    $form.addClass('was-validated');
        //});

        $('#files').on("change", function () {
            console.log("test upload bha " + $recordId.val());
            upload($recordId.val())
        });

        function upload(stoneId) {
            var formData = new FormData();
            var file = $('#files')[0];
            console.log(file.files[0]);
            formData.append('file', file.files[0]);

         //   console.log(formData);

            $.ajax({
                url: $.helper.resolveApi("~/Core/Stone/upload?stoneId=" + stoneId),
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (r) {
                    $("#image").val(r.status.message)
                    // if (r.status.success) {
                    //     toastr.success("Info", "Success to upload");
                    // }
                },
                error: function () {
                    toastr.error("Warning", "Failed to upload");
                }
            });
            return;
        }



        $('#btn-save').on('click', function () {
           
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/Stone/save'), data, function (r) {

                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        upload(r.data.recordId);
                        toastr.success(r.status.message);
                        
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });

            }
            else return;
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});