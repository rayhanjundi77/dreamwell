﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formIadc")

    //-- Lookup
    $("#type").val('');
    $("#type").select2({
        dropdownParent: $form,
        placeholder: "Select a PT / NPT",
        allowClear: true,
        maximumSelectionLength: 1,
    });

    $("#category").val('');
    $("#category").select2({
        dropdownParent: $form,
        placeholder: "Select a Category",
        allowClear: true,
        maximumSelectionLength: 1,
    });

    $('#btn-iadc-parent-lookup').click(function () {
        var btn = $(this);
        btn.button('loading');
        jQuery.ajax({
            type: 'POST',
            url: '/core/Iadc/Lookup',
            success: function (data) {
                btn.button('reset');
                var $box = bootbox.dialog({
                    message: data,
                    title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                    callback: function (e) {
                        console.log(e);
                    }
                });
                $box.on("onSelected", function (o, event) {
                    $box.modal('hide');
                    console.log(event.node.id);
                    console.log(event.node.text);
                    $("#parent_id").val(event.node.id);
                    $("#btn-iadc-parent-lookup").text(event.node.text);
                });
            }
        });
    });

    $("#isSub").change(function () {
        if (this.checked) {
            //Do stuff
            $("#parent-group").show();
        } else {
            $("#parent-group").hide();
            $("#parent_id").val("");
            $("#btn-iadc-parent-lookup").text("Click to choose IADC Parent");
        }
    });

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/iadc/' + $recordId.val() + '/detail'), function (r) {
                    $("#isSub").removeAttr("checked");
                    $("#parent-group").hide();
                    $("#btn-iadc-parent-lookup").text("Click to choose IADC Parent");
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                        if (r.data.parent_id != null) {
                            $("#isSub").attr("checked", "checked");
                            $("#parent-group").show();
                            $("#btn-iadc-parent-lookup").text(r.data.parent_code + " [" + r.data.parent_description + "]");
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };

        $('#btn-save').click(function (event) {
            var $dtIadc = $('#dt_listIadc');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                console.log(data);
                $.post($.helper.resolveApi('~/core/Iadc/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                        $dtIadc.DataTable().ajax.reload();
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});