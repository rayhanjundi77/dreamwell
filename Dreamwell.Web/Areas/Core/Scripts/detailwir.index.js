﻿$(document).ready(function () {
    'use strict';

    var bussinessUnitID = $('input[id=PrimaryBusinessUnitId]');
    var $recordId = $('input[id=recordId]');
    var $wellId = $('input[id=well_id]');


    var $form = $('#form-wir');
    var $dtAttach = $('#dt_Attachments');

    $('#form-wir, #formWorkload, #formRatio, #formFilled, #form-estimation, #form-potential').hide();

    var $formWirWorkload = $("#formWorkload");
    var $formWIRFilled = $("#formFilled");




    var $btnSaveWIR = $("#btn-saveGeneralWIR");
    //var $btnSaveWorkload = $("#btn-save-workload");
    var $btnSaveFill = $("#btn-save-filled");




    //$btnSaveWorkload.on('click', function () {
    //    saveWorkload();
    //});


    $('#well_color').on('change', function () {
        const selectedOption = $(this).find('option:selected');
        const bgColor = selectedOption.data('bg');

        $(this).css('background-color', bgColor);
    });

    $('.summernote').summernote({
        placeholder: 'Enter text here...',
        tabsize: 2,
        height: 200,
        callbacks: {
            onImageUpload: function (files) {
                console.log('Uploading file:', files[0]);
            }
        }
    });

    $('#well_id').cmSelect2({
        url: $.helper.resolveApi('~/core/well/lookup'),
        result: {
            id: 'id',
            text: 'well_name'
        },
        filters: function (params) {
            return [{
                field: "well_name",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            placeholder: "Select a Well",
            allowClear: true,
            maximumSelectionLength: 1,
        }
    });

    // Event handler untuk well_id
    $('#well_id').on('select2:select', function (e) {
        const wellId = e.params.data.id; // Ambil well_id yang dipilih
        const wellName = e.params.data.text; // Ambil nama well yang dipilih
        console.log("Selected Well ID:", wellId);
        console.log("Selected Well Name:", wellName);

        $('#selected-well-id').text(`(${wellId})`);
        $('#well_name').val(wellName);

        if (wellId) {
            $('#well_name').val(wellName);
            $('#form-wir, #formWorkload, #formRatio, #formFilled, #form-estimation, #form-potential').fadeIn();
        } else {
            $('#well_name').val('');
            $('#form-wir, #formWorkload, #formRatio, #formFilled, #form-estimation, #form-potential').fadeOut();
            $('#tableWIRWorkload tbody').empty();
        }
    });

    var $field_id = "";
    $("#field_id").cmSelect2({
        url: $.helper.resolveApi('~/core/BusinessUnitField/lookup?bussinessUnitID=' + bussinessUnitID),
        result: {
            id: 'field_id',
            text: 'field_name'
        },
        filters: function (params) {
            console.log("Filter params:", params);
            return [{
                field: "field_name",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            placeholder: "Select a field",
            allowClear: true,
            maximumSelectionLength: 1,
        }
    });

    $("#field_id").on('select2:select', function (e) {
        var fieldId = e.params.data.id;  // Ambil field_id yang dipilih
        var fieldName = e.params.data.text;  // Ambil field_name yang dipilih

        console.log("Selected Field ID: ", fieldId);
        console.log("Selected Field Name: ", fieldName);

        // Menyinkronkan nilai field_name di form
        $('#field_id').val(fieldId).trigger('change');  // Update field_id dan trigger perubahan
    });


    $('#btn-saveGeneralWIR').on('click', function (e) {
        e.preventDefault();

        // Validasi form
        if (!$('#form-wir')[0].checkValidity()) {
            $('#form-wir')[0].reportValidity();
            return;
        }

        // Serialisasi data form
        const formData = {
            id: $('#recordId').val(),
            wir_no: $('#wir_no').val(),
            originator: $('#originator').val(),
            issues_date: $('#issues_date').val(),
            field_id: $('#field_id').val(),  // Ambil field_id yang terpilih
            well_id: $('#well_id').val(),  // Ambil well_id yang terpilih
            well_name: $('#well_name').val(),  // Ambil well_name yang terpilih
            well_color: $('#well_color').val(),
            priority: $('#priority').val(),
            main_job: $('#main_job').val(),
            budget_ass: $('#budget_ass').val(),
            request_status: $('#request_status').val(),
            type_operation: $('#type_operation').summernote('code'),
            present_status: $('#present_status').summernote('code'),
            main_lines: $('#main_lines').summernote('code'),
            reservoir_information: $('#reservoir_information').summernote('code'),
        };

        // Tambahkan indikator loading
        $('#btn-saveGeneralWIR').prop('disabled', true).text('Saving...');

        // Kirim data melalui AJAX
        $.ajax({
            url: $.helper.resolveApi('~/core/wir/save'),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function (response) {
                toastr.success('Well Intervention Request saved successfully!');

                // Clear form fields after successful save
                $('#form-wir')[0].reset(); // Reset all form fields
                $('#form-wir, #formWorkload, #formRatio, #formFilled, #form-estimation, #form-potential').fadeOut(); // Hide forms again
                $('#well_id').val('').trigger('change'); // Clear the Well ID selection and trigger change
                $('#well_name').val(''); // Clear Well Name input
                $('#selected-well-id').text(''); // Clear selected well ID display

                // Clear summernote editors
                $('#type_operation').summernote('code', '');
                $('#present_status').summernote('code', '');
                $('#main_lines').summernote('code', '');
                $('#reservoir_information').summernote('code', '');

                // Reset row count in the workload table and clear rows
                $('#tableWIRWorkload tbody').empty();
                rowCount = 1;

                // Enable the save button again and reset text
                $('#btn-saveGeneralWIR').prop('disabled', false).text('Save');
            },
            error: function (xhr, status, error) {
                alert('An error occurred while saving the record: ' + error);
                $('#btn-saveGeneralWIR').prop('disabled', false).text('Save');
            }
        });
    });

    function getWellData(wellId) {
        // Pastikan ID well tidak kosong
        if (!wellId) {
            alert('Well ID is required');
            return;
        }

        // Kirim request GET untuk mengambil data well
        $.ajax({
            url: $.helper.resolveApi('~/core/wir/listByWellId/' + wellId),
            type: 'GET',
            success: function (response) {
                if (response.status.success && response.data.length > 0) {
                    var wellData = response.data[0];  // Ambil data pertama dari array response

                    // Isi form dengan data yang diambil
                    $('#wir_no').val(wellData.wir_no);
                    $('#originator').val(wellData.originator);
                    $('#issues_date').val(wellData.issues_date);
                    $('#field_id').val(wellData.field_id).trigger('change');  // Pastikan field_id diupdate dengan benar
                    $('#well_color').val(wellData.well_color);
                    $('#priority').val(wellData.priority);
                    $('#main_job').val(wellData.main_job);
                    $('#budget_ass').val(wellData.budget_ass);
                    $('#request_status').val(wellData.request_status);

                    // Summernote fields
                    $('#type_operation').summernote('code', wellData.type_operation);
                    $('#present_status').summernote('code', wellData.present_status);
                    $('#main_lines').summernote('code', wellData.main_lines);
                    $('#reservoir_information').summernote('code', wellData.reservoir_information);
                } else {
                    alert(' aa');
                }
            },
            error: function (xhr, status, error) {
                console.log(status)
                alert('An error occurred while fetching data: ' + error);
            }
        });
    }



    // Example file upload handler (if needed)
    function uploadFile(file, editor) {
        // Logic to handle image/file upload
        console.log('Uploading file:', file);
    }

    $('#issues_date').datepicker({
        format: 'mm/dd/yyyy',
        autoclose: true,
        todayHighlight: true,
        language: 'en'
    });
});
