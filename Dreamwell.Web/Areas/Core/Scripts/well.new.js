﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $bussinessunitID = $('input[id=bussiness_unit_id]');
    var $form = $("#formNewWell")
    var $test = 0


    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    }

    //-- L

    var pageFunction = function () {
        var wellTypeLookup = function () {
            $("#well_status").val('');
            $("#well_status").select2({
                dropdownParent: $form,
                placeholder: "Select a Project Type",
                allowClear: true,
            });

            $("select[id='well_status']").on('change', function () {
                $("#parent_well").val('');
                $("#parent_well").trigger('change');
                loadAPH(['']);
                loadFields([''], '0');
                if ($(this).val() == 0) {
                    $(".row-parent").show();
                    // $("#parent_well").val('');
                    setTimeout(function() {
                        $(".row-parent").find('.textbox.easyui-fluid.combo').css('width','100%');
                        $(".row-parent").find('.textbox-text.validatebox-text').css('width','100%');
                    }, 200);
                } else {
                    $(".row-parent").hide();
                    //$("#parent_well").val('');
                }
                loadWell($(this).val());
            });
        }

        var wellTypesLookup = function () {
            $("#well_type").val('');
            $("#well_type").select2({
                dropdownParent: $form,
                placeholder: "Select a Project Type",
                allowClear: true,
            });

           
        }

        var loadWell = function (status) {
            if (status != null) {
                if (status == 0) {
                    $("#parent_well").prop('disabled', 'disabled');
                } else {
                    $("#parent_well").removeAttr('disabled');
                    $("#parent_well").cmSelect2({
                        url: $.helper.resolveApi('~/core/well/lookup'),
                        result: {
                            id: 'id',
                            text: 'well_name'
                        },
                        filters: function (params) {
                            console.log(params);
                            return [{
                                field: "well_name",
                                operator: "contains",
                                value: params.term || '',
                            }];
                        },
                        options: {
                            destroy: true,
                            dropdownParent: $form,
                            placeholder: "Select a Well",
                            allowClear: true,
                        }
                    });

                    $("#parent_well").on('select2:select', function (e) {
                        var id = e.target.value;
                        getAphField(id);
                    });
                }
            }
        }

        var loadAPH = function (initValue) {
            console.log("try to load region ");
            console.log("nilai regions id ", initValue);
            easyloader.load('combotree', function () {        // load the specified module
                $('#business_unit_id').combotree({
                    textField: 'text',
                    value: initValue,
                    loader: function (param, success, error) {
                        $.ajax({
                            type: "POST",
                            url: $.helper.resolveApi("~/core/BusinessUnit/combotree"),
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({}),
                            dataType: 'json',
                            success: function (response) {
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err');
                                error.apply(this, arguments);
                            }
                        });
                    },
                    onSelect: function (node) {
                        console.log("Node:: ");
                        console.log(node);
                        loadFields([''], node.id);
                        //loadField(node.id, []);
                    }
                });
            })
        }
        loadAPH(['']);

        var loadFields = function (initValue, businessUnitId) {
            easyloader.load('combotree', function () {        // load the specified module
                $('#field_id').combotree({
                    textField: 'text',
                    value: initValue,
                    loader: function (param, success, error) {
                        $.ajax({
                            type: "POST",
                            url: $.helper.resolveApi("~/core/Asset/GetFieldNode?countryId=&businessUnitId=" + businessUnitId),
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({}),
                            dataType: 'json',
                            success: function (response) {
                                console.log(response);
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err');
                                error.apply(this, arguments);
                            }
                        });
                    },
                    onSelect: function (node) {
                        console.log("Node:: ");
                        console.log(node);
                        //loadField(node.id, []);
                    }
                });
            })
        }
        loadAPH(['']);

        var getAphField = function (id) {
            if (id != null) {
                $.get($.helper.resolveApi('~/core/well/' + id + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#business_unit_id').combotree({ value: r.data.business_unit_id });
                        $('#field_id').combotree({ value: r.data.field_id });
                    }
                });
            }
        }

        $('#btn-save').click(function (event) {
            var data = $form.serializeToJSON();
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                Swal.fire(
                    {
                        title: "",
                        text: "Are you sure want to create a new Well?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes"
                    }).then(function (result) {
                        if (result.value) {
                            btn.button('loading');
                            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
                            $.post($.helper.resolveApi('~/core/well/new'), data, function (r) {
                                if (r.status.success) {
                                    $('#formNewWell').modal('hide');
                                    window.location = $.helper.resolve("/core/well/detail?id=") + r.data.recordId;
                                } else {
                                    toastr.error(r.status.message)
                                }
                                $('#loadingModal').modal('hide');
                                btn.button('reset');
                            }, 'json').fail(function (r) {
                                btn.button('reset');
                                toastr.error(r.statusText);
                            });
                        }
                    });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });
        return {
            init: function () {
                wellTypeLookup();
                wellTypesLookup();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});