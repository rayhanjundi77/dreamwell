﻿(function ($) {
    'use strict';

    var $tree = $('#applicationMenu-tree');
    var $form = $('#form-applicationMenuCategory');
    var $recordId = $('input[id=recordId]');
    var $applicationMenuFormModal = $('#businessUnitModal');
    var categoryID = $('.selected-menu-category').data('selected');
    var $appModule = $('#application_module');
    var $btnAdd = $('#btnAdd');
    var pageFunction = function () {


        $appModule.select2({
            dropdownAutoWidth: true,
            minimumResultsForSearch: -1,
            placeholder: "Choose module",
            ajax: {
                url: $.helper.resolveApi("~/core/ApplicationModule/GetData"),
                type: "GET",
                delay: 250,
                data: function (params) {
                    return {
                        moduleName: "" // search term
                    };
                },
                processResults: function (data) {
                    var result = { results: [], more: false };
                    if (data && data.data) {

                        $.each(data.data, function () {
                            result.results.push({ id: this.id, text: this.module_name });
                        });
                    };
                    return result;

                    //return {
                    //    results: response.data
                    //};
                },
                cache: true
            }
        }).on('change', function () {
            $btnAdd.removeAttr('disabled');
            $btnAdd
                .removeAttr('data-href')
                .attr('data-href', $.helper.resolve('~core/applicationmenu/managedetail?id=&categoryId=' + categoryID + '&moduleId=' + $(this).val()));
            loadTree(categoryID);
        });


        var loadTree = function (category) {
            $tree.jstree("destroy").empty();
            $tree.jstree({
                contextmenu: {
                    select_node: false,
                    items: customMenu
                },
                core: {
                    themes: {
                        "responsive": false
                    },
                    check_callback: true,
                    data: function (obj, callback) {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: $.helper.resolveApi("~/core/ApplicationMenu/GetTree?categoryId=" + category +"&moduleId="+$appModule.val()),
                            success: function (data) {
                                callback.call(this, data);
                            }
                        });
                    }
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "root": {
                        "class": "css-custom"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                },
                state: { "key": "id" },
                plugins: ["dnd", "state", "types", "contextmenu"]


            }).bind("move_node.jstree rename_node.jstree", function (e, data) {
                if (e.type == "move_node") {
                    $.get($.helper.resolveApi("~/core/ApplicationMenu/order"),
                        {
                            recordId: data.node.id,
                            parentTargetId: data.parent,
                            newOrder: data.old_position < data.position ? data.position + 1 : data.position
                        }, function (data) {
                            if (data.status.success) {
                                $tree.jstree(true).refresh();
                            }
                        });
                }
            });
        }

        function customMenu(node) {
            // The default set of all items
            var items = {
                Create: {
                    label: "Create",
                    icon: "fa fa-plus-square-o",
                    action: function (n) {
                        $(this).modalDialog.modal({
                            modalPath: $.helper.resolve("~core/applicationmenu/managedetail?id=&categoryId=" + categoryID + "&=moduleId="+$appModule.val()+"&parentId=" + node.id),
                            modalTarget: "#applicationMenuManageModal",
                            modalToggle: "modal-remote",
                            show: true
                        });
                    }
                },
                Edit: {
                    label: "Edit",
                    icon: "fa fa-pencil-square-o",
                    action: function () {

                        $(this).modalDialog.modal({
                            modalPath: $.helper.resolve("~core/applicationmenu/managedetail?id=" + node.id + "&categoryId=" + categoryID),
                            modalTarget: "#applicationMenuManageModal",
                            modalToggle: "modal-remote",
                            show: true
                        });

                        //$applicationMenuFormModal.modal('show'),47d03a74-4920-0671-756c-4ca62b170964
                        //    $.helper.form.clear($form),
                        //    $.get($.helper.resolveApi('~/core/ApplicationMenu/' + node.id + '/detail'), function (r) {
                        //        console.log(r);
                        //        if (r.status.success) {
                        //            $.helper.form.fill($form, r.data);
                        //        }
                        //    }).fail(function (r) {
                        //        $.helper.noty.error(r.status, r.statusText);
                        //    });
                    }
                },
                Delete: {
                    label: "Delete",
                    icon: "fa fa-trash-o",
                    action: function () {

                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are you sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    className: "btn-danger",
                                    label: "Confirm"
                                }
                            },
                            callback: function (result) {
                                if (result) {

                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/applicationmenu/delete"),
                                        data: JSON.stringify([node.id]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.success("Data has been deleted");
                                                $tree.jstree("refresh");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            b.modal('hide');
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });

                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });

                    }
                }
            };
            return items;
        }

        $(document).on('hidden.bs.modal', "#applicationMenuManageModal", function () {
            $tree.jstree("refresh");
        });

        return {
            init: function () {

            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));

