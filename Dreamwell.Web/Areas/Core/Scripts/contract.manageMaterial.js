﻿$(document).ready(function () {
    'use strict';
    var $materialId = $('input[id=materialId]');;
    var $afeId = $('input[id=afeId]');
    var $isNewPurchase = $('input[id=isNewPurchase]');
    var $dt_ManageMaterial = $("#dt_ManageMaterial");
    var $form = $("#formContractDetail")


    var pageFunction = function () {
        $.fn.editable.defaults.url = "/post";
        $.fn.editable.defaults.mode = 'inline';
        var initEditableMaterialUsing = function () {
            $(".editable-material-using").editable({
                url: $.helper.resolveApi("~/core/afecontract/save"),
                ajaxOptions: { contentType: 'application/json', dataType: 'json' },
                params: function (params) {
                    var amount = params.value;//.replace(/\./g, '');//.replace(/,/g, "");
                    var req = {
                        id: $(this).data('afecontractid'),
                        afe_id: $afeId.val(),
                        contract_detail_id: $(this).data('contractdetailid'),
                        unit: amount,
                    };
                    console.log(req);
                    return JSON.stringify(req);
                },
                type: "number",
                step: 'any',
                pk: 1,
                name: "amount",
                title: "Enter Program Budget",
                display: function (value) {
                    //var numeric = value.replace(/\./g, '');//.replace(/,/g, "");
                    $(this).text(thousandSeparatorDecimal(value));
                },
                validate: function (value) {
                    var myStr = value.replace(/\./g, '');//.replace(/,/g, "");
                    if ($.isNumeric(myStr) == '') {
                        return 'Only numbers are allowed';
                    } else {
                        if (myStr < 1)
                            return 'Quantity must be greater than 0';
                    }
                },
                success: function (r) {
                    console.log(r);
                    if (r.status.success) {
                        $("#material-remove-" + $(this).data('contractdetailid')).attr("data-afecontractid", r.data.id);
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                        return r.status.message;
                    }
                }
            }).on('shown', function (e, editable) {
                autoNumericEditable(editable);
            });
        }


        var loadDataTable = function () {
            var url = "";
            if ($isNewPurchase.val() == 1)
                url += $.helper.resolveApi('~/core/contractdetail/getListByMaterial/' + $materialId.val() + '/' + $afeId.val()) + '/true';
            else
                url += $.helper.resolveApi('~/core/contractdetail/getListIssuedStock/' + $materialId.val() + '/' + $afeId.val());

            var dt = $dt_ManageMaterial.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: url,
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        console.log(JSON.stringify(d));
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "contract_no",
                        orderable: true,
                        searchable: true,
                        class: "text-left align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                console.log(row);
                                var output = `  <span class="fw-700">` + row.material_name + `</span>
                                                <br />
                                                <span class="text-success">Contract No: `+ row.contract_no +`</span>`;
                                return output;
                            }
                            return data;
                        }
                    },
                    {
                        data: "afe_line_description",
                        orderable: true,
                        searchable: true,
                        class: "text-left align-middle pl-2",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return row.afe_line_description;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-center align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.unit == null) {
                                    return "N/A";
                                } else {
                                    return thousandSeparatorWithoutComma(row.unit);
                                }
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-right align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.unit_price == null) {
                                    return "N/A";
                                } else {
                                    return row.currency_code + " " + thousandSeparatorWithoutComma(row.unit_price);
                                }
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-right align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.total_price == null) {
                                    return "N/A";
                                } else {
                                    return row.currency_code + " " + thousandSeparatorWithoutComma(row.total_price);
                                }
                            }
                            return data;
                        }
                    },
                    //{
                    //    data: "current_rate_value",
                    //    orderable: true,
                    //    searchable: true,
                    //    class: "text-right align-middle",
                    //    render: function (data, type, row) {
                    //        if (type === 'display') {
                    //            return "USD " + thousandSeparatorWithoutComma(row.current_rate_value);
                    //        }
                    //        return data;
                    //    }
                    //},
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-right align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return "USD " + thousandSeparatorWithoutComma(row.actual_price);
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-center fw-700 text-success align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return row.remaining_unit;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center fw-700 align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var usage = 0;
                                if (row.unit_usage != null)
                                    usage = row.unit_usage;
                                var html = `<a class="editable-material-using" id="cd-` + row.id + `" data-afecontractid="` + row.afe_contract_id + `" data-contractdetailid="` + row.id + `" data-title="Enter Amount">` + thousandSeparatorDecimal(usage) + `</a>`;
                                return html;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center fw-700 align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                console.log(row);
                                var html = `<button href="javascript:void(0);" id="material-remove-` + row.id+`" data-contractid="` + row.contract_id + `" data-afecontractid="` + row.afe_contract_id + `" data-contractdetailid="` + row.id +`" type="button" class="row-delete btn btn-warning btn-sm waves-effect waves-themed rounded-0 pl-2 pr-2" style="">
                                                Remove
                                            </button>`;
                                return html;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    initEditableMaterialUsing();

                    $(this).on('click', '.row-delete', function () {
                        var btn = $(this);
                        var afecontractid = $(this).data('afecontractid');
                        var contractdetailid = $(this).data('contractdetailid');
                        var contractid = $(this).data('contractid');
                        Swal.fire({
                            title: "",
                            text: "Are you sure want to remove this item on AFE?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Yes"
                        }).then(function (result) {
                            if (result.value) {

                                if (!afecontractid) {
                                    toastr.error("Material on this contract is not exist on AFE.");
                                    return;
                                } 

                                var url = $.helper.resolveApi("~/core/afecontract/delete/" + afecontractid + "/" + $afeId.val() + "/" + contractid + "/" + contractdetailid);
                                if ($isNewPurchase.val() == 1)
                                    url += "/true";
                                else
                                    url += "/false";

                                btn.button('loading');
                                $.ajax({
                                    type: "POST",
                                    dataType: 'json',
                                    contentType: 'application/json',
                                    url: url,
                                    success: function (r) {
                                        if (r.status.success) {
                                            loadDataTable();
                                            toastr.success(r.status.message);
                                        } else {
                                            toastr.error(r.status.message);
                                        }
                                        btn.button('reset');
                                    },
                                    error: function (r) {
                                        toastr.error(r.statusText);
                                        btn.button('reset');
                                    }
                                });
                                return false;
                            }
                        });
                    })
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        var getListMaterial = function () {
            var templateScript = $("#contractdetail-template").html();
            var template = Handlebars.compile(templateScript);

            var url = $.helper.resolveApi('~/core/contractdetail/getListByMaterial/' + $materialId.val() + '/' + $afeId.val()) + '/';
            if ($isNewPurchase.val() == 1)
                url += "true";
            else
                url += "false";

            $.get(url, function (r) {
                console.log(r);
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#contractdetail > tbody").html(template({ data: r.data }));
                        //$(".contract_details").on('click', function () {
                        //    contractDetails();
                        //});
                        initEditableMaterialUsing();
                    }
                }
                else {
                    toastr.error(r.status.message);
                }
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        Handlebars.registerHelper("printUsingMaterial", function (contractdetailid, afe_contract_id, unit_usage) {
            var html = "";
            //if (!isNaN(unit_usage) && unit_usage > 0) {
            //    html = "<span>USD " + thousandSeparatorDecimal(unit_usage) + "</span>";
            //}
            //else {
            html = `<a class="editable-material-using" id="cd-` + contractdetailid +`" data-afecontractid="` + afe_contract_id + `" data-contractdetailid="` + contractdetailid + `" data-title="Enter Amount">` + thousandSeparatorDecimal(unit_usage)+`</a>`;
            //}
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper("number", function (index) {
            return index + 1;
        });

        return {
            init: function () {
                //getListMaterial();
                loadDataTable();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});