﻿
$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $wellId = $('input[id=well_id]');
    var $operation_data_period = $('input[id=operation_data_period]');
    var $operation_data_early = $('input[id=operation_data_early]');
    var $operation_data_planned = $('input[id=operation_data_planned ]');

    var $afeId = '';
    var $drillingDate = "";
    var $drillingBhaId = $("#drilling_bha_id");
    var $drillingBitId = $("#drilling_bit_id")
    var $formDrillingTop = $("#form-Drilling-top");
    var $formDrillingOperation = $("#formDrillingOperation");
    var $formOperationActivities = $("#formOperationActivities");
    var $formHSSE = $("#formHSSE");
    var $formDrillingTransport = $("#formDrillingTransport");
    var $formDrillingBulk = $("#formBulkItem");
    var $formDrillingDeviation = $("#formDrillingDeviation");
    var $formDrillingBIT = $("#form-drillingBIT");
    var $formDrillingHoleAndCasing = $("#form-drillingHoleAndCasing");
    var $formMudloggingData = $("#formMudloggingData");
    var $formDrillingAerated = $("#formAerated");
    var $form_mud1 = $("#form-mud1");
    var $form_mud2 = $("#form-mud2");
    var $form_mud3 = $("#form-mud3");
    var $formDrillingPit = $("#form-pit");
    var $formChemicalUsed = $("#form-chemicalUsed");
    var $formTotalVolume = $("#form-totalVolume");
    var $formDrillingWeather = $('#formDrillingWeather');
    var $formDrillingPersonel = $("#formDrillingPersonel");

    var $formDrillingBhaBit = $("#form-drillingBhaBit");

    var $planBudgetModal = $("#manageUsageModal");
    var $formManageBuget = $("#afeManageBudget");

    var formActualInformation = $('#formActualInformation');

    var $btnSaveOperationActivities = $("#btn-SaveOperationActivities");
    var $btnSaveDrillingBhaBit = $("#btn-SaveBhaBit");

    var $btnNewBIT = $("#addNewBIT");
    var $btnUpdateBIT = $("#btn-updateBIT");
    var $btnSaveBIT = $("#btn-saveBIT");

    var getLastDayTotalVolume = [];

    var $btnSaveDrilling = $("#btn-save-drilling-top");
    var $btnSaveHSSE = $("#btn-save-hsse");
    var $btnSaveBulk = $("#btn-save-bulk");
    var $btnSaveMudlogging = $("#btn-save-mudlogging");
    var $btnSaveWeather = $("#btn-save-weather");
    var $btnSaveDeviation = $("#btn-save-deviation");
    var $btnSaveTransport = $("#btn-save-transport");
    var $btnSaveBhaBit = $("#btnSaveBhaBit");

    var $btnSaveMud1 = $("#btn-save-mud1");
    var $btnSaveMud2 = $("#btn-save-mud2");
    var $btnSaveMud3 = $("#btn-save-mud3");
    var $btnSaveVolume = $("#btn-save-volume");
    var $bntSavePit = $("#btn-save-pit");
    var $btnSaveChemicals = $("#btn-save-chemicals");

    var $btnSaveAerated = $("#btn-save-aerated");
    var $btnSavePersonel = $("#btn-save-personel");

    var $btnNewHole = $("#btn-NewHoleCasing");

    var $btnSubmit = $("#btn-submit");
    var $btnApprove = $("#btn-Approve");
    var $btnReject = $("#btn-Reject");

    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    }


    $('#form-mud1 :input[name=chloride]').change(function () {

        // console.log("chloride update");

        UpdateRumusMudOne();
    });


    $('#form-mud1 :input[name=mw_out]').change(function () {

        // console.log("mw_out update");

        UpdateRumusMudOne();
    });


    $('#form-mud1 :input[name=water]').change(function () {

        // console.log("water update");

        UpdateRumusMudOne();
    });


    $('#form-mud1 :input[name=oil]').change(function () {

        // console.log("oil update");

        UpdateRumusMudOne();
    });


    $('#form-mud1 :input[name=solids]').change(function () {

        // console.log("solids update");

        UpdateRumusMudOne();
    });


    var UpdateRumusMudOne = function () {
        var chloride = $('#form-mud1 :input[name=chloride]').val();
        var mw_out = $('#form-mud1 :input[name=mw_out]').val();
        var water = $('#form-mud1 :input[name=water]').val();
        var oil = $('#form-mud1 :input[name=oil]').val();
        var solid = $('#form-mud1 :input[name=solids]').val();


        var tempChloride = 0;
        var tempmw_out = 0;
        var tempwater = 0;
        var tempoil = 0;

        var tempsolid = 0;




        if (chloride != null) {
            tempChloride = chloride;
        }


        if (mw_out != null) {
            tempmw_out = mw_out;
        }

        if (water != null) {
            tempwater = water;
        }


        if (oil != null) {
            tempoil = oil;
        }


        if (solid != null) {
            tempsolid = solid;
        }

        //= (7, 5 * (35 - B13) - (2 * H13) - (2, 1 * H14) - (+(0, 13 * B17 * 1, 65 * H13) / 300000))
        var LGS = (7.5 * (35 - tempmw_out) - (2 * tempwater) - (2.1 * tempoil) - (+(0.13 * tempChloride * 1.65 * tempwater) / 300000));


        //hgs = (+(H11 - (+(0, 13 * B17 * 1, 65 * H13) / 300000)) - H16)

        // console.log("nilai nya " + tempsolid + " nilai cholodre " + tempChloride + " nilai tempat wateer" + tempwater);
        var hgs = (+(tempsolid - (+(0.13 * tempChloride * 1.65 * tempwater) / 300000)) - LGS);


        $('#form-mud1 :input[name=lgs]').val(LGS);

        $('#form-mud1 :input[name=hgs]').val(hgs);


    }






    //mud 2
    $('#form-mud2 :input[name=chloride]').change(function () {

        // console.log("chloride mud 2 update");

        UpdateRumusMudDua();
    });


    $('#form-mud2 :input[name=mw_out]').change(function () {

        // console.log("mw_out mud2 update");

        UpdateRumusMudDua();
    });


    $('#form-mud2 :input[name=water]').change(function () {

        // console.log("water mud2 update");

        UpdateRumusMudDua();
    });


    $('#form-mud2 :input[name=oil]').change(function () {

        // console.log("oil mud2 update");

        UpdateRumusMudDua();
    });


    $('#form-mud2 :input[name=solids]').change(function () {

        // console.log("solid mud2 update");

        UpdateRumusMudDua();
    });

    var UpdateRumusMudDua = function () {
        var chloride = $('#form-mud2 :input[name=chloride]').val();
        var mw_out = $('#form-mud2 :input[name=mw_out]').val();
        var water = $('#form-mud2 :input[name=water]').val();
        var oil = $('#form-mud2 :input[name=oil]').val();
        var solid = $('#form-mud2 :input[name=solids]').val();


        var tempChloride = 0;
        var tempmw_out = 0;
        var tempwater = 0;
        var tempoil = 0;

        var tempsolid = 0;




        if (chloride != null) {
            tempChloride = chloride;
        }


        if (mw_out != null) {
            tempmw_out = mw_out;
        }

        if (water != null) {
            tempwater = water;
        }


        if (oil != null) {
            tempoil = oil;
        }


        if (solid != null) {
            tempsolid = solid;
        }


        var LGS = ((7.5 * (35 - tempmw_out)) - (2 * (tempwater) - (2.1 * tempoil))) - (0.13 * tempChloride * 1.65 * tempwater) / 300000;
        var hgs = tempsolid - ((0.13 * tempChloride * 1.65 * tempwater) / 300000) - LGS;

        $('#form-mud2 :input[name=lgs]').val(LGS);

        $('#form-mud2 :input[name=hgs]').val(hgs);


    }



    //mud 3
    $('#form-mud3 :input[name=chloride]').change(function () {

        // console.log("chloride mud3 update");

        UpdateRumusMudTiga();
    });


    $('#form-mud3 :input[name=mw_out]').change(function () {

        // console.log("mw_out mud3 update");

        UpdateRumusMudTiga();
    });


    $('#form-mud3 :input[name=water]').change(function () {

        // console.log("water mud3 update");

        UpdateRumusMudTiga();
    });


    $('#form-mud3 :input[name=oil]').change(function () {

        // console.log("oil mud3 update");

        UpdateRumusMudTiga();
    });


    $('#form-mud3 :input[name=solid]').change(function () {

        // console.log("solid mud3 update");

        UpdateRumusMudTiga();
    });

    var UpdateRumusMudTiga = function () {
        var chloride = $('#form-mud3 :input[name=chloride]').val();
        var mw_out = $('#form-mud3 :input[name=mw_out]').val();
        var water = $('#form-mud3 :input[name=water]').val();
        var oil = $('#form-mud3 :input[name=oil]').val();
        var solid = $('#form-mud3 :input[name=solid]').val();


        var tempChloride = 0;
        var tempmw_out = 0;
        var tempwater = 0;
        var tempoil = 0;

        var tempsolid = 0;




        if (chloride != null) {
            tempChloride = chloride;
        }


        if (mw_out != null) {
            tempmw_out = mw_out;
        }

        if (water != null) {
            tempwater = water;
        }


        if (oil != null) {
            tempoil = oil;
        }


        if (solid != null) {
            tempsolid = solid;
        }


        var LGS = ((7.5 * (35 - tempmw_out)) - (2 * (tempwater) - (2.1 * tempoil))) - (0.13 * tempChloride * 1.65 * tempwater) / 300000;
        var hgs = tempsolid - ((0.13 * tempChloride * 1.65 * tempwater) / 300000) - LGS;

        $('#form-mud3 :input[name=lgs]').val(LGS);

        $('#form-mud3 :input[name=hgs]').val(hgs);


    }

    var lastRowPersonel = 0;
    var lastRowPit = 0;
    var lastRowChemicalUsed = 0;
    var lastBhaNo = 0;
    var lastBitNo = 0;
    var $dailyCost = 0;
    var $cummulativeCost = 0;
    var $dailyMudCost = 0;
    var $cummulativeMudCost = 0;
    var lastRowActualInformation = 0;

    var isNewBHA = false;

    var ObjectMapper = [];

    var iadcLookUp = function () {
        $('.iadc-lookup').click(function () {
            var element = $(this);
            element.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/Iadc/Lookup',
                success: function (data) {
                    element.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                        callback: function (e) {
                            //// console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o, event) {
                        $box.modal('hide');
                        element.closest("tr").children(".activitiesCategory").find("input").val(event.node.original.category);
                        if (event.node.original.type == "1") {
                            element.closest("tr").children(".activitiesPTorNPT").find("input").val("PT");
                        } else if (event.node.original.type == "2") {
                            element.closest("tr").children(".activitiesPTorNPT").find("input").val("NPT");

                        }
                        element.siblings().val(event.node.id);
                        element.html(event.node.text);
                    });
                }
            });
        });
    }

    var mudTypeLookup = function () {
        $("#mud_type1,#mud_type2,#mud_type3").cmSelect2({
            //$(".mud-type").cmSelect2({
            url: $.helper.resolveApi('~/core/MudType/lookup'),
            result: {
                id: 'id',
                text: 'mud_name'
            },
            filters: function (params) {
                return [{
                    field: "mud_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select a Mud Type",
                allowClear: true,
            }
        });
    }

    var jobPersonelLookUp = function (id) {
        var $element;
        if (id == null || id == "") {
            $element = $(".lookup-personel");
        } else {
            $element = $("#" + id);
        }
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/JobPersonel/lookup'),
            result: {
                id: 'id',
                text: 'job_title'
            },
            filters: function (params) {
                return [{
                    field: "job_title",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: $formDrillingPersonel,
                placeholder: "Select a Job Personel",
                allowClear: true,
                //tags: true,
                //multiple: true,
                maximumSelectionLength: 1,
            }
        });
    }

    /*
    var holeAndCasingLookup = function (id) {
        $("#holeandcasinglookup-" + id).cmSelect2({
            url: $.helper.resolveApi("~/core/DrillingOperationActivity/getHoleandCasing/" + $wellId.val()),
            result: {
                id: 'id',
                text: 'hole_name',
                casing_name: 'casing_name',
                hole_depth: 'hole_depth',
                casing_setting_depth: 'casing_setting_depth'
            },
            filters: function (params) {
                return [{
                    field: "hole_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select Hole and Casing",
                allowClear: true,
                templateResult: function (repo) {

                    return "<b>Hole:</b> " + repo.text + `"` + " <b>Casing:</b> " + repo.casing_name + `"`;
                },
            }
        });
        $("#holeandcasinglookup-" + id).on('select2:select', function (e) {
            $("#hole-diameter-" + id).val(e.params.data.text);
            $("#hole-diameter-" + id).val(e.params.data.text);
            $("#casing-diameter-" + id).val(e.params.data.casing_name);
            $("#hole-target-" + id).val(e.params.data.hole_depth);
        });
    }
    */

    
    var holeAndCasingLookup = function (id) {
        $("#holeandcasinglookup-" + id).cmSelect2({
            url: $.helper.resolveApi("~/core/wellholeandcasing/lookup?wellId=" + $wellId.val()),
            result: {
                id: 'id',
                text: 'hole_name',
                casing_name: 'casing_name',
                hole_depth: 'hole_depth',
                casing_setting_depth: 'casing_setting_depth'
            },
            filters: function (params) {
                return [{
                    field: "hole_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select Hole and Casing",
                allowClear: true,
                templateResult: function (repo) {
                   
                    return "<b>Hole:</b> " + repo.text + `"` + " <b>Casing:</b> " + repo.casing_name + `"`;
                },
            }
        });
        $("#holeandcasinglookup-" + id).on('select2:select', function (e) {
            $("#hole-diameter-" + id).val(e.params.data.text);
            $("#hole-diameter-" + id).val(e.params.data.text);
            $("#casing-diameter-" + id).val(e.params.data.casing_name);
            $("#hole-target-" + id).val(e.params.data.hole_depth);
        });
    }

    

    var holeAndCasingLookupByDrilling = function (id) {
        var $element;
        if (id == null || id == "") {
            $element = $(".holeandcasinglookupByDrilling");
        } else {
            $element = $("#" + id);
        }

        $element.cmSelect2({
            url: $.helper.resolveApi("~/core/wellholeandcasing/lookupHoleCasingByDrilling?drillingId=" + $recordId.val()),
            result: {
                id: 'id',
                text: 'hole_name',
                casing_name: 'casing_name',
                hole_depth: 'hole_depth',
                casing_setting_depth: 'casing_setting_depth'
            },
            filters: function (params) {
                return [{
                    field: "hole_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select Hole and Casing",
                allowClear: true,
                templateResult: function (repo) {
                    // console.log("get repo id " + repo.id);
                    return "<b>Hole:</b> " + repo.text + `"` + " <b>Casing:</b> " + repo.casing_name + `"`;
                },
            }
        });

        $element.on('select2:select', function (e) {
            // console.log("ini adalah targetnya " + $element.attr('id'));
            var idTarget = e.target.value;
            // console.log(e);
            // console.log(idTarget);
            CheckLastDay(id, $element.attr('id').split("-"));
        });



        //  $element.on('select2:select', function (e) {

        //  });
    }

    var CheckLastDay = function (id, elementid) {
        // console.log("check last data " + id + " and element id " + elementid[1]);
        if (getLastDayTotalVolume.length > 0) {

            // console.log("total last day volume " + getLastDayTotalVolume.length);
            var temporarayDataLastVolume = getLastDayTotalVolume[0];


            // console.log("temporary data " + temporarayDataLastVolume.total_volume_start);

            if (temporarayDataLastVolume.length > 0 || temporarayDataLastVolume != null) {

                var getLastData = temporarayDataLastVolume.total_volume_start;
                // console.log("nilai get last day endingnya adalah " + getLastData);

                ;
                // var totalVolumeStart = $('input[name="drillingVolume[' +elementid[1]+'].total_volume_start"]').val();
                $('input[name="drillingVolume[' + elementid[1] + '].total_volume_start"]').val(temporarayDataLastVolume.ending);






            }

        }

        //volume start
        $('input[name="drillingVolume[' + elementid[1] + '].total_volume_start"]').change(function () {

            // console.log("drillingVolume update");

            initTotalVolumeCalculation(elementid);

        });



        //lost update
        $('input[name="drillingVolume[' + elementid[1] + '].lost_surface"]').change(function () {

            // console.log("lost update update");


            initTotalVolumeCalculation(elementid);
        });



        //lost downhole
        $('input[name="drillingVolume[' + elementid[1] + '].lost_downhole"]').change(function () {

            // console.log("lost downhole update");

            initTotalVolumeCalculation(elementid);
        });


        //dumped
        $('input[name="drillingVolume[' + elementid[1] + '].dumped"]').change(function () {

            // console.log("lost dumpet update");


            initTotalVolumeCalculation(elementid);
        });



        //build
        $('input[name="drillingVolume[' + elementid[1] + '].build"]').change(function () {

            // console.log("lost build update");


            initTotalVolumeCalculation(elementid);
        });
    }

    function initTotalVolumeCalculation(elementid) {


        var tempTotal = 0.0;
        var tempLostSurface = 0.0;
        var tempLostDownHole = 0.0;
        var tempDumped = 0.0;
        var tempBuild = 0.0;


        var total = $('input[name="drillingVolume[' + elementid[1] + '].total_volume_start"]').val().trim().replace(",", "");
        var lost_surface = $('input[name="drillingVolume[' + elementid[1] + '].lost_surface"]').val().trim().replace(",", "");
        var lost_downhole = $('input[name="drillingVolume[' + elementid[1] + '].lost_downhole"]').val().trim().replace(",", "");
        var dumped = $('input[name="drillingVolume[' + elementid[1] + '].dumped"]').val().trim().replace(",", "");
        var build = $('input[name="drillingVolume[' + elementid[1] + '].build"]').val().trim().replace(",", "");

        // console.log("before " + total + " before nilai templost surface " + lost_surface + " lost downhole " + lost_downhole + " nilai dumped " + dumped + "nilai build " + build);
        // console.log("before nilai nya temp total : " + tempTotal + " nilai total sekarang" + total);
        if (total.length != 0) {
            // console.log("nilai temp total : " + tempTotal + " nilai total sekarang" + total + " nilai downhole " + lost_downhole);
            tempTotal = total;
        }

        if (lost_surface.length != 0) {
            // console.log("nilai lost surface tidak nol :" + lost_surface);
            tempLostSurface = lost_surface;
        }
        if (lost_downhole.length != 0) {
            tempLostDownHole = lost_downhole;
        }
        if (dumped.length != 0) {
            tempDumped = dumped;
        }
        if (build.length != 0) {
            tempBuild = build;
        }


        // console.log("nila temp total " + tempTotal + " nilai templost surface " + tempLostSurface + " nilai temp lost downhole " + tempLostDownHole + " nilai temp dumped " + tempDumped + "nilai temp build " + tempBuild);
        var tempEnding = parseFloat(tempTotal) - parseFloat(tempLostSurface) - parseFloat(tempLostDownHole) - parseFloat(tempDumped) + parseFloat(tempBuild);

        // console.log("nila temp ending " + tempEnding);
        $('input[name="drillingVolume[' + elementid[1] + '].ending"]').val(tempEnding);

    }
    var pitLookup = function (id) {
        var $element;
        if (id == null || id == "") {
            $element = $(".lookup-pit");
        } else {
            $element = $("#" + id);
        }
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/pit/lookup'),
            result: {
                id: 'id',
                text: 'pit_name'
            },
            filters: function (params) {
                return [{
                    field: "pit_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                //dropdownParent: $formDrillingPit,
                placeholder: "Select a Pit",
                allowClear: true,
                //tags: true,
                //multiple: true,
                maximumSelectionLength: 1,
            }
        });
    }

    //var chemicalUomLookup = function (id) {
    //    var $element;
    //    if (id == null || id == "") {
    //        $element = $(".lookup-chemical_uom");
    //    } else {
    //        $element = $("#" + id);
    //    }
    //    $element.cmSelect2({
    //        url: $.helper.resolveApi('~/core/uom/lookup'),
    //        result: {
    //            id: 'id',
    //            text: 'description',
    //            uom_code: 'uom_code'
    //        },
    //        filters: function (params) {
    //            return [{
    //                logic: "OR",
    //                filters: [
    //                    {
    //                        field: "description",
    //                        operator: "contains",
    //                        value: params.term || '',
    //                    },
    //                    {
    //                        field: "uom_code",
    //                        operator: "contains",
    //                        value: params.term || '',
    //                    }
    //                ]
    //            }];
    //        },
    //        options: {
    //            //dropdownParent: $formChemicalUsed,
    //            placeholder: "Select a Unit of Measurement",
    //            allowClear: true,
    //            //tags: true,
    //            //multiple: true,
    //            templateResult: function (repo) {
    //                return repo.text + ` (` + repo.uom_code + `)`;
    //            },
    //        }
    //    });
    //}

    var chemicalUomLookup = function (id) {
        console.log("chemicalUomLookup called with id:", id);
        var $element;
        if (id == null || id == "") {
            $element = $(".lookup-chemical_uom");
        } else {
            $element = $("#" + id);
        }
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/uom/lookup'),
            ajax: {
                transport: function (params, success, failure) {
                    var request = $.ajax(params);
                    request.then(success);
                    request.fail(failure);
                    return request;
                },
                data: function (d) {
                    if (d.filters && d.filters.length > 0) {
                        for (var i = 0; i < d.filters[0].filters.length; i++) {
                            var filter = d.filters[0].filters[i];
                            if (filter.value) {
                                console.log('Original Filter Value:', filter.value);
                                filter.value = filter.value.replace(/%/g, '%25'); // Encode % to %25
                                console.log('Encoded Filter Value:', filter.value);
                            }
                        }
                    }
                    console.log('Request Data before JSON.stringify:', d); // Logging request data before stringify
                    var jsonData = JSON.stringify(d);
                    console.log('Request Data after JSON.stringify:', jsonData); // Logging request data after stringify
                    return jsonData;
                },
            },
            result: {
                id: 'id',
                text: 'description',
                uom_code: 'uom_code'
            },
            filters: function (params) {
                return [{
                    logic: "OR",
                    filters: [
                        {
                            field: "description",
                            operator: "contains",
                            value: params.term || '',
                        },
                        {
                            field: "uom_code",
                            operator: "contains",
                            value: params.term || '',
                        }
                    ]
                }];
            },
            options: {
                placeholder: "Select a Unit of Measurement",
                allowClear: true,
                templateResult: function (repo) {
                    return repo.text + ` (` + repo.uom_code + `)`;
                },
            }
        });
    }
    //// Call the function to see the logs
    chemicalUomLookup();

    var datepickerFormat = function (element) {
        // console.log("ae", element)
        element.datepicker({
            autoclose: true,
            todayBtn: "linked",
            format: 'mm/dd/yyyy',
            orientation: "bottom left",
            todayHighlight: true,
            templates: controls
        });
    }

    var datepickerFormatCustom = function (element) {
        // console.log("iu",element)
        element.datepicker({
            autoclose: true,
            todayBtn: "linked",
            format: 'mm/dd/yyyy',
            orientation: "bottom left",
            todayHighlight: true,
            templates: controls
        }).datepicker('setDate', moment($drillingDate).toDate());
    }

    var wellBhaLookUp = function (e) {
        //var $element = e;
        //if (e == null)
        var $element = $(".wellBhaLookup");
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/wellbha/lookupByWell/' + $wellId.val()),
            result: {
                id: 'id',
                text: 'bha_no'
            },
            filters: function (params) {
                return [{
                    field: "bha_no",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "",
                allowClear: true,
                templateResult: function (repo) {
                    if (repo.loading) {
                        return repo.text;
                    }
                    return "<span class='fw-700'>No: " + repo.text + "</span>";
                },
                templateSelection: function (data, container) {
                    if (data.text == "")
                        return "Choose BHA";
                    else
                        return "<span class='fw-700'>No: " + data.text + "</span>";
                },
            }
        });
    }

    var wellBitLookUp = function (e) {
        //var $element = e;
        //if (e == null)
        var $element = $(".wellBitLookup");
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/wellbit/lookupByWell/' + $wellId.val()),
            result: {
                id: 'id',
                text: 'bit_number',
                bit_size: 'bit_size'
            },
            filters: function (params) {
                return [{
                    field: "bit_number",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "",
                allowClear: true,
                templateResult: function (repo) {
                    if (repo.loading) {
                        return repo.text;
                    }
                    return "<span class='fw-700'>No: " + repo.text + "</span>";
                },
                templateSelection: function (data, container) {
                    if (data.text == "")
                        return "Choose BIT";
                    else
                        return "<span class='fw-700'>No: " + data.text + "</span>";
                },
            }
        });

        //$element.on('select2:select', function (response) {
        //    //// console.log();
        //    var id = response.target.value;
        //    //// console.log("Well Bit Selected");
        //    //// console.log(response);
        //    var element = $element.closest('.row-data').find(".bit-run");

        //    getBitLastRun(response.target.value, element);
        //});
    }

    var stoneLookup = function (id) {
        var $element;
        if (id == null || id == "") {
            $element = $(".lookup-stone");
        } else {
            $element = $("#" + id);
        }
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/Stone/lookup'),
            result: {
                id: 'id',
                text: 'name'
            },
            filters: function (params) {
                return [{
                    field: "name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: formActualInformation,
                placeholder: "Select a Stone",
                allowClear: true,
                maximumSelectionLength: 1,
            }
        });
    }

    var getBitLastRun = function (wellBitId, $element) {
        $.get($.helper.resolveApi('~/core/wellbit/GetLastRun/' + $wellId.val() + '/' + $recordId.val() + '/' + wellBitId), function (r) {
            if (r.status.success) {
                // console.log("getBitLastRun");
                // console.log(r);
                if (r.status.success) {
                    //toastr.success("");
                    $element.val(parseInt(r.data));
                } else {
                    toastr.error(r.status.message);
                }
            }
        }).fail(function (r) {
        }).done(function () {
        });
    }

    var maskingMoney = function () {
        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });
    }

    var lotFitLookup = function (id, isNew) {
        if (isNew) {
            $("#hole-casing-tipe-" + id).val('');
        }
        $("#hole-casing-tipe-" + id).select2({
            placeholder: "Select a Lot or FIT",
            allowClear: true
        });
    }

    var pageFunction = function () {
        datepickerFormat($('.date-picker'));
        $(".numeric").inputmask({
            digits: 0,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });
        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });

        function modalPlanBudget(e) {
            //$.helper.form.clear($formBudgetPlan);

            var afePlanId = $(e.currentTarget).data("afeplanid");
            //$("#budgetMaterialName").val($(e.currentTarget).data("materialname"));
            //$("#material_id").val($(e.currentTarget).data("materialid"));
            //$("#afe_plan_id").val(afePlanId);
            loadAfePlanDetail();
        }

        var loadAfePlanDetail = function () {
            $("#afeManageBudget > .panel-body").hide();
            $("#afeManageBudget > .panel-footer").show();
            $planBudgetModal.modal('show');
        }

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

        iadcLookUp();

        var recalculateProgressMD = function () {
            var previous = $("#previous_depth_md").val();
            var current = $("#current_depth_md").val();
            // $("#progress").val(parseInt(previous) + parseInt(current));
            $("#progress").val(parseInt(current) - parseInt(previous));
        }

        var templateScript = $("#rowOperationActivities-template").html();
        var template = Handlebars.compile(templateScript);
        var cloneOperationActivities = function (id) {
            $("#tableOperationActivities-" + id + "> tbody").append(template);

            $("input").on("change", function () {
                $(this).removeClass("input-danger");
            });

            $('#tableOperationActivities-' + id + '> tbody tr:last > .activitiesIadc > .iadc-lookup').click(function () {
                var element = $(this);
                element.button('loading');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/Iadc/Lookup',
                    success: function (data) {
                        element.button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                            callback: function (e) {
                                //// console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {
                            $box.modal('hide');
                            element.closest("tr").children(".activitiesCategory").find("input").val(event.node.original.category);
                            if (event.node.original.type == "1") {
                                element.closest("tr").children(".activitiesPTorNPT").find("input").val("PT");
                            } else if (event.node.original.type == "2") {
                                element.closest("tr").children(".activitiesPTorNPT").find("input").val("NPT");
                            }
                            element.siblings().val(event.node.id);
                            element.html(event.node.text);
                        });
                    }
                });
            });

            $("#tableOperationActivities-" + id + "> tbody tr:last > .activitiesDepth > .numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            var curentDate = moment($drillingDate).format('MM/DD/YYYY');
            datepickerFormat($('#tableOperationActivities-' + id + '> tbody tr:last > .activitiesDate > .date-picker').datepicker("setDate", curentDate));

            $('.row-delete').on('click', function () {
                $(this).closest('.rowOperation').remove();
            });
        }

        $('#cloneDrillingTransport').on('click', function () {
            var templateScript = $("#rowTransport-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableTransport > tbody tr.no-data').remove();
            var lastIndex = parseInt($('#tableTransport tbody>tr:last').data('idx') || 0);
            lastIndex++;

            $("#tableTransport > tbody").append(template({ idx: lastIndex }));

            datepickerFormatCustom($('#tableTransport>tbody tr:last > .transportDate > .date-picker-shipping'));

            $('.row-delete').on('click', function () {
                $(this).closest('.rowOperation').remove();
            });
        });

        $('#cloneBhaBitMudlogging').on('click', function () {
            cloneMudloggingData();
        });

        $("#cloneActulInformaton").on('click', function () {
            var templateScript = $("#rowActualInformation-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableActualInformation > tbody tr.no-data').remove();
            $("#tableActualInformation > tbody").append(template);

            $('#tableActualInformation>tbody tr:last > .lookup_stone > select').attr("id", "stone_id-" + lastRowActualInformation);
            $('#tableActualInformation>tbody tr:last > .seq > input').val(lastRowActualInformation + 1);

            stoneLookup("stone_id-" + lastRowActualInformation);
            lastRowActualInformation++;

            maskingMoney();
            $('.row-delete').on('click', function () {
                // console.log($(this));
                $(this).closest('.myRow').remove();
            });
        });

        var cloneMudloggingData = function () {
            var templateScript = $("#rowBhaBit-template").html();
            var template = Handlebars.compile(templateScript);

            var lastIndex = parseInt($('#form-drillingBhaBit .row-data').last().data('idx') || 0);
            lastIndex++;
            GetWellObjectUomMap();
            $("#form-drillingBhaBit").append(template({ idx: lastIndex }));


            wellBhaLookUp($('#form-drillingBhaBit .row-data:last > .wellBhaLookup'));
            wellBitLookUp($('#form-drillingBhaBit .row-data:last > .wellBitLookup'));

            $("#depth_in-" + lastIndex).on("change", function () {
                var index = $(this).data("index");
                calculateRop(index);
            });
            $("#depth_out-" + lastIndex).on("change", function () {
                var index = $(this).data("index");
                calculateRop(index);
            });
            $("#duration-" + lastIndex).on("change", function () {
                var index = $(this).data("index");
                calculateRop(index);
            });

            $("#form-drillingBhaBit .row-data:last .numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });



            $('.row-delete').on('click', function () {
                $(this).closest('.row-data').remove();
            });
        }

        $("#cloneDrillingDeviation").on('click', function () {
            var templateScript = $("#rowDrillingDeviationDaily-template").html();
            var template = Handlebars.compile(templateScript);
            $('#tableDrillingDeviation > tbody tr.no-data').remove();
            var lastIndex = parseInt($('#tableDrillingDeviation tbody>tr:last').data('idx') || 0);
            lastIndex++;


            $("#tableDrillingDeviation > tbody").append(template({ idx: lastIndex }));
            maskingMoney();

            //$('#tableDrillingDeviation >.row-delete').on('click', function () {
            //    alert('test');
            //    $(this).closest('tr').remove();
            //});
        });

        $(document).on('click', '#tableDrillingDeviation .row-delete', function () {
            $(this).closest('tr').remove();
        });

        $("#cloneDrillingPersonel").on('click', function () {
            var templateScript = $("#rowDrillingPersonel-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableDrillingPersonel > tbody tr.no-data').remove();
            var lastIndex = parseInt($('#tableDrillingPersonel>tbody tr:last').data('idx') || 0);
            lastIndex++;

            $("#tableDrillingPersonel > tbody").append(template({ idx: lastIndex }));

            $('#tableDrillingPersonel>tbody tr:last > .jobPersonel > select').attr("id", "job_personel_id-" + lastIndex);

            jobPersonelLookUp("job_personel_id-" + lastIndex);
            lastIndex++;

            $('.row-delete').on('click', function () {
                $(this).closest('.rowPersonel').remove();
            });
        });

        $("#cloneDrillingPit").on('click', function () {
            var templateScript = $("#rowDrillingPit-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableDrillingPit > tbody tr.no-data').remove();
            var lastIndex = parseInt($('#tableDrillingPit tbody>tr:last').data('idx') || 0);
            lastIndex++;

            $("#tableDrillingPit > tbody").append(template({ idx: lastIndex }));

            $('#tableDrillingPit>tbody tr:last > .pitSelect > select').attr("id", "pit_id-" + lastIndex);

            pitLookup("pit_id-" + lastIndex);
            lastIndex++;

            $('.row-delete').on('click', function () {
                $(this).closest('.rowRecord').remove();
            });
            maskingMoney();
        });

        $("#cloneDrillingChemicalUsed").on('click', function () {
            var templateScript = $("#rowDrillingChemicalUsed-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableChemicalUsed > tbody tr.no-data').remove();
            var lastIndex = parseInt($('#tableChemicalUsed tbody>tr:last').data('idx') || 0);
            lastIndex++;

            $("#tableChemicalUsed > tbody").append(template({ idx: lastIndex }));

            $('#tableChemicalUsed>tbody tr:last > .chemicalSelect > select').attr("id", "uom_id-" + lastIndex);

            //$('#tableChemicalUsed>tbody tr:last > .holeCasingSelect > select').attr("id", "well_hole_casing_id-" + lastIndex);

            //holeAndCasingLookupByDrilling("well_hole_casing_id-" + lastIndex);

            chemicalUomLookup("uom_id-" + lastIndex);
            lastIndex++;

            $('.row-delete').on('click', function () {
                $(this).closest('.rowRecord').remove();
            });
            maskingMoney();
        });

        $("#cloneDrillingTotalVolume").on('click', function () {
            var templateScript = $("#rowVolume-template").html();
            var template = Handlebars.compile(templateScript);

            var lastIndex = parseInt($("#form-totalVolume > .volumeIndex").last().data('idx') || 0);
            lastIndex++;

            $("#form-totalVolume").append(template({ idx: lastIndex }));

            holeAndCasingLookupByDrilling("volume_well_hole_casing_id-" + lastIndex);

            $('.row-delete').on('click', function () {
                // console.log("try to delete");
                $("#volumeIndex-" + $(this).data("index")).remove();
            });
        });

        $(document).on('click', '#tableChemicalUsed .row-delete', function () {

        });

        Handlebars.registerHelper('setType', function (type) {
            if (type == 1) {
                return "PT";
            } else if (type == "2") {
                return "NPT";
            }
        });

        Handlebars.registerHelper('setIadcName', function (iadc_code, iadc_description, type) {
            if (type == 1) {
                return iadc_code + " - " + iadc_description + " (PT)";
            } else if (type == "2") {
                return iadc_code + " - " + iadc_description + " (NPT)";
            }
        });

        Handlebars.registerHelper('printHoleCasingType', function (type, id) {
            var html = "";

            html += "<select class=\"form-control hole-casing-tipe\" id='hole-casing-tipe-" + id + "' name=\"lot_fit[]\">";
            if (type == 1) {
                html += `<option value="1" selected="true">LOT</option>
                          <option value="2">FIT</option>`;
            } else if (type == 2) {
                html += `<option value="1">LOT</option>
                          <option value="2" selected="true">FIT</option>`;
            } else {
                html += `<option value="1">LOT</option>
                          <option value="2">FIT</option>`;
            }
            html += "</select>";
            html += "<div class=\"invalid-feedback\">Lot or Fit type cannot be empty</div>";
            return new Handlebars.SafeString(html);
        });

        var calculateRop = function (index) {
            var depth_in = $("#depth_in-" + index).val().replace(/,/g, '');;
            var depth_out = $("#depth_out-" + index).val().replace(/,/g, '');;
            var duration = $("#duration-" + index).val().replace(/,/g, '');;

            var footage = parseFloat(depth_out) - parseFloat(depth_in);
            var rop = parseFloat(footage) / parseFloat(duration);

            $("#footage-" + index).val(footage);
            $("#rop_overall-" + index).val(rop);
        }

        var getDrillingBhaBit = function () {
            var templateScript = $("#readBhaBit-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillingBhaBit/getDetail/' + $recordId.val()), function (r) {
                // console.log('aasad');
                // console.log(r.data);
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#form-drillingBhaBit").html(template({ data: r.data }));

                        GetWellObjectUomMap();

                        $("#form-drillingBhaBit .numeric-money").inputmask({
                            digits: 2,
                            greedy: true,
                            definitions: {
                                '*': {
                                    validator: "[0-9]"
                                }
                            },
                            rightAlign: false
                        });

                        $(".depth-out").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".depth-in").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });
                        $(".duration-bit").on("change", function () {
                            var index = $(this).data("index");
                            calculateRop(index);
                        });

                        $.each(r.data, function (key, value) {
                            wellBhaLookUp(value.well_bha_id);
                            wellBitLookUp(value.well_bit_id);

                        });


                    } else {
                        cloneMudloggingData();
                    }

                    $('.row-delete').on('click', function () {
                        $(this).closest('.row-data').remove();
                    });


                    $('.bit-run').keyup(function () {
                        const str = this.value;
                        const result = str.replace(/[^\d.]|\.(?=.*\.)/g, '');
                        this.value = result;
                    });
                }
            }).fail(function (r) {
            }).done(function () {
            });
        }

        var isBhaExist = function () {
            $.get($.helper.resolveApi('~/core/wellbha/isExistBha/' + $wellId.val()), function (r) {
                if (r.status.success) {
                    //isBitExist();
                }
            }).fail(function (r) {
            }).done(function () {
            });
        }

        var isBitExist = function () {
            $.get($.helper.resolveApi('~/core/wellbit/isExistBit/' + $wellId.val()), function (r) {
                if (r.status.success) {
                    if (r.data == false) {
                    } else {
                        $("#alert-drillingBhaBit").hide();
                        wellBhaLookUp("");
                        wellBitLookUp("");
                    }
                }
            }).fail(function (r) {
            }).done(function () {
            });
        }
        var getDrilOperationData = function (recordId) {
            // Define the API calls
            var apiCall1 = $.get($.helper.resolveApi('~/core/drillingoperationactivity/getByDrillingWellIds/' + recordId + '/' + $wellId.val()));
            var apiCall2 = $.get($.helper.resolveApi('~/core/drillingoperationactivity/GetByDrillingDescWellId/' + recordId + '/' + $wellId.val()));
            var combinedData = [];

            // Return the promise
            return $.when(apiCall1, apiCall2).then(function (result1, result2) {
                var apiData1 = result1[0].data;
                var apiData2 = result2[0].data;

               // console.log("apiData1:", apiData1);
               // console.log("apiData2:", apiData2);

                apiData1.forEach(function (item1) {
                    var dataDesct = [];

                    var matchingItems = apiData2.filter(function (item2) {
                        return item1.id === item2.id;
                    });

                    matchingItems.forEach(function (matchingItem) {
                        dataDesct.push({
                            casing: matchingItem.casing,
                            fren: matchingItem.fren,
                            id: matchingItem.id,
                            drilling_id: matchingItem.drilling_id,
                            depth: matchingItem.depth,
                            iadc_id: matchingItem.iadc_id,
                            iadc_code: matchingItem.iadc_code,
                            iadc_description: matchingItem.iadc_description,
                            type: matchingItem.type,
                            type_desc: matchingItem.type == "1" ? "PT" : "NPT",
                            category: matchingItem.category,
                            operation_start_date: matchingItem.operation_start_date,
                            operation_end_date: matchingItem.operation_end_date,
                            description: matchingItem.description,
                            drilling_hole_and_casing_id: matchingItem.drilling_hole_and_casing_id,
                        });
                    });

                    var combinedItem = {
                        id: item1.id,
                        hole_name: item1.hole_name,
                        hole_val: item1.hole_val,
                        hole_depth: item1.hole_depth,
                        casing_name: item1.casing_name,
                        casing_val: item1.casing_val,
                        lot_fit: item1.lot_fit,
                        lot_fit_val: item1.lot_fit_val,
                        cement_val: item1.cement_val,
                        well_hole_and_casing_id: item1.well_hole_and_casing_id,
                        drillingOperationActivities: dataDesct
                    };

                  //  combinedData.push(combinedItem);
                });

                console.log("Combined Data:", combinedData);
                return combinedData;
            }).fail(function (error1, error2) {
                console.error("Failed to get data from one or both APIs. Error 1:", error1.statusText, "Error 2:", error2.statusText);
                // You might want to handle the failure or return an error object.
            });
        };


        
        var getActivityByDrillingId = function (recordId) {
            var templateScript = $("#readHoleAndCasing-template").html();
            var template = Handlebars.compile(templateScript);
            console.log("ini idnyaa",recordId)
            $.get($.helper.resolveApi('~/core/drillingoperationactivity/getByDrillingId/' + recordId + '/' + $wellId.val()), function (r) {
                if (r.status.success) {
                    // console.log("r.data----------amigo");
                    console.log("ini drororor",r.data);

                    //  for (var i = 0; i < r.data.length; i++) {
                    //    var data = r.data[i];
                    //  for (var g = 0; g < data.drillingOperationActivities.length; g++) {
                    //    var drillingOperationActivities = data.drillingOperationActivities[g];
                    //  if(drillingOperationActivities.hours < 0){
                    //    // console.log("drillingOperationActivities.hours");
                    //  // console.log(drillingOperationActivities.hours);
                    // drillingOperationActivities.hours = 24 + parseFloat(drillingOperationActivities.hours);
                    // }
                    // if(drillingOperationActivities.operation_start_date == drillingOperationActivities.operation_end_date){
                    //   drillingOperationActivities.hours = "24"
                    // }                            
                    // }
                    //}
                    $("#formOperationActivities").html(template({ data: r.data }));
                    GetWellObjectUomMap();

                    $.each(r.data, function (key, value) {
                        holeAndCasingLookup(value.id);
                        // console.log(value.id);
                        lotFitLookup(value.id, false);

                        $("#cloneOperationActivies-" + value.id).on('click', function () {
                            cloneOperationActivities(value.id);
                        });
                    });


                    $("input").on("change", function () {
                        $(this).removeClass("input-danger");
                    });

                    datepickerFormat($('.date-picker'));
                    iadcLookUp();

                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowOperation').remove();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                console.log("gagal")
            }).done(function () {

            });
        }

        
       // Section Operation Activities

       /*
        var getActivityByDrillingId = function (recordId) {
            var templateScript = $("#readHoleAndCasing-template").html();
            var template = Handlebars.compile(templateScript);

            // Use getDrilOperationData as a promise
            getDrilOperationData(recordId)
                .then(function (result) {
                    var responseData = result; // No need to access .data here

                    console.log(responseData);

                    $("#formOperationActivities").html(template({ data: responseData }));
                    GetWellObjectUomMap();

                    $.each(responseData, function (key, value) {
                        holeAndCasingLookup(value.id);
                        lotFitLookup(value.id, false);

                        $("#cloneOperationActivies-" + value.id).on('click', function () {
                            cloneOperationActivities(value.id);
                        });
                    });

                    $("input").on("change", function () {
                        $(this).removeClass("input-danger");
                    });

                    datepickerFormat($('.date-picker'));
                    iadcLookUp();

                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowOperation').remove();
                    });

                    $('.loading-detail').hide();
                })
                .fail(function (error) {
                    console.error("Failed to get drilling operation data. Error:", error.statusText);
                });
        };

        */


        var getDrillingTransportById = function (recordId) {
            var templateScript = $("#readTransport-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingtransport/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $("#tableTransport > tbody").html(template({ data: r.data }));
                    datepickerFormat($('.date-picker'));

                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowOperation').remove();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
            }).done(function () {

            });
        }

        var getDrillingMud = function (recordId, mudType) {
            $.get($.helper.resolveApi('~/core/drillingmud?drillingId=' + recordId + '&dataMudType=' + mudType), function (r) {
                if (r.status.success) {
                    if (mudType == "mud_1") {
                        $.helper.form.fill($form_mud1, r.data);
                        $("#form-mud1 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                    if (mudType == "mud_2") {
                        $.helper.form.fill($form_mud2, r.data);
                        $("#form-mud2 #mud_time").val(timeFormat(r.data.mud_time));
                    }
                    if (mudType == "mud_3") {
                        $.helper.form.fill($form_mud3, r.data);
                        $("#form-mud3 #mud_time").val(timeFormat(r.data.mud_time));
                    }

                }
                $('.loading-detail').hide();
            }).fail(function (r) {

            }).done(function () {

            });
        }

        var getDrillingMudPit = function (recordId) {
            var templateScript = $("#readDrillingPit-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingmudpit/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    var newArr = [];
                    for (var i = 0; i < r.data.length; i++) {
                        var dd = r.data[i];
                        newArr[dd.seq] = dd;
                    }
                    $("#tableDrillingPit > tbody").html(template({ data: newArr }));

                    jobPersonelLookUp("");
                    $('.row-delete').on('click', function () {
                        $(this).closest('.rowRecord').remove();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
            }).done(function () {

            });
        }

        var getDrillingAerated = function (recordId) {
            $.get($.helper.resolveApi('~/core/drillingaerated/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    $.helper.form.fill($formDrillingAerated, r.data);
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        var getDrillingChemicalUsed = function (recordId) {
            var templateScript = $("#readDrillingChemicalUsed-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingchemicalused/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#tableChemicalUsed > tbody").html(template({ data: r.data }));

                        chemicalUomLookup("");
                        $('.row-delete').on('click', function () {
                            $(this).closest('.rowRecord').remove();
                        });
                    }
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }


        var getDrillingTotalVolumeLastDay = function (drillingDate, spudate) {


            // console.log("drilling date " + drillingDate + "and spud date " + spudate);


            var convertDrillingDate = moment(drillingDate).format('YYYY-MM-DD');
            var convertSpudDate = moment(spudate).format('YYYY-MM-DD');

            var drillingDateBefore = null;
            if (convertDrillingDate == convertSpudDate) {
                // console.log("ini smaa nilainya");
            }
            else if (convertDrillingDate > convertSpudDate) {
                // console.log("ini harus dapat one day beforenya");
                drillingDateBefore = moment(convertDrillingDate).subtract(1, 'days');
                drillingDateBefore = moment(drillingDateBefore).format("YYYY-MM-DD");

            }

            // console.log("drilling date before nya " + drillingDateBefore + " and well id" + $wellId.val());
            if (drillingDateBefore != null) {
                $.ajax({
                    url: $.helper.resolveApi('~/core/drilling/getListDrilling/' + $wellId.val() + "/" + drillingDateBefore + "/" + convertDrillingDate),
                    type: 'GET',
                    success: function (r) {

                        // console.log("ini data before dan after");

                        //// console.log(r.data);

                        //get index beforenya 

                        var drillingBeforeOneDay = r.data[0];

                        // console.log("get data drilling before " + drillingBeforeOneDay.id);
                        $.get($.helper.resolveApi('~/core/drillingtotalvolume/' + drillingBeforeOneDay.id + '/getByDrillingId'), function (r) {
                            if (r.status.success) {
                                if (r.data.length > 0) {
                                    // console.log("get total volume last day");
                                    // console.log(r.data);

                                    getLastDayTotalVolume.push(r.data[r.data.length - 1]);
                                } else {

                                }
                            }
                            $('.loading-detail').hide();
                        }).fail(function (r) {
                            //// console.log(r);
                        }).done(function () {

                        });

                    }
                });

            }
            else {

                //tidak ada aktifitas sebelummnya
            }


        }


        var getDrillingTotalVolume = function (recordId) {
            $.get($.helper.resolveApi('~/core/drillingtotalvolume/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        var templateScript = $("#readVolume-template").html();
                        var template = Handlebars.compile(templateScript);

                        $("#form-totalVolume").html(template({ data: r.data }));
                    } else {

                        //$("#tableChemicalUsed > tbody").append(template({ idx: lastIndex }));
                        var templateScript = $("#rowVolume-template").html();
                        var template = Handlebars.compile(templateScript);

                        $("#form-totalVolume").html(template({ idx: 0 }));
                    }
                    holeAndCasingLookupByDrilling("");

                    $('.row-delete').on('click', function () {
                        $("#volumeIndex-" + $(this).data("index")).remove();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        var getDrillingPersonelId = function (recordId) {
            var templateScript = $("#readDrillingPersonel-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingpersonel/' + recordId + '/getByDrillingId'), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#tableDrillingPersonel > tbody").html(template({ data: r.data }));

                        jobPersonelLookUp("");
                        $('.row-delete').on('click', function () {
                            $(this).closest('.rowPersonel').remove();
                        });
                    }
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        var getDrillingItemByDrillingId = function (drilling_id, well_id, drilling_date) {
            var templateScript = $("#readDrillingItemByWellId-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/drillingbulk/' + drilling_id + '/' + well_id + '/' + drilling_date + '/getByDrillingId'), function (r) {
                // console.log('aaa');
                // console.log(r);
                if (r.status.success && r.data.length > 0) {

                    $btnSaveBulk.on('click', function () {
                        saveBulk();
                    });

                    $("#drillingBulkItem > tbody").html(template({ data: r.data }));
                    maskingMoney();

                    $('.item-consumed').on('change', function () {
                        var previous = $(this).closest("tr").children(".item-previous").find("input").val().replace(/,/g, '');
                        var consumed = $(this).closest("tr").children(".item-consumed").find("input").val().replace(/,/g, '');
                        var received = $(this).closest("tr").children(".item-received").find("input").val().replace(/,/g, '');
                        var balance = parseFloat(received) + parseFloat(previous) - parseFloat(consumed);
                        $(this).closest("tr").children(".item-balance").find("input").val(thousandSeparatorWithoutComma(balance))
                    });

                    $('.item-received').on('change', function () {
                        var previous = $(this).closest("tr").children(".item-previous").find("input").val().replace(/,/g, '');
                        var consumed = $(this).closest("tr").children(".item-consumed").find("input").val().replace(/,/g, '');
                        var received = $(this).closest("tr").children(".item-received").find("input").val().replace(/,/g, '');
                        var balance = parseFloat(received) + parseFloat(previous) - parseFloat(consumed);
                        $(this).closest("tr").children(".item-balance").find("input").val(thousandSeparatorWithoutComma(balance))
                    });
                }
                else {
                    $("#drillingBulkItem > tbody").html(`<tr class="no-data">
                                                    <td colspan="10" class="text-center">No Item Available. Please Input your item on Well.</td>
                                                </tr>`);
                    $btnSaveBulk.hide();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        var getDrillingDeviationByDrillingId = function (drillingId) {
            var templateScript = $("#readDrillingDeviation-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillingDeviation/' + drillingId + '/detailByDrillingId'), function (r) {
                // console.log('drillingDeviationDt');
                // console.log(r.data);
                if (r.status.success && r.data.length > 0) {
                    $("#tableDrillingDeviation > tbody").html(template({ data: r.data }));
                    recalculateProgressMD();
                } else {
                    $("#tableDrillingDeviation > tbody").html(`<tr class="no-data">
                                                    <td colspan="10" class="text-center">No data available. Add Row to input new data.</td>
                                                </tr>`);
                }
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }
        var getDOL = function () {
            return new Promise(function (resolve, reject) {
                if ($recordId.val() !== '') {
                    $.get($.helper.resolveApi('~/core/drilling/getDol/' + $wellId.val()), function (r) {
                        if (r.data && Array.isArray(r.data) && r.data.length > 0) {
                            resolve(r.data[0].dol);
                        } else {
                            reject("Data tidak ditemukan atau bukan array.");
                        }
                    });
                } else {
                    reject("Record ID tidak valid.");
                }
            });
        };
       



    


        var loadDetail = function () {
            GetWellObjectUomMap();
            getActivityByDrillingId($recordId.val());

            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/drilling/' + $recordId.val() + '/detail'), function (r) {
                    getDrillingTransportById($recordId.val());
                    getDrillingDeviationByDrillingId($recordId.val());
                    getDrillingMud($recordId.val(), 'mud_1');
                    getDrillingMud($recordId.val(), 'mud_2');
                    getDrillingMud($recordId.val(), 'mud_3');
                    getDrillingMudPit($recordId.val());
                    getDrillingChemicalUsed($recordId.val());
                    getDrillingTotalVolume($recordId.val());
                    getDrillingPersonelId($recordId.val());
                    loadActualInformation($wellId.val());
                    //getDrillingItemByDrillingId($recordId.val());
                    getDrillingItemByDrillingId($recordId.val(), $wellId.val(), moment(r.data.drilling_date).format('YYYY-MM-DD'));
                    getDrillingTotalVolumeLastDay(r.data.drilling_date, r.data.spud_date);
                    //loadDefault(moment(r.data.drilling_date).format('YYYY-MM-DD'), r.data);
                    getDOL();
                    getDrillingAerated($recordId.val());
                    getDrillingBhaBit();
                    if (r.status.success) {
                        if (r.data.submitted_by != null) {
                            $(".btnClassInput").remove();
                        }

                        $afeId = r.data.afe_id;
                        getDOL()
                            .then(function (dolint) {
                                var currentDol = 0;
                                if (dolint !== 0) {
                                    currentDol = dolint + r.data.report_no; // Anda mungkin perlu mengganti 'r' dengan objek yang sesuai
                                }
                                $("#dol").val(currentDol);
                                //console.log("Ini cek", currentDol);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });


                        //loadDataTableUsage($afeId, $recordId.val());
                        //loadManageUsage($afeId, $recordId.val());
                        var intNumber = parseInt(r.data.report_no) + 1;
                        $("#report_no").val("#" + intNumber);
                        $("#drilling_date").val(moment(r.data.drilling_date).format('DD MMM YYYY'));
                        $drillingDate = r.data.drilling_date;
                        //loadDefault(moment(r.data.drilling_date).format('YYYY-MM-DD'), r.data);
                        $.helper.form.fill($formDrillingTop, r.data, ["id"]);
                        if (r.data.well_type === 'Vertical') {
                            $("#current_depth_tvd").val(r.data.current_depth_md);
                        }
                        $.helper.form.fill($formHSSE, r.data);
                        $("#hsse_tbop_press").val(r.data.hsse_tbop_press);
                        $("#hsse_tbop_func").val(r.data.hsse_tbop_func);
                        $("#hsse_dkick").val(r.data.hsse_dkick);
                        $("#hsse_dstrip").val(r.data.hsse_dstrip);
                        $("#hsse_dfire").val(r.data.hsse_dfire);
                        $("#hsse_dmis_pers").val(r.data.hsse_dmis_pers);
                        $("#hsse_aband_rig").val(r.data.hsse_aband_rig);
                        $("#hsse_dh2s").val(r.data.hsse_dh2s);
                        //console.log("ini dfs",r.data)
                        $.helper.form.fill($formDrillingWeather, r.data);
                        $.helper.form.fill($formMudloggingData, r.data);
                        recalculateProgressMD();
                        getDailyMudCost(r.data.well_id, r.data.drilling_date);
                        if (r.data.daily_mud_cost == null) {
                            $("#daily_mud_cost").val(0);
                        }
                        else {
                            $("#daily_mud_cost").val(r.data.daily_mud_cost);
                        }
                        //var ds = (r.data.daily_cost).toFixed(10);
                        var ds = (r.data.daily_cost).toFixed(2);
                        $("#daily_cost").text(thousandSeparatorDecimal(ds));

                        $("#daily_cost").attr("data-href", "/Core/Drilling/DailyCost?id=" + $recordId.val() + "&afeId=" + $afeId);

                        $("#spud_date").val(moment(r.data.spud_date).format('DD MMM YYYY'));

                        if (!r.data.canSubmit) {
                            $btnSubmit.remove();
                        }

                        if (!r.data.canApprove) {
                            $btnApprove.remove();
                            $btnReject.remove();
                        }
                        else {
                            $btnApprove.show();
                            $btnReject.show();
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //// console.log(r);
                }).done(function () {

                });
            }
        };

        var getDailyCost = function () {
            $.get($.helper.resolveApi('~/core/dailycost/GetDailyCost/' + $wellId.val() + '/' + $recordId.val()), function (r) {
                if (r.status.success) {
                    $("#cummulative_cost").val(thousandSeparatorDecimal(r.data.cummulative_cost));
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }
        $(document).on('hidden.bs.modal', "#dailyCostModal", function () {
            getDailyCost();
        });

        $("#btn-save-actulInformaton").on('click', function () {
            saveActualInformation();
        });

        var saveActualInformation = function () {
            $('.error-tab').hide();
            var btn = $("#btn-save-actulInformaton");

            var data = new Object();
            data.drilling = new Object();
            data.drilling.id = $recordId.val();
            data.drilling.well_id = $wellId.val();
            data.drill_Formations_Actual = new Object();

            var isValidate = formActualInformation[0].checkValidity();

            if (data.drill_Formations_Actual.length <= 0 || !isValidate) {
                $(".error-pi").show();
                isValidate = false;
                toastr.error("Actual Information cannot be empty.");
                return;
            }

            if (isValidate) {
                btn.button('loading');
                event.preventDefault();

                //-- Actual Information
                var actual_informations = [];
                var total = document.getElementsByName('ActualInformationId[]').length;
                for (var i = 0; i < total; i++) {
                    var id = document.getElementsByName('ActualInformationId[]')[i].value;
                    var depth = document.getElementsByName('depthActual[]')[i].value.replace(/,/g, '');
                    // console.log("data depth nya adalah " + depth + " and drilling information " + $recordId.val());
                    //var stone_id = document.getElementsByName('stone_id[]')[i].value;
                    var formation_unit = document.getElementsByName('formationUnit[]')[i].value;
                    var well_id = $wellId.val();
                    actual_informations.push({
                        id: id,
                        formation_data: formation_unit,
                        depth: depth,
                        well_id: well_id,
                        drilling_id: $recordId.val()
                    });
                }
                data.drill_Formations_Actual = actual_informations;
                $.post($.helper.resolveApi('~/core/DrillFormationsActual/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                        loadActualInformation(well_id)
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required fields");
                event.preventDefault();
                event.stopPropagation();
            }
        }

        var loadActualInformation = function (recordId) {
            var templateScript = $("#readActualInformation-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillFormationsActual/' + recordId + '/detailbywellidNoRole'), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        // console.log("information actual information ", r.data);
                        var newArr = [];
                        for (var i = 0; i < r.data.length; i++) {
                            var dd = r.data[i];
                            newArr[dd.seq - 1] = dd;
                        }

                        // $("#tableActualInformation > tbody").html(template({ data: r.data }));
                        $("#tableActualInformation > tbody").html(template({ data: newArr }));

                        lastRowActualInformation = r.data[r.data.length - 1].seq;
                        maskingMoney();
                        //stoneLookup("");
                        $('.row-delete').on('click', function () {
                            $(this).closest('.myRow').remove();
                        });
                    }

                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        var getDailyMudCost = function (wellId, drilling_date) {
            var drillingDate = moment(drilling_date).format('YYYY-MM-DD');
            $.get($.helper.resolveApi('~/core/drilling/getDailyMudCost/' + wellId + '/' + drillingDate), function (r) {
                if (r.status.success) {
                    var sum = 0;
                    $.each(r.data, function (key, value) {
                        sum += value.daily_mud_cost;
                    })
                    $("#cummulative_mud_cost").val(sum);
                }
            }).fail(function (r) {

            }).done(function () {

            });
        }

        $(document).on('hidden.bs.modal', "#setBhaBitModal", function () {
            getDrillingBhaBit();
        });

        var GetWellObjectUomMap = function () {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $wellId.val()), function (r) {
                if (r.status.success && r.data.length > 0) {
                    var ObjectMapper = r.data;
                    $("form#form-Drilling-top :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                if (objectName.toLowerCase().trim() == "planned td") {
                                    $("#" + elementId).val(uom.uom_code + "TVD");
                                } else {
                                    $("#" + elementId).val(uom.uom_code);
                                }
                            }
                        }
                    });

                    $("form#formDrillingTop :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });

                    $("form#formOperationActivities :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("#form-drillingHoleAndCasing :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("form#form-drillingBHA :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("#form-drillingBhaComponent :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("form#form-drillingBIT :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("#tableDrillingDeviation :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("#form-drillingBhaBit :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("#formOperationActivities :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("#tableActualInformation :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                    $("#tableAeratedDrilling :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });

                    $("#sectionDrillingMudData :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });

                    $("#formDrillingWeather :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });

                    $("#formOperationActivities span.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).html(uom.uom_code);
                            }
                        }
                    });

                    $("#form-totalVolume span.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).html(uom.uom_code);
                            }
                        }
                    });

                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        $("input[type=text].numeric").val(function (index, value) {
            //return value.trim();
        });

        $btnNewHole.on('click', function () {
            var reqlength = $('.top-cement').length;
            var value = $('.top-cement').filter(function () {
                return this.value != '';
            });

            if (value.length >= 0 && (value.length !== reqlength)) {
                toastr.error("Existing Hole and Casing must be close by Fill Top of Cement Value to new Hole and Casing.");
                return;
            }

            var templateScript = $("#rowHoleAndCasing-template").html();
            var template = Handlebars.compile(templateScript);
            Swal.fire({
                title: "",
                text: "Are you sure create a new Hole and Casing?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    //$("#listHoleAndCasing").html("");
                    $btnNewHole.button('loading');
                    $.post($.helper.resolveApi('~/core/DrillingHoleAndCasing/new/' + $wellId.val()), function (r) {
                        if (r.status.success) {
                            // console.log("r.data-----------------------------ami");
                            //console.log("sasa",r.data);
                            $("#formOperationActivities").append(template({ data: r.data }));

                            GetWellObjectUomMap();
                            holeAndCasingLookup(r.data.id);
                            lotFitLookup(r.data.id, true);

                            $("#cloneOperationActivies-" + r.data.id).on('click', function () {
                                cloneOperationActivities($(this).data("id"));
                            });

                            $formOperationActivities.removeClass('was-validated');
                        } else {
                            toastr.error(r.status.message)
                        }
                        $btnNewHole.button('reset');
                    }, 'json').fail(function (r) {
                        $btnNewHole.button('reset');
                        toastr.error(r.statusText);
                    });
                }
            });
        });

        $btnSaveOperationActivities.on('click', function () {
            var isValidate = $formOperationActivities[0].checkValidity();
            if (!isValidate) {
                $formOperationActivities.addClass('was-validated');
            }
            if (isValidate) {
                // console.log("save the operation activ");
                saveOperationActivities();
            } else {
                toastr.error("Please complete Hole and Casing and Operation fields");
                event.preventDefault();
                event.stopPropagation();
                //  return;
            }
        })
        var saveOperationActivities = function () {
            $formOperationActivities.removeClass('was-validated');
            var drillingDate = moment($drillingDate).format('YYYY-MM-DD');
            var Tomorrow = moment($drillingDate).add(1, 'days').format('YYYY-MM-DD');

            var isValid = true;
            var data = new Object();
            data.drilling_id = $recordId.val();
            data.well_id = $wellId.val();
            data.drilling_hole_and_casing = new Object();

            var holeAndCasing = [];
            var totalHoleCasing = document.getElementsByName('well_hole_and_casing_id[]').length;

            for (var i = 0; i < totalHoleCasing; i++) {
                var drillingHoleAndCasingid = document.getElementsByName('drilling_hole_and_casing_id[]')[i].value;
                holeAndCasing.push({
                    id: drillingHoleAndCasingid,
                    well_hole_and_casing_id: document.getElementsByName('well_hole_and_casing_id[]')[i].value,
                    casing_val: document.getElementsByName('casing_val[]')[i].value == "" ? null : document.getElementsByName('casing_val[]')[i].value.replace(/,/g, ''),
                    hole_val: document.getElementsByName('hole_val[]')[i].value == "" ? null : document.getElementsByName('hole_val[]')[i].value.replace(/,/g, ''),
                    lot_fit: document.getElementsByName('lot_fit[]')[i].value == "" ? null : document.getElementsByName('lot_fit[]')[i].value.replace(/,/g, ''),
                    lot_fit_val: document.getElementsByName('lot_fit_val[]')[i].value == "" ? null : document.getElementsByName('lot_fit_val[]')[i].value.replace(/,/g, ''),
                    cement_val: document.getElementsByName('cement_val[]')[i].value == "" ? null : document.getElementsByName('cement_val[]')[i].value.replace(/,/g, ''),

                });


                //-- Validation
                if ((totalHoleCasing > 1) && (i < totalHoleCasing - 1) && (document.getElementsByName('cement_val[]')[i].value == "")) {
                    toastr.error("You have to close Hole and Casing first by Fill Top of Cement Value.");
                    return;
                }

                var activities = [];
                var operationActivitiesTable = document.getElementById("tableOperationActivities-" + drillingHoleAndCasingid);
                //$(operationActivitiesTable).find("tbody").find("input").removeClass("input-danger");
                if ($(operationActivitiesTable).find("tbody").children().length > 0) {
                    $(operationActivitiesTable).find("tbody").children().each(function () {
                        var id = $(this).find("input[name^='activityId']").val();
                        var activityDate = $(this).find("input[name^='actvitiyDate']").val();
                        var startTime = $(this).find("input[name^='startTime']").val();
                        var endTime = $(this).find("input[name^='endTime']").val();
                        var depth = $(this).find("input[name^='depth']").val().replace(/,/g, '');
                        var iadc = $(this).find("input[name^='iadc']").val();
                        var detail = $(this).find("textarea[name^='detail']").val();

                        activities.push({
                            id: id,
                            operation_start_date: activityDate + " " + startTime,
                            operation_end_date: activityDate + " " + endTime,
                            depth: depth,
                            iadc_id: iadc,
                            description: detail,
                        });

                        //-- Validation
                        var $activityDate = moment(activityDate).format('YYYY-MM-DD');
                        var $activityStartDate = moment(activityDate + " " + startTime).format('YYYY-MM-DD HH:mm');
                        var $activityEndDate = moment(activityDate + " " + endTime).format('YYYY-MM-DD HH:mm');
                        var $activityEndDateTime = moment(activityDate + " " + endTime).format('HH:mm');
                        // console.log("activityEndDate")
                        // console.log($activityEndDate)
                        if ($activityDate < drillingDate) {
                            toastr.error("Operation Date cannot be smaller than Drilling Date");
                            $(this).find("input[name^='actvitiyDate']").addClass("input-danger");
                            isValid = false;
                        } else if ($activityDate > Tomorrow) {
                            toastr.error("Operation Date cannot be greater than Drilling Date");
                            $(this).find("input[name^='actvitiyDate']").addClass("input-danger");
                            isValid = false;
                        } else if ($activityDate == Tomorrow) {
                            if ($activityStartDate > $activityEndDate && $activityEndDateTime != "00:00") {
                                $(this).find("input[name^='startTime']").addClass("input-danger");
                                $(this).find("input[name^='endTime']").addClass("input-danger");
                                toastr.error("Start Activity must be smaller than End Activity");
                                isValid = false;
                            } else {
                                var $lastActivityDate = moment(Tomorrow + " 06:00").format('YYYY-MM-DD HH:mm');
                                if ($activityEndDate > $lastActivityDate && $activityEndDateTime != "00:00") {
                                    $(this).find("input[name^='endTime']").addClass("input-danger");
                                    toastr.error("End Activity must be smaller than " + $activityDate);
                                    isValid = false;
                                }
                            }
                        } else if ($activityDate == drillingDate) {
                            var $lastActivityDate = moment(Tomorrow + " 06:00").format('YYYY-MM-DD HH:mm');
                            if ($activityStartDate > $activityEndDate && $activityEndDateTime != "00:00") {
                                $(this).find("input[name^='startTime']").addClass("input-danger");
                                $(this).find("input[name^='endTime']").addClass("input-danger");
                                toastr.error("Start Activity must be smaller than End Activity");
                                isValid = false;
                            }
                        }

                        if (iadc == "") {
                            toastr.error("IADC on operation activity records cannot be empty.");
                            isValid = false;
                        }
                    });
                } else {
                    toastr.error("Please input operation activity records.");
                    isValid = false;
                }
                holeAndCasing[i].drilling_operation_activity = activities;
            }
            data.drilling_hole_and_casing = holeAndCasing;

            if (isValid) {
                $(operationActivitiesTable).find("tbody").find("input").removeClass("input-danger");
                Swal.fire({
                    title: "",
                    text: "Are you sure want to save Operation Activities?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes"
                }).then(function (result) {
                    if (result.value) {
                        $btnSaveOperationActivities.button('loading');
                        $.post($.helper.resolveApi('~/core/drillingholeandcasing/save'), data, function (r) {
                            if (r.status.success) {
                                toastr.success(r.status.message);
                                $("#current_depth_md").val(r.data.current_depth_md);
                                $("#progress").val(r.data.progress);
                            } else {
                                toastr.error(r.status.message);
                            }
                            $btnSaveOperationActivities.button('reset');
                        }, 'json').fail(function (r) {
                            $btnSaveOperationActivities.button('reset');
                            toastr.error(r.statusText);
                        });
                    }
                });
            }
        }

        $btnSaveDrillingBhaBit.on('click', function () {
            saveDrillingBhaBit();
        });
        var saveDrillingBhaBit = function () {
            var data = $formDrillingBhaBit.serializeToJSON({ associativeArrays: false });

            var isValidate = $formDrillingBhaBit[0].checkValidity();
            if (!isValidate) {
                $formDrillingBhaBit.addClass('was-validated');
                toastr.error("Please complete required fields");
                return;
            }

            $btnSaveDrillingBhaBit.button('loading');
            Swal.fire({
                title: "",
                text: "Are you sure want to save?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                console.log(data.bhaAndBit);
                if (result.value) {
                    //-- Do Save
                    $.ajax({
                        url: $.helper.resolveApi('~/core/drillingbhabit/save'),
                        type: "POST",
                        data: JSON.stringify(data.bhaAndBit || []),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            if (r.status.success) {
                                window.location.reload();
                                //toastr.success(r.status.message);
                            } else {
                                toastr.error(r.status.message);
                            }
                            $btnSaveDrillingBhaBit.button('reset');
                        }
                    });
                } else {
                    $btnSaveDrillingBhaBit.button('reset');
                }
            });

            return;

            if (isValidate) {

            } else {
                toastr.error("Please select BHA and BIT first.");
            }
        }

        $btnSaveDrilling.on('click', function () {
            // console.log("save drilling operation");
            saveDrillingTop();

            // console.log("end check");
        });

        $btnSaveHSSE.on('click', function () {
            saveHSSE();
        });

        $btnSaveMudlogging.on('click', function () {
            saveMudlogging();
        });

        $btnSaveWeather.on('click', function () {
            saveWeather();
        });

        $btnSaveDeviation.on('click', function () {
            saveDeviation();
        });
        $btnSaveBhaBit.on('click', function () {
            saveDrillingBhaBit();
        })
        $btnSaveTransport.on('click', function () {
            saveTransport();
        })

        $btnSaveMud1.on('click', function () {
            saveMud1();
        })

        $btnSaveMud2.on('click', function () {
            saveMud2();
        })

        $btnSaveMud3.on('click', function () {
            saveMud3();
        })

        $btnSaveVolume.on('click', function () {
            saveVolume();
        })

        $bntSavePit.on('click', function () {
            savePit();
        })

        $btnSaveChemicals.on('click', function () {
            saveChemicals();
        });

        $btnSaveAerated.on('click', function () {
            saveAerated();
        })

        $btnSavePersonel.on('click', function () {
            savePersonel();
        })

        $btnSubmit.on('click', function () {
            Swal.fire({
                title: "",
                text: "Are you sure want to submit ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    $btnSubmit.button('loading');
                    $.post($.helper.resolveApi('~/core/Drilling/submit?id=' + $recordId.val()), function (r) {
                        if (r.status.success) {
                            toastr.success(r.status.message);
                            loadDetail();
                        } else {
                            toastr.error(r.status.message);
                        }
                        $btnSubmit.button('reset');
                    }, 'json').fail(function (r) {
                        $btnSubmit.button('reset');
                        toastr.error(r.statusText);
                    });
                }
            });
        });

        $btnApprove.on('click', function () {
            doApproval($btnApprove, true);
        });
        $btnReject.on('click', function () {
            doApproval($btnReject, false);
        });

        var doApproval = function (btn, isAccepted) {

            var confirmMessage = "Approve";
            if (!isAccepted) {
                confirmMessage = "Reject";
            }

            Swal.fire({
                title: "Confirm " + confirmMessage + " ?",
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off',
                    placeholder: 'Type your reason',
                },
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (rs) => {
                    if (!isAccepted) {
                        if (rs == '') {
                            Swal.showValidationMessage(
                                "Please type your reason "
                            );
                        }
                    }
                },
                allowOutsideClick: () => Swal.showValidationMessage(
                    "Please type your reason "
                ).isLoading()
            }).then((result) => {
                var data = JSON.stringify({
                    recordId: $recordId.val(),
                    isAccepted: isAccepted,
                    description: result.value
                });

                if (!result.dismiss) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        url: $.helper.resolveApi("~/core/drilling/approve"),
                        data: data,
                        success: function (r) {
                            if (r.status.success) {
                                toastr.success("Data has been " + confirmMessage);
                                loadDetail();
                            } else {
                                toastr.error(r.status.message);
                            }
                        },
                        error: function (r) {
                            toastr.error(r.statusText);
                        }
                    });
                }

            })

            //var msgisAccepted = "approve";

            //if (!isAccepted) {
            //    msgisAccepted = "reject";
            //}
            //Swal.fire({
            //    title: "",
            //    text: "Are you sure want to " + msgisAccepted + " ?",
            //    type: "warning",
            //    showCancelButton: true,
            //    confirmButtonText: "Yes"
            //}).then(function (result) {
            //    if (result.value) {
            //        btn.button('loading');
            //        var data = JSON.stringify({
            //            recordId: $recordId.val(),
            //            isAccepted: isAccepted
            //        });
            //        $.ajax({
            //            type: "POST",
            //            dataType: 'json',
            //            contentType: 'application/json',
            //            url: $.helper.resolveApi("~/core/drilling/approve"),
            //            data: data,
            //            success: function (r) {
            //                if (r.status.success) {
            //                    toastr.success("Data has been " + msgisAccepted);
            //                } else {
            //                    toastr.error(r.status.message);
            //                }
            //                btn.button('reset');
            //            },
            //            error: function (r) {
            //                toastr.error(r.statusText);
            //            }
            //        });
            //    }
            //});
        };





        var saveHSSE = function () {
            var btn = $btnSaveHSSE;
            var data = $formHSSE.serializeToJSON();
            var isValidate = $formHSSE[0].checkValidity();
            if (!isValidate) {
                $formHSSE.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.post($.helper.resolveApi('~/core/drilling/save'), data, function (r) {
                if (!r.status.success) {
                    toastr.error(r.status.message);
                }
                else {
                    toastr.success(r.status.message);
                }
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            });

        }

        var saveBulk = function () {
            var btn = $btnSaveBulk;
            var data = new Object();
            data.drilling = new Object();
            data.drilling.id = $recordId.val();
            data.drilling_bulk = new Object();
            var isValidate = $formDrillingBulk[0].checkValidity();
            if (!isValidate) {
                $formDrillingBulk.addClass('was-validated');
                return;
            }


            var bulk = [];
            var total = document.getElementsByName('id[]').length;

            for (var i = 0; i < total; i++) {
                var id = document.getElementsByName('id[]')[i].value;
                var drilling_id = document.getElementsByName('drilling_id[]')[i].value;
                var well_item_id = document.getElementsByName('well_item_id[]')[i].value;
                var previous = document.getElementsByName('previous[]')[i].value.replace(/,/g, '');
                var consumed = document.getElementsByName('consumed[]')[i].value.replace(/,/g, '');
                var received = document.getElementsByName('received[]')[i].value.replace(/,/g, '');
                var balance = document.getElementsByName('balance[]')[i].value.replace(/,/g, '');

                bulk.push({
                    id: id,
                    drilling_id: drilling_id,
                    well_item_id: well_item_id,
                    previous: previous,
                    consumed: consumed,
                    received: received,
                    balance: balance
                });
            }
            data.drilling_bulk = bulk;

            if (isValidate) {
                btn.button('loading');
                // console.log('aasas');
                // console.log(data);
                $.post($.helper.resolveApi('~/core/drillingbulk/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        getDrillingItemByDrillingId($recordId.val(), $wellId.val(), moment($drillingDate).format('YYYY-MM-DD'));

                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            }

        }

        var saveMudlogging = function () {
            var btn = $btnSaveMudlogging;
            var data = $formMudloggingData.serializeToJSON();
            var isValidate = $formMudloggingData[0].checkValidity();
            if (!isValidate) {
                $formMudloggingData.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.post($.helper.resolveApi('~/core/drilling/save'), data, function (r) {
                if (!r.status.success) {
                    toastr.success(r.status.message);
                }
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            });

        }

        var saveWeather = function () {
            var btn = $btnSaveWeather;
            var data = $formDrillingWeather.serializeToJSON();
            var isValidate = $formDrillingWeather[0].checkValidity();
            if (!isValidate) {
                $formDrillingWeather.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.post($.helper.resolveApi('~/core/drilling/save'), data, function (r) {
                if (!r.status.success) {
                    toastr.error(r.status.message);
                }
                else {
                    toastr.success(r.status.message);
                }
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            });

        }

        var saveDeviation = function () {
            var btn = $btnSaveDeviation;
            var data = $formDrillingDeviation.serializeToJSON({ associativeArrays: false });

            // console.log('deviation');
            // console.log(data);

            var isValidate = $formDrillingDeviation[0].checkValidity();
            if (!isValidate) {
                $formDrillingDeviation.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.ajax({
                url: $.helper.resolveApi('~/core/DrillingDeviation/saveAll?drillingId=' + $recordId.val()),
                type: "POST",
                data: JSON.stringify(data.deviation || []),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    if (!r.status.success) {
                        toastr.error(r.status.message);
                    }
                    else {
                        toastr.success(r.status.message);
                        getDrillingDeviationByDrillingId($recordId.val());
                    }
                    btn.button('reset');
                }
            })
        }

        var saveTransport = function () {
            var btn = $btnSaveTransport;
            var data = new Object();
            data.drilling = new Object();
            data.drilling.id = $recordId.val();
            data.drilling_transport = new Object();
            var isValidate = $formDrillingTransport[0].checkValidity();
            if (!isValidate) {
                $formDrillingTransport.addClass('was-validated');
                toastr.error("Please complete personel fields");
                return;
            }


            var transports = [];
            var total = document.getElementsByName('transportId[]').length;

            for (var i = 0; i < total; i++) {
                var id = document.getElementsByName('transportId[]')[i].value;
                var name = document.getElementsByName('name[]')[i].value;
                var type = document.getElementsByName('type[]')[i].value;
                var departedDate = document.getElementsByName('departedDate[]')[i].value;
                var departedTime = document.getElementsByName('departedTime[]')[i].value;
                var etaDate = document.getElementsByName('etaDate[]')[i].value;
                var etaTime = document.getElementsByName('etaTime[]')[i].value;
                var days = 0;
                var comment = document.getElementsByName('comment[]')[i].value;

                transports.push({
                    id: id,
                    drilling_id: $recordId.val(),
                    name: name,
                    type: type,
                    departed_date: departedDate + " " + departedTime,
                    eta_date: etaDate + " " + etaTime,
                    days: days,
                    comment: comment
                });
            }
            data.drilling_transport = transports;

            btn.button('loading');
            $.post($.helper.resolveApi('~/core/drillingtransport/saveAll?drillingId=' + $recordId.val()), data, function (r) {
                if (r.status.success) {
                    toastr.success(r.status.message);
                    getDrillingTransportById($recordId.val());
                } else {
                    toastr.error(r.status.message);
                }
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            })
        }

        var saveMud1 = function () {
            var btn = $btnSaveMud1;
            var data = $form_mud1.serializeToJSON({ associativeArrays: false });
            var isValidate = $form_mud1[0].checkValidity();
            if (!isValidate) {
                $form_mud1.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.ajax({
                url: $.helper.resolveApi('~/core/DrillingMud/save'),
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                },
                error: function (xhr, status, error) {
                    toastr.error(xhr.statusText);
                    btn.button('reset');
                }
            });
        }


        var saveMud2 = function () {
            var btn = $btnSaveMud2;
            var data = $form_mud2.serializeToJSON({ associativeArrays: false });
            var isValidate = $form_mud2[0].checkValidity();
            if (!isValidate) {
                $form_mud2.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.ajax({
                url: $.helper.resolveApi('~/core/DrillingMud/save'),
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                },
                error: function (xhr, status, error) {
                    toastr.error(xhr.statusText);
                    btn.button('reset');
                }
            });
        }


        var saveMud3 = function () {
            var btn = $btnSaveMud3;
            var data = $form_mud3.serializeToJSON({ associativeArrays: false });
            var isValidate = $form_mud3[0].checkValidity();
            if (!isValidate) {
                $form_mud3.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.ajax({
                url: $.helper.resolveApi('~/core/DrillingMud/save'),
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                },
                error: function (xhr, status, error) {
                    toastr.error(xhr.statusText);
                    btn.button('reset');
                }
            });
        }


        var saveVolume = function () {
            var btn = $btnSaveVolume;
            //var data = $formTotalVolume.serializeToJSON();

            var data = $formTotalVolume.serializeToJSON({ associativeArrays: false });

            var isValidate = $formChemicalUsed[0].checkValidity();
            if (!isValidate) {
                $formChemicalUsed.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.ajax({
                url: $.helper.resolveApi('~/core/drillingtotalvolume/SaveList?drillingId=' + $recordId.val()),
                type: "POST",
                data: JSON.stringify(data.drillingVolume || []),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    //// console.log(r);
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        getDailyMudCost($wellId.val(), $drillingDate);
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }
            });
        }

        var savePit = function () {
            var btn = $bntSavePit;
            var data = $formDrillingPit.serializeToJSON({ associativeArrays: false });
            //// console.log(data);
            var isValidate = $formDrillingPit[0].checkValidity();
            if (!isValidate) {
                $formDrillingPit.addClass('was-validated');
                return;
            }

            // insert object drilling_id

            var datapitsArr = [];
            for (var i = 0; i < data.pits.length; i++) {
                if (data.pits[i]) {
                    data.pits[i].drilling_id = $recordId.val();
                    datapitsArr.push(data.pits[i]);
                }
            }

            // for (var i = 0; i < data.pits.length; i++) {
            //     data.pits[i].drilling_id = $recordId.val();
            // }

            // console.log("data.pits")
            // console.log(data.pits)
            // console.log(datapitsArr)

            btn.button('loading');
            $.ajax({
                url: $.helper.resolveApi('~/core/drillingmudpit/saveAll?drillingId=' + $recordId.val()),
                type: "POST",
                data: JSON.stringify(datapitsArr || []),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    //// console.log(r);
                    if (!r.status.success) {
                        toastr.error(r.status.message);
                    }
                    else {
                        toastr.success("The data has been saved.");
                        getDrillingMudPit($recordId.val());
                    }
                    btn.button('reset');
                }
            })
        }

        var saveChemicals = function () {
            var btn = $btnSaveChemicals;
            var data = $formChemicalUsed.serializeToJSON({ associativeArrays: false });
            var isValidate = $formChemicalUsed[0].checkValidity();
            if (!isValidate) {
                $formChemicalUsed.addClass('was-validated');
                return;
            }
            btn.button('loading');
            $.ajax({
                url: $.helper.resolveApi('~/core/drillingchemicalused/saveAll?drillingId=' + $recordId.val()),
                type: "POST",
                data: JSON.stringify(data.chemicals || []),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    //// console.log(r);
                    if (r.status.success) {
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }
            })
        }

        var saveAerated = function () {
            var btn = $btnSaveAerated;
            var data = $formDrillingAerated.serializeToJSON({ associativeArrays: false });
            var isValidate = $formDrillingAerated[0].checkValidity();
            if (!isValidate) {
                $formDrillingAerated.addClass('was-validated');
                return;
            }
            //// console.log(data);
            btn.button('loading');
            $.post($.helper.resolveApi('~/core/drillingaerated/save'), data, function (r) {
                if (!r.status.success) {
                    toastr.error(r.status.message);
                }
                toastr.success(r.status.message);
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            })
        }

        var savePersonel = function () {
            var btn = $btnSavePersonel;
            var data = $formDrillingPersonel.serializeToJSON({ associativeArrays: false });
            //data.drilling = new Object();
            //data.drilling.id = $recordId.val();
            //data.drilling_personel = new Object();
            var isValidate = $formDrillingPersonel[0].checkValidity();
            if (!isValidate) {
                $formDrillingPersonel.addClass('was-validated');
                toastr.error("Please complete personel fields");
                return;
            }
            // console.log(JSON.stringify(data.personel || []));
            btn.button('loading');
            $.ajax({
                url: $.helper.resolveApi('~/core/drillingpersonel/saveAll?drillingId=' + $recordId.val()),
                type: "POST",
                data: JSON.stringify(data.personel || []),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    //// console.log(r);
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        getDrillingPersonelId($recordId.val());
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }
            })
        }


        var saveDrillingTop = function () {
            // console.log("save operation activites");


            // console.log("save operation ");



            var obj = new Object();
            obj.id = $recordId.val();
            obj.well_id = $wellId.val();
            obj.operation_data_period = $('textarea#operation_data_period').val();
            obj.operation_data_early = $('textarea#operation_data_early').val();
            obj.event = $('input[name=event]').val();
            obj.dol = $('input[name=dol]').val();
            obj.previous_depth_md = $('input[name=previous_depth_md]').val();

            obj.operation_data_planned = $('textarea#operation_data_planned').val();
            // obj.cummulative_cost = $(newDDR).find('costDay:first').text();

            // console.log("data perior " + $('textarea#operation_data_period').val());
            //convert object to json string
            var string = JSON.stringify(obj);
            //convert string to Json Object
            var dataOperation = JSON.parse(string);

            // console.log(string);
            // console.log(dataOperation);
            $.ajax({
                type: "POST",
                dataType: 'json',
                cache: false,
                contentType: 'application/json',
                url: $.helper.resolveApi("~/core/drilling/save"),
                data: string,
                success: function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }

                },
                error: function (r) {
                    toastr.error(r.statusText);

                }
            });


            // console.log("what happende");

        }


        //-- Function field change
        $('#daily_cost').on('change', function () {
            var newDailyCost = $(this).val().replace(/,/g, '');
            var newValue = parseInt($cummulativeCost) - parseInt($dailyCost) + parseInt(newDailyCost);
            $("#cummulative_cost").val(thousandSeparatorWithoutComma(newValue));
        });

        //var dailymudCost = function (cost) {
        //    $('#daily_mud_cost').on('change', function () {
        //        var newDailyMudCost = $(this).val().replace(/,/g, '');
        //        var DailyMudCost = cost.replace(/,/g, '');
        //        var newValue = parseFloat(DailyMudCost) + parseFloat(newDailyMudCost);
        //        $("#cummulative_mud_cost").val(thousandSeparatorWithoutComma(newValue));
        //    });
        //}

        $('#current_depth_md').on('change', function () {
            calculateProgressDepth();
        });

        var calculateProgressDepth = function () {
            var currentDepthMD = $("#current_depth_md").val().replace(/,/g, '');
            var previousDepthMD = $("#previous_depth_md").val().replace(/,/g, '');
            var result = currentDepthMD - previousDepthMD;
            $("#progress").val(thousandSeparatorFromValueWithComma(result));
        };

        $('#form-Drilling-top')[0].scrollIntoView();



        return {
            init: function () {
                loadDetail();
                mudTypeLookup();
                //$(":input").inputmask();
                getUrlParameter();

                getDailyCost();
                getDrilOperationData();


            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
        $(document).on("change", ".form-time-shipping, .date-picker-shipping", function () {
            var dp = $(this).parents(".rowOperation").find(".date-picker-shipping");
            var tp = $(this).parents(".rowOperation").find(".form-time-shipping");
            var day = $(this).parents(".rowOperation").find(".day");
            var dpDeparted = $(dp[0]).val();
            var dpEta = $(dp[1]).val();
            var tpDeparted = $(tp[0]).val();
            var tpEta = $(tp[1]).val();
            if (dpDeparted && dpEta && tpDeparted && tpEta) {
                var day1 = moment(dpDeparted + " " + tpDeparted, 'MM/DD/YYYY HH:mm')
                var day2 = moment(dpEta + " " + tpEta, 'MM/DD/YYYY HH:mm')
                var diff = day2.diff(day1, 'days', true);
                $(day).text(diff.toFixed(2));
            }
        });
    });
});