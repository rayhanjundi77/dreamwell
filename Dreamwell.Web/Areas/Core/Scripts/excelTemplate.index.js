﻿(function ($) {
    'use strict';
    var pageFunction = function () {

        var LoadData = function () {
            $.ajax({
                type: "GET",
                dataType: 'json',
                contentType: 'application/json',
                url: $.helper.resolveApi("~/core/ExcelTemplate/List"),
                success: function (r) {
                    if (r.status.success) {
                        var template = Handlebars.compile($("#list_template").html());
                        $('#js_default_list>ul').html(template({ items: r.data}));

                    }
                },
                error: function (err) {
                    console.error(err);
                }
            });

        }


        return {
            init: function () {
                LoadData();
            }
        }
    }();

    $(document).ready(function () {
        initApp.listFilter($('#js_default_list'), $('#js_default_list_filter'));
        pageFunction.init();
    });
}(jQuery));