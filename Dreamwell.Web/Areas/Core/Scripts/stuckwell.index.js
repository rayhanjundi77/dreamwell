﻿$(document).ready(function () {
	'use strict';

	var $btnDownload = $('#btnDownloadFile');
	var valueAppType = "";
	var exportAppType = "";
	var $aphId = "";
	var $fieldId = "";
	var controls = {
		leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
		rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
	}

	var pageFunction = function () {
		//totalhasilkesimpulan

		$("#answerone").change(function () {

			console.log("change the position " + this.value);
			if (this.value == "0") {
				$('#pergerakan_pipa_satu').html('2');
				$('#pergerakan_pipa_dua').html('0');
				$('#pergerakan_pipa_tiga').html('2');
			}
			else if (this.value == "1") {
				$('#pergerakan_pipa_satu').html('0');
				$('#pergerakan_pipa_dua').html('0');
				$('#pergerakan_pipa_tiga').html('2');
			}
			else if (this.value == "2") {
				$('#pergerakan_pipa_satu').html('1');
				$('#pergerakan_pipa_dua').html('0');
				$('#pergerakan_pipa_tiga').html('2');
			}
			else if (this.value == "3") {
				$('#pergerakan_pipa_satu').html('0');
				$('#pergerakan_pipa_dua').html('0');
				$('#pergerakan_pipa_tiga').html('2');
			}
			else if (this.value == "4") {
				$('#pergerakan_pipa_satu').html('2');
				$('#pergerakan_pipa_dua').html('2');
				$('#pergerakan_pipa_tiga').html('0');
			}
			
		
			calculatethevalue(); 
		});


	
		$("#answersecond").change(function () {

			console.log("change  " + this.value);
			if (this.value == "0") {
				$('#pipa_terjepit_satu').html('0');
				$('#pipa_terjepit_dua').html('0');
				$('#pipa_terjepit_tiga').html('0');
			}
			else if (this.value == "1") {
				$('#pipa_terjepit_satu').html('1');
				$('#pipa_terjepit_dua').html('0');
				$('#pipa_terjepit_tiga').html('2');
			}
			else if (this.value == "2") {
				$('#pipa_terjepit_satu').html('0');
				$('#pipa_terjepit_dua').html('0');
				$('#pipa_terjepit_tiga').html('0');
			}
			calculatethevalue(); 
		});

		$("#answerthird").change(function () {

			console.log("this  " + this.value);
			if (this.value == "0") {
				$('#pipa_perputaran_satu').html('0');
				$('#pipa_perputaran_dua').html('0');
				$('#pipa_perputaran_tiga').html('2');
			}
			else if (this.value == "1") {
				$('#pipa_perputaran_satu').html('2');
				$('#pipa_perputaran_dua').html('0');
				$('#pipa_perputaran_tiga').html('2');
			}
			else if (this.value == "2") {
				$('#pipa_perputaran_satu').html('0');
				$('#pipa_perputaran_dua').html('0');
				$('#pipa_perputaran_tiga').html('0');
			}
			calculatethevalue(); 
		});

		$("#answerfourth").change(function () {

			console.log("this  " + this.value);
			if (this.value == "0") {
				$('#pipa_tekanan_satu').html('0');
				$('#pipa_tekanan_dua').html('2');
				$('#pipa_tekanan_tiga').html('2');
			}
			else if (this.value == "1") {
				$('#pipa_tekanan_satu').html('2');
				$('#pipa_tekanan_dua').html('0');
				$('#pipa_tekanan_tiga').html('0');
			}
			else if (this.value == "2") {
				$('#pipa_tekanan_satu').html('2');
				$('#pipa_tekanan_dua').html('0');
				$('#pipa_tekanan_tiga').html('0');
			}
			calculatethevalue(); 
		});


		function calculatethevalue() {
			var pergerakan_pipa_satu = $("#pergerakan_pipa_satu").text();
			var pipa_terjepit_satu = $("#pipa_terjepit_satu").text();
			var pipa_perputaran_satu = $("#pipa_perputaran_satu").text();
			var pipa_tekanan_satu = $("#pipa_tekanan_satu").text();

			var total_value_satu = parseInt(pergerakan_pipa_satu) + parseInt(pipa_terjepit_satu) + parseInt(pipa_perputaran_satu) + parseInt(pipa_tekanan_satu);

			console.log("total value satu " + total_value_satu);
			$('#totalsatu').html(total_value_satu);




			var pergerakan_pipa_dua = $("#pergerakan_pipa_dua").text();
			var pipa_terjepit_dua = $("#pipa_terjepit_dua").text();
			var pipa_perputaran_dua = $("#pipa_perputaran_dua").text();
			var pipa_tekanan_dua = $("#pipa_tekanan_dua").text();

			var total_value_dua = parseInt(pergerakan_pipa_dua) + parseInt(pipa_terjepit_dua) + parseInt(pipa_perputaran_dua) + parseInt(pipa_tekanan_dua);

			console.log("total value dua " + total_value_dua);
			$('#totaldua').html(total_value_dua);




			var pergerakan_pipa_tiga = $("#pergerakan_pipa_tiga").text();
			var pipa_terjepit_tiga = $("#pipa_terjepit_tiga").text();
			var pipa_perputaran_tiga = $("#pipa_perputaran_tiga").text();
			var pipa_tekanan_tiga = $("#pipa_tekanan_tiga").text();

			var total_value_tiga = parseInt(pergerakan_pipa_tiga) + parseInt(pipa_terjepit_tiga) + parseInt(pipa_perputaran_tiga) + parseInt(pipa_tekanan_tiga);

			console.log("total value totaltiga " + total_value_tiga);
			$('#totaltiga').html(total_value_tiga);




			var totalValue = [];
			var index = 0;
			var objectsatu = {
				nilai: total_value_satu,
				index : "PACK-OFF/BRIDGE"
            }
			totalValue.push(objectsatu);

			var objectdua = {
				nilai: total_value_dua,
				index: "DIFFERENTIAL STICKING"
			}
			totalValue.push(objectdua);

			var objecttiga = {
				nilai: total_value_tiga,
				index: "WELLBORE GEOMETRY"
			}
			totalValue.push(objecttiga);

			var maximumValue = totalValue.sort((a, b) => b.nilai - a.nilai);

			let getpoint = maximumValue[0];


			console.log("total valuenya adlaah " + getpoint.nilai + " total unde" + getpoint.index);


			var labelresulut = "Kemungkinan mekanisme jepitan pipa adalah kolom dengan total nilai terbanyak. Yaitu: " + getpoint.index + " (Total Nilai " + getpoint.nilai + ")";
			$('#totalhasilkesimpulan').html(labelresulut);
		
			
		}





		$btnDownload.on('click', function () {

			var doc = new jsPDF();
			doc.fromHTML($('#content').html(), 15, 15, {
				'width': 170
			});
			doc.save('sample-file.pdf');
		});

		var download = function (date) {
			var aryWell = [$well_id];
			//$.each($("input[name='wellId[]']:checked"), function () {
			//	aryWell.push($(this).val());
			//});

			var wellid = '';
			if (aryWell.length > 0) {
				wellid = aryWell[0];
			}

			var type = $('#export_type').find(":selected").val()
			var url = $.helper.resolveApi('~utc/Exim/Download/' + date + '/' + wellid + '/' + type + '?x-Token=' + $('meta[name=x-token]').attr("content"));
			$('<a href="' + url + '" target="blank"></a>')[0].click();
			//window.location.href = $.helper.resolveApi('~utc/Exim/Download/' + date + '/' + wellid + '?x-Token=' + $('meta[name=x-token]').attr("content"));
		};

		return {
			init: function () {
		
			}
		}
	}();

	$(document).ready(function () {
		pageFunction.init();
	});


});
