﻿$(document).ready(function () {
	'use strict';



	var pageFunction = function () {
        $("#cloneDrillingDeviation").on('click', function () {
            var templateScript = $("#rowDrillingDeviationDaily-template").html();
            var template = Handlebars.compile(templateScript);
            $('#tableDrillingDeviation > tbody tr.no-data').remove();
            var lastIndex = parseInt($('#tableDrillingDeviation tbody>tr:last').data('idx') || 0);
            lastIndex++;


            $("#tableDrillingDeviation > tbody").append(template({ idx: lastIndex }));
            maskingMoney();

            //$('#tableDrillingDeviation >.row-delete').on('click', function () {
            //    alert('test');
            //    $(this).closest('tr').remove();
            //});
        });

        $(document).on('click', '#tableDrillingDeviation .row-delete', function () {
            $(this).closest('tr').remove();
        });

        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

		return {
			init: function () {

			}
		}
	}();

	$(document).ready(function () {
		pageFunction.init();
	});


});
