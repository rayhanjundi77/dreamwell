﻿(function ($) {
    'use strict';
    var $userListContent = $('#page-user-content');
    var pagination = $('#pagination');

    var $recordId = $('input[id=recordId]');
    var $dt_basic = $('#dt_basic');
    var dt = undefined;
    var $form = $('#form-applicationUser');
    var $pageLoading = $('#page-loading');
    var $btnSave = $('#btn-save');
    var $btnDelete = $('#btn-delete');



    var pageFunction = function () {
        $('a[data-toggle="tooltip"].set-role-all').each(function (i, obj) {
            $(this).click(function () {
                var access = $(this).data('access');
                setAllEntity(access);
            })
        });

        var setAllEntity = function (access) {
            $.ajax({
                type: "POST",
                dataType:"json",
                contentType: 'application/json',
                url: $.helper.resolveApi("~/core/ApplicationRole/setRoleAllEntity?roleId=" + $recordId.val()+"&access="+access),
                success: function (r) {
                    if (r.status.success) {
                        toastr.success("Entities has been set access use this role selected");
                        $dt_basic.DataTable().ajax.reload();
                    } else {
                        toastr.error(r.statusText);
                    }                    
                },
                error: function (r) {
                    toastr.error(r.statusText);
                }
            });
        }        

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/ApplicationRole/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#role_name').text(':: ' + r.data.role_name);
                    }
                }).fail(function (r) {
                    toastr.error(r.statusText);
                });
            }
        }

        function accessTemplate(data, field) {
            var output = '<input  type="text" name="' + field + '" data-id="' + data.id + '" value="' + data[field] + '" class="entity-pie form-control form-control-sm m-input" />';
            return output;
        }

        var updateAccess = function (entityID, name, value) {
            var obj = {};
            var RoleAccessId = $recordId.val();
            switch (name) {
                case "access_create":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_create: value
                    }
                    break;
                case "access_read":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_read: value
                    }
                    break;
                case "access_update":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_update: value
                    }
                    break;
                case "access_delete":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_delete: value
                    }
                    break;
                case "access_approve":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_approve: value
                    }
                    break;
                case "access_lock":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_lock: value
                    }
                    break;
                case "access_activate":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_activate: value
                    }
                    break;
                case "access_append":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_append: value
                    }
                    break;
                case "access_share":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_share: value
                    }
                    break;
                case "access_level":
                    obj = {
                        id: entityID,
                        application_role_id: RoleAccessId,
                        access_level: value
                    }
                    break;

            }

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                url: $.helper.resolveApi("~/core/ApplicationRole/roleAccess/update"),
                data: JSON.stringify(obj),
                success: function (r) {
                    if (!r.status.success) {
                        toastr.error(r.status.message);
                    }
                },
                error: function (r) {
                    toastr.error(r.statusText);
                }
            });
        };


        var loadDataTable = function () {
            dt = $dt_basic.cmDataTable({
                searchDelay: 1000,
                pageLength: 10,
                "dom": '<"newtoolbar">frtip',
                ajax: {
                    url: $.helper.resolveApi("~/core/ApplicationRole/" + $recordId.val() + "/roleAccess"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                order: [[1, "asc"]],
                columns: [
                    /*BEGIN::row number*/
                    {
                        data: "id",
                        width: "30px",
                        orderable: false,
                        searchable: false,
                        class: "fw-500",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    /*END::row number*/
                    {
                        data: "display_name",
                        width: "250px",
                        class: "fw-500",
                    },
                    {
                        data: "access_create",
                        class: "text-center",
                        orderable: true,
                        render: function (data, type, row) {
                            return accessTemplate(row, "access_create");
                        }
                    },
                    {
                        data: "access_read",
                        class: "text-center",
                        render: function (data, type, row) {
                            return accessTemplate(row, "access_read");
                        }
                    },
                    {
                        data: "access_update",
                        class: "text-center",
                        render: function (data, type, row) {
                            return accessTemplate(row, "access_update");
                        }
                    },
                    {
                        data: "access_delete",
                        class: "text-center",
                        render: function (data, type, row) {
                            return accessTemplate(row, "access_delete");
                        }
                    },
                    //{
                    //    data: "access_approve",
                    //    class: "text-center",
                    //    render: function (data, type, row) {
                    //        return accessTemplate(row, "access_approve");
                    //    }
                    //},
                    {
                        data: "id",
                        class: "text-center",
                        width: "130px",
                        render: function (data, type, row) {
                            var output = `
                                <button class="btn btn-sm btn-icon btn-circle"><pie class="ten icon-sm"></pie></button>
                                <button class="btn btn-sm btn-icon btn-circle"><pie class="twentyfive icon-sm"></pie></button>
                                <button class="btn btn-sm btn-icon btn-circle"><pie class="fifty icon-sm"></pie></button>
                                <button class="btn btn-sm btn-icon btn-circle"><pie class="seventyfive icon-sm"></pie></button>
                                <button class="btn btn-sm btn-icon btn-circle"><pie class="onehundred icon-sm"></pie></button>`;
                            return output;
                        }
                    }
                ],
                drawCallback: function (settings, json) {
                    $('input.entity-pie').closest('td').find('a').remove();
                    $('input.entity-pie').cmRole(function (input) {
                        var entityID = $(input).attr('data-id');
                        var name = $(input).attr('name');
                        var value = $(input).val();
                        updateAccess(entityID, name, value);
                    });

                }
            }, function (a, b, c) { });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                //    $pageLoading.niftyOverlay('show');
                } else {
                //    $pageLoading.niftyOverlay('hide');
                }
            });
        }


        /*BEGIN::Event*/

        /*END::Event*/

        return {
            init: function () {
                loadDetail();
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));