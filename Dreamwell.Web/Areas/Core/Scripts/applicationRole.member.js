﻿(function ($) {
    'use strict';
    //$('#container').addClass("aside-bright aside-bright aside-left aside-fixed aside-in ");
    $('#container').addClass("page-fixedbar");

    var $userListContent = $('#page-user-content');
    var pagination = $('#pagination');

    var $recordId = $('input[id=recordId]');
    var $dt_basic = $('#dt_basic');
    var dt = undefined;
    var $form = $('#form-applicationUser');
    var $pageLoading = $('#page-loading');
    var $btnSave = $('#btn-save');
    var $btnDelete = $('#btn-delete');

    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/ApplicationRole/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#role_name').text(':: ' + r.data.role_name);
                    }
                }).fail(function (r) {
                    toastr.error(r.statusText);
                });
            }
        }

        var loadDataTable = function () {
            dt = $dt_basic.cmDataTable({
                searchDelay: 1000,
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/ApplicationRole/" + $recordId.val() + "/member"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        console.log(d);
                        return JSON.stringify(d);
                    }
                },
                order: [[1, "asc"]],
                columns: [
                    /*BEGIN::row number*/
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    /*END::row number*/
                    {
                        data: "app_username",
                        orderable: true,
                        class: "list-group-item",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                var img = (row.gender == true ? "/Content/img/profile-photos/5.png" : "/Content/img/profile-photos/8.png");
                                output = `<div class="d-flex flex-row">
                                            <span class="status status-danger">
                                                <img class="profile-image rounded-circle" src="`+ img + `" alt="Profile Picture">
                                            </span>
                                            <div class="ml-3">
                                                <a href="javascript:void(0);" title="{{app_fullname}}" class="d-block fw-700 text-dark">`+ row.app_username + `</a>
                                                <span>`+ (row.email != null ? row.email : 'N/A') + `</span>
                                            </div>
                                        </div>`;
                                return output;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                if (row.is_active) {
                                    output = `<span class="badge badge-success">Active</span>`;
                                } else {
                                    output = `<span class="badge badge-warning">Inactive</span>`;
                                }
                                return output;
                            }
                            return data;
                        }
                    },                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class="btn-group" data-id="`+ row.id + `">
                                    <a class="row-deleted btn btn-sm btn-warning btn-hover-warning fa fa-trash add-tooltip" href="#"
                                        data-original-title="Delete" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }

                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/ApplicationRole/removeMember"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.success("Data has been deleted");
                                                $dt_basic.DataTable().ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            reloadPlugin();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                   // $pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            });
        }

        pagination.data('twbs-pagination.js', null);

        var templateScript = $("#member-template").html();
        var template = Handlebars.compile(templateScript);
      
        var pagePlugin = function (options) {
            var o = $userListContent;
            var dataSource = {
                pageSize: 3,
                page: 1,
                filter: {
                    logic: "OR",
                    filters: [
                        {
                            field: "app_fullname",
                            operator: "contains",
                            value: $('input[name=term]').val()
                        },
                        {
                            field: "app_email",
                            operator: "contains",
                            value: $('input[name=term]').val()
                        }
                    ]
                }
            }
            options.dataSource = $.extend({}, dataSource, options.dataSource);
            var load = function (opt) {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: opt.url,
                    data: JSON.stringify(opt.dataSource),
                    success: function (data) {
                        if (data.items.length > 0) {
                            o.html(template(data));
                            $('.row-selected').click(function () {
                                var recordId = $(this).data("id");
                                console.log(recordId);
                                addMember(recordId);
                            });
                        } else {
                            o.empty();
                        }
                    },
                    error: function (err) {
                        console.error(err);
                    }
                });
            };

            var addMember = function (userId) {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi('~/core/applicationRole/addMember'),
                    data: JSON.stringify({
                        application_role_id: $recordId.val(),
                        application_user_id: userId
                    }),
                    success: function (r) {
                        if (r.status.success) {
                            $dt_basic.DataTable().ajax.reload();
                            reloadPlugin();
                        } else {
                            bootbox.alert({
                                message: "<p class='text-semibold text-main'>Information</p><p>" + r.status.message + "</p>",
                                callback: function (result) {
                                    //Callback function here
                                },
                                animateIn: 'bounceIn',
                                animateOut: 'bounceOut'
                            });
                        }

                    },
                    error: function (err) {
                        console.error(err);
                    }
                });

            }

            load(options);
            return {
                refresh: function (opt) {
                    load(opt);
                }
            }
        };
        var reloadPlugin = function () {
            pagePlugin({
                url: $.helper.resolveApi("~/core/ApplicationRole/" + $recordId.val() + "/userAvailable"),
                dataSource: {
                    pageSize: 100
                }
            });
        }


        $('#btn-search-user').click(function () {
            reloadPlugin();
        });

        /*BEGIN::Event*/
        $btnSave.on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~core/ApplicationUser/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordId);
                            $('input[name=id]').val(r.data.recordId);
                            toastr.success(r.status.message);
                            loadDetail();
                        } else {
                            btn.button('reset');
                            toastr.error(r.status.message);
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });



            }
            else return;
        });

        /*END::Event*/

        return {
            init: function () {
                reloadPlugin(),
                    loadDetail(),
                    loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));