$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=well_hole_and_casing_id]');
    var $wellid = $('input[id=well_id]');
    var $form = $("#form-casinggNew")

    var $btnSave = $("#btn_save-casing");

    var ObjectMapper = [];

    var holeCasingTypeLookup = function (selectedId) {
        if (selectedId == "") {
            $(".hole-casing-tipe").val('');
        } else {
            $(".hole-casing-tipe").val(selectedId);
        }
        $(".hole-casing-tipe").select2({
            dropdownParent: $form,
            placeholder: "Select Casing Type",
            allowClear: true
        });
    }

    var pageFunction = function () {
        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            $(".numeric").inputmask({
                digits: 0,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

        var GetWellObjectUomMap = function () {

            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $wellid.val()), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        ObjectMapper = r.data;
                        $("form#form-wellHoleCasing :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                $.each(r.data, function (key, value) {
                                    if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                        $("#" + elementId).val("" + value.uom_code + "");
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        var holeAndCasingLookup = function (id) {
            $(".holeandcasinglookup").cmSelect2({
                url: $.helper.resolveApi("~/core/wellholeandcasing/Multilookup?wellId=" + $wellid.val()),  // Corrected the URL concatenation here
                result: {
                    id: 'id',
                    casing_name: 'casing_name'
                },
                filters: function (params) {
                    return [{
                        field: "casing_name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select Hole and Casing",
                    allowClear: true,
                    templateResult: function (repo) {
                        return " <b>Casing Name:</b> " + repo.casing_name;
                    },
                }
            });
        }

        $(".holeandcasinglookup").on('select2:select', function (e) {
            if (e.params.data.hole_depth) {
                $("#section_casing").hide();
                $("#section_casing input.form-control-input, #section_casing select").removeAttr("required");
                $("#section_casing input.form-control-input").val("");
            } else {
                $("#section_casing").show();
                $("#section_casing input.form-control-input, #section_casing select").attr("required", "");
            }
            getMulticasing(e.params.data.id)

        });
        Handlebars.registerHelper('printHoleCasingType', function (type, id) {
            var html = `<select class="form-control hole-casing-tipe" name="casing_type" data-field="casing_type" data-text required>`;
            var casing_type_1 = "";
            if (casing_type == 1) {
            } else if (casing_type == 2) {
                casing_type_name = "Surface";
            } else if (casing_type == 3) {
                casing_type_name = "Drilling Liner";
            } else if (casing_type == 4) {
                casing_type_name = "Intermediate";
            } else if (casing_type == 5) {
                casing_type_name = "Production Liner";
            } else if (casing_type == 6) {
                casing_type_name = "Perforated Liner";
            } else if (casing_type == 7) {
                casing_type_name = "Production";
            }
            html += `</select>`;

            html = ` <select class="form-control hole-casing-tipe" name="casing_type" data-field="casing_type" data-text required>
                        <option value="1">Conductor</option>
                        <option value="2">Surface</option>
                        <option value="3">Drilling Liner</option>
                        <option value="4">Intermediate</option>
                        <option value="5">Production Liner</option>
                        <option value="6">Perforated Liner</option>
                        <option value="7">Production</option>
                    </select>`;
            html += "<div class=\"invalid-feedback\">Casing type cannot be empty</div>";
            return new Handlebars.SafeString(html);
        });

        var getMulticasing = function (id) {
            //console.log("Fetching details for ID:", id); // Better console log message
            $.get($.helper.resolveApi('~/core/wellholeandcasing/' + id + '/detail'), function (r) {
                console.log("Data received:", r.data);
                // Assuming `r.data` contains all the necessary details
                $('#id').val(r.data.id);
                $('#hole_name').val(r.data.hole_name);
                $('#hole_depth').val(r.data.hole_depth);
                $('#plan_cost').val(r.data.plan_cost);
                $('#plan_volume').val(r.data.plan_volume);
                $('#casing_name').val(r.data.casing_name);
                $('#casing_setting_depth').val(r.data.casing_setting_depth);
                $('#casing_type').val(r.data.casing_type);
                $('#casing_weight').val(r.data.casing_weight);
                $('#casing_grade').val(r.data.casing_grade);
                $('#casing_connection').val(r.data.casing_connection);
                $('#casing_top_of_liner').val(r.data.casing_top_of_liner);
                $("#multicasing").val(1); // Set multicasing value
            });
        }


       

        $btnSave.on('click', function () {
            var btn = $btnSave;
            var data = $form.serializeToJSON();
            console.log(data);

            var isValidate = $form[0].checkValidity();
            console.log(isValidate);
            if (isValidate) {
                Swal.fire({
                    title: "",
                    text: "Are you sure save to create a Hole and Casing?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes"
                }).then(function (result) {
                    if (result.value) {
                        $.post($.helper.resolveApi('~/core/wellholeandcasing/updatecasing'), data, function (r) {
                            if (r.status.success) {
                                btn.button('reset');
                                window.location.reload();
                                toastr.success(r.status.message)
                            } else {
                                toastr.error(r.status.message)
                            }
                            btn.button('reset');
                        }, 'json').fail(function (r) {
                            btn.button('reset');
                            toastr.error(r.statusText);
                        });
                    }
                });

            } else {
                $form.addClass('was-validated');
                toastr.error("Please complete all form Hole and Casing fields.");
                event.preventDefault();
                event.stopPropagation();
            }

        })

        var loadDetail = function () {
            $("#section_casing").show();
            $("#section_casing input, #section_casing select").attr("required", "");

             
        };

        return {
            init: function () {
                holeAndCasingLookup();
                GetWellObjectUomMap();
                maskingMoney();
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});