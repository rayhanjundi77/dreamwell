﻿(function ($) {
    'use strict';

    var $dt_listRkap = $('#dt_listRkap');
    var $pageLoading = $('#page-loading-content');
    var $rkapModal = $('#rkapModal');
    var $rkapHistory = $('#rkapHistory');
    var $rkapRealisasi = $('#rkapRealisasi');
    var $form = $('#form-rkap');
    var $formReal = $('#form-rkapRealisasi');
    var $recordId = $('input[id=recordId]');
    var $bussinessunitID = $('input[id=bussiness_unit_id]');
    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    }


    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };

    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            currency_code: {
                validators: {
                    notEmpty: {
                        message: 'The currency code is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION
    $formReal.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            currency_code: {
                validators: {
                    notEmpty: {
                        message: 'The currency code is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    var pageFunction = function () {

        var cekData = function (fieldId) {
            $.ajax({
                url: $.helper.resolveApi('~/core/Rkap/' + fieldId + '/getWellRkap'),
                type: "GET",
                contentType: "application/json",
                dataType: "json", // Tipe data yang diharapkan dari respon
                data: function (d) {
                    console.log("sasas", d)
                    //return JSON.stringify(d);
                },
                success: function (response) {
                    console.log("Success:", response); // Log response jika request berhasil
                },
                error: function (xhr, status, error) {
                    console.error("Error:", error); // Log error jika request gagal
                }
            });
        }

        var loadDataTable = function () {

            var dt = $dt_listRkap.cmDataTable({
                pageLength: 10,
                paging: true,
                lengthChange: true,
                ajax: {
                    url: $.helper.resolveApi("~/core/Rkap/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        console.log("asasawdwdwd", d); // Pindahkan kode console.log ke sini
                        return JSON.stringify(d);
                    }
                },

                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "well_name",
                        render: function (data, type, row) {
                            console.log("Rendering well_name:", data);  // Debug output
                            if (type === 'display') {
                                return "<span>" + data + "</span>";  // Ensure no additional text is added
                            }
                            return data;
                        }
                    },
                    { data: "well_classification" },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.end_date == null)
                                    return "<span>N/A</span>";
                                else
                                    return "<span>" + moment(row.spud_date).format('DD MMM YYYY') + "</span>";
                            }
                            return data;
                        }
                    },
                    {
                        data: "realization_date",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (data == null) {
                                    var buttonsHtml = '<div class="btn-group" data-id="' + row.id + '">' +
                                        '<button id="btn-real" data-id="' + row.id + '" class="btn btn-success btn-sm ml-2 row-realisasi">' +
                                        '<span class="fal fa-plane-alt mr-1"></span>Realisasi' +
                                        '</button>';

                                    buttonsHtml += '</div>';

                                    return buttonsHtml;
                                } else if (data !== null) {
                                    return "<span> Terealisasi </span>";
                                } else {
                                    return ''; // Kondisi default jika nilai status tidak sesuai
                                }
                            }
                            return data;
                        }
                    },
                    {
                        data: "status",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (data === 1) {
                                    var buttonsHtml = '<div class="btn-group" data-id="' + row.id + '">' +
                                        '<button class="btn btn-info btn-sm ml-2 row-history">Lihat History</button>';
                                    buttonsHtml += '</div>';
                                    return buttonsHtml;
                                } else if (data === 0) {
                                    return '<button class="btn btn-warning text-center">Rencana Awal</button>';
                                } else {
                                    return ''; // Kondisi default jika nilai status tidak sesuai
                                }
                            }
                            return data;
                        }


                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.id + `" >
                                    <a class ="row-edit btn btn-xs btn-info btn-hover-info fa fa-pencil add-tooltip" href="#"
                                        data-original-title="Edit" data-container="body">
                                        Proyeksi
                                    </a>
                                    <a class="row-deleted btn btn-xs btn-warning btn-hover-warning fa fa-trash add-tooltip" href="#"
                                        data-original-title="Delete" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        },
                        width: "10%"
                    }
                ],
                initComplete: function (settings, json) {
                    var $dataTable = $(this);
                    $dataTable.on('click', '.row-history', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        //console.log(recordId);
                        $.get($.helper.resolveApi('~/core/rkap/' + recordId + '/detailHistory'))
                            .done(function (response) {

                                //cekData(response.)
                                //console.log("asasa", response.data.field_id)
                                if (response.status.success) {
                                    // Clear existing table rows
                                    $('#historyTable tbody').empty();
                                    // Populate table with data
                                    //console.log("ini fiels", response.data.field_id)
                                    response.data.forEach(function (item) {
                                        //console.log("ini datanya", item.field_id)
                                        var statusButton = '';
                                        if (item.status === 0) {
                                            statusButton = '<button class="btn btn-danger">Rencana Awal</button>';
                                        } else if (item.status === 1) {
                                            statusButton = '<button class="btn btn-info">Proyeksi</button>';
                                        } else if (item.status === 2) {
                                            statusButton = '<button class="btn btn-success">Realisasi</button>';
                                        } // Construct the table row with the status button

                                        console.log("spud_date value:", item.spud_date); // Log the spud_date value

                                        var formattedDate = 'N/A'; // Default value in case of an invalid date
                                        if (item.spud_date) {
                                            var momentDate = moment(item.spud_date);
                                            if (momentDate.isValid()) {
                                                formattedDate = momentDate.format('DD MMM YYYY');
                                            } else {
                                                console.error("Invalid date format:", item.spud_date);
                                            }
                                        }
                                        var newRow = '<tr>' +
                                            '<td>' + item.well_name + '</td>' +
                                            '<td>' + formattedDate + '</td>' +
                                            '<td>' + statusButton + '</td>' +
                                            // Add more columns as needed
                                            '</tr>';
                                        $('#historyTable tbody').append(newRow);
                                    });
                                    $rkapHistory.modal('show');
                                } else {
                                    toastr.error(response.status.message);
                                }
                            })
                            .fail(function (xhr) {
                                toastr.error(xhr.statusText);
                            });
                    });

                    $dataTable.on('click', '.row-realisasi', function () {
                        var btn = $(this);
                        var recordId = $(this).closest('.btn-group').data('id');
                        btn.button('loading');
                        $.get($.helper.resolveApi('~/core/rkap/' + recordId + '/detail'))
                            .done(function (response) {
                                if (response.status.success) {
                                    if (response.data.status === 0) {
                                        toastr.warning("Harap Lakukan Proyeksi Terlebih dahulu");
                                    } else {
                                        $('#header_id').val(response.data.id);
                                        $('#header_name').text(response.data.well_name);
                                        $rkapRealisasi.modal('show');
                                    }
                                    //$.helper.form.fill($formReal, response.data);// buatkan untuk mengisi header data nya saja mengambil id nya

                                    btn.button('reset');
                                } else {
                                    toastr.error(response.status.message);
                                }
                            })
                            .fail(function (xhr) {
                                btn.button('reset');
                                toastr.error(xhr.statusText);
                            });
                    });

                    // Meng-handle klik pada tombol edit
                    $dataTable.on('click', '.row-edit', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        $.get($.helper.resolveApi('~/core/rkap/' + recordId + '/detail'))
                            .done(function (response) {
                                if (response.status.success) {
                                    console.log("asasa's", response.data.rig_id)
                                    $.helper.form.fill($form, response.data);
                                    $('#business_unit_id').val(response.data.business_unit_id);
                                    $('#field_id').val(response.data.field_id);
                                    $rkapModal.modal('show');
                                } else {
                                    toastr.error(response.status.message);
                                }
                            })
                            .fail(function (xhr) {
                                toastr.error(xhr.statusText);
                            });
                    });
                    $dataTable.on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        console.log("ini", recordId)
                        bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are you sure?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfirm = $('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfirm.button('loading');
                                    $.post($.helper.resolveApi("~/core/rkap/delete"), { id: recordId }, function (response) {
                                        $dataTable.DataTable().ajax.reload();
                                        toastr.success("Data has been deleted");
                                    }).fail(function (xhr) {
                                        toastr.error(xhr.statusText);
                                    }).always(function () {
                                        btnConfirm.button('reset');
                                    });
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            });
        }
        // Menonaktifkan setiap elemen input di dalam   
        function disableFormInputs() {
            $('#form-rkapRealisasi input').prop('readonly', true);
            $('#form-rkapRealisasi select').prop('readonly', true);
            $('#form-rkapRealisasi textarea').prop('readonly', true);
        }
        function showLoading(button) {
            $(button).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
        }
        function hideLoading(button) {
            button.html('Realisasi'); // Kembalikan teks tombol menjadi semula
            // button.html('Proyeksi'); // Kembalikan teks tombol menjadi semula
        }

        function getData(fieldId) {
            $.get($.helper.resolveApi('~/core/rkap/' + fieldId + '/getWellRkap'))
                .done(function (response) {
                    if (response.status.success) {
                        console.log("ini drororor", response.data);
                        // Mengisi form dengan data yang diterima
                        //$.helper.form.fill($('#form-rkapRealisasi'), response.data);
                        wellFill(fieldId);
                        $rkapRealisasi.modal('show');
                    }
                })
                .fail(function (xhr) {
                    console.log("gagal");
                    toastr.error(xhr.statusText);
                })
                .done(function () {
                    hideLoading($('.row-realisasi'));

                    // Kode tambahan untuk menangani setelah permintaan selesai
                });
        }

        var loadCombo = function () {
            /*
            $("#header_id").cmSelect2({
                url: $.helper.resolveApi('~/core/rkap/dataTable'),
                result: {
                    id: 'id',
                    text: 'well_name'
                },
                filters: function (params) {
                    return [{
                        field: "header_id",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "Select a Rkap",
                    allowClear: true,
                    maximumSelectionLength: 2,
                    dropdownParent: $rkapRealisasi
                }
            });
            $("#header_id").on('select2:select', function (e) {
                var selectedHeaderId = e.params.data.id; // Mengambil nilai header_id yang dipilih
                console.log(selectedHeaderId);
                // Gunakan nilai selectedHeaderId sesuai kebutuhan, misalnya menyimpannya ke dalam variabel atau melakukan tindakan lain.
            });
            */
            $("#well_id").cmSelect2({
                url: $.helper.resolveApi('~/core/well/lookupWellField'),
                result: {
                    id: 'id',
                    text: 'well_name'
                },
                filters: function (params) {
                    return [{
                        field: "well_name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "Select a Well",
                    allowClear: true,
                    maximumSelectionLength: 2,
                    dropdownParent: $rkapRealisasi
                }
            });

            $("#well_id").on('select2:select', function (e) {
                var id = e.target.value;
                var name = e.params.data.text;
                var region = e.params.data.reg;
                var rig = e.params.data.rig;
                //console.log('sasawwwqwq',e.params.data.reg);
                //console.log(name);
                getByWellId(id, name, region, rig);
                getNewRkapReal(id)
            });

            $("#well_classification").val('');
            $("#well_classification").select2({
                placeholder: "Select a Well Classification",
                allowClear: true,
                dropdownParent: $rkapModal
            });

            $("select[id='well_classification']").on('change', function () {
                console.log("Well classification changed");
                $("well_name").val('');
                $("well_name").trigger('change');
                loadAPH(['']);
                loadFields([''], '0');
                if ($(this).val() == 0) {
                    $(".row-parent").show();
                    setTimeout(function () {
                        $(".row-parent").find('.textbox.easyui-fluid.combo').css('width', '100%');
                        $(".row-parent").find('.textbox-text.validatebox-text').css('width', '100%');
                    }, 200);
                } else {
                    $(".row-parent").hide();
                }
                loadWell($(this).val());
            });


            $("#rig_id").cmSelect2({
                url: $.helper.resolveApi('~/core/rig/lookup'),
                result: {
                    id: 'id',
                    text: 'name'
                },
                filters: function (params) {
                    return [{
                        field: "name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "Select a Rig",
                    allowClear: true,
                    maximumSelectionLength: 2,
                    dropdownParent: $rkapModal
                }
            });
            $("#rig_id").on('select2:select', function (e) {
                console.log(e);
            });
        }

        var loadWell = function (status) {
            if (status != null) {
                if (status == 0) {
                    $("well_name").prop('disabled', 'disabled');
                } else {
                    $("well_name").removeAttr('disabled');
                    $("well_name").cmSelect2({
                        url: $.helper.resolveApi('~/core/well/lookup'),
                        result: {
                            id: 'id',
                            text: 'well_name'
                        },
                        filters: function (params) {
                            return [{
                                field: "well_name",
                                operator: "contains",
                                value: params.term || '',
                            }];
                        },
                        options: {
                            destroy: true,
                            dropdownParent: $form,
                            placeholder: "Select a Well",
                            allowClear: true,
                        }
                    });

                    $("well_name").on('select2:select', function (e) {
                        var id = e.target.value;
                        getAphField(id);
                    });
                }
            }
        }

        var loadAPH = function (initValue) {
            console.log("try to load region ");
            console.log("nilai regions id ", initValue);
            if (typeof easyloader !== 'undefined') {
                easyloader.load('combotree', function () {
                    $('#business_unit_id').combotree({
                        textField: 'text',
                        value: initValue,
                        loader: function (param, success, error) {
                            $.ajax({
                                type: "POST",
                                url: $.helper.resolveApi("~/core/BusinessUnit/combotree"),
                                contentType: "application/json; charset=utf-8",
                                data: JSON.stringify({}),
                                dataType: 'json',
                                success: function (response) {
                                    success(response.data.children);
                                },
                                error: function () {
                                    console.log('err');
                                    error.apply(this, arguments);
                                }
                            });
                        },
                        onSelect: function (node) {
                            console.log("Node:: ");
                            console.log(node);
                            loadFields([''], node.id);
                        }
                    });
                });
            } else {
                console.error('easyloader is not defined');
            }
        }

        loadAPH(['']);

        var loadFields = function (initValue, businessUnitId) {
            if (typeof easyloader !== 'undefined') {
                easyloader.load('combotree', function () {
                    $('#field_id').combotree({
                        textField: 'text',
                        value: initValue,
                        loader: function (param, success, error) {
                            $.ajax({
                                type: "POST",
                                url: $.helper.resolveApi("~/core/Asset/GetFieldNode?countryId=&businessUnitId=" + businessUnitId),
                                contentType: "application/json; charset=utf-8",
                                data: JSON.stringify({}),
                                dataType: 'json',
                                success: function (response) {
                                    success(response.data.children);
                                },
                                error: function () {
                                    console.log('err');
                                    error.apply(this, arguments);
                                }
                            });
                        },
                        onSelect: function (node) {
                            console.log("Node:: ");
                            console.log(node);
                        }
                    });
                });
            } else {
                console.error('easyloader is not defined');
            }
        }
        loadAPH(['']);


        var getNewRkapReal = function (id) {
            var wellId = id;
            console.log('AAa', wellId)
            $.get($.helper.resolveApi('~/core/well/' + wellId + '/detail'), function (r) {
                console.log('apa aja ya:', r.data);
                $('#hp_no').val(r.data.rig_rating);
                console.log('rig rating:', r.data.rig_rating);
                $('#water_depth').val(r.data.water_depth);
                console.log('nah yg ini WD nya:', r.data.water_depth);
            }).fail(function (r) {

            });
        }
        var getByWellId = function (id, name, businessId, rigId) {
            $('#loadingModal').modal({ backdrop: 'static', keyboard: false });
            var wellId = id;
            console.log("aaaaaaiisss", name);
            //$.get($.helper.resolveApi('~/core/Well/' + id + '/detail'), function (r) {
            $.get($.helper.resolveApi('~/core/drilling/' + wellId + '/detailbyIdRkap'), function (r) {
                if (r.status.success) {
                    if (r.data.planned_days != null) {
                        var plannedDaysFormatted = parseFloat(r.data.planned_days).toFixed(2);
                        $("#planned_days").val(plannedDaysFormatted);
                    }
                    console.log("coba ya", r.data);
                    console.log("WD nya", r.data.water_depth);
                    $('#well_name').val(name);
                    $('#well_classification_real').val(r.data.well_classification);
                    $('#business_unit_id_real').val(businessId);
                    $('#bussines_name').val(r.data.unit_name);
                    $('#field_id_real').val(r.data.field_id);
                    $('#field_name').text(r.data.field_name);
                    //var plannedDaysString = r.data.planned_days.toString();
                    //var twoDigitString = plannedDaysString.substring(0, 2);
                    $('#planned_days').val(r.data.planned_days);
                    $('#rig_name').text(r.data.rig_name);
                    $('#rig_id_real').val(rigId);
                    $('#hp_no').val(r.data.rig_rating);
                    $('#water_depth').val(r.data.water_depth);
                    //var dateValue = r.data.spud_date;
                    //if (dateValue) {
                    //    let formattedDate = moment(dateValue).format('DD/MM/YYYY');
                    //    $('#spud_date').val(formattedDate);
                    //} else {
                    //    console.error("Invalid date value: spud_date is missing or invalid");
                    //}
                    $('#spud_date').val(moment(r.data.spud_date).format('MM/DD/YYYY'));
                    $('#budget').val(r.data.afe_cost.toLocaleString());

                    $('#loadingModal').modal('hide');
                } else {
                    toastr.error(r.status.message);
                }

                $('.loading-detail').hide();
            }).fail(function (r) {
                console.log(r);
            });
        }

        var loadDateTime = function () {
            $('.date-picker').datepicker({
                autoclose: true,
                todayBtn: "linked",
                format: 'mm/dd/yyyy',
                orientation: "bottom left",
                todayHighlight: true,
                templates: controls
            });

            $(".numeric").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });

            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

        /*
        $('#btn-real').on('click', function () {
            // Panggil modal('show') pada modal "rkapRealisasi"
            $rkapRealisasi.modal('show');
        });
        */

        $('#btn-addNew').on('click', function () {
            $("#rate-value-form").show();
            $("#rate-value-form input").removeAttr("disabled");
            $.helper.form.clear($form);
            $.helper.form.fill($form, {
                well_classification: null,
                well_name: null,
                region: null,
                business_unit_id: null,
                field_id: null,
                field_name: null,
                spud_date: null,
                planned_days: 0,
                rig_id: null,
                rig_name: null,
                hp_no: null,
                water_depth: null,
                budget: null
            });
            $rkapModal.modal('show');
        });

        $('#btn-save').on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();

                console.log("kiyanassas", data);

                btn.button('loading');
                $.post($.helper.resolveApi('~/core/rkap/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        $("#planned_days").val(r.data.planned_days);
                        $rkapModal.modal('hide');
                        btn.button('reset');
                        loadDataTable();
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusTex);
                });


            }
            else return;
        });

        $('#btn-saveReal').on('click', function () {
            //var recordId = $(this).closest('.btn-group').data('id');
            var btn = $(this);
            console.log("gomenasai")
            var validator = $formReal.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $formReal.serializeToJSON();

                console.log(data);
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/rkap/saveReal'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        loadDataTable();
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusTex);
                });

            }
            else return;
        });

        return {
            init: function () {
                loadDataTable();
                loadCombo();
                loadDateTime();
                disableFormInputs();

                //cekData();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();

        // Menyimpan data form saat modal ditutup
        $('#rkapModal').on('hide.bs.modal', function () {
            var formData = $form.serializeArray();  // Ambil data dari form
            sessionStorage.setItem('rkapFormData', JSON.stringify(formData)); // Simpan data ke sessionStorage
        });

        // Mengisi data form saat modal dibuka kembali
        $('#rkapModal').on('show.bs.modal', function () {
            var savedData = sessionStorage.getItem('rkapFormData');
            if (savedData) {
                var formData = JSON.parse(savedData);
                $.each(formData, function (index, field) {
                    $('[name=' + field.name + ']', $form).val(field.value);
                });
            }
        });

        // Menghapus data saat halaman di-refresh
        $(window).on('beforeunload', function () {
            sessionStorage.removeItem('rkapFormData');
        });

    });

}(jQuery));