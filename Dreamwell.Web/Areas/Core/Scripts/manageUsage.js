﻿$(document).ready(function () {
    'use strict';
    var $materialId = $('input[id=materialId]');
    var $afeId = $('input[id=afeId]');
    var $drillingId = $('input[id=drillingId]');

    var pageFunction = function () {
        console.log($materialId.val());
        console.log($afeId.val());
        console.log($drillingId.val());
        var loadUsage = function () {
            var templateScript = $("#usageMaterial-template").html();
            var template = Handlebars.compile(templateScript);
                    $("#listDailyCost > tbody").html(template);
                    initEditableDailyCost();
                
        }

        var initEditableDailyCost = function () {
            $(".editable-daily-cost").editable({
                url: $.helper.resolveApi("~/core/AfeManageBudget/save"),
                ajaxOptions: { contentType: 'application/json', dataType: 'json' },
                params: function (params) {
                    var amount = params.value;
                    var req = {
                        id: $(this).data('id'),
                        afe_id: $(this).data('afeid'),
                        afe_line_id: $(this).data('afelineid'),
                        contract_detail_id: $(this).data('contractdetailid'),
                        original_budget: amount,
                    };
                    return JSON.stringify(req);
                },
                type: "number",
                step: 'any',
                pk: 1,
                name: "amount",
                title: "Enter Program Budget",
                display: function (value) {
                    $(this).text(thousandSeparatorDecimal(value));
                },
                validate: function (value) {
                    var myStr = value.replace(/\./g, '');
                    if ($.isNumeric(myStr) == '') {
                        return 'Only numbers are allowed';
                    }
                },
                success: function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                        return r.status.message;
                    }
                }
            }).on('shown', function (e, editable) {
                autoNumericEditable(editable);
            });
        }

        return {
            init: function () {
                loadUsage();
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });


});