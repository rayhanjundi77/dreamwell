﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formHoleAndCasing")

    var pageFunction = function () {
        $(".numeric-money").inputmask({
            digits: 3,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });
        $(".numeric").inputmask({
            digits: 0,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });

        $("#open_hole").change(function () {
            if (this.checked) {
                $("#form-group-casing-diameter").hide();
                $("#casing_name").removeAttr("required");
            } else {
                $("#form-group-casing-diameter").show();
                $("#casing_name").attr("required", "");
            }
        });

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/holeandcasing/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        console.log(r.data);
                        $.helper.form.fill($form, r.data);
                        if (r.data.hole_type) {
                            $("#open_hole").prop("checked", true)
                            $("#form-group-casing-diameter").hide();
                            $("#casing_name").removeAttr("required");
                        } else {
                            $("#form-group-casing-diameter").show();
                            $("#casing_name").attr("required", "");
                        };
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {
                });
            }
        };

        $('#btn-save').click(function (event) {
            var $dtList = $('#dt_Basic');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                $.post($.helper.resolveApi('~/core/holeandcasing/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                        $dtList.DataTable().ajax.reload();
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });
        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});