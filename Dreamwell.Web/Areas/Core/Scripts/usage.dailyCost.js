﻿(function ($) {
    'use strict';
    var $drillingId = $('input[id=drillingId]');
    var $afeId = $('input[id=afeId]');
    var $afeLineId = $('input[id=afeLineId]');
    var $costDate = $('input[id=costDate]');

    var pageFunction = function () {
        $.fn.editable.defaults.url = "/post";
        $.fn.editable.defaults.mode = 'inline';
       
        var loadDailyCost = function () {
            var templateScript = $("#manageDailyCost-template").html();
            var template = Handlebars.compile(templateScript);

            $.get($.helper.resolveApi('~/core/DailyCost/getDetailCost/' + $drillingId.val() + '/' + $afeId.val() + '/' + $afeLineId.val() + '/' + $costDate.val()), function (r) {
                console.log(r);
                if (r.status.success && r.data.length > 0) {
                    $("#listDailyCost > tbody").html(template({ data: r.data }));
                    initEditableDailyCost();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        var initEditableDailyCost = function () {
            $(".editable-daily-cost").editable({
                url: $.helper.resolveApi("~/core/DailyCost/save?afeId=" + $afeId.val()),
                ajaxOptions: { contentType: 'application/json', dataType: 'json' },
                params: function (params) {
                    var amount = params.value;
                    var req = {
                        id: $(this).data('id'),
                        drilling_id: $drillingId.val(),
                        contract_detail_id: $(this).data('contractdetailid'),
                        cost_date: $costDate.val(),
                        cost: amount,
                    };
                    console.log(req);
                    return JSON.stringify(req);
                },
                type: "number",
                step: 'any',
                pk: 1,
                name: "amount",
                title: "Enter Program Budget",
                display: function (value) {
                    $(this).text(thousandSeparatorDecimal(value));
                },
                validate: function (value) {
                    var myStr = value.replace(/\./g, '');
                    if ($.isNumeric(myStr) == '') {
                        return 'Only numbers are allowed';
                    }
                },
                success: function (r) {
                    console.log(r);
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        loadDailyCost();
                        //loadAfeManageBudget($drillingId.val(), $afeId.val(), $costDate.val());
                    } else {
                        toastr.error(r.status.message);
                        return r.status.message;
                    }
                }
            }).on('shown', function (e, editable) {
                autoNumericEditable(editable);
            });
        }

        Handlebars.registerHelper("counter", function (index) {
            return index + 1;
        });

        Handlebars.registerHelper("remainingBudget", function (actual_budget, total_cost) {
            var remainingBudget = actual_budget - total_cost;
            return "USD " + thousandSeparatorDecimal(remainingBudget);
        });

        //-- This function exactly same with from usage.detail.js
        var loadAfeManageBudget = function (drillingId, afeId, costDate) {
            var templateScript = $("#afeManageBudget-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DailyCost/getManageDailyCost/' + drillingId + '/' + afeId + '/' + costDate), function (r) {
                console.log(r);
                if (r.status.success && r.data.length > 0) {
                    $("#listAfeManageBudget > tbody").html(template({ data: r.data }));
                    initEditableDailyCost();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        return {
            init: function () {
                loadDailyCost();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 