﻿(function ($) {
    'use strict';

    var $dt_well = $('#dt_well');
    var $pageLoading = $('#page-loading-content');
    var $currencyUpdateModal = $('#currencyUpdateModal');
    var $form = $('#form-Well');

    var btnSubmitGWI = $('#btn_gwi_submit');
    
    var $btnUpdateWell = $('#btn-updateWell');
    var $btnSaveUpdateWell = $('#btn-saveUpdateWell');
    var $recordId = $('input[id=recordId]');

    

    btnSubmitGWI.on('click', function (e) { });

    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fal fa-check-circle fa-lg text-success',
        invalid: 'fal fa-times-circle fa-lg',
        validating: 'fal fa-refresh'
    };
    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            field_name: {
                validators: {
                    notEmpty: {
                        message: 'The Well Name is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION

    var pageFunction = function () {

        var loadDataTable = function () {

            var dt = $dt_well.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/wellwi/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    },

                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "well_name" },
                    { data: "field_name" },
                    { data: "well_type" },
                    //{
                    //    data: "id",
                    //    orderable: false,
                    //    searchable: false,
                    //    class: "text-left",
                    //    render: function (data, type, row) {
                    //        if (type === 'display') {
                    //            if (row.well_status == 0)
                    //                return `<span>Original Well</span>`;
                    //            else if (row.well_status == 1)
                    //                return `<span>Side Track</span>`;
                    //            else if (row.well_status == 2)
                    //                return `<span>Work Over</span>`;
                    //            else if (row.well_status == 2)
                    //                return `<span>Well Services</span>`;
                    //        }
                    //        return data;
                    //    },
                    //},
                    { data: "contractor_name" },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.submitted_by == null)
                                    return `<span class="badge badge-info p-1 pr-3 pl-3" style="width: 75px;">Draft</span>`;
                                else {
                                    if (row.closed_by == null)
                                        return `<span class="badge badge-warning p-1 pr-3 pl-3" style="width: 75px;">Submitted</span>`;
                                }
                            }
                            return data;
                        },
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.id + `" >
                                    <a class ="row-edit btn btn-xs btn-info btn-hover-info fa fa-pencil add-tooltip" href="/core/wellwi/Detail?id=`+ row.id + `"
                                        data-original-title="Edit" data-container="body">
                                    </a>
                                    <a class="row-deleted btn btn-xs btn-warning btn-hover-warning fa fa-trash add-tooltip" href="#"
                                        data-original-title="Delete" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        },
                        width: "10%"
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/wellwi/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                                //$dt_field.DataTable().ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dt_well.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);

                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    }),
                        $(this).on('click', '.row-edit', function () {

                            var recordId = $(this).closest('.btn-group').data('id');
                            $WellFormModal.modal('show'), $form.loading('start');
                            $.helper.form.clear($form);
                            $.get($.helper.resolveApi('~/core/wellwi/' + recordId + '/detail'), function (r) {

                                if (r.status.success) {
                                    $.helper.form.fill($form, r.data);
                                }
                                $form.loading('stop');
                            }).fail(function (r) {
                                toastr.error(r.statusText);
                                $form.loading('stop');
                            });

                        });
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
                console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $('#btn-addNew').on('click', function () {
        $("#rate-value-form").show();
        $("#rate-value-form input").removeAttr("disabled");
        $("#btn-save").show();
        $("#btn-saveChanges").hide();
        $('.magic-checkbox').attr('checked', false);
        //$.helper.form.clear($form),
        //    $.helper.form.fill($form, {
        //        currency_code: null,
        //        currency_description: null
        //    });
        $wellFormModal.modal('show');
    });

    $btnUpdateWell.on('click', function () {
        $btnUpdateWell.button('loading');

        var templateScript = $('#currency-template').html();
        var template = Handlebars.compile(templateScript);

        $.ajax({
            type: "GET",
            datatype: 'json',
            url: $.helper.resolveApi("~core/wellwi/getAllWell"),
            success: function (r) {
                console.log(r);
                if (r.status.success) {
                    var hasBaseRate = false;
                    $.each(r.data, function (key, value) {
                        if (value.base_rate) {
                            hasBaseRate = true;
                            $("#base_rate_name").text(value.currency_description + " (" + value.currency_symbol + ")");
                        }
                    });

                    if (hasBaseRate) {
                        $('#listWell').html(template(r));
                    } else {
                        $("#base_rate_name").text("Please Choose 1 Base Rate");
                        $("#btn-saveUpdateWell").hide();
                    }



                    $btnUpdateWell.button('reset');
                } else {

                }
            }
        });

        $currencyUpdateModal.modal('show');
    });

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));