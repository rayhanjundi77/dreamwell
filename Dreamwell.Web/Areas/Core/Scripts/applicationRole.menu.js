﻿(function ($) {
    'use strict';

    var $tree = $('#applicationMenu-tree');
    var $form = $('#form-applicationMenuCategory');
    var $recordId = $('input[id=recordId]');
    var $applicationMenuFormModal = $('#businessUnitModal');

    var roleId = $('input[id=role_id]');
    var categoryID = $('.selected-menu-category').data('selected');
    var $appModule = $('#application_module');
    var $btnAdd = $('#btnAdd');
    

    var pageFunction = function () {


        $appModule.select2({
            dropdownAutoWidth: true,
            minimumResultsForSearch: -1,
            placeholder: "Choose module",
            ajax: {
                url: $.helper.resolveApi("~/core/ApplicationModule/GetData"),
                type: "GET",
                delay: 250,
                data: function (params) {
                    return {
                        moduleName: "" // search term
                    };
                },
                processResults: function (data) {
                    var result = { results: [], more: false };
                    if (data && data.data) {

                        $.each(data.data, function () {
                            result.results.push({ id: this.id, text: this.module_name });
                        });
                    };
                    return result;

                    //return {
                    //    results: response.data
                    //};
                },
                cache: true
            }
        }).on('change', function () {
            $btnAdd.removeAttr('disabled');
            $btnAdd
                .removeAttr('data-href')    
                .attr('data-href', $.helper.resolve('~core/applicationmenu/managedetail?id=&categoryId=' + categoryID + '&moduleId=' + $(this).val()));
            loadTree(categoryID);
        });


        var loadTree = function (category) {
            $tree.jstree("destroy").empty();
            $tree.jstree({
              
                core: {
                    themes: {
                        "responsive": false
                    },
                    check_callback: true,
                    data: function (obj, callback) {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: $.helper.resolveApi("~/core/ApplicationMenu/getTreeByApplicationRoleId/" + roleId.val() + "?categoryId=" + category + "&moduleId=" + $appModule.val()),
                            success: function (data) {
                                console.log(data);
                                callback.call(this, data.applicationMenu);
                            }
                        });
                    }
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "root": {
                        "class": "css-custom"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                },
                state: { "key": "id" },
                plugins: ["dnd", "types", "checkbox"],
                checkbox: {
                    "keep_selected_style": false
                },
                checkbox: {
                    three_state: false, // to avoid that fact that checking a node also check others
                    whole_node: false,  // to avoid checking the box just clicking the node 
                    tie_selection: false // for checking without selecting and selecting without checking
                },


            }).on("check_node.jstree uncheck_node.jstree", function (e, result) {
                var loading = `<span class="jstree-loading"><i class="jstree-icon jstree-ocl"></i></span>`;
                var data = {
                    application_role_id: roleId.val(),
                    application_menu_id: result.node.id,
                };
                $("#" + result.node.id + ">a").append(loading);

                if (result.node.state.checked) {
                    console.log(data);
                    $.post($.helper.resolveApi('~/core/RoleAccessMenu/save'), data, function (r) {
                        $("#" + result.node.id + ">a>span").remove();
                    });
                } else {
                    console.log(data);
                    $.post($.helper.resolveApi('~/core/RoleAccessMenu/delete'), data, function (r) {
                        $("#" + result.node.id + ">a>span").remove();
                    });
                }
            });
        }

        return {
            init: function () {

            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));

