﻿var templateCurrencyItemScript = $("#currency-item-template").html();
var templateCurrencyItem = Handlebars.compile(templateCurrencyItemScript);

(function ($) {
    'use strict';

    var $dt_listCurrency = $('#table-currency-items');
    var $pageLoading = $('#page-loading-content');
    var $currencyFormModal = $('#CurrencyModal');
    var $editRatesModal = $('#editCurrencyModal');
    var $form = $('#form-currencyhistory');
    var $formedit = $('#form-editCurrency');
    var $btnhistory = $('#btn-history');
    var $btnDelete = $('#btn-delete');
    var $recordId = $('input[id=recordId]');
    var $btnNewCurrencyItem = $("#new-currency");
    
    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };

    $editRatesModal.niftyOverlay({
        iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
    });

    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            start_date: {
                validators: {
                    notEmpty: {
                        message: 'The start date is required.'
                    }
                }
            },
            end_date: {
                validators: {
                    notEmpty: {
                        message: 'The end date is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    $formedit.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            rate_value: {
                validators: {
                    notEmpty: {
                        message: 'The rate value is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION


    var pageFunction = function () {

        $('#demo-dp-range .input-daterange').datepicker({                   
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate show', function (e) {
            // Revalidate the date when user change it
            $form.bootstrapValidator('revalidateField', 'start_date');
            $form.bootstrapValidator('revalidateField', 'end_date');
        });;

        var loaddetail = function () {
            if ($recordId.val() !== '') {
                $pageLoading.niftyOverlay('show');
                $.get($.helper.resolveApi('~core/CurrencyHistory/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#currency_code').text(':: ' + r.data.currency_code);
                    }
                    $pageLoading.niftyOverlay('hide');
                }).fail(function (r) {
                    $.helper.noty.error(r.status, r.statusText);
                    $pageLoading.niftyOverlay('hide');
                });
            }
        }

        var loadDataTable = function () {
            var dt = $dt_listCurrency.cmDataTable({

                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/CurrencyHistory/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "currency_code" },
                    { data: "rate_value" },                                       
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class="btn-group" data-id="`+ row.id + `">
                                    <a class ="row-edit btn btn-sm btn-default btn-hover-success demo-psi-pen-5 add-tooltip" href="#"
                                        data-original-title="Edit" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    
                    $(this).on('click', '.row-edit', function () {

                        var recordId = $(this).closest('.btn-group').data('id');
                        console.log('record id currency history '+recordId)
                        //$currencyFormModal.niftyOverlay('show'),
                        $editRatesModal.modal('show'),
                            $.helper.form.clear($form),
                            $.get($.helper.resolveApi('~core/CurrencyHistory/' + recordId + '/detail'), function (r) {
                                if (r.status.success) {
                                    console.log('data get'+r.data);
                                    $.helper.formedit.fill($form, r.data);
                                    //loaddetail();
                                }
                                $editRatesModal.niftyOverlay('hide');
                            }).fail(function (r) {
                                $.helper.noty.error(r.status, r.statusText);
                                $editRatesModal.niftyOverlay('hide');
                            });

                    });
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 

            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.niftyOverlay('hide');
                if (processing) {
                    $pageLoading.niftyOverlay('show');
                } else {
                    $pageLoading.niftyOverlay('hide');
                }
            })
        }

        $('#btn-addNew').on('click', function () {
            //$.helper.form.clear($form),
            //    $.helper.form.fill($form, {
            //        currency_code: null,
            //        currency_description: null
            //    });
            //$currencyFormModal.modal('show');
           // window.location = '/Detail'
        });


        //-- add new item
        $('#new-currency').on('click', function () {
            var btn = this;
            var data = $form.serializeToJSON();
            console.log('test');
            
            console.log(data);
            console.log(data.start_date);
            $.post($.helper.resolveApi('~core/CurrencyHistory/generateitem'), data, function (r) {
            //$.post($.helper.resolveApi('~core/Currency/lookup'), data, function (r) {
                setTimeout(function () {
                    if (r.status.success) {
                        console.log('data' + r.data);
                        // $("#table-currency-items>tbody").prepend(templateCurrencyItem(r.data));
                        loadDataTable();
                        //$("#table-currency-items>tbody").prepend(templateCurrencyItem(r));

                        //  $("#otc-item").remove();
                     //   initEditableOtcItemDetail();
                        $.helper.noty.success("Successfully", r.status.message);
                        //   initItemLookup();

                     } else {
                        $.helper.noty.error("Information", r.status.message);
                     }

                }, 2000);
            }, 'json').fail(function (r) {
                btn.button('reset');
                $.niftyNoty({
                    type: 'warning',
                    container: 'floating',
                    html: '<h4 class="alert-title">' + r.status + '</h4><p class="alert-message">' + r.statusText + '</p>',
                    closeBtn: true,
                    floating: {
                        position: 'top-right',
                        animationIn: 'bounceInRight',
                        animationOut: 'bounceOutRight'
                    },
                    focus: true,
                    timer: 0
                });
                $.helper.noty.error(r.status, r.statusText);
            });
        });
       

        $('#btn-updateRate').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var validator = $formedit.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $formedit.serializeToJSON();
                console.log('data to save value: ' + data.rate_value);
                console.log('data to save id: ' + data.id);
                console.log('data to save currency id: ' + data.currency_id);
                
                btn.button('loading');
                $.post($.helper.resolveApi('~core/CurrencyHistory/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            $('input[name=id]').val(r.data.recordId);
                            $.helper.noty.success("Successfully", r.status.message);
                            loadDataTable();
                        } else {
                            btn.button('reset');
                            $.helper.noty.error("Information", r.status.message);
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    $.niftyNoty({
                        type: 'warning',
                        container: 'floating',
                        html: '<h4 class="alert-title">' + r.status + '</h4><p class="alert-message">' + r.statusText + '</p>',
                        closeBtn: true,
                        floating: {
                            position: 'top-right',
                            animationIn: 'bounceInRight',
                            animationOut: 'bounceOutRight'
                        },
                        focus: true,
                        timer: 0
                    });
                    $.helper.noty.error(r.status, r.statusText);
                });

            }
            else return;
        });

        $('#btn-save').on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~core/Currency/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            $('input[name=id]').val(r.data.recordId);
                            $.helper.noty.success("Successfully", r.status.message);
                            loadDataTable();
                        } else {
                            btn.button('reset');
                            $.helper.noty.error("Information", r.status.message);
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    $.niftyNoty({
                        type: 'warning',
                        container: 'floating',
                        html: '<h4 class="alert-title">' + r.status + '</h4><p class="alert-message">' + r.statusText + '</p>',
                        closeBtn: true,
                        floating: {
                            position: 'top-right',
                            animationIn: 'bounceInRight',
                            animationOut: 'bounceOutRight'
                        },
                        focus: true,
                        timer: 0
                    });
                    $.helper.noty.error(r.status, r.statusText);
                });

            }
            else return;
        });


        return {
            init: function () {
               // loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));