﻿(function ($) {
    'use strict';

    var $dt_listMudType = $('#dt_listMudType');
    var $pageLoading = $('.page-loading-content');
    var $btnDelete = $('#btn-delete');

    var pageFunction = function () {
        var loadDataTable = function () {
            $pageLoading.loading('start');
            var dt = $dt_listMudType.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/mudType/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "mud_name" },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output =
                                    `<div class ="btn-group" data-id= "` + row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/Core/MudType/Detail?id=") + row.id + `" class="modalTrigger btn btn-info btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#mudTypeModal">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-warning btn-xs btn-info waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/MudType/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                                $dt_listMudType.DataTable().ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    }),
                    $(this).on('click', '.row-edit', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        $MudFormModal.modal('show'), $form.loading('start');
                        $.helper.form.clear($form);
                        $.get($.helper.resolveApi('~/core/MudType/' + recordId + '/detail'), function (r) {
                            if (r.status.success) {
                                $.helper.form.fill($form, r.data);
                            }
                            $form.loading('stop');
                        }).fail(function (r) {
                            toastr.error(r.statusText);
                            $form.loading('stop');
                        });

                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });
            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('stop');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            })
        }
        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));