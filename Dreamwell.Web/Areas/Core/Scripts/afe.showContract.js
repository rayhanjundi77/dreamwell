﻿$(document).ready(function () {
    'use strict';
    //var $recordId = $('input[id=recordId]');
    var $afeId = $('input[id=recordId]');
    var $form = $("#formContractDetail")

    var $btnSearchContract = $("#btnSearchContract");
    
    var pageFunction = function () {
        var loadDetail = function () {
            Handlebars.registerHelper('checkbox', function (is_checked, afe_id) {
                if (is_checked == 1) {
                    if (afe_id == $afeId.val()) {
                        return "checked";
                    } 
                } 
            });

            $btnSearchContract.on('click', function () {
                var contractNumber = $('#searchByNumber').val();
                if (contractNumber) {
                    searchContract(contractNumber);
                } else {
                    searchContract("");
                }
                    
            });
            var searchContract = function (contractNumber) {
                var templateScript = $("#contract-template").html();
                var template = Handlebars.compile(templateScript);
                var noResult = `<div class="col-md-12"><div class="panel-tag">No Result found.</div></div>`;

                var url = $.helper.resolveApi('~/core/contract/getByContractNumber/' + contractNumber);
                if (contractNumber == "")
                    url = $.helper.resolveApi('~/core/contract/getAllContract');

                $.get(url, function (r) {
                    console.log(r);
                    if (r.status.success) {
                        if (r.data.length > 0) {
                            $("#list-contract").html(template({ data: r.data }));
                            $(".contract_details").on('click', function () {
                                contractDetails();
                            });
                        }
                        else
                            $("#list-contract").html(noResult);

                        $(".addContract").on('click', function () {
                            addContractToAfe($(this), $(this).data("contractid"));
                        });
                    }
                    else {
                        toastr.error(r.status.message);
                    }
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }

            var addContractToAfe = function (element, contractId) {
                var btn = element;
                var data = new Object();
                data.contract_id = contractId;
                data.afe_id = $afeId.val();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/AfeContract/AssignContractToAfe'), data, function (r) {
                    if (r.status.success) {
                        btn.button('reset');
                        Swal.fire({
                            title: "",
                            text: "Contract has been assigned",
                            type: "success",
                            confirmButtonText: "OK"
                        }).then(function (result) {
                        });
                        $("#dt_listAfeDetail").DataTable().ajax.reload();
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    console.log(r);
                    toastr.error(r.statusText);
                    });
            }

           
            var contractDetails = function () {
            }

            //if ($recordId.val() !== '') {
            //    console.log($recordId.val());
            //    $.get($.helper.resolveApi('~/core/AfeDetail/' + $recordId.val() + '/detail'), function (r) {
            //        console.log(r);
            //        if (r.status.success) {
            //            $.helper.form.fill($form, r.data);
            //        }
            //        $('.loading-detail').hide();
            //    }).fail(function (r) {
            //        //console.log(r);
            //    }).done(function () {

            //    });
            //}
        };

        //var isChecked = function (e) {
        //    $(".checkContract").on("change", function () {
        //        //console.log(this);
        //        var id = $(this).data('id');

        //        if ($("#isChecked" + id).prop("checked") == true) {
        //            $("#content" + id).addClass("border-info");
        //            selected++;
        //        } else {
        //            $("#content" + id).removeClass("border-info");
        //            selected--;
        //        }
        //        $("#total-selected").html(selected + " of " + totalItem + " selected");
        //    })
        //}

        //$('#btn-save').click(function (event) {
        //    event.preventDefault();
        //    var searchIDs = $("#formContractDetail input:checkbox:checked").map(function () {
        //        return $(this).val();
        //    }).get(); // <----
        //    console.log(searchIDs);

        //    var data = new Object();
        //    data.id = $recordId.val();
        //    data.afe_id = $afeId.val();
        //    if (searchIDs.length > 0) {
        //        data.contractDetailId = searchIDs;
        //    } else {
        //        data.contractDetailId = null;
        //    }

        //    var $dt_listAfeDetail = $('#dt_listAfeDetail');
        //    var btn = $(this);
        //    $.post($.helper.resolveApi('~/core/afedetail/save'), data, function (r) {
        //        if (r.status.success) {
        //            toastr.success(r.status.message)
        //            btn.button('reset');
        //            $dt_listAfeDetail.DataTable().ajax.reload();
        //        } else {
        //            toastr.error(r.status.message)
        //        }
        //        btn.button('reset');
        //    }, 'json').fail(function (r) {
        //        btn.button('reset');
        //        toastr.error(r.statusText);
        //    });
        //});

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});