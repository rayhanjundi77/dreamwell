﻿$(document).ready(function () {
    'use strict';

    var $recordId = $('input[id=recordId]');
    var $form = $("#frm-drilling_contractor")

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/DrillingContractor/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                    }
                }).fail(function (r) {
                    console.log(r);
                }).done(function () {
                    
                });
            }

        };

        $('#btn-save').click(function (event) {
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                $.post($.helper.resolveApi('~/core/DrillingContractor/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr["info"](r.status.message);
                    } else {
                        toastr["error"](r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                });


            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});