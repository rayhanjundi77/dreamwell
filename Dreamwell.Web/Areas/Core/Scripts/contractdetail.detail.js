﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formContract")
    var $contractModal = $("#contractModal")
    var $afeLineId = "";

    var masking = function () {
        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });

        $('.numeric').keyup(function () {
            const str = this.value;
            const result = str.replace(/[^\d.]|\.(?=.*\.)/g, '');
            this.value = result;
        });

    }

    //-- Lookup

    $('#btn-afe-lookup').click(function () {
        var btn = $(this);
        btn.button('loading');
        jQuery.ajax({
            type: 'POST',
            url: '/core/afeline/Lookup',
            success: function (data) {
                btn.button('reset');
                var $box = bootbox.dialog({
                    message: data,
                    title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                    callback: function (e) {
                        console.log(e);
                    }
                });
                $box.on("onSelected", function (o, event) {
                    $box.modal('hide');
                    $('#btn-afe-lookup').text(event.node.text);
                    lookupMaterialItemType();
                    $afeLineId = event.node.id;
                });
            }
        });
    });

    var lookupMaterialItemType = function () {
        $("#section-material-type").removeClass("d-none");
        $("#item_type").select2({
            dropdownParent: $contractModal,
            placeholder: "Select a Material Type",
        }).on('select2:select', function (e) {
            var data = e.params.data;
            var itemType = $("#item_type").val();
            LookupMaterial(itemType);
        });
        $('#item_type').val(null).trigger('change');
    }

    var LookupMaterial = function (itemType) {
        $("#material_id").cmSelect2({
            url: $.helper.resolveApi('~/core/material/LookupByAfeLineAndType/' + $afeLineId + '/' + itemType),
            result: {
                id: 'id',
                text: 'description'
            },
            filters: function (params) {
                return [{
                    field: "description",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: $form,
                placeholder: "Select a Material",
                allowClear: true,
                maximumSelectionLength: 1,
                tags: "true",
            }
        });
    }

    $("#currency_id").cmSelect2({
        url: $.helper.resolveApi("~/core/Currency/lookup"),
        result: {
            id: 'id',
            text: 'currency_code',
            rate: 'rate_value',
            symbol: 'currency_symbol',
            start_date: 'start_date',
            end_date: 'end_date',
            base_rate: 'base_rate'
        },
        filters: function (params) {
            return [{
                field: "currency_code",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            dropdownParent: $form,
            placeholder: "Select a Currency",
            allowClear: true,
            templateResult: function (data) {
                console.log(data);
                return data.text;
            },
            templateSelection: function (data, container) {
                if (data.id != "" && data.id != null) {
                    $(".input_currency_code_label").text("1 " + data.text);
                    $(".currency_code_label").text("Equivalent in " + data.text);

                }
                return data.text;
            },
        }
    });
    $("#currency_id").on('select2:select', function (e) {
        if (e.params.data.base_rate) {
            $("#current_rate_value").val(1);
            $("#exchangeRateGroup").hide();
        } else {
            $("#exchangeRateGroup").show();
        }
    });

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/contractdetail/' + $recordId.val() + '/detail'), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        LookupMaterial(r.data.afe_line_id);
                        $("#btn-afe-lookup").html(r.data.afe_line_description);
                        $.helper.form.fill($form, r.data);

                        if (r.data.base_rate)
                            $("#exchangeRateGroup").hide();
                        $("#current_rate_value").val(r.data.current_rate_value);
                        $(".currency_code_label").text(r.data.currency_code);
                        $("#unit").val(r.data.unit);

                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            } else {
                $("#exchangeRateGroup").hide();
            }
        };

        $('#btn-save').click(function (event) {
            var $dtContract = $('#dt_listContract');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                console.log(data);
                $.post($.helper.resolveApi('~/core/contractdetail/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                        $dtContract.DataTable().ajax.reload();
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
                masking();

            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});