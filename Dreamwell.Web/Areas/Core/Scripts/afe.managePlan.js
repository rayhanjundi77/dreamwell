﻿(function ($) {
    'use strict';
    var $dt_listAfeDetail = $('#dt_listAfeDetail');
    var $pageLoading = $('#page-loading-content');
    var $afeId = $('input[id=afe_id]');
    var $afePlanId = $('input[id=afe_plan_id]');
    var $afeLineId = "";


    var $formBudgetPlan = $("#formBudgetPlan");
    var $btnSaveBudgetPlan = $("#btn-saveBudgetPlan");
    var $planBudgetModal = $("#planBudgetModal");

    //<input type="text" id="recordId" name="id" data-field="id" value="@ViewBag.recordId" />
    //    <input type="text" name="afeId" data-field="afeId" value="@ViewBag.afeId" />
    //    <input type="text" name="afeLineId" data-field="afeLineId" value="@ViewBag.afeLineId" />

    var pageFunction = function () {
        $('#btn-afe-lookup').click(function () {
            var btn = $(this);
            btn.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/afeline/Lookup',
                success: function (data) {
                    btn.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                        callback: function (e) {
                            console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o, event) {
                        $box.modal('hide');
                        $('#btn-afe-lookup').text(event.node.text);
                        lookupMaterialItemType();
                        $afeLineId = event.node.id;
                        $("#section-material").addClass("d-none");
                        $('#item_type').val(null).trigger('change');
                        $('#material_id').val(null).trigger('change');
                    });
                }
            });
        });

        var lookupMaterialItemType = function () {
            $("#section-material-type").removeClass("d-none");
            $("#item_type").select2({
                dropdownParent: $formBudgetPlan,
                placeholder: "Select a Material Type",
            }).on('select2:select', function (e) {
                var data = e.params.data;
                var itemType = $("#item_type").val();
                LookupMaterial(itemType);
            });
            $('#item_type').val(null).trigger('change');
        }

        var LookupCurrency = function () {
            $("#currency_id").cmSelect2({
                url: $.helper.resolveApi("~/core/Currency/lookup"),
                result: {
                    id: 'id',
                    text: 'currency_code',
                    rate: 'rate_value',
                    symbol: 'currency_symbol',
                    start_date: 'start_date',
                    end_date: 'end_date',
                    base_rate: 'base_rate'
                },
                filters: function (params) {
                    return [{
                        field: "currency_code",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    dropdownParent: $planBudgetModal,
                    placeholder: "Select a Currency",
                    allowClear: true,
                    maximumSelectionLength: 1,
                    //templateResult: function (data) {
                    //    return data.text;
                    //},
                    templateSelection: function (data, container) {
                        if (data.id != "" && data.id != null) {
                            $(".input_currency_code_label").text("1 " + data.text);
                            $(".currency_code_label").text("in " + data.text);

                        }
                        return data.text;
                    },
                }
            });

            $("#currency_id").on('select2:select', function (e) {
                if (e.params.data.base_rate) {
                    $("#current_rate_value").val(1);
                    $("#exchangeRateGroup").hide();
                } else {
                    $("#exchangeRateGroup").show();
                }
            });
        }

        var LookupMaterial = function (itemType) {
            console.log("LookupMaterial")
            console.log($afeLineId)
            console.log(itemType);
            $("#section-material").removeClass("d-none");
            $('#material_id').val(null).trigger('change');
            $("#material_id").cmSelect2({
                url: $.helper.resolveApi('~/core/material/LookupByAfeLineAndType/' + $afeLineId + '/' + itemType),
                result: {
                    id: 'id',
                    text: 'description'
                },
                filters: function (params) {
                    return [{
                        field: "description",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    dropdownParent: $planBudgetModal,
                    placeholder: "Select a Material",
                    allowClear: true,
                    maximumSelectionLength: 1,
                    tags: "true",
                }
            });
        }

        var masking = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            $('.numeric').keyup(function () {
                const str = this.value;
                const result = str.replace(/[^\d.]|\.(?=.*\.)/g, '');
                this.value = result;
            });
        }

        var loadDetail = function () {
            LookupCurrency();

            $("#formBudgetPlan > .panel-body").hide();
            $("#formBudgetPlan > .panel-footer").show();
            if ($afePlanId.val() == "") {
                $("#formBudgetPlan > .panel-body").show();
                $("#formBudgetPlan > .panel-footer").hide();
            } else {
                $.get($.helper.resolveApi('~/core/afeplan/' + $afePlanId.val() + '/detail'), function (r) {
                    $("#exchangeRateGroup").hide();
                    if (r.status.success) {
                        $afeLineId = r.data.afe_line_id;

                        LookupMaterial(r.data.afe_line_id, 1);
                        $.helper.form.fill($formBudgetPlan, r.data);
                        $("#btn-afe-lookup").text(r.data.afe_line_description);

                        if (r.data.base_rate) {
                            $("#exchangeRateGroup").hide();
                        } else {
                            $("#exchangeRateGroup").show();
                        }

                    } else {
                    }

                    $("#formBudgetPlan > .panel-body").show();
                    $("#formBudgetPlan > .panel-footer").hide();
                }).fail(function (r) {
                    toastr.error(r.statusText);
                });
            }
        }


        $btnSaveBudgetPlan.on('click', function () {
            saveBudgetPlan();
        });
        var saveBudgetPlan = function () {
            var btn = $btnSaveBudgetPlan;
            var isValidate = $formBudgetPlan[0].checkValidity();

            if (!isValidate) {
                $formBudgetPlan.addClass('was-validated');
            }

            if (isValidate) {
                var data = $formBudgetPlan.serializeToJSON();
                data.afe_id = $afeId.val();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/afeplan/save'), data, function (r) {
                    if (r.status.success) {
                        $("#afe-unit-" + data.material_id).html(thousandSeparatorWithoutComma(r.data.unit));
                        $("#afe-unit-" + data.material_id).data("afeplanid", r.data.id);
                        $("#afe-unitprice-" + data.material_id).html(r.data.currency_code + " " + thousandSeparatorFromValueWithComma(r.data.unit_price));
                        $("#afe-totalprice-" + data.material_id).html(r.data.currency_code + " " + thousandSeparatorFromValueWithComma(r.data.total_price));
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });

            } else {
                toastr.error("Please complete all budget plan fields.");
                event.preventDefault();
                event.stopPropagation();
            }
        }


        return {
            init: function () {
                loadDetail();
                masking();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 