﻿(function ($) {
    'use strict';
    var $UOMFormModal = $('#UOMModal');

    var $dt_UOM = $('#dt_listUom');
    var $pageLoading = $('.page-loading-content');

    var $btnDelete = $('#btn-delete');




    var pageFunction = function () {

        var loadDataTable = function () {
            $pageLoading.loading('start');
            var dt = $dt_UOM.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/UOM/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        if (d.search && d.search.value) {
                            console.log('Original Search Value:', d.search.value);
                            d.search.value = d.search.value.replace(/%/g, '%25'); // Encode % to %25
                            console.log('Encoded Search Value:', d.search.value);
                        }
                        console.log('Request Data:', d); // Logging request data
                        return JSON.stringify(d);
                    },
                    dataSrc: function (json) {
                        console.log('Response Data:', json); // Logging response data
                        return json.data;
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "description",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output = `<div class="d-flex align-items-center flex-row">
                                          <div class="fw-500 flex-1 d-flex flex-column " >
                                              <div class="fs-md">
                                                  `+ row.description + `
                                                  <span class="fs-nano fw-400 ml-2 text-muted"><b>`+ row.uom_code + `</b></span>
                                              </div>
                                          </div>
                                          <div class="collapsed-reveal" data-id="`+ row.id + `">
                                              <button onShowModal='true' data-href="` + $.helper.resolve("/core/Uom/detail?id=") + row.id + `" class="modalTrigger btn btn-primary btn-xs btn-info waves-effect waves-themed"
                                                  data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-xs" role="status" aria-hidden="true"></span>' data-target="#UOMModal">
                                                  <span class="fal fa-pencil"></span>
                                              </button>
                                              <a class="row-deleted btn btn-xs btn-warning btn-hover-danger fa fa-trash add-tooltip" href="#"
                                                  data-original-title="Delete" data-container="body">
                                              </a>
                                          </div>
                                      </div>`;

                                return output;
                            }
                            return data;
                        }
                    },
                    {
                        data: "uom_code",
                        searchable: true,
                        visible: false
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.collapsed-reveal').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/UOM/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                                $dt_UOM.DataTable().ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            reloadPlugin();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);

                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    }),
                        $(this).on('click', '.row-edit', function () {
                            var recordId = $(this).closest('.btn-group').data('id');

                            $UOMFormModal.modal('show'), $form.loading('start');
                            $.helper.form.clear($form);
                            $.get($.helper.resolveApi('~/core/UOM/' + recordId + '/detail'), function (r) {
                                if (r.status.success) {
                                    $.helper.form.fill($form, r.data);
                                }
                                $form.loading('stop');
                            }).fail(function (r) {
                                toastr.error(r.statusText);
                                $form.loading('stop');
                            });

                        });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            })
        }


        $('#btn-addNew').on('click', function () {
            $.helper.form.clear($form),
                $.helper.form.fill($form, {
                    organization_id: null,
                    unit_name: null
                });
            $UOMFormModal.modal('show');
        });




        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));