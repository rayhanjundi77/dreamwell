﻿$(document).ready(function () {
    'use strict';
   


    var pageFunction = function () {
        var $recordId = $('input[id=recordId]');
        var $form = $('#form-UOM');

        //// FORM VALIDATION FEEDBACK ICONS
        //// =================================================================
        //var faIcon = {
        //    valid: 'fa fa-check-circle fa-lg text-success',
        //    invalid: 'fa fa-times-circle fa-lg',
        //    validating: 'fa fa-refresh'
        //};


        //// FORM VALIDATION
        //// =================================================================
        //$form.bootstrapValidator({
        //    message: 'This value is not valid',
        //    feedbackIcons: faIcon,
        //    fields: {
        //        unit_name: {
        //            validators: {
        //                notEmpty: {
        //                    message: 'The unit name is required.'
        //                }
        //            }
        //        }
        //    }
        //}).on('success.field.bv', function (e, data) {
        //    var $parent = data.element.parents('.form-group');
        //    // Remove the has-success class
        //    $parent.removeClass('has-success');
        //});
        //// END FORM VALIDATION

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/UOM/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                    }
                }).fail(function (r) {
                    toastr.error(r.statusText);
                });
            }
        }

        $('#btn-save').on('click', function () {
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/UOM/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });

            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');


        });


        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});