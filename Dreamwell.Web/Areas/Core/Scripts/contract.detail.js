﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formContract")
    var bussinessUnitID = $('input[id=PrimaryBusinessUnitId]');
    var bussinessUnitName = $('input[id=PrimaryBusinessUnit]');
    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    }

    //-- Lookup
    $("#vendor_contract_id").cmSelect2({
        url: $.helper.resolveApi('~/core/Vendor/lookup'),
        result: {
            id: 'id',
            text: 'name'
        },
        filters: function (params) {
            return [{
                field: "name",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            dropdownParent: $form,
            placeholder: "Select a Vendor",
            allowClear: true,
            //tags: true,
            //multiple: true,
            maximumSelectionLength: 1,
        }
    });




    var pageFunction = function () {
        $("#start_date,#end_date").datepicker({
            autoclose: true,
            todayBtn: "linked",
            format: 'mm/dd/yyyy',
            orientation: "bottom left",
            todayHighlight: true,
            templates: controls
        });
        $("#business_unit_id").val(bussinessUnitID.val());
        $("#btn-business-unit-lookup").text(bussinessUnitName.val());
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                console.log($recordId.val());
                $.get($.helper.resolveApi('~/core/contract/' + $recordId.val() + '/detail'), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                        $("#start_date").val(moment(r.data.start_date).format('MM/DD/YYYY'));
                        $("#end_date").val(moment(r.data.end_date).format('MM/DD/YYYY'));
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };

        $('#btn-save').click(function (event) {
            var data = $form.serializeToJSON();
            if (moment(data.start_date) > moment(data.end_date)) {
                toastr.error("end date must be greater than start date");
            } else {
                var $dt_base = $('#dt_listContract');
                var btn = $(this);
                var isvalidate = $form[0].checkValidity();
                if (isvalidate) {
                    event.preventDefault();
                    btn.button('loading');
                        $.post($.helper.resolveApi('~/core/contract/save'), data, function (r) {
                            if (r.status.success) {
                                $('input[name=id]').val(r.data.recordId);
                                toastr.success(r.status.message)
                                btn.button('reset');
                                $dt_base.DataTable().ajax.reload();
                                $("#contractModal").modal('hide');
                            } else {
                              //  toastr.error(r.status.message)
                            }

                            toastr.success("Data Contract saved");
                            $dt_base.DataTable().ajax.reload();
                            $("#contractModal").modal('hide');
                            console.log("status save adalah " + r.status);
                            btn.button('reset');
                        }, 'json').fail(function (r) {
                            btn.button('reset');
                            toastr.error(r.statusText);
                            console.log("kenapa gagal ");
                            console.log("status anda adalah gagal di " + r.status);
                        });
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                }
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        //FieldLookup();
        pageFunction.init();
    });


});