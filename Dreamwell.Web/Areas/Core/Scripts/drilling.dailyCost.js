﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $afeId = $('input[id=afeId]');

    var $dt_usage = $("#tblManageUsage");
    var $isClosed = false;

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/Afe/' + $afeId.val() + '/detail'), function (r) {
                    console.log("Afe Detail",r);
                    //console.log(r);
                    if (r.status.success) {
                        $isClosed = r.data.is_closed == null ? false : true;
                        loadManageUsage();
                    }
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };

        var loadManageUsage = function () {
            easyloader.load('treegrid', function () {        // load the specified module

                //console.log("nilai afe id " + $afeId.val() + " record id " + $recordId.val());
                $dt_usage.cmTreeGrid({
                    pagination: true,
                    url: $.helper.resolveApi("~/core/dailycost/" + $afeId.val() + "/" + $recordId.val() + "/GetManageUsage"),
                    base: {
                        idField: 'id',
                        treeField: 'description',
                        onLoadSuccess: function () {
                            if ($isClosed) {
                                $(".manage-usage").removeAttr("data-href");
                                $(".manage-usage").removeAttr("data-loading-text");
                                $(".manage-usage").removeAttr("data-toggle");
                                $(".manage-usage").removeAttr("data-target");
                            }
                            //$(this).datagrid('freezeRow', 0).datagrid('freezeRow', 1);
                        },
                    }
                });


                $(".easyui-fluid").css("width", "100%");
                $(".datagrid-wrap").css("width", "100%");
            });
        }

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});