﻿(function ($) {
    'use strict';

    var $tree = $('#asset-tree');
    var $assetFormModal = $('#assetModal');
    var $formAsset = $('#form-asset');

    var $fieldFormModal = $('#fieldModal');
    var $formField = $('#form-field');
    var $selectedAPHId = "";
    var pageFunction = function () {

        // FORM VALIDATION
        // =================================================================
        $formAsset.bootstrapValidator({
            message: 'This value is not valid',
            fields: {
                asset_code: {
                    validators: {
                        notEmpty: {
                            message: 'The asset code is required.'
                        }
                    }
                },
                asset_name: {
                    validators: {
                        notEmpty: {
                            message: 'The asset name is required.'
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            // Remove the has-success class
            $parent.removeClass('has-success');
        });

        $formField.bootstrapValidator({
            message: 'This value is not valid',
            fields: {
                asset_name: {
                    validators: {
                        notEmpty: {
                            message: 'The asset id is required.'
                        }
                    }
                },
                field_name: {
                    validators: {
                        notEmpty: {
                            message: 'The area name or field name is required.'
                        }
                    }
                }
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            // Remove the has-success class
            $parent.removeClass('has-success');
        });

        // END FORM VALIDATION

        $tree.jstree({
            contextmenu: {
                select_node: false,
                items: customMenu
            },
            core: {
                themes: {
                    "responsive": false
                },
                check_callback: true,
                multiple: false,
                data: {
                    type: "POST",
                    url: function (node) {
                        var url = $.helper.resolveApi("~/core/Asset/tree/assets?countryId=")
                        if (node.id != "#") {
                            if (node.original.nodeType === "ASSET") {

                                url = $.helper.resolveApi("~/core/Asset/tree/fields?assetId=" + node.id);
                            }
                        }
                        return url;
                    },
                    data: function (node) {
                        //$('.vakata-context').css('z-index', 999);
                        return node;
                    }
                }
            },
            "types": {
                "default": {
                    "icon": "fal fa-folder icon-state-warning icon-lg"
                },
                "root": {
                    "class": "css-custom"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            },
            state: { "key": "id" },
            plugins: ["state", "types", "contextmenu"]
        });

        $tree.on("dblclick.jstree", function (e) {
            var instance = $.jstree.reference(this),
                node = instance.get_node(e.target);
            console.log(node);

        });

        function customMenu(node) {
            // The default set of all items
            var items = {
                Refresh: {
                    label: "Refresh",
                    icon: "fal fa-redo-alt",
                    action: function (obj) {
                        var _this = this;
                        $tree.jstree(true).refresh_node(node.id)

                    }
                },
                AddField: {
                    label: "Create Field / Area",
                    icon: "fa fa-plus-square-o",
                    action: function (n) {
                        $.helper.form.clear($fieldFormModal);
                        $.helper.form.fill($fieldFormModal, {
                            asset_id: node.data.id,
                            asset_name: node.data.asset_name,
                        });
                        $fieldFormModal.modal('show');
                    }
                },
                Edit: {
                    label: "Edit",
                    icon: "fa fa-pencil-square-o",
                    action: function () {
                        $assetFormModal.modal('show');
                        $.helper.form.clear($formAsset);
                        $.helper.form.fill($formAsset, node.data);
                    }
                },
                Delete: {
                    label: "Delete",
                    icon: "fa fa-trash-o",
                    action: function () {
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are you sure delete this asset ?</p><p><b>Field</b> and <b>wells</b> will be removed cascade  by the system, You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    className: "btn-danger",
                                    label: "Confirm"
                                }
                            },
                            callback: function (result) {
                                var btn = $(b).find('.bootbox-accept');
                                if (result) {
                                    btn.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/Asset/delete"),
                                        data: JSON.stringify([node.id]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.success("Data has been deleted")
                                                $tree.jstree("refresh");
                                            } else {
                                                toastr.error(r.status.message)
                                            }
                                            b.modal('hide');
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText)
                                            btn.button('reset');
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });

                    }
                }


            };

            if (node.original.nodeType == "FIELD") {
                items = {};
                items = {
                    Edit: {
                        label: "Edit",
                        icon: "fa fa-pencil-square-o",
                        action: function () {
                            $fieldFormModal.modal('show');
                            $.helper.form.clear($formField);
                            $.helper.form.fill($formField, node.data);
                        }
                    },
                    Delete: {
                        label: "Delete",
                        icon: "fa fa-trash-o",
                        action: function () {
                            var b = bootbox.confirm({
                                message: "<p class='text-semibold text-main'>Are you sure delete this field ?</p><p><b>Field</b> and <b>wells</b> will be removed cascade  by the system, You won't be able to revert this!</p>",
                                buttons: {
                                    confirm: {
                                        className: "btn-danger",
                                        label: "Confirm"
                                    }
                                },
                                callback: function (result) {
                                    var btn = $(b).find('.bootbox-accept');
                                    if (result) {
                                        btn.button('loading');
                                        $.ajax({
                                            type: "POST",
                                            dataType: 'json',
                                            contentType: 'application/json',
                                            url: $.helper.resolveApi("~/core/Field/delete"),
                                            data: JSON.stringify([node.id]),
                                            success: function (r) {
                                                if (r.status.success) {
                                                    toastr.success("Data has been deleted")
                                                    $tree.jstree("refresh");
                                                } else {
                                                    toastr.error(r.status.message)
                                                }
                                                b.modal('hide');
                                            },
                                            error: function (r) {
                                                toastr.error(r.statusText)
                                                btn.button('reset');
                                                b.modal('hide');
                                            }
                                        });
                                        return false;
                                    }
                                },
                                animateIn: 'bounceIn',
                                animateOut: 'bounceOut'
                            });

                        }
                    }
                }
            }
            return items;
        }

        $('#btn-addNew').on('click', function () {
            $.helper.form.clear($formAsset),
                $.helper.form.fill($formAsset, {
                    asset_id: null,
                    asset_name: null
                });
            $assetFormModal.modal('show');
        });

        $('#btn-saveField').on('click', function () {
            var btn = $(this);

            var validator = $formField.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $formField.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/Field/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            $formField.find('input[name=id]').val(r.data.recordId);
                            toastr.success(r.status.message)

                            $tree.jstree("refresh");
                        } else {
                            btn.button('reset');
                            toastr.error(r.status.message)
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText)
                });
            }

        });

        $('#btn-save').on('click', function () {
            var btn = $(this);
            var validator = $formAsset.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $formAsset.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/Asset/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            $formAsset.find('input[name=id]').val(r.data.recordId);
                            toastr.success(r.status.message)

                            $tree.jstree("refresh");
                        } else {
                            btn.button('reset');
                            toastr.error(r.status.message)
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText)
                });

            }
            else return;
        });


        return {
            init: function () {
                //loadTree();
            }
        }
    }();

    var pageAPHConfigFunction = function () {


        var $treeBusinessUnit = $('#unit-tree');
        var $treeAssetUnit = $('#assetUnit-tree');

        $treeBusinessUnit.jstree({
            contextmenu: {
                select_node: false
            },
            core: {
                themes: {
                    "responsive": false
                },
                check_callback: true,
                multiple: false,
                data: function (obj, callback) {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: $.helper.resolveApi("~/core/BusinessUnit/tree"),
                        success: function (data) {
                            callback.call(this, data);
                        }
                    });
                }

            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "root": {
                    "class": "css-custom"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            },
            state: { "key": "id" },
            plugins: ["types"]
        });

        //$('#assetUnit-tree').off("contextmenu.jstree", ".jstree-anchor");


        $treeBusinessUnit.on("dblclick.jstree", function (e) {
            var instance = $.jstree.reference(this),
                node = instance.get_node(e.target);
            $selectedAPHId = node.id;
            $treeAssetUnit.data('businessUnit', '').data('businessUnit', node.id);
            loadAssetByBusinessUnit(node.id);
        });


        function loadAssetByBusinessUnit(businessUnitId) {

            $('#btn-saveUnitField').removeClass('hidden-md-up');
            $treeAssetUnit.jstree("destroy").empty();
            $treeAssetUnit.jstree({
                contextmenu: {
                    select_node: false,
                },
                core: {
                    themes: {
                        "responsive": false
                    },
                    check_callback: true,
                    multiple: true,
                    data: function (obj, callback) {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: $.helper.resolveApi("~/core/Asset/tree/assets/BusinessUnit?countryId=&businessUnitId=" + businessUnitId),
                            success: function (data) {
                                callback.call(this, data);
                            }
                        });
                    }
                },
                "types": {
                    "default": {
                        "icon": "fal fa-folder icon-state-warning icon-lg"
                    },
                    "root": {
                        "class": "css-custom"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                },
                state: { "key": "id" },
                plugins: ["types", "contextmenu", "checkbox"]
            });


        }

        function customMenu(node) {
            // The default set of all items
            var items = {
                Refresh: {
                    label: "Refresh",
                    icon: "fal fa-redo-alt",
                    action: function (obj) {
                        var _this = this;
                        $treeBusinessUnit.jstree(true).refresh_node(node.id)

                    }
                }
            }
            return items;
        }

        $('#btn-saveUnitField').on('click', function () {
            var btn = $(this);
            var isValid = true;
            var checked = $treeAssetUnit.jstree("get_selected", true);

            var Ids = [];
            jQuery.each(checked, function (i, val) {
                if (val.original.nodeType == "ASSET") {
                    if (val.children.length <= 0) {
                        toastr.error("You must choose Field of " + val.text  + ". Cannot choose an Asset");
                        isValid = false;
                    }
                }
                if (val.original.nodeType != "ASSET") // delete index
                {
                    Ids.push(val.id);
                }
            });

            if (!isValid)
                return;

            btn.button('loading');
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                url: $.helper.resolveApi('~/core/BusinessUnit/' + $selectedAPHId + '/field/save'),
                data: JSON.stringify(Ids),
                success: function (r) {
                    console.log(r);
                    if (r.status.success) {
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                },
                error: function (r) {
                    btn.button('reset');
                }
            });

            
        });





        return {
            init: function () {
                //loadBusinessUnit();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
        pageAPHConfigFunction.init();
    });
}(jQuery));

