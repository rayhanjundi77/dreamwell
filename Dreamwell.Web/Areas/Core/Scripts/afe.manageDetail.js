﻿(function ($) {
    'use strict';
    var $dt_listAfeDetail = $('#dt_listAfeDetail');
    var $dt_afePlan = $('#dt_afePlan');
    var $pageLoading = $('#page-loading-content');
    var $recordId = $('input[id=recordId]');
    var $form = $("#formAfeDetail");
    var $form_header_afe = $("#header-afe");
    var $planBudgetModal = $("#planBudgetModal");


    var $is_submitted = false;
    var $is_approved = false;
    var $has_record = false;
    var $isClosed = true;
    var $materialId = "";

    var pageFunction = function () {
        var loadDataTableAfe = function () {
            $dt_afePlan.cmTreeGrid({
                pagination: true,
                url: $.helper.resolveApi("~/core/AfePlan/" + $recordId.val() + "/GetBS20ManageDetail"),

                base: {
                    idField: 'id',
                    treeField: 'description',
                    sortName: 'description', // Menyebutkan kolom yang digunakan untuk pengurutan
                    sortOrder: 'asc',
                    //fitColumns: true,
                    onLoadSuccess: function () {
                        isClosed($isClosed);
                        var deleteMaterial = document.querySelectorAll(".deleteMaterial");
                        deleteMaterial.forEach(function (del) {
                            del.addEventListener('click', function () {
                                var id = this.getAttribute("id");
                                var btn = $(this);
                                Swal.fire({
                                    title: "",
                                    text: "Are you sure want to delete this material ?",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonText: "Yes"
                                }).then(function (result) {
                                    if (result.value) {
                                        btn.button('loading');
                                        $.post($.helper.resolveApi('~/core/afePlan/' + id + '/deleteMaterial'), function (r) {
                                            if (r.status.success) {
                                                $(this).remove();
                                                loadDataTableAfe();
                                                toastr.success("Material has been deleted");
                                            } else {
                                                toastr.error(r.status.message)
                                            }
                                            btn.button('reset');
                                        }, 'json').fail(function (r) {
                                            btn.button('reset');
                                            toastr.error(r.statusText);
                                        });
                                    }
                                });
                            });
                        });
                        $(this).datagrid('freezeRow', 0).datagrid('freezeRow', 1);
                    },
                }

            });
            $(".easyui-fluid").css("width", "100%");
            $(".datagrid-wrap").css("width", "100%");

        }

        var cek = function () {
            $.get($.helper.resolveApi('~/core/AfePlan/' + $recordId.val() + '/GetBS20ManageDetail'), function (r) {
                console.log("Data:", r.data);
            });
        };


        var isClosed = function (is_closed) {
            if (is_closed) {
                $(".modalTrigger").attr("disabled", "disabled");
                $(".modalTrigger").css("opacity", "1");

            } else {
                $("#addMaterial").removeClass("d-none");
                $("#btn-afe_close").removeClass("d-none");
            }
        }

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/afe/' + $recordId.val() + '/detail'), function (r) {
                //$.get($.helper.resolveApi('~/core/afe/' + $recordId.val() + '/detailNew'), function (r) {
                    if (r.status.success) {
                        console.log("ini", r.data)
                        $.helper.form.fill($form, r.data);
                        
                        $.helper.form.fill($form_header_afe, r.data);
                        if (r.data.well_status === '0') {
                            $("#well_status").text("Original Well (Drilling)");
                        } else if (r.data.well_status === '1') {
                            $("#well_status").text("Side Track (Drilling)");
                        } else if (r.data.well_status === '2') {
                            $("#well_status").text("Work Over");
                        } else {
                            $("#well_status").text("Well Services");
                        }

                        $isClosed = r.data.is_closed;

                        if (r.data.approved_by == null && r.data.approved_on == null) {
                            $("#approve_afe").removeClass("d-none");
                        } else {
                            $("#approve_afe").remove();
                        }

                        //-- Load Datatables
                        easyloader.load('treegrid', function () {        // load the specified module
                            loadDataTableAfe();
                        });
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };


        $('#approve_afe').click(function (event) {
            if (!$has_record) {
                toastr.error("This AFE doesn't have any details.");
                return;
            }

            Swal.fire({
                title: "",
                text: "Are you sure want to approve this AFE ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    var btn = $(this);
                    var isvalidate = true;
                    if (isvalidate && $recordId.val() !== '') {
                        event.preventDefault();
                        var data = JSON.stringify({
                            isAccepted: true,
                            recordId: $recordId.val(),
                            message: ''
                        });
                        btn.button('loading');
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            data: data,
                            url: $.helper.resolveApi('~/core/afe/approval'),
                            success: function (r) {
                                console.log(r);
                                if (r.status.success) {
                                    $("#approve_afe").remove();
                                    toastr.success("AFE has been approve");
                                } else {
                                    toastr.error(r.status.message)
                                }
                                btn.button('reset');
                            },
                            error: function (r) {
                                btn.button('reset');
                                toastr.error(r.statusText);
                            }
                        });

                    } else {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                }
            });

        });

        var loadContract = function () {
            $('#listContract').cmDataTable({
                pageLength: 10,
                responsive: true,
                ajax: {
                    url: $.helper.resolveApi("~/core/AfeContract/dataTable?afeId=" + $recordId.val()),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: null,
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return "<b><a href='#'><i class='fal fa-2x fa-angle-double-right'></i></a></b>";
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1
                        }
                    },
                    { data: "contract_no" },
                    { data: "start_date" },
                    { data: "end_date" }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {

                    });

                },
                fnRowCallback: function (nRow, aData) {
                    var $nRow = $(nRow);

                    $nRow.attr("data-toggle", 'toggle');

                    return nRow;
                },
            });
        }

        $(document).on('hidden.bs.modal', "#afeManageMaterial", function () {
            var node = $dt_afePlan.treegrid('getSelected');
            console.log('tree selected');
            console.log(node);
            $dt_afePlan.treegrid('reload', { id: node.afe_line_id });
            //easyloader.load('treegrid', function () {        // load the specified module
            //    loadDataTableAfe();
            //});
        });
        $(document).on('hidden.bs.modal', "#planBudgetModal", function () {
            easyloader.load('treegrid', function () {        // load the specified module
                loadDataTableAfe();
            });
        });
        $(document).on('hidden.bs.modal', "#afeDetailModal", function () {
            $('#listContract').DataTable().ajax.reload();
        });

        $("#btnCloseContractDetail").on('click', function () {
            $("#rowContractDetail").hide();
        });

        Handlebars.registerHelper("printNominal", function (budget) {
            if (!isNaN(budget) && budget > 0) {
                return "USD " + thousandSeparatorDecimal(budget);
            } else {
                return "............";
            }
        });
        Handlebars.registerHelper("printActualBudget", function (nominal) {
            if (nominal != null && nominal > 0) {
                return "USD " + thousandSeparatorDecimal(nominal);
            } else {
                return "";
            }
        });
        Handlebars.registerHelper("printActualExpense", function (budget, expense) {
            if (budget == null) {
                return "";
            } else {
                if (expense == null)
                    return "USD 0"
                else
                    return "USD " + thousandSeparatorDecimal(expense);
            }
        });
        Handlebars.registerHelper("counter", function (index) {
            return index + 1;
        });


        $('#btn-afe_close').on("click", function () {
            var btn = $(this);
            Swal.fire({
                title: "Are you sure want to close / complete this AFE on this well ?",
                text: "Please make sure to completing AFE, this action cannot be undo. ",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    btn.button('loading');
                    $.post($.helper.resolveApi('~/core/afe/' + $recordId.val() + '/close'), function (r) {
                        if (r.status.success) {
                            $(this).remove();
                            $("#addMaterial").remove();
                            toastr.success("AFE has been closed");
                        } else {
                            toastr.error(r.status.message)
                        }
                        btn.button('reset');
                    }, 'json').fail(function (r) {
                        btn.button('reset');
                        toastr.error(r.statusText);
                    });
                }
            });


        });

        return {
            init: function () {
                loadDetail();
                cek();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 