﻿$(document).ready(function () {
    'use strict';
    var $driling_approval = $("#driling_approval");

    var pageFunction = function () {
        var loadData = function () {
            $("#well_id").cmSelect2({
                url: $.helper.resolveApi('~/core/well/lookupWellField'),
                result: {
                    id: 'id',
                    text: 'well_name'
                },
                filters: function (params) {
                    return [{
                        field: "well_name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    placeholder: "Select a Well",
                    allowClear: true,
                }
            });

            $("#well_id").on('select2:select', function (e) {
                var id = e.target.value;
                getByWellId(id);
            });



            var getByWellId = function (id) {
                var dt = $driling_approval.cmDataTable({
                    pageLength: 10,
                    ajax: {
                        url: $.helper.resolveApi('~core/drilling/getDrillingApproved/' + id),
                        type: "POST",
                        contentType: "application/json",
                        data: function (d) {
                            return JSON.stringify(d);
                        }

                    },
                    columns: [
                        {
                            data: "id",
                            orderable: false,
                            searchable: false,
                            class: "text-center align-middle",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }, width: "1px"
                        },
                        {
                            data: "id",
                            orderable: true,
                            searchable: true,
                            class: "text-left align-middle",
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    return row.well_name;
                                }
                                return data;
                            }
                        },
                        {
                            data: "id",
                            orderable: true,
                            searchable: true,
                            class: "text-center align-middle",
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    return row.dfs;
                                }
                                return data;
                            }
                        },
                        {
                            data: "drilling_date",
                            orderable: false,
                            searchable: false,
                            class: "text-left align-middle",
                            render: function (data, type, row) {
                                console.log('test');
                                console.log(row);
                                var date = moment(row.drilling_date).format('MMM DD, YYYY');
                                if (type === 'display') {
                                    return `<a href="` + $.helper.resolve("/core/drilling/detail?id=" + row.id + "&well_id=" + row.well_id) + `" target="_blank" class="text-bold text-info fw-700" style="border-bottom: 0 !important">` + date + `</a>`;
                                }
                                return data;
                            }
                        },
                        {
                            data: "id",
                            orderable: false,
                            searchable: false,
                            class: "text-center p-1 align-middle",
                            render: function (data, type, row) {
                                if (type === 'display') {
                                    if (row.approval_rejected_level == null) {
                                        if (row.approval_level == 2 && row.approval_status == 200)
                                            return `<span class="badge badge-success p-1 pr-2 pl-2">Approved by Manager</span>`;
                                        else if (row.approval_level == 1 && row.approval_status == 2)
                                            return `<span class="badge badge-warning p-1 pr-2 pl-2">Waiting for Approval Engineer</span>`;
                                        else if (row.approval_level == 2 && row.approval_status == 2)
                                            return `<span class="badge badge-info p-1 pr-2 pl-2">Waiting for Approval Manager</span>`;
                                        else
                                            return `<span class="badge badge-danger p-1 pr-2 pl-2">[ Level: ` + row.approval_level + ` ] [ Status: ` + row.approval_status + ` ]</span>`;
                                    } else {
                                        if (row.approval_rejected_level == 1)
                                            return `<span class="badge badge-danger p-1 pr-2 pl-2">Rejected by Engineer</span>`;
                                        else if (row.approval_rejected_level == 2)
                                            return `<span class="badge badge-danger p-1 pr-2 pl-2">Rejected by Manager</span>`;
                                    }
                                }
                                return data;
                            },
                        },
                
                    ],
                    initComplete: function (setting, json) {
                        $(this).on('click', '.row-approve', function () {
                            var recordId = $(this).closest('.btn-group').data("id");

                            var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                            doApprove(recordId, true);

                            return;
                            //var b = bootbox.confirm({
                            //    message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            //    buttons: {
                            //        confirm: {
                            //            label: "Approve"
                            //        }
                            //    },
                            //    callback: function (result) {
                            //        if (result) {

                            //            var data = JSON.stringify({
                            //                recordId: recordId,
                            //                isAccepted: true
                            //            });

                            //            var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                            //            btnConfim.button('loading');
                            //            $.ajax({
                            //                type: "POST",
                            //                dataType: 'json',
                            //                contentType: 'application/json',
                            //                url: $.helper.resolveApi("~/core/drilling/approve"),
                            //                data: data,
                            //                success: function (r) {
                            //                    if (r.status.success) {
                            //                        toastr.success("Data has been approve");
                            //                    } else {
                            //                        toastr.error(r.status.message);
                            //                    }
                            //                    btnConfim.button('reset');
                            //                    b.modal('hide');
                            //                    $driling_approval.DataTable().ajax.reload();
                            //                },
                            //                error: function (r) {
                            //                    toastr.error(r.statusText);

                            //                    b.modal('hide');
                            //                }
                            //            });
                            //            return false;
                            //        }
                            //    },
                            //    animateIn: 'bounceIn',
                            //    animateOut: 'bounceOut'
                            //});
                        }),
                            $(this).on('click', '.row-reject', function () {
                                var recordId = $(this).closest('.btn-group').data("id");

                                doApprove(recordId, false);

                                return;
                                //var b = bootbox.confirm({
                                //    message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                                //    buttons: {
                                //        confirm: {
                                //            label: "Reject"
                                //        }
                                //    },
                                //    callback: function (result) {
                                //        if (result) {
                                //            var data = JSON.stringify({
                                //                recordId: recordId,
                                //                isAccepted: false
                                //            });

                                //            var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                //            btnConfim.button('loading');
                                //            $.ajax({
                                //                type: "POST",
                                //                dataType: 'json',
                                //                contentType: 'application/json',
                                //                url: $.helper.resolveApi("~/core/drilling/approve"),
                                //                data: data,
                                //                success: function (r) {
                                //                    if (r.status.success) {
                                //                        toastr.success("Data has been reject");
                                //                    } else {
                                //                        toastr.error(r.status.message);
                                //                    }
                                //                    btnConfim.button('reset');
                                //                    b.modal('hide');
                                //                    $driling_approval.DataTable().ajax.reload();
                                //                },
                                //                error: function (r) {
                                //                    toastr.error(r.statusText);

                                //                    b.modal('hide');
                                //                }
                                //            });
                                //            return false;
                                //        }
                                //    },
                                //    animateIn: 'bounceIn',
                                //    animateOut: 'bounceOut'
                                //});
                            });
                    }
                }, function (e, settings, json) {
                    var $table = e; // table selector 
                });

                dt.on('processing.dt', function (e, settings, processing) {
                    if (processing) {
                    } else {
                    }
                })
            }

            var doApprove = function (recordId, isAccepted) {
                var confirmMessage = "Approve";
                if (!isAccepted) {
                    confirmMessage = "Reject";
                }

                Swal.fire({
                    title: "Confirm " + confirmMessage + " ?",
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off',
                        placeholder: 'Type your reason',
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (rs) => {
                        if (!isAccepted) {
                            if (rs == '') {
                                Swal.showValidationMessage(
                                    "Please type your reason "
                                );
                            }
                        }
                    },
                    allowOutsideClick: () => Swal.isLoading()
                }).then((result) => {
                    var data = JSON.stringify({
                        recordId: recordId,
                        isAccepted: isAccepted,
                        description: result.value
                    });
                    console.log(data);
                    if (!result.dismiss) {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            url: $.helper.resolveApi("~/core/drilling/approve"),
                            data: data,
                            success: function (r) {
                                if (r.status.success) {
                                    toastr.success("Data has been " + confirmMessage);
                                } else {
                                    toastr.error(r.status.message);
                                }
                                $driling_approval.DataTable().ajax.reload();
                            },
                            error: function (r) {
                                toastr.error(r.statusText);
                            }
                        });
                    }
                })
            }
        }

        return {
            init: function () {
                loadData();
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });


});