﻿(function ($) {
    'use strict';
    var $dt_listAfeLine = $('#dt_listAfeLine');
    var $pageLoading = $('#page-loading-content');
    var $tree = $('#afeLine-tree');
    var pageFunction = function () {


        var loadTree = function () {
            $tree.jstree({
                contextmenu: {
                    select_node: false,
                    items: customMenu
                },
                core: {
                    themes: {
                        "responsive": false
                    },
                    check_callback: true,
                    data: function (obj, callback) {
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            url: $.helper.resolveApi("~/core/AfeLine/tree"),
                            success: function (data) {
                                callback.call(this, data);
                            }
                        });
                    }

                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "root": {
                        "class": "css-custom"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                },
                state: { "key": "id" },
                plugins: ["dnd", "state", "types", "contextmenu"]
            });

            $tree.bind("move_node.jstree rename_node.jstree", function (e, data) {
                if (e.type == "move_node") {
                    $.get($.helper.resolveApi("~/core/AfeLine/order"),
                        {
                            recordId: data.node.id,
                            parentTargetId: data.parent,
                            newOrder: data.old_position < data.position ? data.position + 1 : data.position
                        }, function (data) {
                            if (data.status.success) {
                                $tree.jstree(true).refresh();
                            }
                        });
                }
            });
        }

        function customMenu(node) {
            // The default set of all items
            var items = {
                Create: {
                    label: "Create",
                    icon: "fa fa-plus-square-o",
                    action: function (n) {
                        $.helper.modalDialog.openModal({
                            href: $.helper.resolve("/core/afeline/detail?parentId=" + node.id + "&parentName=" + node.text) ,
                            target: '#afeLineModal',
                            toggle:'modal-remote'
                        });
                    }
                },
                Edit: {
                    label: "Edit",
                    icon: "fa fa-pencil-square-o",
                    action: function () {
                        $.helper.modalDialog.openModal({
                            href: $.helper.resolve("/core/afeline/detail?id="+node.id),
                            target: '#afeLineModal',
                            toggle: 'modal-remote'
                        });
                    }
                },
                Delete: {
                    label: "Delete",
                    icon: "fa fa-trash-o",
                    action: function () {
                        //var b = bootbox.confirm({
                        //    message: "<p class='text-semibold text-main'>Are you sure ?</p><p>You won't be able to revert this!</p>",
                        //    buttons: {
                        //        confirm: {
                        //            className: "btn-danger",
                        //            label: "Confirm"
                        //        }
                        //    },
                        //    callback: function (result) {
                        //        if (result) {
                        //            $.ajax({
                        //                type: "POST",
                        //                dataType: 'json',
                        //                contentType: 'application/json',
                        //                url: $.helper.resolveApi("~/core/BusinessUnit/delete"),
                        //                data: JSON.stringify([node.id]),
                        //                success: function (r) {
                        //                    if (r.status.success) {
                        //                        toastr.success("Data has been deleted")
                        //                        $tree.jstree("refresh");
                        //                    } else {
                        //                        toastr.error(r.status.message)
                        //                    }
                        //                    b.modal('hide');
                        //                },
                        //                error: function (r) {
                        //                    toastr.error(r.statusText)
                        //                    b.modal('hide');
                        //                }
                        //            });
                        //            return false;
                        //        }
                        //    },
                        //    animateIn: 'bounceIn',
                        //    animateOut: 'bounceOut'
                        //});

                    }
                }
            };
            return items;
        }

        $(document).on('hidden.bs.modal', "#afeLineModal", function () {
            $tree.jstree("refresh");
        });

        var loadDataTable = function () {
            var dt = $dt_listAfeLine.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/afeLine/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "line_name" },
                    { data: "line" },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                if (row.is_active) {
                                    output = `<span class="badge badge-success">Active</span>`;
                                } else {
                                    output = `<span class="badge badge-danger">Inactive</span>`;
                                }
                                return output;
                            }
                            return data;
                        }
                    },
                    //{
                    //    data: "id",
                    //    orderable: false,
                    //    searchable: false,
                    //    class: "text-center",
                    //    render: function (data, type, row) {
                    //        if (type === 'display') {
                    //            var output;
                    //            output = `<div class="custom-control custom-checkbox custom-control-inline" style="margin-right: 0px;">`;
                    //            if (row.fl_active) {
                    //                output += `    <input type="checkbox" class="custom-control-input" checked="" disabled="">`;
                    //            } else {
                    //                output += `    <input type="checkbox" class="custom-control-input" disabled="">`;
                    //            }
                    //            output += `    <label class="custom-control-label" for="defaultInline3"></label>`;
                    //            output += `</div>`;
                    //            return output;
                    //        }
                    //        return data;
                    //    }
                    //},
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output =
                                    `<div class ="btn-group" data-id= "` + row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/core/afeline/detail?id=") + row.id + `" class="modalTrigger btn btn-info btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#afeLineModal">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-warning btn-xs btn-warning waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                order: [],
                "rowGroup": {
                    "dataSrc": ["parent_name"]
                },  
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/afeline/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dt_listAfeLine.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                //loadDataTable();
                loadTree();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 