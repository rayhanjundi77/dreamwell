﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formHoleAndCasing")

    //-- Lookup
    
    var pageFunction = function () {
        var loadDetail = function () {
            $("#hole_id").cmSelect2({
                url: $.helper.resolveApi('~/core/hole/lookupcasing'),
                result: {
                    id: 'id',
                    text: 'name'
                },
                filters: function (params) {
                    return [{
                        field: "name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select a Hole",
                    allowClear: true,
                    maximumSelectionLength: 1,
                }
            });
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/casing/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                        var $newOption = $("<option selected='selected'></option>").val(r.data.hole_id).text(r.data.hole_name);

                        $("#hole_id").append($newOption).trigger('change');
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {
                });
            }


        };

        $('#btn-save').click(function (event) {
            var $dtList = $('#dt_Basic');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                $.post($.helper.resolveApi('~/core/casing/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                        $dtList.DataTable().ajax.reload();
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});