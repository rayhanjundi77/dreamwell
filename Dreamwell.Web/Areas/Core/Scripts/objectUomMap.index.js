﻿(function ($) {
    'use strict';

    var $dt = $('#dt');
    var $pageLoading = $('#page-loading-content');
    var $btnDelete = $('#btn-delete');

    var pageFunction = function () {

        var loadDataTable = function () {
            var url = $.helper.resolveApi('~/core/Uom/dataTable/objectMapper');
            var dt = $dt.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: url,
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        if (d.search && d.search.value) {
                            console.log('Original Search Value:', d.search.value);
                            d.search.value = d.search.value.replace(/%/g, '%25'); // Encode % to %25
                            console.log('Encoded Search Value:', d.search.value);
                        }
                        console.log('Request Data:', d); // Logging request data
                        return JSON.stringify(d);
                    },
                    dataSrc: function (json) {
                        console.log('Response Data:', json); // Logging response data
                        return json.data;
                    }
                },
                columns: [
                    {
                        data: "object_name",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `<div class="d-flex align-items-center flex-row">
                                                    <div class="fw-500 flex-1 d-flex flex-column cursor-pointer" data-toggle="collapse" data-target="#msg-01 > .js-collapse">
                                                        <div class="fs-lg">
                                                            `+ data + `
                                                            <span class="badge badge-danger">`+ row.uom_code + `</span>
                                                        </div>
                                                        <div class="fs-nano">
                                                            `+ row.uom_category_name + `
                                                        </div>
                                                    </div>
                                                    <div class="collapsed-reveal">
                                                        <button data-href="`+ $.helper.resolve("~Core/ObjectUomMap/detail?id=" + row.id) + `" class="modalTrigger btn-link"
                                                                data-toggle="modal-remote" data-target="#objectUomMapModal">
                                                            <span class="fal fa-edit"></span>
                                                        </button>
                                                    </div>
                                                </div>`;
                                return output;
                            }
                            return data;
                        },
                    },
                    {
                        data: "uom_code",
                        searchable: true,
                        visible: false
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/Uom/delete/objectMapper"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                                dt.ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            reloadPlugin();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);

                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });

                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
                console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop');
                }
            });
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));
