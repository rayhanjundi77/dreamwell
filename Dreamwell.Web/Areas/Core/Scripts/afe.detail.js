﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formAfe")

    var pageFunction = function () {

        //-- Lookup
        $('.select2-default').select2({
            placeholder: "select a well",
            destroy: true,
            dropdownParent: $form
        });

        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

        var loadAPH = function (initValue) {
            easyloader.load('combotree', function () {        // load the specified module
                $('#business_unit_id').combotree({
                    textField: 'text',
                    value: initValue,
                    loader: function (param, success, error) {
                        $.ajax({
                            type: "POST",
                            url: $.helper.resolveApi("~/core/BusinessUnit/combotree"),
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({}),
                            dataType: 'json',
                            success: function (response) {
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err');
                                error.apply(this, arguments);
                            }
                        });
                    },
                    onSelect: function (node) {
                        loadField(node.id, []);
                    }
                });
            })
        }


        var loadField = function (unitId, initValue) {
            easyloader.load('combotree', function () {        // load the specified module
                $('#field_id').removeAttr('disabled');
                $('#field_id').combotree({
                    textField: 'text',
                    loader: function (param, success, error) {
                        $.ajax({
                            type: "POST",
                            url: $.helper.resolveApi("~/core/BusinessUnit/combotree/field?unitId=" + unitId),
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({}),
                            dataType: 'json',
                            success: function (response) {
                                success(response.data.children);
                            },
                            error: function () {
                                console.log('err');
                                error.apply(this, arguments);
                            }
                        });
                    },
                    onSelect: function (node) {
                        console.log('selected node');
                        console.log(node);
                        if (node.id == null) {
                            throw "Please Select Field only";
                        } else {
                            loadWellByField(node.id);
                        }
                    }

                }).combotree('setValues', initValue);
            })
        }

        var loadWellByField = function (fieldId) {
            $("#well_id").removeAttr('disabled');
            $("#well_id").cmSelect2({
                url: $.helper.resolveApi('~/core/well/lookup'),
                result: {
                    id: 'id',
                    text: 'well_name'
                },
                filters: function (params) {
                    return [{
                        logic: "AND",
                            filters: [
                                {
                                    field: "field_id",
                                    operator: "eq",
                                    value: fieldId
                                },
                                {
                                    field: "well_name",
                                    operator: "contains",
                                    value: params.term || '',
                                }
                            ]
                    }];
                    //return [
                    //    {
                    //        logic: "AND",
                    //        filters: [
                    //            {
                    //                field: "field_id",
                    //                operator: "eq",
                    //                value: fieldId
                    //            },
                    //            {
                    //                field: "well_name",
                    //                operator: "contains",
                    //                value: params.term || '',
                    //            }
                    //        ]
                    //    }
                    //];
                },
                options: {
                    destroy: true,
                    dropdownParent: $form,
                    placeholder: "Select a Well",
                    allowClear: true,
                }
            });
        }

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/Afe/' + $recordId.val() + '/detail'), function (r) {
                    console.log("Afe Detail");
                    console.log(r);
                    if (r.status.success) {

                        loadWellByField(r.data.field_id);
                        $.helper.form.fill($form, r.data);
                        loadField(r.data.business_unit_id, [{ id: r.data.field_id, text: r.data.field_name }]);
                        //$('#material_lookup').combotree('setValues', [{ id: 1, text: 'test' }]);
                        loadAPH([r.data.business_unit_id]);

                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
            else {
                loadAPH([]);
            }
        };

        $('#btn-save').click(function (event) {

            var $dtAfe = $('#dt_listAfe');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            var isNew = ($recordId.val() === '');

            //console.log($form.serializeToJSON());
            //console.log($('#business_unit_id').val());
            //return;
            if (isvalidate) {
                var swalTitle = "Are you sure want to create a new AFE?";
                if (!isNew)
                    swalTitle = "Are you sure want to update this AFE ?";

                Swal.fire({
                    title: swalTitle,
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes"
                }).then(function (result) {
                    if (result.value) {
                        //-- Do Save
                        event.preventDefault();
                        btn.button('loading');
                        var data = $form.serializeToJSON();
                        console.log(data);
                        //data.business_unit_id = 

                        $.post($.helper.resolveApi('~/core/afe/save'), data, function (r) {
                            if (r.status.success) {
                                toastr.success(r.status.message);
                                $.helper.form.clear($form);
                                btn.button('reset');
                                $dtAfe.DataTable().ajax.reload();
                                redirect(r.data.recordId);
                            } else {
                                //toastr.error(r.status.message)
                            }
                            btn.button('reset');
                        }, 'json').fail(function (r) {
                            btn.button('reset');
                            toastr.error(r.statusText);
                        });
                    }
                });
            } else {
                $form.addClass('was-validated');
                event.preventDefault();
                event.stopPropagation();
            }
        });


        var redirect = function (recordId) {
            Swal.fire({
                title: "",
                text: "New AFE has been created",
                type: "success",
                confirmButtonText: "Go to detail"
            }).then(function (result) {
                if (result.value) {
                    window.location = $.helper.resolve("/core/Afe/ManageDetail?id=") + recordId;
                }
            });
        }

        return {
            init: function () {
                //loadAPH([]);
                loadDetail();
                maskingMoney();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();


    });


});