﻿window.StoneList = [];
$(document).ready(function () {
    'use strict';

    var $recordId = $('input[id=wellBhaId]');
    var $drill_formation_actual_id = $('input[id=drill_formation_actual_id]');
    if($drill_formation_actual_id.val() == null || $drill_formation_actual_id.val() == ""){
        toastr.error('Please Save Lithology Before Setup Lithology');
        setTimeout(function() {
            $("#setLithologyModal .close").click();
        }, 2000);
        return;
    }
    var $form = $("#form-wellBHA")

    var lastrowActualFormationDetail = 0;
    var $btnSaveBHA = $("#btn-saveBHA");
    var $formWellBHA = $("#form-wellBHA");
    var $formLithology = $("#form-wellBhaComponent");

    var tb_new_bha = $("#tableActualFormationDetail > tbody");

    var pageFunction = function () {
        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            $(".numeric").inputmask({
                digits: 0,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

        var bhaComponentLookup = function (id) {
            var $element;
            if (id == null || id == "") {
                $element = $(".lookup-bha-component");
            } else {
                $element = $("#" + id);
            }
            $element.cmSelect2({
                url: $.helper.resolveApi('~/core/Stone/lookupAll'),
                result: {
                    id: 'id',
                    text: 'name'
                },
                filters: function (params) {
                    console.log("params lithology")
                    console.log(params)
                    return [{
                        field: "name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select a Lithology",
                    allowClear: true,
                    //tags: true,
                    //multiple: true,
                    maximumSelectionLength: 1,
                }
            });
        }

        $("#cloneBhaComponent").on('click', function () {
            var templateScript = $("#rowActualInformationDetailComponent-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableActualFormationDetail > tbody tr.no-data').remove();
            $("#tableActualFormationDetail > tbody").append(template);

            $('#tableActualFormationDetail > tbody tr:last.rowActualFormationDetail > .select.p-1 > select.lookup-bha-component ').attr("id", "bha_component_id-" + lastrowActualFormationDetail);

            $("#tableActualFormationDetail > tbody tr:last.rowActualFormationDetail > td > input.numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });

            bhaComponentLookup("bha_component_id-" + lastrowActualFormationDetail);
            lastrowActualFormationDetail++;

            $('.row-delete').on('click', function () {
                $(this).closest('.rowActualFormationDetail').remove();
            });

            $(".calculate-length").on('input change keyup', function () {
                recalculateBHA();
            });
        })

        var recalculateBHA = function () {
            if (tb_new_bha.children().length > 0) {
                var i = 0;
                var cummLength = 0;

                tb_new_bha.children().each(function () {
                    var currentLength = $(this).find("input[name*='length']").val().replace(/,/g, '');

                    console.log(currentLength);
                    if (i == 0) {
                        cummLength = currentLength;
                    } else {
                        cummLength = parseFloat(cummLength) + parseFloat(currentLength);
                    }


                    if (!isNaN(cummLength)) {
                        $(this).find("input[name*='cumm_length']").val(parseFloat(cummLength).toFixed(2));
                    } else {
                        $(this).find("input[name*='cumm_length']").val("");
                    }
                    i++;
                });
            }
        }

        var saveLithology = function () {
            var btn = $btnSaveBHA;
            var isValid = true;

            var data = new Object();
            data.drilling_id = $recordId.val();
            data.well_bha = $formWellBHA.serializeToJSON();
            data.well_bha_component = new Object();
            data.is_new = true;

            var isValidateWellBha = $formWellBHA[0].checkValidity();
            var isValidateWellBhaComponents = $formLithology[0].checkValidity();

            if (!isValidateWellBha) {
                $formWellBHA.addClass('was-validated');
                isValid = false;
            }

            if (!isValidateWellBhaComponents) {
                $formLithology.addClass('was-validated');
                isValid = false;
            }


            if (isValid) {
                var components = [];
                var total = document.getElementsByName('drill_formation_actual_id[]').length;
                for (var i = 0; i < total; i++) {
                    var drill_formation_actual_id = document.getElementsByName('drill_formation_actual_id[]')[i].value;
                    var well_id = document.getElementsByName('well_id[]')[i].value;
                    var drilling_id = document.getElementsByName('drilling_id[]')[i].value;
                    var percentage = document.getElementsByName('percentage[]')[i].value;
                    var stone_id = document.getElementsByName('stone_id[]')[i].value;
                    components.push({
                        drill_formation_actual_id: drill_formation_actual_id,
                        well_id: well_id,
                        drilling_id: drilling_id,
                        percentage: percentage,
                        stone_id: stone_id,
                    });
                }
                data.drill_Formations_Actual_Detail = components;
            }


            if (isValid) {
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/DrillFormationsActual/saveActualDetail'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all Setup Lithology.");
                event.preventDefault();
                event.stopPropagation();
            }

        }

        $btnSaveBHA.on('click', function () {
            Swal.fire({
                title: "",
                text: "Are you sure want to create a new Lithology?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    saveLithology();
                }
            });
        })

        var loadDetail = function () {
            console.log("load detail for the asyn setup lithology");
            if ($drill_formation_actual_id.val() !== '') {
                // var templateScript = $("#rowActualInformationDetailComponentList-template").html();
                // var template = Handlebars.compile(templateScript);                
                console.log($drill_formation_actual_id.val());
                $btnSaveBHA.show();
                $.get($.helper.resolveApi('~/core/DrillFormationsActual/getDetail/' + $drill_formation_actual_id.val()), function (r) {
                    console.log("");
                    console.log(r);
                    if (r.status.success) {
                       
                        // $.helper.form.fill($form, r.data);
                        // $("#tableActualFormationDetail > tbody").html(template({ data: r.data }));
                        for (var i = 0; i < r.data.length; i++) {
                            $("#cloneBhaComponent").click()
                        }

                        $(".rowActualFormationDetail").each(function (idx) {
                            loadStoneById(r.data[idx].stone_id,"bha_component_id-" + idx)
                            // bhaComponentLookup("bha_component_id-" + idx)

                            $( "span[aria-labelledby='select2-bha_component_id-0-container']" ).click();                            
                        })

                        $(".rowActualFormationDetail").each(function (idx) {
                            $(this).find('.stone_id').val(r.data[idx].stone_id);
                            $(this).find('.percentage').val(r.data[idx].percentage);
                            $(this).find('.stone_id').trigger('change');
                        })

                        setTimeout(function() {
                            $(".rowActualFormationDetail").each(function (idx) {
                                $(this).find('.stone_id').val(r.data[idx].stone_id);
                                $(this).find('.stone_id').trigger("change");
                            })
                        }, 1000);
                    }
                    $("#bha-component-action-col").hide();
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            } else {
                $btnSaveBHA.show();
            }
        };

        var loadStoneById = function (stone_id,element) {
            console.log("window.StoneList-----------------");
            console.log(window.StoneList);
            for (var i = 0; i < window.StoneList.length; i++) {
                var item = window.StoneList[i];
                $("#"+element).append('<option value="'+item.id+'">'+item.name+'</option>');
            }
            $("#"+element).val(stone_id);
            $("#"+element).trigger("change");
        }

        var SetStoneToVariable = function function_name() {
            var nn = {
                logic:'OR',
                filters:[{
                        field: "name",
                        operator: "contains",
                        value: '',
                    }],
            }
            var data = {
                pageSize:100,
                filter:nn
            }
            
            $.post($.helper.resolveApi('~/core/Stone/lookupAll'), data ,function (r) {
                window.StoneList = r.items;
                loadDetail();
            }).fail(function (r) {
            }).done(function () {

            });
        }

        var GetWellBhaComponents = function (wellBhaId) {
            var templateScript = $("#readBhaComponent-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/wellbhacomponent/GetByWellBha/' + wellBhaId), function (r) {
                if (r.status.success) {
                    console.log("Well Bha Components");
                    console.log(r);
                    $("#tableActualFormationDetail > tbody").html(template({ data: r.data }));
                    //    bhaComponentLookup();
                    maskingMoney();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
            }).done(function () {

            });
        }

        var GetWellObjectUomMap = function (wellid) {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + wellid), function (r) {
                if (r.status.success && r.data.length > 0) {
                    //console.log("--GetWellObjectUomMap--");
                    //console.log(r);
                    $("form#form-wellBHA :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("form#form-wellBhaComponent :input.uom-definition").each(function () {
                        console.log("haihai")
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        return {
            init: function () {
                SetStoneToVariable();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});