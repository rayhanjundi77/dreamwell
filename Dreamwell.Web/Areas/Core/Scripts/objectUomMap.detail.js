﻿$(document).ready(function () {
    'use strict';

    var $recordId = $('input[id=recordId]');
    var $form = $("#form-objectUom");

    var pageFunction = function () {

        $("#uom_category_id").cmSelect2({
            url: $.helper.resolveApi('~/core/Uom/lookup/UomCategory'),
            result: {
                id: 'id',
                text: 'category_name'
            },
            filters: function (params) {
                return [{
                    field: "category_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: $form,
                placeholder: "Select a category",
                allowClear: true,
                maximumSelectionLength: 1,
            }
        });

        $("#uom_category_id").on('change', function () {
            if (this.value == '') {

            }
            else {
                LookupUom(this.value);
            }
        });


        var LookupUom = function (categoryId) {
            $("#uom_id").cmSelect2({
                url: $.helper.resolveApi('~/core/Uom/lookup'),
                result: {
                    id: 'id',
                    text: 'description'
                },
                filters: function (params) {
                    return [
                        {
                            logic: "AND",
                            filters: [{
                                field: "uom_category_id",
                                operator: "eq",
                                value: categoryId,
                            },
                            {
                                field: "unit_alias",
                                operator: "contains",
                                value: params.term || '',
                            }]
                        }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select a unit",
                    allowClear: true,
                    maximumSelectionLength: 1,
                }
            });
        }

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/Uom/' + $recordId.val() + '/detail/ObjectMapper'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    console.log(r);
                }).done(function () {

                });
            }

        };

        $('#btn-save').click(function (event) {
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();

                $.post($.helper.resolveApi('~/core/Uom/save/ObjectMapper'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr["info"](r.status.message);
                    } else {
                        toastr["error"](r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                });


            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        
        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});