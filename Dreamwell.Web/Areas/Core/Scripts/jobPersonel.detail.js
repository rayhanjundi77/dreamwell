﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formJobPersonel")

    //-- Lookup
    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/jobpersonel/' + $recordId.val() + '/detail'), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };

        $('#btn-save').click(function (event) {
            var $dtBasic = $('#dt_listJobPersonel');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                Swal.fire({
                    title: "",
                    text: "Are you sure want to create a new Job Personel?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes"
                }).then(function (result) {
                    if (result.value) {
                        //-- Do Save
                        event.preventDefault();
                        btn.button('loading');
                        var data = $form.serializeToJSON();
                        $.post($.helper.resolveApi('~/core/jobpersonel/save'), data, function (r) {
                            if (r.status.success) {
                                $.helper.form.clear($form);
                                btn.button('reset');
                                toastr.success(r.status.message)
                                $('#jobPersonelModal').modal('hide');
                                $dtBasic.DataTable().ajax.reload();
                            } else {
                                toastr.error(r.status.message)
                            }
                            btn.button('reset');
                        }, 'json').fail(function (r) {
                            btn.button('reset');
                            toastr.error(r.statusText);
                        });
                    }
                });
            } else {
                $form.addClass('was-validated');
                event.preventDefault();
                event.stopPropagation();
            }
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});