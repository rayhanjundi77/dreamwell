﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=wellBitId]');

    var $btnSaveBit = $("#btn-saveBit");
    var $formWellBit = $("#form-wellBit");

    var pageFunction = function () {
        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            $(".numeric").inputmask({
                digits: 0,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }
        var iadcLookup = function () {
            $('.iadc-lookup').click(function () {
                var element = $(this);
                element.button('loading');
                jQuery.ajax({
                    type: 'POST',
                    url: '/core/Iadc/Lookup',
                    success: function (data) {
                        element.button('reset');
                        var $box = bootbox.dialog({
                            message: data,
                            title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                            callback: function (e) {
                                //console.log(e);
                            }
                        });
                        $box.on("onSelected", function (o, event) {
                            $box.modal('hide');
                            //element.closest("tr").children(".activitiesCategory").find("input").val(event.node.original.category);
                            //if (event.node.original.type == "1") {
                            //    element.closest("tr").children(".activitiesPTorNPT").find("input").val("PT");
                            //} else if (event.node.original.type == "2") {
                            //    element.closest("tr").children(".activitiesPTorNPT").find("input").val("NPT");
                            //}
                            element.siblings().val(event.node.id);
                            element.html(event.node.text);
                        });
                    }
                });
            });
        }
        

        var saveBit = function () {
            var btn = $btnSaveBit;
            var data = $formWellBit.serializeToJSON();

            var isValidate = $formWellBit[0].checkValidity();
            if (!isValidate) {
                $formWellBit.addClass('was-validated');
            }
            console.log('data');
            console.log(data);
            if (isValidate) {
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/wellbit/save'), data, function (r) {
                    console.log("Well Bit Record");
                    console.log(r);
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                        $('#setBhaBitModal').modal('hide');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all BIT fields.");
                event.preventDefault();
                event.stopPropagation();
            }


            console.log(data);
            return;
        }

        $btnSaveBit.on('click', function () {
            Swal.fire({
                title: "",
                text: "Are you sure want to create a newaa BIT?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    saveBit();
                }
            });
        }) 


        
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $btnSaveBit.hide();
                $.get($.helper.resolveApi('~/core/wellbit/getDetail/' + $recordId.val()), function (r) {
                    if (r.status.success) {
                        console.log('DATA');
                        console.log(r.data);
                        GetWellObjectUomMap(r.data.well_id);
                        $.helper.form.fill($formWellBit, r.data);
                        //$("#iadc_code").html(r.data.iadc_description);
                        //iadcLookup();
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            } else {
                var params = new Proxy(new URLSearchParams(window.location.search), {
                    get: (searchParams, prop) => searchParams.get(prop),
                });
                GetWellObjectUomMap(params.well_id);
                $btnSaveBit.show();
            }
        };

        var GetWellObjectUomMap = function (wellid) {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + wellid), function (r) {
                if (r.status.success && r.data.length > 0) {
                    //console.log("--GetWellObjectUomMap--");
                    //console.log(r);
                    $("form#form-wellBit :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            var uom = r.data.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                            if (uom) {
                                $("#" + elementId).val(uom.uom_code);
                            }
                        }
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        $("#dg_depth_in").on('change', function () {
            calculateBitRop();
        });
        $("#dg_depth_out").on('change', function () {
            calculateBitRop();
        });
        $("#dg_hours_on_bottom").on('change', function () {
            calculateBitRop();
        });

        var calculateBitRop = function () {
            var depth_in = $("#dg_depth_in").val();
            var depth_out = $("#dg_depth_out").val();
            var hours_on_bottom = $("#dg_hours_on_bottom").val();

            var footage = parseFloat(depth_out) - parseFloat(depth_in);
            var bit_rop_overall = parseFloat(footage) / parseFloat(hours_on_bottom);

            $("#dg_footage").val(footage);
            $("#dg_rop_overall").val(bit_rop_overall);
        }

        $("#count_1").on('change', function () {
            calculateTFA();
        });
        $("#count_2").on('change', function () {
            calculateTFA();
        });
        $("#count_3").on('change', function () {
            calculateTFA();
        });
        $("#count_4").on('change', function () {
            calculateTFA();
        });
        $("#count_5").on('change', function () {
            calculateTFA();
        });

        $("#noozle_1").on('change', function () {
            calculateTFA();
        });
        $("#noozle_2").on('change', function () {
            calculateTFA();
        });
        $("#noozle_3").on('change', function () {
            calculateTFA();
        });
        $("#noozle_4").on('change', function () {
            calculateTFA();
        });
        $("#noozle_5").on('change', function () {
            calculateTFA();
        });

        var calculateTFA = function () {
            console.log('aa');
            var count_1 = $("#count_1").val().replace(/,/g, '');
            var count_2 = $("#count_2").val().replace(/,/g, '');
            var count_3 = $("#count_3").val().replace(/,/g, '');
            var count_4 = $("#count_4").val().replace(/,/g, '');
            var count_5 = $("#count_5").val().replace(/,/g, '');

            var noozle_1 = $("#noozle_1").val().replace(/,/g, '');
            var noozle_2 = $("#noozle_2").val().replace(/,/g, '');
            var noozle_3 = $("#noozle_3").val().replace(/,/g, '');
            var noozle_4 = $("#noozle_4").val().replace(/,/g, '');
            var noozle_5 = $("#noozle_5").val().replace(/,/g, '');

            var math1 = (noozle_1 === '') ? 0 : Math.pow(noozle_1, 2);
            var math2 = (noozle_2 === '') ? 0 : Math.pow(noozle_2, 2);
            var math3 = (noozle_3 === '') ? 0 : Math.pow(noozle_3, 2);
            var math4 = (noozle_4 === '') ? 0 : Math.pow(noozle_4, 2);
            var math5 = (noozle_5 === '') ? 0 : Math.pow(noozle_5, 2);

            var result1 = (math1 * count_1);
            var result2 = (math2 * count_2);
            var result3 = (math3 * count_3);
            var result4 = (math4 * count_4);
            var result5 = (math5 * count_5);

            var result = (result1 + result2 + result3 + result4 + result5)/1303.8;
            $("#TFA").val(result);
        };

        return {
            init: function () {
                loadDetail();
                iadcLookup();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});