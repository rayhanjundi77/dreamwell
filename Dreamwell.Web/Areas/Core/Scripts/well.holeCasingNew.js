﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=well_hole_id]');
    var $wellid = $('input[id=well_id]');
    var $form = $("#form-wellHoleCasing")

    var $btnSave = $("#btn_save-hcd");
    var $btnAddCasing = $("#btn_add_casinng");
    var holeType = true;
    var ObjectMapper = [];

    var holeCasingTypeLookup = function (selectedId) {
        if (selectedId == "") {
            $(".hole-casing-tipe").val('');
        } else {
            $(".hole-casing-tipe").val(selectedId);
        }
        $(".hole-casing-tipe").select2({
            dropdownParent: $form,
            placeholder: "Select Casing Type",
            allowClear: true
        });
    }

    var pageFunction = function () {
        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            $(".numeric").inputmask({
                digits: 0,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

        var GetWellObjectUomMap = function () {

            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $wellid.val()), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        ObjectMapper = r.data;
                        $("form#form-wellHoleCasing :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                $.each(r.data, function (key, value) {
                                    if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                        $("#" + elementId).val("" + value.uom_code + "");
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        var holeAndCasingLookup = function (id) {
            $(".holeandcasinglookup").cmSelect2({
                url: $.helper.resolveApi("~/core/hole/lookup"),
                result: {
                    id: 'id',
                    text: 'name',
                    hole_type: 'hole_type'
                },
                filters: function (params) {
                    return [{
                        field: "name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select Hole and Casing",
                    allowClear: true,
                    templateResult: function (repo) {
                        return "<b>Hole:</b> " + repo.text + `"` + " <b>Type:</b> " + (repo.hole_type ? "Open Hole" : "Non Open Hole") + ``;
                    },
                }
            });
        }

        $(".holeandcasinglookup").on('select2:select', function (e) {
            if (e.params.data.hole_type) {
                $("#section_casing").hide();
                $("#section_casing input.form-control-input, #section_casing select").removeAttr("required");
                $("#section_casing input.form-control-input").val("");
                $btnAddCasing.hide();
            } else {
                $("#section_casing").show();
                $("#section_casing input.form-control-input, #section_casing select").attr("required", "");
                $btnAddCasing.show();
            }
            holeType = e.params.data.hole_type;
            $('#hole_name').val(e.params.data.text);
            $('#casing_name').val(e.params.data.casing_name);
            lookUpCasing(e.params.data.id);
        });

        var lookUpCasing = function (holeId) {
            $(".casinglookup").val(null).trigger("change");
            $(".casinglookup").cmSelect2({
                url: $.helper.resolveApi('~/core/casing/lookup'),
                result: {
                    id: 'id',
                    text: 'name'
                },
                filters: function (params) {
                    return [
                        {
                            logic: "AND",
                            filters: [{
                                field: "hole_id",
                                operator: "eq",
                                value: holeId,
                            },
                            {
                                field: "name",
                                operator: "contains",
                                value: params.term || '',
                            }]
                        }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select a casing",
                    allowClear: true,
                    maximumSelectionLength: 1,
                }
            });
        }

        $btnAddCasing.on('click', function () {
            var element = `<div class="casing-row">
                            <hr class="mt-2 mb-3">
                                <div class="form-group row mb-2">
                                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="hole_name">Casing Diameter<span class="text-danger"> *</span></label>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <select class="form-control form-control-sm casinglookup" name="casing_id" data-text="casing_name" data-field="casing_id" placeholder="" required></select>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_name">Casing Setting Depth<span class="text-danger"> *</span></label>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <div class="input-group">
                                            <input class="form-control form-control-input numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" id="casing_setting_depth" name="casing_setting_depth[]" data-text="casing_setting_depth" data-field="casing_setting_depth" placeholder="Casing Setting Depth" inputmode="numeric" required="">
                                            <div class="input-group-append border-0 uom-background">
                                                <input type="text" class="form-control-plaintext uom-definition" id="casing_setting_depth_uom" data-objectname="Casing Setting Depth" value="-" readonly="" style="border: 1px solid #ccc;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-2 casing-type">
                                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_name">Casing Type<span class="text-danger"> *</span></label>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <select class="form-control form-control-input hole-casing-tipe" id="casing_type" name="casing_type[]" data-field="casing_type" data-text required="">
                                            <option value="1">Conductor</option>
                                            <option value="2">Surface</option>
                                            <option value="3">Drilling Liner</option>
                                            <option value="4">Intermediate</option>
                                            <option value="5">Production Liner</option>
                                            <option value="6">Perforated Liner</option>
                                            <option value="7">Production</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_name">Casing Weight<span class="text-danger"> *</span></label>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <div class="input-group">
                                            <input class="form-control form-control-input numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" id="casing_weight" name="casing_weight[]" data-text="casing_weight" data-field="casing_weight" placeholder="Casing Weight" inputmode="numeric" required="">
                                            <div class="input-group-append border-0 uom-background">
                                                <input type="text" class="form-control-plaintext uom-definition" id="casing_weight_uom" data-objectname="Casing Weight" value="-" readonly="" style="border: 1px solid #ccc;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_grade">Casing Grade<span class="text-danger"> *</span></label>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" id="casing_grade" name="casing_grade[]" data-text="casing_grade" data-field="casing_grade" class="form-control form-control-input form-control-sm" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-2">
                                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_connection">Casing Connection<span class="text-danger"> *</span></label>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" id="casing_connection" name="casing_connection[]" data-text="casing_connection" data-field="casing_connection" class="form-control form-control-input form-control-sm" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-2" style="padding-bottom: 16px; margin-bottom: 10px;">
                                    <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_top_of_liner">Casing Top of Liner<span class="text-danger"> *</span></label>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                        <input type="text" id="casing_top_of_liner" name="casing_top_of_liner[]" data-text="casing_top_of_liner" data-field="casing_top_of_liner" class="form-control form-control-input form-control-sm" required="">
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <div class="col-lg-12 text-right">
                                        <button type="button" class="btn btn-danger btn-sm waves-effect waves-themed btn-action btn-delete-casing" style="width: 100px;">Delete Casing</button>
                                    </div>
                                </div>
                           </div>`;
            $("#section_casing").append(element);
            $("#section_casing").find('.casing-row').last().find('.hole-casing-tipe').select2({
                dropdownParent: $form,
                placeholder: "Select Casing Type",
                allowClear: true
            });
            $("#section_casing").find('.casing-row').last().find('.casinglookup').cmSelect2({
                url: $.helper.resolveApi("~/core/casing/lookup"),
                result: {
                    id: 'id',
                    text: 'name',
                    hole_type: 'hole_type'
                },
                filters: function (params) {
                    return [
                        {
                            logic: "AND",
                            filters: [{
                                field: "hole_id",
                                operator: "eq",
                                value: $('.holeandcasinglookup').val(),
                            },
                            {
                                field: "name",
                                operator: "contains",
                                value: params.term || '',
                            }]
                        }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select Casing",
                    allowClear: true,
                    tags: true,
                    templateResult: function (repo) {
                        return "<b>Casing:</b> " + repo.text + `"`;
                    },
                }
            });
        });

        $(document).on('click', '.btn-delete-casing', function () {
            $(this).closest('.casing-row').remove();
        });

        $btnSave.on('click', function () {
            var btn = $btnSave;
            var data = $form.serializeToJSON();
            console.log(data);
            var isValidate = $form[0].checkValidity();
            console.log(isValidate);
            if (isValidate) {
                Swal.fire({
                    title: "",
                    text: "Are you sure save to create a Hole and Casing?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes"
                }).then(function (result) {
                    if (result.value) {
                        var wellHole = {
                            id: $("#well_hole_id").val(),
                            hole_depth: $("#hole_depth").val().replace(",", ""),
                            plan_cost: $("#plan_cost").val(),
                            well_id: $("#well_id").val(),
                            plan_volume: $("#plan_volume").val(),
                            hole_id: $(".holeandcasinglookup").val(),
                            hole_type: holeType
                        };

                        var wellCasings = [];

                        for (var i = 0; i < $(".casinglookup").length; i++) {
                            wellCasings.push({
                                casing_id: $($(".casinglookup")[i]).val(),
                                casing_setting_depth: $($("input[name='casing_setting_depth[]")[i]).val(),
                                casing_type: $($("select[name='casing_type[]")[i]).val(),
                                casing_weight: $($("input[name='casing_weight[]")[i]).val(),
                                casing_grade: $($("input[name='casing_grade[]")[i]).val(),
                                casing_connection: $($("input[name='casing_connection[]")[i]).val(),
                                casing_top_of_liner: $($("input[name='casing_top_of_liner[]")[i]).val(),
                            })
                        }

                        if (wellCasings.length > 0) {
                            wellHole.casings = wellCasings
                        }
                        console.log(wellHole)
                        $.ajax({
                            url: $.helper.resolveApi('~/core/wellholeandcasing/savenew/'),
                            type: 'POST',
                            data: wellHole ,
                            datatype: 'json',
                            ContentType: 'application/json',
                            success: function (r) {
                                if (r.status.success) {
                                    toastr.success(r.status.message)
                                    btn.button('reset');
                                } else {
                                    toastr.error(r.status.message)
                                }
                                btn.button('reset');
                            },
                            error: function (r) {
                                btn.button('reset');
                                toastr.error(r.statusText);
                            }
                        });
                        //$.post($.helper.resolveApi('~/core/wellholeandcasing/savenew/'), { 'wellCasings': wellCasings }, function (r) {
                        //    if (r.status.success) {
                        //        toastr.success(r.status.message)
                        //        btn.button('reset');
                        //    } else {
                        //        toastr.error(r.status.message)
                        //    }
                        //    btn.button('reset');
                        //}, 'json').fail(function (r) {
                        //    btn.button('reset');
                        //    toastr.error(r.statusText);
                        //});
                    }
                });
            } else {
                $form.addClass('was-validated');
                toastr.error("Please complete all form Hole and Casing fields.");
                event.preventDefault();
                event.stopPropagation();
            }
        })

        var loadDetail = function () {
            holeAndCasingLookup();
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/wellholeandcasing/' + $recordId.val() + '/detailnew'), function (r) {
                    console.log("Well Hole and Casing");
                    console.log(r);
                    if (r.status.success) {
                        console.log(r)
                        holeCasingTypeLookup(r.data.casing_type + "");
                        $.helper.form.fill($form, r.data);
                        $(".holeandcasinglookup").select2("trigger", "select", {
                            data: { id: r.data.hole_id, text: r.data.hole_name }
                        });
                        if (r.data.hole_type) {
                            $btnAddCasing.hide();
                            $("#section_casing").hide();
                            $("#section_casing input, #section_casing select").removeAttr("required");
                        } else {
                            $btnAddCasing.show();
                            $("#section_casing").show();
                            $("#section_casing input, #section_casing select").attr("required", "");
                            r.data.casings.forEach(casing => {
                                var element = `<div class="casing-row">
                                                <hr class="mt-2 mb-3">
                                                    <div class="form-group row mb-2">
                                                        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="hole_name">Casing Diameter<span class="text-danger"> *</span></label>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                            <select class="form-control form-control-sm casinglookup" name="casing_id" data-text="casing_name" data-field="casing_id" placeholder="" required></select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-2">
                                                        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_name">Casing Setting Depth<span class="text-danger"> *</span></label>
                                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                            <div class="input-group">
                                                                <input class="form-control form-control-input numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" id="casing_setting_depth" name="casing_setting_depth[]" data-text="casing_setting_depth" data-field="casing_setting_depth" placeholder="Casing Setting Depth" inputmode="numeric" required=""
                                                                value="${casing.casing_setting_depth}">
                                                                <div class="input-group-append border-0 uom-background">
                                                                    <input type="text" class="form-control-plaintext uom-definition" id="casing_setting_depth_uom" data-objectname="Casing Setting Depth" value="-" readonly="" style="border: 1px solid #ccc;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-2 casing-type">
                                                        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_name">Casing Type<span class="text-danger"> *</span></label>
                                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                            <select class="form-control form-control-input hole-casing-tipe" id="casing_type" name="casing_type[]" data-field="casing_type" data-text required="">
                                                                <option value="1">Conductor</option>
                                                                <option value="2">Surface</option>
                                                                <option value="3">Drilling Liner</option>
                                                                <option value="4">Intermediate</option>
                                                                <option value="5">Production Liner</option>
                                                                <option value="6">Perforated Liner</option>
                                                                <option value="7">Production</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-2">
                                                        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_name">Casing Weight<span class="text-danger"> *</span></label>
                                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                            <div class="input-group">
                                                                <input class="form-control form-control-input numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" id="casing_weight" name="casing_weight[]" data-text="casing_weight" data-field="casing_weight" placeholder="Casing Weight" inputmode="numeric" required=""
                                                                value="${casing.casing_weight}">
                                                                <div class="input-group-append border-0 uom-background">
                                                                    <input type="text" class="form-control-plaintext uom-definition" id="casing_weight_uom" data-objectname="Casing Weight" value="-" readonly="" style="border: 1px solid #ccc;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-2">
                                                        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_grade">Casing Grade<span class="text-danger"> *</span></label>
                                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                            <input type="text" id="casing_grade" name="casing_grade[]" data-text="casing_grade" data-field="casing_grade" class="form-control form-control-input form-control-sm" required=""
                                                            value="${casing.casing_grade}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-2">
                                                        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_connection">Casing Connection<span class="text-danger"> *</span></label>
                                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                            <input type="text" id="casing_connection" name="casing_connection[]" data-text="casing_connection" data-field="casing_connection" class="form-control form-control-input form-control-sm" required=""
                                                            value="${casing.casing_connection}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-2" style="padding-bottom: 16px; margin-bottom: 10px;">
                                                        <label class="col-lg-3 col-md-3 col-sm-3 col-xs-3 control-label" for="casing_top_of_liner">Casing Top of Liner<span class="text-danger"> *</span></label>
                                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                            <input type="text" id="casing_top_of_liner" name="casing_top_of_liner[]" data-text="casing_top_of_liner" data-field="casing_top_of_liner" class="form-control form-control-input form-control-sm" required=""
                                                            value="${casing.casing_top_of_liner}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row mb-3">
                                                        <div class="col-lg-12 text-right">
                                                            <button type="button" class="btn btn-danger btn-sm waves-effect waves-themed btn-action btn-delete-casing" style="width: 100px;">Delete Casing</button>
                                                        </div>
                                                    </div>
                                               </div>`;
                                                $("#section_casing").append(element);
                                                $("#section_casing").find('.casing-row').last().find('.hole-casing-tipe').select2({
                                                    dropdownParent: $form,
                                                    placeholder: "Select Casing Type",
                                                    allowClear: true
                                                });
                                                $("#section_casing").find('.casing-row').last().find('.hole-casing-tipe').select2("val", casing.casing_type);
                                                $("#section_casing").find('.casing-row').last().find('.casinglookup').cmSelect2({
                                                    url: $.helper.resolveApi("~/core/casing/lookup"),
                                                    result: {
                                                        id: 'id',
                                                        text: 'name',
                                                        hole_type: 'hole_type'
                                                    },
                                                    filters: function (params) {
                                                        return [
                                                            {
                                                                logic: "AND",
                                                                filters: [{
                                                                    field: "hole_id",
                                                                    operator: "eq",
                                                                    value: $('.holeandcasinglookup').val(),
                                                                },
                                                                {
                                                                    field: "name",
                                                                    operator: "contains",
                                                                    value: params.term || '',
                                                                }]
                                                            }];
                                                    },
                                                    options: {
                                                        dropdownParent: $form,
                                                        placeholder: "Select Casing",
                                                        allowClear: true,
                                                        templateResult: function (repo) {
                                                            return "<b>Casing:</b> " + repo.text + `"`;
                                                        },
                                                    }
                                                });
                                                $("#section_casing").find('.casing-row').last().find('.casinglookup').select2("trigger", "select", {
                                                    data: { id: casing.casing_id, text: casing.casing_name }
                                                });
                            });
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            } else {
                holeCasingTypeLookup("");
            }
        };

        var setupSearchSelect2 = function () {
            $(document).on('keypress', '.select2-search__field', function () {
                $(this).val($(this).val().replace(/[^\d+(\.\d+)*$].+/, ""));
                if (!((event.which >= 48 && event.which <= 57) || event.which == 46)) {
                    event.preventDefault();
                }
            });
        };

        return {
            init: function () {
                //holeAndCasingLookup();
                GetWellObjectUomMap();
                maskingMoney();
                loadDetail();
                setupSearchSelect2();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});