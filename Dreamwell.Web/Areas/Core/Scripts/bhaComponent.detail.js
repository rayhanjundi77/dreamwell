﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formBhaComponent")

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/bhacomponent/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {
                });
            }
        };

        //$('#files').on("change", function () {
        //    console.log("test upload bha " + $recordId.val());
        //    upload($recordId.val())
        //});


        function upload(bhaComponentId) {
            console.log(bhaComponentId)
            var formData = new FormData();
            var files = $('#files')[0].files;
            if (files.length === 0) {
                console.error('No file selected');
                return;
            }
            
            formData.append('file', files[0]);
            $.ajax({
                url: $.helper.resolveApi("~/Core/BhaComponent/upload?d=" + bhaComponentId),
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log('Response:', response);
                    if (response.status.success) {
                        $("#image_id").val(response.data.recordId);
                        console.log("Image ID:", response.data.imageId);
                    } else {
                        console.error('Failed to upload:', response.status.message);
                    }
                },
                error: function (xhr, status, error) {
                    console.error('Error uploading file:', error);
                }
            });
        }


        $('#btn-save').click(function (event) {
            var $dtList = $('#dt_list');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                $.post($.helper.resolveApi('~/core/bhacomponent/save'), data, function (r) {
                    console.log('RR.', r)

                    /*
                    let promise = new Promise(function (resolve, reject) {
                        resolve(
                            upload(r.data.recordId)
                        )
                        reject(toastr.success(r.status.message))
                    })

                    promise.then(
                        (result) => {
                            if (r.status.success) {
                                //$('input[name=id]').val(r.data.recordId);
                                //upload(r.data.recordId);
                                $.helper.form.clear($form);
                                btn.button('reset');
                                toastr.success(r.status.message)
                                $('#formBhaComponent').modal('hide')
                                $dtList.DataTable().ajax.reload();

                            } else {
                                toastr.error(r.status.message)
                            }
                            //btn.button('reset');
                            //window.location.reload();
                        },
                        (error) => {
                            toastr.error(r.status.message)
                        }
                    )
                    */
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        upload(r.data.recordId);
                        toastr.success(r.status.message)
                      $dtList.DataTable().ajax.reload();
                    } else {
                      toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});