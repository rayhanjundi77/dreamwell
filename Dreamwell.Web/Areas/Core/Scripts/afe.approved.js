﻿$(document).ready(function () {
    'use strict';
    var $dt_approved = $('#dt_approved');
    var $pageLoading = $('#page-loading-content');

    var pageFunction = function () {
        var loadData = function () {
            var dt = $dt_approved.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/afe/approved"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }

                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "afe_no",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            console.log('ROW');
                            if (type === 'display') {
                                return `<a href="` + $.helper.resolve("/core/Afe/ApprovalDetail?id=") + row.id + `" class="text-bold text-info fw-700">` + row.afe_no + `</a>`;
                            }
                            return data;
                        }
                    },
                    { data: "well_name" },
                    { data: "field_name" },
                    { data: "unit_name" },
                    {
                        data: "duration",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return `<div>` + thousandSeparatorWithoutComma(row.duration) + `</div>`;
                            }
                            return data;
                        }
                    }
                ],

                //initComplete: function (settings, json) {
                //    $(this).on('click', '.row-deleted', function () {
                //        var recordId = $(this).closest('.btn-group').data('id');
                //        var b = bootbox.confirm({
                //            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                //            buttons: {
                //                confirm: {
                //                    label: "Delete"
                //                }
                //            },
                //            callback: function (result) {
                //                if (result) {
                //                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                //                    btnConfim.button('loading');
                //                    $.ajax({
                //                        type: "POST",
                //                        dataType: 'json',
                //                        contentType: 'application/json',
                //                        url: $.helper.resolveApi("~/core/afe/delete"),
                //                        data: JSON.stringify([recordId]),
                //                        success: function (r) {
                //                            if (r.status.success) {
                //                                toastr.error("Data has been deleted");
                //                            } else {
                //                                toastr.error(r.status.message);
                //                            }
                //                            btnConfim.button('reset');
                //                            b.modal('hide');
                //                            $dt_approved.DataTable().ajax.reload();
                //                        },
                //                        error: function (r) {
                //                            toastr.error(r.statusText);
                //                            b.modal('hide');
                //                        }
                //                    });
                //                    return false;
                //                }
                //            },
                //            animateIn: 'bounceIn',
                //            animateOut: 'bounceOut'
                //        });
                //    });
                //}
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadData();
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });


}(jQuery));