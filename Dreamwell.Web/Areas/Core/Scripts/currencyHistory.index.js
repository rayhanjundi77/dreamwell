﻿(function ($) {
    'use strict';

    var $dt_listCurrency = $('#dt_listCurrency');
    var $pageLoading = $('#page-loading-content');
    var $editDateModal = $('#editDateModal');
    var $editCurrencyModal = $('#editCurrencyModal');
    var $form = $('#form-editCurrency');
    var $formDate = $('#form-dateCurrency');
    var $btnhistory = $('#btn-history');
    var $btnDelete = $('#btn-delete');
    var $recordId = $('input[id=recordId]');
    
    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };    

    $editDateModal.niftyOverlay({
        iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
    });

    $editCurrencyModal.niftyOverlay({
        iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
    });

    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            currency_code: {
                validators: {
                    notEmpty: {
                        message: 'The currency code is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION


    var pageFunction = function () {
        $('#demo-dp-range .input-daterange').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate show', function (e) {
            // Revalidate the date when user change it
            $form.bootstrapValidator('revalidateField', 'start_date');
            $form.bootstrapValidator('revalidateField', 'end_date');
        });;

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $pageLoading.niftyOverlay('show');
                $.get($.helper.resolveApi('~core/Currency/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#currency_code').text(':: ' + r.data.currency_code);
                    }
                    $pageLoading.niftyOverlay('hide');
                }).fail(function (r) {
                    $.helper.noty.error(r.status, r.statusText);
                    $pageLoading.niftyOverlay('hide');
                });
            }
        }

        var loadDataTable = function () {
            var dt = $dt_listCurrency.cmDataTable({

                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/CurrencyHistory/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "currency_code" },
                    { data: "rate_value" },
                    { data: "start_date" },
                    { data: "end_date" },                    
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class="btn-group" data-id="`+ row.id + `">
                                    <a class ="row-edit btn btn-sm btn-default btn-hover-success demo-psi-pen-5 add-tooltip" href="#"
                                        data-original-title="Edit" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    
                    $(this).on('click', '.row-edit', function () {

                        var recordId = $(this).closest('.btn-group').data('id');

                        //$currencyFormModal.niftyOverlay('show'),
                        $editCurrencyModal.modal('show'),
                            $.helper.form.clear($form),
                            $.get($.helper.resolveApi('~core/CurrencyHistory/' + recordId + '/detail'), function (r) {
                                if (r.status.success) {
                                    $.helper.form.fill($form, r.data);
                                }
                                $editCurrencyModal.niftyOverlay('hide');
                            }).fail(function (r) {
                                $.helper.noty.error(r.status, r.statusText);
                                $editCurrencyModal.niftyOverlay('hide');
                            });

                    });
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 

            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.niftyOverlay('hide');
                if (processing) {
                    $pageLoading.niftyOverlay('show');
                } else {
                    $pageLoading.niftyOverlay('hide');
                }
            })
        }

        $('#btn-addNew').on('click', function () {
            //$.helper.form.clear($form),
            //    $.helper.form.fill($form, {
            //        currency_code: null,
            //        currency_description: null
            //    });
            //$currencyFormModal.modal('show');
            window.location = '/Detail'
        });

        $('#new-currency').on('click', function () {
            //$.helper.formDate.clear($formDate),
            //    $.helper.formDate.fill($formDate, {
            //        start_date: null,
            //        end_date: null
            //    });
            $editDateModal.modal('show');            
        });

        $('#btn-history').on('click', function () {
            console.log('abc');
            window.location = 'Currency/HistoryCurrency'
        });

        $('#btn-updateDate').on('click', function () {
            var btn = this;
            var data = $formDate.serializeToJSON();      
          
            $.post($.helper.resolveApi('~core/CurrencyHistory/generateitem'), data, function (r) {                
                setTimeout(function () {
                    if (r.status.success) {                        
                        loadDataTable();                        
                        $.helper.noty.success("Successfully", r.status.message);                       
                        $editDateModal.modal('hide');
                    } else {
                        $.helper.noty.error("Information", r.status.message);
                    }

                }, 2000);
            }, 'json').fail(function (r) {
                btn.button('reset');
                $.niftyNoty({
                    type: 'warning',
                    container: 'floating',
                    html: '<h4 class="alert-title">' + r.status + '</h4><p class="alert-message">' + r.statusText + '</p>',
                    closeBtn: true,
                    floating: {
                        position: 'top-right',
                        animationIn: 'bounceInRight',
                        animationOut: 'bounceOutRight'
                    },
                    focus: true,
                    timer: 0
                });
                $.helper.noty.error(r.status, r.statusText);
            });
        });

        $('#btn-updateRate').on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~core/CurrencyHistory/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            $('input[name=id]').val(r.data.recordId);
                            $.helper.noty.success("Successfully", r.status.message);
                            loadDataTable();
                        } else {
                            btn.button('reset');
                            $.helper.noty.error("Information", r.status.message);
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    $.niftyNoty({
                        type: 'warning',
                        container: 'floating',
                        html: '<h4 class="alert-title">' + r.status + '</h4><p class="alert-message">' + r.statusText + '</p>',
                        closeBtn: true,
                        floating: {
                            position: 'top-right',
                            animationIn: 'bounceInRight',
                            animationOut: 'bounceOutRight'
                        },
                        focus: true,
                        timer: 0
                    });
                    $.helper.noty.error(r.status, r.statusText);
                });

            }
            else return;
        });


        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));