﻿$(document).ready(function () {
    'use strict';
    var $pageLoading = $('.page-loading-content');
    var dt = $('#dt_listDetailContract');
    var dtApprove = $('#dt_listddrapprove');
    var dtApproved = $('#dt_listddrapproved');


    var $label = {
        total_new_DDR: $('label#total_new_DDR'),
        total_well: $('label#total_well'),
        total_DDR: $('label#total_DDR'),
        totalDDRUpload: $('label#total_ddr_upload'),
        total_contract_active: $('label#total_contract_active'),
        total_nearly_expired_contracts: $('label#total_nearly_expired_contracts'),
        total_expired_contracts: $('label#total_expired_contracts'),
        total_daily_cost: $('label#total_daily_cost'),
        total_DDR_Approval: $('label#total_DDR_Approval'),
        total_DDR_Approved: $('label#total_DDR_Approved')
    };


    


    var pageFunction = function () {

        function getTotalNewDDR() {
            $.get($.helper.resolveApi('~/core/Drilling/getViewAllNew?isadmin='+$("#userisadmin").val()), function (r) {
                if (r.status.success) {
                    // $label.total_new_DDR.text(r.data.length);
                    $label.total_new_DDR.text(r.data);
                }
            }, 'json').fail(function (r) {

            });
        };

        // function getTotalWell() {
        //     $.get($.helper.resolveApi('~/core/Well/getAll'), function (r) {
        //         if (r.status.success) {
        //             $label.total_well.text(r.data.length);
        //         }
        //     }, 'json').fail(function (r) {
        //         //console.log(r);
        //     });
        // };

        function getTotalWell() {
            $.get($.helper.resolveApi('~/core/Well/getAllNew?isadmin='+$("#userisadmin").val()), function (r) {
                if (r.status.success) {
                    $label.total_well.text(r.data);
                }
            }, 'json').fail(function (r) {
                //console.log(r);
            });
        };

        
        /*
        async function getTotalDDR() {
            try {
                const response = await $.get($.helper.resolveApi('~/core/approvalhistory/approvedHistory?isadmin=' + $("#userisadmin").val()), 'json');
                console.log("ini", response);
                if (response.status.success) {
                    $label.total_DDR.text(response.data);
                }
            } catch (error) {
                // Handle error
                console.error(error);
            }
        }
        */

        // function getTotalContractActive() {
        //     $.get($.helper.resolveApi('~/core/Contract/getAll'), function (r) {
        //         if (r.status.success) {
        //             $label.total_contract_active.text(r.data.length);
        //         }
        //     }, 'json').fail(function (r) {

        //     });
        // };

        function getTotalContractActive() {
            $.get($.helper.resolveApi('~/core/Contract/getAllNew?isadmin='+$("#userisadmin").val()), function (r) {
                if (r.status.success) {
                    $label.total_contract_active.text(r.data);
                }
            }, 'json').fail(function (r) {

            });
        }; 

        function getTotalNearlyExpiredContract() {
            $.get($.helper.resolveApi('~core/Contract/getAllNearExpired'), function (r) {
                if (r.status.success) {
                    $label.total_nearly_expired_contracts.text(r.data);
                    //console.log("ini total near", r.data);
                }
            }, 'json').fail(function (r) {

            });
        }; 

        function getTotalExpiredContract() {
            $.get($.helper.resolveApi('~core/Contract/getAllExpired'), function (r) {
                if (r.status.success) {
                    $label.total_expired_contracts.text(r.data);
                }
            }, 'json').fail(function (r) {

            });
        }; 

        function getTotalDailyCost() {
            $.get($.helper.resolveApi('~/core/DailyCost/getAll'), function (r) {
                if (r.status.success) {
                    $label.total_daily_cost.text(r.data.length);
                }
            }, 'json').fail(function (r) {

            });
        };

        function getTotalApproval() {
            $.get($.helper.resolveApi("~/core/Drilling/getTotalApproval"), function (response) {
                if (response.status.success) {
                    var totalData = response.data;
                   // console.log(totalData);
                    $label.total_DDR_Approval.text(totalData);
                }
            }, 'json').fail(function (response) {
                // Handle failure jika diperlukan
            });
        }

        function getTotalRejected() {
            $.get($.helper.resolveApi("~/core/Drilling/getTotalRejected"), function (response) {
                if (response.status.success) {
                    var totalData = response.data;
                    //console.log("rejek",totalData);
                    //$label.total_DDR_Approval.text(totalData);
                }
            }, 'json').fail(function (response) {
                // Handle failure jika diperlukan
            });
        }


        function getTotalApproved() {
            $.get($.helper.resolveApi("~/core/Drilling/getTotalApproved"), function (response) {
                if (response.status.success) {
                    var totalData = response.data;
                    //console.log(totalData);
                    $label.total_DDR_Approved.text(totalData);
                }
            }, 'json').fail(function (response) {
                // Handle failure jika diperlukan
            });
        }


      
        function initDataTable() {
            //dt.cmDataTable({
            //    pageLength: 10,
            //    processing: true,
            //    serverSide: true,
            //    ordering: true,
            //    ajax: {
            //        url: $.helper.resolveApi("~/core/contract/nearexpired/dataTable"),
            //        type: "POST",
            //        contentType: "application/json",
            //        data: function (d) {
            //            return JSON.stringify(d);
            //        }
            //    },
            //    columns: [
            //        {
            //            data: "id",
            //            orderable: false,
            //            searchable: false,
            //            sortable: true,
            //            class: "text-center",
            //            render: function (data, type, row, meta) {
            //                return meta.row + meta.settings._iDisplayStart + 1;
            //            }
            //        },
            //        {
            //            data: "contract_no",
            //            name: "contract_no",
            //            orderable: true,
            //            sortable: true,
            //            searchable: true,
            //            class: "text-left",
            //            render: function (data, type, row) {
            //                if (type === 'display') {
            //                    return `<a href="` + $.helper.resolve("/core/contractdetail?id=") + row.id + `" class="text-bold text-info fw-700" target="_blank">` + row.contract_no + `</a>`;//row.contract_no;
            //                }
            //                return data;
            //            }
            //        },
            //        { data: "contract_title" },
            //        {
            //            data: "total",
            //            orderable: true,
            //            searchable: true,
            //            sortable: true,
            //            class: "text-right",
            //            render: function (data, type, row) {
            //                if (type === 'display') {
            //                    if (row.total == null) {
            //                        return "N/A";
            //                    } else {
            //                        return "USD " + thousandSeparatorWithoutComma(Math.round(row.total * 100) / 100);
            //                    }
            //                }
            //                return data;
            //            }
            //        },
            //        { data: "vendor_name", sortable: true, },
            //        { data: "unit_name", sortable: true, },
            //        {
            //            data: "start_date",
            //            orderable: true,
            //            searchable: true,
            //            class: "text-left",
            //            render: function (data, type, row) {
            //                if (type === 'display') {
            //                    return moment(row.start_date).format('MMM DD, YYYY');
            //                }
            //                return data;
            //            }
            //        },
            //        {
            //            data: "end_date",
            //            orderable: true,
            //            searchable: true,
            //            class: "text-left",
            //            render: function (data, type, row) {
            //                if (type === 'display') {
            //                    return moment(row.end_date).format('MMM DD, YYYY');
            //                }
            //                return data;
            //            }
            //        },
            //    ],
            //}, function (e, settings, json) {
            //    var $table = e; // table selector 
            //});
        };

        $(".ddr-btn").click(function () {
            var url = "";
            if ($(this).data().context == "approval") {
                url = $.helper.resolveApi("~/core/Drilling/getAllApproval")
            } else {
                url = $.helper.resolveApi("~/core/Drilling/getAllApproved")
            }

            dtApprove.cmDataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
                ordering: true,
                ajax: {
                    url: url,
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        sortable: true,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "well_name" },
                    { data: "dfs" },
                    {
                        data: "drilling_date",
                        orderable: false,
                        searchable: false,
                        class: "text-left align-middle",
                        render: function (data, type, row) {
                            // console.log('test');
                            // console.log(row);
                            var date = moment(row.drilling_date).format('MMM DD, YYYY');
                            if (type === 'display') {
                                return `<a href="` + $.helper.resolve("/core/drilling/ApprovalDetail?id=" + row.id + "&well_id=" + row.well_id) + `" target="_blank" class="text-bold text-info fw-700" style="border-bottom: 0 !important">` + date + `</a>`;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center p-1 align-middle",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.approval_rejected_level == null) {
                                    if (row.approval_level == 2 && row.approval_status == 200)
                                        return `<span class="badge badge-success p-1 pr-2 pl-2">Approved by Manager</span>`;
                                    else if (row.approval_level == 1 && row.approval_status == 2)
                                        return `<span class="badge badge-warning p-1 pr-2 pl-2">Waiting for Approval Engineer</span>`;
                                    else if (row.approval_level == 2 && row.approval_status == 2)
                                        return `<span class="badge badge-info p-1 pr-2 pl-2">Waiting for Approval Manager</span>`;
                                    else
                                        return `<span class="badge badge-danger p-1 pr-2 pl-2">[ Level: ` + row.approval_level + ` ] [ Status: ` + row.approval_status + ` ]</span>`;
                                } else {
                                    if (row.approval_rejected_level == 1)
                                        return `<span class="badge badge-danger p-1 pr-2 pl-2">Rejected by Engineer</span>`;
                                    else if (row.approval_rejected_level == 2)
                                        return `<span class="badge badge-danger p-1 pr-2 pl-2">Rejected by Manager</span>`;
                                }
                            }
                            return data;
                        },
                    },
                ],
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });
        });



        $(".contract-btn").click(function () {
            var url = "";
            if ($(this).data().context == "nearly-expired") {
                url = $.helper.resolveApi("~/core/contract/nearexpired/dataTable")
            } else {
                url = $.helper.resolveApi("~/core/contract/expired/dataTable")
            }

            dt.cmDataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
                ordering: true,
                ajax: {
                    url:url,
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        sortable: true,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "contract_no",
                        name: "contract_no",
                        orderable: true,
                        sortable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return `<a href="` + $.helper.resolve("/core/contractdetail?id=") + row.id + `" class="text-bold text-info fw-700" target="_blank">` + row.contract_no + `</a>`;//row.contract_no;
                            }
                            return data;
                        }
                    },
                    { data: "contract_title" },
                    {
                        data: "total",
                        orderable: true,
                        searchable: true,
                        sortable: true,
                        class: "text-right",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.total == null) {
                                    return "N/A";
                                } else {
                                    return "USD " + thousandSeparatorWithoutComma(Math.round(row.total * 100) / 100);
                                }
                            }
                            return data;
                        }
                    },
                    { data: "vendor_name", sortable: true, },
                    { data: "unit_name", sortable: true, },
                    {
                        data: "start_date",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return moment(row.start_date).format('MMM DD, YYYY');
                            }
                            return data;
                        }
                    },
                    {
                        data: "end_date",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return moment(row.end_date).format('MMM DD, YYYY');
                            }
                            return data;
                        }
                    },
                ],
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });
        });

        var end = [];
        var now = [];
        function getContract() {
            $.get($.helper.resolveApi('~/core/Contract/getAll'), function (r) {
                //console.log(r.data.contract_no);
                if (r.status.success) {
                    $.each(r.data, function (key, value) {
                        end.push(moment(value.end_date).format('DD-MM-YYYY'));
                        now.push(moment(new Date()).format('DD-MM-YYYY'));

                    })
                    //$label.total_DDR.text(r.data.length);
                }   
            }, 'json').fail(function (r) {

            });
        };
        return {
            init: function () {
                getTotalNewDDR();
                getTotalWell();
                //getTotalDDR();
                getTotalRejected();
                getContract();
                getTotalContractActive();
                getTotalDailyCost();
                getTotalNearlyExpiredContract();
                getTotalExpiredContract();
                initDataTable();
                getTotalApproval();
                getTotalApproved();
            }
        };
    }();

    $(document).ready(function () {
        pageFunction.init();
    });


});