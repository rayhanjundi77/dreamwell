﻿(function ($) {
    'use strict';
    var $dt_listContract = $('#dt_listContract');
    var $pageLoading = $('#page-loading-content');
    var pageFunction = function () {
        var loadDataTable = function () {
            var dt = $dt_listContract.cmDataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
                ordering: true,
                ajax: {
                    url: $.helper.resolveApi("~/core/contract/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: true, // Tambahkan properti orderable: true
                        searchable: false,
                        sortable: true,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "contract_no",
                        name: "contract_no",
                        orderable: true,
                        sortable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return `<a href="` + $.helper.resolve("/core/contractdetail?id=") + row.id + `" class="text-bold text-info fw-700">` + row.contract_no + `</a>`;
                            }
                            return data;
                        }
                    },
                    { data: "contract_title", orderable: true }, // Tambahkan properti orderable: true
                    {
                        data: "total",
                        orderable: true,
                        searchable: true,
                        sortable: true,
                        class: "text-right",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.total == null) {
                                    return "N/A";
                                } else {
                                    return "USD " + thousandSeparatorWithoutComma(Math.round(row.total * 100) / 100);
                                }
                            }
                            return data;
                        }
                    },
                    { data: "vendor_name", sortable: true },
                    { data: "unit_name", sortable: true },
                    {
                        data: "start_date",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return moment(row.start_date).format('MMM DD, YYYY');
                            }
                            return data;
                        }
                    },
                    {
                        data: "end_date",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return moment(row.end_date).format('MMM DD, YYYY');
                            }
                            return data;
                        }
                    },
                    //{
                    //    data: "id",
                    //    orderable: true,
                    //    searchable: true,
                    //    class: "text-center",
                    //    render: function (data, type, row) {
                    //        if (type === 'display') {
                    //            var output = ``;
                    //            if (row.submitted_by != null && row.submitted_on != null) {
                    //                output = `<span class="badge badge-warning pt-1 pb-1 pl-3 pr-3">Final</span>`;
                    //            } else {
                    //                output = `<span class="badge badge-secondary pt-1 pb-1 pl-3 pr-3">Draft</span>`;
                    //            }
                    //            return output;
                    //        }
                    //        return data;
                    //    }
                    //},
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                {
                                  return `<div class ="btn-group" data-id= "` + row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/core/contract/detail?id=") + row.id + `" class="modalTrigger btn btn-primary btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#contractModal">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-warning btn-xs btn-info waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                </div>`; 
                                }                                
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return '';
                            }
                            return data;
                        }
                    },
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/contract/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dt_listContract.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });
            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 