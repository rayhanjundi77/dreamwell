$(document).ready(function () {
    'use strict';

    var $recordId = $('input[id=well_hole_and_casing_id]');
    var $wellid = $('input[id=well_id]');
    var $form = $("#form-casinggNew");
    var $btnDelete = $("#btn-mc-delete");

    var ObjectMapper = [];

    var holeCasingTypeLookup = function (selectedId) {
        if (selectedId == "") {
            $(".hole-casing-tipe").val('');
        } else {
            $(".hole-casing-tipe").val(selectedId);
        }
        $(".hole-casing-tipe").select2({
            dropdownParent: $form,
            placeholder: "Select Casing Type",
            allowClear: true
        });
    };

    var pageFunction = function () {
        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            $(".numeric").inputmask({
                digits: 0,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        };

        var GetWellObjectUomMap = function () {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $wellid.val()), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        ObjectMapper = r.data;
                        $("form#form-wellHoleCasing :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname");
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                $.each(r.data, function (key, value) {
                                    if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                        $("#" + elementId).val("" + value.uom_code + "");
                                    }
                                });
                            }
                        });
                    }
                }
            });
        };

      


        var holeAndCasingLookup = function () {
            $(".holeandcasinglookup").cmSelect2({
                url: $.helper.resolveApi("~/core/holeandcasing/lookup"),
                result: {
                    id: 'id',
                    text: 'hole_name',
                    casing_name: 'casing_name',
                    hole_type: 'hole_type'
                },
                filters: function (params) {
                    return [{
                        field: "hole_name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select Hole and Casing",
                    allowClear: true,
                    templateResult: function (repo) {
                        return "<b>Hole:</b> " + repo.text + " <b>Casing:</b> " + repo.casing_name;
                    },
                }
            });
        };

        $(".holeandcasinglookup").on('select2:select', function (e) {
            if (e.params.data.hole_type) {
                $("#section_casing").hide();
                $("#section_casing input.form-control-input, #section_casing select").removeAttr("required");
                $("#section_casing input.form-control-input").val("");
            } else {
                $("#section_casing").show();
                $("#section_casing input.form-control-input, #section_casing select").attr("required", "");
            }
            $('#hole_name').val(e.params.data.text);
            $('#casing_name').val(e.params.data.casing_name);
        });

        Handlebars.registerHelper('printHoleCasingType', function (type, id) {
            var html = ` <select class="form-control hole-casing-tipe" name="casing_type" data-field="casing_type" data-text required>
                            <option value="1">Conductor</option>
                            <option value="2">Surface</option>
                            <option value="3">Drilling Liner</option>
                            <option value="4">Intermediate</option>
                            <option value="5">Production Liner</option>
                            <option value="6">Perforated Liner</option>
                            <option value="7">Production</option>
                        </select>`;
            html += "<div class=\"invalid-feedback\">Casing type cannot be empty</div>";
            return new Handlebars.SafeString(html);
        });

        var loadMultiCasingData = function () {
            var templateScript = $("#multicasing-template").html();
            var template = Handlebars.compile(templateScript);
            var recordId = $recordId.val();

            if (!recordId) {
                console.error("Record ID is not defined");
                return;
            }

            console.log("Fetching sETAAANNNrecordId");

            $.get($.helper.resolveApi('~/core/wellholeandcasing/' + recordId + '/getAllHoleCasingByWellId'), function (r) {
                console.log("Response received:", r);

                if (r.status.success && r.data.length > 0) {
                    $("#form-multicasing > .panel-body").html(template({ data: r.data }));
                    maskingMoney();
                    console.log("Data rendered:", r.data);
                } else {
                    console.warn("No data found or response unsuccessful");
                }
            }).fail(function (r) {
                console.error("Failed to fetch data:", r);
            });
        };

        var loadDetail = function () {
            holeAndCasingLookup();
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/wellholeandcasing/' + $recordId.val() + '/detail'), function (r) {
                    console.log("Well Hole and Casing details received:", r);
                    if (r.status.success) {
                        holeCasingTypeLookup(r.data.casing_type + "");
                        loadMultiCasingData();

                        $.helper.form.fill($form, r.data);
                        $("#multicasing").val(1);

                        if (r.data.hole_type) {
                            $("#section_casing").hide();
                            $("#section_casing input, #section_casing select").removeAttr("required");
                        } else {
                            $("#section_casing").show();
                            $("#section_casing input, #section_casing select").attr("required", "");
                        }
                    }

                    $('.loading-detail').hide();
                }).fail(function (r) {
                    console.error("Failed to fetch details:", r);
                });
            } else {
                holeCasingTypeLookup("");
            }
        };

        $(document).on('click', '#btn-mc-delete', function () {
            var recordIdHCD = $(this).data("id");
            var b = bootbox.confirm({
                message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this! </p>",
                buttons: {
                    confirm: {
                        label: "Delete"
                    }
                },
                callback: function (result) {
                    if (result) {
                        var btnConfim = $(this).find('.bootbox-accept');
                        btnConfim.button('loading');
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            url: $.helper.resolveApi("~/core/wellholeandcasing/delete"),
                            data: JSON.stringify([recordIdHCD]),
                            success: function (r) {
                                if (r.status.success) {
                                    toastr.success("Data has been deleted");
                                    location.reload(); 
                                } else {
                                    toastr.error(r.status.message);
                                }
                                btnConfim.button('reset');
                                b.modal('hide');
                                $dtBasic.DataTable().ajax.reload();
                            },
                            error: function (r) {
                                toastr.error(r.statusText);
                                b.modal('hide');
                            }
                        });
                        return false;
                    }
                },
                animateIn: 'bounceIn',
                animateOut: 'bounceOut'
            });
        });

        return {
            init: function () {
                GetWellObjectUomMap();
                maskingMoney();
                loadDetail();
            }
        };
    }();

    pageFunction.init();
});
