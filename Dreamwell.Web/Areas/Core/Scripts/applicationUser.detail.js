﻿(function ($) {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $('#form-applicationUser');
    var $pageLoading = $('#page-loading');
    var $btnSave = $('#btn-save');
    var $btnDelete = $('#btn-delete');


    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    }



    $('#inputImage').on("change", function () {

        console.log("update profile picture");
        console.log("test id profile picture " + $recordId.val());
        upload($recordId.val())
    });


    function upload(iduser) {
        var formData = new FormData();
        var file = $('#inputImage')[0];
        console.log(file.files[0]);
        formData.append('file', file.files[0]);

        $.ajax({
            url: $.helper.resolveApi("~/Core/ApplicationUser/uploadImagePicture?d=" + iduser),
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (r) {
                if (r.status.success) {
                    toastr.success(r.status.message);
                }
            },
            error: function () {
                toastr.success(r.status.message);
            }
        });
        return;
    }
    var pageFunction = function () {

        
        //$("#business_unit_id").cmSelect2({
        //    url: $.helper.resolveApi('~core/BusinessUnit/lookup'),
        //    result: {
        //        id: 'id',
        //        text: 'unit_name'
        //    },
        //    filters: function (params) {
        //        return [{
        //            field: "unit_name",
        //            operator: "contains",
        //            value: params.term || '',
        //        }];
        //    },
        //    options: {
        //        placeholder: "Select a strategic goal",
        //        allowClear: true,
        //    }
        //});

        //// Content OverLay
        //$pageLoading.niftyOverlay({
        //    iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
        //});
        

        // FORM VALIDATION
        // =================================================================
        $form.bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: faIcon,
            fields: {
                app_username: {
                    message: 'The username is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The username is required.'
                        }
                    }
                },
                first_name: {
                    message: 'The first name is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The first name is required.'
                        }
                    }
                },
                //email: {
                //    validators: {
                //        notEmpty: {
                //            message: 'The email address is required and can\'t be empty'
                //        },
                //        emailAddress: {
                //            message: 'The input is not a valid email address'
                //        }
                //    }
                //}
            }
        }).on('success.field.bv', function (e, data) {
            // $(e.target)  --> The field element
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');
        });
        // END FORM VALIDATION

        $('#btn-business-unit-lookup').click(function () {
            var btn = $(this);
            btn.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/businessunit/Lookup',
                success: function (data) {
                    btn.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "",
                        callback: function (e) {
                            console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o, event) {
                        $box.modal('hide');
                        console.log(event.node.id);
                        console.log(event.node.text);
                        $("#business_unit_id").val(event.node.id);
                        $("#btn-business-unit-lookup").text(event.node.text);
                    });
                }
            });
        });

        async function getFileData(filemasterid, datagender) {
            return $.ajax({
                url: $.helper.resolveApi("~Core/GetFile/" + filemasterid + "/detailImageFile"),
                type: 'GET',
                contentType: "application/json",
                success: function (r) {
                    console.log("get image file")
                    console.log(r);

                  
                    if (datagender) {
                        $('#img-app-user-profile-male').attr('src', $.helper.resolve("/uploadProfilePicture/") + r.data.filepath);

                        $('#img-app-user-profile-male').show();
                        $('#img-app-user-profile-female').hide();
                    } else {
                        $('#img-app-user-profile-male').attr('src', $.helper.resolve("/uploadProfilePicture/") + r.data.filepath);
                        $('#img-app-user-profile-male').hide();
                        $('#img-app-user-profile-female').show();
                    }


                },
                error: function (r) {

                },
            });

        }

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/ApplicationUser/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#fullname').text(r.data.app_fullname);

                        getFileData($recordId.val(), r.data.gender);

                      
                     
                        $.helper.form.fill($form, r.data);
                        //data-href="@Url.Content("/core/ApplicationMenu/ChangePassword?")" 
                        
                        $('#btn-resetPasswordModal').attr('data-href', $.helper.resolve("~core/ApplicationUser/ChangePassword?id=" + $recordId.val()));

                        console.log(r);

                        $("#business_unit_id").val(r.data.business_unit_id);
                        $("#btn-business-unit-lookup").text(r.data.unit_name);
                    }
                }).fail(function (r) {
                    toastr.error(r.statusText);
                });
            }
        }


        /*BEGIN::Event*/
        $btnSave.on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            console.log("Validator");
            console.log($form);
            console.log(validator);
            console.log("End Validator");


            var data = $form.serializeToJSON();
            console.log(data);
            $.post($.helper.resolveApi('~/core/ApplicationUser/save'), data, function (r) {
                if (r.status.success) {
                    history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordId);
                    $('input[name=id]').val(r.data.recordId);
                    toastr.success(r.status.message);
                    loadDetail();
                } else {
                    btn.button('reset');
                    toastr.error(r.status.message);
                }
                btn.button('reset');
            }, 'json').fail(function (r) {
                toastr.error(r.statusText);
            });


            //}
            //else return;
        });

        /*END::Event*/

        return {
            init: function () {
                loadDetail();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));