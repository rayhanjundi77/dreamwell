﻿(function ($) {
    'use strict';
    var $dt_listAfeDetail = $('#dt_listAfeDetail');
    var $pageLoading = $('#page-loading-content');
    var $afeId = $('input[id=afeId]');
    var $afeLineId = $('input[id=afeLineId]');

    //<input type="text" id="recordId" name="id" data-field="id" value="@ViewBag.recordId" />
    //    <input type="text" name="afeId" data-field="afeId" value="@ViewBag.afeId" />
    //    <input type="text" name="afeLineId" data-field="afeLineId" value="@ViewBag.afeLineId" />

    var pageFunction = function () {
        $.fn.editable.defaults.url = "/post";
        $.fn.editable.defaults.mode = 'inline';

        var initEditableOriginalBudget = function () {
            $(".editable-original-budget").editable({
                url: $.helper.resolveApi("~/core/AfeManageBudget/save"),
                ajaxOptions: { contentType: 'application/json', dataType: 'json' },
                params: function (params) {
                    var amount = params.value;//.replace(/\./g, '');//.replace(/,/g, "");
                    var req = {
                        id: $(this).data('id'),
                        afe_id: $(this).data('afeid'),
                        afe_line_id: $(this).data('afelineid'),
                        contract_detail_id: $(this).data('contractdetailid'),
                        original_budget: amount,
                    };
                    console.log(req);
                    return JSON.stringify(req);
                },
                type: "number",
                step: 'any',
                pk: 1,
                name: "amount",
                title: "Enter Program Budget",
                display: function (value) {
                    //var numeric = value.replace(/\./g, '');//.replace(/,/g, "");
                    $(this).text(thousandSeparatorDecimal(value));
                },
                validate: function (value) {
                    var myStr = value.replace(/\./g, '');//.replace(/,/g, "");
                    if ($.isNumeric(myStr) == '') {
                        return 'Only numbers are allowed';
                    }
                },
                success: function (r) {
                    console.log(r);
                    if (r.status.success) {
                        //$(this).removeAttr("data-id");
                        //$(this).closest("tr").children(".programBudget").find("a").attr("id", "programBudget-" + r.data.id);
                        //$(this).closest("tr").children(".revBudget1").find("a").attr("id", "revBudget1-" + r.data.id);
                        //$(this).closest("tr").children(".revBudget2").find("a").attr("id", "revBudget2-" + r.data.id);
                        //$(this).data("id", r.data.id);
                        //$(this).closest("tr").children(".revBudget1").find("a").data("id", r.data.id);
                        //$(this).closest("tr").children(".revBudget2").find("a").data("id", r.data.id);
                        //$(this).closest("tr").children(".contractBudget").find("a").html(thousandSeparatorWithoutComma(r.data.original_budget));
                        loadManageBudget();
                        toastr.success(r.status.message);
                    } else {
                        toastr.error(r.status.message);
                        return r.status.message;
                    }
                }
            }).on('shown', function (e, editable) {
                autoNumericEditable(editable);
            });
        }
        var initEditableReviseBudget1 = function () {
            $(".editable-rev-budget-1").editable({
                url: $.helper.resolveApi("~/core/AfeManageBudget/save"),
                ajaxOptions: { contentType: 'application/json', dataType: 'json' },
                params: function (params) {
                    var amount = params.value;
                    var req = {
                        id: $(this).data('id'),
                        afe_id: $(this).data('afeid'),
                        afe_line_id: $(this).data('afelineid'),
                        contract_detail_id: $(this).data('contractdetailid'),
                        rev_budget_1: amount,
                    };
                    return JSON.stringify(req);
                },
                type: "number",
                step: 'any',
                pk: 1,
                name: "text",
                title: "Enter Revise Budget 1",
                display: function (value) {
                    //var numeric = value.replace(/\./g, '');//.replace(/,/g, "");
                    $(this).text(thousandSeparatorDecimal(value));
                },
                validate: function (value) {
                    var myStr = value.replace(/\./g, '');//.replace(/,/g, "");
                    if ($.isNumeric(myStr) == '') {
                        return 'Only numbers are allowed';
                    }
                },
                success: function (r) {
                    console.log(r);
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        loadManageBudget();
                        //$(this).closest("tr").children(".contractBudget").find("a").html(thousandSeparatorWithoutComma(r.data.rev_budget_1));
                    } else {
                        toastr.error(r.status.message);
                        return r.status.message;
                    }
                }
            })
            .on('shown', function (e, editable) {
                autoNumericEditable(editable);
            });
        }
        var initEditableReviseBudget2 = function () {
            $(".editable-rev-budget-2").editable({
                url: $.helper.resolveApi("~/core/AfeManageBudget/save"),
                ajaxOptions: { contentType: 'application/json', dataType: 'json' },
                params: function (params) {
                    var amount = params.value;//.replace(/\./g, '');//.replace(/,/g, "");
                    var req = {
                        id: $(this).data('id'),
                        afe_id: $(this).data('afeid'),
                        afe_line_id: $(this).data('afelineid'),
                        contract_detail_id: $(this).data('contractdetailid'),
                        rev_budget_2: amount,
                    };
                    return JSON.stringify(req);
                },
                type: "number",
                step: 'any',
                pk: 1,
                name: "amount",
                title: "Enter Revise Budget 2",
                display: function (value) {
                    //var numeric = value.replace(/\./g, '');//.replace(/,/g, "");
                    $(this).text(thousandSeparatorDecimal(value));
                },
                validate: function (value) {
                    var myStr = value.replace(/\./g, '');//.replace(/,/g, "");
                    if ($.isNumeric(myStr) == '') {
                        return 'Only numbers are allowed';
                    }
                },
                success: function (r) {
                    console.log(r);
                    if (r.status.success) {
                        loadManageBudget();
                        toastr.success(r.status.message);
                        //$(this).closest("tr").children(".contractBudget").find("a").html(thousandSeparatorWithoutComma(r.data.rev_budget_2));
                    } else {
                        toastr.error(r.status.message);
                        return r.status.message;
                    }
                }
            }).on('shown', function (e, editable) {
                autoNumericEditable(editable);
            });
        }
       
        var loadManageBudget = function () {
            var templateScript = $("#afeManageBudget-template").html();
            var template = Handlebars.compile(templateScript);

            $.get($.helper.resolveApi('~/core/AfeManageBudget/getDetail/' + $afeId.val() + '/' + $afeLineId.val()), function (r) {
                console.log(r);
                if (r.status.success && r.data.length > 0) {
                    $("#listAfeManageBudget > tbody").html(template({ data: r.data }));
                }
                initEditableOriginalBudget();
                initEditableReviseBudget1();
                initEditableReviseBudget2();
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        Handlebars.registerHelper("printOriginalBudget", function (id, afeLineId, contractDetailId, originalBudget) {
            var html = "";
            if (!isNaN(originalBudget) && originalBudget > 0) {
                html = "<span>USD " + thousandSeparatorDecimal(originalBudget) + "</span>";
            }
            else {
                html = `<a class="editable-original-budget" id="originalBudget-` + id + `" data-afelineid="` + afeLineId + `" data-afeid="` + $afeId.val() + `"  data-contractdetailid="` + contractDetailId +`" data-title="Enter Amount">
                        </a>`;
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper("printRevBudget1", function (id, afeLineId, contractDetailId, originalBudget, revBudget1) {
            var html = "";
            if (!isNaN(originalBudget) && originalBudget > 0) {
                if (!isNaN(revBudget1) && revBudget1 > 0) {
                    html = "<span>USD " + thousandSeparatorDecimal(revBudget1) + "</span>";
                } else {
                    html = `<a class="editable-rev-budget-1" data-afelineid="` + afeLineId + `" data-afeid="` + $afeId.val() + `" data-id="`+id+`" data-contractdetailid="` + contractDetailId + `" data-title="Enter Amount">
                            ` + revBudget1 + `
                        </a>`;
                }
            } else {
                html = "";
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper("printRevBudget2", function (id, afeLineId, contractDetailId, revBudget1, revBudget2) {
            var html = "";
            if (!isNaN(revBudget1) && revBudget1 > 0) {
                if (!isNaN(revBudget2) && revBudget2 > 0) {
                    html = "<span>USD " + thousandSeparatorDecimal(revBudget2) + "</span>";
                } else {
                    html = `<a class="editable-rev-budget-2" data-afelineid="` + afeLineId + `" data-afeid="` + $afeId.val() + `" data-id="` + id + `" data-contractdetailid="` + contractDetailId + `" data-title="Enter Amount">
                            ` + revBudget2 + `
                        </a>`;
                }
            } else {
                html = "";
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper("printActualBudgetDetail", function (budget) {
            if (!isNaN(budget) && budget > 0) {
                return "USD " + thousandSeparatorDecimal(budget);
            } else {
                return "USD 0";
            }
        });
        Handlebars.registerHelper("counter", function (index) {
            return index + 1;
        });

        return {
            init: function () {
                loadManageBudget();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 