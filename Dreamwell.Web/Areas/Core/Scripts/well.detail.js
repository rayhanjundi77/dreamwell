﻿(function ($) {
    'use strict';

    var $recordId = $('input[id=recordId]');
    var formGWI = $('#form-gwi');
    var formTVD = $('#form-tdc');
    var formCVD = $('#form-cvd');
    var formDH = $('#form-dh');
    var formPI = $('#form-pi');
    var formHCD = $('#form-hcd');
    var formHCDNew = $('#form-hcd-new');
    var formRD = $('#form-rd');
    var formBI = $('#form-bi');
    var formWellDeviation = $('#formWellDeviation');

    var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
    }

    var btn_add_tdc = $('#add_depth_cost');
    var tdc_content = $('#tb_depth_cost');

    var btn_add_cvd = $('#cvd_add');
    var cvd_content = $('#cvd_content');

    var btn_add_dh = $('#dh_add_depth');
    var dh_content = $('#form-dh');

    var btn_add_hcd = $('#hcd_add');
    var btn_delete_hcd = $('#hcd_add');

    var hcd_content = $('#hcd_content');

    var btn_add_bi = $('#bi_add_bulk');
    var bi_content = $('#bi_content');

    var $pageLoading = $('.page-loading-content');

    var isUnitOfMeasurementSI = null;

    var ObjectMapper = [];

    // Bulk Item Lookup
    var itemLookup = function (id) {
        var $element;
        if (id == null || id == "") {
            $element = $(".item-lookup");
        } else {
            $element = $("#" + id);
        }
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/item/lookup'),
            result: {
                id: 'id',
                text: 'item_name'
            },
            filters: function (params) {
                return [{
                    field: "item_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: formBI,
                placeholder: "Select an item",
                allowClear: true,
                //tags: true,
                //multiple: true,
                //maximumSelectionLength: 1,
            }
        });
    }

    // Start Event in every tabs
    btn_add_tdc.on('click', function (e) {
        var count = Number(tdc_content.find("tr").length);
        //// console.log("count = " + count);

        var elems = '<tr class="myRow">'
            + '<td class="pt-1 pb-1 align-middle">' + (count + 1) + '<input type="hidden" name="tvd_plan[' + count + '].no" data-field="tvd_plan[' + count + '].no" value="' + (count + 1) + '"/></td>'
            + '<td class="pt-1 pb-1"><input type="text" data-field="tvd_plan[' + count + '].activity" name="tvd_plan[' + count + '].activity" class="form-control" required /></td>';
        elems += `<td class="pt-1 pb-1"><input type="text" data-field="tvd_plan[` + count + `].depth" name="tvd_plan[` + count + `].depth" class="form-control numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" required /></td>`;
        elems += `<td class="pt-1 pb-1"><input type="text" data-field="tvd_plan[` + count + `].hour" name="tvd_plan[` + count + `].hour" class="form-control calculate-hour numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" required /></td>`;
        elems += `<td class="pt-1 pb-1 align-middle"><input class="form-control-plaintext numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" name="tvd_plan[` + count + `].days" data-field="tvd_plan[` + count + `].days" type="text" readonly required /></td>`;
        elems += `<td class="pt-1 pb-1"><input type="text" data-field="tvd_plan[` + count + `].cost" name="tvd_plan[` + count + `].cost" class="form-control calculate-cost numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" required /></td>`;
        elems += `<td  class="pt-1 pb-1 align-middle"><input class="form-control-plaintext numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" type="text"  name="tvd_plan[` + count + `].cumm_cost" data-field="tvd_plan[` + count + `].cumm_cost" required readonly /></td>`;
        elems += '<td class="pt-1 pb-1 action text-center" style="vertical-align: middle;">'
            + '<button href="javascript:void(0);" type="button" class="row-delete btn btn-danger btn-sm btn-icon waves-effect waves-themed rounded-0" style="background-color: red; border-color: maroon;"><i class="fal fa-trash-alt"></i></button>'
            + '</td>'
            + '</tr >';
        //// console.log(elems);
        tdc_content.append(elems);

        $('.row-delete').on('click', function () {
            recalculateTVD();
            $(this).closest('.myRow').remove();
        });

        $(".calculate-hour").on('input change keyup', function () {
            recalculateTVD();
        });
        $(".calculate-cost").on('input change keyup', function () {
            recalculateTVD();
        });

        maskingMoney();
    });

    btn_add_cvd.on('click', function (e) {
        var count = cvd_content.find(".form-group").length;
        var elems = '<div class="form-group" >'
            + '<div class="row">'
            + '<div class="col-md-2">'
            + 'Day : ' + (Number(count) + 1) + '<input name="cvd_plan[' + count + '].no" value="' + (Number(count) + 1) + '" type="hidden" />'
            + '</div>'
            + '<div class="col-md-5">'
            + '<input class="form-control" name="cvd_plan[' + count + '].depth" placeholder="Depth (ft)" />'
            + '</div>'
            + '<div class="col-md-5">'
            + '<input class="form-control" name="cvd_plan[' + count + '].cost" placeholder="Cost ($)" />'
            + '</div></div></div > ';
        cvd_content.append(elems);
    });

    btn_add_dh.on('click', function (e) {
        var count = dh_content.find(".form-group").length;
        var elems = '<div class="form-group mb-2" >'
            + '<div class="row">'
            + '<div class="col-md-4">'
            + '<input name="drilling_hazard[' + count + '].id" type="hidden" />'
            + '<input class="form-control" name="drilling_hazard[' + count + '].depth" placeholder="Depth" required />'
            + '</div>'
            + '<div class="col-md-7">'
            + '<input class="form-control" name="drilling_hazard[' + count + '].description" placeholder="Description" required />'
            + '</div>'
            + '<div class="col-md-1">'
            + '<button type="button" class="btn btn-warning btn-remove-dh fw-700">Remove</button>'
            + '</div>'
            + '</div>'
            + '</div >';
        dh_content.append(elems);

        removeDrillingHazard();
    });

    var removeDrillingHazard = function () {
        $(".btn-remove-dh").on('click', function () {
            $(this).closest('.form-group').remove();
        });
    }

    var removeBulkItem = function () {
        $(".btn-remove-bi").on('click', function () {
            $(this).closest('.form-group').remove();
        });
    }

    var lastRowPlanInformation = 0;
    var lastRowHoleAndCasing = 1;

    var stoneLookup = function (id) {
        var $element;
        if (id == null || id == "") {
            $element = $(".lookup-stone");
        } else {
            $element = $("#" + id);
        }
        $element.cmSelect2({
            url: $.helper.resolveApi('~/core/Stone/lookup'),
            result: {
                id: 'id',
                text: 'name'
            },
            filters: function (params) {
                return [{
                    field: "name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: formPI,
                placeholder: "Select a Stone",
                allowClear: true,
                //tags: true,
                //multiple: true,
                maximumSelectionLength: 1,
            }
        });
    }

    $("#clonePlanInformation").on('click', function () {
        var templateScript = $("#rowPlanInformation-template").html();
        var template = Handlebars.compile(templateScript);

        $('#tablePlanInformation > tbody tr.no-data').remove();
        $("#tablePlanInformation > tbody").append(template);

        $('#tablePlanInformation>tbody tr:last > .lookup_stone > select').attr("id", "stone_id-" + lastRowPlanInformation);
        $('#tablePlanInformation>tbody tr:last > .seq > input').val(lastRowPlanInformation + 1);

        stoneLookup("stone_id-" + lastRowPlanInformation);
        lastRowPlanInformation++;

        maskingMoney();
        $('.row-delete').on('click', function () {
            // console.log($(this));
            $(this).closest('.myRow').remove();
        });
    });

    btn_add_hcd.on('click', function (e) {
        var templateScript = $("#rowHoleAndCasing-template").html();
        var template = Handlebars.compile(templateScript);

        $("#form-hcd > .panel-body").append(template({ rowIndex: $("#form-hcd > .panel-body > .rowHole").length, well_id: $recordId.val() }));

        //$('#form-hcd > .panel-body > div.rowHole:last-child > div.hole-number > label > strong').html("Hole " + lastRowHoleAndCasing);
        //$('#form-hcd > .panel-body > div.rowHole:last-child > div.casing-number > label > strong').html("Casing " + lastRowHoleAndCasing);
        $('#form-hcd > .panel-body > div.rowHole:last-child > div.casing-type > div > select').attr("id", "holeAndCasing-" + lastRowHoleAndCasing);

        holeAndCasingLookup(lastRowHoleAndCasing);

        $('#form-hcd > .panel-body .rowHole').find('input.uom-definition').each(function () {
            var $obj = $(this);
            var objectName = $obj.data("objectname"); // this is the jquery object of the input, do what you will
            // console.log(objectName);
            var elementId = $obj[0].id;
            if (objectName != undefined) {
                $.each(ObjectMapper, function (key, value) {
                    if (objectName.toLowerCase().trim() == value.object_name.toLowerCase().trim()) {
                        $obj.val(value.uom_code);
                    }
                });
            }
        });

        maskingMoney();
        lastRowHoleAndCasing++;
    });

    btn_add_bi.on('click', function (e) {
        var count = Number(bi_content.find(".form-group").length) + 1;
        var elems = `
                    <div class="form-group mb-2">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="item_name">Item Name</label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" type="hidden" name="wellItemId[]">
                                <select type="text" class="form-control" name="item_id[]" id="well_item-`+ count + `" data-field="item_name" data-text="item_name" placeholder="" required></select>
                                <div class="invalid-feedback">Personel Job cannot be empty</div>
                            </div>
                            <div class="col-md-2">
                               <button type="button" class="btn btn-warning btn-remove-bi fw-700"> <i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                    </div>
                    `;

        bi_content.append(elems);

        itemLookup("well_item-" + count);
        removeBulkItem();

    });

    var FieldLookup = function () {
        $('#field-lookup').click(function () {
            var element = $(this);
            element.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/Asset/LookupField',
                success: function (data) {
                    element.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "Choose a Field <small>Double click an item below to selected as a field</small>",
                        callback: function (e) {
                            // console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o, event) {

                        if (event.node.parent == '#')
                            alert('Please double click field only');
                        else {
                            $box.modal('hide');
                        }
                        $("#field_id").val(event.node.id);
                        element.html(": " + event.node.text);
                    });
                }
            });
        });
    }

    var maskingMoney = function () {
        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });
    }

    var getCountry = function () {
        $.ajax({
            url: $.helper.resolveApi('~/core/Country/all'),
            type: "GET",
            dataType: "json",
            success: function (data) {
                console.log("Data from URL:", data);
            },
            error: function (xhr, status, error) {
                console.error("Error fetching data:", error);
            }
        });

    }

    var lookUp = function () {
        getCountry();
        $("#country_id").cmSelect2({
            url: $.helper.resolveApi('~/core/Country/lookup'),
            result: {
                id: 'id',
                text: 'country_name'
            },
            filters: function (params) {
                return [{
                    field: "country_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select a Country",
                allowClear: true,
            }
        });

        //$(".list-stone").cmSelect2({
        //    url: $.helper.resolveApi('~/core/Stone/lookup'),
        //    result: {
        //        id: 'id',
        //        text: 'name'
        //    },
        //    filters: function (params) {
        //        return [{
        //            field: "name",
        //            operator: "contains",
        //            value: params.term || '',
        //        }];
        //    },
        //    options: {
        //        placeholder: "Select a Stone",
        //        allowClear: true,
        //    }
        //});

        $("#well_type").val('');
        $("#well_type").select2({
            placeholder: "Select a Well Type",
            allowClear: true
        });

        $(".uom_id").cmSelect2({
            url: $.helper.resolveApi('~/core/uom/lookup'),
            result: {
                id: 'id',
                text: 'uom_code'
            },
            filters: function (params) {
                return [{
                    field: "uom_code",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select a Unit of Measurement",
                allowClear: true,
                maximumSelectionLength: 1,
            }
        });
        $("#drilling_contractor_id").cmSelect2({
            url: $.helper.resolveApi('~/core/drillingcontractor/lookup'),
            result: {
                id: 'id',
                text: 'contractor_name'
            },
            filters: function (params) {
                return [{
                    field: "contractor_name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select a Contractor",
                allowClear: true,
                //maximumSelectionLength: 1,
            }
        });

        $("#well_classification").val('');
        $("#well_classification").select2({
            placeholder: "Select a Well Classification",
            allowClear: true
        });

        $("#detail_well_classification").val('');
        $("#detail_well_classification").select2({
            placeholder: "Select a Detail Well Classification",
            allowClear: true
        });


        $("#rig_id").cmSelect2({
            url: $.helper.resolveApi('~/core/rig/lookup'),
            result: {
                id: 'id',
                text: 'name'
            },
            filters: function (params) {
                return [{
                    field: "name",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                placeholder: "Select a Rig",
                allowClear: true,
                maximumSelectionLength: 2,
            }
        });
        $("#rig_id").on('select2:select', function (e) {
            var id = e.target.value;
            // console.log(e);
        });
        $("#well-onshore").click(function () {
            $('input:radio[name=environment]:nth(0)').attr('checked', true);
            $("#form-group-water-depth").hide();
            $("#form-group-rt_to_seabed").hide();
            $("#rt_to_seabed").removeAttr("required");
            $("#water_depth").removeAttr("required");
        });
        $("#well-offshore").click(function () {
            $('input:radio[name=environment]:nth(0)').attr('checked', true);
            $("#form-group-water-depth").show();
            $("#form-group-rt_to_seabed").show();
            $("#rt_to_seabed").attr("required", "");
            $("#water_depth").attr("required", "");
        });
    }

    var arrayUomObject = new Object;

    var recalculateTVD = function () {
        if (tdc_content.children().length > 0) {
            var i = 0;
            var cummDays = 0;
            var cummCost = 0;

            tdc_content.children().each(function () {
                var currentHour = $(this).find("input[name*='hour']").val().replace(/,/g, '');
                var currentCost = $(this).find("input[name*='cost']").val().replace(/,/g, '');

                // // console.log("currentHour: " + currentHour);
                if (i == 0) {
                    if (currentHour == 0) {
                        cummDays = parseFloat(0);
                    } else {
                        cummDays = parseFloat(currentHour / 24);
                    }
                    cummCost = currentCost;
                } else {
                    if (currentHour == 0) {
                        cummDays = parseFloat(cummDays) + 0;
                    } else {
                        cummDays = parseFloat(cummDays) + (parseFloat(currentHour) / 24);
                    }
                    cummCost = parseFloat(cummCost) + parseFloat(currentCost);
                }

                //// console.log("Cum Days: " + cummDays);
                //// console.log("Cum Cost: " + cummCost);
                if (!isNaN(cummDays)) {
                    $(this).find("input[name*='days']").val(parseFloat(cummDays).toFixed(2));
                } else {
                    $(this).find("input[name*='days']").val("");
                }

                if (!isNaN(cummCost)) {
                    $(this).find("input[name*='cumm_cost']").val(parseFloat(cummCost).toFixed(2));
                } else {
                    $(this).find("input[name*='cumm_cost']").val("");
                }
                i++;
            });
        }
    }

    var pageFunction = function () {
        $('.date-picker').datepicker({
            autoclose: true,
            todayBtn: "linked",
            format: 'mm/dd/yyyy',
            orientation: "bottom left",
            todayHighlight: true,
            templates: controls
        });

        $(".numeric").inputmask({
            digits: 0,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });
        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });

        $(".well-uom-standart").on('change', function () {
            if ($('input:radio[name=metric_unit][value=true]:checked').is(':checked')) {
                isUnitOfMeasurementSI = true;
                $("#well-uom-mapping").hide();
                SetObjectUomMap("Standard International");
            }
            else if ($('input:radio[name=metric_unit][value=false]:checked').is(':checked')) {
                isUnitOfMeasurementSI = false;
                SetObjectUomMap("API");
                $("#well-uom-mapping").hide();
            }
            else {
                isUnitOfMeasurementSI = null;
                SetObjectUomMap("Mixed");
                $("#well-uom-mapping").show();
            }
            GetWellObjectUomMap();
        });

        $(document).on('hidden.bs.modal', "#objectUomMapModal", function () {
            GetWellObjectUomMap();
        });
        $(document).on('hidden.bs.modal', "#holeCasingModal", function () {
            loadHoleAndCasingByWellId($recordId.val());
            loadHoleAndCasingByWellIdNew($recordId.val());
        });

        var SetObjectUomMap = function (to) {
            Swal.fire(
                {
                    title: "Are you sure unit of measurement want to change to " + to + " unit ?",
                    text: "This action cannot undo, please make sure all current unit configuration want to set " + to + " default",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes"
                }).then(function (result) {
                    if (result.value) {
                        $pageLoading.loading('start');
                        $.post($.helper.resolveApi('~/core/well/objectUomMapper/change'),
                            {
                                id: $recordId.val(),
                                is_std_international: $('input[name="metric_unit"]:checked').val() == "" ? null : $('input[name="metric_unit"]:checked').val()
                            }, function (r) {
                                $pageLoading.loading('stop');
                                if (r.status.success) {
                                    toastr["success"]("Unit of Measurement has been changed");
                                    GetWellObjectUomMap();
                                } else {
                                    toastr.error(r.status.message)
                                }
                            }, 'json').fail(function (r) {
                                toastr.error(r.statusText);
                            });
                    }
                });
        }

        //var GetSIObjectUomMap = function () {
        //    $.get($.helper.resolveApi('~/core/ObjectUomMap/GetViewAll'), function (r) {
        //        if (r.status.success) {
        //            if (r.data.length > 0) {
        //                arrayUomObject = r.data;
        //                GetStandartInternationalUom();
        //            }
        //        }
        //        $('.loading-detail').hide();
        //    }).fail(function (r) {
        //        //// console.log(r);
        //    }).done(function () {

        //    });
        //}


        var GetWellObjectUomMap = function () {

            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + $recordId.val()), function (r) {
                if (r.status.success) {

                    if (r.data.length > 0) {
                        ObjectMapper = r.data;
                        // console.log(ObjectMapper);
                        //// console.log("tes", ObjectMapper);
                        $("form#form-gwi :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                                if (uom) {
                                    $("#" + elementId).val(uom.uom_code);
                                }
                                //$.each(r.data, function (key, value) {
                                //    if (objectName.toLowerCase().trim() == (value.object_name||'').toLowerCase().trim()) {
                                //        $("#" + elementId).val(value.uom_code);
                                //    }
                                //});
                            }
                        });
                        $("form#form-tdc :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                //$.each(r.data, function (key, value) {
                                //    if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                //        $("#" + elementId).val("(" + value.uom_code + ")");
                                //    }
                                //});
                                var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                                if (uom) {
                                    $("#" + elementId).val(uom.uom_code);
                                }
                            }
                        });
                        $("form#DH-header :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                //$.each(r.data, function (key, value) {
                                //    if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                //        $("#" + elementId).val("(" + value.uom_code + ")");
                                //    }
                                //});
                                var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                                if (uom) {
                                    $("#" + elementId).val(uom.uom_code);
                                }
                            }
                        });

                        $("form#form-pi :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                //$.each(r.data, function (key, value) {
                                //    if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                //        $("#" + elementId).val("(" + value.uom_code + ")");
                                //    }
                                //});
                                var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                                if (uom) {
                                    $("#" + elementId).val(uom.uom_code);
                                }
                            }
                        });

                        $("form#formWellDeviation :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                //$.each(r.data, function (key, value) {
                                //    if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                //        $("#" + elementId).val("(" + value.uom_code + ")");
                                //    }
                                //});
                                var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                                if (uom) {
                                    $("#" + elementId).val(uom.uom_code);
                                }
                            }
                        });

                        $("form#form-hcd > .panel-body :input.uom-definition").each(function () {
                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementid = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                //$.each(r.data, function (key, value) {
                                //    if (objectname.tolowercase().trim() == (value.object_name || '').tolowercase().trim()) {
                                //        $("#" + elementid).val("" + value.uom_code + "");
                                //    }
                                //});
                                var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                                if (uom) {
                                    $("#" + elementid).val(uom.uom_code);
                                }
                            }
                        });

                        $("form#form-rd :input").each(function () {

                            var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                            var elementId = $(this)[0].id;
                            if (objectName != undefined || objectName == '') {
                                //$.each(r.data, function (key, value) {
                                //    if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                //        $("#" + elementId).val(value.uom_code);
                                //    }
                                //});
                                var uom = ObjectMapper.find(x => x.object_name.toLowerCase().trim() == objectName.toLowerCase().trim())
                                if (uom) {
                                    $("#" + elementId).val(uom.uom_code);
                                }
                            }
                        });



                        //

                    }
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        //var GetStandartInternationalUom = function () {
        //    $.get($.helper.resolveApi('~/core/uom/GetStandartInternational'), function (r) {
        //        if (r.status.success) {
        //            $("form#form-gwi :input.uom-definition").each(function () {
        //                var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
        //                var elementId = $(this)[0].id;

        //                $.each(arrayUomObject, function (key, value) {
        //                    if (objectName.toLowerCase() == value.object_name.toLowerCase()) {
        //                        $.each(r.data, function (key, uom_value) {
        //                            if (uom_value.id == value.uom_id) {
        //                                //$el.id;
        //                                $("#" + elementId).val(value.uom_code);
        //                            }
        //                        });
        //                    }
        //                });
        //            });
        //            toastr["success"]("Unit of Measurement has been changed to SI");
        //        }
        //        $('.loading-detail').hide();
        //    }).fail(function (r) {
        //        //// console.log(r);
        //    }).done(function () {

        //    });
        //}

        var getItemByWellId = function (recordId) {
            $.get($.helper.resolveApi('~/core/wellitem/' + recordId + '/getByWellId'), function (r) {
                if (r.status.success) {
                    //console.log("Data received from server: ", r.data);  // Debugging log

                    var data = r.data;
                    //console.log("Sorted data: ", data);  // Debugging log

                    var count = data.length;

                    for (var i = 0; i < count; i++) {

                        console.log("juju", data[i].item_name)
                        var elems = `
                            <div class="form-group mb-2 item-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="item_name">Item Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control" type="hidden" name="wellItemId[]" value="` + data[i].id + `">
                                        <select class="form-control" name="item_id[]" id="well_item-`+ i + `" data-field="item_name" data-text="item_name" placeholder="" required>
                                            <option value="` + data[i].item_id + `">` + data[i].item_name + `</option>
                                        </select>
                                        <div class="invalid-feedback">Personel Job cannot be empty</div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-warning btn-remove-bi fw-700"> 
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            `;

                        $('#bi_content').append(elems);  // Pastikan id atau selector untuk append benar

                    }

                    itemLookup("");
                    removeBulkItem();

                    // Event listener untuk tombol delete
                    $('#bi_content').on('click', '.btn-remove-bi', function () {
                        var itemRow = $(this).closest('.item-row');
                        var itemId = itemRow.find('input[name="wellItemId[]"]').val();

                        // Konfirmasi penghapusan
                        if (confirm('Are you sure you want to delete this item?')) {
                            // Kirim permintaan penghapusan ke server
                            $.post($.helper.resolveApi('~/core/wellitem/delete'), { id: itemId }, function (r) {
                                if (r.status.success) {
                                    toastr.success(r.status.message);
                                    itemRow.remove();
                                } else {
                                    toastr.error(r.status.message);
                                }
                            }, 'json').fail(function (r) {
                                toastr.error(r.statusText);
                            });
                        }
                    });

                    $('.loading-detail').hide();
                } else {
                    console.log("Failed to receive data: ", r.status.message);  // Debugging log
                }
            }).fail(function (r) {
                console.log("Error in request: ", r);  // Debugging log
            }).done(function () {
                // Any additional functionality after completion
            });


        }
        var loadTimeVDepth = function () {
            $.get($.helper.resolveApi('~/core/TVDPlan/' + $recordId.val() + '/detailbywellid'), function (r) {
                if (r.status.success) {

                    var data = r.data.sort((function (index) {
                        return function (a, b) {
                            return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
                        };
                    }));
                    var count = data.length;
                    for (var i = 0; i < count; i++) {
                        // console.log(data[i].activity);
                        if (i == 0) {
                            $("input[name^='tvd_plan[" + i + "].id']").val(data[i].id);
                            $("input[name^='tvd_plan[" + i + "].no']").val(i + 1);
                            $("input[name^='tvd_plan[" + i + "].activity']").val(data[i].activity);
                            $("input[name^='tvd_plan[" + i + "].depth']").val(data[i].depth);
                            $("input[name^='tvd_plan[" + i + "].hour']").val(data[i].hour);
                            $("input[name^='tvd_plan[" + i + "].days']").val(parseFloat(data[i].days).toFixed(2));
                            $("input[name^='tvd_plan[" + i + "].cost']").val(data[i].cost);
                            $("input[name^='tvd_plan[" + i + "].cumm_cost']").val(parseFloat(data[i].cumm_cost).toFixed(2));
                        } else {
                            var dataAct = data[i].activity;
                            var elems = '<tr class="myRow">'
                                + '<td class="pt-1 pb-1 align-middle">' + (i + 1) + '<input type="hidden" name="tvd_plan[' + i + '].no" data-field="tvd_plan[' + i + '].no" value="' + data[i].no + '"/>'
                                + '<input type="hidden" name="tvd_plan[' + i + '].id" data-field="tvd_plan[' + i + '].id" value="' + data[i].id + '"/></td>'
                                + '<td class="pt-1 pb-1"><input type="text" data-field="tvd_plan[' + i + '].activity" name="tvd_plan[' + i + '].activity" class="form-control data-activity" value="" required /></td>';
                            elems += `<td class="pt-1 pb-1"><input type="text" data-field="tvd_plan[` + i + `].depth" name="tvd_plan[` + i + `].depth" class="form-control numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" value="` + data[i].depth + `" required /></td>`;
                            elems += `<td class="pt-1 pb-1"><input type="text" data-field="tvd_plan[` + i + `].hour" name="tvd_plan[` + i + `].hour" class="form-control calculate-hour numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" value="` + data[i].hour + `" required /></td>`;
                            elems += `<td class="pt-1 pb-1 align-middle"><input class="form-control-plaintext numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" name="tvd_plan[` + i + `].days" data-field="tvd_plan[` + i + `].days" type="text" value="` + parseFloat(data[i].days).toFixed(2) + `" readonly required /></td>`;
                            elems += `<td class="pt-1 pb-1"><input type="text" data-field="tvd_plan[` + i + `].cost" name="tvd_plan[` + i + `].cost" class="form-control calculate-cost numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" value="` + data[i].cost + `" required /></td>`;
                            elems += `<td class="pt-1 pb-1 align-middle"><input class="form-control-plaintext numeric-money" data-inputmask="'alias': 'currency'" im-insert="true" inputmode="numeric" type="text" readonly name="tvd_plan[` + i + `].cumm_cost" data-field="tvd_plan[` + i + `].cumm_cost" value="` + parseFloat(data[i].cumm_cost).toFixed(2) + `" required /></td>`;
                            elems += '<td class="pt-1 pb-1 action text-center" style="vertical-align: middle;">'
                                + '<button href="javascript:void(0);" type="button" class="row-delete btn btn-danger btn-sm btn-icon waves-effect waves-themed rounded-0" style="background-color: red; border-color: maroon;"><i class="fal fa-trash-alt"></i></button>'
                                + '</td>'
                                + '</tr >';
                            var $elem = $(elems);
                            $elem.find('.data-activity').val(dataAct);
                            tdc_content.append($elem);
                        }
                    }

                    maskingMoney();

                    $('.row-delete').on('click', function () {
                        $(this).closest('.myRow').remove();
                        recalculateTVD();
                    });

                    $(".calculate-hour").on('input change keyup', function () {
                        recalculateTVD();
                    });
                    $(".calculate-cost").on('input change keyup', function () {
                        recalculateTVD();
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                // console.log(r);
            });
        }

        var loadWellDeviation = function (wellId) {
            var templateScript = $("#readDrillingDeviation-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/WellDeviation/' + wellId + '/detailByWellId'), function (r) {
                if (r.status.success && r.data.length > 0) {
                    //$("#wellDeviationDummy").hide();
                    $("#tableWellDeviation > tbody").html(template({ data: r.data }));
                }
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        var loadHoleAndCasingByWellId = function (wellId) {
            var templateScript = $("#readHoleAndCasing-template").html();
            var template = Handlebars.compile(templateScript);

            //$.get($.helper.resolveApi('~/core/wellholeandcasing/' + wellId + '/getAllHoleCasingByWellId'), function (r) {
            $.get($.helper.resolveApi('~/core/wellholeandcasing/' + wellId + '/getAllByWellId'), function (r) {
                console.log("ini dataku", r.data)
                if (r.status.success && r.data.length > 0) {
                    $("#form-hcd > .panel-body").html(template({ data: r.data }));

                    maskingMoney();
                }
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });

        }

        var loadHoleAndCasingByWellIdNew = function (wellId) {
            var templateScript = $("#readHoleAndCasingNew-template").html();
            var template = Handlebars.compile(templateScript);

            $.get($.helper.resolveApi('~/core/wellholeandcasing/' + wellId + '/getAllByWellIdnew'), function (r) {
                if (r.status.success && r.data.length > 0) {
                    $("#form-hcd-new > .panel-body").html(template({ data: r.data }));

                    maskingMoney();
                    console.log("ini data", r.data)
                }
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        $(document).on('click', 'form#form-hcd > .panel-body .btn-hcd-delete', function () {
            var recordIdHCD = $(this).data("record_id");
            var b = bootbox.confirm({
                message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this! " + recordIdHCD + " </p>",
                buttons: {
                    confirm: {
                        label: "Delete"
                    }
                },
                callback: function (result) {
                    if (result) {
                        var btnConfim = $(this).find('.bootbox-accept');
                        btnConfim.button('loading');
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            url: $.helper.resolveApi("~/core/wellholeandcasing/delete"),
                            data: JSON.stringify([recordIdHCD]),
                            success: function (r) {
                                if (r.status.success) {
                                    toastr.error("Data has been deleted");
                                    loadHoleAndCasingByWellId($recordId.val());
                                } else {
                                    toastr.error(r.status.message);
                                }
                                btnConfim.button('reset');
                                b.modal('hide');
                                $dtBasic.DataTable().ajax.reload();
                            },
                            error: function (r) {
                                toastr.error(r.statusText);
                                b.modal('hide');
                            }
                        });
                        return false;
                    }
                },
                animateIn: 'bounceIn',
                animateOut: 'bounceOut'
            });
        });

        $(document).on('click', 'form#form-hcd-new > .panel-body .btn-hcd-delete', function () {
            var recordIdHCD = $(this).data("record_id");
            var b = bootbox.confirm({
                message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this! </p>",
                buttons: {
                    confirm: {
                        label: "Delete"
                    }
                },
                callback: function (result) {
                    if (result) {
                        var btnConfim = $(this).find('.bootbox-accept');
                        btnConfim.button('loading');
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            url: $.helper.resolveApi("~/core/wellholeandcasing/deletenew"),
                            data: JSON.stringify(recordIdHCD),
                            success: function (r) {
                                if (r.status.success) {
                                    $("#form-hcd-new > .panel-body").empty();
                                    toastr.error("Data has been deleted");
                                    loadHoleAndCasingByWellIdNew($recordId.val());
                                } else {
                                    toastr.error(r.status.message);
                                }
                                btnConfim.button('reset');
                                b.modal('hide');
                                //$dtBasic.DataTable().ajax.reload();
                            },
                            error: function (r) {
                                toastr.error(r.statusText);
                                b.modal('hide');
                            }
                        });
                        return false;
                    }
                },
                animateIn: 'bounceIn',
                animateOut: 'bounceOut'
            });
        });


        var loadPlanInformation = function (recordId) {
            var templateScript = $("#readPlanInformation-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/DrillFormationsPlan/' + recordId + '/detailbywellid'), function (r) {
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#tablePlanInformation > tbody").html(template({ data: r.data }));

                        lastRowPlanInformation = r.data[r.data.length - 1].seq;
                        stoneLookup("");
                        $('.row-delete').on('click', function () {
                            $(this).closest('.myRow').remove();
                        });
                    }

                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //// console.log(r);
            }).done(function () {

            });
        }

        var loadDetail = function () {
            itemLookup("");
            if ($recordId.val() !== '') {
                //debugger;
                loadWellDeviation($recordId.val());
                getItemByWellId($recordId.val());
                loadTimeVDepth();
                loadPlanInformation($recordId.val());
                loadHoleAndCasingByWellId($recordId.val());
                loadHoleAndCasingByWellIdNew($recordId.val())

                var url = $.helper.resolveApi('~/core/Stone/lookup');
                $.get($.helper.resolveApi('~/core/Well/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        // console.log('rrr');
                        // console.log(r.data);
                        $.helper.form.fill(formGWI, r.data);
                        //console.log(r.data.well_status);

                        if (r.data.planned_days != null)
                            $("#planned_days").val(r.data.planned_days);
                        if (r.data.planned_td != null)
                            $("#planned_td").val(r.data.planned_td);
                        $.helper.form.fill(formRD, r.data);
                        isUnitOfMeasurementSI = r.data.is_std_international;

                        if (r.data.environment == "onshore") {
                            $("#form-group-water-depth").hide();
                            $("#form-group-rt_to_seabed").hide();
                            $("#rt_to_seabed").removeAttr("required");
                            $("#water_depth").removeAttr("required");
                        } else {
                            $('input:radio[name=environment]:nth(0)').attr('checked', true);
                            $("#form-group-water-depth").show();
                            $("#form-group-rt_to_seabed").show();
                            $("#rt_to_seabed").attr("required");
                            $("#water_depth").attr("required");
                        };


                        //if (r.data.submitted_by == null)
                        //    $("#btn-submit").show();
                        //else {
                        //    $("#btn-submit").hide();
                        //    $(".btn-action").hide();
                        //    //if (r.data.closed_by == null)
                        //    //    $("#btn-closed").show();
                        //    //else
                        //    //    $("#btn-closed").hide();
                        //}



                        /*BEGIN::Unit of measurement*/
                        GetWellObjectUomMap();
                        if (isUnitOfMeasurementSI == null) {
                            $("#well-uom-mapping").show();
                        }
                        $('input[name=metric_unit][value=' + r.data.is_std_international + ']').prop('checked', true);
                        /*END::Unit of measurement*/


                        $('input[name=environment]').removeAttr("checked");
                        if (r.data.environment == "onshore")
                            $("#well-onshore").prop("checked", true)
                        else
                            $("#well-offshore").prop("checked", true);

                        if (r.data.rig_id == null) {
                            $('#rig_id').val(null).trigger('change');
                        }

                        if (r.data.field_name != null)
                            $("#field-lookup").html(": " + r.data.field_name);

                        if (r.data.well_status === '0') {
                            $("#well_status").text("Original Well (Drilling)");
                        } else if (r.data.well_status === '1') {
                            $("#well_status").text("Side Track (Drilling)");
                        } else if (r.data.well_status === '2') {
                            $("#well_status").text("Work Over");
                        } else {
                            $("#well_status").text("Well Services");
                        }
                    }
                    else {
                        toastr.error(r.status.message);
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    // console.log(r);
                });

                $.get($.helper.resolveApi('~/core/CVDPlan/' + $recordId.val() + '/detailbywellid'), function (r) {
                    if (r.status.success) {
                        var data = r.data.sort((function (index) {
                            return function (a, b) {
                                return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
                            };
                        }));
                        var count = data.length;
                        for (var i = 0; i < count; i++) {
                            if (i == 0) {
                                $("input[name^='cvd_plan[" + i + "].id']").val(data[i].id);
                                $("input[name^='cvd_plan[" + i + "].no']").val(data[i].no);
                                $("input[name^='cvd_plan[" + i + "].cost']").val(data[i].cost);
                                $("input[name^='cvd_plan[" + i + "].cost']").val(data[i].cost);
                                $("input[name^='cvd_plan[" + i + "].depth']").val(data[i].depth);
                            } else {
                                var elems = '<div class="form-group" >'
                                    + '<div class="row">'
                                    + '<div class="col-md-2">'
                                    + 'Day : ' + (i + 1) + '<input name="cvd_plan[' + i + '].no" value="' + data[i].no + '" type="hidden" />'
                                    + '<input name="cvd_plan[' + i + '].id" value="' + data[i].id + '" type="hidden" /></div>'
                                    + '<div class="col-md-5">'
                                    + '<input class="form-control" name="cvd_plan[' + i + '].depth" placeholder="Depth (ft)" value="' + data[i].depth + '" />'
                                    + '</div>'
                                    + '<div class="col-md-5">'
                                    + '<input class="form-control" name="cvd_plan[' + i + '].cost" placeholder="Cost ($)" value="' + data[i].cost + '" />'
                                    + '</div></div></div > ';
                                cvd_content.append(elems);
                            }
                        }

                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    // console.log(r);
                });

                $.get($.helper.resolveApi('~/core/DrillingHazard/' + $recordId.val() + '/detailbywellid'), function (r) {
                    // console.log('r');
                    // console.log(r);
                    if (r.status.success) {
                        var data = r.data.sort((function (index) {
                            return function (a, b) {
                                return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
                            };
                        }));
                        var count = data.length;

                        for (var i = 0; i < count; i++) {
                            if (i == 0) {
                                $("input[name^='drilling_hazard[" + i + "].id']").val(data[i].id);
                                $("input[name^='drilling_hazard[" + i + "].depth']").val(data[i].depth);
                                $("input[name^='drilling_hazard[" + i + "].description']").val(data[i].description);
                            } else {
                                var elems = '<div class="form-group mb-2" >'
                                    + '<div class="row">'
                                    + '<div class="col-md-4">'
                                    + '<input name="drilling_hazard[' + i + '].id" type="hidden" value="' + data[i].id + '" />'
                                    + '<input class="form-control" name="drilling_hazard[' + i + '].depth" placeholder="Depth (ft)" value="' + data[i].depth + '" required />'
                                    + '</div>'
                                    + '<div class="col-md-7">'
                                    + '<input class="form-control" name="drilling_hazard[' + i + '].description" placeholder="Description" value="' + data[i].description + '" required />'
                                    + '</div>'
                                    + '<div class="col-md-1">'
                                    + '<button type="button" class="btn btn-warning btn-remove-dh fw-700">Remove</button>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div >';
                                dh_content.append(elems);
                            }
                        }
                        removeDrillingHazard();
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    // console.log(r);
                });

                $.get($.helper.resolveApi('~/core/WellProfileProgram/' + $recordId.val() + '/detailbywellid'), function (r) {
                    if (r.status.success) {
                        var data = r.data.sort((function (index) {
                            return function (a, b) {
                                return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
                            };
                        }));
                        var count = data.length;
                        for (var i = 0; i < count; i++) {
                            if (i == 0) {
                                $("input[name^='well_profile_program[" + i + "].id']").val(data[i].id);
                                $("input[name^='well_profile_program[" + i + "].hole_diameter']").val(data[i].hole_diameter);
                                $("input[name^='well_profile_program[" + i + "].hole_depth']").val(data[i].hole_depth);
                                $("input[name^='well_profile_program[" + i + "].casing_diameter']").val(data[i].casing_diameter);
                                $("input[name^='well_profile_program[" + i + "].casing_depth']").val(data[i].casing_depth);
                                $("input[name^='well_profile_program[" + i + "].casing_type_id']").val(data[i].casing_type_id);
                                $("input[name^='well_profile_program[" + i + "].weight']").val(data[i].weight);
                                $("input[name^='well_profile_program[" + i + "].grade']").val(data[i].grade);
                                $("input[name^='well_profile_program[" + i + "].connection']").val(data[i].connection);
                                $("input[name^='well_profile_program[" + i + "].top_of_liner']").val(data[i].top_of_liner);
                            } else {
                                var elems = '<div class="hcd_container" >'
                                    + '--- Hole ---'
                                    + '<hr />'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="hole_diameter">Hole Diameter</label>'
                                    + '</div>'
                                    + '<div class="col-md-8"><input type="hidden" name="well_profile_program[' + i + '].id" />'
                                    + '<input type="text" class="form-control" name="well_profile_program[' + i + '].hole_diameter" placeholder="Diameter (inch)" />'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="hole_depth">Hole Depth</label> '
                                    + '</div>'
                                    + '<div class="col-md-8">'
                                    + '<input class="form-control" name="well_profile_program[' + i + '].hole_depth" placeholder="Depth (ft)" />'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '--- Casing-- -'
                                    + '<hr />'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="casing_diameter">Casing Diameter</label>'
                                    + '</div>'
                                    + '<div class="col-md-8">'
                                    + '<input class="form-control" name="well_profile_program[' + i + '].casing_diameter" placeholder="Diameter (inch)" />'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="casing_setting_depth">Casing Setting Depth</label > '
                                    + '</div>'
                                    + '<div class="col-md-8">'
                                    + '<input class="form-control" name="well_profile_program[' + i + '].casing_depth" placeholder="Setting Depth (ft)" />'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="casing_type">Casing Type</label>'
                                    + '</div>'
                                    + '<div class="col-md-8">'
                                    + '<select class="form-control" name="well_profile_program[' + i + '].casing_type_id" ></select>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="casing_weight">Casing Weight</label>'
                                    + '</div>'
                                    + '<div class="col-md-8">'
                                    + '<input class="form-control" name="well_profile_program[' + i + '].weight" placeholder="Casing Weight (ppf)" />'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="casing_grade">Casing Grade</label>'
                                    + '</div>'
                                    + '<div class="col-md-8">'
                                    + '<input class="form-control" name="well_profile_program[' + i + '].grade" placeholder="Casing Grade" />'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="casing_connection">Casing Connection</label>'
                                    + '</div>'
                                    + '<div class="col-md-8">'
                                    + '<input class="form-control" name="well_profile_program[' + i + '].connection" placeholder="Casing Connection" />'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '<div class="form-group">'
                                    + '<div class="row">'
                                    + '<div class="col-md-3">'
                                    + '<label for="casing_top_of_liner">Casing Top of Liner</label>'
                                    + '</div>'
                                    + '<div class="col-md-8">'
                                    + '<input class="form-control" name="well_profile_program[' + i + '].top_of_liner" placeholder="Casing Top of Liner" />'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>'
                                    + '</div>';
                                hcd_content.append(elems);
                            }
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    // console.log(r);
                });

                $.get($.helper.resolveApi('~/core/Well/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill(formGWI, r.data);
                        $.helper.form.fill(formRD, r.data);
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    // console.log(r);
                });
            }
        };

        function checkProperties(obj) {
            for (var key in obj) {
                if (obj[key] !== null && obj[key] != "")
                    return false;
            }
            return true;
        }

        //$("#btn-uom-mapping").on("click", function () {
        //    alert();
        //});

        $('#btn-save').click(function (event) {
            if (isUnitOfMeasurementSI == null) {
                toastr["error"]("Please choose Unit of Measurement.");
                return;
            }

            var isValid = true;
            $('.error-tab').hide();
            var $dt_Basic = $('#dt_well');
            var btn = $(this);

            var data = new Object();
            var RD = formRD.serializeToJSON();
            data.well = formGWI.serializeToJSON();
            data.well = $.extend(true, data.well, RD);
            data.well_item = new Object();
            data.drill_formations_plan = new Object();
            data.well_deviation = new Object();
            data.hole_and_casing = new Object();
            //data.well.id = $recordId.val();

            // console.log(formGWI);
            // console.log(data.well);

            var isValidateGWI = formGWI[0].checkValidity();
            var isValidateRigDataForm = formRD[0].checkValidity();
            var isValidatePlanInformation = formPI[0].checkValidity();
            var isValidateHoleAndCasing = formHCD[0].checkValidity();




            if (!isValidateGWI) {
                formGWI.addClass('was-validated');
                isValid = false;
                $(".error-gwi").show();
            }
            if (!isValidateHoleAndCasing) {
                formHCD.addClass('was-validated');
                isValid = false;
                $(".error-hcd").show();
            }
            if (!isValidateRigDataForm) {
                formRD.addClass('was-validated');
                isValid = false;
                $(".error-rd").show();
            }
            if (data.drill_formations_plan.length <= 0 || !isValidatePlanInformation) {
                $(".error-pi").show();
                isValid = false;
                toastr.error("Plan Information cannot be empty.");
            }
            if (data.well_item.length <= 0) {
                $(".error-bi").show();
                isValid = false;
            }
            if (data.well_deviation.length <= 0) {
                $(".error-wellDeviation").show();
                isValid = false;
            }




            if (isValid) {
                event.preventDefault();

                var holeCasings = [];
                var total = document.getElementsByName('hole_name[]').length;
                if (total <= 0) {
                    toastr.error("Please input hole and casing.");
                    $(".error-hcd").show();
                    return;
                }
                for (var i = 0; i < total; i++) {
                    var id = null;
                    var hole_name = document.getElementsByName('hole_name[]')[i].value;
                    var hole_depth = document.getElementsByName('hole_depth[]')[i].value;
                    var casing_name = document.getElementsByName('casing_name[]')[i].value;
                    var casing_setting_depth = document.getElementsByName('casing_setting_depth[]')[i].value;
                    var casing_type = document.getElementsByName('casing_type[]')[i].value;
                    var casing_weight = document.getElementsByName('casing_weight[]')[i].value;
                    var casing_grade = document.getElementsByName('casing_grade[]')[i].value;
                    var casing_connection = document.getElementsByName('casing_connection[]')[i].value;
                    var casing_top_of_liner = document.getElementsByName('casing_top_of_liner[]')[i].value;
                    holeCasings.push({
                        id: id,
                        hole_name: hole_name,
                        hole_depth: parseFloat(hole_depth.replace(",", "")),
                        casing_name: casing_name,
                        casing_setting_depth: parseFloat(casing_setting_depth.replace(",", "")),
                        casing_type: casing_type,
                        casing_weight: parseFloat(casing_weight.replace(",", "")),
                        casing_grade: casing_grade,
                        casing_connection: casing_connection,
                        casing_top_of_liner: casing_top_of_liner,
                    });

                }
                data.hole_and_casing = holeCasings;
                //--  Well Item
                var items = [];
                var total = document.getElementsByName('wellItemId[]').length;
                for (var i = 0; i < total; i++) {
                    items.push({
                        id: document.getElementsByName('wellItemId[]')[i].value,
                        item_id: document.getElementsByName('item_id[]')[i].value,
                    });
                }
                data.well_item = items;

                //-- Plan Information
                var plan_informations = [];
                var total = document.getElementsByName('wellPlanInformationId[]').length;
                for (var i = 0; i < total; i++) {
                    var id = document.getElementsByName('wellPlanInformationId[]')[i].value;
                    var depth = document.getElementsByName('depth[]')[i].value;
                    var stone_id = document.getElementsByName('stone_id[]')[i].value;
                    plan_informations.push({
                        id: id,
                        stone_id: stone_id,
                        depth: depth,
                    });
                }
                data.drill_formations_plan = plan_informations;

                //-- Well Deviation
                var deviations = [];
                var total = document.getElementsByName('deviationId[]').length;
                for (var i = 0; i < total; i++) {
                    var id = document.getElementsByName('deviationId[]')[i].value;
                    var measured_depth = document.getElementsByName('measured_depth[]')[i].value;
                    var inclination = document.getElementsByName('inclination[]')[i].value;
                    var azimuth = document.getElementsByName('azimuth[]')[i].value;
                    var tvd = document.getElementsByName('tvd[]')[i].value;
                    var v_section = document.getElementsByName('v_section[]')[i].value;
                    var n_s = document.getElementsByName('n_s[]')[i].value;
                    var e_w = document.getElementsByName('e_w[]')[i].value;
                    var dls = document.getElementsByName('dls[]')[i].value;
                    var survey_tool = document.getElementsByName('survey_tool[]')[i].value;
                    deviations.push({
                        id: id,
                        measured_depth: measured_depth,
                        inclination: inclination,
                        azimuth: azimuth,
                        //tvd: tvd,
                        //v_section: v_section,
                        //n_s: n_s,
                        //e_w: e_w,
                        //dls: dls,
                        survey_tool: survey_tool,
                    });
                }
                data.well_deviation = deviations;

                //var DataDrillingBulk = formBI.serializeToJSON()["drilling_bulk"];
                //var DataDrillFormation = formPI.serializeToJSON()["drill_formations_plan"];
                //var DataCVD = formCVD.serializeToJSON()["cvd_plan"];
                var DataTVD = formTVD.serializeToJSON()["tvd_plan"];
                var DataDrillingHazard = formDH.serializeToJSON()["drilling_hazard"];
                //var DataWellProfile = formW.serializeToJSON()["well_profile_program"];

                //data.drill_formations_plan = (Object.keys(DataDrillFormation).every(function (x) { return checkProperties(DataDrillFormation[x]); }) === false) ? DataDrillFormation : null;
                //if (data.drill_formations_plan === null) { delete data.drill_formations_plan; }
                //data.cvd_plan = (Object.keys(DataCVD).every(function (x) { return checkProperties(DataCVD[x]); }) === false) ? DataCVD : null;
                //if (data.cvd_plan === null) { delete data.cvd_plan; }
                data.tvd_plan = (Object.keys(DataTVD).every(function (x) { return checkProperties(DataTVD[x]); }) === false) ? DataTVD : null;
                if (data.tvd_plan === null) { delete data.tvd_plan; }
                data.drilling_hazard = (Object.keys(DataDrillingHazard).every(function (x) { return checkProperties(DataDrillingHazard[x]); }) === false) ? DataDrillingHazard : null;
                if (data.drilling_hazard === null) { delete data.drilling_hazard; }
                //data.drilling_bulk = (Object.keys(DataDrillingBulk).every(function (x) { return checkProperties(DataDrillingBulk[x]); }) === false) ? DataDrillingBulk : null;
                //if (data.drilling_bulk === null) { delete data.drilling_bulk; }
                //data.well_profile_program = (Object.keys(DataWellProfile).every(function (x) { return checkProperties(DataWellProfile[x]); }) === false) ? DataWellProfile : null;
                //if (data.well_profile_program === null) { delete data.well_profile_program; }

                btn.button('loading');
                $.post($.helper.resolveApi('~/core/Well/SaveAll'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                        $dt_Basic.DataTable().ajax.reload();
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required fields");
                event.preventDefault();
                event.stopPropagation();
            }

        });

        $('#btn-saveGeneralInfo').click(function (event) {
            saveGeneralInfo();
        });
        $("#btn-saveTimeVDepth").on('click', function () {
            saveTimeVDepth();
        });
        $("#btn-saveDrillingHazard").on('click', function () {
            saveDrillingHazard();
        });
        $("#btn-savePlanFormation").on('click', function () {
            savePlanInformation();
        });

        //$('#btn_save-hcd').on('click', function () {
        //    var btn = $(this);
        //    var isValidate = formHCD[0].checkValidity();
        //    if (isValidate) {
        //        event.preventDefault();
        //        btn.button('loading');
        //        var data = formHCD.serializeToJSON({ associativeArrays: false });

        //        $.ajax({
        //            type: "POST",
        //            dataType: 'json',
        //            contentType: 'application/json',
        //            url: $.helper.resolveApi('~/core/WellHoleAndCasing/save'),
        //            data: JSON.stringify(data.table),
        //            success: function (r) {
        //                if (r.status.success) {
        //                    toastr.success(r.status.message)
        //                    loadHoleAndCasingByWellId($recordId.val());
        //                } else {
        //                    toastr["error"](r.status.message);
        //                }
        //                btn.button('reset');
        //            },
        //            error: function (err) {
        //                // console.error(err);
        //                btn.button('reset');
        //            }
        //        });

        //    }
        //    else {
        //        event.preventDefault();
        //        event.stopPropagation();
        //    }
        //    formHCD.addClass('was-validated');

        //});

        $("#btn-saveRigData").on('click', function () {
            saveRigData();
        });
        $("#btn-saveBulkItem").on('click', function () {
            saveBulkData();
        });
        $("#btn-saveDirectionalSurvey").on('click', function () {
            saveDirectionalSurvey();
        });
        $("#btn-submit").on("click", function () {
            Swal.fire({
                title: "",
                text: "Are you sure want to submit Well?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    submitWell();
                }
            });
        });
        $("#btn-closed").on("click", function () {
            Swal.fire({
                title: "",
                text: "Are you sure want to closed Well?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    closedWell();
                }
            });
        });



        var saveGeneralInfo = function () {
            $('.error-tab').hide();
            var btn = $('#btn-saveGeneralInfo');

            var data = formGWI.serializeToJSON();
            var isValid = formGWI[0].checkValidity();

            // console.log(data);

            if (!isValid) {
                formGWI.addClass('was-validated');
                isValid = false;
                $(".error-gwi").show();
            }

            if (isValid) {
                event.preventDefault();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/Well/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required fields");
                event.preventDefault();
                event.stopPropagation();
            }
        }
        var saveTimeVDepth = function () {
            $('.error-tab').hide();
            var btn = $("#btn-saveTimeVDepth");

            var data = new Object();
            data.well = new Object();
            data.well.id = $recordId.val();
            data.tvd_plan = new Object();

            var isValidate = formTVD[0].checkValidity();
            if (isValidate) {
                if (tdc_content.children().length > 0) {
                    var tvd = [];
                    tdc_content.children().each(function () {
                        tvd.push({
                            id: $(this).find("input[name*='id']").val() == undefined ? "" : $(this).find("input[name*='id']").val(),
                            activity: $(this).find("input[name*='activity']").val(),
                            depth: $(this).find("input[name*='depth']").val().replace(/,/g, ''),
                            hour: $(this).find("input[name*='hour']").val().replace(/,/g, ''),
                            days: $(this).find("input[name*='days']").val().replace(/,/g, ''),
                            cost: $(this).find("input[name*='cost']").val().replace(/,/g, ''),
                            cumm_cost: $(this).find("input[name*='cumm_cost']").val().replace(/,/g, '')
                        });
                    });
                    data.tvd_plan = tvd;
                }

                btn.button('loading');
                $.post($.helper.resolveApi('~/core/TVDPlan/save'), data, function (r) {
                    if (r.status.success) {
                        // console.log("TVD Saved.");
                        // console.log(r);
                        toastr.success(r.status.message)
                        $("#planned_days").val(r.data.planned_days);
                        $("#planned_td").val(r.data.planned_td);
                        maskingMoney();
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                formTVD.addClass('was-validated');
                toastr.error("Please complete all required fields");
                event.preventDefault();
                event.stopPropagation();
            }
        }
        var saveDrillingHazard = function () {
            $('.error-tab').hide();
            var btn = $("#btn-saveDrillingHazard");

            var data = new Object();
            data.well = new Object();
            data.well.id = $recordId.val();

            var DataDrillingHazard = formDH.serializeToJSON()["drilling_hazard"];

            var isValidate = formDH[0].checkValidity();

            if (DataDrillingHazard == undefined) {
                toastr.error("Drilling Hazard cannot be empty.");
                return;
            }


            if (isValidate) {
                event.preventDefault();

                data.drilling_hazard = (Object.keys(DataDrillingHazard).every(function (x) { return checkProperties(DataDrillingHazard[x]); }) === false) ? DataDrillingHazard : null;
                if (data.drilling_hazard === null) { delete data.drilling_hazard; }

                btn.button('loading');
                $.post($.helper.resolveApi('~/core/drillinghazard/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required drilling hazard fields");
                event.preventDefault();
                event.stopPropagation();
            }
        }
        /*
        var saveBulk = function () {
            $('.error-tab').hide();
            var btn = $("#btn-saveBulkItem");

            var data = new Object();
            data.well = new Object();
            data.well.id = $recordId.val();

            var DataDrillingBulk = formBI.serializeToJSON()["drilling_bulk"];

            var isValidate = formDH[0].checkValidity();

            if (DataDrillingBulk == undefined) {
                toastr.error("Drilling Hazard cannot be empty.");
                return;
            }


            if (isValidate) {
                event.preventDefault();

                data.drilling_bulk = (Object.keys(DataDrillingBulk).every(function (x) { return checkProperties(DataDrillingBulk[x]); }) === false) ? DataDrillingHazard : null;
                if (data.drilling_bulk === null) { delete data.drilling_bulk; }

                btn.button('loading');
                $.post($.helper.resolveApi('~/core/wellitem/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required Bulk Item fields");
                event.preventDefault();
                event.stopPropagation();
            }
        }
        */
        var savePlanInformation = function () {
            $('.error-tab').hide();
            var btn = $("#btn-savePlanFormation");

            var data = new Object();
            data.well = new Object();
            data.well.id = $recordId.val();
            data.drill_formations_plan = new Object();

            var isValidate = formPI[0].checkValidity();

            if (data.drill_formations_plan.length <= 0 || !isValidate) {
                $(".error-pi").show();
                isValidate = false;
                toastr.error("Plan Information cannot be empty.");
                return;
            }

            if (isValidate) {
                btn.button('loading');
                event.preventDefault();

                //-- Plan Information
                var plan_informations = [];
                var total = document.getElementsByName('wellPlanInformationId[]').length;
                for (var i = 0; i < total; i++) {
                    var id = document.getElementsByName('wellPlanInformationId[]')[i].value;
                    var depth = document.getElementsByName('depth[]')[i].value.replace(/,/g, '');
                    var stone_id = document.getElementsByName('stone_id[]')[i].value;
                    plan_informations.push({
                        id: id,
                        stone_id: stone_id,
                        depth: depth,
                    });
                }
                data.drill_formations_plan = plan_informations;

                $.post($.helper.resolveApi('~/core/DrillFormationsPlan/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required fields");
                event.preventDefault();
                event.stopPropagation();
            }
        }
        var saveRigData = function () {
            $('.error-tab').hide();
            var btn = $("#btn-saveRigData");

            var data = new Object();
            var RD = formRD.serializeToJSON();
            data.well = formGWI.serializeToJSON();
            data.well = $.extend(true, data.well, RD);
            data.well.id = $recordId.val();

            var isValid = formRD[0].checkValidity();

            if (!isValid) {
                formRD.addClass('was-validated');
                isValid = false;
                $(".error-rd").show();
            }

            if (isValid) {
                event.preventDefault();

                btn.button('loading');
                $.post($.helper.resolveApi('~/core/Well/Save'), data.well, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required fields");
                event.preventDefault();
                event.stopPropagation();
            }
        }
        var saveBulkData = function () {
            $('.error-tab').hide();
            var btn = $("#btn-saveBulkItem");

            var data = new Object();
            data.well = new Object();
            data.well.id = $recordId.val();
            data.well_item = new Object();

            var isValid = formBI[0].checkValidity();

            //--  Well Item
            var items = [];
            var total = document.getElementsByName('wellItemId[]').length;
            for (var i = 0; i < total; i++) {
                items.push({
                    id: document.getElementsByName('wellItemId[]')[i].value,
                    item_id: document.getElementsByName('item_id[]')[i].value,
                });
            }
            data.well_item = items;

            if (data.well_item.length <= 0) {
                $(".error-bi").show();
                toastr.error("Well Item cannot be empty.");
                return;
            }

            if (isValid) {
                event.preventDefault();

                btn.button('loading');
                $.post($.helper.resolveApi('~/core/wellitem/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required fields");
                event.preventDefault();
                event.stopPropagation();
            }
        }

        var saveDirectionalSurvey = function () {
            $('.error-tab').hide();
            var btn = $("#btn-saveDirectionalSurvey");

            var data = new Object();
            data.well = new Object();
            data.well.id = $recordId.val();
            data.well_deviation = new Object();
            data.well.target_direction = $("#target_direction").val();

            if (data.well.target_direction == "" || data.well.target_direction == null || isNaN(data.well.target_direction)) {
                toastr.error("Target Directional Fields cannot be empty.");
                return;
            }

            var isValidate = formWellDeviation[0].checkValidity();

            //-- Well Deviation
            var deviations = [];
            var total = document.getElementsByName('deviationId[]').length;
            for (var i = 0; i < total; i++) {
                var id = document.getElementsByName('deviationId[]')[i].value;
                var measured_depth = document.getElementsByName('measured_depth[]')[i].value;
                var inclination = document.getElementsByName('inclination[]')[i].value;
                var azimuth = document.getElementsByName('azimuth[]')[i].value;
                var tvd = document.getElementsByName('tvd[]')[i].value;
                var v_section = document.getElementsByName('v_section[]')[i].value;
                var n_s = document.getElementsByName('n_s[]')[i].value;
                var e_w = document.getElementsByName('e_w[]')[i].value;
                var dls = document.getElementsByName('dls[]')[i].value;
                var survey_tool = document.getElementsByName('survey_tool[]')[i].value;
                deviations.push({
                    id: id,
                    measured_depth: measured_depth,
                    inclination: inclination,
                    azimuth: azimuth,
                    //tvd: tvd,
                    //v_section: v_section,
                    //n_s: n_s,
                    //e_w: e_w,
                    //dls: dls,
                    survey_tool: survey_tool,
                });
            }
            data.well_deviation = deviations;

            if (data.well_deviation.length <= 0) {
                $(".error-wellDeviation").show();
                isValidate = false;
            }

            if (isValidate) {
                event.preventDefault();


                btn.button('loading');
                $.post($.helper.resolveApi('~/core/welldeviation/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all required fields");
                event.preventDefault();
                event.stopPropagation();
            }

        }

        var submitWell = function () {
            var btn = $("#btn-submit");

            var data = new Object();
            data.id = $recordId.val();

            btn.button('loading');
            $.post($.helper.resolveApi('~/core/Well/submit'), data, function (r) {
                if (r.status.success) {
                    toastr.success(r.status.message);
                    btn.hide();
                } else {
                    toastr.error(r.status.message);
                }
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            });
        }
        var closedWell = function () {
            var btn = $("#btn-closed");

            var data = new Object();
            data.id = $recordId.val();

            btn.button('loading');
            $.post($.helper.resolveApi('~/core/Well/closed'), data, function (r) {
                if (r.status.success) {
                    toastr.success(r.status.message);
                    btn.hide();
                } else {
                    toastr.error(r.status.message);
                }
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            });
        }


        $("#cloneWellDeviation").on('click', function () {
            var templateScript = $("#rowWellDeviation-template").html();
            var template = Handlebars.compile(templateScript);
            $('#tableWellDeviation > tbody tr.no-data').remove();
            $("#tableWellDeviation > tbody").append(template);
        })

        Handlebars.registerHelper('printHoleCasingNumber', function (index) {
            lastRowHoleAndCasing = index + 1;
            var html = lastRowHoleAndCasing;
            return new Handlebars.SafeString(html);
        });

        Handlebars.registerHelper('printCasingType', function (casing_type) {
            var casing_type_name = "";
            if (casing_type == 1) {
                casing_type_name = "Conductor";
            } else if (casing_type == 2) {
                casing_type_name = "Surface";
            } else if (casing_type == 3) {
                casing_type_name = "Drilling Liner";
            } else if (casing_type == 4) {
                casing_type_name = "Intermediate";
            } else if (casing_type == 5) {
                casing_type_name = "Production Liner";
            } else if (casing_type == 6) {
                casing_type_name = "Perforated Liner";
            } else if (casing_type == 7) {
                casing_type_name = "Production";
            }
            return casing_type_name;
        });

        $(document).on('click', '.row-delete', function () {
            $(this).closest('.rowRecord').remove();
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();

    $(document).ready(function () {
        lookUp();
        FieldLookup();
        pageFunction.init();

    });
}(jQuery));