﻿(function ($) {
    'use strict';
    var $dt_listIadc = $('#dt_listIadc_history');
    var $pageLoading = $('#page-loading-content');

    var pageFunction = function () {
        var loadDataTable = function () {
            var dt = $dt_listIadc.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/IadcHistory/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "iadc_code" },
                    { data: "description" },
                    { data: "remark" },
                    {
                        data: "type",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            console.log(row);
                            if (type === 'display') {
                                if (row.type == "1")
                                    return "<span>PT</span>";
                                else if (row.type == "2")
                                    return "<span>NPT</span>";
                            }
                            return data;
                        }
                    },
                    //{ data: "category" },
                    { data: "phase" },
                    { data: "task" },
             
                    { data: "activity" },
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        // var recordId = $(this).closest('.btn-group').data('id');
                        var recordId = $(this).data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/iadc/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.success("Data has been deleted");
                                                dt.DataTable().ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 