﻿$(document).ready(function () {
    'use strict';
    var $wellId = $('input[id=wellId]');
    var $drillingId = $('input[id=drillingId]');
    var $dt_tableWellBha = $("#tableWellBha");
    var $dt_tableWellBit = $("#tableWellBit");

    tableWellBit

    var pageFunction = function () {
        var loadWellBhaDataTable = function () {
            var dt = $dt_tableWellBha.cmDataTable({
                pageLength: 5,
                ajax: {
                    url: $.helper.resolveApi("~/core/WellBha/dataTable?wellId=" + $wellId.val()),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [

                    // {
                    //     data: "id",
                    //     orderable: false,
                    //     searchable: false,
                    //     class: "text-center",
                    //     render: function (data, type, row, meta) {
                    //         return meta.row + meta.settings._iDisplayStart + 1;
                    //     }
                    // },
                    { data: "bha_no" },
                    //{ data: "string_weight" },
                    //{ data: "weight_rotate" },
                    //{ data: "depth_in" },
                    //{ data: "depth_out" },
                    { data: "total_components" },
                    {
                        data: "id",
                        orderable: true,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output =
                                    `<button onShowModal='true' data-href="` + $.helper.resolve("/core/drilling/bha") + "?well_bha_id=" + row.id + "&well_id=" + $wellId.val() + "&drilling_id=" + $drillingId.val() + `" class="modalTrigger btn btn-info btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#setBhaBitModal">
                                        <span>Details</span>

                                    </button>`;
                                var editBtn =
                                    `<button onShowModal='true' data-href="` + $.helper.resolve("/core/drilling/bhaedit") + "?well_bha_id=" + row.id + "&well_id=" + $wellId.val() + "&drilling_id=" + $drillingId.val() + `" class="modalTrigger btn btn-success btn-xs btn-success waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#setBhaBitModal">
                                        <span>Edit</span>
                                    </button>`;
                                var deleteBtn =
                                    `<button data-id="`+row.id+`" class="btn btn-info btn-xs btn-danger waves-effect waves-themed row-deletedBha" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#setBhaBitModal">
                                        <span>Delete</span>
                                    </button>`;
                                return output + editBtn + deleteBtn;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return "";
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deletedBha', function () {
                        var recordId = $(this).data('id');
                        console.log(recordId)
                        var b = bootbox.confirm({
                           message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                           buttons: {
                               confirm: {
                                   label: "Delete"
                               }
                           },
                           callback: function (result) {
                               if (result) {
                                   var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                   btnConfim.button('loading');
                                   $.ajax({
                                       type: "POST",
                                       dataType: 'json',
                                       contentType: 'application/json',
                                       url: $.helper.resolveApi("~/core/wellbha/delete"),
                                       data: JSON.stringify([recordId]),
                                       success: function (r) {
                                           if (r.status.success) {
                                               toastr.success("Data has been deleted");
                                               $("#setBhaBitModal").modal("hide");
                                               // $dt_tableWellBha.DataTable().ajax.reload();
                                           } else {
                                               toastr.error(r.status.message);
                                           }
                                           btnConfim.button('reset');
                                           b.modal('hide');
                                       },
                                       error: function (r) {
                                           toastr.error(r.statusText);
                                           b.modal('hide');
                                       }
                                   });
                                   return false;
                               }
                           },
                           animateIn: 'bounceIn',
                           animateOut: 'bounceOut'
                       });
                    });
                    //$(this).on('click', '.row-edit', function () {
                    //        var recordId = $(this).closest('.btn-group').data('id');
                    //        $CountryFormModal.modal('show'), $form.loading('start');
                    //        $.helper.form.clear($form);
                    //        $.get($.helper.resolveApi('~/core/Country/' + recordId + '/detail'), function (r) {
                    //            if (r.status.success) {
                    //                $.helper.form.fill($form, r.data);
                    //            }
                    //            $form.loading('stop');
                    //        }).fail(function (r) {
                    //            toastr.error(r.statusText);
                    //            $form.loading('stop');
                    //        });

                    //    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });
            dt.on('processing.dt', function (e, settings, processing) {
                if (processing) {
                } else {
                }
            })
        }

        var loadWellBitDataTable = function () {
            var dt = $dt_tableWellBit.cmDataTable({
                pageLength: 5,
                ajax: {
                    url: $.helper.resolveApi("~/core/WellBit/dataTable?wellId=" + $wellId.val()),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [

                    { data: "bit_number" },
                    //{ data: "bit_run" },
                    { data: "bit_size" },
                    { data: "bit_type" },
                    { data: "serial_number" },
                    {
                        data: "id",
                        orderable: true,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output =
                                    `<button onShowModal='true' data-href="` + $.helper.resolve("/core/drilling/bit") + "?well_bit_id=" + row.id + "&well_id=" + $wellId.val() + "&drilling_id=" + $drillingId.val() + `" class="modalTrigger btn btn-info btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#setBhaBitModal">
                                        <span>Details</span>
                                    </button>`;
                                var editBtn =
                                    `<button onShowModal='true' data-href="` + $.helper.resolve("/core/drilling/bitedit") + "?well_bit_id=" + row.id + "&well_id=" + $wellId.val() + "&drilling_id=" + $drillingId.val() + `" class="modalTrigger btn btn-success btn-xs btn-success waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#setBhaBitModal">
                                        <span>Edit</span>
                                    </button>`;
                                var deleteBtn =
                                    `<button data-id="`+row.id+`" class="btn btn-info btn-xs btn-danger waves-effect waves-themed row-deletedBit" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#setBhaBitModal">
                                        <span>Delete</span>
                                    </button>`;
                                return output + editBtn + deleteBtn;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return "";
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deletedBit', function () {
                        var recordId = $(this).data('id');
                        console.log(recordId)
                        var b = bootbox.confirm({
                           message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                           buttons: {
                               confirm: {
                                   label: "Delete"
                               }
                           },
                           callback: function (result) {
                               if (result) {
                                   var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                   btnConfim.button('loading');
                                   $.ajax({
                                       type: "POST",
                                       dataType: 'json',
                                       contentType: 'application/json',
                                       url: $.helper.resolveApi("~/core/wellbit/delete"),
                                       data: JSON.stringify([recordId]),
                                       success: function (r) {
                                           if (r.status.success) {
                                               toastr.success("Data has been deleted");
                                               $("#setBhaBitModal").modal("hide");
                                               $dt_tableWellBha.DataTable().ajax.reload();
                                           } else {
                                               toastr.error(r.status.message);
                                           }
                                           btnConfim.button('reset');
                                           b.modal('hide');
                                       },
                                       error: function (r) {
                                           toastr.error(r.statusText);
                                           b.modal('hide');
                                       }
                                   });
                                   return false;
                               }
                           },
                           animateIn: 'bounceIn',
                           animateOut: 'bounceOut'
                       });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });
            dt.on('processing.dt', function (e, settings, processing) {
                if (processing) {
                } else {
                }
            })
        }

        var getWellBha = function () {
            var templateScript = $("#bhaRecords-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/wellbha/getWellBhaByWell/' + $wellId.val()), function (r) {
                console.log("Well Bha List");
                console.log(r);
                if (r.status.success) {
                    if (r.data.length > 0) {
                        $("#tableWellBha > tbody").html(template({ data: r.data }));

                        //jobPersonelLookUp("");
                        //$('.row-delete').on('click', function () {
                        //    $(this).closest('.rowPersonel').remove();
                        //});
                    }
                }
                //$('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }
        Handlebars.registerHelper('printNumber', function (index) {
            return new Handlebars.SafeString(index + 1);
        });

        return {
            init: function () {
                loadWellBhaDataTable();
                loadWellBitDataTable();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});