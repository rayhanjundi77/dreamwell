﻿(function ($) {
    'use strict';

    var $dt_stone = $('#dt_stone');
    var $pageLoading = $('#page-loading-content');
    var $StoneFormModal = $('#StoneModal');
    var $btnDelete = $('#btn-delete');
 
   

    var pageFunction = function () {

        async function getDataFileMaster(filemasterid) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: $.helper.resolveApi("~Core/GetFile/" + filemasterid + "/detailFile"),
                    type: 'GET',
                    contentType: "application/json",
                    success: function (data) {
                       // console.log("get image file list data")
                       // console.log(r);
                        var urlimageprofile = $.helper.resolve("/UploadLithologyPath/");
                        if(r.data != null){
                            $(".imgpp-"+filemasterid).attr("src",urlimageprofile+r.data.filepath);
                        }else{
                            $(".imgpp-"+filemasterid).attr("src",urlimageprofile+'default.jpg');
                        }
                        // resolve(data)
                    },
                    error: function (error) {
                        reject(error)
                    },
                });
            })

        }


        async function getFileData(stoneid) {
            return $.ajax({
                url: $.helper.resolveApi("~Core/GetFile/" + stoneid + "/detailImageFile"),
                type: 'GET',
                contentType: "application/json",
                success: function (r) {
                   // console.log("get image file list data")
                   // console.log(r);
                    var urlimageprofile = $.helper.resolve("/UploadLithologyPath/");
                    if(r.data != null){
                        $(".imgpp-"+stoneid).attr("src",urlimageprofile+r.data.filepath);
                    }else{
                        $(".imgpp-"+stoneid).attr("src",urlimageprofile+'default.jpg');
                    }
                },
                error: function (r) {

                },
            });

        }


        var loadDataTable = function () {
            var url = $.helper.resolveApi('~/core/Stone/lookup');
            var dt = $dt_stone.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/Stone/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "name",
                        render: function (data, type, row) {
                            if (type === 'display') {
                         

                                var output;
                                getFileData(row.id)
                                output = `<div class="d-flex align-items-center">
                                                        <img class="imgpp-`+row.id+`" src="#" class="mr-3" style="width:50px">
                                                        <h1 class="fw-300 m-0 l-h-n">
                                                            <span class="text-contrast">`+ data + `</span>
                                                        </h1>
                                                    </div>`;



                                return output;
                            }
                            return data;
                        },
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/core/Stone/detail?id=") + row.id + `" class="modalTrigger btn btn-info btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#StoneModal">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-warning btn-xs btn-warning waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                   
                                </div>`;
                                return output;
                                //<a class="row-deleted btn btn-sm btn-primary btn-hover-danger fa fa-trash add-tooltip" href="#"
                                //    data-original-title="Delete" data-container="body">
                                //</a>
                            }
                            return data;
                        },
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/Stone/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                                dt.ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            reloadPlugin();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);

                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                  
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
               // console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        }

        $(document).on('hidden.bs.modal', $StoneFormModal, function () {
            loadDataTable();
        });




        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

   
    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));