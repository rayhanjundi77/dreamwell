﻿(function ($) {
    'use strict';

    var $dt_listCurrency = $('#dt_listCurrency');
    var $pageLoading = $('#page-loading-content');
    var $currencyFormModal = $('#currencyModal');
    var $currencyUpdateModal = $('#currencyUpdateModal');
    var $form = $('#form-Currency');
    var $formUpdate = $("#form-updateCurrency");
    var $btnDelete = $('#btn-delete');
    var $btnUpdateCurrency = $('#btn-updateCurrency');
    var $btnSaveUpdateCurrency = $('#btn-saveUpdateCurrency');
    var $recordId = $('input[id=recordId]');
    var $baseRateId = "";
    
    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };

    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            currency_code: {
                validators: {
                    notEmpty: {
                        message: 'The currency code is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION


    var pageFunction = function () {
 
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $.get($.helper.resolveApi('~/core/Currency/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $('#currency_code').text(':: ' + r.data.currency_code);
                    }
                }).fail(function (r) {
                    $.helper.noty.error(r.status, r.statusText);
                });
            }
        }

        var loadDataTable = function () {
            var dt = $dt_listCurrency.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/Currency/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            console.log(row);
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "currency_code" },
                    { data: "currency_description" },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.currency_symbol == null)
                                    return "<span>No Current Rate";
                                else
                                    return "<span>" + row.currency_symbol + "</span>";
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-right",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.rate_value == null)
                                    return "<span>No Current Rate";
                                else
                                    return "<span>" + thousandSeparator(row.rate_value) + "</span>";
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.end_date == null)
                                    return "<span>N/A</span>";
                                else
                                    return "<span>" + moment(row.start_date).format('DD MMM YYYY') + "</span>";
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.end_date == null)
                                    return "<span>N/A</span>";
                                else
                                    return "<span>" + moment(row.end_date).format('DD MMM YYYY') + "</span>";
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-left",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                if (row.base_rate) {
                                    return "<span class=\"badge badge-info badge-pill\" style=\"font-size: 11px;\">Base Rate</span>";
                                } else {
                                    return "<span class=\"badge badge-primary badge-pill\" style=\"font-size: 11px;\">Not Base Rate</span>";
                                }
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class="btn-group" data-id="`+ row.id + `">
                                    <a class ="row-edit btn btn-xs btn-info btn-hover-info fa fa-pencil add-tooltip" href="#"
                                        data-original-title="Edit" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-edit', function () {
                        $currencyFormModal.modal('show');
                        var recordId = $(this).closest('.btn-group').data('id');
                        console.log(recordId);
                        //$(".panel-overlay").remove();
                        //$("#rate-value-form").hide();
                        //$("#rate-value-form input").attr("disabled", "");
                        //$("#btn-save").hide();
                        //$("#btn-saveChanges").show();
                        $.helper.form.clear($form),
                        $.get($.helper.resolveApi('~/core/Currency/' + recordId + '/detail'), function (r) {
                            console.log(r);
                            if (r.status.success) {
                                $.helper.form.fill($form, r.data);
                                if (r.data.base_rate) {
                                    $('.magic-checkbox').attr('checked', true);
                                } else {
                                    $('.magic-checkbox').attr('checked', false);
                                }
                            }
                        }).fail(function (r) {
                            toastr.error(r.statusText);
                        });
                    });
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 

            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        $('#btn-addNew').on('click', function () {
            $("#rate-value-form").show();
            $("#rate-value-form input").removeAttr("disabled");
            //$("#btn-save").show();
            //$("#btn-saveChanges").hide();
            $('.magic-checkbox').attr('checked', false);
            $.helper.form.clear($form),
                $.helper.form.fill($form, {
                    currency_code: null,
                    currency_description: null
                });
            $currencyFormModal.modal('show');
        });
     
        $btnUpdateCurrency.on('click', function () {
            $btnUpdateCurrency.button('loading');

            var templateScript = $('#currency-template').html();
            var template = Handlebars.compile(templateScript);

            $.ajax({
                type: "GET",
                datatype: 'json',
                url: $.helper.resolveApi("~/core/Currency/getAllCurrency"),
                success: function (r) {
                    console.log(r);
                    if (r.status.success) {
                        var hasBaseRate = false;
                        $.each(r.data, function (key, value) {
                            if (value.base_rate)
                            {
                                $baseRateId = value.id;
                                hasBaseRate = true;
                                $("#base_rate_name").text(value.currency_description + " (" + value.currency_symbol + ")");
                            }
                        });

                        if (hasBaseRate) {
                            $('#listCurrency').html(template(r));
                            $("#btn-saveUpdateCurrency").show();
                        } else {
                            $("#base_rate_name").text("Please Choose 1 Base Rate");
                            $("#btn-saveUpdateCurrency").hide();
                        }
                        $btnUpdateCurrency.button('reset');
                    } else {

                    }
                }
            });

            $currencyUpdateModal.modal('show');
        });

        $('#btn-save').on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();

                //console.log(data); return;
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/currency/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        loadDataTable();
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusTex);
                });

            }
            else return;
        });

        $('#btn-saveChanges').on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/Currency/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                        loadDataTable();
                    } else {
                        toastr.error(r.status.message);
                    }
                    btn.button('reset');
                    toastr.success(r.status.message);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });

            }
            else return;
        });

        $btnSaveUpdateCurrency.on('click', function () {
            var currency_exchange = [];

            var currency_id = $formUpdate.find('.currency_id');
            //var startDate = $("input[name='start_date']").val();
            //var endDate = $("input[name='end_date']").val();



            $.each(currency_id, function (key, value) {
                var currency_id = $(value).val();
                var rateValue = $("#rate-value-" + currency_id).val();

                var arytemp = {
                    source_id: $baseRateId,
                    target_id: currency_id,
                    rate_value: rateValue,
                    //start_date: startDate,
                    //end_date: endDate
                }
                currency_exchange.push(arytemp);
            });

            //-- Base Rate 1:1
            var arytemp = {
                source_id: $baseRateId,
                target_id: $baseRateId,
                rate_value: 1,
            }
            currency_exchange.push(arytemp);

            //console.log(currency_exchange); return;
            $btnSaveUpdateCurrency.button('loading');

            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                url: $.helper.resolveApi('~/core/CurrencyExchange/saveall'),
                data: JSON.stringify(currency_exchange),
                success: function (data) {
                    if (data.status.success) {
                        loadDataTable();
                        toastr.success(data.status.message);
                    }
                    else {
                        toastr.error(data.status.message);
                    }
                    $currencyUpdateModal.modal('hide');
                    $btnSaveUpdateCurrency.button('reset');
                },
                error: function (err) {
                    console.error(err);
                }
            });

        });


        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));