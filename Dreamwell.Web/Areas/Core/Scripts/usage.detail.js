﻿ $(document).ready(function () {
     'use strict';
     var $recordId = $('input[id=recordId]');
     var $costDate = $('input[id=costDate]');
     var $wellId = $('input[id=well_id]');
     var $form = $("#formDrilling");
     var $afeId = "";

     var $formDrillingOperation = $("#formDrillingOperation");
     var $formOperationActivities = $("#formOperationActivities");

     var controls = {
        leftArrow: '<i class="fal fa-angle-left" style="font-size: 1.25rem"></i>',
        rightArrow: '<i class="fal fa-angle-right" style="font-size: 1.25rem"></i>'
     }

     var datepickerFormat = function (element) {
         element.datepicker({
             autoclose: true,
             todayBtn: "linked",
             format: 'mm/dd/yyyy',
             orientation: "bottom left",
             todayHighlight: true,
             templates: controls
         });
     }

     var pageFunction = function () {
         $.fn.editable.defaults.url = "/post";
         $.fn.editable.defaults.mode = 'inline';

         var loadDetail = function () {
             if ($recordId.val() !== '') {
                 $.get($.helper.resolveApi('~/core/drilling/' + $recordId.val() + '/detailDrilling'), function (r) {
                     console.log(r);
                     if (r.status.success) {
                         $afeId = r.data.afe_id;
                         $("#well_name").html(r.data.well_name);
                         $("#report_date").html(moment(r.data.drilling_date).format('MMM DD, YYYY'));
                         $("#daily_cost").html("USD " + thousandSeparatorWithoutComma(r.data.daily_cost));
                         loadAfeManageBudget($recordId.val(), r.data.afe_id, $costDate.val());
                     }
                     $('.loading-detail').hide();
                 }).fail(function (r) {
                 }).done(function () {

                 });
             }
         };

         var loadAfeManageBudget = function (drillingId, afeId, costDate) {
             var templateScript = $("#afeManageBudget-template").html();
             var template = Handlebars.compile(templateScript);
             $.get($.helper.resolveApi('~/core/DailyCost/getManageDailyCost/' + drillingId + '/' + afeId + '/' + costDate), function (r) {
                 if (r.status.success && r.data.length > 0) {
                     $("#listAfeManageBudget > tbody").html(template({ data: r.data }));
                     initEditableDailyCost();
                 }
                 $('.loading-detail').hide();
             }).fail(function (r) {
             }).done(function () {

             });
         }

         var initEditableDailyCost = function () {
             $(".editable-daily-cost").editable({
                 url: $.helper.resolveApi("~/core/AfeManageBudget/save"),
                 ajaxOptions: { contentType: 'application/json', dataType: 'json' },
                 params: function (params) {
                     var amount = params.value;
                     var req = {
                         id: $(this).data('id'),
                         afe_id: $(this).data('afeid'),
                         afe_line_id: $(this).data('afelineid'),
                         contract_detail_id: $(this).data('contractdetailid'),
                         original_budget: amount,
                     };
                     return JSON.stringify(req);
                 },
                 type: "number",
                 step: 'any',
                 pk: 1,
                 name: "amount",
                 title: "Enter Program Budget",
                 display: function (value) {
                     $(this).text(thousandSeparatorDecimal(value));
                 },
                 validate: function (value) {
                     var myStr = value.replace(/\./g, '');
                     if ($.isNumeric(myStr) == '') {
                         return 'Only numbers are allowed';
                     }
                 },
                 success: function (r) {
                     if (r.status.success) {
                         toastr.success(r.status.message);
                     } else {
                         toastr.error(r.status.message);
                         return r.status.message;
                     }
                 }
             }).on('shown', function (e, editable) {
                 autoNumericEditable(editable);
             });
         }

         $(document).on('hidden.bs.modal', "#afeManageBudgetModal", function () {
             loadAfeManageBudget($recordId.val(), $afeId, $costDate.val());
         });

         Handlebars.registerHelper("formatNominal", function (budget) {
             if (!isNaN(budget) && budget > 0) {
                 return "USD " + thousandSeparatorDecimal(budget);
             } else {
                 return "-";
             }
         });
         Handlebars.registerHelper("formatExpense", function (budget, expense) {
             if (!isNaN(budget) && budget > 0) {
                 return "USD " + thousandSeparatorDecimal(expense);
             } else {
                 return "-";
             }
         });
         Handlebars.registerHelper("counter", function (index) {
             return index + 1;
         });

         $(document).on('click', '.row-delete', function () {
             $(this).closest('.rowOperation').remove();
         });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});