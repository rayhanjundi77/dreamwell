﻿
(function ($) {
    'use strict';
    var pageFunction = function () {
        var $recordId = $('input[id=recordId]');

        var is_openCellMapper = false;
        var $modal = $('#modal_column_mapper');
        var $form = $('#frm_template');


        //BEGIN::Dropzone
        Dropzone.autoDiscover = false;
        $("#demo-dropzone").show();
        var myDropzone = new Dropzone("div#demo-dropzone", { // Make the whole body a dropzone
            thumbnailWidth: 50,
            thumbnailHeight: 50,
            parallelUploads: 20,
            autoQueue: true, // Make sure the files aren't queued until manually added,
            headers:
            {
                "Authorization": 'Bearer ' + $('meta[name=x-token]').attr('content'),
                "Server-Type": 'Dreamwell.ASP.NET Web API'
            },
            accept: function (file, done) {
                done();
            },
            init: function () {
                this.on("processing", function (file) {
                    this.options.url = $.helper.resolveApi("~/core/FileMaster/upload/" + $recordId.val() +"?entityName=excel template");
                });
                this.on("complete", function (file) {
                    $.helper.noty.success("Successfully", "Upload file successfully");
                    loadAttachment();
                    this.removeAllFiles();
                });
            }
        });


        //END::Dropzone
        $(document).bind("onInspectCell", function (t, e) {
            is_openCellMapper = true;
        });
        function onSelect(arg) {

            if (is_openCellMapper) {
                console.log(arg);
                console.log(arg.range);
                console.log("New range selected. New value: " + arg.range.value());
                var columnAddress = $.helper.excel.columnIndexToRangeName(arg.range._ref.col);
                $('input[name=cell_address_value]').val(columnAddress + (arg.range._ref.row + 1));

                $modal.unbind('onInspectCell');
                $("#spreadsheet").unbind('mousemove');
                $(".cursor-tooltip").css({
                    "left": 0,
                    "top": 0,
                    "visibility": "hidden"
                });
                $modal.modal('show');
                is_openCellMapper = false;
            }

        }

        $("#spreadsheet").kendoSpreadsheet({
            select: onSelect,
            sheetsbar: false,
            excel: {
                // Required to enable saving files in older browsers
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
            },
            pdf: {
                proxyURL: "https://demos.telerik.com/kendo-ui/service/export"
            }
        });

        var spreadsheet = $("#spreadsheet").data("kendoSpreadsheet"),
            cellContextMenu = spreadsheet.cellContextMenu();

        cellContextMenu.append([{ text: "Set Parameter", cssClass: "set_parameter" }]);


        cellContextMenu.bind("select", function (e) {
            var command = $(e.item).text();
            if (command == "Set Parameter") {
                var urlMapper = "/Core/ExcelTemplate/ColumnMapper?id=";

                var sheet = spreadsheet.activeSheet(),
                    selection = sheet.selection();
                console.log(selection);
                //selection.background("green");
                $modal.find('.modal-content').html('');
                $.get(urlMapper, showModalCellMapper);


            }
        });


        var showModalCellMapper = function (data) {
            var options = { show: true };

            options = $.extend({}, options, {
                backdrop: "static", //remove ability to close modal with click
                keyboard: false, //remove option to close with keyboard
                show: true //Display loader!
            });
            $modal.find('.modal-content').html('');
            $modal.find('.modal-content').append(data);
            $modal.modal(options);

            var sheet = spreadsheet.activeSheet(),
                selection = sheet.selection();
            var columnAddress = $.helper.excel.columnIndexToRangeName(selection._sheet._viewSelection.originalActiveCell.col);
            $('input[name=cell_address_column]').val(columnAddress + (selection._sheet._viewSelection.originalActiveCell.row + 1));
            console.log(selection._sheet._viewSelection.originalActiveCell.col);



        }

        var loadDetail = function () {
            if ($recordId.val() != '') {
                $.get($.helper.resolveApi('~/core/ExcelTemplate/' + $recordId.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                        spreadsheet.fromJSON(JSON.parse(r.data.json_template));
                    }
                }).fail(function (r) {

                });
            }

        };

        $('#btn_save').click(function () {
            var btn = $(this);

            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                btn.button('loading');
                var data = $form.serializeToJSON();
                data['json_template'] = JSON.stringify(spreadsheet.toJSON());

                $.post($.helper.resolveApi('~/core/ExcelTemplate/save'), data, function (r) {
                    if (r.status.success) {
                        history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordId);
                        $('input[name=id]').val(r.data.recordId);
                        toastr["info"](r.status.message);
                    } else {
                        toastr["error"](r.status.message);
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                });

            }
            $form.addClass('was-validated');
        });


        $('#btn_json').click(function () {

            console.log(spreadsheet.toJSON());
        });

        $('#btn_get_cell').click(function () {
            var range = spreadsheet.activeSheet();
            console.log(range.sheetIndex);

        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));