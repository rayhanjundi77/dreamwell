$(document).ready(function () {
    'use strict';
    //var $id = $('input[id=well_hole_and_casing_id]');
    var $recordId = $('#drillingId').val();
    console.log("Drilling ID:", $recordId);


    var maskingMoney = function () {
        $(".numeric-money").inputmask({
            digits: 2,
            greedy: true,
            definitions: {
                '*': {
                    validator: "[0-9]"
                }
            },
            rightAlign: false
        });
    };

    var getdrillingId = function (recordId) {
        console.log("hehe", recordId)
        $.get($.helper.resolveApi('~/core/DrillingHoleAndCasing/' + recordId + '/multicasing'), function (r) {
            var dataCase = r.data[0]
            console.log("kakaka", dataCase.well_id)
            getlistMulticasing(dataCase.well_hole_and_casing_id)
            //$.helper.form.fill($form, r.data);    
        }).fail(function (r) {
            //console.log(r);
        }).done(function () {

        });
    }

    var getlistMulticasing = function (dataId) {
        console.log("ididididid", dataId)
        var templateScript = $("#listMulticasing-template").html();
        var template = Handlebars.compile(templateScript);
        $.get($.helper.resolveApi('~/core/DrillingHoleAndCasing/' + dataId + '/ListMulticasing'), function (r) {
            console.log("kakaka", r.data)
            $("#form-ListMulticasing > .panel-body").html(template({ data: r.data }));
            maskingMoney();
            //$.helper.form.fill($form, r.data);    
        }).fail(function (r) {
            //console.log(r);
        }).done(function () {

        });
    }

    $(document).on('click', '#btn-mc-delete', function () {
        var recordIdHCD = $(this).data("id");
        console.log("Delete button clicked for ID:", recordIdHCD);
        var b = bootbox.confirm({
            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this! </p>",
            buttons: {
                confirm: {
                    label: "Delete"
                }
            },
            callback: function (result) {
                if (result) {
                    var btnConfim = $(this).find('.bootbox-accept');
                    btnConfim.button('loading');
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: 'application/json',
                        url: $.helper.resolveApi("~/core/DrillingHoleAndCasing/delete"),
                        data: JSON.stringify([recordIdHCD]),
                        success: function (r) {
                            if (r.status.success) {
                                toastr.success("Data has been deleted");
                                location.reload();
                            } else {
                                toastr.error(r.status.message);
                                location.reload();
                            }
                            btnConfim.button('reset');
                            b.modal('hide');
                            $dtBasic.DataTable().ajax.reload();
                        },
                        error: function (r) {
                            toastr.error(r.statusText);
                            b.modal('hide');
                        }
                    });
                    return false;
                }
            },
            animateIn: 'bounceIn',
            animateOut: 'bounceOut'
        });
    });
    var pageFunction = function () {
        return {
            init: function () {
                //casinglookup(1); // Assuming the ID is 1 for the first field
                maskingMoney();
                getdrillingId($recordId);

            }
        };
    }();

    pageFunction.init();
});
