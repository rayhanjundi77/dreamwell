﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#form")

    $('#btn-business-unit-lookup').click(function () {
        $("#business_unit_id").val("");
        $("#business-unit-validation").hide();
        var btn = $(this);
        btn.button('loading');
        jQuery.ajax({
            type: 'POST',
            url: '/core/businessunit/Lookup',
            success: function (data) {
                btn.button('reset');
                var $box = bootbox.dialog({
                    message: data,
                    title: "",
                    callback: function (e) {
                        console.log(e);
                    }
                });
                $box.on("onSelected", function (o, event) {
                    $box.modal('hide');
                    $("#business_unit_id").val(event.node.id);
                    $("#btn-business-unit-lookup").text(event.node.text);
                });
            }
        });
    });


    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                console.log($recordId.val());
                $.get($.helper.resolveApi('~/core/contract/' + $recordId.val() + '/detail'), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };

        $('#btn-save').click(function (event) {
            var data = $form.serializeToJSON();
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/team/save'), data, function (r) {
                    console.log(r);
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                if (data.business_unit_id == null)
                    $("#business-unit-validation").show();
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});