﻿(function ($) {
    'use strict';

    var $dt_well = $('#dt_well');
    var $pageLoading = $('#page-loading-content');
    var $currencyUpdateModal = $('#currencyUpdateModal');
    var $form = $('#form-Well');

    var btnSubmitGWI = $('#btn_gwi_submit');
    var btnSubmitTDC = $('#btn_tdc_submit');
    var btnSubmitDH = $('#btn_dh_submit');
    var btnSubmitPI = $('#btn_pi_submit');
    var btnSubmitHCD = $('#btn_hcd_submit');
    var btnSubmitRD = $('#btn_rd_submit');
    var btnSubmitBI = $('#btn_bi_submit');
    var btnSubmitPDS = $('#btn_pds_submit');

    var btn_add_dh = $('#dh_add_depth');
    var dh_content = $('#dh_content');

    var btn_add_pi = $('#pi_add_depth');
    var pi_content = $('#pi_content');

    var btn_add_hcd = $('#hcd_add');
    var hcd_content = $('#hcd_content');

    var btn_add_bi = $('#bi_add_bulk');
    var bi_content = $('#bi_content');

    var btn_add_pds = $('#pds_add');
    var pds_content = $('#pds_content');

    var $btnUpdateWell = $('#btn-updateWell');
    var $btnSaveUpdateWell = $('#btn-saveUpdateWell');
    var $recordId = $('input[id=recordId]');

    // ============ Button Add in tab (Modal) =================
    btn_add_dh.on('click', function (e) {
        var count = dh_content.find(".form-group").length;
        var elems = '<div class="form-group" >'
            + '<div class="row">'
            + '<div class="col-md-6">'
            + '<input class="form-control" id="dh_depth' + (Number(count) + 1) + '" placeholder="Depth (ft)" />'
            + '</div>'
            + '<div class="col-md-6">'
            + '<input class="form-control" id="dh_description' + (Number(count) + 1) + '" placeholder="Description" />'
            + '</div>'
            + '</div>'
            + '</div >';
        dh_content.append(elems);
    });

    btn_add_pi.on('click', function (e) {
        var count = pi_content.find(".form-group").length;
        var elems = '<div class="form-group" >'
            + '<div class="row">'
            + '<div class="col-md-2">'
            + 'No:' + (Number(count) + 1)
            + '</div>'
            + '<div class="col-md-5">'
            + '<input class="form-control" id="pi_depth' + (Number(count) + 1) + '" placeholder="Depth (ft)" />'
            + '</div>'
            + '<div class="col-md-5">'
            + '<select class="form-control" id="pi_description' + (Number(count) + 1) + '">' + '</select>'
            + '</div></div></div > ';
        pi_content.append(elems);
    });

    btn_add_hcd.on('click', function (e) {
        var count = Number(hcd_content.find(".hcd_container").length) + 1;
        var elems = '<div class="hcd_container" >'
            + '--- Hole ---'
            + '<hr />'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="hole_diameter">Hole Diameter</label>'
            + '</div>'
            + '<div class="col-md-8">'
            + '<input type="text" class="form-control" id="hole_diameter' + count + '" placeholder="Diameter (inch)" />'
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="hole_depth">Hole Depth</label> '
            + '</div>'
            + '<div class="col-md-8">'
            + '<input class="form-control" id="hole_depth' + count + '" placeholder="Depth (ft)" />'
            + '</div>'
            + '</div>'
            + '</div>'
            + '--- Casing-- -'
            + '<hr />'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="casing_diameter">Casing Diameter</label>'
            + '</div>'
            + '<div class="col-md-8">'
            + '<input class="form-control" id="casing_diameter' + count + '" placeholder="Diameter (inch)" />'
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="casing_setting_depth">Casing Setting Depth</label > '
            + '</div>'
            + '<div class="col-md-8">'
            + '<input class="form-control" id="casing_setting_depth' + count + '" placeholder="Setting Depth (ft)" />'
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="casing_type">Casing Type</label>'
            + '</div>'
            + '<div class="col-md-8">'
            + '<select class="form-control" id="casing_type' + count + '">' + '</select>'
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="casing_weight">Casing Weight</label>'
            + '</div>'
            + '<div class="col-md-8">'
            + '<input class="form-control" id="casing_weight' + count + '" placeholder="Casing Weight (ppf)" />'
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="casing_grade">Casing Grade</label>'
            + '</div>'
            + '<div class="col-md-8">'
            + '<input class="form-control" id="casing_grade' + count + '" placeholder="Casing Grade" />'
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="casing_connection">Casing Connection</label>'
            + '</div>'
            + '<div class="col-md-8">'
            + '<input class="form-control" id="casing_connection' + count + '" placeholder="Casing Connection" />'
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="form-group">'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="casing_top_of_liner">Casing Top of Liner</label>'
            + '</div>'
            + '<div class="col-md-8">'
            + '<input class="form-control" id="casing_top_of_liner' + count + '" placeholder="Casing Top of Liner" />'
            + '</div>'
            + '</div>'
            + '</div>'
            + '</div>';
        hcd_content.append(elems);
    });

    btn_add_bi.on('click', function (e) {
        var count = Number(bi_content.find(".form-group").length) + 1;
        var elems = '<div class="form-group" >'
            + '<div class="row">'
            + '<div class="col-md-3">'
            + '<label for="bulk_item_name">Item Name</label>'
            + '</div>'
            + '<div class="col-md-8">'
            + '<input class="form-control" id="bulk_item_name' + count + '" placeholder="Bulk Item Name" />'
            + '</div>'
            + '</div>'
            + '</div >';
        bi_content.append(elems);
    });

    btn_add_pds.on('click', function (e) {
        var count = Number(pds_content.find("tr").length) + 1;
        var elems = '<tr>'
            + '<td><input type="number" id="bi_md' + count + '" class="form-control" /></td>'
            + '<td><input type="number" id="bi_in' + count + '" class="form-control" /></td>'
            + '<td><input type="number" id="bi_az' + count + '" class="form-control" /></td>'
            + '<td><input type="number" id="bi_tvd' + count + '" class="form-control" /></td>'
            + '<td><input type="number" id="bi_vs' + count + '" class="form-control" /></td>'
            + '<td><input type="number" id="bi_ns' + count + '" class="form-control" /></td>'
            + '<td><input type="number" id="bi_ew' + count + '" class="form-control" /></td>'
            + '<td><input type="number" id="bi_dls' + count + '" class="form-control" /></td>'
            + '<td><input type="number" id="bi_st' + count + '" class="form-control" /></td>'
            + '</tr >';
        pds_content.append(elems);
    });

    btnSubmitGWI.on('click', function (e) { });
    btnSubmitTDC.on('click', function (e) { });
    btnSubmitDH.on('click', function (e) { });
    btnSubmitPI.on('click', function (e) { });
    btnSubmitHCD.on('click', function (e) { });
    btnSubmitRD.on('click', function (e) { });
    btnSubmitBI.on('click', function (e) { });
    btnSubmitPDS.on('click', function (e) { });
    //=========================================================

    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fal fa-check-circle fa-lg text-success',
        invalid: 'fal fa-times-circle fa-lg',
        validating: 'fal fa-refresh'
    };
    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            field_name: {
                validators: {
                    notEmpty: {
                        message: 'The Well Name is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION

    var pageFunction = function () {

        var loadDataTable = function () {

            var dt = $dt_well.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/Well/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    },

                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "well_name" },
                    { data: "field_name" },
                    { data: "well_type" },
                    //{
                    //    data: "id",
                    //    orderable: false,
                    //    searchable: false,
                    //    class: "text-left",
                    //    render: function (data, type, row) {
                    //        if (type === 'display') {
                    //            if (row.well_status == 0)
                    //                return `<span>Original Well</span>`;
                    //            else if (row.well_status == 1)
                    //                return `<span>Side Track</span>`;
                    //            else if (row.well_status == 2)
                    //                return `<span>Work Over</span>`;
                    //            else if (row.well_status == 2)
                    //                return `<span>Well Services</span>`;
                    //        }
                    //        return data;
                    //    },
                    //},
                    { data: "contractor_name" },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.submitted_by == null)
                                    return `<span class="badge badge-info p-1 pr-3 pl-3" style="width: 75px;">Draft</span>`;
                                else {
                                    if (row.closed_by == null)
                                        return `<span class="badge badge-warning p-1 pr-3 pl-3" style="width: 75px;">Submitted</span>`;
                                }
                            }
                            return data;
                        }, 
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.id + `" >
                                    <a class ="row-edit btn btn-xs btn-info btn-hover-info fa fa-pencil add-tooltip" href="/Core/Well/Detail?id=`+ row.id + `"
                                        data-original-title="Edit" data-container="body">
                                    </a>
                                    <a class="row-deleted btn btn-xs btn-warning btn-hover-warning fa fa-trash add-tooltip" href="#"
                                        data-original-title="Delete" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        },
                        width: "10%"
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/Well/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                                //$dt_field.DataTable().ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dt_well.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);

                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    }),
                        $(this).on('click', '.row-edit', function () {

                            var recordId = $(this).closest('.btn-group').data('id');
                            $WellFormModal.modal('show'), $form.loading('start');
                            $.helper.form.clear($form);
                            $.get($.helper.resolveApi('~/core/Well/' + recordId + '/detail'), function (r) {

                                if (r.status.success) {
                                    $.helper.form.fill($form, r.data);
                                }
                                $form.loading('stop');
                            }).fail(function (r) {
                                toastr.error(r.statusText);
                                $form.loading('stop');
                            });

                        });
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
                console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        }

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $('#btn-addNew').on('click', function () {
        $("#rate-value-form").show();
        $("#rate-value-form input").removeAttr("disabled");
        $("#btn-save").show();
        $("#btn-saveChanges").hide();
        $('.magic-checkbox').attr('checked', false);
        //$.helper.form.clear($form),
        //    $.helper.form.fill($form, {
        //        currency_code: null,
        //        currency_description: null
        //    });
        $wellFormModal.modal('show');
    });

    $btnUpdateWell.on('click', function () {
        $btnUpdateWell.button('loading');

        var templateScript = $('#currency-template').html();
        var template = Handlebars.compile(templateScript);

        $.ajax({
            type: "GET",
            datatype: 'json',
            url: $.helper.resolveApi("~core/Well/getAllWell"),
            success: function (r) {
                console.log(r);
                if (r.status.success) {
                    var hasBaseRate = false;
                    $.each(r.data, function (key, value) {
                        if (value.base_rate) {
                            hasBaseRate = true;
                            $("#base_rate_name").text(value.currency_description + " (" + value.currency_symbol + ")");
                        }
                    });

                    if (hasBaseRate) {
                        $('#listWell').html(template(r));
                    } else {
                        $("#base_rate_name").text("Please Choose 1 Base Rate");
                        $("#btn-saveUpdateWell").hide();
                    }



                    $btnUpdateWell.button('reset');
                } else {

                }
            }
        });

        $currencyUpdateModal.modal('show');
    });

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));