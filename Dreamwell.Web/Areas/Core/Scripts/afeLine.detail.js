﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formAfeLine")

   
    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                console.log($recordId.val());
                $.get($.helper.resolveApi('~/core/afeLine/' + $recordId.val() + '/detail'), function (r) {
                   
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                        if (r.data.is_active) {
                            $("#is_active").attr("checked", "checked");
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    console.error(r);
                })
            }
        };

        $('#btn-save').click(function (event) {
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();
            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                $.post($.helper.resolveApi('~/core/afeLine/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});