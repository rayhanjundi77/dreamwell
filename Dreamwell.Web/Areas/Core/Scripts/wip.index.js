﻿$(document).ready(function () {
    'use strict';



    $('#well_color').on('change', function () {
        const selectedOption = $(this).find('option:selected');
        const bgColor = selectedOption.data('bg');

        $(this).css('background-color', bgColor);
    });

    // Initialize Summernote
    $('#objective').summernote({
        placeholder: 'Enter Objective',
        tabsize: 2,
        height: 200,
        callbacks: {
            onImageUpload: function (files) {
                uploadFile(files[0], $(this));
            }
        }
    });

    $('#introduction').summernote({
        placeholder: 'Enter Introduction',
        tabsize: 2,
        height: 200,
        callbacks: {
            onImageUpload: function (files) {
                uploadFile(files[0], $(this));
            }
        }
    });

    $('#program_detail').summernote({
        placeholder: 'Enter Program Detail',
        tabsize: 2,
        height: 200,
        callbacks: {
            onImageUpload: function (files) {
                uploadFile(files[0], $(this));
            }
        }
    });

    $('#wli_comments_wpi').summernote({
        placeholder: 'Enter Objective',
        tabsize: 2,
        height: 200,
        callbacks: {
            onImageUpload: function (files) {
                uploadFile(files[0], $(this));
            }
        }
    });


    let rowCount = 1;

    // Add row function
    $('#addRowButton').on('click', function () {
        // Get values from the dropdowns
        const selectedUnit = $('#filter_unit').val();
        const selectedIntegrity = $('#well_integrity').val();

        // Validation: Check if the user selected a unit and integrity value
        if (!selectedUnit || !selectedIntegrity) {
            alert('Please select both a Unit and Well Integrity before adding a row.');
            return;
        }

        // Add row to the table
        const newRow = `
            <tr>
                <td>${rowCount++}</td>
                <td>${selectedUnit}</td>
                <td><input type="text" class="form-control" placeholder="Enter Job Title"></td>
                <td><input type="text" class="form-control" placeholder="Enter Workload"></td>
                <td>${selectedIntegrity}</td>
                <td>
                    <button type="button" class="btn btn-danger btn-sm" onclick="removeRow(this)">
                        <i class="fal fa-trash"></i>
                    </button>
                </td>
            </tr>
        `;

        // Append the row to the table body
        $('#dt_workload tbody').append(newRow);
    });

});
