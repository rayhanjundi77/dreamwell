﻿(function ($) {
    'use strict';
    var $dt_basic = $('#dt_basic');

    var pageFunction = function () {
        $('#btn_lookup_bu').click(function () {
            var btn = $(this);
            btn.button('loading');
            jQuery.ajax({
                type: 'POST',
                url: '/core/BusinessUnit/Lookup',
                success: function (data) {
                    btn.button('reset');
                    var $box = bootbox.dialog({
                        message: data,
                        title: "Choose Business Unit <small>Double click to selected this item</small>",
                        callback: function (e) {
                            console.log(e);
                        }
                    });
                    $box.on("onSelected", function (o,event) {
                        $box.modal('hide');
                    });
                }
            });
        });

        var loadDataTable = function () {
            var dt = $dt_basic.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/DrillingContractor/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },

                    { data: "contractor_name" },
                    { data: "contractor_address" },
                    { data: "contractor_email" },
                    { data: "phone_1" },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output =
                                    `<div class ="btn-group" data-id= "` + row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/core/DrillingContractor/detail?id=") + row.id + `" class="modalTrigger btn btn-info btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#modal_drilling_contractor">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-warning btn-xs btn-warning waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/drillingcontractor/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dt_basic.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
                console.log(e);
            });
            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.loading('start');
                if (processing) {
                    //$pageLoading.loading('start');
                } else {
                    //$pageLoading.loading('stop')
                }
            })
        }


        $(document).on('hidden.bs.modal', $dt_basic, function () {
            loadDataTable();
        });

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));