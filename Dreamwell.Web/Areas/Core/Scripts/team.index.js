﻿(function ($) {
    'use strict';
    var $dt_basic = $('#dt_team');
    var $form = $('#form-team');
    var $recordId = $('input[id=recordId]');

    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };

    var pageFunction = function () {
        var loadDataTable = function () {
            var dt = $dt_basic.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/Team/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        //return d;
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "team_name",
                        orderable: true,
                        searchable: true,
                        class: "text-left align-middle fw-500",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return `<span>` + row.team_name + `</span>`;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-center align-middle fw-500",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.is_active) {
                                    return `<label class="badge badge-success" title="Active" style="font-size: 11px;">Active</label>`;
                                } else {
                                    return `<label class="badge badge-danger" title="Active" style="font-size: 11px;">Non-Active</label>`;
                                }
                            }
                            return data;
                        }
                    },
                    {
                         data: "id",
                         orderable: false,
                         searchable: false,
                         class: "text-center",
                         render: function (data, type, row) {
                             var output;
                             output = `
                                <div class ="btn-group" data-id="`+ row.id + `" >
                                    <a class ="btn btn-default btn-hover-danger fa fa-users add-tooltip" href="`+ $.helper.resolve("~Core/Team/Member?id=" + row.id )+`"></a>
                                </div>
                                <div class ="btn-group" data-id="`+ row.id + `" >
                                    <a class ="btn btn-primary btn-hover-danger fa fa-braille add-tooltip" href="`+$.helper.resolve("~Core/Team/Role?id="+row.id)+`"></a>
                                </div>
                                <div class ="btn-group" data-id="`+ row.id + `" >
                                    <button class="btn btn-sm btn-warning waves-effect waves-themed row-deleted" style="width: 50px;"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                </div> `;
                             return output;
                         }
                     }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/team/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            loadDataTable();
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                //if (processing) {
                //    $pageLoading.niftyOverlay('show');
                //} else {
                //    $pageLoading.niftyOverlay('hide');
                //}
            })
        }

        $(document).on('hidden.bs.modal', "#setupModal", function () {
            loadDataTable();
        });

        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();

    });
}(jQuery));