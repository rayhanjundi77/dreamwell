﻿$(document).ready(function () {
    'use strict';
    var $form = $('#form-UpdateLeader');
    var $recordId = $('input[id=srecordId]');
    var $defaultTeam = $('input[id=defaultTeam]');

    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };




    var pageFunction = function () {

        var loadDetail = function () {
            if ($defaultTeam.val() !== '') {
                $.get($.helper.resolveApi('~/core/Team/' + $defaultTeam.val() + '/detail'), function (r) {
                    if (r.status.success) {
                        $.helper.form.fill($form, r.data);
                    }
                }).fail(function (r) {
                }).done(function () {

                });
            }
        };

        // FORM VALIDATION
        // =================================================================
        $("#team_leader").cmSelect2({
            url: $.helper.resolveApi('~/core/ApplicationUser/lookup'),
            result: {
                id: 'id',
                text: 'app_fullname'
            },
            filters: function (params) {
                return [{
                    field: "app_fullname",
                    operator: "contains",
                    value: params.term || '',
                }];
            },
            options: {
                dropdownParent: $form,
                placeholder: "Select a Team Leader",
                allowClear: true,
                //tags: true,
                multiple: false,
                //maximumSelectionLength: 1,
            }
        });

        $('#btnsave').on('click', function () {
            $('#msgerror').hide();
            var $teamleaderval = $("#team_leader").val();

            if ($teamleaderval == null) {
                $('#msgerror').show();
                return;
            }

            var btn = $(this);
            if ($recordId.val() != '') {
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/BusinessUnit/' + $recordId.val() + '/UpdateLeader?appuserid=' + $teamleaderval), function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            toastr.success(r.status.message)
                        } else {
                            btn.button('reset');
                            toastr.error(r.status.message)
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText)
                });

            }
            else return;
        });


        return {
            init: function () {
                loadDetail();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));

