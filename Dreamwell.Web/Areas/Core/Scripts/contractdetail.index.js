﻿(function ($) {
    'use strict';
    var $dt_listContract = $('#dt_listContract');
    var $pageLoading = $('#page-loading-content');
    var $contractId = $('input[id=recordId]');
    var $is_submitted = false;
    var $has_record = false;

    var pageFunction = function () {
        var hidebtn = function () {
            $.get($.helper.resolveApi('~/core/contract/' + $contractId.val() + '/detail'), function (r) {
                if (r.data.submitted_by == null && r.data.submitted_on == null) {
                    $("#btn_add").removeClass("d-none");
                    $("#btn_submitted").removeClass("d-none");
                } else {
                    $("#btn_add").remove();
                    $("#btn_submitted").remove();
                    $is_submitted = true;
                }
            })
        }
        var loadDataTable = function () {
            var dt = $dt_listContract.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/contractdetail/dataTableByContractId/" + $contractId.val()),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            if (data) {
                                $has_record = true;
                            }
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            console.log(data);
                            if (type === 'display') {
                                return row.line;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        searchable: true,
                        class: "text-left",
                        render: function (data, type, row) {
                            console.log(data);
                            if (type === 'display') {
                                return row.afe_line_description;
                            }
                            return data;
                        }
                    },
                    { data: "material_name" },
                    {
                        data: "unit",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return thousandSeparatorWithoutComma(Math.round(row.unit * 100) / 100);
                            }
                            return data;
                        }
                    },
                    {
                        data: "unit_price",
                        orderable: false,
                        searchable: false,
                        class: "text-right",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return row.currency_code + " " + thousandSeparatorWithoutComma(Math.round(row.unit_price * 100) / 100);
                            }
                            return data;
                        }
                    },
                    {
                        data: "total_price",
                        orderable: false,
                        searchable: false,
                        class: "text-right",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return row.currency_code + " " + thousandSeparatorWithoutComma(Math.round(row.total_price * 100) / 100);
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-right",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return thousandSeparatorWithoutComma(Math.round(row.current_rate_value * 100) / 100);
                            }
                            return data;
                        }
                    },
                    {
                        data: "actual_price",
                        orderable: false,
                        searchable: false,
                        class: "text-right fw-700",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return "USD " + thousandSeparatorWithoutComma(Math.round(row.actual_price * 100) / 100);
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output = ``;
                                if ($is_submitted) {
                                    output = ``;
                                } else {
                                    output =
                                        `<div class ="btn-group" data-id= "` + row.id + `" >
                                    <button onShowModal='true' data-href="` + $.helper.resolve("/core/contractdetail/detail?id=") + row.id + `&contractId='` + $contractId.val() + `'" class="modalTrigger btn btn-primary btn-xs btn-info waves-effect waves-themed"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' data-target="#contractModal">
                                        <span class="fal fa-pencil"></span>
                                    </button>
                                    <button class="btn btn-warning btn-xs btn-info waves-effect waves-themed row-deleted"
                                        data-toggle="modal-remote" data-loading-text='<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'>
                                        <span class="fal fa-trash"></span>
                                    </button>
                                </div>`;
                                }
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/contractdetail/deleteById/" + $contractId.val() + "/" + recordId),
                                        //data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            $dt_listContract.DataTable().ajax.reload();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);
                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    });
                }
            }, function (e, settings, json) {
                var $table = e; // table selector 
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            })
        }

        $('#btn_submitted').click(function (event) {
            if (!$has_record) {
                toastr.error("This contract doesn't have any details.");
                return;
            }

            Swal.fire({
                title: "",
                text: "Are you sure want to submit this contract?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    var btn = $(this);
                    var isvalidate = true;
                    if (isvalidate) {
                        event.preventDefault();
                        btn.button('loading');
                        $.post($.helper.resolveApi("~/core/contract/" + $contractId.val() + "/submit"), function (r) {
                            if (r.status.success) {
                                $("#btn_add").remove();
                                $("#btn_submitted").remove();
                                toastr.success("Contract has been submitted");
                            } else {
                                toastr.error(r.status.message)
                            }
                            btn.button('reset');
                        }, 'json').fail(function (r) {
                            btn.button('reset');
                            toastr.error(r.statusText);
                        });
                    } else {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                }
            });



            //$form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDataTable();
                hidebtn();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery)); 