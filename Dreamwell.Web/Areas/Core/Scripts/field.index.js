﻿(function ($) {
    'use strict';

    var $dt_field = $('#dt_field');
    var $pageLoading = $('#page-loading-content');
    var $FieldFormModal = $('#FieldModal');
    var $currencyUpdateModal = $('#currencyUpdateModal');
    var $form = $('#form-Field');
    var $formUpdate = $("#form-updateField");
    //var $btnhistory = $('#btn-history');
    var $btnDelete = $('#btn-delete');
    var $btnUpdateField = $('#btn-updateField');
    var $recordId = $('input[id=recordId]');

    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fal fa-check-circle fa-lg text-success',
        invalid: 'fal fa-times-circle fa-lg',
        validating: 'fal fa-refresh'
    };
    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            field_name: {
                validators: {
                    notEmpty: {
                        message: 'The Field Name is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION

    var pageFunction = function () {

        var loadDataTable = function () {


            var dt = $dt_field.cmDataTable({
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/Field/dataTable"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                columns: [
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }, 
                    },
                    { data: "field_name" },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class ="btn-group" data-id= "`+ row.id + `" >
                                    <a class ="row-edit btn btn-sm btn-info btn-hover-success fa fa-pencil add-tooltip" href="#"
                                        data-original-title="Edit" data-container="body">
                                    </a>
                                    <a class="row-deleted btn btn-sm btn-warning btn-hover-danger fa fa-trash add-tooltip" href="#"
                                        data-original-title="Delete" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        }, width: "10%"
                    }
                ],
                initComplete: function (settings, json) {
                    $(this).on('click', '.row-deleted', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        var b = bootbox.confirm({
                            message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                            buttons: {
                                confirm: {
                                    label: "Delete"
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                                    btnConfim.button('loading');
                                    $.ajax({
                                        type: "POST",
                                        dataType: 'json',
                                        contentType: 'application/json',
                                        url: $.helper.resolveApi("~/core/Field/delete"),
                                        data: JSON.stringify([recordId]),
                                        success: function (r) {
                                            if (r.status.success) {
                                                toastr.error("Data has been deleted");
                                                $dt_field.DataTable().ajax.reload();
                                            } else {
                                                toastr.error(r.status.message);
                                            }
                                            btnConfim.button('reset');
                                            b.modal('hide');
                                            reloadPlugin();
                                        },
                                        error: function (r) {
                                            toastr.error(r.statusText);

                                            b.modal('hide');
                                        }
                                    });
                                    return false;
                                }
                            },
                            animateIn: 'bounceIn',
                            animateOut: 'bounceOut'
                        });
                    }),
                        $(this).on('click', '.row-edit', function () {
                            
                            var recordId = $(this).closest('.btn-group').data('id');

                            $FieldFormModal.modal('show'), $form.loading('start');
                            $.helper.form.clear($form);
                        $.get($.helper.resolveApi('~/core/Field/' + recordId + '/detail'), function (r) {
                            
                                if (r.status.success) {
                                    $.helper.form.fill($form, r.data);
                                }
                                $form.loading('stop');
                            }).fail(function (r) {
                                toastr.error(r.statusText);
                                $form.loading('stop');
                            });

                        });
                }
            }, function (e, settings, json) {

                var $table = e; // table selector 
                console.log(e);
            });

            dt.on('processing.dt', function (e, settings, processing) {
                $pageLoading.loading('start');
                if (processing) {
                    $pageLoading.loading('start');
                } else {
                    $pageLoading.loading('stop')
                }
            });
        }


        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $('#btn-addNew').on('click', function () {
        $("#rate-value-form").show();
        $("#rate-value-form input").removeAttr("disabled");
        $("#btn-save").show();
        $("#btn-saveChanges").hide();
        $('.magic-checkbox').attr('checked', false);
        $FieldFormModal.modal('show');
    });

    $btnUpdateField.on('click', function () {
        $btnUpdateField.button('loading');

        var templateScript = $('#currency-template').html();
        var template = Handlebars.compile(templateScript);

        $.ajax({
            type: "GET",
            datatype: 'json',
            url: $.helper.resolveApi("~/core/Field/getAllField"),
            success: function (r) {
                console.log(r);
                if (r.status.success) {
                    var hasBaseRate = false;
                    $.each(r.data, function (key, value) {
                        if (value.base_rate) {
                            hasBaseRate = true;
                            $("#base_rate_name").text(value.currency_description + " (" + value.currency_symbol + ")");
                        }
                    });

                    if (hasBaseRate) {
                        $('#listField').html(template(r));
                    } else {
                        $("#base_rate_name").text("Please Choose 1 Base Rate");
                        $("#btn-saveUpdateField").hide();
                    }
                    $btnUpdateField.button('reset');
                } else {

                }
            }
        });

        $currencyUpdateModal.modal('show');
    });

    $('#btn-save').on('click', function () {
        
        var btn = $(this);
        var validator = $form.data('bootstrapValidator');
        validator.validate();
        if (validator.isValid()) {
            var data = $form.serializeToJSON();
            btn.button('loading');
            $.post($.helper.resolveApi('~/core/Field/save'), data, function (r) {
                if (r.status.success) {
                    $('input[name=id]').val(r.data.recordId);
                    toastr.success(r.status.message);
                    pageFunction.init();
                    $FieldFormModal.modal('hide');
                } else {
                    toastr.error(r.status.message)
                }
                btn.button('reset');
            }, 'json').fail(function (r) {
                btn.button('reset');
                toastr.error(r.statusText);
            });

        }
        else return;
    });
    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));