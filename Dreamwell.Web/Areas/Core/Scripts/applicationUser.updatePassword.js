﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=user_id]');
    var $form = $("#frm-changePassword");
    
    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };

    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            new_password: {
                enabled: false,
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    identical: {
                        field: 'app_password',
                        message: 'The password and its confirm must be the same'
                    }
                }
            },
            app_password: {
                enabled: false,
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and cannot be empty'
                    },
                    identical: {
                        field: 'new_password',
                        message: 'The password and its confirm must be the same'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });


    var pageFunction = function () {



        var loadDetail = function () {
            if ($recordId.val() !== '') {
                console.log('here');
                console.log($recordId.val());
            }
        };

        $('#btn-resetPassword').click(function (e) {
            var isvalidate = $form[0].checkValidity();
         //   alert(isvalidate);
            if (isvalidate) {

                var data = $form.serializeToJSON();


                var obj = new Object();
                obj.id = $('#user_id').val();
                obj.app_password = $('#app_password').val();  
                obj.app_username = $('#AppUsername').val();
                //convert object to json string
                var string = JSON.stringify(obj);
                var dataJSONWELL = JSON.parse(string);
               // console.log(string);
                $.post($.helper.resolveApi('~/core/ApplicationUser/updatePasssword'), dataJSONWELL, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message);
                     
                    } else {
                    
                        toastr.error(r.status.message);
                    }
                   
                }, 'json').fail(function (r) {
                    toastr.error(r.statusText);
                });

            }

            //var validator = $form.data('bootstrapValidator');
            //validator.validate();
            //if (validator.isValid()) {
            //    alert('valid');
            //} else {
            //    alert('not valid');
            //}
        });

        //$('#btn-save').click(function (event) {
        //    var $dtBasic = $('#dtBasic');
        //    var btn = $(this);
        //    var isvalidate = $form[0].checkValidity();
        //    if (isvalidate) {
        //        event.preventDefault();
        //        btn.button('loading');
        //        var data = $form.serializeToJSON();
        //        console.log(data);
        //        $.post($.helper.resolveApi('~/core/ApplicationMenuCategory/save'), data, function (r) {
        //            if (r.status.success) {
        //                $('input[name=id]').val(r.data.recordId);
        //                toastr.success(r.status.message)
        //                btn.button('reset');
        //                $dtBasic.DataTable().ajax.reload();
        //            } else {
        //                toastr.error(r.status.message)
        //            }
        //            btn.button('reset');
        //        }, 'json').fail(function (r) {
        //            btn.button('reset');
        //            toastr.error(r.statusText);
        //        });
        //    } else {
        //        event.preventDefault();
        //        event.stopPropagation();
        //    }
        //    $form.addClass('was-validated');
        //});

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});