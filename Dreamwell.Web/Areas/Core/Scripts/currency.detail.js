﻿(function ($) {
    'use strict';

    var $dt_listCurrency = $('#dt_listCurrency');
    var $pageLoading = $('#page-loading-content');
    var $currencyFormModal = $('#CurrencyModal');
    var $form = $('#form-Currency');
    //var $btnhistory = $('#btn-history');
    var $btnDelete = $('#btn-delete');
    var $recordId = $('input[id=recordId]');

    
    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };


    $currencyFormModal.niftyOverlay({
        iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
    });

    // FORM VALIDATION
    // =================================================================
    $form.bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: faIcon,
        fields: {
            currency_code: {
                validators: {
                    notEmpty: {
                        message: 'The currency code is required.'
                    }
                }
            }
        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    // END FORM VALIDATION


    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                $pageLoading.niftyOverlay('show');
                $.get($.helper.resolveApi('~core/CurrencyExchange/' + $recordId.val() + '/detail'), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        $.helper.form.fill($('#active-currency'), r.data);
                    }
                    $pageLoading.niftyOverlay('hide');
                }).fail(function (r) {
                    $.helper.noty.error(r.status, r.statusText);
                    $pageLoading.niftyOverlay('hide');
                });
            }
        }

        $('#demo-dp-range').datepicker({
            format: "yyyy/mm/dd",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        }).on('changeDate show', function (e) {
            // Revalidate the date when user change it
            $form.bootstrapValidator('revalidateField', 'start_date');
            $form.bootstrapValidator('revalidateField', 'end_date');
        });

        var loadDataTable = function () {
            if ($recordId.val() !== '')
            {
                loadDetail();
                var dt = $dt_listCurrency.cmDataTable({
                    pageLength: 10,
                    ajax: {
                        url: $.helper.resolveApi("~/core/CurrencyExchange/dataTable/" + $recordId.val()),
                        type: "POST",
                        contentType: "application/json",
                        data: function (d) {
                            return JSON.stringify(d);
                        }
                    },
                    columns: [
                        {
                            data: "id",
                            orderable: false,
                            searchable: false,
                            class: "text-center",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { data: "rate_value" },
                        { data: "start_date" },
                        { data: "end_date" },
                        {
                             data: "is_active",
                             orderable: false,
                             searchable: false,
                             class: "text-center",
                             render: function (data, type, row) {
                                 if (type === 'display') {
                                     if (row.is_active == true)
                                     {
                                         return "<span class=\"label label-success\">Active</span>";
                                     }
                                     else {

                                         return "<span class=\"label label-danger\">Non Active</span>";
                                     }
                                 }
                                 return data;
                             }
                         }

                     
                    ],
                    initComplete: function (settings, json) {
                        //$(this).on('click', '.row-deleted', function () {
                        //    var recordId = $(this).closest('.btn-group').data('id');
                        //    var b = bootbox.confirm({
                        //        message: "<p class='text-semibold text-main'>Are your sure ?</p><p>You won't be able to revert this!</p>",
                        //        buttons: {
                        //            confirm: {
                        //                label: "Delete"
                        //            }
                        //        },
                        //        callback: function (result) {
                        //            if (result) {
                        //                var btnConfim = $(document).find('.bootbox.modal.bootbox-confirm.in button[data-bb-handler=confirm]:first');
                        //                btnConfim.button('loading');
                        //                $.ajax({
                        //                    type: "POST",
                        //                    dataType: 'json',
                        //                    contentType: 'application/json',
                        //                    url: $.helper.resolveApi("~/core/Currency/delete"),
                        //                    data: JSON.stringify([recordId]),
                        //                    success: function (r) {
                        //                        if (r.status.success) {
                        //                            $.helper.noty.success("Successfully", "Data has been deleted");
                        //                            dt.ajax.reload();
                        //                        } else {
                        //                            $.helper.noty.error("Information", r.status.message);
                        //                        }
                        //                        btnConfim.button('reset');
                        //                        b.modal('hide');
                        //                        reloadPlugin();
                        //                    },
                        //                    error: function (r) {
                        //                        $.helper.noty.error(r.status, r.statusText);
                        //                        b.modal('hide');
                        //                    }
                        //                });
                        //                return false;
                        //            }
                        //        },
                        //        animateIn: 'bounceIn',
                        //        animateOut: 'bounceOut'
                        //    });
                        //}),
                        //$(this).on('click', '.row-edit', function () {

                        //    var recordId = $(this).closest('.btn-group').data('id');

                        //    //$currencyFormModal.niftyOverlay('show'),
                        //    $currencyFormModal.modal('show'),
                        //        $.helper.form.clear($form),
                        //        $.get($.helper.resolveApi('~core/Currency/' + recordId + '/detail'), function (r) {
                        //            if (r.status.success) {
                        //                $.helper.form.fill($form, r.data);
                        //            }
                        //            $currencyFormModal.niftyOverlay('hide');
                        //        }).fail(function (r) {
                        //            $.helper.noty.error(r.status, r.statusText);
                        //            $currencyFormModal.niftyOverlay('hide');
                        //        });

                        //});
                    }
                }, function (e, settings, json) {

                    var $table = e; // table selector 

                });

                dt.on('processing.dt', function (e, settings, processing) {
                    $pageLoading.niftyOverlay('hide');
                    if (processing) {
                        $pageLoading.niftyOverlay('show');
                    } else {
                        $pageLoading.niftyOverlay('hide');
                    }
                })
            }
        }

        $('#btn-addNew').on('click', function () {
            $currencyFormModal.modal('show');
        });
       

        $('#btn-save').on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~core/CurrencyExchange/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            $.helper.noty.success("Successfully", r.status.message);
                            $.helper.form.fill($('#active-currency'), r.data);
                            loadDataTable();
                        } else {
                            btn.button('reset');
                            $.helper.noty.error("Information", r.status.message);
                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    $.niftyNoty({
                        type: 'warning',
                        container: 'floating',
                        html: '<h4 class="alert-title">' + r.status + '</h4><p class="alert-message">' + r.statusText + '</p>',
                        closeBtn: true,
                        floating: {
                            position: 'top-right',
                            animationIn: 'bounceInRight',
                            animationOut: 'bounceOutRight'
                        },
                        focus: true,
                        timer: 0
                    });
                    $.helper.noty.error(r.status, r.statusText);
                });

            }
            else return;
        });


        return {
            init: function () {
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));