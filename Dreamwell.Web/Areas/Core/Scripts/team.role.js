﻿(function ($) {
    'use strict';
    //$('#container').addClass("aside-bright aside-bright aside-left aside-fixed aside-in ");
    $('#container').addClass("page-fixedbar");

    var $userListContent = $('#page-user-content');
    var pagination = $('#pagination');

    var $recordId = $('input[id=recordId]');
    var $dt_basic = $('#dt_basic');
    var dt = undefined;
    var $form = $('#form-applicationUser');
    var $pageLoading = $('#page-loading');
    var $btnSave = $('#btn-save');
    var $btnDelete = $('#btn-delete');


    // FORM VALIDATION FEEDBACK ICONS
    // =================================================================
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    }

    var pageFunction = function () {


        // Content OverLay
        //$pageLoading.niftyOverlay({
        //    iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
        //});


        var loadDataTable = function () {

            dt = $dt_basic.cmDataTable({
                searchDelay: 1000,
                pageLength: 10,
                ajax: {
                    url: $.helper.resolveApi("~/core/TeamRole/" + $recordId.val() + "/member"),
                    type: "POST",
                    contentType: "application/json",
                    data: function (d) {
                        return JSON.stringify(d);
                    }
                },
                order: [[1, "asc"]],
                columns: [
                    /*BEGIN::row number*/
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    /*END::row number*/
                    {
                        data: "role_name",
                        orderable: true,
                        class: "list-group-item",
                        render: function (data, type, row) {
                            
                            if (type === 'display') {
                                var output;
                                var img = (row.gender == true ? "/Content/img/profile-photos/5.png" : "/Content/img/profile-photos/8.png");
                                output = `
	                                        <div class="media-body">
		                                        <p class="mar-no text-main">`+ row.role_name + `</p>
	                                        </div>`;
                                return output;
                            }
                            return data;
                        }
                    },
                    {
                        data: "id",
                        orderable: true,
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (row.is_active) {
                                    return `<label class="badge badge-success" title="Active" style="font-size: 11px;">Active</label>`;
                                } else {
                                    return `<label class="badge badge-danger" title="Active" style="font-size: 11px;">Non-Active</label>`;
                                }
                            }
                        }
                    },
                    {
                        data: "id",
                        orderable: false,
                        searchable: false,
                        class: "text-center",
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var output;
                                output = `
                                <div class="btn-group" data-id="`+ row.id + `">
                                    <a class="row-deleted btn btn-sm btn-warning btn-hover-danger fa fa-trash add-tooltip" href="#"
                                        data-original-title="Delete" data-container="body">
                                    </a>
                                </div>`;
                                return output;
                            }
                            return data;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $('.row-deleted').on('click', function () {
                        var recordId = $(this).closest('.btn-group').data('id');
                        $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            url: $.helper.resolveApi("~/core/TeamRole/removeTeamRole"),
                            data: JSON.stringify([recordId]),
                            success: function (r) {
                                if (r.status.success) {
                                    toastr.success("Data has been deleted")
                                    loadDataTable();
                                } else {
                                    toastr.error(r.status.message)
                                }
                            },
                            error: function (r) {
                                toastr.error(r.statusText)
                                b.modal('hide');
                            }
                        });
                    });
                }
            });

            dt.on('processing.dt', function (e, settings, processing) {
                //$pageLoading.niftyOverlay('hide');
                if (processing) {
                    //$pageLoading.niftyOverlay('show');
                } else {
                    //$pageLoading.niftyOverlay('hide');
                }
            });
        }

        pagination.data('twbs-pagination.js', null);

        var templateScript = $("#member-template").html();
        var template = Handlebars.compile(templateScript);
        //pagination.niftyOverlay({
        //    iconClass: 'demo-psi-repeat-2 spin-anim icon-2x'
        //});
        var pagePlugin = function (options) {
            var o = $userListContent;
            var dataSource = {
                pageSize: 3,
                page: 1,
                filter: {
                    logic: "OR",
                    filters: [
                        {
                            field: "role_name",
                            operator: "contains",
                            value: $('input[name=term]').val()
                        }
                        
                    ]
                }
            }
            options.dataSource = $.extend({}, dataSource, options.dataSource);
            var load = function (opt) {
                //pagination.niftyOverlay('show');
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: opt.url,
                    data: JSON.stringify(opt.dataSource),
                    success: function (data) {
                        console.log(data);
                        if (data.items.length > 0) {
                            o.html(template(data));
                            $('.row-selected').click(function () {
                                var recordId = $(this).data("id");
                                addTeam(recordId);
                                loadDataTable();
                            });
                        } else {
                            o.empty();
                        }

                        //pagination.niftyOverlay('hide');
                    },
                    error: function (err) {
                        console.error(err);
                        //pagination.niftyOverlay('hide');
                    }
                });
            };
            
            var deleteTeam = function (id) {
                
                return false;
            };

            var addTeam = function (roleId) {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    url: $.helper.resolveApi('~/core/TeamRole/addTeamRole'),
                    data: JSON.stringify({
                        team_id: $recordId.val(),
                        application_role_id: roleId
                    }),
                    success: function (r) {
                        if (r.status.success) {
                            dt.ajax.reload(null, false);
                            reloadPlugin();
                        } else {
                            bootbox.alert({
                                message: "<p class='text-semibold text-main'>Information</p><p>" + r.status.message + "</p>",
                                callback: function (result) {
                                    //Callback function here
                                },
                                animateIn: 'bounceIn',
                                animateOut: 'bounceOut'
                            });
                        }

                    },
                    error: function (err) {
                        console.error(err);
                    }
                });

            }


            load(options);
            return {
                refresh: function (opt) {
                    load(opt);
                }
            }
        };
        var reloadPlugin = function () {
            pagePlugin({
                url: $.helper.resolveApi("~/core/Team/" + $recordId.val() + "/roleAvailable"),
                dataSource: {
                    pageSize: 100
                }
            });
        }

        $('#btn-search-user').click(function () {
            reloadPlugin();
        });

        /*BEGIN::Event*/
        $btnSave.on('click', function () {
            var btn = $(this);
            var validator = $form.data('bootstrapValidator');
            validator.validate();
            if (validator.isValid()) {
                var data = $form.serializeToJSON();
                btn.button('loading');
                $.post($.helper.resolveApi('~core/ApplicationRole/save'), data, function (r) {
                    setTimeout(function () {
                        if (r.status.success) {
                            history.pushState('', 'ID', location.hash.split('?')[0] + '?id=' + r.data.recordId);
                            $('input[name=id]').val(r.data.recordId);
                            toastr.success(r.status.message)

                            //loadDetail();
                        } else {
                            btn.button('reset');
                            toastr.success(r.status.message)

                        }
                        btn.button('reset');
                    }, 2000);
                }, 'json').fail(function (r) {
                    toastr.error(r.statusText)
                });



            }
            else return;
        });

        /*END::Event*/

        return {
            init: function () {
               // loadDetail();
                reloadPlugin();
               //,
                   // loadDetail(),
                loadDataTable();
            }
        }
    }();

    $(document).ready(function () {
        pageFunction.init();
    });
}(jQuery));