$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=wellBhaId]');
    var $form = $("#form-wellBHA")

    var lastRowBhaComponent = 0;
    var $btnSaveBHA = $("#btn-saveBHA");
    var $formWellBHA = $("#form-wellBHA");
    var $formWellBHAComponent = $("#form-wellBhaComponent");

    var tb_new_bha = $("#tableWellBhaComponent > tbody");

    var pageFunction = function () {
        var maskingMoney = function () {
            $(".numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
            $(".numeric").inputmask({
                digits: 0,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });
        }

        var bhaComponentLookup = function (id) {
            var $element;
            if (id == null || id == "") {
                $element = $(".lookup-bha-component");
            } else {
                $element = $("#" + id);
            }
            $element.cmSelect2({
                url: $.helper.resolveApi('~/core/bhacomponent/lookup'),
                result: {
                    id: 'id',
                    text: 'component_name'
                },
                filters: function (params) {
                    return [{
                        field: "component_name",
                        operator: "contains",
                        value: params.term || '',
                    }];
                },
                options: {
                    dropdownParent: $form,
                    placeholder: "Select a BHA Component",
                    allowClear: true,
                    //tags: true,
                    //multiple: true,
                    maximumSelectionLength: 1,
                }
            });
        }

        $("#cloneBhaComponent").on('click', function () {
            var templateScript = $("#rowBhaComponent-template").html();
            var template = Handlebars.compile(templateScript);

            $('#tableWellBhaComponent > tbody tr.no-data').remove();
            $("#tableWellBhaComponent > tbody").append(template);

            $('#tableWellBhaComponent > tbody tr:last.rowBhaComponent > .select.p-1 > select.lookup-bha-component ').attr("id", "bha_component_id-" + lastRowBhaComponent);

            $("#tableWellBhaComponent > tbody tr:last.rowBhaComponent > td > input.numeric-money").inputmask({
                digits: 2,
                greedy: true,
                definitions: {
                    '*': {
                        validator: "[0-9]"
                    }
                },
                rightAlign: false
            });

            bhaComponentLookup("bha_component_id-" + lastRowBhaComponent);
            lastRowBhaComponent++;

            //$('.row-delete').on('click', function () {
            //    $(this).closest('.rowBhaComponent').remove();
            //});

            $(".calculate-length").on('input change keyup', function () {
                recalculateBHA();
            });
        })


        $(document).on('click', '.row-delete', function () {
            var element = this;
            var id = $(element).data().id;
            Swal.fire({
                title: "",
                text: "Are you sure want to delete BHA?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    $(element).closest('.rowBhaComponent').remove();
                    if (id) {
                        $.post($.helper.resolveApi('~/core/wellbha/deletecomponent/' + id), null, function (r) {
                            if (r.status.success) {
                                toastr.success(r.status.message)
                                $(element).closest('.rowBhaComponent').remove();
                            } else {
                                toastr.error(r.status.message)
                            }
                        }, 'json').fail(function (r) {
                            btn.button('reset');
                            toastr.error(r.statusText);
                        });
                    }

                }
            });
        })
        var recalculateBHA = function () {
            if (tb_new_bha.children().length > 0) {
                var i = 0;
                var cummLength = 0;

                tb_new_bha.children().each(function () {
                    var currentLength = $(this).find("input[name*='length']").val().replace(/,/g, '');

                    console.log("kikiki",currentLength);
                    if (i == 0) {
                        cummLength = currentLength;
                    } else {
                        cummLength = parseFloat(cummLength) + parseFloat(currentLength);
                    }


                    if (!isNaN(cummLength)) {
                        $(this).find("input[name*='cumm_length']").val(parseFloat(cummLength).toFixed(2));
                    } else {
                        $(this).find("input[name*='cumm_length']").val("");
                    }
                    i++;
                });
            }
        }

        var saveBHA = function () {
            var btn = $btnSaveBHA;
            var isValid = true;

            var data = new Object();
            data.drilling_id = $recordId.val();
            data.well_bha = $formWellBHA.serializeToJSON();
            data.well_bha_component = new Object();
            data.is_new = true;

            var isValidateWellBha = $formWellBHA[0].checkValidity();
            var isValidateWellBhaComponents = $formWellBHAComponent[0].checkValidity();

            if (!isValidateWellBha) {
                $formWellBHA.addClass('was-validated');
                isValid = false;
            }

            if (!isValidateWellBhaComponents) {
                $formWellBHAComponent.addClass('was-validated');
                isValid = false;
            }


            if (isValid) {
                var components = [];
                var total = document.getElementsByName('well_bha_comp_detail_id[]').length;
                for (var i = 0; i < total; i++) {
                    var id = document.getElementsByName('well_bha_comp_detail_id[]')[i].value;
                    var bha_component_id = document.getElementsByName('bha_component_id[]')[i].value;
                    var component_detail = document.getElementsByName('component_detail[]')[i].value;
                    var joint = document.getElementsByName('joint[]')[i].value;
                    var n_od = document.getElementsByName('n_od[]')[i].value;
                    var n_id = document.getElementsByName('n_id[]')[i].value;
                    var weight = document.getElementsByName('weight[]')[i].value;
                    var length = document.getElementsByName('length[]')[i].value;
                    var cumm_length = document.getElementsByName('cumm_length[]')[i].value;
                    var con_type = document.getElementsByName('con_type[]')[i].value;
                    var con_size = document.getElementsByName('con_size[]')[i].value;
                    var serial_number = document.getElementsByName('serial_number[]')[i].value;
                    components.push({
                        id: id,
                        bha_component_id: bha_component_id,
                        component_detail: component_detail,
                        joint: joint,
                        n_od: n_od,
                        n_id: n_id,
                        weight: weight,
                        length: length,
                        cumm_length: cumm_length,
                        con_type: con_type,
                        con_size: con_size,
                        serial_number: serial_number,
                    });
                }
                data.well_bha_component = components;
            }

            if (isValid) {
                btn.button('loading');
                $.post($.helper.resolveApi('~/core/wellbha/save'), data, function (r) {
                    if (r.status.success) {
                        toastr.success(r.status.message)
                        btn.button('reset');
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                toastr.error("Please complete all Drilling BHA and BHA components fields.");
                event.preventDefault();
                event.stopPropagation();
            }



        }

        $btnSaveBHA.on('click', function () {
            Swal.fire({
                title: "",
                text: "Are you sure want to create a new BHA?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes"
            }).then(function (result) {
                if (result.value) {
                    saveBHA();
                }
            });
        })

        var loadDetail = function () {
            if ($recordId.val() !== '') {
                console.log($recordId.val());
                // $btnSaveBHA.hide();
                $.get($.helper.resolveApi('~/core/wellbha/getDetail/' + $recordId.val()), function (r) {
                    if (r.status.success) {
                        GetWellObjectUomMap(r.data.well_id);
                        $.helper.form.fill($form, r.data);
                        GetWellBhaComponents(r.data.id);
                    }
                    //$("#bha-component-action-col").hide();
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            } else {
                $btnSaveBHA.show();
            }
        };

        var GetWellBhaComponents = function (wellBhaId) {
            var templateScript = $("#readBhaComponent-template").html();
            var template = Handlebars.compile(templateScript);
            $.get($.helper.resolveApi('~/core/wellbhacomponent/GetByWellBha/' + wellBhaId), function (r) {
                if (r.status.success) {
                    //console.log("Well Bha Components");
                    //console.log(r);
                    $("#tableWellBhaComponent > tbody").html(template({ data: r.data }));
                    bhaComponentLookup();
                    maskingMoney();
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
            }).done(function () {

            });
        }

        var GetWellObjectUomMap = function (wellid) {
            $.get($.helper.resolveApi('~/core/wellobjectuommap/GetAllByWellId/' + wellid), function (r) {
                if (r.status.success && r.data.length > 0) {
                    //console.log("--GetWellObjectUomMap--");
                    //console.log(r);
                    $("form#form-wellBHA :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val(value.uom_code);
                                }
                            });
                        }
                    });
                    $("form#form-wellBhaComponent :input.uom-definition").each(function () {
                        var objectName = $(this).data("objectname"); // this is the jquery object of the input, do what you will
                        var elementId = $(this)[0].id;
                        if (objectName != undefined) {
                            $.each(r.data, function (key, value) {
                                if (objectName.toLowerCase().trim() == (value.object_name || '').toLowerCase().trim()) {
                                    $("#" + elementId).val("(" + value.uom_code + ")");
                                }
                            });
                        }
                    });
                }
                $('.loading-detail').hide();
            }).fail(function (r) {
                //console.log(r);
            }).done(function () {

            });
        }

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});