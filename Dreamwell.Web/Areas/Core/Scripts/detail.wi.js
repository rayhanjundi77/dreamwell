$(document).ready(function () {
    'use strict';
    console.log('ini js');
    var $formMOC = $('#form-moc');
    var $btnSaveMOC = $('#btn-savemoc');
    var $recordId = 'da8029c2-4e49-4ec8-9592-cf7c4c2768fb';
    var getWipData = function () {
        $.get($.helper.resolveApi('~/core/wip/' + $recordId + '/detail'), function (r) {
                console.log('ini suu', r.data);
                $('#wsp_no').val(r.data.wip_no);
                $('#wsp_approval_date').val(r.data.created_on);
                $('#field').val(r.data.field);
                $('#well_name').val(r.data.well_name);
                $('#string').val(r.data.string);
                $('#main_job').val(r.data.main_job);
                getMocData(r.data.id);
        }, 'json').fail(function (r) {
            toastr.error('r.statusText');
        });
    };
    getWipData();
    console.log('gelo');
    var getMocData = function (id) {
        console.log('adsadsa', id);
        $.get($.helper.resolveApi('~/core/moc/' + id + '/detailbyId'), function (r) {
            if (r.status.success) {
                console.log('ini suuss', r.data[0]);
                $('#no_1').val(r.data[0].last_approved_program);
                $('#no_2').val(r.data[0].reason_for_change);
                $('#no_3').val(r.data[0].recomendation_for_change);
                $('#no_4').val(r.data[0].risk_evaluation);
                $('#no_5').val(r.data[0].new_program);
                getApprovalData(r.data[0].id);
            } else {
                toastr.error(r.status.message);
            }
        }, 'json').fail(function (r) {
            toastr.error(r.statusText);
        });
    }
    $btnSaveMOC.click(function (event) {
        const isValid = $formMOC[0].checkValidity();
        if (isValid) {
            event.preventDefault();

            // Disable button and show loading state
            $btnSaveMOC.prop('disabled', true).text('Saving...');

            // Serialize form data to JSON
            const data = $formMOC.serializeArray().reduce((obj, item) => {
                obj[item.name] = item.value;
                return obj;
            }, {});
            data.wip_id = $recordId;
            console.log('Serialized Data:', data);

            // Post data to server
            $.post($.helper.resolveApi('~/core/wip/moc/save'), data, function (r) {
                if (r.status.success) {
                    $('input[name=id]').val(r.data.recordId);
                    toastr.success(r.status.message);
                } else {
                    toastr.error(r.status.message);
                }
                $btnSaveMOC.prop('disabled', false).text('Save');
            }, 'json').fail(function (r) {
                toastr.error(r.statusText);
                $btnSaveWIP.prop('disabled', false).text('Save');
            });
            
        } else {
            event.preventDefault();
            event.stopPropagation();
            $btnSaveMOC.addClass('was-validated');
        }
    });

    $('#btn-approvedmocA').on('click', async function () {
        var $row = $(this).closest('tr');
        var departement = $row.find('input[name="departement"]').val(); 
        var approverName = $row.find('input[name="approver_name"]').val();  
        var approverDate = $row.find('input[name="approver_date"]').val();
        var data = {
            departement: departement,
            approver_name: approverName,
            approved_date: approverDate,
            moc_id: 'e2d10c1a-4d37-4173-a753-2ec12d967a84'
        }
        console.log('Button A', data);
        saveApprovalMoc(data);
    });

    $('#btn-approvedmocB').on('click', async function () {
        var $row = $(this).closest('tr');
        var departement = $row.find('input[name="departement"]').val();
        var approverName = $row.find('input[name="approver_name"]').val();
        var approverDate = $row.find('input[name="approver_date"]').val();
        var data = {
            departement: departement,
            approver_name: approverName,
            approved_date: approverDate,
            moc_id: 'e2d10c1a-4d37-4173-a753-2ec12d967a84'
        }
        console.log('Button B', data);
        saveApprovalMoc(data);
    });
    $('#btn-approvedmocC').on('click', async function () {
        var $row = $(this).closest('tr');
        var departement = $row.find('input[name="departement"]').val();
        var approverName = $row.find('input[name="approver_name"]').val();
        var approverDate = $row.find('input[name="approver_date"]').val();
        var data = {
            departement: departement,
            approver_name: approverName,
            approved_date: approverDate,
            moc_id: 'e2d10c1a-4d37-4173-a753-2ec12d967a84'
        }
        console.log('Button B', data);
        saveApprovalMoc(data);
    });

    var saveApprovalMoc = function (data) {
        console.log('Result', data);
        $.post($.helper.resolveApi('~/core/moc/approver/save'), data, function (r) {
            if (r.status.success) {
                console.log('as', r);
                toastr.success(r.status.message);
            } else {
                toastr.error(r.status.message);
            }
            $btnSaveMOC.prop('disabled', false).text('Save');
        }, 'json').fail(function (r) {
            toastr.error(r.statusText);
            $btnSaveWIP.prop('disabled', false).text('Save');
        });
    }
    var getApprovalData = function (id) {
        $.get($.helper.resolveApi('~/core/moc/approve/' + id + '/detailbyId'), function (r) {
            if (r.status.success) {
                console.log('ini approve', r.data);

                if (r.data == null) {
                    $('#btn-approvedmocB').replaceWith('<span class="text-center">Not Submitted</span>');
                    $('#btn-approvedmocC').replaceWith('<span class="text-center">Not Submitted</span>');
                } else {
                    var latestData = r.data[r.data.length - 1];
                    getCurrentStatus(latestData); // Update current status based on the latest data

                    if (r.data[0]) {
                        $('#departement_a').val(r.data[0].departement).prop('disabled', true);
                        $('#approver_name_a').val(r.data[0].approver_name).prop('disabled', true);
                        if (r.data[0].approved_date) {
                            var approverDate = new Date(r.data[0].approved_date);
                            var formattedDate = approverDate.toISOString().split('T')[0]; // Format YYYY-MM-DD
                            $('#approver_date_a').val(formattedDate).prop('disabled', true);
                        }
                        $('#btn-approvedmocA').replaceWith('<span class="text-center text-success">Submitted</span>');
                    }
                    if (r.data[1]) {
                        $('#departement_b').val(r.data[1].departement).prop('disabled', true);
                        $('#approver_name_b').val(r.data[1].approver_name).prop('disabled', true);
                        if (r.data[1].approved_date) {
                            var approverDate = new Date(r.data[1].approved_date);
                            var formattedDate = approverDate.toISOString().split('T')[0]; // Format YYYY-MM-DD
                            $('#approver_date_b').val(formattedDate).prop('disabled', true);
                        }
                        $('#btn-approvedmocB').replaceWith('<span class="text-success">Submitted</span>');

                        // Jika data[1] ada, tombol btn-approvedmocC kembali menjadi tombol
                        $('#btn-approvedmocC').replaceWith('<button class="btn btn-info" id="btn-approvedmocC">Submit</button>');
                    }
                    if (r.data[2]) {
                        $('#departement_c').val(r.data[2].departement).prop('disabled', true);
                        $('#approver_name_c').val(r.data[2].approver_name).prop('disabled', true);
                        if (r.data[2].approved_date) {
                            var approverDate = new Date(r.data[2].approved_date);
                            var formattedDate = approverDate.toISOString().split('T')[0]; // Format YYYY-MM-DD
                            $('#approver_date_c').val(formattedDate).prop('disabled', true);
                        }
                        $('#btn-approvedmocC').replaceWith('<span class="text-success">Submitted</span>');
                    }
                }
            } else {
                toastr.error(r.status.message);
            }
        }, 'json').fail(function (r) {
            toastr.error(r.statusText);
        });
    };

    var getCurrentStatus = function (data) {
        console.log('ini latest', data);
        if (data) {
            $('#created_by').val(data.created_by).prop('disabled', true);
            $('#submited_by').val(data.approver_name).prop('disabled', true);
            $('#current_status').val('Approve Moc is already in ' + data.approver_name).prop('disabled', true);
        } else {
            console.log('No Data');
        }
    };

    $('#btn-updatewsp').on('click', async function () {
        // Mengambil nilai dari textarea, bukan input
        var objective = $('textarea[name="objective_wsp"]').val();
        var introduction = $('textarea[name="introduction_wsp"]').val();
        var workload = $('textarea[name="workload_wsp"]').val();
        var description = $('textarea[name="description_wsp"]').val();

        // Menyusun data yang akan dikirim
        var data = {
            objective: objective,
            introduction: introduction,
            workload: workload,
            description: description,
            departement: departement,
            approver_name: approverName,
            approved_date: approverDate,
            moc_id: 'e2d10c1a-4d37-4173-a753-2ec12d967a84'
        };

        // Tampilkan data untuk debugging
        console.log('Button B', data);
    });

       
});
