﻿$(document).ready(function () {
    'use strict';
    var $recordId = $('input[id=recordId]');
    var $form = $("#formMaterial")
    var $afeLineId = "";

    //-- Lookup
    $("#material_type").val('');
    $("#material_type").select2({
        dropdownParent: $form,
        placeholder: "Select a Material Type",
        allowClear: true,
        maximumSelectionLength: 1,
    });
    $("#item_type").val('');
    $("#item_type").select2({
        dropdownParent: $form,
        placeholder: "Select a Material Category",
        allowClear: true,
        maximumSelectionLength: 1,
    });

    $('#btn-afe-lookup').click(function () {
        var btn = $(this);
        btn.button('loading');
        jQuery.ajax({
            type: 'POST',
            url: '/core/afeline/Lookup',
            success: function (data) {
                btn.button('reset');
                $('input[name=afe_line_id]').val('');
                var $box = bootbox.dialog({
                    message: data,
                    title: "Choose a Code <small>Double click an item below to selected as a parent</small>",
                    callback: function (e) {
                        console.log(e);
                    }
                });
                $box.on("onSelected", function (o, event) {
                    $box.modal('hide');
                    $('#btn-afe-lookup').text(event.node.text);
                    $('input[name=afe_line_id]').val(event.node.id);
                    $afeLineId = event.node.id;
                });
            }
        });
    });

    $("#uom_id").cmSelect2({
        url: $.helper.resolveApi('~/core/uom/lookup'),
        result: {
            id: 'id',
            text: 'description',
            uom_code: 'uom_code'
        },
        filters: function (params) {
            return [{
                field: "description",
                operator: "contains",
                value: params.term || '',
            }];
        },
        options: {
            dropdownParent: $form,
            placeholder: "Select a Unit of Measurement",
            allowClear: true,
            //tags: true,
            //multiple: true,
            maximumSelectionLength: 1,
            templateResult: function (repo) {
                return repo.text + ` (` + repo.uom_code + `)`;
            },
        }
    });

    var pageFunction = function () {
        var loadDetail = function () {
            if ($recordId.val() !== '') {
                console.log($recordId.val());
                $.get($.helper.resolveApi('~/core/material/' + $recordId.val() + '/detail'), function (r) {
                    console.log(r);
                    if (r.status.success) {
                        $('#btn-afe-lookup').text(r.data.afe_line_name);
                        $.helper.form.fill($form, r.data);

                        if (r.data.fl_active) {
                            $("#fl_active").attr("checked", "checked");
                        }
                    }
                    $('.loading-detail').hide();
                }).fail(function (r) {
                    //console.log(r);
                }).done(function () {

                });
            }
        };

        $('#btn-save').click(function (event) {
            var $dt_Material = $('#dt_listMaterial');
            var btn = $(this);
            var isvalidate = $form[0].checkValidity();

            console.log($form[0]);
            console.log($form[0].checkValidity);

            //if ($afeLineId == "") {
            //    toastr.error("Please choose a AFE Line");
            //    return;
            //}

            if (isvalidate) {
                event.preventDefault();
                btn.button('loading');
                var data = $form.serializeToJSON();
                data.afe_line_id = $afeLineId;
                $.post($.helper.resolveApi('~/core/material/save'), data, function (r) {
                    if (r.status.success) {
                        $('input[name=id]').val(r.data.recordId);
                        toastr.success(r.status.message)
                        btn.button('reset');
                        $dt_Material.DataTable().ajax.reload();
                    } else {
                        toastr.error(r.status.message)
                    }
                    btn.button('reset');
                }, 'json').fail(function (r) {
                    btn.button('reset');
                    toastr.error(r.statusText);
                });
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
            $form.addClass('was-validated');
        });

        return {
            init: function () {
                loadDetail();
            }
        }
    }();


    $(document).ready(function () {
        pageFunction.init();
    });


});