﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Identity.Controllers
{
    public class CaptchaController : Controller
    {
        [HttpGet]
        public ActionResult GenerateCaptcha()
        {
            // Generate captcha code
            string captchaCode = GenerateCaptchaCode();
            Session["CaptchaCode"] = captchaCode;

            // Generate captcha image
            byte[] captchaImage = GenerateCaptchaImage(captchaCode);

            // Return the image as PNG
            return File(captchaImage, "image/png");
        }

        private string GenerateCaptchaCode()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            char[] captcha = new char[5];
            for (int i = 0; i < captcha.Length; i++)
            {
                captcha[i] = chars[random.Next(chars.Length)];
            }
            return new string(captcha);
        }

        private byte[] GenerateCaptchaImage(string captchaCode)
        {
            using (Bitmap bitmap = new Bitmap(150, 50))
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                // Set background color
                graphics.Clear(Color.LightGray);

                // Draw the captcha text
                using (Font font = new Font("Arial", 24, FontStyle.Bold))
                {
                    graphics.DrawString(captchaCode, font, Brushes.Blue, 10, 10);
                }

                // Add noise (optional)
                AddNoise(graphics, bitmap.Width, bitmap.Height);

                // Save the image as a byte array
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    bitmap.Save(memoryStream, ImageFormat.Png);
                    return memoryStream.ToArray();
                }
            }
        }

        private void AddNoise(Graphics graphics, int width, int height)
        {
            Random random = new Random();

            // Add random lines
            for (int i = 0; i < 10; i++)
            {
                var start = new Point(random.Next(width), random.Next(height));
                var end = new Point(random.Next(width), random.Next(height));
                graphics.DrawLine(Pens.Gray, start, end);
            }

            // Add random dots
            for (int i = 0; i < 50; i++)
            {
                var x = random.Next(width);
                var y = random.Next(height);
                graphics.FillRectangle(Brushes.Black, x, y, 2, 2);
            }
        }
    }
}
