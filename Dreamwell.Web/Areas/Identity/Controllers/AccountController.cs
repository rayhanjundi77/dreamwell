﻿using Dreamwell.Common.Cryptography;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.Options;
using Dreamwell.Infrastructure.Session;
using Dreamwell.Web.Areas.Identity.Models;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using RestSharp;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Dreamwell.Web.Areas.Identity.Controllers
{
    public class AccountController : Controller
    {
        Logger appLogger = LogManager.GetCurrentClassLogger();
        // GET: Identity/Account

        public ActionResult Login(string returnUrl)
        {
            if (SessionManager.UserSession == null)
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
            else
                return Redirect("~/Core/Home");
        }

        public ActionResult LoginExternal(string clientId, string passPhrase, string returnUrl)
        {
            var client = new RestClient(new RestClientOptions(DreamwellSettings.ApiServerUrl)
            {
                MaxTimeout = -1,
            });
            var request = new RestRequest($"/core/applicationexternal/GetByClientId?clientid={clientId}", Method.Get);
            if (SessionManager.UserSession != null)
                request.AddHeader("Authorization", $"Bearer {SessionManager.UserSession.Token}");
            request.AddHeader("Server-Type", AppConstants.ServerTypeAPI);
            request.AddParameter(clientId, clientId);
            var response = client.Execute<ExternalApp>(request);
            if (response.Data != null)
            {
                var resultData = response.Data;
                string phrase = $"{resultData.pass_phrase}|{DateTime.Now.ToString("yyyyddMM")}";
                passPhrase = Base64UrlEncoder.Decode(passPhrase);
                string phraseIn = AesEncryption.Decrypt(passPhrase);
                if (phrase == phraseIn)
                    if (SessionManager.UserSession == null)
                        return RedirectToAction("Login", new { returnUrl = returnUrl });
                    else
                        return Redirect($"{returnUrl}?token={SessionManager.UserSession.Token}");
            }

            return RedirectToAction("Login", new { returnUrl = returnUrl });
        }

        public ActionResult LoginUTC()
        {
            return View();
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Login(FormCollection data, string returnUrl)
        //{
        //    //-- iserng
        //    var yourusername = data["username"];
        //    var yourpassword = data["password"];

        //    TempData["errorLogin"] = false;
        //    var Token = string.Empty;
        //    var log = string.Empty;
        //    var Response = await Task.Run<ApiResponse>(() =>
        //    {
        //        var result = new ApiResponse();
        //        if (data.AllKeys.Contains("username") && data.AllKeys.Contains("password"))
        //        {
        //            using (var _client = new HttpClient())
        //            {
        //                try
        //                {
        //                    var request = new
        //                    {
        //                        Username = data["username"].ToString(),
        //                        Password = data["password"].ToString()
        //                    };
        //                    _client.BaseAddress = new Uri(DreamwellSettings.ApiServerUrl);
        //                    var stringContent = new StringContent(JsonConvert.SerializeObject(request), UnicodeEncoding.UTF8, "application/json");
        //                    var apiUri = Infrastructure.Options.DreamwellSettings.ApiServerUrl;
        //                    var task = _client.PostAsync(string.Format("{0}/{1}", apiUri, "core/Authentication/authenticate"), stringContent).Result;

        //                    if (task.Content != null)
        //                    {
        //                        var responseContent = task.Content.ReadAsStringAsync().Result;
        //                        var response = JsonConvert.DeserializeObject<dynamic>(responseContent);
        //                        if (((bool)((JValue)response.status.success).Value))
        //                        {
        //                            Token = response.data.token;
        //                            var userSession = SessionManager.FromToken(Token);
        //                            userSession.Token = Token;

        //                            Session[AppConstants.SessionKey] = userSession;
        //                            result.Status.Success = true;

        //                        }
        //                        else
        //                        {
        //                            log = response.status.message;
        //                            result.Status.Success = false;
        //                            result.Status.Message = log;
        //                        }
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    appLogger.Error(ex);
        //                    result.Status.Success = false;
        //                    result.Status.Message = ex.Message;
        //                }
        //            }
        //        }
        //        return result;
        //    });

        //    if (Response != null && Response.Status.Success)
        //    {
        //        if (!string.IsNullOrEmpty(returnUrl) && (returnUrl.StartsWith("http:") || returnUrl.StartsWith("https:")))
        //            return Redirect($"{returnUrl}?token={SessionManager.UserSession.Token}");
        //        else
        //            return Redirect(!string.IsNullOrEmpty(returnUrl) ? returnUrl : "~/Core/Home");
        //    }
        //    else
        //    {
        //        TempData["MyMsg"] = Response.Status.Message;
        //        return Redirect("~/Identity/Account/Login/");
        //    }

        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(FormCollection data, string returnUrl)
        {
            // Validasi Captcha
            var inputCaptchaCode = data["CaptchaCode"];
            if (Session["CaptchaCode"] == null || inputCaptchaCode != Session["CaptchaCode"].ToString())
            {
                TempData["MyMsg"] = "Invalid captcha code. Please try again.";
                return Redirect("~/Identity/Account/Login/");
            }

            //-- Proses login
            var yourusername = data["username"];
            var yourpassword = data["password"];

            TempData["errorLogin"] = false;
            var Token = string.Empty;
            var log = string.Empty;

            var Response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                if (data.AllKeys.Contains("username") && data.AllKeys.Contains("password"))
                {
                    using (var _client = new HttpClient())
                    {
                        try
                        {
                            var request = new
                            {
                                Username = yourusername,
                                Password = yourpassword
                            };
                            _client.BaseAddress = new Uri(DreamwellSettings.ApiServerUrl);
                            var stringContent = new StringContent(JsonConvert.SerializeObject(request), UnicodeEncoding.UTF8, "application/json");
                            var apiUri = Infrastructure.Options.DreamwellSettings.ApiServerUrl;

                            var task = _client.PostAsync($"{apiUri}/core/Authentication/authenticate", stringContent).Result;

                            if (task.Content != null)
                            {
                                var responseContent = task.Content.ReadAsStringAsync().Result;
                                var response = JsonConvert.DeserializeObject<dynamic>(responseContent);

                                if (((bool)((JValue)response.status.success).Value))
                                {
                                    Token = response.data.token;
                                    var userSession = SessionManager.FromToken(Token);
                                    userSession.Token = Token;

                                    Session[AppConstants.SessionKey] = userSession;
                                    result.Status.Success = true;
                                }
                                else
                                {
                                    log = response.status.message;
                                    result.Status.Success = false;
                                    result.Status.Message = log;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            appLogger.Error(ex);
                            result.Status.Success = false;
                            result.Status.Message = ex.Message;
                        }
                    }
                }
                return result;
            });

            // Proses setelah login
            if (Response != null && Response.Status.Success)
            {
                if (!string.IsNullOrEmpty(returnUrl) && (returnUrl.StartsWith("http:") || returnUrl.StartsWith("https:")))
                {
                    return Redirect($"{returnUrl}?token={SessionManager.UserSession.Token}");
                }
                else
                {
                    return Redirect(!string.IsNullOrEmpty(returnUrl) ? returnUrl : "~/Core/Home");
                }
            }
            else
            {
                TempData["MyMsg"] = Response.Status.Message;
                return Redirect("~/Identity/Account/Login/");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LoginUTC(FormCollection data)
        {
            var yourusername = data["username"];
            var yourpassword = data["password"];

            var returnUrl = Request.QueryString["ReturnUrl"] ?? string.Empty;
            TempData["errorLogin"] = false;
            var log = string.Empty;
            var Response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                if (data.AllKeys.Contains("username") && data.AllKeys.Contains("password"))
                {
                    var Token = string.Empty;
                    using (var _client = new HttpClient())
                    {
                        try
                        {
                            var request = new
                            {
                                Username = data["username"].ToString(),
                                Password = data["password"].ToString()
                            };
                            _client.BaseAddress = new Uri(DreamwellSettings.ApiServerUrl);
                            var stringContent = new StringContent(JsonConvert.SerializeObject(request), UnicodeEncoding.UTF8, "application/json");
                            var apiUri = Infrastructure.Options.DreamwellSettings.ApiServerUrl;
                            var task = _client.PostAsync(string.Format("{0}/{1}", apiUri, "core/Authentication/authenticate"), stringContent).Result;

                            if (task.Content != null)
                            {
                                var responseContent = task.Content.ReadAsStringAsync().Result;
                                var response = JsonConvert.DeserializeObject<dynamic>(responseContent);
                                if (((bool)((JValue)response.status.success).Value))
                                {
                                    Token = response.data.token;
                                    var userSession = SessionManager.FromToken(Token);
                                    userSession.Token = Token;
                                    Session[AppConstants.SessionKey] = userSession;
                                    result.Status.Success = true;

                                }
                                else
                                {
                                    log = response.status.message;
                                    result.Status.Success = false;
                                    result.Status.Message = log;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            appLogger.Error(ex);
                            result.Status.Success = false;
                            result.Status.Message = ex.Message;
                        }
                    }
                }
                return result;
            });

            if (Response != null && Response.Status.Success)
            {
                return Redirect(!string.IsNullOrEmpty(returnUrl) ? returnUrl : "~/UTC/APH/Dashboard");
            }
            else
            {
                TempData["MyMsg"] = Response;
                //return Redirect("~/Identity/Account/LoginUTC/");
                return Redirect("~/Identity/Account/Login/");
            }

        }


        public async Task<ActionResult> Logout()
        {
            return await Task.Run<ActionResult>(() =>
            {
                Session.Clear();
                return RedirectToAction("Login");
            });
        }

        public async Task<ActionResult> LogoutUTC()
        {
            return await Task.Run<ActionResult>(() =>
            {
                Session.Clear();
                return RedirectToAction("LoginUTC");
            });
        }


    }
}