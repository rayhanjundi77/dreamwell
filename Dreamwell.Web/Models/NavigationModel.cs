﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using CommonTools.SmartNavigation;
using Dreamwell.Infrastructure.Options;
using Dreamwell.Infrastructure.Session;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Dreamwell.Web.Models
{
    internal static class NavigationBuilder
    {
        private static JsonSerializerSettings DefaultSettings => SerializerSettings();

        private static JsonSerializerSettings SerializerSettings(bool indented = true) => new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            Formatting = indented ? Formatting.Indented : Formatting.None,
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> { new StringEnumConverter() }
        };

        public static SmartNavigation FromJson(string json) => JsonConvert.DeserializeObject<SmartNavigation>(json, DefaultSettings);
    }
    public static class NavigationModel
    {
        static NLog.Logger appLogger = NLog.LogManager.GetCurrentClassLogger();
        private const string Underscore = "_";
        private const string Space = " ";

        public static SmartNavigation Seed => BuildNavigation();
        public static SmartNavigation SeedForUTC => BuildNavigationUTC();

        private static SmartNavigation BuildNavigation(bool seedOnly = true)
        {
            var userSession = SessionManager.UserSession;
            //var jsonText = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "nav.json");
            //var navigation =  NavigationBuilder.FromJson(jsonText);
            var navigation = new SmartNavigation();
            using (var _client = new HttpClient() )
            {
                try
                {
                    _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userSession.Token);
                    _client.DefaultRequestHeaders.Add("Server-Type",Infrastructure.AppConstants.ServerTypeAPI);
                    _client.BaseAddress = new Uri(DreamwellSettings.ApiServerUrl);
                    var apiUri = Infrastructure.Options.DreamwellSettings.ApiServerUrl;
                    var task = _client.PostAsync(string.Format("{0}/{1}", apiUri, "core/ApplicationMenu/GetByUserId?moduleName=AP&Category=SIDEBAR"),null).Result;

                    if (task.Content != null)
                    {
                        var responseContent = task.Content.ReadAsStringAsync().Result;
                        navigation = NavigationBuilder.FromJson(responseContent);
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                }
            }

            var menu = FillProperties(navigation.Lists, seedOnly);

            return new SmartNavigation(menu);
        }

        private static SmartNavigation BuildNavigationUTC(bool seedOnly = true)
        {
            var userSession = SessionManager.UserSession;
            //var jsonText = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "nav.json");
            //var navigation =  NavigationBuilder.FromJson(jsonText);
            var navigation = new SmartNavigation();
            using (var _client = new HttpClient())
            {
                try
                {
                    //var ss = ControllerContext.RouteData.Values["unit"].ToString();
                    var businessUnitRoute = HttpContext.Current.Request.RequestContext.RouteData.Values["unit"].ToString();

                    _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userSession.Token);
                    _client.DefaultRequestHeaders.Add("Server-Type", Infrastructure.AppConstants.ServerTypeAPI);
                    _client.BaseAddress = new Uri(DreamwellSettings.ApiServerUrl);
                    var apiUri = Infrastructure.Options.DreamwellSettings.ApiServerUrl;
                    var task = _client.PostAsync(string.Format("{0}/{1}", apiUri, "core/ApplicationMenu/"+ businessUnitRoute + "/GetByUserId?moduleName=UTC&Category=SIDEBAR"), null).Result;

                    if (task.Content != null)
                    {
                        var responseContent = task.Content.ReadAsStringAsync().Result;
                        appLogger.Debug("Navigation UTC Response");
                        appLogger.Debug(responseContent);
                        navigation = NavigationBuilder.FromJson(responseContent);
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                }
            }

            var menu = FillProperties(navigation.Lists, seedOnly);

            return new SmartNavigation(menu);

            //var jsonText = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "nav_utc.json");
            //var navigation = NavigationBuilder.FromJson(jsonText);
            //var menu = FillProperties(navigation.Lists, seedOnly);

            //return new SmartNavigation(menu);
        }

        private static List<ListItem> FillProperties(IEnumerable<ListItem> items, bool seedOnly, ListItem parent = null)
        {
            var result = new List<ListItem>();

            foreach (var item in items)
            {
                item.Text = item.Text ?? item.Title;
                item.Tags = string.Concat(parent?.Tags, Space, item.Title.ToLower()).Trim();

                var route = Path.GetFileNameWithoutExtension(item.Href ?? string.Empty)?.Split(Underscore.ToCharArray());

                item.Route = route?.Length > 1 ? $"/{route.First()}/{string.Join(string.Empty, route.Skip(1))}" : item.Href;

                item.I18n = parent == null
                    ? $"nav.{item.Title.ToLower().Replace(Space, Underscore)}"
                    : $"{parent.I18n}_{item.Title.ToLower().Replace(Space, Underscore)}";

                item.Items = FillProperties(item.Items, seedOnly, item);

                if (!seedOnly || item.ShowOnSeed)
                    result.Add(item);
            }

            return result;
        }
    }

   
}