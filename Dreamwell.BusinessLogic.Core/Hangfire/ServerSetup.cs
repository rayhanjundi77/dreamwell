﻿using Hangfire;
using Hangfire.SqlServer;
using Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Hangfire
{
    public class ServerSetup
    {
        public static void init(IAppBuilder app)
        {

            var connStr = System.Configuration.ConfigurationManager.
              ConnectionStrings["connStr"].ConnectionString;


            GlobalConfiguration.Configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(connStr, new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    //QueuePollInterval = TimeSpan.FromMinutes(5),
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                });

            //app.UseHangfireDashboard();
            //app.UseHangfireServer();




            var option = new BackgroundJobServerOptions
            {
                WorkerCount = Environment.ProcessorCount * 4
            };

            app.UseHangfireServer(option); app.UseHangfireDashboard();
            app.UseHangfireServer();


            // Nanti di buat kan job secara berurutan, dari create organisasi, user, sampai default data yg di perlukan

            BackgroundJob.Enqueue(() => new SysConfig.ApplicationMenuSetup().Setup());


        }
        public static void Job()
        {
            Debug.WriteLine($"Done ! {DateTime.Now}");
        }


    }
}
