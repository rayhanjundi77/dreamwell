﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Newtonsoft.Json.Linq;
using System.Web;
using DataTables.Mvc;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{

    //1=waiting, 2=In Process, 3=Rejected, 200=Accepted
    public enum approvalStatus
    {
        Waiting = 1,
        InProcess = 2,
        Rejected = 3,
        Accepted = 200
    }

    public class getApprovalitem
    {
        public string assignTo { get; set; }
    }

    public class approvalRequestBody
    {
        public bool IsAccepted { get; set; }
        public string RecordId { get; set; }
        public string Message { get; set; }

        public approvalRequestBody()
        {
            IsAccepted = false;
        }
    }
}
