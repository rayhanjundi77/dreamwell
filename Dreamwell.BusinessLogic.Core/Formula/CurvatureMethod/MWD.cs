﻿namespace Dreamwell.BusinessLogic.Core.CurvatureMethod
{
    public class MWD
    {
        public double SurveyDepth { get; set; }
        public double Inclination { get; set; }
        public double Azimuth { get; set; }
        public double TVD { get; set; }
        public double EastWest { get; set; }
        public double NorthSouth { get; set; }
    }
}
