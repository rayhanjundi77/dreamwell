﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.CurvatureMethod
{
    public class MinimumCurvature
    {
        public MinimumCurvatureResult Calculate(MWD Upper, MWD Lower, decimal? TargetDirection)
        {
            MinimumCurvatureResult result = new MinimumCurvatureResult();

            try
            {
                var MD = Lower.SurveyDepth - Upper.SurveyDepth;
                var I1 = Upper.Inclination * Math.PI / 180;
                var I2 = Lower.Inclination * Math.PI / 180; 
                var A1 = Upper.Azimuth * Math.PI / 180;
                var A2 = Lower.Azimuth * Math.PI / 180;

                var beta = Math.Cos(I2 - I1) - Math.Sin(I1) * Math.Sin(I2) * (1 - Math.Cos(A2 - A1));

                beta = Math.Acos(beta);

                //-- AH15
                double RF = Math.Abs((2 / beta * Math.Tan(beta / 2)));
                bool checkRF = double.IsNaN(RF);
                if (checkRF)
                    RF = 1;
                //bool isNumeric = double.TryParse(checkRF.ToString(), out RF);

                double TVD = 0;
                if (Lower.SurveyDepth > 0)
                {
                    TVD = MD / 2 * (Math.Cos(I1) + Math.Cos(I2)) * RF;
                    TVD += Upper.TVD;
                }

                //-- M15
                double EastWest = 0;
                if (Lower.SurveyDepth > 0)
                {
                    var currentEastWest = (RF * (MD / 2)) * ((Math.Sin(I1) * Math.Sin(A1)) + (Math.Sin(I2) * Math.Sin(A2)));
                    EastWest = currentEastWest + Upper.EastWest;
                }

                //-- L15
                double NorthSouth = 0;
                if (Lower.SurveyDepth > 0)
                {
                    var currentNorthSouth = (RF * (MD / 2)) * ((Math.Sin(I1) * Math.Cos(A1)) + (Math.Sin(I2) * Math.Cos(A2)));
                    NorthSouth = currentNorthSouth + Upper.NorthSouth;
                }
              
                //-- Closure Distance
                var ClosureDistance = Math.Sqrt(Math.Pow(NorthSouth, 2) + Math.Pow(EastWest, 2));

                //-- AJ15
                var AI15 = Math.Abs(Math.Atan(EastWest / NorthSouth));
                if (double.IsNaN(AI15))
                    AI15 = 0;
                var AJ15 = ((0 < EastWest && NorthSouth > 0) ? AI15 : ((0 < EastWest && NorthSouth < 0) ? Math.PI - AI15: ((0 > EastWest && NorthSouth < 0) ? Math.PI + AI15: 2 * Math.PI - AI15)));

                double VerticalSection = 0;
                if (Lower.SurveyDepth > 0)
                    VerticalSection  = Math.Cos((double)TargetDirection * Math.PI / 180 - AJ15) * ClosureDistance;

                double DLS = 0;
                if (Lower.SurveyDepth > 0)
                    DLS = beta * 180 / Math.PI * 100 / MD;

                result.TVD = Math.Round(TVD,2);
                result.North = NorthSouth;
                result.East = EastWest;
                result.VerticalSection = VerticalSection;
                result.DLS = DLS;
                result.MeasureDepth = MD + Upper.SurveyDepth;
                result.CourseLength = MD;
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public MinimumCurvatureResult CalculateOnRadians(MWD Upper, MWD Lower, double TargetDirection)
        {
            MinimumCurvatureResult result = new MinimumCurvatureResult();

            try
            {
                var MD = Lower.SurveyDepth - Upper.SurveyDepth;
                var I1 = Upper.Inclination;
                var I2 = Lower.Inclination;
                var A1 = Upper.Azimuth;
                var A2 = Lower.Azimuth;

                var beta =
                    Math.Cos((I2 / 180 * Math.PI) - (I1 / 180 * Math.PI))
                    - Math.Sin(I1 / 180 * Math.PI)
                    * Math.Sin(I2 / 180 * Math.PI)
                    * (1 - Math.Cos((A2 / 180 * Math.PI) - (A1 / 180 * Math.PI)));
                var Dbeta =
                    Math.Cos(I2 - I1)
                    - Math.Sin(I1)
                    * Math.Sin(I2)
                    * (1 - Math.Cos(A2 - A1));
                //Console.WriteLine($"beta cos = {beta}");

                beta = Math.Acos(beta);
                //Console.WriteLine($"beta rad = {beta}");

                //-- AH15
                var RF = Math.Abs((2 / beta * Math.Tan(beta / 2)));
                // Need to be check, existing formula as below
                //var RF = (2 / beta * Math.Tan(beta / 2)) ? (2 / beta * Math.Tan(beta / 2)) : 1;
                //Console.WriteLine($"RF = {RF}");

                //var TVD = MD / 2 * (Math.Cos(I1 / 180 * Math.PI) + Math.Cos(I2 / 180 * Math.PI)) * RF;
                var TVD =
                    //Upper.TVD + 
                    MD / 2 *
                    (Math.Cos(I1) + Math.Cos(I2))
                    * RF;
                TVD += Upper.TVD;
                //Console.WriteLine($"TVD = {TVD}");

                /*
                    TOLONG DIPERHATIKAN TVD-nya.
                    Karena akan dipake Upper TVD sebagai LAST TVD untuk Akumulasi TVD.

                 */

                //Console.WriteLine($"North = {North}");

                //-- M15
                var currentEastWest = (Upper.SurveyDepth > 0 ?
                    //Upper.EastWest + 
                    (RF * (MD / 2))
                    * (((Math.Sin(I1 * Math.PI / 180)
                    * Math.Sin(A1 * Math.PI / 180))
                    + (Math.Sin(I2 * Math.PI / 180)
                    * Math.Sin(A2 * Math.PI / 180))))
                    : 0);
                var EastWest = currentEastWest + Upper.EastWest;

                /*
                 =IF(F16>0,L15+I16/2*(SIN(AD15)*COS(AE15)+SIN(AD16)*COS(AE16))*AH16," ") 
                 */
                //-- L15
                var currentNorthSouth = Upper.SurveyDepth > 0 ?
                    //Upper.NorthSouth + 
                    (RF * (MD / 2))
                    * (((Math.Sin(I1 * Math.PI / 180)
                    * Math.Cos(A1 * Math.PI / 180))
                    + (Math.Sin(I2 * Math.PI / 180)
                    * Math.Cos(A2 * Math.PI / 180))))
                    : 0;
                var NorthSouth = currentNorthSouth + Upper.NorthSouth;

                //-- Closure Distance
                var ClosureDistance = Math.Sqrt(Math.Pow(NorthSouth, 2) + Math.Pow(EastWest, 2));

                //-- AJ15
                var AI15 = Math.Abs(Math.Atan(EastWest / NorthSouth));
                var AJ15 = ((0 < EastWest && NorthSouth > 0) ? AI15 : ((0 < EastWest && NorthSouth < 0) ? Math.PI - AI15 : ((0 > EastWest && NorthSouth < 0) ? Math.PI + AI15 : 2 * Math.PI - AI15)));

                double VerticalSection = 0;
                if (Upper.SurveyDepth > 0)
                    VerticalSection = Math.Cos(TargetDirection * Math.PI / 180 - AJ15) * ClosureDistance;

                double DLS = 0;
                if (Upper.SurveyDepth > 0)
                    DLS = beta * 180 / Math.PI * 100 / MD;


                result.TVD = TVD;
                result.North = NorthSouth;
                result.East = EastWest;
                result.VerticalSection = VerticalSection;
                result.DLS = DLS;
                result.MeasureDepth = MD;
                result.CourseLength = MD;

                // Vertical Section - M15







                //Closure Azimuth
                //var ClosureAzimuth = Math.Atan(TotalEastWest/ TotalNorthSouth);
                //if (AzimuthTarget >= 90 && AzimuthTarget < 180)
                //    ClosureAzimuth = 180 - ClosureAzimuth;  
                //else if (AzimuthTarget >= 180 && AzimuthTarget < 270)
                //    ClosureAzimuth = 180 + ClosureAzimuth;
                //else if (AzimuthTarget >= 270 && AzimuthTarget <= 360)
                //    ClosureAzimuth = 360 - ClosureAzimuth;

                ////Directional Difference
                //var DirectionalDifference = AzimuthTarget - ClosureAzimuth;


                // DogLeg Severity
                //var DLS = (DL * 100)/ MD; // calculated per 100ft.
                //result.VerticalSection = VerticalSection;
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }
    }
}
