﻿namespace Dreamwell.BusinessLogic.Core.CurvatureMethod
{
    public class MinimumCurvatureResult
    {
        public double? CourseLength { get; set; }
        public double? North { get; set; }
        public double? East { get; set; }
        public double? TVD { get; set; }
        public double? VerticalSection { get; set; }
        public double? DLS { get; set; }
        public double? BLD { get; set; }
        public double? WalkRate { get; set; }
        public double? MeasureDepth { get; set; }
    }
}
