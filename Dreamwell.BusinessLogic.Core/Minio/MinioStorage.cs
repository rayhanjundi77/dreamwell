﻿using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.Repository;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.Options;
using Minio;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static Dreamwell.DataAccess.dreamwellRepo;

namespace Dreamwell.BusinessLogic.Core.Minio
{
    public class MinioFile
    {
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string Extention { get; set; }
        public string MimeType { get; set; }

        public string BucketName { get; set; }
        public string ObjectName { get; set; }
        public MemoryStream StreamFile { get; set; }

        public MinioFile()
        {
            StreamFile = new MemoryStream();
        }

    }
   
    public class MinioStorage<TEntity> : FileMasterRepository 
    {
        MinioClient minioClient;
        string serverAddress = MinioStorage.Server;
        string bucketName = string.Empty;
        const string shareBucketName = "dreamwell-bucket";
        string AppEntityName = string.Empty;
        string AppEntityId = string.Empty;

        public MinioStorage(DataContext dataContext) : base(dataContext)
        {
            /*Minio endpoint client connection*/
            minioClient = new MinioClient(serverAddress,
                                       MinioStorage.Username,
                                       MinioStorage.Password
                                 );

            /*SET Bucket Name*/
            bucketName = this._services.dataContext.OrganizationName.ToLower();
            var appEntityObject = Activator.CreateInstance<TEntity>();
            AppEntityId = ((IEntity)appEntityObject).GetEntityId();
            AppEntityName = ((IEntity)appEntityObject).GetEntityDisplayName().Replace(" ", "-").ToLower();
        }


        public async static Task CreateBucket(MinioClient minio,
                                 string bucketName = shareBucketName)
        {
            try
            {
                appLogger.Debug("Running example for API: MakeBucketAsync");
                await minio.MakeBucketAsync(bucketName);
                appLogger.Debug("Created bucket " + bucketName);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
        }


        public bool CheckFile(string pathObject, string postedFile, string recordId)
        {
            var result = false;
            try
            {
                string bucketName = this._services.dataContext.OrganizationName.ToLower();
             
                string fullFileName = postedFile;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string extension = Path.GetExtension(postedFile);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile);

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                var newPathObject =
                    string.IsNullOrEmpty(pathObject) ?
                    string.Format("{0}/{1}/{2}", AppEntityName, recordId, postedFile) :
                    string.Format("{0}/{1}/{2}{3}", AppEntityName, recordId, pathObject, postedFile);

                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newPathObject);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                try
                {
                    if ((fileData == null) || (fileData.filepath != newPathObject))
                    {
                        result = true;
                    }
                    else if (fileData.filepath == newPathObject)
                    {
                        throw new NullReferenceException("File Existed.");
                    }
                }
                catch
                {
                    appLogger.Error("File Existed. Try Again");
                    result = false;
                    //throw;
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                // throw;
            }
            return result;
        }

        public async Task<bool> Upload(string pathObject, HttpPostedFile postedFile, string recordId, string columnTarget = "filemaster_id")
        {
            var result = false;
            try
            {
                // bool found = await minioClient.BucketExistsAsync(bucketName);
                // if (!found)
                // {
                //     await CreateBucket(minioClient, bucketName);
                // }

                // Create the Directory.
                string path = "C:\\UploadsLithology";
                // if (!Directory.Exists(path))
                // {
                //     Directory.CreateDirectory(path);
                // }

                string fullFileName = postedFile.FileName;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string ext = Path.GetExtension(postedFile.FileName);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile.FileName);

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                AppEntityName = "Lithology";
                string fileName = Path.GetFileName(postedFile.FileName);
                string newFilePath = string.Format("{0}{1}", recordId, ext);
                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newFilePath);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                if (fileData?.filepath == newFilePath)
                {
                    var servicesFile = new FileMasterServices(this._services.dataContext);
                    var detailFile = servicesFile.Remove(fileData.id);
                }

                string filePath = Path.Combine(path, newFilePath);
                // Save the File asynchronously.
                await Task.Run(() => postedFile.SaveAs(filePath));

                // Uncomment if you use Minio asynchronously
                // await minioClient.PutObjectAsync(bucketName, newPathObject, postedFile.InputStream, postedFile.ContentLength, mime, metaData);

                result = true;
                var recordFile = new filemaster();
                recordFile.is_minio = false;
                recordFile.bucket_name = bucketName;
                recordFile.name = fullFileName;
                recordFile.realname = fullFileName;
                recordFile.extension = ext;
                recordFile.size = postedFile.ContentLength;
                recordFile.mimetype = mime;
                recordFile.filepath = newFilePath;
                recordFile.application_entity_id = AppEntityId;
                recordFile.record_id = recordId;
                recordFile.organization_id = this._services.dataContext.OrganizationId;
                result &= this._services.dataContext.SaveEntity<filemaster>(recordFile, true);

                if (result && !string.IsNullOrEmpty(recordFile.id))
                {
                    var appObj = Activator.CreateInstance<TEntity>();
                    var entityRecord = this._services.db.FirstOrDefault<TEntity>("WHERE id=@0", recordId);

                    CommonTools.Helper.SetPropertyByName.SetPropValue<TEntity>(entityRecord, columnTarget, recordFile.id);
                    result &= this._services.dataContext.SaveEntity<TEntity>(entityRecord, false);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }


        public Task<string> UploadBHA(string pathObject, HttpPostedFile postedFile, string recordId, string columnTarget = "filemaster_id")
        {
            var result = false;
            string name = "";
            try
            {
                var path = ConfigurationManager.AppSettings["UploadBHAPath"].ToString();
                string dirName = AppDomain.CurrentDomain.BaseDirectory;
                FileInfo fileInfo = new FileInfo(dirName);
                DirectoryInfo parentDir = fileInfo.Directory.Parent;
                string parentDirName = parentDir.FullName;

                path = parentDirName + path;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                bha_component record = new bha_component();

                if (recordId == "null")
                    recordId = CommonTools.GuidHash.ConvertToMd5HashGUID(record.component_name).ToString();

                string fullFileName = postedFile.FileName;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string ext = Path.GetExtension(postedFile.FileName);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile.FileName);
                string extString = fullFileName.Substring(fullFileName.Length - 4);
                fullFileName = string.Format("{0}{1}", recordId, ext);
                name = fullFileName;

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                AppEntityName = "BHAComponent";
                string fileName = Path.GetFileName(postedFile.FileName);
                string newFilePath = string.Format("{0}{1}", recordId, ext);
                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newFilePath);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                if (fileData?.filepath == newFilePath)
                {
                    System.IO.File.Delete(Path.Combine(path, newFilePath));
                }

                string filePath = Path.Combine(path, newFilePath);
                postedFile.SaveAs(filePath);

                result = true;
                var recordFile = new filemaster();
                recordFile.is_minio = false;
                recordFile.bucket_name = bucketName;
                recordFile.name = fullFileName;
                recordFile.realname = fullFileName;
                recordFile.extension = ext;
                recordFile.size = postedFile.ContentLength;
                recordFile.mimetype = mime;
                recordFile.filepath = newFilePath;
                recordFile.application_entity_id = AppEntityId;
                recordFile.record_id = recordId;
                recordFile.organization_id = this._services.dataContext.OrganizationId;
                result &= this._services.dataContext.SaveEntity<filemaster>(recordFile, true);
                name = recordFile.id;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return Task.FromResult(name);
        }

        public async Task<string> UploadBHANew(HttpPostedFile postedFile, string recordId, string columnTarget = "image_id")
        {
            var result = false;
            string name = "";
            try
            {
                string path = ConfigurationManager.AppSettings["UploadBHAPath"].ToString();
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string fullFileName = postedFile.FileName;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string ext = Path.GetExtension(postedFile.FileName);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile.FileName);
                string extString = fullFileName.Substring(fullFileName.Length - 4);
                fullFileName = string.Format("{0}{1}", recordId, ext);
                name = fullFileName;

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                AppEntityName = "BHAComponent";
                string fileName = Path.GetFileName(postedFile.FileName);
                string newFilePath = string.Format("{0}{1}", recordId, ext);
                string newFilePathForFile = string.Format("{0}{1}", recordId, ext);
                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newFilePath);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                if (fileData?.filepath == newFilePath)
                {
                    var servicesFile = new FileMasterServices(this._services.dataContext);
                    var detailFile = servicesFile.Remove(fileData.id);
                }

                string filePath = Path.Combine(path, newFilePath);
                await Task.Run(() => postedFile.SaveAs(filePath));

                // Uncomment if Minio is being used asynchronously
                // await minioClient.PutObjectAsync(bucketName, newPathObject, postedFile.InputStream, postedFile.ContentLength, mime, metaData);

                result = true;
                var recordFile = new filemaster();
                recordFile.is_minio = false;
                recordFile.bucket_name = bucketName;
                recordFile.name = fullFileName;
                recordFile.realname = fullFileName;
                recordFile.extension = ext;
                recordFile.size = postedFile.ContentLength;
                recordFile.mimetype = mime;
                recordFile.filepath = newFilePathForFile;
                recordFile.application_entity_id = AppEntityId;
                recordFile.record_id = recordId;
                recordFile.organization_id = this._services.dataContext.OrganizationId;
                result &= this._services.dataContext.SaveEntity<filemaster>(recordFile, true);

                if (result & !string.IsNullOrEmpty(recordFile.id))
                {
                    var appObj = Activator.CreateInstance<TEntity>();
                    var entityRecord = this._services.db.FirstOrDefault<TEntity>("WHERE id=@0", recordId);

                    CommonTools.Helper.SetPropertyByName.SetPropValue<TEntity>(entityRecord, columnTarget, recordFile.id);
                    result &= this._services.dataContext.SaveEntity<TEntity>(entityRecord, false);
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }

            return name;
        }


        public async Task<string> UploadLithology(string pathObject, HttpPostedFile postedFile, string recordId, string columnTarget = "filemaster_id")
        {
            var result = false;
            string name = "";
            try
            {
                string path = ConfigurationManager.AppSettings["UploadLithologyPath"].ToString() + "/stone/" + recordId + "/";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string fullFileName = postedFile.FileName;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string ext = Path.GetExtension(postedFile.FileName);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile.FileName);
                string extString = fullFileName.Substring(fullFileName.Length - 4);
                fullFileName = string.Format("{0}{1}", recordId, ext);
                name = fullFileName;

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                AppEntityName = "Lithology";
                string fileName = Path.GetFileName(postedFile.FileName);
                string newFilePath = string.Format("{0}{1}", recordId, ext);
                string newFilePathForFile = "/stone/" + recordId + "/" + string.Format("{0}{1}", recordId, ext);
                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newFilePath);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                if (fileData?.filepath == newFilePath)
                {
                    var servicesFile = new FileMasterServices(this._services.dataContext);
                    var detailFile = servicesFile.Remove(fileData.id);
                }

                string filePath = Path.Combine(path, newFilePath);
                await Task.Run(() => postedFile.SaveAs(filePath));

                // await minioClient.PutObjectAsync(bucketName, newPathObject, postedFile.InputStream, postedFile.ContentLength, mime, metaData);
                result = true;
                var recordFile = new filemaster();
                recordFile.is_minio = false;
                recordFile.bucket_name = bucketName;
                recordFile.name = fullFileName;
                recordFile.realname = fullFileName;
                recordFile.extension = ext;
                recordFile.size = postedFile.ContentLength;
                recordFile.mimetype = mime;
                recordFile.filepath = newFilePathForFile;
                recordFile.application_entity_id = AppEntityId;
                recordFile.record_id = recordId;
                recordFile.organization_id = this._services.dataContext.OrganizationId;
                result &= this._services.dataContext.SaveEntity<filemaster>(recordFile, true);
                name = recordFile.id;

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }

            return name;
        }

        public async Task<string> UploadAttachmentWIR(string pathObject, HttpPostedFile postedFile, string recordId, string columnTarget = "filemaster_id")
        {
            var result = false;
            string name = "";
            try
            {
                string path = ConfigurationManager.AppSettings["UploadAttachmentWIR"].ToString() + "/attach/" + recordId + "/";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string fullFileName = postedFile.FileName;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string ext = Path.GetExtension(postedFile.FileName);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile.FileName);
                string extString = fullFileName.Substring(fullFileName.Length - 4);
                fullFileName = string.Format("{0}{1}", recordId, ext);
                name = fullFileName;

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                AppEntityName = "Attachment WIR";
                string fileName = Path.GetFileName(postedFile.FileName);
                string newFilePath = string.Format("{0}{1}", recordId, ext);
                string newFilePathForFile = "/attach/" + recordId + "/" + string.Format("{0}{1}", recordId, ext);
                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newFilePath);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                if (fileData?.filepath == newFilePath)
                {
                    var servicesFile = new FileMasterServices(this._services.dataContext);
                    var detailFile = servicesFile.Remove(fileData.id);
                }

                string filePath = Path.Combine(path, newFilePath);
                await Task.Run(() => postedFile.SaveAs(filePath));

                // await minioClient.PutObjectAsync(bucketName, newPathObject, postedFile.InputStream, postedFile.ContentLength, mime, metaData);
                result = true;
                var recordFile = new filemaster();
                recordFile.is_minio = false;
                recordFile.bucket_name = bucketName;
                recordFile.name = fullFileName;
                recordFile.realname = fullFileName;
                recordFile.extension = ext;
                recordFile.size = postedFile.ContentLength;
                recordFile.mimetype = mime;
                recordFile.filepath = newFilePathForFile;
                recordFile.application_entity_id = AppEntityId;
                recordFile.record_id = recordId;
                recordFile.organization_id = this._services.dataContext.OrganizationId;
                result &= this._services.dataContext.SaveEntity<filemaster>(recordFile, true);
                name = recordFile.id;

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }

            return name;
        }


        public async Task<string> UploadLampiranPDF(string pathObject, HttpPostedFile postedFile, string recordId, string columnTarget = "filemaster_id")
        {
            var result = false;
            string name = "";
            try
            {
                string path = ConfigurationManager.AppSettings["UploadLampiranPDF"].ToString() + "/lampiran/" + recordId + "/";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string fullFileName = postedFile.FileName;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string ext = Path.GetExtension(postedFile.FileName);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile.FileName);

                // Tambahkan pembeda unik (misalnya GUID) ke nama file
                string uniqueSuffix = Guid.NewGuid().ToString();
                fullFileName = string.Format("{0}{1}", uniqueSuffix, ext);
                name = fullFileName;

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                // Fetch the File Name.
                AppEntityName = "Lampiran";
                string newFilePath = string.Format("{0}_{1}{2}", recordId, uniqueSuffix, ext);
                string newFilePathForFile = "/lampiran/" + recordId + "/" + newFilePath;
                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newFilePath);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                if (fileData?.filepath == newFilePath)
                {
                    var servicesFile = new FileMasterServices(this._services.dataContext);
                    var detailFile = servicesFile.Remove(fileData.id);
                }

                string filePath = Path.Combine(path, newFilePath);
                // Save the File asynchronously.
                await Task.Run(() => postedFile.SaveAs(filePath));

                result = true;
                var recordFile = new filemaster();
                recordFile.is_minio = false;
                recordFile.bucket_name = bucketName;
                recordFile.name = fullFileName;
                recordFile.realname = fullFileName;
                recordFile.extension = ext;
                recordFile.size = postedFile.ContentLength;
                recordFile.mimetype = mime;
                recordFile.filepath = newFilePathForFile;
                recordFile.application_entity_id = AppEntityId;
                recordFile.record_id = recordId;
                recordFile.organization_id = this._services.dataContext.OrganizationId;
                result &= this._services.dataContext.SaveEntity<filemaster>(recordFile, true);
                name = recordFile.id;

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }

            return name;
        }


        public async Task<bool> uploadPicture(string pathObject, HttpPostedFile postedFile, string recordId, string columnTarget = "filemaster_id")
        {
            var result = false;
            try
            {
                // bool found = await minioClient.BucketExistsAsync(bucketName);
                // if (!found)
                // {
                //     await CreateBucket(minioClient, bucketName);
                // }

                // Create the Directory.
                string path = ConfigurationManager.AppSettings["UploadPicturePath"].ToString();

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string fullFileName = postedFile.FileName;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string ext = Path.GetExtension(postedFile.FileName);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile.FileName);

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                AppEntityName = "ProfilePicture";
                string fileName = Path.GetFileName(postedFile.FileName);
                string newFilePath = string.Format("{0}{1}", recordId, ext);
                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newFilePath);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                if (fileData?.filepath == newFilePath)
                {
                    var servicesFile = new FileMasterServices(this._services.dataContext);
                    var detailFile = servicesFile.Remove(fileData.id);
                }

                string filePath = Path.Combine(path, newFilePath);
                // Save the File asynchronously.
                await Task.Run(() => postedFile.SaveAs(filePath));

                // Uncomment if you use Minio asynchronously
                // await minioClient.PutObjectAsync(bucketName, newPathObject, postedFile.InputStream, postedFile.ContentLength, mime, metaData);

                result = true;
                var recordFile = new filemaster();
                recordFile.is_minio = false;
                recordFile.bucket_name = bucketName;
                recordFile.name = fullFileName;
                recordFile.realname = fullFileName;
                recordFile.extension = ext;
                recordFile.size = postedFile.ContentLength;
                recordFile.mimetype = mime;
                recordFile.filepath = newFilePath;
                recordFile.application_entity_id = AppEntityId;
                recordFile.record_id = recordId;
                recordFile.organization_id = this._services.dataContext.OrganizationId;
                result &= this._services.dataContext.SaveEntity<filemaster>(recordFile, true);

                if (result && !string.IsNullOrEmpty(recordFile.id))
                {
                    var appObj = Activator.CreateInstance<TEntity>();
                    var entityRecord = this._services.db.FirstOrDefault<TEntity>("WHERE id=@0", recordId);

                    CommonTools.Helper.SetPropertyByName.SetPropValue<TEntity>(entityRecord, columnTarget, recordFile.id);
                    result &= this._services.dataContext.SaveEntity<TEntity>(entityRecord, false);
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }

        public async Task<ApiResponse> UploadWithReturn(string pathObject, System.Web.HttpPostedFile postedFile, string recordId, string columnTarget = "filemaster_id")
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            var service = new FileMasterServices(_services.dataContext);
            try
            {
                bool found = await minioClient.BucketExistsAsync(bucketName);
                if (!found)
                {
                    await CreateBucket(minioClient, bucketName);
                }
                string fullFileName = postedFile.FileName;
                string fileNameOnly = Path.GetFileNameWithoutExtension(fullFileName);
                string extension = Path.GetExtension(postedFile.FileName);
                string mime = CommonTools.Helper.MimeTypeMap.GetMimeType(postedFile.FileName);

                #region Meta Data / Etag
                var metaData = new Dictionary<string, string>();
                metaData.Add("X-UploadedBy", this._services.dataContext.AppUsername);
                metaData.Add("X-recordId", recordId);
                #endregion

                var newPathObject =
                    string.IsNullOrEmpty(pathObject) ?
                    string.Format("{0}/{1}/{2}", AppEntityName, recordId, postedFile.FileName) :
                    string.Format("{0}/{1}/{2}{3}", AppEntityName, recordId, pathObject, postedFile.FileName);

                var sql = Sql.Builder.Append(filemaster.DefaultView);
                sql.Append(" WHERE  r.is_active=@0 AND r.filepath=@1", true, newPathObject);
                var fileData = this._services.db.FirstOrDefault<vw_filemaster>(sql);

                if (fileData?.filepath == newPathObject)
                {
                    var servicesFile = new FileMasterServices(this._services.dataContext);
                    var detailFile = servicesFile.Remove(fileData.id);
                }


             await minioClient.PutObjectAsync(bucketName, newPathObject, postedFile.InputStream, postedFile.ContentLength, mime, metaData);
                var isNew = false;
                var recordFile = new filemaster();
                recordFile.is_minio = true;
                recordFile.bucket_name = bucketName;
                recordFile.name = fullFileName;
                recordFile.realname = fullFileName;
                recordFile.extension = extension;
                recordFile.size = postedFile.ContentLength;
                recordFile.mimetype = mime;
                recordFile.filepath = newPathObject;
                recordFile.application_entity_id = AppEntityId;
                recordFile.record_id = recordId;
                recordFile.organization_id = this._services.dataContext.OrganizationId;
                result.Status.Success = service.SaveEntity(recordFile, ref isNew, (r) => recordFile.id = r.id);

                if (result.Status.Success & !string.IsNullOrEmpty(recordFile.id))
                {
                    var appObj = Activator.CreateInstance<TEntity>();
                    var entityRecord = this._services.db.FirstOrDefault<TEntity>("WHERE id=@0", recordId);
                    CommonTools.Helper.SetPropertyByName.SetPropValue<TEntity>(entityRecord, columnTarget, recordFile.id);
                    result.Status.Success &= this._services.dataContext.SaveEntity<TEntity>(entityRecord, false);
                }

                if (result.Status.Success)
                {
                    result.Data = recordFile.id;
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public Stream Download(string bucketName, string objectName)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                var t = minioClient.GetObjectAsync(bucketName, objectName, (stream) =>
                {
                    stream.CopyTo(ms);
                });
                t.Wait();
                return ms;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                return null;
            }

        }

        public MinioFile GetFile(string fileId)
        {
            MinioFile result = new MinioFile();
            try
            {
                if (string.IsNullOrEmpty(fileId)) return null;

                var recordData = this.GetById(fileId);
                if (recordData == null) return null;
                var bucketName = recordData.bucket_name;
                var objName = recordData.filepath;
                var t = minioClient.GetObjectAsync(bucketName, objName, (stream) =>
                {
                    result.FileId = recordData.id;
                    result.FileName = recordData.name;
                    result.Extention = recordData.extension;
                    result.MimeType = recordData.mimetype;

                    stream.CopyTo(result.StreamFile);
                });
                t.Wait();
                return result;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                return null;
            }

        }

        public bool DeleteFile(string fileId)
        {
            var result = false;
            //  MinioFile result = new MinioFile();
            try
            {
                if (string.IsNullOrEmpty(fileId)) return false;

                var recordData = this.GetById(fileId);
                if (recordData == null) return false;
                var bucketName = recordData.bucket_name;
                var objName = recordData.filepath;
                var t = minioClient.RemoveObjectAsync(bucketName, objName);
                result = true;
                t.Wait();
                return result;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result = false;
            }
            return result;
        }

    }
}
