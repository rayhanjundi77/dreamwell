﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellBitServices : WellBitRepository
    {
        public WellBitServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(well_bit record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var lastRecord = this.GetFirstOrDefault(" AND well_id = @0 ORDER BY bit_number DESC ", record.well_id);
                    if (record.bit_number == null)
                    {
                        record.bit_number = lastRecord == null ? 1 : lastRecord.bit_number + 1;
                    }
                    result.Status.Success &= this.SaveEntity(record, ref isNew, (r) => record.id = r.id);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                            result.Status.Message = "The data has been added.";
                        else
                            result.Status.Message = "The data has been updated.";

                        result.Data = record;
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse<int> GetLastRun(string wellId, string drillingId, string wellBitId)
        {
            var result = new ApiResponse<int>();
            var service_drilling = new DrillingServices(_services.dataContext);
            try
            {
                //var record = service_drilling.GetById(drillingId);
                //var Qry = Sql.Builder.Append(@"
                //                            SELECT TOP 1 COALESCE(r.bit_run, 0) + 1 AS total
                //                            FROM drilling_bha_bit r
                //                            INNER JOIN drilling d ON d.well_id = r.well_id
                //                            WHERE d.drilling_date < @0 AND d.well_id = @1 AND well_bit_id = @2
                //                            ORDER BY d.drilling_date DESC ", record.drilling_date, wellId, wellBitId);


                var Qry = Sql.Builder.Append(@"
                                        SELECT TOP 1 COALESCE(r.bit_run, 0) + 1 AS total
                                        FROM drilling_bha_bit r
                                        WHERE r.drilling_id != @0 
                                        AND r.well_id = @1 
                                        AND r.well_bit_id = @2
                                        ORDER BY r.bit_run DESC ", drillingId, wellId, wellBitId);
                var bitRun = this._services.db.FirstOrDefault<int?>(Qry);
                if (bitRun == null || bitRun == 0)
                    result.Data = 1;
                else
                    result.Data = (int)bitRun;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public Page<vw_well_bit> Lookup(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.well_id=@0 ", wellId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY r.bit_number ASC ");
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
        public ApiResponsePage<vw_well_bit> LookupByWellBha(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            var result = new ApiResponsePage<vw_well_bit>();
            result.Items = this._services.db.Fetch<vw_well_bit>(@"select * from vw_well_bit as r where is_active=1 AND r.id IN (SELECT d.well_bit_id FROM drilling_bha_bit d WHERE d.well_bha_id = @0)  ", wellId);
            return result;
        }

        //public Page<vw_well_bit> LookupByWellBha(MarvelDataSourceRequest marvelDataSourceRequest, string wellBhaId)
        //{
        //    #region Use Filters
        //    var sqlFilter = Sql.Builder.Append(" AND r.id IN (SELECT d.well_bit_id FROM drilling_bha_bit d WHERE d.well_bha_id = @0) ", wellBhaId);
        //    if (marvelDataSourceRequest.Filter != null)
        //    {
        //        Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
        //        if (filterBuilder != null)
        //            sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
        //    }
        //    sqlFilter.Append(" ORDER BY r.bit_number ASC ");
        //    #endregion
        //    return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        //}
    }
}
