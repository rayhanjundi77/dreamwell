﻿using System;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class StoneServices : StoneRepository
    {
        public StoneServices(DataContext dataContext): base(dataContext){}

        /*Do not use this function on datatables*/
        public Page<vw_stone> GetViewPerPage(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public ApiResponse<vw_stone> GetUserDetail(string recordId)
        {
            var result = new ApiResponse<vw_stone>();
            try
            {
                var serviceProject = new StoneServices(this._services.dataContext);
                var record = serviceProject.GetViewById(recordId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = record;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }
        public ApiResponse Save(stone record) {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                
                result.Status.Success = SaveEntity(record, ref isNew,(r)=> recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public Page<vw_stone> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            var kosong = Sql.Builder.Append("  \n AND (\n ) ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null && filterBuilder.SQL != kosong.SQL)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY r.name ASC ");
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_stone> LookupAll(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            var kosong = Sql.Builder.Append("  \n AND (\n ) ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null && filterBuilder.SQL != kosong.SQL)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY r.name ASC ");
            #endregion
            return base.GetViewPerPage(1, 1000, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_stone> LookupUser(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 AND r.is_sysadmin IS NULL AND r.is_ldap=@0", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY COALESCE(r.last_name, r.first_name) ASC ");
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
