﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingTotalVolumeServices : DrillingTotalVolumeRepository
    {
        public DrillingTotalVolumeServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(drilling_total_volume record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<drilling_total_volume>> SaveList(List<drilling_total_volume> records, string drillingId)
        {
            var result = new ApiResponse<List<drilling_total_volume>>();
            var service_drilling = new DrillingServices(_services.dataContext);
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    var sql = Sql.Builder.Append(@"DELETE FROM drilling_total_volume where drilling_id = @0", drillingId);
                    this._services.db.Execute(sql);

                    records = records.Where(o => o != null).ToList();
                    result.Status.Success = true;

                    if (records.Count > 0)
                    {
                        foreach (var record in records)
                        {
                            var isNew = false;
                            record.drilling_id = drillingId;
                            result.Status.Success &= SaveEntity(record, ref isNew, (r) => record.id = r.id);
                        }

                        if (result.Status.Success)
                        {
                            var isNewDrilling = false;
                            var drilling = new drilling();
                            drilling.id = drillingId;
                            drilling.daily_mud_cost = records.Sum(r => r.mud_cost);
                            result.Status.Success &= service_drilling.SaveEntity(drilling, ref isNewDrilling, (r) => drilling.id = r.id);
                        }
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = records;
                        result.Status.Message = "Successfully Save Total Volume";
                    }

                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    appLogger.Error(ex);
                }

            }
            return result;
        }
    }
}
