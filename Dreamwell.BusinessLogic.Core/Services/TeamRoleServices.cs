﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using PetaPoco;
using CommonTools;
using Dreamwell.BusinessLogic.Core;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class TeamRoleServices : TeamRoleRepository
    {
        public TeamRoleServices(DataContext dataContext) : base(dataContext) { }


        public ApiResponse Save(team_role record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public DataTablesResponse GetListDataTablesRoleMember(DataTableRequest DataTableRequest, string teamId)
        {
            var qry = Sql.Builder.Append(" WHERE r.is_active=@0 AND r.team_id=@1", true, teamId);
            return base.GetListDataTables(DataTableRequest, qry);
        }

     
        public Page<vw_application_role> GetRoleAvailable(MarvelDataSourceRequest marvelDataSourceRequest, string teamId)
        {
            #region Use Filters
            var userService = new ApplicationRoleServices(_services.dataContext);
            var sqlFilter = Sql.Builder.Append(@" AND r.is_active=1 AND r.organization_id=@0 AND r.id NOT IN (SELECT ur.application_role_id FROM dbo.team_role ur WHERE ur.team_id=@1 AND ur.application_role_id=r.id) ", _services.dataContext.OrganizationId, teamId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = userService.MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return userService.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_team_role> GetProjectManager(MarvelDataSourceRequest marvelDataSourceRequest, string team_name)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(@" AND r.is_active=1 AND r.team_id IN (SELECT t.id FROM team t WHERE t.team_name LIKE @0 AND r.team_id = t.id)", "%" +team_name + "%");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
