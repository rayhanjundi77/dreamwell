﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public interface IServices<T>
    {
        T GetDetail(string id);
        //Boolean save();
    }
}
