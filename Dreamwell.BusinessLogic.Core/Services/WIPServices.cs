﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WIPServices : WIPRepository
    {
        public WIPServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(wip record)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var recordId = record.id;

                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {

                        if (isNew)
                        {
                            result.Status.Message = "The data has been added.";
                        }
                        else
                        {
                            //var last = service_history.GetAll(" AND is_active=@0 AND iadcwi_id=@1 ORDER BY rev DESC ", true, recordId).SingleOrDefault();
                            //history.rev = last.rev + 1;
                            result.Status.Message = "The data has been updated.";
                        }
                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse<List<vw_wip>> getByDetail(string wirid)
        {
            var result = new ApiResponse<List<vw_wip>>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder
                        .Append("SELECT * FROM vw_well_intervention_program")
                        .Append("WHERE wir_id = @0", wirid);

                    result.Data = db.Fetch<vw_wip>(sql);
                }
                result.Status.Success = true;
                result.Status.Message = "Data retrieval successful";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = "Failed to retrieve data: " + ex.Message;
            }
            return result;
        }


        public Page<vw_wip> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            var result = base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
            return result;
        }

    }
}
