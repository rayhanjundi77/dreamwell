﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using DataTables.Mvc;
using PetaPoco;
using Dreamwell.DataAccess;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class UserRoleServices : UserRoleRepository
    {
        public UserRoleServices(DataContext dataContext): base(dataContext){}

        public DataTablesResponse GetListDataTables(DataTableRequest DataTableRequest)
        {
            var qry = Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
            return base.GetListDataTables(DataTableRequest, qry);
        }

        public DataTablesResponse GetListDataTablesMember(DataTableRequest DataTableRequest,string roleId)
        {
            var qry = Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
            qry.Append(" AND r.application_role_id=@0 ",roleId);
            return base.GetListDataTables(DataTableRequest, qry);
        }

        public Page<vw_application_user> GetUserAvailable(MarvelDataSourceRequest marvelDataSourceRequest, string roleId)
        {
            #region Use Filters
            var userService = new ApplicationUserServices(_services.dataContext);
            var sqlFilter = Sql.Builder.Append(@"
                    AND r.is_active=1 AND r.organization_id=@0 AND r.id NOT IN (
                        SELECT ur.application_user_id FROM dbo.user_role ur WHERE ur.application_role_id=@1 AND ur.application_user_id=r.id
                    ) ", _services.dataContext.OrganizationId, roleId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = userService.MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return userService.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

    }
}
