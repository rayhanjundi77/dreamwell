﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using CommonTools.UnitOfMeasurement;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using System.Data.SqlClient;
using CommonTools.EeasyUI;
using CommonTools.Helper;
using Dreamwell.DataAccess.Models.Entity;
using Dreamwell.DataAccess.Repository;
using System.Text.RegularExpressions;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class SuccessRatioServices : SuccessRepository
    {
        private readonly DataContext _dataContext;

        public SuccessRatioServices(DataContext dataContext) : base(dataContext)
        {

            _dataContext = dataContext;
        }

        public ApiResponse Save(status_ratio record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, this._services.dataContext.PrimaryTeamId, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_status_ratio>> GetByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_status_ratio>>();
            try
            {
                if (string.IsNullOrWhiteSpace(wellId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Well ID cannot be empty.";
                    return result;
                }

                appLogger.Info($"Fetching data for Well ID: {wellId}");
                var sql = Sql.Builder.Append(@"
                SELECT *
                FROM vw_status_ratio
                WHERE well_id = @0
                ORDER BY created_on DESC", wellId);

                result.Data = this._services.db.Fetch<vw_status_ratio>(sql);
                appLogger.Info($"Retrieved {result.Data.Count} records.");
                result.Status.Success = true;
                result.Status.Message = "Data retrieved successfully.";
            }
            catch (Exception ex)
            {
                appLogger.Error($"Error in GetByWellId: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"Error retrieving data: {ex.Message}";
            }
            return result;
        }

    }
}
