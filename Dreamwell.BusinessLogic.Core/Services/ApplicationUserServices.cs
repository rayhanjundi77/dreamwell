﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using System.Transactions;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ApplicationUserServices : ApplicationUserRepository
    {
        public ApplicationUserServices(DataContext dataContext): base(dataContext){}

        /*Do not use this function on datatables*/
        public Page<vw_application_user> GetViewPerPage(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public ApiResponse<vw_application_user> GetUserDetail(string recordId)
        {
            var result = new ApiResponse<vw_application_user>();
            try
            {
                var serviceProject = new ApplicationUserServices(this._services.dataContext);
                var record = serviceProject.GetViewById(recordId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = record;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse Save(application_user record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    var TeamServices = new TeamServices(this._services.dataContext);
                    var userOldRecord = application_user.FirstOrDefault("WHERE id=@0", record.id);

                    if (string.IsNullOrEmpty(record.id) || string.IsNullOrWhiteSpace(record.id))
                    {
                        record.app_password = SysConfig.UserSetup.defaultPassword;
                    }

                    if (!string.IsNullOrEmpty(record.business_unit_id))
                    {
                        var teamRecord = TeamServices.GetTeamByBusinessUnit(record.business_unit_id);
                        if (teamRecord == null)
                            throw new Exception($"Business Unit does't have default team");

                        record.primary_team_id = teamRecord.id;
                        record.owner_id = teamRecord.id;
                    }


                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                    #region Team Member
                    var mustRegisterMember = false;
                    #region Check Member Exist on Primary Team
                    if (!isNew)
                    {
                        if (userOldRecord.business_unit_id != record.business_unit_id)
                        {
                            /*Remove from existing team*/
                            var qryDeleteMember = Sql.Builder.Append(@"DELETE FROM team_member WHERE application_user_id=@0 AND team_id=@1", record.id, TeamServices.GetTeamByBusinessUnit(userOldRecord.business_unit_id).id);
                            this._services.db.Execute(qryDeleteMember);

                            mustRegisterMember = true;
                        }
                    }
                    else
                    {
                        mustRegisterMember = true;
                    }

                    if (mustRegisterMember)
                    {
                        /*Register user to team member, First time new user or change business unit*/
                        var memberRecord = new team_member();
                        memberRecord.team_id = record.primary_team_id;
                        memberRecord.application_user_id = recordId;
                        result.Status.Success &= this._services.dataContext.SaveEntity<team_member>(memberRecord, true);
                    }

                    #endregion

                    #endregion
                   

                    if (result.Status.Success)
                    {
                        if (isNew)
                        {
                            result.Status.Message = "The data has been added.";
                        }
                        else
                        {
                            result.Status.Message = "The data has been updated.";
                        }
                    }
                    else
                    {
                        result.Status.Success = false;
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse updatePassword(application_user record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                string newPassword = string.Format("{0}", PasswordHash.CreateHash(record.app_password));
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                 
                    var userOldRecord = application_user.FirstOrDefault("WHERE id=@0", record.id);

                    // userOldRecord.app_password = newPassword;
                   // record.app_password = newPassword;

                 
                  
                    var hasil = false;
                    int situasi = 0;

                   
                    try
                    {

                        if (userOldRecord != null)
                        {

                            situasi = _services.db.Execute("UPDATE dbo.application_user SET app_password =@0 WHERE id= @1", newPassword, record.id);

                            if (situasi >= 1)
                            {
                                scope.Complete();
                                hasil = true;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                        throw;
                    }


                    result.Status.Success = hasil;

                    if (result.Status.Success)
                    {
                        result.Status.Message = "The new password has been update.";
                    }
                    else
                    {
                        result.Status.Success = false;
                    }


                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        //public ApiResponse Save(application_user record) {
        //    var result = new ApiResponse();
        //    try
        //    {
        //        var isNew = false;
        //        var recordId = record.id;
        //        if (string.IsNullOrEmpty(record.id) || string.IsNullOrWhiteSpace(record.id))
        //        {
        //            record.app_password = SysConfig.UserSetup.defaultPassword;
        //        }
        //        result.Status.Success = SaveEntity(record, ref isNew,(r)=> recordId = r.id);
        //        if (result.Status.Success)
        //        {
        //            if (isNew)
        //            {
        //                result.Status.Message = "The data has been added.";
        //            }
        //            else
        //            {
        //                result.Status.Message = "The data has been updated.";
        //            }
        //            result.Data = new
        //            {
        //                recordId
        //            };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        result.Status.Success = false;
        //        result.Status.Message = ex.Message;
        //        appLogger.Error(ex);
        //    }
        //    return result;
        //}

        public Page<vw_application_user> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY COALESCE(r.last_name, r.first_name) ASC ");
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_application_user> LookupUser(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 AND r.is_sysadmin IS NULL AND r.is_ldap=@0", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY COALESCE(r.last_name, r.first_name) ASC ");
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
