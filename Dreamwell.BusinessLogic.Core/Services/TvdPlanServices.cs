﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using System.Transactions;
using System.Collections.Generic;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class TVDPlanServices : TvdPlanRepository
    {
        public TVDPlanServices(DataContext dataContext) : base(dataContext) { }

        /*Do not use this function on datatables*/
        public Page<vw_tvd_plan> GetViewPerPage(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public ApiResponse<vw_tvd_plan> GetUserDetail(string recordId)
        {
            var result = new ApiResponse<vw_tvd_plan>();
            try
            {
                var serviceProject = new TVDPlanServices(this._services.dataContext);
                var record = serviceProject.GetViewById(recordId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = record;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse<well> Save(WellViewModel record)
        {
            var result = new ApiResponse<well>();
            result.Status.Success = true;
            var service_well = new WellServices(_services.dataContext);
            try
            {
                var wellRecord = new well();
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    if (record.tvd_plan != null && record.tvd_plan.Count > 0)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM tvd_plan where well_id = @0", record.well.id);
                        this._services.db.Execute(sql);

                        short seq = 1;
                        foreach (tvd_plan item in record.tvd_plan)
                        {
                            bool isNew = false;
                            item.well_id = record.well.id;
                            item.no = seq;
                            result.Status.Success &= SaveEntity(item, ref isNew, (r) => item.id = r.id);
                            seq++;
                        }

                        var isNewWell = false;
                        wellRecord.id = record.well.id;
                        wellRecord.planned_days = record.tvd_plan[record.tvd_plan.Count - 1].days;
                        wellRecord.planned_td = record.tvd_plan[record.tvd_plan.Count - 1].depth;
                        result.Status.Success &= service_well.SaveEntity(wellRecord, ref isNewWell, (r) => wellRecord.id = r.id);
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Time vs Depth & Cost has been changed.";
                        result.Data = wellRecord;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public Page<vw_tvd_plan> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            var kosong = Sql.Builder.Append("  \n AND (\n ) ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null && filterBuilder.SQL != kosong.SQL)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_tvd_plan> LookupUser(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 AND r.is_sysadmin IS NULL AND r.is_ldap=@0", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY COALESCE(r.last_name, r.first_name) ASC ");
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
