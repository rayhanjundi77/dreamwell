﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingBhaBitServices : DrillingBhaBitRepository
    {
        public DrillingBhaBitServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(List<drilling_bha_bit> records)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {

                    records = records.Where(o => o != null).ToList();

                    if (records.Count > 0)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_bha_bit where drilling_id = @0", records.FirstOrDefault().drilling_id);
                        this._services.db.Execute(sql);
                        foreach (var item in records)
                        {
                            var isNew = false;
                            result.Status.Success &= SaveEntity(item, ref isNew, (r) => item.id = r.id);
                        }

                        if (result.Status.Success)
                        {
                            scope.Complete();
                            result.Status.Message = "Drilling Bha and Bit has been saved.";
                        }
                    } 
                    else
                    { 
                        result.Status.Message = "Drilling Bha and Bit record cannot be empty.";
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }
        public ApiResponse<List<vw_drilling_bha_bit_component_detail>> GetBhaAndBitDetail(string wellId, DateTime startdate, DateTime enddate)
        {
            var result = new ApiResponse<List<vw_drilling_bha_bit_component_detail>>();
            try
            {
                var sql = Sql.Builder.Append(@"SELECT k.bha_no,wb.bit_number,wb.bit_run, r.depth_in,r.depth_out ,com.component_detail,com.joint, com.n_id,com.n_od, com.length,com.cumm_length, d.drilling_date
                            FROM drilling_bha_bit r
							LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
						    LEFT OUTER JOIN well_bit wb ON r.well_bit_id = wb.id
							LEFT OUTER JOIN well_bha k ON r.well_bha_id = k.id
							LEFT OUTER JOIN well_bha_component com ON k.id = com.well_bha_id
                            WHERE r.well_id =  @0 AND (d.drilling_date BETWEEN  @1 AND  @2)", wellId, startdate, enddate);
                result.Data = this._services.db.Fetch<vw_drilling_bha_bit_component_detail>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_bha_bit>> getBhaMudloginData(string wellId, DateTime startdate, DateTime enddate)
        {
            var result = new ApiResponse<List<vw_drilling_bha_bit>>();
            try
            {
                var sql = Sql.Builder.Append(@"SELECT r.mudlogging_wob_max,r.mudlogging_wob_min,r.mudlogging_rpm_max,r.mudlogging_rpm_min,
							r.mudlogging_dhrpm_max,r.mudlogging_dhrpm_min,r.mudlogging_torque_max,
							r.mudlogging_torque_min,r.mudlogging_flowrate_max,r.mudlogging_flowrate_min,
							r.mudlogging_spp_max,r.mudlogging_spp_min,r.mudlogging_spm_max,r.mudlogging_spm_min,
							r.mudlogging_spmpress_max,r.mudlogging_spmpress_min, d.drilling_date
                            FROM drilling_bha_bit r
							LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
						    LEFT OUTER JOIN well_bit wb ON r.well_bit_id = wb.id
							LEFT OUTER JOIN well_bha k ON r.well_bha_id = k.id
							LEFT OUTER JOIN well_bha_component com ON k.id = com.well_bha_id
                            WHERE r.well_id =  @0 AND (d.drilling_date BETWEEN  @1 AND  @2)", wellId, startdate, enddate);
                result.Data = this._services.db.Fetch<vw_drilling_bha_bit>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse<List<vw_drilling_bha_bit>> GetDetail(string drillingId)
        {
            var result = new ApiResponse<List<vw_drilling_bha_bit>>();
            try
            {
                result.Data = this.GetViewAll(" AND r.drilling_id = @0 ", drillingId);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_bha_bit>> GetDetailMud(string drillingId)
        {
            var result = new ApiResponse<List<vw_drilling_bha_bit>>();
            try
            {
                result.Data = this.GetViewAll(" AND r.well_id = @0  order by drilling_date asc", drillingId);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                appLogger.Error(ex);
            }
            return result;
        }


        public ApiResponse<List<vw_drilling_bha_bit>> GetDetailMudPdf(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_bha_bit>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                            SELECT 
                                r.bit_number,
                                r.bit_run,
                                r.bha_no,
                                r.mudlogging_avg_rop_min,
                                r.mudlogging_avg_rop_max,
                                r.mudlogging_wob_min,
                                r.mudlogging_wob_max,
                                r.mudlogging_rpm_min,
                                r.mudlogging_rpm_max,
                                r.mudlogging_spp_min,
                                r.mudlogging_spp_max,
                                r.mudlogging_flowrate_min,
                                r.mudlogging_flowrate_max,
                                r.mudlogging_spmpress_min,
                                r.mudlogging_spmpress_max,
                                wb.bit_size,
                                wb.manufacturer,
                                wb.bit_type,
                                wb.iadc_code,
                                wb.serial_number,
                                wb.noozle_1,
                                wb.depth_in,
                                wb.depth_out,
                                wb.depth_out - wb.depth_in as segtiga,
                                wb.TFA,
                                wb.dull_grade
                            FROM 
                                vw_drilling_bha_bit r
                            JOIN 
                                vw_well_bit wb ON r.well_bit_id = wb.id
                            WHERE 
                                r.well_id = @0
                            ORDER BY r.drilling_date asc
                            ", wellId);
                result.Data = this._services.db.Fetch<vw_drilling_bha_bit>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
