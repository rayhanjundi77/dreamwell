﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingOperationActivityServices : DrillingOperationActivityRepository
    {
        public DrillingOperationActivityServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(drilling_operation_activity record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse<List<vw_drilling_hole_and_casing>> GetActivityByDrillingId(string drillingId, string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
            try
            {
                var service_drillingHoleAndCasing = new DrillingHoleAndCasingServices(_services.dataContext);
                var myData = new List<vw_drilling_hole_and_casing>();
                //ini harus di cek isianya
                var sql = Sql.Builder.Append(@"
                    SELECT * 
                    FROM vw_drilling_operation_activity
                    WHERE drilling_id = @0
                    ORDER BY drilling_date, created_on", drillingId);
                var drillingOperationActivity = this._services.db.Fetch<vw_drilling_operation_activity>(sql); ;

                if (drillingOperationActivity == null || drillingOperationActivity.Count <= 0)
                {
                    var drillingHoleCasingRecord = service_drillingHoleAndCasing.GetViewFirstOrDefault(" AND r.well_id = @0 AND r.cement_val IS NULL AND r.id IN (SELECT drilling_hole_and_casing_id FROM drilling_operation_activity do INNER JOIN drilling d ON d.id = do.drilling_id) ORDER BY r.created_on DESC", wellId);
                    if (drillingHoleCasingRecord != null)
                        myData.Add(drillingHoleCasingRecord);
                }
                else
                {
                    myData = (drillingOperationActivity.GroupBy(r => new { r.drilling_hole_and_casing_id, r.well_hole_and_casing_id, r.hole_name, r.hole_val, r.hole_depth, r.casing_name, r.casing_val, r.lot_fit, r.lot_fit_val, r.cement_val }, (key, groupData) => new vw_drilling_hole_and_casing()
                    {
                        id = key.drilling_hole_and_casing_id,
                        well_hole_and_casing_id = key.well_hole_and_casing_id,
                        hole_name = key.hole_name,
                        hole_depth = key.hole_depth,
                        hole_val = key.hole_val,
                        casing_name = key.casing_name,
                        casing_val = key.casing_val,
                        lot_fit = key.lot_fit,
                        lot_fit_val = key.lot_fit_val,
                        cement_val = key.cement_val,
                        DrillingOperationActivities = (from r in groupData
                                                       select new vw_drilling_operation_activity()
                                                       {
                                                           id = r.id,
                                                           drilling_id = r.drilling_id,
                                                           depth = r.depth,
                                                           iadc_id = r.iadc_id,
                                                           iadc_code = r.iadc_code,
                                                           iadc_description = r.iadc_description,
                                                           type = r.type,
                                                           type_desc = r.type == "1" ? "PT" : "NPT",
                                                           category = r.category,
                                                           operation_start_date = r.operation_start_date,
                                                           operation_end_date = r.operation_end_date,
                                                           description = r.description,
                                                           drilling_hole_and_casing_id = "",
                                                       }).OrderBy(x => x.operation_start_date).ToList()
                    }).ToList());
                }

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                //if (myData.Count > 0)
                //    myData.OrderBy(r => r.created_on);
                result.Data = myData;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = "Error";
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_operation_activity>> GetDrillingOperationActivity(string drillingId)
        {
            var result = new ApiResponse<List<vw_drilling_operation_activity>>();

            try
            {
                var sql = Sql.Builder.Append(@"
             SELECT *
             FROM vw_drilling_operation_activity
             WHERE drilling_id = @0", drillingId);
                result.Data = this._services.db.Fetch<vw_drilling_operation_activity>(sql);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse<List<vw_drilling_hole_and_casing>> GetDrillingOperationActivityes(string drillingId, string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();

            try
            {
                var service_drillingHoleAndCasing = new DrillingHoleAndCasingServices(_services.dataContext);
                var myData = new List<vw_drilling_hole_and_casing>();


                var drillingHoleCasingRecord = service_drillingHoleAndCasing;



            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_hole_and_casing>> GetActivityByDrillingWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                    SELECT *
                    FROM vw_drilling_hole_and_casing
                    WHERE well_id = @0", wellId);
                result.Data = this._services.db.Fetch<vw_drilling_hole_and_casing>(sql);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_hole_and_casing>> GetActivityByDrillingWellIds(string drillingId, string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                        SELECT
                            dhc.id,
                            dhc.well_hole_and_casing_id,
                            dhc.hole_name,
                            MAX(dhc.hole_val) AS hole_val,
                            MAX(dhc.hole_depth) AS hole_depth,
                            dhc.casing_name,
                            MAX(dhc.casing_val) AS casing_val,
                            MAX(dhc.lot_fit) AS lot_fit,
                            MAX(dhc.lot_fit_val) AS lot_fit_val,
                            MAX(dhc.cement_val) AS cement_val,
                            MAX(doa.drilling_id) AS drilling_id,
                            MAX(doa.operation_start_date) AS operation_start_date,
                            MAX(doa.operation_end_date) AS operation_end_date
                        FROM
                            vw_drilling_hole_and_casing dhc
                        LEFT JOIN
                            vw_drilling_operation_activity doa ON dhc.id = doa.drilling_hole_and_casing_id
                        WHERE
                            doa.drilling_id = @0
                            AND dhc.well_id = @1
                        GROUP BY
                            dhc.id,
                            dhc.well_hole_and_casing_id,
                            dhc.hole_name,
                            dhc.casing_name
                        ORDER BY
                            MIN(doa.operation_start_date);", drillingId, wellId);
                result.Data = this._services.db.Fetch<vw_drilling_hole_and_casing>(sql);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_operation_activity>> GetDescOperation(string drillingId, string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_operation_activity>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                    SELECT
                         dhc.id,
                         dhc.well_hole_and_casing_id,
                         dhc.hole_name,
                         dhc.hole_val,
                         dhc.hole_depth,
                         dhc.casing_name,
                         dhc.casing_val,
                         dhc.lot_fit,
                         dhc.lot_fit_val,
                         dhc.cement_val,
                         doa.drilling_id,
                         doa.depth,
                         doa.iadc_id,
                         doa.iadc_code,
                         doa.iadc_description,
                         doa.type,
                         CASE doa.type WHEN '1' THEN 'PT' ELSE 'NPT' END AS type_desc,
                         doa.category,
                         doa.operation_start_date,
                         doa.operation_end_date,
                         doa.description,
                         '' AS drilling_hole_and_casing_id
                     FROM
                         vw_drilling_hole_and_casing dhc
                     LEFT JOIN
                         vw_drilling_operation_activity doa ON dhc.id = doa.drilling_hole_and_casing_id
                     WHERE
                         doa.drilling_id = @0
                         AND dhc.well_id = @1
                     ORDER BY
                         dhc.id, doa.operation_start_date
                  ", drillingId, wellId);
                result.Data = this._services.db.Fetch<vw_drilling_operation_activity>(sql);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }




        public ApiResponse<List<vw_drilling_operation_activity>> getStartDateAndAroundDate(string wellId, DateTime startdate, DateTime enddate)
        {
            var result = new ApiResponse<List<vw_drilling_operation_activity>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                            SELECT  r.*,k.*,hole.*,d.drilling_date,CONCAT(i.iadc_code,' - ',i.description) as iadc_description,i.type,i.category,
	                        CASE i.type WHEN 1 THEN 'PT' WHEN 2 THEN 'NPT'  END AS type_desc
                            FROM drilling_operation_activity r  
                            LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
							LEFT OUTER JOIN iadc i ON r.iadc_id = i.id
							LEFT OUTER JOIN drilling_hole_and_casing k ON r.drilling_hole_and_casing_id = k.id
	                        LEFT OUTER JOIN well_hole_and_casing s ON k.well_hole_and_casing_id = s.id
							LEFT OUTER JOIN hole_and_casing hole ON s.hole_and_casing_id = hole.id
                            WHERE d.well_id =  @0 AND (d.drilling_date BETWEEN  @1 AND  @2)  Order by r.created_on ASC", wellId, startdate, enddate);
                result.Data = this._services.db.Fetch<vw_drilling_operation_activity>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse<List<vw_drilling_operation_activity>> getByWellId(string wellId, DateTime drillingDate)
        {
            var result = new ApiResponse<List<vw_drilling_operation_activity>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                            SELECT r.*, CONCAT(i.iadc_code,' - ',i.description) as iadc_description,i.type,i.category,
	                        CASE i.type WHEN 1 THEN 'PT' WHEN 2 THEN 'NPT'  END AS type_desc
                            FROM drilling_operation_activity r
                            LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
							LEFT OUTER JOIN iadc i ON r.iadc_id = i.id
                            WHERE d.well_id = @0 AND d.drilling_date = @1", wellId, drillingDate);
                result.Data = this._services.db.Fetch<vw_drilling_operation_activity>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_operation_activity>> getByWellIdOnly(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_operation_activity>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                                SELECT 
                                      r.drilling_id, 
                                      r.drilling_hole_and_casing_id, 
                                      dh.well_hole_and_casing_id, 
                                      d.drilling_date, 
                                      r.*, 
                                      CONCAT(
                                        i.iadc_code, ' - ', i.description
                                      ) as iadc_description, 
                                      i.type, 
                                      i.category, 
                                      CASE i.type WHEN 1 THEN 'PT' WHEN 2 THEN 'NPT' END AS type_desc 
                                    FROM 
                                      drilling_operation_activity r 
                                      LEFT OUTER JOIN drilling d ON r.drilling_id = d.id 
                                      LEFT OUTER JOIN dbo.drilling_hole_and_casing AS dh ON r.drilling_hole_and_casing_id = dh.id 
                                      LEFT OUTER JOIN dbo.well_casing AS wc ON wc.id = dh.well_hole_and_casing_id 
                                      LEFT OUTER JOIN dbo.casing AS c ON wc.casing_id = c.id 
                                      LEFT OUTER JOIN iadc i ON r.iadc_id = i.id  
                                WHERE d.well_id = @0
                                AND dh.multicasing is null", wellId);
                // switch =  wc.id as well_hole_and_casing_id
                result.Data = this._services.db.Fetch<vw_drilling_operation_activity>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
