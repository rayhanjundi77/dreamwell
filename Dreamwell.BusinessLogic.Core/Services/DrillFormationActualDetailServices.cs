﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using System.Transactions;
using System.Text;
using System.Reflection;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillFormationActualDetailServices : DrillFormationsActualDetailRepository
    {
        public DrillFormationActualDetailServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse GetDetailListByActualId(string actualid){
            ApiResponse result = new ApiResponse();
            var qry = Sql.Builder.Append("Select * from drill_formations_actual_detail where drill_formation_actual_id=@0 ", actualid);
            System.Collections.Generic.List<drill_formations_actual_detail> listActual = drill_formations_actual_detail.Fetch(qry);
            result.Data = listActual;
            return result;
        }
 
        public ApiResponse SaveDetailActual(DrillingViewModel record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    if (record.drill_Formations_Actual_Detail != null)
                    {
                        if(CheckPermission()==0)
                        {
                            throw new Exception("User doesn't have permission to create this entity");
                        }

                        string drill_formation_actual_id = "";
                        foreach (drill_formations_actual_detail detail_actual in record.drill_Formations_Actual_Detail)
                        {
                            drill_formation_actual_id = detail_actual.drill_formation_actual_id;
                        }

                        var sql = Sql.Builder.Append(@"DELETE FROM drill_formations_actual_detail WHERE drill_formation_actual_id = @0", drill_formation_actual_id);
                        this._services.db.Execute(sql);

                        int seq = 1;
                        foreach (drill_formations_actual_detail detail_actual in record.drill_Formations_Actual_Detail)
                        {
                            sql = Sql.Builder.Append(@"
                                            INSERT INTO drill_formations_actual_detail
                                                       ([id]
                                                       ,[created_by]
                                                       ,[created_on]
                                                       ,[modified_by]
                                                       ,[modified_on]
                                                       ,[is_active]
                                                       ,[is_locked]
                                                       ,[is_default]
                                                       ,[owner_id]
                                                       ,[approved_by]
                                                       ,[approved_on]
                                                       ,[organization_id]
                                                       ,[well_id]
                                                       ,[percentage]
                                                       ,[seq]
                                                       ,[drilling_id]
                                                       ,[drill_formation_actual_id]
                                                       ,[formation_data]
                                                       ,[stone_id])
                                                 VALUES
                                                       (@0
                                                       ,@1
                                                       ,@2
                                                       ,@3
                                                       ,@4
                                                       ,@5
                                                       ,@6
                                                       ,@7
                                                       ,@8
                                                       ,@9
                                                       ,@10
                                                       ,@11
                                                       ,@12
                                                       ,@13
                                                       ,@14
                                                       ,@15
                                                       ,@16
                                                       ,@17
                                                       ,@18)",
                                                       Guid.NewGuid().ToString(),
                                                       this._services.dataContext.AppUserId,
                                                       DateTime.Now,
                                                       null,
                                                       null,
                                                       true,
                                                       null,
                                                       null,
                                                       this._services.dataContext.AppUserId,
                                                       null,
                                                       null,
                                                       this._services.dataContext.OrganizationId,
                                                       detail_actual.well_id,
                                                       detail_actual.percentage,
                                                       seq,
                                                       detail_actual.drilling_id,
                                                       detail_actual.drill_formation_actual_id,
                                                       detail_actual.formation_data,
                                                       detail_actual.stone_id
                                                       );
                            this._services.db.Execute(sql);

                            //bool isNew = true;
                            //detail_actual.well_id = detail_actual.well_id;
                            //detail_actual.drilling_id = detail_actual.drilling_id;
                            //detail_actual.seq = seq;
                            //detail_actual.drill_formation_actual_id = detail_actual.drill_formation_actual_id;
                            //detail_actual.percentage = detail_actual.percentage;
                            //detail_actual.stone_id = detail_actual.stone_id;
                            //result.Status.Success &= SaveEntity(detail_actual, ref isNew, (r) => detail_actual.id = r.id);

                            //var sql = Sql.Builder.Append(@"SELECT SUM(r2.access_create) FROM dbo.team_role r INNER JOIN role_access r2 ON r2.application_role_id = r.application_role_id AND r2.application_entity_id = '3891e889-74c1-c07a-bba3-184ca1ebe84b' WHERE r.team_id IN( SELECT team_id FROM team_member WHERE application_user_id = @0 UNION SELECT id FROM team WHERE team_leader = @0 ) AND r.is_active = 1WHERE r.team_id IN( SELECT team_id FROM team_member WHERE application_user_id = @0 UNION SELECT id FROM team WHERE team_leader = @0 ) AND r.is_active = 1", this._services.dataContext.AppUserId);
                            //var result = this._services.db.ExecuteScalar<int>(sql);
                            //if (string.IsNullOrEmpty(((IBaseRecord)detail_actual).id)
                            //    || string.IsNullOrWhiteSpace(((IBaseRecord)detail_actual).id))
                            //    ((IBaseRecord)record).id = Guid.NewGuid().ToString();

                            //if (((IBaseRecord)record).is_active == null) ((IBaseRecord)detail_actual).is_active = true;
                            //if (((IBaseRecord)record).created_by == null) ((IBaseRecord)detail_actual).created_by = _services.dataContext.AppUserId;
                            //if (((IBaseRecord)record).created_on == null) ((IBaseRecord)detail_actual).created_on = DateTime.Now;

                            //((IBaseRecord)record).modified_by = null;
                            //((IBaseRecord)record).modified_on = null;

                            //((IBaseRecord)record).owner_id = _services.dataContext.AppUserId;

                            seq++;
                        }
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Success = true;
                        result.Status.Message = "Drilling Actual Detail Formation has been updated.";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }

            return result;
        }

        private long CheckPermission()
        {
            if (this._services.dataContext.IsSystemAdministrator)
            {
                return 1;
            }

            var sql = Sql.Builder.Append(@"SELECT SUM(r2.access_create)
                                            FROM dbo.team_role r
                                            INNER JOIN role_access r2 ON (r2.application_role_id = r.application_role_id
                                                                          AND r2.application_entity_id = '3891e889-74c1-c07a-bba3-184ca1ebe84b')
                                            WHERE r.team_id IN
                                                (SELECT team_id
                                                 FROM team_member
                                                 WHERE application_user_id = @0
                                                 UNION SELECT id
                                                 FROM team
                                                 WHERE team_leader = @0 )
                                              AND r.is_active = 1", this._services.dataContext.AppUserId);
            var result = this._services.db.ExecuteScalar<long>(sql);
            if(result != 0)
            {
                return 1;
            }
            return 0;
        }
    }

}
