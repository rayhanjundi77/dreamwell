﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class HoleAndCasingServices : HoleAndCasingRepository
    {
        public HoleAndCasingServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(hole_and_casing record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (record.hole_type == null)
                    record.hole_type = false;
                if (record.hole_type ?? true)
                    record.casing_name = null;
                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public Page<vw_hole_and_casing> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            sqlFilter.Append(" ORDER BY r.hole_name, r.casing_name ASC ");
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
        public Page<vw_hole_and_casing> LookupByWellId(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 AND r.well_id=@1 ", _services.dataContext.OrganizationId, wellId);

            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }

            sqlFilter.Append(" ORDER BY r.hole_name ASC ");
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
       
    }
}
