﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using CommonTools.SmartNavigation;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ApplicationMenuServices : ApplicationMenuRepository
    {
        public ApplicationMenuServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(application_menu record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public List<application_menu_category> GetApplicationMenu()
        {
            var results = new List<application_menu_category>();
            try
            {
                var categories = application_menu_category.Fetch(" WHERE is_active=@0 ", true);

                if (categories.Count > 0 && categories != null)
                {
                    foreach (var category in categories)
                    {
                        category.ApplicationMenu = new List<JSTreeResponse<application_menu>>();

                        var sql = Sql.Builder.Append(@" SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 1
                                                        FROM application_menu r
                                                        WHERE r.is_active=@0 AND application_menu_category_id = @1 AND parent_menu_id is null 
                                                        ORDER BY order_index ASC", true, category.id);
                        var item = this._services.db.Fetch<application_menu>(sql);

                        List<string> childNodes = null;
                        int row = 0;
                        foreach (var detail in item)
                        {
                            var menu = new JSTreeResponse<application_menu>();
                            if (row == 0)
                                menu.State.Opened = true;

                            var children = GetDetailMenuByParentUnit(detail.id, ref childNodes);
                            menu.Id = detail.id;
                            menu.Text = detail.menu_name;
                            menu.Icon = detail.icon_class;
                            menu.ActionUrl = detail.action_url;
                            menu.State = new JSTreeState();
                            if (children.Count > 0 && children != null)
                            {
                                menu.State.Opened = true;
                                menu.Children = children;
                            }
                            category.ApplicationMenu.Add(menu);
                            row++;
                        }
                    }

                    results = categories;
                }

                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }
        public application_menu_category GetApplicationMenu(string applicationRoleId, string applicationModuleId, string categoryId)
        {
            var results = new application_menu_category();
            try
            {

                results = application_menu_category.FirstOrDefault(" WHERE is_active=@0 AND id=@1", true, categoryId);
                results.ApplicationMenu = new List<JSTreeResponse<application_menu>>();

                var sql = Sql.Builder.Append(@" SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 0
                                                        FROM application_menu r
                                                        WHERE r.is_active=@0 AND r.id NOT IN ( SELECT application_menu_id FROM role_access_menu WHERE application_role_id=@1 ) AND application_menu_category_id = @2 AND application_module_id=@3 AND parent_menu_id is null 
                                                        UNION
                                                        SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 1
                                                        FROM application_menu r
                                                        WHERE r.is_active=@0 AND r.id IN ( SELECT application_menu_id FROM role_access_menu WHERE application_role_id=@1 ) AND application_menu_category_id = @2 AND application_module_id=@3 AND parent_menu_id is null 
                                                        ORDER BY order_index ASC ", true, applicationRoleId, categoryId, applicationModuleId);
                var item = this._services.db.Fetch<application_menu>(sql);

                List<string> childNodes = null;
                int row = 0;
                foreach (var detail in item)
                {
                    var menu = new JSTreeResponse<application_menu>();
                    if (row == 0)
                        menu.State.Opened = true;

                    var children = GetDetailMenuByParentUnitChecked(detail.id, ref childNodes, applicationRoleId);
                    menu.Id = detail.id;
                    menu.Text = detail.menu_name;
                    menu.Icon = detail.icon_class;
                    menu.ActionUrl = detail.action_url;
                    menu.State = new JSTreeState();
                    menu.State.Checked = detail.is_checked ? true : false;
                    menu.State.selected = detail.is_checked ? true : false;
                    if (children.Count > 0 && children != null)
                    {
                        menu.State.Opened = true;
                        menu.Children = children;
                    }
                    results.ApplicationMenu.Add(menu);
                    row++;
                }


                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        //public List<application_menu_category> GetApplicationMenu(List<string> applicationRoleIds)
        //{
        //    //var applicationRoleIds = new List<string>();
        //    //if (filterRoleIds.Count > 0 && filterRoleIds != null)
        //    //    applicationRoleIds = filterRoleIds;

        //    var results = new List<application_menu_category>();
        //    try
        //    {
        //        var categories = application_menu_category.Fetch(" WHERE is_active=@0 ", true);

        //        if (categories.Count > 0 && categories != null)
        //        {
        //            foreach (var category in categories)
        //            {
        //                category.ApplicationMenu = new List<JSTreeResponse<application_menu>>();

        //                var sql = Sql.Builder.Append(@" 
        //                               SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 0
        //                               FROM application_menu r
        //                               WHERE r.is_active=@0 AND application_menu_category_id = @1 AND parent_menu_id is null AND r.id NOT IN ( SELECT application_menu_id FROM role_access_menu WHERE 1=1 ", true, category.id);

        //                int index = 0;
        //                foreach (var roleId in applicationRoleIds)
        //                {
        //                    if (index == 0)
        //                        sql.Append(@" AND application_role_id=@0 ", roleId);
        //                    else
        //                        sql.Append(@" OR application_role_id=@0 ", roleId);
        //                    index++;
        //                }

        //                sql.Append(@" ) ");
        //                sql.Append(@" UNION
        //                              SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 1
        //                              FROM application_menu r
        //                              WHERE r.is_active=@0 AND application_menu_category_id = @1 AND parent_menu_id is null AND r.id IN ( SELECT application_menu_id FROM role_access_menu WHERE 1=1 ", true, category.id);

        //                index = 0;
        //                foreach (var roleId in applicationRoleIds)
        //                {
        //                    if (index == 0)
        //                        sql.Append(@" AND application_role_id=@0 ", roleId);
        //                    else
        //                        sql.Append(@" OR application_role_id=@0 ", roleId);
        //                    index++;
        //                }

        //                sql.Append(@" ) ");
        //                sql.Append(@"UNION
        //                             SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, (SELECT COUNT(*) FROM (SELECT TOP 1 id FROM shared_record WHERE shared_to = @0 AND application_entity_id = @1) AS a) AS is_checked
        //                             FROM application_menu r
        //                             WHERE r.is_active=@2 AND menu_name = 'Project'
        //                             AND r.application_menu_category_id = @3
        //                             AND parent_menu_id is null ", _userContext.getUserDataContext().AppUserId, project.EntityId, true, category.id);
        //                sql.Append(@" ORDER BY order_index ASC ");
        //                var item = this._services.db.repo.Fetch<application_menu>(sql);

        //                List<string> childNodes = null;
        //                int row = 0;
        //                foreach (var detail in item)
        //                {
        //                    var menu = new JSTreeResponse<application_menu>();
        //                    if (row == 0)
        //                        menu.State.Opened = true;

        //                    var children = GetDetailMenuByParentUnitChecked(detail.id, ref childNodes, applicationRoleIds);
        //                    menu.Id = detail.id;
        //                    menu.Text = detail.menu_name;
        //                    menu.Icon = detail.icon_class;
        //                    menu.ActionUrl = detail.action_url;
        //                    menu.State = new JSTreeState();
        //                    menu.State.Checked = detail.is_checked ? true : false;
        //                    menu.State.selected = detail.is_checked ? true : false;
        //                    if (children.Count > 0 && children != null)
        //                    {
        //                        menu.State.Opened = true;
        //                        menu.Children = children;
        //                    }
        //                    category.ApplicationMenu.Add(menu);
        //                    row++;
        //                }
        //            }

        //            results = categories;
        //        }

        //        appLogger.Debug(JsonConvert.SerializeObject(results));
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        throw;
        //    }
        //    return results;
        //}

        public List<JSTreeResponse<application_menu>> GetTreeApplicationMenu()
        {
            List<JSTreeResponse<application_menu>> results = new List<JSTreeResponse<application_menu>>();
            try
            {
                List<application_menu> data = new List<application_menu>();
                data = application_menu.Fetch(" WHERE is_active=@0 AND parent_menu_id is null ORDER BY order_index ASC ", true);

                List<string> childNodes = null;
                int row = 0;
                foreach (var item in data)
                {
                    var menu = new JSTreeResponse<application_menu>();
                    if (row == 0)
                        menu.State.Opened = true;

                    var children = GetDetailMenuByParentUnit(item.id, ref childNodes);
                    menu.Id = item.id;
                    menu.Text = item.menu_name;
                    menu.Icon = "demo-pli-folder";
                    menu.Children = children;
                    menu.State = new JSTreeState();
                    results.Add(menu);
                    row++;
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }
        public List<JSTreeResponse<application_menu>> GetTreeApplicationMenu(string applicationModuleId, string categoryId)
        {
            List<JSTreeResponse<application_menu>> results = new List<JSTreeResponse<application_menu>>();
            try
            {
                List<application_menu> data = new List<application_menu>();
                data = application_menu.Fetch(" WHERE is_active=@0 AND application_menu_category_id=@1 AND parent_menu_id is null AND application_module_id=@2 ORDER BY order_index ASC ",
                    true, categoryId, applicationModuleId);

                List<string> childNodes = null;
                int row = 0;
                foreach (var item in data)
                {
                    var menu = new JSTreeResponse<application_menu>();
                    if (row == 0)
                        menu.State.Opened = true;

                    var children = GetDetailMenuByParentUnit(item.id, ref childNodes);
                    menu.Id = item.id;
                    menu.Text = item.menu_name;
                    menu.Icon = item.icon_class;
                    menu.Children = children;
                    results.Add(menu);
                    row++;
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }
        protected List<JSTreeResponse<application_menu>> GetDetailMenuByParentUnitChecked(String parentId, ref List<string> childNodes, string applicationRoleId)
        {
            List<JSTreeResponse<application_menu>> results = new List<JSTreeResponse<application_menu>>();
            if (childNodes == null)
            {
                childNodes = new List<string>();
                childNodes.Add(parentId);
            }
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                //var data = application_menu.Fetch(" WHERE parent_menu_id=@0", parentId);

                var sql = Sql.Builder.Append(@" SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 0
                                                FROM application_menu r
                                                WHERE r.is_active=@0 AND r.id NOT IN (SELECT application_menu_id FROM role_access_menu WHERE application_role_id=@1) AND r.parent_menu_id = @2
                                                UNION
                                                SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 1
                                                FROM application_menu r
                                                WHERE r.is_active=@0 AND r.id IN (SELECT application_menu_id FROM role_access_menu WHERE application_role_id=@1) AND r.parent_menu_id = @2
                                                ORDER BY r.order_index ASC ", true, applicationRoleId, parentId);
                var data = this._services.db.Fetch<application_menu>(sql);

                foreach (var item in data)
                {
                    if (childNodes.Contains(item.id)) continue;
                    var menu = new JSTreeResponse<application_menu>();
                    menu.Id = item.id;
                    menu.Text = item.menu_name;
                    menu.Icon = item.icon_class;
                    menu.ActionUrl = item.action_url;
                    menu.State = new JSTreeState();
                    menu.State.Checked = item.is_checked ? true : false;
                    menu.State.selected = item.is_checked ? true : false;
                    var children = GetDetailMenuByParentUnitChecked(item.id, ref childNodes, applicationRoleId);
                    if (children.Count > 0 && children != null)
                    {
                        menu.State.Opened = true;
                        menu.Children = children;
                    }

                    results.Add(menu);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }
        protected List<JSTreeResponse<application_menu>> GetDetailMenuByParentUnitChecked(String parentId, ref List<string> childNodes, List<string> applicationRoleIds)
        {
            List<JSTreeResponse<application_menu>> results = new List<JSTreeResponse<application_menu>>();
            if (childNodes == null)
            {
                childNodes = new List<string>();
                childNodes.Add(parentId);
            }
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var sql = Sql.Builder.Append(@" SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 0
                                                FROM application_menu r
                                                WHERE r.is_active=@0 AND r.parent_menu_id = @1 AND r.id NOT IN (SELECT application_menu_id FROM role_access_menu WHERE 1=1 ", true, parentId);
                int index = 0;
                foreach (var roleId in applicationRoleIds)
                {
                    if (index == 0)
                        sql.Append(@" AND application_role_id=@0 ", roleId);
                    else
                        sql.Append(@" OR application_role_id=@0 ", roleId);
                    index++;
                }

                sql.Append(@" ) ");
                sql.Append(@" UNION
                              SELECT r.id, r.menu_name, r.action_url, r.icon_class, r.order_index, is_checked = 1
                              FROM application_menu r
                              WHERE r.is_active=@0 AND r.parent_menu_id = @1 AND r.id IN (SELECT application_menu_id FROM role_access_menu WHERE 1=1 ", true, parentId);
                index = 0;
                foreach (var roleId in applicationRoleIds)
                {
                    if (index == 0)
                        sql.Append(@" AND application_role_id=@0 ", roleId);
                    else
                        sql.Append(@" OR application_role_id=@0 ", roleId);
                    index++;
                }
                sql.Append(@" ) ");
                sql.Append(@" ORDER BY r.order_index ASC ");

                var data = this._services.db.Fetch<application_menu>(sql);
                foreach (var item in data)
                {
                    if (childNodes.Contains(item.id)) continue;
                    var menu = new JSTreeResponse<application_menu>();
                    menu.Id = item.id;
                    menu.Text = item.menu_name;
                    menu.Icon = item.icon_class;
                    menu.ActionUrl = item.action_url;
                    menu.State = new JSTreeState();
                    menu.State.Checked = item.is_checked ? true : false;
                    menu.State.selected = item.is_checked ? true : false;
                    var children = GetDetailMenuByParentUnitChecked(item.id, ref childNodes, applicationRoleIds);
                    if (children.Count > 0 && children != null)
                    {
                        menu.State.Opened = true;
                        menu.Children = children;
                    }

                    results.Add(menu);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        protected List<JSTreeResponse<application_menu>> GetDetailMenuByParentUnit(String parentId, ref List<string> childNodes)
        {
            List<JSTreeResponse<application_menu>> results = new List<JSTreeResponse<application_menu>>();
            if (childNodes == null)
            {
                childNodes = new List<string>();
                childNodes.Add(parentId);
            }
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var data = application_menu.Fetch(" WHERE parent_menu_id=@0 ORDER BY order_index ASC ", parentId);
                foreach (var item in data)
                {
                    if (childNodes.Contains(item.id)) continue;
                    var menu = new JSTreeResponse<application_menu>();
                    menu.Id = item.id;
                    menu.Text = item.menu_name;
                    menu.Icon = item.icon_class;
                    menu.ActionUrl = item.action_url;
                    menu.State = new JSTreeState();
                    var children = GetDetailMenuByParentUnit(item.id, ref childNodes);
                    if (children != null)
                    {
                        menu.Children = children;
                    }
                    results.Add(menu);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public bool OrderTree(String recordId, String parentTargetId, int newOrder)
        {
            var result = false;

            try
            {
                this._services.db.Execute(";EXEC usp_application_menu_order @@id = @0 , @@order_index = @1 ,@@parent_unit_id = @2",
                    recordId, newOrder, parentTargetId);
                result = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }

        public CommonTools.SmartNavigation.SmartNavigation GetNavigationByUserId(string module, string category = "SIDEBAR")
        {
            var result = new CommonTools.SmartNavigation.SmartNavigation();

            result.Version = "4.0";

            try
            {
                var menu = new List<application_menu>();

                var sql = Sql.Builder.Append("SELECT * FROM application_menu");

                if (this._services.dataContext.IsSystemAdministrator)
                {
                    sql.Append(@"WHERE is_active=1 
                        AND application_module_id=@1 
                        AND application_menu_category_id=@0 
                        AND parent_menu_id IS NULL ",
                                application_menu_category.GetCategoryId(category), application_module.GetModuleId(module.ToUpper()));

                }
                else
                {
                    sql.Append(@"WHERE is_active=1 
                        AND application_module_id=@1 
                        AND application_menu_category_id=@0 
                        AND parent_menu_id IS NULL ",
                              application_menu_category.GetCategoryId(category), application_module.GetModuleId(module.ToUpper()));

                    sql.Append(@"AND id IN (
                                SELECT distinct rm.application_menu_id 
                                FROM dbo.role_access_menu rm
                                INNER JOIN dbo.application_role ar ON rm.application_role_id=ar.id
                                WHERE ar.id IN (SELECT ur.application_role_id 
				                            FROM dbo.user_role ur 
				                            WHERE ur.application_user_id=@0))", this._services.dataContext.AppUserId);

                    //sql.Append(@" AND id IN (
                    //    SELECT distinct rm.application_menu_id 
                    //    FROM dbo.role_access_menu rm
                    //    INNER JOIN dbo.application_role ar ON rm.application_role_id=ar.id
                    //    WHERE ar.id IN (SELECT ur.application_role_id FROM dbo.user_role ur WHERE ur.application_user_id=@0 )
                    //    )", this._services.dataContext.AppUserId);

                    //var service = new ApplicationMenuAccessRepository(_services.dataContext, this._services.dataContext.AppUserId);
                    //var _tempMenu = service.GetViewAll(" ORDER BY r.order_index ASC ");

                    //if (_tempMenu.Count > 0)
                    //{
                    //    foreach (var item in _tempMenu)
                    //    {
                    //        var _menu = new application_menu();
                    //        _menu.InjectFrom(item);
                    //        menu.Add(_menu);
                    //    }
                    //}
                }

                sql.Append(" ORDER BY order_index ASC");
                menu = application_menu.Fetch(sql);

                foreach (var m in menu)
                {
                    var childs = GetChilds(m.id);
                    result.Lists.Add(new ListItem
                    {
                        Title = m.menu_name,
                        IsGroup = m.is_group ?? false,
                        Href = m.action_url,
                        Icon = m.icon_class,
                        Items = childs
                    });
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }


            return result;
        }

        public CommonTools.SmartNavigation.SmartNavigation GetNavigationByUserId(string module, string bussinessUnitRoute, string category = "SIDEBAR")
        {
            var result = new CommonTools.SmartNavigation.SmartNavigation();
            result.Version = "4.0";

            var sql = Sql.Builder.Append("SELECT * FROM application_menu WHERE is_active=1 AND application_module_id=@1 AND application_menu_category_id=@0 AND parent_menu_id IS NULL ", application_menu_category.GetCategoryId(category), application_module.GetModuleId(module.ToUpper()));
            if (!this._services.dataContext.IsSystemAdministrator)
            {
                sql.Append(@" AND id IN (
                    SELECT distinct rm.application_menu_id FROM dbo.role_access_menu rm
                    INNER JOIN dbo.application_role ar ON rm.application_role_id=ar.id
                    WHERE ar.id IN (SELECT ur.application_role_id FROM dbo.user_role ur WHERE ur.application_user_id=@0 )
                    )", this._services.dataContext.AppUserId);
            }
            sql.Append(" ORDER BY order_index ASC");

            var menu = application_menu.Fetch(sql);

            foreach (var m in menu)
            {
                var childs = new List<ListItem>();
                if (m.action_url.Equals("::UTC.field.well_data") || m.action_url.Equals("::UTC.field.single_well"))
                {
                    //kenapa ya w bikin begini, jadinya kan gak muncul. masih di pikirkan
                    //AND parent_unit is null AND is_default is null

                    if (!string.IsNullOrEmpty(bussinessUnitRoute))
                    {
                        var bussinessUnitRouteId = business_unit.FirstOrDefault("WHERE unit_code=@0 ", bussinessUnitRoute)?.id;
                        childs = GetAsset(string.Empty, bussinessUnitRouteId, m.action_url);
                    }
                    else
                    {
                        childs = GetAsset(string.Empty, null, m.action_url);
                    }
                }
                else
                {
                    childs = GetChilds(m.id);
                }

                result.Lists.Add(new ListItem
                {
                    Title = m.menu_name,
                    IsGroup = m.is_group ?? false,
                    Href = m.action_url,
                    Icon = m.icon_class,
                    Items = childs
                });
            }


            return result;
        }

        protected List<ListItem> GetChilds(string parentId)
        {
            var result = new List<ListItem>();

            var sql = Sql.Builder.Append(" WHERE is_active=1 AND parent_menu_id =@0 ", parentId);

            if (!this._services.dataContext.IsSystemAdministrator)
            {
                sql.Append(@" AND id IN (
                    SELECT distinct rm.application_menu_id FROM dbo.role_access_menu rm
                    INNER JOIN dbo.application_role ar ON rm.application_role_id=ar.id
                    WHERE ar.id IN (SELECT ur.application_role_id FROM dbo.user_role ur WHERE ur.application_user_id=@0 )
                    )", this._services.dataContext.AppUserId);
            }
            sql.Append(" ORDER BY order_index ASC");
            var menu = application_menu.Fetch(sql);
            foreach (var m in menu)
            {
                var childs = GetChilds(m.id);

                if (childs.Count > 0)
                {
                    result.Add(new ListItem()
                    {
                        Title = m.menu_name,
                        Href = m.action_url,
                        Icon = m.icon_class,
                        Items = childs
                    });
                }
                else
                {
                    result.Add(new ListItem()
                    {
                        Title = m.menu_name,
                        Href = m.action_url,
                        Icon = m.icon_class
                    });
                }
            }


            return result;
        }

        #region getMenuByLogin
        //public List<application_menu_category> GetMenuByLogin()
        //{
        //    var result = new List<application_menu_category>();
        //    try
        //    {
        //        var user = this._userContext.getUserDataContext();
        //        if (user.IsSystemAdministrator)
        //        {
        //            result = GetApplicationMenu();
        //            foreach (var item in result)
        //            {
        //                item.ApplicationMenu.ForEach(r => r.State.selected = true);
        //                foreach (var detail in item.ApplicationMenu)
        //                    detail.Children.ForEach(r => r.State.selected = true);
        //            }
        //        }
        //        else
        //        {
        //            var sql = Sql.Builder.Append(@" SELECT r.application_role_id FROM vw_user_role r where r.application_user_id = @0 ", user.AppUserId);
        //            var data = this._services.db.Fetch<team_role>(sql).ToList();
        //            List<string> applicationRoleId = new List<string>();

        //            if (data.Count > 0 && data != null)
        //            {
        //                foreach (var item in data)
        //                {
        //                    applicationRoleId.Add(item.application_role_id);
        //                }
        //            }

        //            result = GetApplicationMenu(applicationRoleId);
        //        }
        //        appLogger.Debug(JsonConvert.SerializeObject(result));
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        throw;
        //    }
        //    return result;
        //}
        #endregion


        //public List<ListItem> GetAsset()
        //{
        //    List<ListItem> results = new List<ListItem>();
        //    try
        //    {
        //        var sql = Sql.Builder.Append(asset.DefaultView);

        //        sql.Append("WHERE r.is_active=1 ");
        //        sql.Append("ORDER BY r.asset_name asc ");
        //        var data = _services.db.Fetch<vw_asset>(sql);

        //        foreach (var m in data)
        //        {
        //            var childs = GetChilds(m.id);
        //            if (childs.Count > 0)
        //            {
        //                results.Add(new ListItem()
        //                {
        //                    Title = m.asset_name,
        //                    Href = "#",
        //                    Items = childs
        //                });
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        throw;
        //    }
        //    return results;
        //}

        public List<ListItem> GetAsset(string countryId, string businessUnitId, string action_url)
        {
            List<ListItem> results = new List<ListItem>();
            try
            {
                //var sql = Sql.Builder.Append(@"
                //        SELECT r.id,r.asset_code,r.asset_name, is_active = 1
                //        FROM dbo.asset as r 
                //        WHERE r.is_active=1 AND r.id IN (
                //         SELECT f.asset_id FROM dbo.business_unit_field uf 
                //         INNER JOIN dbo.field f ON uf.field_id=f.id
                //         WHERE uf.business_unit_id=@0 AND f.asset_id=r.id  GROUP BY f.asset_id)
                //        ORDER BY r.asset_name ASC", businessUnitId);

                var sql = Sql.Builder.Append(@"select r.id, r.asset_code, r.asset_name, 1 AS is_active
                            from asset r");
                if (!string.IsNullOrEmpty(businessUnitId))
                {
                    sql.Append(@"LEFT JOIN
                            (
	                            SELECT f.asset_id FROM dbo.business_unit_field uf 
	                            INNER JOIN dbo.field f ON ISNULL(uf.field_id,'')=f.id
	                            WHERE uf.business_unit_id=@0 GROUP BY f.asset_id
                            )
                            s ON r.id=s.asset_id
                            WHERE s.asset_id IS NOT NULL
                            ORDER BY r.asset_name ASC ", businessUnitId);
                }
                else
                {
                    sql.Append(@"INNER JOIN
                            (
	                            SELECT f.asset_id FROM dbo.business_unit_field uf 
	                            INNER JOIN dbo.field f ON ISNULL(uf.field_id,'')=f.id
	                            GROUP BY f.asset_id
                            )
                            s ON r.id=s.asset_id
                            ORDER BY r.asset_name ASC ");
                }


                var data = _services.db.Fetch<dynamic>(sql);
                foreach (var item in data)
                {
                    var childs = GetFieldTreeByBusinessUnit(item.id, businessUnitId, action_url);
                    if (childs.Count > 0)
                    {
                        results.Add(new ListItem()
                        {
                            Title = item.asset_name,
                            Href = "javascript:void(0);",
                            Items = childs
                        });
                    }
                }


            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }


        public List<ListItem> GetFieldTreeByBusinessUnit(string assetId, string businessUnitId, string action_url)
        {
            List<ListItem> results = new List<ListItem>();
            try
            {
                if (string.IsNullOrEmpty(assetId))
                {
                    return null;
                }

                //var sql = Sql.Builder.Append(@"
                //        SELECT r.id,r.field_name,r.asset_id, is_active = 1
                //        FROM dbo.field as r
                //        WHERE r.id IN (SELECT uf.field_id FROM dbo.business_unit_field uf WHERE uf.business_unit_id=@1 AND uf.field_id=r.id)
                //        AND r.asset_id=@0 AND r.is_active=1
                //        ", assetId, businessUnitId);



                var sql = Sql.Builder.Append(@"SELECT r.id,r.field_name,r.asset_id,  1 as is_active
                    FROM dbo.field as r ");

                if (!string.IsNullOrEmpty(businessUnitId))
                {
                    sql.Append(@"LEFT JOIN 
                    (
                    SELECT uf.field_id FROM dbo.business_unit_field uf WHERE uf.business_unit_id =@1
                    )
                    s
                    ON r.id = s.field_id
                    WHERE r.asset_id =@0 AND r.is_active = 1 AND ISNULL(s.field_id, '') <> ''", assetId, businessUnitId);
                }
                else
                {
                    sql.Append(@"WHERE r.asset_id =@0 AND r.is_active = 1", assetId);
                }

                sql.Append("ORDER BY r.field_name asc ");
                var data = _services.db.Fetch<dynamic>(sql);
                var businessUnitName = businessUnitId == null ? "APH" : business_unit.FirstOrDefault("WHERE id=@0", businessUnitId)?.unit_code;

                var WebURI = Infrastructure.Options.DreamwellSettings.WebServerUrl.Trim();
                if (WebURI.Substring(WebURI.Length - 1) != "/")
                {
                    WebURI = $"{WebURI}/";
                }
                foreach (var item in data)
                {
                    ListItem itemList = new ListItem();
                    itemList.Title = item.field_name;
                    if (action_url == "::UTC.field.well_data")
                        itemList.Href = string.Format("{0}UTC/{1}/WellData?field={2}&name={3}", WebURI, businessUnitName, item.id, item.field_name);
                    else if (action_url == "::UTC.field.single_well")
                        itemList.Href = string.Format("{0}UTC/{1}/singlewell?field={2}&name={3}", WebURI, businessUnitName, item.id, item.field_name);
                    itemList.Text = item.field_name;

                    results.Add(itemList);

                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }



    }


}
