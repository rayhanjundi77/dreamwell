﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingTransportServices : DrillingTransportRepository
    {
        public DrillingTransportServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(List<drilling_transport> records, string drillingId)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    var sql = Sql.Builder.Append(@"DELETE FROM drilling_transport where drilling_id = @0", drillingId);
                    this._services.db.Execute(sql);
                    if (records!= null)
                    {
                        records = records.Where(o => o != null).ToList();

                        if (records.Count <= 0)
                        {
                            throw new Exception("Drilling Transport cannot be empty.");
                        }

                        result.Status.Success = true;
                        foreach (var record in records)
                        {
                            var isNew = false;
                            result.Status.Success &= SaveEntity(record, ref isNew, (r) => record.id = r.id);
                        }

                        if (result.Status.Success)
                        {
                            scope.Complete();
                            result.Status.Message = "Successfully save drilling personel";
                        }
                    }
                    else
                    {
                        scope.Complete();
                        result.Status.Success = true;
                        result.Status.Message = "Successfully save drilling personel";
                    }

                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    appLogger.Error(ex);
                }

            }
            return result;

        }

    }
}
