﻿using DataTables.Mvc;
using Dreamwell.DataAccess.Repository;
using Dreamwell.Infrastructure.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DataTableService
    {
        public DataTablesResponse generate<T>(DataTableRequest dataTableRequest, string query = "")
        {
            DatatablesRepository  repo = new DatatablesRepository();
            return repo.generate<T>(dataTableRequest, query);
        }
    }
}
