﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using System.Transactions;
using Omu.ValueInjecter;
using System.Collections.Generic;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;
using System.Linq;
using CommonTools.JSTree;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellWIServices : WellWIRepository
    {
        public WellWIServices(DataContext dataContext) : base(dataContext) { }

        /*Do not use this function on datatables*/
        public Page<vw_wellwi> GetViewPerPage(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public ApiResponse<vw_wellwi> GetUserDetail(string recordId)
        {
            var result = new ApiResponse<vw_wellwi>();
            try
            {
                var serviceProject = new WellWIServices(this._services.dataContext);
                var record = serviceProject.GetViewById(recordId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = record;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }


        public ApiResponse New(wellwi record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                bool isNew = false;
                var recordId = record.id;
                var service_objectUom = new ObjectUomMapServices(_services.dataContext);
                var service_wellObjectUom = new WellObjectUomMapServices(_services.dataContext);
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    //default unit of measurement Std International
                    record.is_std_international = true;

                    if (record.business_unit_id == null)
                        result.Status.Success = SaveEntity(record, ref isNew, _services.dataContext.PrimaryTeamId, (r) => recordId = r.id);
                    else
                    {
                        var anakPerusahaanRecord = business_unit.FirstOrDefault("WHERE id=@0", record.business_unit_id);
                        if (anakPerusahaanRecord == null)
                            throw new Exception("Anak Perusahaan Record doesn't found.");
                        result.Status.Success &= SaveEntity(record, ref isNew, anakPerusahaanRecord.default_team, (r) => recordId = r.id);
                    }

                    var uomObjects = service_objectUom.GetViewAll("AND r.is_active=1");

                    if (uomObjects.Count > 0)
                    {
                        var uomData = uom.Fetch("WHERE is_active=1");
                        foreach (var item in uomObjects)
                        {
                            bool isNewWellUom = false;
                            var newRecord = new well_object_uom_map();
                            newRecord.object_name = item.object_name;
                            newRecord.uom_id = uomData?.Where(o => o.uom_category_id == item.uom_category_id && o.is_std_international == record.is_std_international).FirstOrDefault()?.id;
                            newRecord.alternate_well_id = recordId;
                            result.Status.Success &= service_wellObjectUom.SaveEntity(newRecord, ref isNewWellUom, (r) => newRecord.id = r.id);
                        }
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Please insert default Unit of Measurement.";
                    }


                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse ChangeUnitOfMeasurement(string wellId, bool? isStdInternational)
        {
            var result = new ApiResponse();
            try
            {
                var record = wellwi.FirstOrDefault("WHERE id=@0", wellId);
                var WellObjectUomService = new WellObjectUomMapServices(_services.dataContext);
                var ObjectMapService = new ObjectUomMapServices(_services.dataContext);

                var ObjectMap = vw_object_uom_map.Fetch("WHERE is_active=1 ");

                if (ObjectMap.Count > 0)
                {
                    using (var scope = new TransactionScope())
                    {
                        try
                        {
                            result.Status.Success = RemoveAllObjectMap(wellId);
                            //if (isStdInternational == null)
                            //    throw new Exception("Please fill Unit of measurement type SI or API unit");

                            if (isStdInternational == true)
                            {
                                var uomData = uom.Fetch("WHERE is_active=1");
                                foreach (var item in ObjectMap)
                                {
                                    bool isNewWellUom = false;
                                    var newRecord = new well_object_uom_map();
                                    newRecord.object_name = item.object_name;
                                    newRecord.uom_id = uomData?.Where(o => o.uom_category_id == item.uom_category_id && o.is_std_international == true).FirstOrDefault()?.id;
                                    newRecord.alternate_well_id = wellId;
                                    result.Status.Success &= WellObjectUomService.SaveEntity(newRecord, ref isNewWellUom, (r) => newRecord.id = r.id);
                                }
                            }
                            else
                            {
                                foreach (var item in ObjectMap)
                                {
                                    bool isNewWellUom = false;
                                    var newRecord = new well_object_uom_map();
                                    newRecord.object_name = item.object_name;
                                    newRecord.uom_id = item.uom_id;
                                    newRecord.alternate_well_id = wellId;
                                    result.Status.Success &= WellObjectUomService.SaveEntity(newRecord, ref isNewWellUom, (r) => newRecord.id = r.id);
                                }
                            }


                            record.is_std_international = isStdInternational;
                            result.Status.Success &= _services.dataContext.SaveEntity<wellwi>(record, false);

                            if (result.Status.Success)
                            {
                                scope.Complete();
                            }
                        }
                        catch (Exception ex)
                        {
                            result.Status.Success = false;
                            result.Status.Message = ex.Message;
                        }
                    }
                }
                else
                {
                    result.Status.Success = false;
                    result.Status.Message = "Please insert default Unit of Measurement.";
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                appLogger.Error(ex);
            }
            return result;
        }

        protected bool RemoveAllObjectMap(string wellId)
        {
            var result = false;
            try
            {
                var ObjectMap = well_object_uom_map.Fetch("WHERE alternate_well_id=@0", wellId);
                var WellObjectUomService = new WellObjectUomMapServices(_services.dataContext);
                result = true;
                string[] ids = new string[ObjectMap.Count];
                var i = 0;
                foreach (var item in ObjectMap)
                {
                    ids[i] = item.id;
                    i++;
                    //result &= WellObjectUomService.Remove(item.id);
                }
                result &= WellObjectUomService.RemoveAll(ids);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }
        public ApiResponse Save(wellwi record)
        {
            var result = new ApiResponse();
            try
            {
                bool isNew = false;
                var recordId = record.id;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    result.Status.Success = SaveEntity(record, ref isNew, this._services.dataContext.PrimaryTeamId, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {
                        if (isNew)
                            result.Status.Message = "The data Well WI has been added.";
                        else
                            result.Status.Message = "The data Well WI has been updated.";
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }
        public ApiResponse Submit(wellwi record)
        {
            var result = new ApiResponse();
            try
            {
                bool isNew = false;
                var recordId = record.id;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    record.submitted_by = _services.dataContext.AppUserId;
                    record.submitted_on = DateTime.Now;

                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {
                        result.Status.Message = "Well WI has been submitted.";
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }
        public ApiResponse Closed(wellwi record)
        {
            var result = new ApiResponse();
            try
            {
                bool isNew = false;
                var recordId = record.id;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    record.closed_by = _services.dataContext.AppUserId;
                    record.closed_on = DateTime.Now;

                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {
                        result.Status.Message = "Well WI has been closed.";
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }
        public ApiResponse SaveAll(WellViewModel record)
        {
            var result = new ApiResponse();
            try
            {
                bool isNew = false;
                bool isNewWellItem = false;
                var recordId = record.well.id;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    DrillingHazardServices drillingHazardServ = new DrillingHazardServices(_services.dataContext);
                    WellProfileProgramServices wellProgramServ = new WellProfileProgramServices(_services.dataContext);
                    DrillingDeviationServices drillingDeveServ = new DrillingDeviationServices(_services.dataContext);
                    DrillFormationsPlanServices drillPlanServc = new DrillFormationsPlanServices(_services.dataContext);
                    TVDPlanServices tvdPlanServc = new TVDPlanServices(_services.dataContext);
                    CVDPlanServices cvdPlanServc = new CVDPlanServices(_services.dataContext);
                    var service_wellItem = new WellItemServices(_services.dataContext);
                    var service_WellDeviation = new WellDeviationServices(_services.dataContext);
                    var service_minimumCurvature = new MinimumCurvature();
                    //var service_holeAndCasing = new HoleAndCasingServices(_services.dataContext);

                    result.Status.Success = SaveEntity(record.wellwi, ref isNew, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {
                        if (isNew)
                            result.Status.Message = "The data Well WI has been added.";
                        else
                            result.Status.Message = "The data Well WI has been updated.";

                        if (record.drill_formations_plan != null && result.Status.Success)
                        {
                            var sql = Sql.Builder.Append(@"DELETE FROM drill_formations_plan where well_id = @0", recordId);
                            this._services.db.Execute(sql);

                            int seq = 1;
                            foreach (drill_formations_plan item in record.drill_formations_plan)
                            {
                                bool isNewDrillPlan = false;
                                item.well_id = recordId;
                                item.seq = seq;
                                result.Status.Success &= drillPlanServc.SaveEntity(item, ref isNewDrillPlan, (r) => item.id = r.id);
                                seq++;
                            }
                        }

                        if (record.tvd_plan != null && result.Status.Success)
                        {
                            var sql = Sql.Builder.Append(@"DELETE FROM tvd_plan where well_id = @0", recordId);
                            this._services.db.Execute(sql);

                            foreach (tvd_plan item in record.tvd_plan)
                            {
                                bool isNewTvd = false;
                                item.well_id = recordId;
                                result.Status.Success &= tvdPlanServc.SaveEntity(item, ref isNewTvd, (r) => item.id = r.id);
                            }
                        }

                        if (record.cvd_plan != null && result.Status.Success)
                        {
                            var sql = Sql.Builder.Append(@"DELETE FROM cvd_plan where well_id = @0", recordId);
                            this._services.db.Execute(sql);

                            foreach (cvd_plan item in record.cvd_plan)
                            {
                                bool isNewCvd = false;
                                item.well_id = recordId;
                                result.Status.Success &= cvdPlanServc.SaveEntity(item, ref isNewCvd, (r) => item.id = r.id);
                            }
                        }

                        if (record.drilling_hazard != null && result.Status.Success)
                        {
                            foreach (drilling_hazard item in record.drilling_hazard)
                            {
                                bool isNewHazar = false;
                                item.well_id = recordId;
                                result.Status.Success &= drillingHazardServ.SaveEntity(item, ref isNewHazar, (r) => item.id = r.id);
                            }
                        }

                        if (record.well_profile_program != null && result.Status.Success)
                        {
                            foreach (well_profile_program item in record.well_profile_program)
                            {
                                bool isNewWellProfile = false;
                                item.well_id = recordId;
                                result.Status.Success &= wellProgramServ.SaveEntity(item, ref isNewWellProfile, (r) => item.id = r.id);
                            }
                        }

                        //if (record.hole_and_casing != null && record.hole_and_casing.Count > 0 && result.Status.Success)
                        //{
                        //    var sql = Sql.Builder.Append(@"DELETE FROM hole_and_casing where well_id = @0", recordId);
                        //    this._services.db.Execute(sql);

                        //    int seq = 1;
                        //    foreach (var item in record.hole_and_casing)
                        //    {
                        //        var isNewRecord = false;
                        //        var newRecord = new hole_and_casing();
                        //        newRecord.InjectFrom(item);
                        //        newRecord.well_id = recordId;
                        //        newRecord.seq = seq;
                        //        result.Status.Success &= service_holeAndCasing.SaveEntity(newRecord, ref isNewRecord, (r) => item.id = r.id);
                        //        seq++;
                        //    }
                        //}


                        if (record.well_deviation != null && record.well_deviation.Count > 0)
                        {
                            var isNewDeviation = false;
                            var sql = Sql.Builder.Append(@"DELETE FROM well_deviation where well_id = @0", recordId);
                            this._services.db.Execute(sql);

                            well_deviation firstRecord = new well_deviation();
                            firstRecord.azimuth = 0;
                            firstRecord.dls = 0;
                            firstRecord.e_w = 0;
                            firstRecord.inclination = 0;
                            firstRecord.measured_depth = 0;
                            firstRecord.n_s = 0;
                            firstRecord.phase = true;
                            firstRecord.seq = 0;
                            firstRecord.tvd = 0;
                            firstRecord.v_section = 0;
                            firstRecord.well_id = recordId;
                            result.Status.Success &= service_WellDeviation.SaveEntity(firstRecord, ref isNewDeviation, (r) => firstRecord.id = r.id);
                            //record.well_deviation.Insert(0, firstRecord);

                            if (result.Status.Success)
                            {
                                for (var i = 0; i < record.well_deviation.Count; i++)
                                {
                                    var newRecord = new well_deviation();
                                    isNewDeviation = false;
                                    var upper = new MWD();
                                    if (i == 0)
                                    {
                                        upper.InjectFrom(firstRecord);
                                    }
                                    else
                                    {
                                        upper.SurveyDepth = (double)record.well_deviation[i - 1].measured_depth;
                                        upper.Azimuth = (double)record.well_deviation[i - 1].azimuth;
                                        upper.Inclination = (double)record.well_deviation[i - 1].inclination;
                                        upper.TVD = (double)record.well_deviation[i - 1].tvd;
                                        upper.EastWest = (double)record.well_deviation[i - 1].e_w;
                                        upper.NorthSouth = (double)record.well_deviation[i - 1].n_s;
                                    }

                                    var lower = new MWD();
                                    lower.SurveyDepth = (double)record.well_deviation[i].measured_depth;
                                    lower.Azimuth = (double)record.well_deviation[i].azimuth;
                                    lower.Inclination = (double)record.well_deviation[i].inclination;

                                    if (result.Status.Success)
                                    {
                                        MinimumCurvatureResult resultMinimumCurvature = new MinimumCurvatureResult();
                                        if (lower.SurveyDepth > 0)
                                            resultMinimumCurvature = service_minimumCurvature.Calculate(upper, lower, record.well.target_direction);
                                        record.well_deviation[i].tvd = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.TVD : 0;
                                        record.well_deviation[i].v_section = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.VerticalSection : 0;
                                        record.well_deviation[i].n_s = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.North : 0;
                                        record.well_deviation[i].e_w = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.East : 0;
                                        record.well_deviation[i].dls = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.DLS : 0;

                                        newRecord.InjectFrom(record.well_deviation[i]);
                                        newRecord.seq = i + 1;
                                        newRecord.phase = true;
                                        newRecord.well_id = recordId;
                                        result.Status.Success &= service_WellDeviation.SaveEntity(newRecord, ref isNewDeviation, (r) => record.well_deviation[i].id = r.id);
                                    }
                                }
                            }
                        }

                        if (record.well_item != null && record.well_item.Count > 0)
                        {
                            var sql = Sql.Builder.Append(@"DELETE FROM well_item where well_id = @0", recordId);
                            this._services.db.Execute(sql);

                            foreach (var item in record.well_item)
                            {
                                var newRecord = new well_item();
                                newRecord.InjectFrom(item);
                                newRecord.well_id = recordId;
                                result.Status.Success &= service_wellItem.SaveEntity(newRecord, ref isNewWellItem, (r) => item.id = r.id);
                            }
                        }
                        else
                        {
                            result.Status.Success = false;
                            result.Status.Message = "Well items cannot be empty";
                            return result;
                        }
                    }
                    else
                    {
                        result.Status.Success = false;
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                if (ex.Message.Contains("Violation of UNIQUE KEY"))
                    result.Status.Message = "Item on Buld Item Tab cannot be same with another.";
                else
                    result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }


        public Page<vw_wellwi> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            if (!this._services.dataContext.IsSystemAdministrator)
            {
                sqlFilter.Append(" AND (r.business_unit_id = @0 OR b.parent_unit = @0)", this._services.dataContext.PrimaryBusinessUnitId);
            }

            if (marvelDataSourceRequest.Filter != null)
            {
                var filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);

                if (filterBuilder != null)
                {
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
                }
            }

            #endregion
            //sqlFilter.Append(" ORDER BY COALESCE(r.parent_well,r.id) ASC ");
            sqlFilter.Append(" ORDER BY r.well_name ASC ");
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_wellwi> LookupUser(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 AND r.is_sysadmin IS NULL AND r.is_ldap=@0", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY COALESCE(r.last_name, r.first_name) ASC ");
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
        public ApiResponse<vw_wellwi> getDetail(string id)
        {
            var result = new ApiResponse<vw_wellwi>();
            var service_afe = new AfeServices(_services.dataContext);
            try
            {
                var wellwi = this.GetViewById(id);
                result.Data = wellwi;
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse Delete(params string[] ids)
        {
            var result = new ApiResponse();
            try
            {
                result.Status.Success = true;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    try
                    {
                        foreach (var recordId in ids)
                        {
                            var record = wellwi.FirstOrDefault("WHERE id=@0", recordId);
                            record.is_active = false;
                            result.Status.Success &= _services.dataContext.SaveEntity<wellwi>(record, false);
                        }
                        if (result.Status.Success)
                        {
                            scope.Complete();

                        }
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
