﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Hosting;
using CommonTools;
using Hangfire;
using PetaPoco;
using Dreamwell.Infrastructure;
using Dreamwell.DataAccess;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ApplicationRoleServices : ApplicationRoleRepository
    {
        public ApplicationRoleServices(DataContext dataContext) : base(dataContext) { }


        /*Do not use this function on datatables*/
        public Page<vw_application_role> GetViewPerPage(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        private bool GenerateRoleAccess(string roleId)
        {
            var success = true;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var services = new RoleAccessServices(_services.dataContext);
                    var entityService = new ApplicationEntityRepository(_services.dataContext);
                    var qry = Sql.Builder.Append(@"
                                 AND r.is_active=1 AND r.id NOT IN (
                                SELECT rs.application_entity_id FROM dbo.role_access rs WHERE rs.application_role_id=@0
                            )",roleId);
                    var entities = entityService.GetViewAll(qry);
                    foreach (var e in entities)
                    {
                        var isNew = false;
                        var record = new role_access();
                        record.application_role_id = roleId;
                        record.application_entity_id = e.id;
                        record.access_create = 0;
                        record.access_read = 0;
                        record.access_update = 0;
                        record.access_delete = 0;
                        record.access_approve = 0;
                        record.access_lock = 0;
                        record.access_activate = 0;
                        record.access_append = 0;
                        record.access_share = 0;
                        record.is_active = true;
                        success &= services.SaveEntity(record,ref isNew);
                    }
                    if (success)
                    {
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    appLogger.Error(ex);
                }
            }
            return success;
        }

        public bool SynchronizeEntityRoleAccess()
        {
            var success = true;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var appRoleList = this.GetAll();
                    foreach (var r in appRoleList)
                    {
                        success &= GenerateRoleAccess(r.id);
                    }
                    if (success)
                    {
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    appLogger.Error(ex);
                }
            }
            return success;
        }


        public bool ApplicationEntitySetupAndRoleAccess()
        {
            var success = true;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var appRoleList = this.GetAll();
                    foreach (var r in appRoleList)
                    {
                        success &= GenerateRoleAccess(r.id);
                    }
                    
                    if (!success)
                        throw new Exception("Synchronize doesn't complete. Please try again!");

                    success &= this.SynchronizeEntityRoleAccess();
                    if (success)
                    {
                        var service = new Dreamwell.BusinessLogic.Core.SysConfig.ApplicationEntitySetup();
                        service.Synchronize();
                        success = true;
                    }

                    if (success)
                    {
                        scope.Complete();
                    }

                }
                catch (Exception ex)
                {
                    success = false;
                    appLogger.Error(ex);
                }
            }
            return success;
        }


        public ApiResponse Save(application_role record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";

                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";

                    }
                    result.Data = new
                    {
                        recordId
                    };
                    HostingEnvironment.QueueBackgroundWorkItem(ct => GenerateRoleAccess(recordId));
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

    }
}
