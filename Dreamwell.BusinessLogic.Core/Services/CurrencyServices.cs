﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class CurrencyServices : CurrencyRepository
    {
        public CurrencyServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(vw_currency record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            var service_currencyExchange = new CurrencyExchange(_services.dataContext);
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var recordId = record.id;

                    if (String.IsNullOrEmpty(recordId))
                        record.id = CommonTools.GuidHash.ConvertToMd5HashGUID(record.currency_code).ToString();

                    var currencyRecord = new currency();
                    currencyRecord.InjectFrom(record);
                    if (record.base_rate ?? false)
                        currencyRecord.base_rate = true;
                    else
                        currencyRecord.base_rate = false;

                    var sql = Sql.Builder.Append(" AND base_rate=@0 ", true);
                    var currentBaseRate = this.GetAll(sql).FirstOrDefault();

                    if ((record.id == null && currentBaseRate != null && currencyRecord.base_rate.Value) ||
                        (record.id != null && currentBaseRate != null && currencyRecord.base_rate.Value && (record.id != currentBaseRate.id)))
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Cannot have more than one base rate";
                    }

                    if (result.Status.Success)
                    {
                        if (currentBaseRate == null)
                        {
                            result.Status.Success = SaveEntity(currencyRecord, ref isNew, (r) => recordId = r.id);
                        }
                        else //-- Have a single record for Base Rate
                        {
                            if ((record.base_rate ?? false) == true)
                            {
                                if (currentBaseRate.id == recordId)
                                    result.Status.Success = SaveEntity(currencyRecord, ref isNew, (r) => recordId = r.id);
                                else
                                    result.Status.Success = false;
                            }
                            else
                                result.Status.Success = SaveEntity(currencyRecord, ref isNew, (r) => recordId = r.id);
                        }
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                            result.Status.Message = "New currency has been created";
                        else
                            result.Status.Message = "Currency has been updated";

                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex.Message);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse SaveNewRate(currency record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse DeleteById(string id)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var qryDeleteItem = Sql.Builder.Append("DELETE FROM currency WHERE id = @0 ", id);
                    _services.db.Execute(qryDeleteItem);

                    result.Status.Success = true;
                    result.Status.Message = "Deleted";
                    scope.Complete();
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }
        public Page<vw_currency> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 ", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
