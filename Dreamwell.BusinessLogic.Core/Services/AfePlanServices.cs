﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;
using Dreamwell.Infrastructure.DataTables;
using DataTables.Mvc;
using CommonTools.Helper;
using CommonTools.EeasyUI;
using System.Net.Http;
using System.Net;
using OfficeOpenXml;
using OfficeOpenXml.Table.PivotTable;
using System.Net.Http.Headers;
using System.IO;

//using OfficeOpenXml;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class AfePlanServices : AfePlanRepository
    {
        public AfePlanServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse<vw_afe_plan> Save(afe_plan record)
        {
            var result = new ApiResponse<vw_afe_plan>();
            result.Status.Success = true;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var recordId = record.id;

                    record.total_price = record.unit * record.unit_price;
                    record.actual_price = record.total_price / record.current_rate_value;

                    result.Status.Success &= SaveEntity(record, ref isNew, (r) => record.id = r.id);
                    result.Data = this.GetViewById(record.id);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                            result.Status.Message = "The data has been added.";
                        else
                            result.Status.Message = "The data has been updated.";
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex.Message);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse DeleteAfe(string id)
        {
            var result = new ApiResponse();
            var is_delete = false;
            var plan = GetViewById(id);
            if (plan == null) return result;
            appLogger.Debug(plan.afe_id);
            var contractService = new AfeContractServices(_services.dataContext);
            var contract = contractService.GetViewAll(" AND r.afe_id = @0", plan.afe_id);
            if (contract.Count == 0)
            {
                result.Status.Success = Remove(id);
            }
            else
            {
                result.Status.Message = "You can't take this action, this material already has a contract";
            }
            return result;
        }

        public DataTablesResponse GetListByQuery(DataTableRequest DataTableRequest, string afe_id)
        {
            var services = new AfeManageDetailRepository(_services.dataContext, afe_id);
            return services.GetListDataTables(DataTableRequest);
            //return base.GetListDataTablesByQuery(DataTableRequest, sql.SQL, " r.afe_line_parent_name ASC, r.afe_line_description ASC, r.material_name ", sql.Arguments);
        }

        public Node<vw_afe_manage_detail> GetAfeLineTree(ITreeRequest request, String afeId)
        {
            var userId = BusinessLogic.Core.SysConfig.UserSetup.sysAdminId;
            Node<vw_afe_manage_detail> results = new Node<vw_afe_manage_detail>(null);
            try
            {

                if (!string.IsNullOrEmpty(request.ReloadByParentId) && !string.IsNullOrWhiteSpace(request.ReloadByParentId))
                {
                    GetAfeLineTree(afeId, request.ReloadByParentId, results);
                }
                else
                {
                    var qry = Sql.Builder.Append(" WHERE is_active=1 and organization_id=@0 ", _services.dataContext.OrganizationId);
                    qry.Append("AND parent_id is null");
                    qry.Append(" ORDER BY line asc ");
                    var afeLineParent = afe_line.Fetch(qry);
                    appLogger.Debug(JsonConvert.SerializeObject(qry));
                    foreach (var item in afeLineParent)
                    {
                        var record = new vw_afe_manage_detail();
                        record.id = item.id;
                        record.description = item.description;
                        record.icon_type = "fw-500 text-uppercase";
                        Node<vw_afe_manage_detail> parent = results.Add(record);
                        GetAfeLineTree(afeId, item.id, parent);
                    }
                }
                //appLogger.Debug(JsonConvert.SerializeObject(results));

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }


        protected vw_afe_manage_detail GetAfeLineTree(String afeId, String parentId, Node<vw_afe_manage_detail> parent)
        {
            var results = new vw_afe_manage_detail();
            var children = parent;
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var data = afe_line.Fetch(" WHERE parent_id=@0 ORDER BY line asc", parentId);
                if (data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        var record = new vw_afe_manage_detail();
                        record.id = item.id;
                        record.description = item.description;
                        record.icon_type = "fw-500 text-uppercase";
                        GetAfeLineTree(afeId, item.id, children.Add(record));
                    }
                }
                else
                {

                    var itemType = children;
                    GetMaterial(afeId, parentId, itemType.Add(new vw_afe_manage_detail
                    {
                        id = string.Format("{0}_DYRHOLEBASED", parentId),
                        description = "DRY HOLE BASED",
                        icon_type = "fw-900 text-uppercase font-italic"
                    }), 1);
                    GetMaterial(afeId, parentId, itemType.Add(new vw_afe_manage_detail
                    {
                        id = string.Format("{0}_COMPLETIONBASED", parentId),
                        description = "COMPLETION BASED",
                        icon_type = "fw-900 text-uppercase font-italic"
                    }), 2);

                    //GetMaterial(parentId, children);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        protected vw_afe_manage_detail GetMaterial(String afeId, String afeLineId, Node<vw_afe_manage_detail> parent, int itemType)
        {
            var results = new vw_afe_manage_detail();
            var children = parent;
            try
            {
                var service = new AfeManageDetailRepository(_services.dataContext, afeId, afeLineId, itemType);
                //if (string.IsNullOrEmpty(parentId)) return results;
                //var qry = Sql.Builder.Append(material.DefaultView);
                //qry.Append(" WHERE r.afe_line_id = @0 AND item_type=@1 ORDER BY r.material_type ASC, r.description ASC ", parentId, itemType);
                var materialRecord = service.GetViewAll().OrderBy(x => x.created_on).ToList();

                if (materialRecord.Count > 0)
                {
                    foreach (var item in materialRecord)
                    {
                        item.icon_type = "material";
                        children.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                //appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public ExcelWorksheet GetCountMaterial(ExcelWorksheet worksheet, int row, string afeLineId, string afeId)
        {
            var serviceDry = new AfeManageDetailRepository(_services.dataContext, afeId, afeLineId, 1);
            var serviceCompletion = new AfeManageDetailRepository(_services.dataContext, afeId, afeLineId, 2);
            var materialDry = serviceDry.GetViewAll();
            var materialCompletion = serviceCompletion.GetViewAll();
            appLogger.Debug($"{afeId}-dry-{materialDry.Sum(x => x.afe_unit)}");
            appLogger.Debug($"{afeId}-dry-{materialCompletion.Sum(x => x.afe_unit)}");
            
            worksheet.Cells[$"O{row}"].Value = materialDry.Sum(x => x.afe_unit) + materialCompletion.Sum(x => x.afe_unit);
            worksheet.Cells[$"P{row}"].Value = materialDry.Sum(x => x.afe_actual_price) + materialCompletion.Sum(x => x.afe_actual_price);
            return worksheet;
        }

        public ApiResponse Test(string afeLineId, string afeId)
        {
            var response = new ApiResponse();
            var serviceDry = new AfeManageDetailRepository(_services.dataContext, afeId, afeLineId, 1);
            var serviceCompletion = new AfeManageDetailRepository(_services.dataContext, afeId, afeLineId, 2);
            var materialDry = serviceDry.GetViewAll();
            var materialCompletion = serviceCompletion.GetViewAll();
            response.Data = new
            {
                qty = materialDry.Sum(x => x.afe_unit) + materialCompletion.Sum(x => x.afe_unit),
                price = materialDry.Sum(x => x.afe_actual_price) + materialCompletion.Sum(x => x.afe_actual_price)
            };
            return response;
        }

        public HttpResponseMessage GenerateBS19(string filePath, string id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                FileInfo file = new FileInfo(filePath);
                using (ExcelPackage excelPackage = new ExcelPackage(file))
                {
                    // Mengecek apakah worksheet dengan nama yang sama sudah ada
                    if (excelPackage.Workbook.Worksheets.Any(ws => ws.Name == "BS-19"))
                    {
                        // Jika worksheet sudah ada, berikan nama yang unik
                        int index = 1;
                        string worksheetName = "BS-19";
                        while (excelPackage.Workbook.Worksheets.Any(ws => ws.Name == worksheetName))
                        {
                            worksheetName = $"BS-19_{index}";
                            index++;
                        }
                        // Membuat worksheet dengan nama yang unik
                        ExcelWorksheet worksheetPivot = excelPackage.Workbook.Worksheets.Add(worksheetName);
                    }
                    else
                    {
                        // Jika worksheet belum ada, tambahkan seperti biasa
                        ExcelWorksheet worksheetPivot = excelPackage.Workbook.Worksheets.Add("BS-19");
                    }

                    // Menyimpan paket Excel
                    excelPackage.Save();

                    // Menyiapkan respons HTTP
                    response.StatusCode = HttpStatusCode.OK;
                    response.ReasonPhrase = "OK";
                    MediaTypeHeaderValue mediaType = new MediaTypeHeaderValue("application/octet-stream");
                    string fileName = string.Format("{0}_{1}.xlsx", "BS19", DateTime.Now.ToString("dd-MM-yyyy"));
                    MemoryStream memoryStream = new MemoryStream(excelPackage.GetAsByteArray());
                    response.Content = new StreamContent(memoryStream);
                    response.Content.Headers.ContentType = mediaType;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName
                    };
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.InternalServerError;
                response.ReasonPhrase = "Internal Server Error";
                response.Content = new StringContent($"Error: {ex.Message}");
            }
            return response;
        }
    }
}
