﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using System.Collections.Generic;
using System.Transactions;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;
using Omu.ValueInjecter;
using System.Linq;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WorkloadServices : WorkloadsRepository
    {
        private readonly DataContext _dataContext;

        public WorkloadServices(DataContext dataContext) : base(dataContext) {

            _dataContext = dataContext;
        }

        public ApiResponse Save(workload_wir record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                // Cek apakah data berhasil disimpan
                result.Status.Success = SaveEntity(record, ref isNew, this._services.dataContext.PrimaryTeamId, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    // Menambahkan pesan berdasarkan apakah data baru atau update
                    result.Status.Message = isNew ? "The data has been added." : "The data has been updated.";
                    result.Data = new { recordId };
                }
                else
                {
                    result.Status.Message = "Failed to save data.";
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = $"Error saving record: {ex.Message}";
                appLogger.Error($"Error saving record: {ex.Message}");  // Log error
            }
            return result;
        }


        public ApiResponse<List<vw_workload_wir>> GetByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_workload_wir>>();
            try
            {
                if (string.IsNullOrWhiteSpace(wellId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Well ID cannot be empty.";
                    return result;
                }

                appLogger.Info($"Fetching data for Well ID: {wellId}");
                var sql = Sql.Builder.Append(@"
                SELECT *
                FROM vw_workload_wir
                WHERE well_id = well_id
                ORDER BY created_on DESC", wellId);

                result.Data = this._services.db.Fetch<vw_workload_wir>(sql);
                appLogger.Info($"Retrieved {result.Data.Count} records.");
                result.Status.Success = true;
                result.Status.Message = "Data retrieved successfully.";
            }
            catch (Exception ex)
            {
                appLogger.Error($"Error in GetByWellId: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"Error retrieving data: {ex.Message}";
            }
            return result;
        }

    }

}
