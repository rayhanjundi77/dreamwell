﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;
using CommonTools.EeasyUI;
using CommonTools.Helper;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellItemServices : WellItemRepository
    {
        public WellItemServices(DataContext dataContext) : base(dataContext) { }

        //public ApiResponse Save(well_item record)
        //{
        //    var result = new ApiResponse();
        //    try
        //    {
        //        var isNew = false;
        //        var recordId = record.id;

        //        result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
        //        if (result.Status.Success)
        //        {
        //            if (isNew)
        //            {
        //                result.Status.Message = "The data has been added.";
        //            }
        //            else
        //            {
        //                result.Status.Message = "The data has been updated.";
        //            }
        //            result.Data = new
        //            {
        //                recordId
        //            };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        result.Status.Success = false;
        //        result.Status.Message = ex.Message;
        //    }
        //    return result;
        //}

        public ApiResponse Save(WellViewModel record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                var recordId = record.well.id;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    if (record.well_item != null)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM well_item where well_id = @0", record.well.id);
                        this._services.db.Execute(sql);

                        foreach (var item in record.well_item)
                        {
                            bool isNew = false;
                            var wellItem = new well_item();
                            wellItem.InjectFrom(item);
                            wellItem.well_id = record.well.id;
                            result.Status.Success &= SaveEntity(wellItem, ref isNew, (r) => item.id = r.id);
                        }
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Well Item has been changed.";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse<List<vw_well_item>> getWellItemByDrillingId(string wellId, string drillingId)
        {
            var result = new ApiResponse<List<vw_well_item>>();
            //var service_drilling = new drilling
            try
            {
                var listWellItem = this.GetViewAll(" AND r.drilling_id = @0 ", drillingId);
                if (listWellItem == null || listWellItem.Count <= 0)
                {

                }

                var sql = Sql.Builder.Append(@"
                            SELECT RTRIM(LTRIM(CONCAT(ISNULL(UPPER(r.description), N''), ' | ', ISNULL(a.name, N'')))) AS budget_description, 
                            r.id AS afe_line_id,
                            afe_id = @0, 
                            budget.original_budget,
                            budget.rev_budget_1, 
                            budget.rev_budget_2, 
	                            budget.actual_budget AS actual_budget,
	                            expense.actual_expense AS actual_expense
                            FROM dbo.afe_line AS r
                            LEFT OUTER JOIN dbo.afe_line_parent AS a
	                            ON r.afe_line_parent_id = a.id
                            CROSS APPLY (
	                            SELECT	SUM(amb1.original_budget) AS original_budget, 
			                            SUM(amb1.rev_budget_1) AS rev_budget_1, 
			                            SUM(amb1.rev_budget_2) AS rev_budget_2, 
			                            SUM(amb1.actual_budget) AS actual_budget, 
			                            SUM(amb1.actual_expense) AS actual_expense
	                            FROM dbo.afe_manage_budget amb1
	                            INNER JOIN dbo.afe a1 ON a1.id = amb1.afe_id
	                            WHERE amb1.afe_line_id = r.id AND a1.id = @0
                            ) AS budget
                            OUTER APPLY (
		                        SELECT SUM(dc.cost) AS actual_expense
		                        FROM daily_cost dc
		                        INNER JOIN drilling AS d ON dc.drilling_id = d.id
		                        INNER JOIN contract_detail AS cd ON cd.id = dc.contract_detail_id
		                        where cd.afe_line_id = r.id AND d.afe_id = @0
	                        ) AS expense
                            ORDER BY budget_description ASC", drillingId);
                var item = this._services.db.Fetch<vw_well_item>(sql);
                result.Data = item;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        //public Node<vw_afe_manage_detail> GetBs19FormatTree(ITreeRequest request, string wellId)
        //{
        //    Node<vw_afe_manage_detail> results = new Node<vw_afe_manage_detail>(null);
        //    try
        //    {

        //        if (!string.IsNullOrEmpty(request.ReloadByParentId) && !string.IsNullOrWhiteSpace(request.ReloadByParentId))
        //        {
        //            GetAfeLineTree(afeId, request.ReloadByParentId, results);
        //        }
        //        else
        //        {
        //            var qry = Sql.Builder.Append(" WHERE is_active=1 and organization_id=@0 ", _services.dataContext.OrganizationId);
        //            qry.Append("AND parent_id is null");
        //            qry.Append(" ORDER BY line asc ");
        //            var afeLineParent = afe_line.Fetch(qry);
        //            appLogger.Debug(JsonConvert.SerializeObject(qry));
        //            foreach (var item in afeLineParent)
        //            {
        //                var record = new vw_afe_manage_detail();
        //                record.id = item.id;
        //                record.description = item.description;
        //                record.icon_type = "fw-500 text-uppercase";
        //                Node<vw_afe_manage_detail> parent = results.Add(record);
        //                GetAfeLineTree(afeId, item.id, parent);
        //            }
        //        }
        //        //appLogger.Debug(JsonConvert.SerializeObject(results));

        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        throw;
        //    }
        //    return results;
        //    //var result = new ApiResponse();
        //    //try
        //    //{
        //    //    var repo = new WellDataRepository();
        //    //    result.Status.Success = true;
        //    //    result.Data = repo.Bs19Format(wellId);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    result.Status.Success = false;
        //    //    result.Status.Message = ex.StackTrace;
        //    //    appLogger.Error(ex);
        //    //}
        //    //return result;
        //}

        //protected vw_afe_manage_detail GetAfeLineTree(String afeId, String parentId, Node<vw_afe_manage_detail> parent)
        //{
        //    var results = new vw_afe_manage_detail();
        //    var children = parent;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(parentId)) return results;
        //        var data = afe_line.Fetch(" WHERE parent_id=@0 ORDER BY line asc", parentId);
        //        if (data.Count > 0)
        //        {
        //            foreach (var item in data)
        //            {
        //                var record = new vw_afe_manage_detail();
        //                record.id = item.id;
        //                record.description = item.description;
        //                record.icon_type = "fw-500 text-uppercase";
        //                GetAfeLineTree(afeId, item.id, children.Add(record));
        //            }
        //        }
        //        else
        //        {

        //            var itemType = children;
        //            GetMaterial(afeId, parentId, itemType.Add(new vw_afe_manage_detail
        //            {
        //                id = string.Format("{0}_DYRHOLEBASED", parentId),
        //                description = "DRY HOLE BASED",
        //                icon_type = "fw-900 text-uppercase font-italic"
        //            }), 1);
        //            GetMaterial(afeId, parentId, itemType.Add(new vw_afe_manage_detail
        //            {
        //                id = string.Format("{0}_COMPLETIONBASED", parentId),
        //                description = "COMPLETION BASED",
        //                icon_type = "fw-900 text-uppercase font-italic"
        //            }), 2);

        //            //GetMaterial(parentId, children);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        throw;
        //    }
        //    return results;
        //}
    }
}
