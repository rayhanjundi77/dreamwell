﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class AfeContractServices : AfeContractRepository
    {
        public AfeContractServices(DataContext dataContext) : base(dataContext) { }

        public DataTablesResponse GetListDataTables(DataTableRequest DataTableRequest,string afeId)
        {
            var qry = Sql.Builder.Append(" WHERE r.is_active=@0 ANd r.afe_id=@1", true, afeId);
            return base.GetListDataTables(DataTableRequest, qry);
        }

        public ApiResponse AssignContractToAfe(afe_contract record)
        {
            var result = new ApiResponse();
            var service_contract = new ContractServices(_services.dataContext);
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    try
                    {
                        if (this.GetFirstOrDefault(" AND contract_id = @0 AND afe_id != @1 ", record.contract_detail_id, record.afe_id) == null)
                            record.is_new_purchase = true;
                        else
                            record.is_new_purchase = false;

                        result.Status.Success = _services.dataContext.SaveEntity<afe_contract>(record, true);

                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("UniqueConstraint"))
                        {
                            throw new Exception("Cannot insert duplicate values.");
                        }
                        throw new Exception(ex.Message);
                    }
                }

            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<afe_contract> Save(afe_contract record)
        {
            //var current = new afe_manage_budget();
            var result = new ApiResponse<afe_contract>();
            var service_contract = new ContractServices(_services.dataContext);
            var service_contractDetail = new ContractDetailServices(_services.dataContext);

            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                result.Status.Success = true;
                try
                {
                    var recordId = record.id;
                    var isNew = false;
                    var isNewAfeContract = false;

                    var contractDetailRecord = service_contractDetail.GetViewFirstOrDefault(" AND r.id=@0 ", record.contract_detail_id);
                    var afeContractRecord = this.GetFirstOrDefault(" AND contract_detail_id=@0 ", record.contract_detail_id);

                    #region Validation
                    if (contractDetailRecord.afe_locked_id != null && record.afe_id != contractDetailRecord.afe_locked_id)
                    {
                        result.Status.Success = false;
                        result.Status.Message = "This material is currently use on another AFE. You can only use this material after AFE closed or still has remaining unit.";
                    }
                    
                    if (record.unit > (contractDetailRecord.remaining_unit == null ? contractDetailRecord.unit : contractDetailRecord.remaining_unit))
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Material on this contract doesn't have enough material.";
                    }
                    #endregion

                    if (result.Status.Success)
                    {
                        var contractRecord = service_contract.GetById(contractDetailRecord.contract_id);
                        if (contractRecord.afe_id == null || contractRecord.afe_id == record.afe_id)
                            record.is_new_purchase = true;
                        else
                            record.is_new_purchase = false;

                        result.Status.Success &= SaveEntity(record, ref isNew, (r) => record.id = r.id);

                        if (contractRecord.afe_id == null)
                        {
                            contractRecord.afe_id = record.afe_id;
                            result.Status.Success &= service_contract.SaveEntity(contractRecord, ref isNewAfeContract);
                        }

                        var qry = Sql.Builder.Append("UPDATE dbo.contract_detail SET afe_locked_id = @0 WHERE id=@1 ", record.afe_id, record.contract_detail_id);
                        _services.db.Execute(qry);
                    }

                    result.Data = record;

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                            result.Status.Message = "The data has been added.";
                        else
                            result.Status.Message = "The data has been updated.";
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex.Message);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse Delete (string recordId, string afeId, string contractId, string contractDetailId, bool isNew)
        {
            var result = new ApiResponse();
            var service_afeContract = new AfeContractServices(_services.dataContext);

            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                result.Status.Success = true;
                try
                {
                    result.Status.Success = this.Remove(recordId);

                    var afeContractRecord = service_afeContract.GetViewAll(" AND r.afe_id = @0 AND r.contract_detail_id = @1 ", afeId, contractDetailId);

                    if ((afeContractRecord.Count < 1 || afeContractRecord == null) && isNew)
                    {
                        var qry = Sql.Builder.Append("UPDATE dbo.contract SET afe_id = NULL WHERE id=@0 ", contractId);
                        _services.db.Execute(qry);

                        result.Status.Success = true;
                    }


                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Material on this contract has been removed from AFE.";
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex.Message);
                    result.Status.Success = false;
                    if (ex.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                        result.Status.Message = "This Material already use on drilling. Cannot remove from AFE.";
                    else
                        result.Status.Message = ex.Message;
                }
            }
            return result;
        }
    }
}
