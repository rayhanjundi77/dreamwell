﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ApplicationEntityServices : ApplicationEntityRepository
    {
        public ApplicationEntityServices(DataContext dataContext) : base(dataContext) { }

        public string GetEntityIdByName(string entityName)
        {
            var entityId = string.Empty;
            try
            {
                entityId = application_entity.FirstOrDefault("WHERE display_name=@0", entityName)?.id;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return entityId;
        }

        public Page<vw_application_entity> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            var kosong = Sql.Builder.Append("  \n AND (\n ) ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null && filterBuilder.SQL != kosong.SQL)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

    }
}
