﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingServices : DrillingRepository
    {
        public DrillingServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(DrillingViewModel record)
        {
            var result = new ApiResponse();
            //var service_drillingOperation = new DrillingOperationServices(_services.dataContext);
            var service_drillingOperationActivity = new DrillingOperationActivityServices(_services.dataContext);
            var service_drillingDeviation = new DrillingDeviationServices(_services.dataContext);
            var service_minimumCurvature = new MinimumCurvature();
            //var service_drillingBha = new DrillingBhaServices(_services.dataContext);
            //var service_drillingBit = new DrillingBitServices(_services.dataContext);
            //var service_drillingBhaBit = new DrillingBhaBitServices(_services.dataContext);
            var service_drillingMud = new DrillingMudServices(_services.dataContext);
            var service_drillingAerated = new DrillingAeratedServices(_services.dataContext);
            var service_drillingMudPit = new DrillingMudPitServices(_services.dataContext);
            var service_drillingChemicalUsed = new DrillingChemicalUsedServices(_services.dataContext);
            var service_drillingTotalVolume = new DrillingTotalVolumeServices(_services.dataContext);
            var service_drillingTransport = new DrillingTransportServices(_services.dataContext);
            var service_drillingBulk = new DrillingBulkServices(_services.dataContext);
            var service_drillingPersonel = new DrillingPersonelServices(_services.dataContext);
            var service_well = new WellServices(_services.dataContext);
            var service_drillingHoleAndCasing = new DrillingHoleAndCasingServices(_services.dataContext);
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var userLogin = _services.dataContext;
                    var isNewDrilling = false;
                    var isNew = false;
                    var isNewDrillingBulk = false;
                    var isNewDrillingMudPit = false;
                    var recordId = record.drilling.id;

                    //var drillingBha = service_drillingBha.GetViewAll(" AND r.well_id = @0 AND r.is_active=@1 ", record.drilling.well_id, true);
                    //if (drillingBha == null || drillingBha.Count <= 0)
                    //{
                    //    result.Status.Success = false;
                    //    result.Status.Message = "This well doesn't have record of BHA. Please save your current BHA or create a new BHA and the components.";
                    //    return result;
                    //}

                    //var drillingBhaBit = service_drillingBhaBit.GetViewAll(" AND r.well_id=@0 ", record.drilling.well_id);
                    //if (drillingBhaBit == null || drillingBhaBit.Count <= 0)
                    //{
                    //    result.Status.Success = false;
                    //    result.Status.Message = "This well doesn't have record of BIT. Please save your current BIT or create a new BIT.";
                    //    return result;
                    //}

                    //var drillingBit = service_drillingBit.GetViewAll(" AND r.is_active=@0 ", true);
                    //if (drillingBit == null || drillingBit.Count <= 0)
                    //{
                    //    result.Status.Success = false;
                    //    result.Status.Message = "This well doesn't have record of BIT. Please save your current BIT or create a new BIT.";
                    //    return result;
                    //}

                    var drilling = new drilling();
                    drilling.InjectFrom(record.drilling);
                    drilling.submitted_by = userLogin.AppUserId;
                    drilling.submitted_on = DateTime.Now;
                    //drilling.drilling_bha_id = drillingBha?.FirstOrDefault()?.id;
                    //drilling.drilling_bit_id = drillingBit?.FirstOrDefault()?.id;
                    result.Status.Success = SaveEntity(drilling, ref isNewDrilling, (r) => recordId = r.id);

                    var well = service_well.GetById(record.drilling.well_id);
                    if (well == null)
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Well is not found";
                        return result;
                    }

                    //foreach (var item in record.drilling_operation)
                    //{
                    //    var newRecord = drilling_operation.FirstOrDefault(" WHERE drilling_id=@0 AND type=@1", recordId, item.type);
                    //    if (newRecord == null)
                    //    {
                    //        newRecord = new drilling_operation();
                    //        newRecord.id = null;
                    //        newRecord.drilling_id = recordId;
                    //        newRecord.operation_date = record.drilling.drilling_date;
                    //        newRecord.type = item.type;
                    //    }
                    //    newRecord.description = item.description;
                    //    result.Status.Success &= service_drillingOperation.SaveEntity(newRecord, ref isNew, (r) => item.id = r.id);
                    //}

                    if (record.drilling_operation_activity != null && record.drilling_operation_activity.Count > 0)
                    {
                        foreach (var item in record.drilling_operation_activity)
                        {
                            var newRecord = new drilling_operation_activity();
                            newRecord.InjectFrom(item);
                            newRecord.drilling_id = recordId;
                            result.Status.Success &= service_drillingOperationActivity.SaveEntity(newRecord, ref isNew, (r) => item.id = r.id);
                        }
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Operation activity cannot be empty";
                        return result;
                    }

                    if (record.drilling_deviation != null && record.drilling_deviation.Count > 0)
                    {
                        var isNewDeviation = false;
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_deviation where drilling_id = @0", recordId);
                        this._services.db.Execute(sql);

                        var lastRecordOfYesterdaySQL = Sql.Builder.Append(@"
                            SELECT TOP 1 r.*
                            FROM drilling_deviation AS r
                            INNER JOIN drilling d ON r.drilling_id = d.id
                            WHERE d.well_id = @0 AND d.drilling_date < @1 
                            ORDER BY d.drilling_date, r.seq DESC", record.drilling.well_id, DateTime.Now);
                        var lastRecord = this._services.db.SingleOrDefault<drilling_deviation>(lastRecordOfYesterdaySQL);
                        if (lastRecord == null)
                        {
                            lastRecord = new drilling_deviation();
                            lastRecord.azimuth = 0;
                            lastRecord.dls = 0;
                            lastRecord.e_w = 0;
                            lastRecord.inclination = 0;
                            lastRecord.measured_depth = 0;
                            lastRecord.n_s = 0;
                            lastRecord.phase = true;
                            lastRecord.seq = 0;
                            lastRecord.tvd = 0;
                            lastRecord.v_section = 0;
                            lastRecord.drilling_id = recordId;
                            //result.Status.Success &= service_drillingDeviation.SaveEntity(lastRecord, ref isNewDeviation, (r) => lastRecord.id = r.id);
                            //record.drilling_deviation.Insert(0, firstRecord);
                        }

                        if (result.Status.Success)
                        {
                            for (var i = 0; i < record.drilling_deviation.Count; i++)
                            {
                                var newRecord = new drilling_deviation();
                                isNewDeviation = false;
                                var upper = new MWD();
                                if (i == 0)
                                {
                                    upper.InjectFrom(lastRecord);
                                }
                                else
                                {
                                    upper.SurveyDepth = (double)record.drilling_deviation[i - 1].measured_depth;
                                    upper.Azimuth = (double)record.drilling_deviation[i - 1].azimuth;
                                    upper.Inclination = (double)record.drilling_deviation[i - 1].inclination;
                                    upper.TVD = (double)record.drilling_deviation[i - 1].tvd;
                                    upper.EastWest = (double)record.drilling_deviation[i - 1].e_w;
                                    upper.NorthSouth = (double)record.drilling_deviation[i - 1].n_s;
                                }

                                var lower = new MWD();
                                lower.SurveyDepth = (double)record.drilling_deviation[i].measured_depth;
                                lower.Azimuth = (double)record.drilling_deviation[i].azimuth;
                                lower.Inclination = (double)record.drilling_deviation[i].inclination;

                                if (result.Status.Success)
                                {
                                    MinimumCurvatureResult resultMinimumCurvature = new MinimumCurvatureResult();
                                    if (lower.SurveyDepth > 0)
                                        resultMinimumCurvature = service_minimumCurvature.Calculate(upper, lower, well.target_direction);
                                    record.drilling_deviation[i].tvd = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.TVD : 0;
                                    record.drilling_deviation[i].v_section = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.VerticalSection : 0;
                                    record.drilling_deviation[i].n_s = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.North : 0;
                                    record.drilling_deviation[i].e_w = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.East : 0;
                                    record.drilling_deviation[i].dls = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.DLS : 0;

                                    newRecord.InjectFrom(record.drilling_deviation[i]);
                                    newRecord.seq = lastRecord == null ? i : (i + 1);
                                    newRecord.phase = true;
                                    newRecord.drilling_id = recordId;
                                    result.Status.Success &= service_drillingDeviation.SaveEntity(newRecord, ref isNewDeviation, (r) => record.drilling_deviation[i].id = r.id);
                                }
                            }
                        }
                    }

                    if (record.drilling_mud_1 != null)
                    {
                        var isMudNew = false;
                        var drillingMud = new drilling_mud();
                        drillingMud.InjectFrom(record.drilling_mud_1);
                        drillingMud.drilling_id = recordId;
                        result.Status.Success &= service_drillingMud.SaveEntity(drillingMud, ref isMudNew, (r) => drillingMud.id = r.id);
                    }
                    if (record.drilling_mud_2 != null)
                    {
                        var isMudNew = false;
                        var drillingMud = new drilling_mud();
                        drillingMud.InjectFrom(record.drilling_mud_2);
                        drillingMud.drilling_id = recordId;
                        result.Status.Success &= service_drillingMud.SaveEntity(drillingMud, ref isMudNew, (r) => drillingMud.id = r.id);
                    }
                    if (record.drilling_mud_3 != null)
                    {
                        var isMudNew = false;
                        var drillingMud = new drilling_mud();
                        drillingMud.InjectFrom(record.drilling_mud_3);
                        drillingMud.drilling_id = recordId;
                        result.Status.Success &= service_drillingMud.SaveEntity(drillingMud, ref isMudNew, (r) => drillingMud.id = r.id);
                    }

                    if (record.drilling_mud_pit != null && record.drilling_mud_pit.Count > 0)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_mud_pit where drilling_id = @0", recordId);
                        this._services.db.Execute(sql);

                        int i = 0;
                        foreach (var item in record.drilling_mud_pit)
                        {
                            var newRecord = new drilling_mud_pit();
                            newRecord.InjectFrom(item);
                            newRecord.drilling_id = recordId;
                            newRecord.seq = i;
                            result.Status.Success &= service_drillingMudPit.SaveEntity(newRecord, ref isNewDrillingMudPit, (r) => item.id = r.id);
                            i++;
                        }
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Mud Pit cannot be empty";
                        return result;
                    }

                    if (record.drilling_hole_and_casing != null && record.drilling_hole_and_casing.Count > 0)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_hole_and_casing where drilling_id = @0", recordId);
                        this._services.db.Execute(sql);
                        int i = 0;
                        foreach (var item in record.drilling_hole_and_casing)
                        {
                            var isNewDrillingHoleAndCasing = false;
                            var newRecord = new drilling_hole_and_casing();
                            newRecord.InjectFrom(item);
                            //newRecord.drilling_id = recordId;
                            newRecord.seq = i;
                            result.Status.Success &= service_drillingHoleAndCasing.SaveEntity(newRecord, ref isNewDrillingHoleAndCasing, (r) => item.id = r.id);
                            i++;
                        }
                    }

                    if (record.drilling_chemical_used != null && record.drilling_chemical_used.Count > 0)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_chemical_used where drilling_id = @0", recordId);
                        this._services.db.Execute(sql);

                        int i = 0;
                        foreach (var item in record.drilling_chemical_used)
                        {
                            var isNewDrillingChemicalUsed = false;
                            var newRecord = new drilling_chemical_used();
                            newRecord.InjectFrom(item);
                            newRecord.drilling_id = recordId;
                            newRecord.seq = i;
                            result.Status.Success &= service_drillingChemicalUsed.SaveEntity(newRecord, ref isNewDrillingChemicalUsed, (r) => item.id = r.id);
                            i++;
                        }
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Chemical Used cannot be empty";
                        return result;
                    }

                    if (record.drilling_total_volume != null)
                    {
                        var isMudNew = false;
                        var drillingTotalVolume = new drilling_total_volume();
                        drillingTotalVolume.InjectFrom(record.drilling_total_volume);
                        drillingTotalVolume.drilling_id = recordId;
                        result.Status.Success &= service_drillingTotalVolume.SaveEntity(drillingTotalVolume, ref isMudNew, (r) => record.drilling_total_volume.id = r.id);
                    }

                    if (record.drilling_aerated != null)
                    {
                        var isAerated = false;
                        var drillingAerated = new drilling_aerated();
                        drillingAerated.InjectFrom(record.drilling_aerated);
                        drillingAerated.drilling_id = recordId;
                        result.Status.Success &= service_drillingAerated.SaveEntity(drillingAerated, ref isAerated, (r) => record.drilling_aerated.id = r.id);
                    }

                    if (record.drilling_transport != null && record.drilling_transport.Count > 0)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_transport where drilling_id = @0", recordId);
                        this._services.db.Execute(sql);

                        foreach (var item in record.drilling_transport)
                        {
                            var newRecord = new drilling_transport();
                            newRecord.InjectFrom(item);
                            newRecord.drilling_id = recordId;
                            result.Status.Success &= service_drillingTransport.SaveEntity(newRecord, ref isNew, (r) => item.id = r.id);
                        }
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Drilling transport cannot be empty";
                        return result;
                    }

                    if (record.drilling_bulk != null && record.drilling_bulk.Count > 0)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_bulk where drilling_id = @0", recordId);
                        this._services.db.Execute(sql);

                        foreach (var item in record.drilling_bulk)
                        {
                            var newRecord = new drilling_bulk();
                            newRecord.InjectFrom(item);
                            newRecord.drilling_id = recordId;
                            result.Status.Success &= service_drillingBulk.SaveEntity(newRecord, ref isNewDrillingBulk, (r) => item.id = r.id);
                        }
                    }
                    //else
                    //{
                    //    result.Status.Success = false;
                    //    result.Status.Message = "Bulk item cannot be empty";
                    //    return result;
                    //}


                    if (record.drilling_personel != null && record.drilling_personel.Count > 0)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_personel where drilling_id = @0", recordId);
                        this._services.db.Execute(sql);

                        foreach (var item in record.drilling_personel)
                        {
                            var newRecord = new drilling_personel();
                            newRecord.InjectFrom(item);
                            newRecord.drilling_id = recordId;
                            result.Status.Success &= service_drillingPersonel.SaveEntity(newRecord, ref isNew, (r) => item.id = r.id);
                        }
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Personel on Board cannot be empty";
                        return result;
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Drilling has been saved.";
                    }

                    result.Data = new
                    {
                        recordId,
                        record.drilling.well_id,
                        drilling_total_volume_id = record.drilling_total_volume.id,
                        drilling_aerated_id = record.drilling_aerated.id,
                    };
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }


        public ApiResponse Save(drilling record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, this._services.dataContext.PrimaryTeamId, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse CreateNew(drilling record)
        {
            var result = new ApiResponse();
            var service_afe = new AfeServices(_services.dataContext);
            try
            {
                var isNew = false;
                var recordId = record.id;

                var recordAfe = service_afe.GetViewFirstOrDefault(" AND r.well_id = @0 ", record.well_id);

                if (recordAfe == null)
                {
                    result.Status.Success = false;
                    result.Status.Message = "Create new drilling record failed. This Well is not registered to 1 AFE. Please check your AFE.";
                    return result;
                }
                else if (recordAfe.is_closed ?? false)
                    throw new Exception("Well " + recordAfe.well_name + " has been Closed. Not available to create new drilling.");

                var lastDrilling = this.GetViewFirstOrDefault(" AND r.well_id = @0 ORDER BY r.drilling_date DESC ", record.well_id);

                record.daily_cost = null;
                record.dfs = 1;
                record.report_no = 0;
                record.previous_depth_md = 0;
                record.current_depth_tvd = 0;
                if (lastDrilling != null)
                {
                    record.dfs = lastDrilling.dfs + 1;
                    record.report_no = (short?)lastDrilling.dfs;
                    record.previous_depth_md = lastDrilling.current_depth_md;
                    record.previous_depth_tvd = lastDrilling.current_depth_tvd;
                }

                result.Status.Success = SaveEntity(record, ref isNew, _services.dataContext.PrimaryTeamId, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Message = ex.Message;
                if (ex.Message.Contains("Violation of UNIQUE KEY"))
                    result.Status.Message = "This well already have record on " + record.drilling_date.Value.Year + "-" + record.drilling_date.Value.Month + "-" + record.drilling_date.Value.Day;
                result.Status.Success = false;
            }
            return result;
        }

        public ApiResponse<vw_drilling> Detail(string recordId, Infrastructure.Session.UserSession session)
        {
            var result = new ApiResponse<vw_drilling>();
            try
            {
                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = this.GetViewById(recordId);
                result.Data.CanApprove = false;
                if (result.Data.approval_level > 0 && result.Data.approval_status == 2)
                {
                    approval apprdata = approval.FirstOrDefault("WHERE record_id=@0 AND status=2", recordId);

                    var appr = approver.FirstOrDefault("WHERE approval_id=@0", apprdata.id);

                    if (appr.approver_id == this._services.dataContext.AppUserId || session.Teams.Where(w => w.id.Contains(appr.approver_id)).Count() > 0)
                    {
                        result.Data.CanApprove = true;
                    }

                    //team teamdata = team.FirstOrDefault("WHERE id=@0", result.Data.approver_id);

                    //if (teamdata != null)
                    //{
                    //    team_member teammemberdata = team_member.FirstOrDefault("WHERE team_id=@0 AND application_user_id=@1", teamdata.id, this._services.dataContext.AppUserId);

                    //    if (teammemberdata != null)
                    //    {
                    //        result.Data.CanApprove = true;
                    //    }
                    //}
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse<List<vw_drilling>> getByDetail(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling>>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder
                        .Append("SELECT * FROM vw_drilling")
                        .Append("WHERE well_id = @0", wellId)
                        .Append("ORDER BY report_no ASC");

                    result.Data = db.Fetch<vw_drilling>(sql);
                }
                result.Status.Success = true;
                result.Status.Message = "Data retrieval successful";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = "Failed to retrieve data: " + ex.Message;
            }
            return result;
        }


        //public ApiResponse<vw_drilling> getForRkap(string wellId)
        //{
        //    var result = new ApiResponse<vw_drilling>();
        //    try
        //    {
        //        using (var db = new dreamwellRepo())
        //        {
        //            var sql = Sql.Builder
        //                .Append(@"
        //            SELECT TOP 1 
        //                *
        //            FROM 
        //                vw_drilling d
        //            WHERE 
        //                d.well_id = @0
        //            ORDER BY 
        //                d.drilling_date DESC", wellId);

        //            result.Data = db.FirstOrDefault<vw_drilling>(sql);
        //        }
        //        result.Status.Success = true;
        //        result.Status.Message = "Data retrieval successful";
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        result.Status.Success = false;
        //        result.Status.Message = "Failed to retrieve data: " + ex.Message;
        //    }
        //    return result;
        //}

        public ApiResponse<vw_drilling> getForRkap(string wellId)
        {
            var result = new ApiResponse<vw_drilling>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder
                        .Append(@"
                    SELECT 
                        d.*, 
                        w.water_depth 
                    FROM 
                        (SELECT TOP 1 
                            * 
                        FROM 
                            vw_drilling d 
                        WHERE 
                            d.well_id = @0 
                        ORDER BY 
                            d.drilling_date DESC) AS d
                    JOIN 
                        vw_well w ON d.well_id = w.id", wellId);

                    result.Data = db.FirstOrDefault<vw_drilling>(sql);
                }
                result.Status.Success = true;
                result.Status.Message = "Data retrieval successful";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = "Failed to retrieve data: " + ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling>> getLastDrilling(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling>>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder
                        .Append("SELECT * FROM vw_drilling")
                        .Append("WHERE well_id = @0", wellId)
                        .Append("AND drilling_date = (")
                        .Append("    SELECT MAX(drilling_date)")
                        .Append("    FROM vw_drilling")
                        .Append("    WHERE well_id = @0)", wellId)
                        .Append("AND submitted_by IS NOT NULL")
                        .Append("ORDER BY report_no ASC");

                    result.Data = db.Fetch<vw_drilling>(sql);
                }
                result.Status.Success = true;
                result.Status.Message = "Data retrieval successful";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = "Failed to retrieve data: " + ex.Message;
            }
            return result;

        }


        public ApiResponse<vw_drilling> getFirstdril(string wellId)
        {
            var result = new ApiResponse<vw_drilling>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder
                        .Append("SELECT TOP 1 * FROM vw_drilling")
                        .Append("WHERE well_id = @0", wellId)
                        .OrderBy("report_no ASC"); // Tambahkan pengurutan

                    result.Data = db.FirstOrDefault<vw_drilling>(sql); // Menggunakan FirstOrDefault untuk mendapatkan satu rekaman
                }
                result.Status.Success = true;
                result.Status.Message = "Data retrieval successful";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = "Failed to retrieve data: " + ex.Message;
            }
            return result;
        }



        public ApiResponse<List<vw_drilling>> GetDOl(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling>>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder.Append(drilling.DefaultView);
                    sql = Sql.Builder
                     .Append("SELECT r.dol FROM vw_drilling r")
                     .Append("WHERE r.well_id = @0 AND r.report_no = 0", wellId);
                    result.Data = db.Fetch<vw_drilling>(sql);
                }
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<vw_drilling> DetailNextDay(string recordId, Infrastructure.Session.UserSession session)
        {
            var result = new ApiResponse<vw_drilling>();
            try
            {
                result.Status.Success = true;
                result.Status.Message = "Successfully";
                var currentDriling = this.GetViewById(recordId);

                result.Data = this.GetViewFirstOrDefault($"and r.dfs = @0 and r.well_id = @1", (currentDriling.dfs + 1), currentDriling.well_id);
                result.Data.CanApprove = false;
                if (result.Data.approval_level > 0 && result.Data.approval_status == 2)
                {
                    approval apprdata = approval.FirstOrDefault("WHERE record_id=@0 AND status=2", recordId);

                    var appr = approver.FirstOrDefault("WHERE approval_id=@0", apprdata.id);

                    if (appr.approver_id == this._services.dataContext.AppUserId || session.Teams.Where(w => w.id.Contains(appr.approver_id)).Count() > 0)
                    {
                        result.Data.CanApprove = true;
                    }

                    //team teamdata = team.FirstOrDefault("WHERE id=@0", result.Data.approver_id);

                    //if (teamdata != null)
                    //{
                    //    team_member teammemberdata = team_member.FirstOrDefault("WHERE team_id=@0 AND application_user_id=@1", teamdata.id, this._services.dataContext.AppUserId);

                    //    if (teammemberdata != null)
                    //    {
                    //        result.Data.CanApprove = true;
                    //    }
                    //}
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }
        public ApiResponse<DrillingViewModel> GetLatest(string wellId)
        {
            var result = new ApiResponse<DrillingViewModel>();
            try
            {
                var recordData = new DrillingViewModel();
                var sql = @"SELECT
                            r.id,
                            r.created_by,
                            r.created_on,
                            r.modified_by,
                            r.modified_on,
                            r.approved_by,
                            r.approved_on,
                            r.submitted_by,
                            r.submitted_on,
                            r.is_active,
                            r.is_locked,
                            r.is_default,
                            r.owner_id,
                            r.organization_id,
                            o1.organization_name AS organization_name,
                            r.country_id,
                            c.country_name,
                            r.well_id,
                            w.well_name,
                            w.well_classification,
                            w.well_type,
                            w.latitude,
                            w.longitude,
                            f.id AS field_id,
                            f.field_name AS field_name,
                            r.drilling_date,
                            r.event,
                            r.drilling_contractor_id,
                            dc.contractor_name,
                            r.report_no,
                            r.rig_id,
                            r.rig_rating,
                            r.rig_rating_uom_id,
                            uo6.uom_code AS rig_rating_uom,
                            r.rig_type,
                            r.rig_power,
                            r.water_depth,
                            r.water_depth_uom_id,
                            uo1.uom_code AS water_depth_uom,
                            r.rkb_elevation,
                            r.rkb_elevation_uom_id,
                            uo2.uom_code AS rkb_elevation_uom,
                            r.rt_to_seabed,
                            r.rt_to_seabed_uom_id,
                            uo3.uom_code AS rt_to_seabed_uom,
                            r.rig_heading,
                            r.rig_heading_uom_id,
                            r.release_date,
                            a.id as afe_id,
                            rg.name as rig_name,
                            a.afe_no,
                            r.afe_cost,
                            r.afe_cost_currency_id,
                            r.daily_cost,
                            r.daily_cost_currency_id,
                            r.daily_mud_cost,
                            r.daily_mud_cost_currency_id,
                            r.cummulative_cost,
                            r.cummulative_cost_currency_id,
                            r.cummulative_mud_cost,
                            r.cummulative_mud_cost_currency_id,
                            r.planned_td,
                            r.planned_td_uom_id,
                            uo4.uom_code AS planned_td_uom,
                            r.planned_days,
                            r.planned_days_uom_id,
                            uo5.uom_code AS planned_days_uom,
                            r.dol,
                            r.dol_uom_id,
                            uo7.uom_code AS dol_uom,
                            r.dfs,
                            r.dfs_uom_id,
                            uo8.uom_code AS dfs_uom,
                            r.current_hole_size,
                            r.current_hole_size_uom_id,
                            uo9.uom_code AS current_hole_size_uom,
                            r.previous_depth_md,
                            r.previous_depth_md_uom_id,
                            uo10.uom_code AS previous_depth_md_uom,
                            r.previous_depth_tvd,
                            r.previous_depth_tvd_uom_id,
                            uo11.uom_code AS previous_depth_tvd_uom,
                            r.current_depth_md,
                            r.current_depth_md_uom_id,
                            uo12.uom_code AS current_depth_md_uom,
                            r.current_depth_tvd,
                            r.current_depth_tvd_uom_id,
                            uo13.uom_code AS current_depth_tvd_uom,
                            r.progress,
                            r.progress_uom_id,
                            uo14.uom_code AS progress_uom,

                            r.hsse_incident_acident,
                            r.hsse_environtmental_spills,
                            r.hsse_safety_alert_received,
                            r.hsse_proactive_safety,
                            r.hsse_near_miss_report,
                            r.hsse_exercise,
                            r.hsse_social_issues,
                            r.hsse_safety_meeting,
                            r.hsse_stop_cards,
                            r.hsse_tofs,
                            r.hsse_jsa,
                            r.hsse_inductions,
                            r.hsse_audits,
                            r.hsse_days_since_lti,
                            r.hsse_tbop_press,
                            r.hsse_tbop_func,
                            r.hsse_dkick,
                            r.hsse_dstrip,
                            r.hsse_dfire,
                            r.hsse_dmis_pers,
                            r.hsse_aband_rig,
                            r.hsse_dh2s,
                            r.hsse_description,
                            r.hsse_incident_description,
                            r.hsse_type,
                            r.hsse_lta,
                            r.hsse_h2stest,
                            r.hsse_mtg,
                            r.hsse_kick_trip,
                            r.hsse_kick_drill,

                            r.mudlogging_wob_min,
                            r.mudlogging_wob_max,
                            r.mudlogging_wob_uom_id,
                            uo15.uom_code AS mudlogging_wob_uom,
                            r.mudlogging_rpm_min,
                            r.mudlogging_rpm_max,
                            r.mudlogging_rpm_uom_id,
                            uo16.uom_code AS mudlogging_rpm_uom,
                            r.mudlogging_dhrpm_min,
                            r.mudlogging_dhrpm_max,
                            r.mudlogging_dhrpm_uom_id,
                            uo17.uom_code AS mudlogging_dhrpm_uom,
                            r.mudlogging_torque_min,
                            r.mudlogging_torque_max,
                            r.mudlogging_torque_uom_id,
                            uo18.uom_code AS mudlogging_torque_uom,
                            r.mudlogging_flowrate_min,
                            r.mudlogging_flowrate_max,
                            r.mudlogging_flowrate_uom_id,
                            uo19.uom_code AS mudlogging_flowrate_uom,
                            r.mudlogging_spp_min,
                            r.mudlogging_spp_max,
                            r.mudlogging_spp_uom_id,
                            uo20.uom_code AS mudlogging_spp_uom,
                            r.mudlogging_spm_min,
                            r.mudlogging_spm_max,
                            r.mudlogging_spm_uom_id,
                            uo21.uom_code AS mudlogging_spm_uom,
                            r.mudlogging_spmpress_min,
                            r.mudlogging_spmpress_max,
                            r.mudlogging_spmpress_uom_id,
                            uo22.uom_code AS mudlogging_spmpress_uom,
                            r.mudlogging_avg_rop_min,
                            r.mudlogging_avg_rop_max,
                            r.mudlogging_avg_rop_uom_id,
                            uo23.uom_code AS mudlogging_avg_rop_uom,

                            r.weather_general,
                            r.weather_wind_speed,
                            r.weather_wind_direction,
                            r.weather_temperature,
                            r.weather_temperature_low,
                            r.weather_visibility,
                            r.weather_cloud,
                            r.weather_barometer,
                            r.weather_wave,
                            r.weather_wave_period,
                            r.weather_wave_direction,
                            r.weather_height,
                            r.weather_current_speed,
                            r.weather_pitch,
                            r.weather_roll,
                            r.weather_heave,
                            r.weather_comments,
                            r.weather_road_condition,
                            r.weather_chill_factor,
                            r.operation_data_period,
                            r.operation_data_early,
                            r.operation_data_planned,

                            RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,
                            RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,
                            RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,
                            RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,
                            RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u4.last_name), N''), ' ', ISNULL(u4.first_name, N''))))  AS record_submitted_by,
                            t.team_name  AS record_owning_team,
                            row_number() over(order by r.created_on desc) as rn

                            FROM dbo.drilling AS r
                            LEFT OUTER JOIN dbo.well AS w
                              ON r.well_id = w.id
                            LEFT OUTER JOIN dbo.field AS f
                              ON w.field_id = f.id
                            LEFT OUTER JOIN dbo.country AS c
                              ON r.country_id = c.id
                            LEFT OUTER JOIN dbo.drilling_contractor AS dc
                              ON r.drilling_contractor_id = dc.id
                            LEFT OUTER JOIN dbo.afe AS a
                              ON a.well_id = w.id
                            LEFT OUTER JOIN dbo.rig AS rg
                              ON r.rig_id = rg.id
                            LEFT OUTER JOIN dbo.uom AS uo1
                              ON r.water_depth_uom_id = uo1.id
                            LEFT OUTER JOIN dbo.uom AS uo2
                              ON r.rkb_elevation_uom_id = uo2.id
                            LEFT OUTER JOIN dbo.uom AS uo3
                              ON r.rt_to_seabed_uom_id = uo3.id
                            LEFT OUTER JOIN dbo.uom AS uo4
                              ON r.planned_td_uom_id = uo4.id
                            LEFT OUTER JOIN dbo.uom AS uo5
                              ON r.planned_days_uom_id = uo5.id
                            LEFT OUTER JOIN dbo.uom AS uo6
                              ON r.rig_rating_uom_id = uo6.id
                            LEFT OUTER JOIN dbo.uom AS uo7
                              ON r.dol_uom_id = uo7.id
                            LEFT OUTER JOIN dbo.uom AS uo8
                              ON r.dfs_uom_id = uo8.id
                            LEFT OUTER JOIN dbo.uom AS uo9
                              ON r.current_hole_size_uom_id = uo9.id
                            LEFT OUTER JOIN dbo.uom AS uo10
                              ON r.previous_depth_md_uom_id = uo10.id
                            LEFT OUTER JOIN dbo.uom AS uo11
                              ON r.previous_depth_tvd_uom_id = uo11.id
                            LEFT OUTER JOIN dbo.uom AS uo12
                              ON r.current_depth_md_uom_id = uo12.id
                            LEFT OUTER JOIN dbo.uom AS uo13
                              ON r.current_depth_tvd_uom_id = uo13.id
                            LEFT OUTER JOIN dbo.uom AS uo14
                              ON r.progress_uom_id = uo14.id
                            LEFT OUTER JOIN dbo.uom AS uo15
                              ON r.mudlogging_wob_uom_id = uo15.id
                            LEFT OUTER JOIN dbo.uom AS uo16
                              ON r.mudlogging_rpm_uom_id = uo16.id
                            LEFT OUTER JOIN dbo.uom AS uo17
                              ON r.mudlogging_dhrpm_uom_id = uo17.id
                            LEFT OUTER JOIN dbo.uom AS uo18
                              ON r.mudlogging_torque_uom_id = uo18.id
                            LEFT OUTER JOIN dbo.uom AS uo19
                              ON r.mudlogging_flowrate_uom_id = uo19.id
                            LEFT OUTER JOIN dbo.uom AS uo20
                              ON r.mudlogging_spp_uom_id = uo20.id
                            LEFT OUTER JOIN dbo.uom AS uo21
                              ON r.mudlogging_spm_uom_id = uo21.id
                            LEFT OUTER JOIN dbo.uom AS uo22
                              ON r.mudlogging_spmpress_uom_id = uo22.id
                            LEFT OUTER JOIN dbo.uom AS uo23
                              ON r.mudlogging_avg_rop_uom_id = uo23.id

                            LEFT OUTER JOIN dbo.organization AS o1
                              ON r.organization_id = o1.id
                            LEFT OUTER JOIN dbo.application_user AS u0
                              ON r.created_by = u0.id
                            LEFT OUTER JOIN dbo.application_user AS u1
                              ON r.modified_by = u1.id
                            LEFT OUTER JOIN dbo.application_user AS u2
                              ON r.approved_by = u2.id
                            LEFT OUTER JOIN dbo.application_user AS u3
                              ON r.owner_id = u3.id
                            LEFT OUTER JOIN dbo.application_user AS u4
                              ON r.submitted_by = u4.id
                            LEFT OUTER JOIN dbo.team AS t
                              ON r.owner_id = t.id ";

                var Qry = Sql.Builder.Append(string.Format("SELECT * FROM ({0} WHERE r.well_id = @0 ) AS T WHERE rn = 1", sql), wellId);
                recordData.drilling = this._services.db.FirstOrDefault<vw_drilling>(Qry);
                //recordData.drilling_operation = service_drillingOperation.GetViewAll(" AND r.drilling_id=@0 ", recordData.drilling.id);
                //recordData.drilling = this.GetViewById(recordId);
                //recordData.drilling_operation = service_drillingOperation.GetViewAll(" AND r.drilling_id=@0 ", recordId);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = recordData;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse<vw_drilling> ViewDrilling(string recordId)
        {
            var result = new ApiResponse<vw_drilling>();
            try
            {
                result.Data = this.GetViewById(recordId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public Page<vw_drilling> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        //public ApiResponse<bool> DeviationData ()
        //{
        //    var result = new ApiResponse<bool>();
        //    var deviationData = new List<drilling_deviation>();

        //    if (deviationData != null && deviationData.Count > 0)
        //    {
        //        for (var i = 0; i < deviationData.Count; i++)
        //        {
        //            double last_measureDepth = 0;
        //            double last_tvd = 0;
        //            double last_inclination = 0;
        //            double last_azimuth = 0;
        //            double last_e_w = 0;
        //            double Last_n_s = 0;
        //            if (i > 0)
        //            {
        //                last_measureDepth = (double)deviationData[i - 1].measured_depth;
        //                last_tvd = (double)deviationData[i - 1].tvd;
        //                last_inclination = (double)deviationData[i - 1].inclination;
        //                last_azimuth = (double)deviationData[i - 1].azimuth;
        //                last_e_w = (double)deviationData[i - 1].e_w;
        //                Last_n_s = (double)deviationData[i - 1].n_s;
        //            }

        //            double current_measureDepth = (double)deviationData[i].measured_depth;
        //            double current_inclination = (double)deviationData[i].inclination;
        //            double current_azimuth = (double)deviationData[i].azimuth;

        //            var delta_measureDepth = current_measureDepth > 0 ? current_measureDepth - last_measureDepth : 0;
        //            //var radians_last_inclination = last_inclination * Math.PI / 180;
        //            //var radians_current_inclination = current_inclination * Math.PI / 180;

        //            //var radians_last_azimuth = last_azimuth * Math.PI / 180;
        //            //var radians_current_azimuth = current_azimuth * Math.PI / 180;

        //            //var dog_leg_angle = Math.Acos(Math.Cos(current_inclination - last_inclination) - (Math.Sin(radians_current_inclination) * Math.Sin(radians_last_inclination) * (1 - Math.Cos(radians_current_azimuth - radians_last_azimuth))));
        //            var dog_leg_angle = Math.Acos(Math.Cos(current_inclination - last_inclination) - (Math.Sin(last_inclination) * Math.Sin(current_inclination) * (1 - Math.Cos(current_azimuth - last_azimuth))));
        //            //-- dog leg angle result in radians
        //            double degrees_of_dog_leg_angle = (180 / Math.PI) * dog_leg_angle;

        //            var ratioFactor = (2 / dog_leg_angle) * Math.Tan(dog_leg_angle / 2);
        //            //var ratioFactor = (2 / dog_leg_angle) * Math.Tan(degrees_of_dog_leg_angle / 2);

        //            var TVD = current_measureDepth > 0 ? delta_measureDepth / 2 * (Math.Cos(last_inclination) + Math.Cos(current_inclination)) * ratioFactor : 0;
        //            var north = current_measureDepth > 0 ? delta_measureDepth / 2 * ((Math.Sin(last_inclination) * Math.Cos(last_azimuth)) + (Math.Sin(current_inclination) * Math.Cos(current_azimuth))) * ratioFactor : 0;
        //            var east = current_measureDepth > 0 ? delta_measureDepth / 2 * ((Math.Sin(last_inclination) * Math.Sin(last_azimuth)) + (Math.Sin(current_inclination) * Math.Sin(current_azimuth))) * ratioFactor : 0;

        //        }
        //    }
        //    return result;
        //}

        public ApiResponse Submit(string recordId)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    //this._services.dataContext.

                    application_user appuser = application_user.FirstOrDefault(" WHERE id=@0", this._services.dataContext.AppUserId);

                    if (appuser == null)
                        throw new Exception("User Id not found");

                    team teamdata = team.FirstOrDefault(" WHERE team_name=@0", "Drilling Engineer");

                    if (teamdata == null)
                        throw new Exception("Drilling Enginerr not found");

                    var drillingRecord = drilling.FirstOrDefault("WHERE id=@0", recordId);
                    drillingRecord.submitted_by = this._services.dataContext.AppUserId;
                    drillingRecord.submitted_on = DateTime.Now;

                    result.Status.Success = this._services.dataContext.SaveEntity<drilling>(drillingRecord, false);

                    ApprovalServices appr = new ApprovalServices(this._services.dataContext);
                    result.Status.Success &= appr.CreateApproval(recordId, this._services.dataContext.AppUserId, 0, approvalStatus.Accepted);
                    result.Status.Success &= appr.CreateApproval(recordId, teamdata.id, 1, approvalStatus.InProcess);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Data has been Submited";
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }

            return result;
        }

        public ApiResponse Approve(ApprovalRequest data, Infrastructure.Session.UserSession userSession)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    var appr = approval.FirstOrDefault("WHERE record_id=@0 AND [status]=2", data.recordId);

                    if (appr != null)
                    {
                        ApprovalServices apprservice = new ApprovalServices(this._services.dataContext);

                        var approverdata = approver.FirstOrDefault("WHERE approval_id=@0", appr.id);
                        var dataManagerZona = team.FirstOrDefault("WHERE team_leader=@0", userSession.AppUserId);

                        if (this._services.dataContext.AppUserId == approverdata?.approver_id || userSession.Teams.Where(o => o.id.Contains(approverdata?.approver_id)).Count() > 0 || dataManagerZona != null)
                        {
                            if (data.isAccepted)
                            {
                                appr.status = (short)approvalStatus.Accepted;
                                if (appr.level < 2)
                                {
                                    result.Status.Success = apprservice.UpdateApproval(appr, approvalStatus.Accepted, data.description);
                                    result.Status.Success &= apprservice.CreateApproval(data.recordId, apprservice.GetManager(), Convert.ToInt16(appr.level + 1), approvalStatus.InProcess);
                                }
                                else
                                {
                                    result.Status.Success = apprservice.UpdateApproval(appr, approvalStatus.Accepted, data.description);
                                }
                            }
                            else
                            {
                                var listapprovaldata = approval.Fetch("WHERE record_id=@0 AND level <= @1 ORDER BY level DESC", data.recordId, Convert.ToInt16(appr.level));
                                if (listapprovaldata.Count > 0)
                                {
                                    approvalStatus apprstatus = approvalStatus.Waiting;
                                    result.Status.Success = true;
                                    for (int i = 0; i < listapprovaldata.Count; i++)
                                    {
                                        listapprovaldata[i].status = (short)approvalStatus.Waiting;
                                        apprstatus = approvalStatus.Waiting;

                                        if (i == 0)
                                        {
                                            apprstatus = approvalStatus.Rejected;
                                        }
                                        else
                                        {
                                            if (i == listapprovaldata.Count - 1)
                                            {
                                                listapprovaldata[i].status = (short)approvalStatus.InProcess;
                                                apprstatus = approvalStatus.InProcess;
                                            }
                                        }

                                        result.Status.Success &= apprservice.UpdateApproval(listapprovaldata[i], apprstatus, data.description);
                                    }

                                    drilling dril_data = drilling.FirstOrDefault("WHERE id=@0", data.recordId);

                                    if (dril_data != null)
                                    {
                                        dril_data.submitted_by = null;
                                        dril_data.submitted_on = null;
                                        result.Status.Success &= this._services.dataContext.SaveEntity<drilling>(dril_data, false);
                                    }
                                }
                            }
                        }
                        else
                        {
                            result.Status.Success = false;
                            result.Status.Message = "You are not authorize to this action";
                        }

                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponseCount<DrillingViewModel> CountTotalDDR(string isadmin)
        {
            var result = new ApiResponseCount<DrillingViewModel>();
            try
            {
                var recordData = new DrillingViewModel();

                var Qry = Sql.Builder.Append(string.Format("select count (id) as countId from drilling where is_active=1"));
                if (isadmin == "NO")
                {
                    Qry = Sql.Builder.Append(string.Format("select count(drilling.id) FROM drilling LEFT OUTER JOIN well ON well.id = drilling.well_id where drilling.is_active=1 and well.business_unit_id=@0"), _services.dataContext.PrimaryBusinessUnitId);
                }
                this._services.db.CommandTimeout = 3000;
                int totalDDR = this._services.db.FirstOrDefault<int>(Qry);
                //recordData.drilling_operation = service_drillingOperation.GetViewAll(" AND r.drilling_id=@0 ", recordData.drilling.id);
                //recordData.drilling = this.GetViewById(recordId);
                //recordData.drilling_operation = service_drillingOperation.GetViewAll(" AND r.drilling_id=@0 ", recordId);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = totalDDR;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                result.Data = 0;
            }

            return result;
        }


        public ApiResponseCount<int> CountTotalApproved()
        {
            var result = new ApiResponseCount<int>();
            try
            {
                var Qry = PetaPoco.Sql.Builder
                    .Append("SELECT COUNT(id) FROM vw_drilling WHERE is_active = @0 AND submitted_by IS NOT NULL AND approval_level = 2 AND approval_status = 200", true);

                if (!this._services.dataContext.IsSystemAdministrator)
                {
                    Qry.Append(" AND record_owning_team = @0", this._services.dataContext.PrimaryTeam);
                }

                this._services.db.CommandTimeout = 3000;
                int totalApprovedCount = this._services.db.ExecuteScalar<int>(Qry);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = totalApprovedCount;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                result.Data = 0;
            }

            return result;
        }

        public ApiResponseCount<int> CountTotalApproval()
        {
            var result = new ApiResponseCount<int>();
            try
            {
                var Qry = PetaPoco.Sql.Builder
                    .Append("SELECT COUNT(id) FROM vw_drilling WHERE is_active= @0 AND submitted_by IS NOT NULL AND approval_status = @1 ", true, 2);

                if (!this._services.dataContext.IsSystemAdministrator)
                {
                    Qry.Append(" AND record_owning_team = @0", this._services.dataContext.PrimaryTeam);
                }

                this._services.db.CommandTimeout = 3000;
                int totalApprovedCount = this._services.db.ExecuteScalar<int>(Qry);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = totalApprovedCount;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                result.Data = 0;
            }

            return result;
        }

        public ApiResponseCount<int> CountRejected()
        {
            var result = new ApiResponseCount<int>();
            try
            {
                var Qry = PetaPoco.Sql.Builder
                    .Append("SELECT COUNT(id) FROM vw_drilling WHERE is_active = 1 AND submitted_by IS NOT NULL AND (approval_rejected_level = 1 OR approval_rejected_level = 2)");

                if (!this._services.dataContext.IsSystemAdministrator)
                {
                    Qry.Append(" AND record_owning_team = @0", this._services.dataContext.PrimaryTeam);
                }

                this._services.db.CommandTimeout = 3000;
                int totalApprovedCount = this._services.db.ExecuteScalar<int>(Qry);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = totalApprovedCount;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                result.Data = 0;
            }

            return result;
        }
        public ApiResponse<List<DrillingViewModel>> GetLatestDrillingDataAsync(string fieldId)
        {
            var result = new ApiResponse<List<DrillingViewModel>>();
            try
            {
                //var sql = @"
                //WITH CTE AS (
                //    SELECT 
                //        d.id, 
                //        d.well_id, 
                //        d.well_name, 
                //        d.field_id, 
                //        d.field_name, 
                //        d.drilling_date,
                //        d.dfs, 
                //        d.event,
                //        doa.category, 
                //        w.planned_td, 
                //        doa.depth,
                //        MAX(CASE WHEN uom.object_name = 'Planned TD' THEN uom.uom_code END) AS planned_td_uom,
                //        MAX(CASE WHEN uom.object_name = 'Depth' THEN uom.uom_code END) AS depth_uom,
                //        ROW_NUMBER() OVER (PARTITION BY d.well_name ORDER BY d.drilling_date DESC) AS row_num
                //    FROM 
                //        vw_drilling d
                //    LEFT JOIN 
                //        vw_drilling_operation_activity doa ON d.id = doa.drilling_id
                //    LEFT JOIN 
                //        vw_well w ON w.id = d.well_id
                //    LEFT JOIN 
                //        vw_well_object_uom_map uom ON uom.well_id = d.well_id 
                //           AND uom.object_name IN ('Planned TD', 'Depth')
                //    GROUP BY 
                //        d.id, d.well_id, d.well_name, d.field_id, d.field_name, 
                //        d.drilling_date, d.dfs, d.event, doa.category, w.planned_td, doa.depth
                //)
                //SELECT *
                //FROM CTE
                //WHERE field_id = @0 AND row_num = 1
                //ORDER BY drilling_date DESC";

                var sql = @"
                WITH CTE AS (
                    SELECT 
                        d.id, 
                        d.well_id, 
                        d.well_name, 
                        d.field_id, 
                        d.field_name, 
                        d.drilling_date,
                        d.dfs, 
                        d.event,
                        w.planned_td, 
                        -- Fetching UOM codes for Planned TD and Depth
                        MAX(CASE WHEN uom.object_name = 'Planned TD' THEN uom.uom_code END) AS planned_td_uom,
                        MAX(CASE WHEN uom.object_name = 'Depth' THEN uom.uom_code END) AS depth_uom,
                        ROW_NUMBER() OVER (PARTITION BY d.well_name ORDER BY d.drilling_date DESC) AS row_num
                    FROM 
                        vw_drilling d
                    LEFT JOIN 
                        vw_drilling_operation_activity doa ON d.id = doa.drilling_id
                    LEFT JOIN 
                        vw_well w ON w.id = d.well_id
                    LEFT JOIN 
                        vw_well_object_uom_map uom ON uom.well_id = d.well_id 
                           AND uom.object_name IN ('Planned TD', 'Depth')
                    GROUP BY 
                        d.id, d.well_id, d.well_name, d.field_id, d.field_name, 
                        d.drilling_date, d.dfs, d.event, w.planned_td
                ),
                LatestOperation AS (
                    SELECT 
                        drilling_id,
                        category,
                        operation_start_date,
                        operation_end_date,
                        depth,
                        ROW_NUMBER() OVER (PARTITION BY drilling_id ORDER BY operation_start_date DESC) AS op_row_num
                    FROM 
                        vw_drilling_operation_activity
                )
                SELECT 
                    CTE.*,
                    LatestOperation.category AS category,
                    LatestOperation.depth AS depth,
                    LatestOperation.operation_start_date,
                    LatestOperation.operation_end_date
                FROM 
                    CTE
                LEFT JOIN 
                    LatestOperation ON CTE.id = LatestOperation.drilling_id AND LatestOperation.op_row_num = 1
                WHERE 
                    CTE.field_id = @0
                    AND CTE.row_num = 1
                ORDER BY 
                    CTE.drilling_date DESC";

                // Fetch all drilling data based on the fieldId
                var drillingDataList = this._services.db.Fetch<vw_drilling>(Sql.Builder.Append(sql, fieldId));

                if (drillingDataList == null || !drillingDataList.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No drilling data found for the given field ID.";
                    return result;
                }

                var viewModelList = new List<DrillingViewModel>();

                // Loop through each drilling record
                foreach (var drillingData in drillingDataList)
                {
                    var drillingOperationActivities = this._services.db.Fetch<vw_drilling_operation_activity>(
                        "SELECT * FROM vw_drilling_operation_activity WHERE drilling_id = @0", drillingData.id);

                    var wellData = this._services.db.FirstOrDefault<vw_well>(
                        "SELECT * FROM vw_well WHERE id = @0", drillingData.well_id);

                    var uomData = this._services.db.Fetch<vw_well_object_uom_map>(
                        "SELECT well_id, object_name, uom_code FROM vw_well_object_uom_map WHERE well_id = @0 AND object_name IN ('Planned TD', 'Depth')",
                        drillingData.well_id);

                    string plannedTdUom = uomData.FirstOrDefault(x => x.object_name == "Planned TD")?.uom_code;
                    string depthUom = uomData.FirstOrDefault(x => x.object_name == "Depth")?.uom_code;

                    if (drillingData != null && wellData != null)
                    {
                        var viewModel = new DrillingViewModel
                        {
                            drilling = drillingData,
                            drilling_operation_activity = drillingOperationActivities ?? new List<vw_drilling_operation_activity>(),
                            well = wellData,
                            PlannedTdUom = plannedTdUom,  // Assign Planned TD UOM
                            DepthUom = depthUom           // Assign Depth UOM
                        };

                        // Optionally set the category from the first operation activity
                        if (drillingOperationActivities.Any())
                        {
                            viewModel.Category = drillingOperationActivities.First().category;
                        }

                        viewModelList.Add(viewModel);
                    }
                }

                result.Data = viewModelList;
                result.Status.Success = true;
                result.Status.Message = "Successfully retrieved the drilling data.";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse<List<DrillingViewModel>> GetLatestDrillingPTNPT(string fieldId)
        {
            var result = new ApiResponse<List<DrillingViewModel>>();
            try
            {
                var sql = @"
                    WITH CTE AS (
                        SELECT 
                            d.id, 
                            d.well_id, 
                            d.well_name, 
                            d.field_id, 
                            d.field_name, 
                            d.drilling_date,
                            d.dfs, 
                            d.event,
                            w.planned_td, 
                            MAX(CASE WHEN uom.object_name = 'Planned TD' THEN uom.uom_code END) AS planned_td_uom,
                            MAX(CASE WHEN uom.object_name = 'Depth' THEN uom.uom_code END) AS depth_uom,
                            ROW_NUMBER() OVER (PARTITION BY d.well_name ORDER BY d.drilling_date DESC) AS row_num
                        FROM 
                            vw_drilling d
                        LEFT JOIN 
                            vw_drilling_operation_activity doa ON d.id = doa.drilling_id
                        LEFT JOIN 
                            vw_well w ON w.id = d.well_id
                        LEFT JOIN 
                            vw_well_object_uom_map uom ON uom.well_id = d.well_id 
                               AND uom.object_name IN ('Planned TD', 'Depth')
                        GROUP BY 
                            d.id, d.well_id, d.well_name, d.field_id, d.field_name, 
                            d.drilling_date, d.dfs, d.event, w.planned_td
                    ),
                    LatestOperation AS (
                        SELECT 
                            drilling_id,
                            category,
                            operation_start_date,
                            operation_end_date,
                            depth,
                            ROW_NUMBER() OVER (PARTITION BY drilling_id ORDER BY operation_start_date DESC) AS op_row_num
                        FROM 
                            vw_drilling_operation_activity
                    ),
                    HoursSummary AS (
                        SELECT 
                            d.well_id,
                            SUM(CASE WHEN i.type = 1 THEN s.interval_hours ELSE 0 END) AS total_hours_pt,
                            SUM(CASE WHEN i.type = 2 THEN s.interval_hours ELSE 0 END) AS total_hours_npt,
                            SUM(s.interval_hours) AS total_hours
                        FROM 
                            drilling d
                        INNER JOIN 
                            drilling_operation_activity doa ON d.id = doa.drilling_id
                        INNER JOIN 
                            iadc AS i ON i.id = doa.iadc_id
                        CROSS APPLY (
                            SELECT * 
                            FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)
                        ) AS s
                        WHERE 
                            d.well_id = d.well_id
                        GROUP BY 
                            d.well_id
                    )
                    SELECT 
                        CTE.*,
                        LatestOperation.category AS latest_category,
                        LatestOperation.depth AS latest_depth,
                        LatestOperation.operation_start_date,
                        LatestOperation.operation_end_date,
                        HoursSummary.total_hours_pt,
                        HoursSummary.total_hours_npt,
                        HoursSummary.total_hours
                    FROM 
                        CTE
                    LEFT JOIN 
                        LatestOperation ON CTE.id = LatestOperation.drilling_id AND LatestOperation.op_row_num = 1
                    LEFT JOIN 
                        HoursSummary ON CTE.well_id = HoursSummary.well_id
                    WHERE 
                        CTE.field_id = @0
                        AND CTE.row_num = 1
                    ORDER BY 
                        CTE.drilling_date DESC";

                var drillingDataList = this._services.db.Fetch<DrillingViewModel>(Sql.Builder.Append(sql, fieldId));

                if (drillingDataList == null || !drillingDataList.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No drilling data found for the given field ID.";
                    return result;
                }

                result.Data = drillingDataList;
                result.Status.Success = true;
                result.Status.Message = "Successfully retrieved the drilling data.";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse<List<DrillingViewModel>> GetDrillingPipe(string fieldId)
        {
            var result = new ApiResponse<List<DrillingViewModel>>();
            try
            {
                var sql = @"
                    SELECT 
                        d.field_id, 
                        d.field_name, 
                        d.well_id, 
                        d.well_name, 
                        doa.iadc_code, 
                        doa.iadc_description, 
                        SUM(CAST(DATEDIFF(MINUTE, doa.operation_start_date, doa.operation_end_date) AS DECIMAL(10,1)) / 60) AS total_operation_duration_hours
                    FROM 
                        vw_drilling d
                    LEFT JOIN 
                        vw_drilling_operation_activity doa 
                        ON d.id = doa.drilling_id
                    WHERE 
                        doa.iadc_code = '20c' 
                        AND d.field_id = @0
                    GROUP BY 
                        d.field_id, 
                        d.field_name, 
                        d.well_id, 
                        d.well_name, 
                        doa.iadc_code, 
                        doa.iadc_description
                    ORDER BY 
                        d.well_name ASC";

                var drillingDataList = this._services.db.Fetch<DrillingViewModel>(Sql.Builder.Append(sql, fieldId));

                if (drillingDataList == null || !drillingDataList.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No drilling data found for the given field ID.";
                    return result;
                }

                result.Data = drillingDataList;
                result.Status.Success = true;
                result.Status.Message = "Successfully retrieved the drilling data.";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        //public ApiResponse<List<DrillingViewModel>> GetDrillingPipe(string fieldId)
        //{
        //    var result = new ApiResponse<List<DrillingViewModel>>();
        //    try
        //    {
        //        var sql = @"
        //            SELECT 
        //                d.field_id, 
        //                d.field_name, 
        //                d.well_id, 
        //                d.well_name, 
        //                doa.iadc_code, 
        //                doa.iadc_description, 
        //                SUM(CAST(DATEDIFF(MINUTE, doa.operation_start_date, doa.operation_end_date) AS DECIMAL(10,1)) / 60) AS total_operation_duration_hours
        //            FROM 
        //                vw_drilling d
        //            LEFT JOIN 
        //                vw_drilling_operation_activity doa 
        //                ON d.id = doa.drilling_id
        //            WHERE 
        //                doa.iadc_code = '20c' 
        //                AND d.field_id = @0
        //            GROUP BY 
        //                d.field_id, 
        //                d.field_name, 
        //                d.well_id, 
        //                d.well_name, 
        //                doa.iadc_code, 
        //                doa.iadc_description
        //            ORDER BY 
        //                d.well_name ASC";

        //        var drillingDataList = this._services.db.Fetch<vw_drilling>(sql, fieldId);

        //        if (drillingDataList == null || !drillingDataList.Any())
        //        {
        //            result.Status.Success = false;
        //            result.Status.Message = "No drilling data found for the given field ID.";
        //            return result;
        //        }

        //        var viewModelList = new List<DrillingViewModel>();

        //        // Loop untuk setiap data pengeboran
        //        foreach (var drillingData in drillingDataList)
        //        {
        //            var drillingOperationActivities = this._services.db.Fetch<vw_drilling_operation_activity>(
        //                "SELECT * FROM vw_drilling_operation_activity WHERE drilling_id = @0", drillingData.id);

        //            var wellData = this._services.db.FirstOrDefault<vw_well>(
        //                "SELECT * FROM vw_well WHERE id = @0", drillingData.well_id);

        //            if (drillingData != null && wellData != null)
        //            {
        //                var viewModel = new DrillingViewModel
        //                {
        //                    drilling = drillingData,
        //                    drilling_operation_activity = drillingOperationActivities ?? new List<vw_drilling_operation_activity>(),
        //                    well = wellData
        //                };

        //                viewModelList.Add(viewModel);
        //            }
        //        }

        //        result.Data = viewModelList;
        //        result.Status.Success = true;
        //        result.Status.Message = "Successfully retrieved the drilling data.";
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        result.Status.Success = false;
        //        result.Status.Message = ex.Message;
        //    }

        //    return result;
        //}

    }
}
