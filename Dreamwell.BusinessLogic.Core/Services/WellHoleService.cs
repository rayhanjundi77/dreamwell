﻿using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dreamwell.DataAccess.Models.Entity;
using Dreamwell.Infrastructure;
using CommonTools;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellHoleService
    {
        WellHoleRepository repo;
        WellCasingRepository wellCasingRepository;
        HoleRepository holeRepo;
        CasingRepository casingRepository;
        DataContext _context;
        public WellHoleService(DataContext context)
        {
            repo = new WellHoleRepository(context);
            wellCasingRepository = new WellCasingRepository(context);
            holeRepo = new HoleRepository(context);
            casingRepository = new CasingRepository(context);
            _context = context;
        }

        public async Task<ApiResponse> Save(WellHoleRequest wellHoleRequest)
        {
            var result = new ApiResponse();
            try
            {
                var wellHole = new WellHole
                {
                    id = wellHoleRequest.id,
                    hole_depth = wellHoleRequest.hole_depth,
                    hole_id = wellHoleRequest.hole_id,
                    plan_cost = wellHoleRequest.plan_cost,
                    well_id = wellHoleRequest.well_id,
                    plan_volume = wellHoleRequest.plan_volume,
                };
                var isNew = string.IsNullOrEmpty(wellHole.id);
                var resultHole = await repo.Save(wellHole, isNew);
                wellCasingRepository.Clear(wellHole.id);
                if(!wellHoleRequest.hole_type && wellHoleRequest.casings != null)
                {
                    foreach (var wellCasing in wellHoleRequest.casings)
                    {
                        wellCasing.well_hole_id = wellHole.id;
                        Guid guidOutput;
                        if (!Guid.TryParse(wellCasing.casing_id, out guidOutput))
                        {
                            var casing = casingRepository.CheckIfExist(wellHoleRequest.hole_id, wellCasing.casing_id);
                            if(casing == null)
                            {
                                casing = new Casing();
                                casing.hole_id = wellHoleRequest.hole_id;
                                casing.name = wellCasing.casing_id;
                                casing = await casingRepository.Save(casing, true);
                            }
                            wellCasing.casing_id = casing.id;
                        }
                        await wellCasingRepository.Save(wellCasing, true);
                    }
                }

                result.Status.Success = true;
                if (isNew)
                {
                    result.Status.Message = "The data has been added.";
                }
                else
                {
                    result.Status.Message = "The data has been updated.";
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public async Task<ApiResponse> GetAll(string wellId)
        {
            var result = new ApiResponse();
            try
            {
                var wellHoleResults = await repo.Get($" and well_id = '{wellId}' order by created_on asc");
                var sb = new StringBuilder();
                foreach(var id in wellHoleResults.Select(x => x.id).ToArray())
                {
                    sb.Append($"'{id}',");
                }

                var wellCasingResults = await wellCasingRepository.Get($" and well_hole_id in ({ sb.ToString().Remove(sb.Length -1) }) order by created_on asc");
                var holes = await holeRepo.Get();
                var casings = await casingRepository.Get();
                List<WellHoleResponse> wellHoleResultsList = new List<WellHoleResponse>();
                foreach(var wellHole in wellHoleResults)
                {
                    var hole = holes.First(x => x.id == wellHole.hole_id);
                    var wellHoleResponse = new WellHoleResponse
                    {
                        id = wellHole.id,
                        hole_depth = wellHole.hole_depth,
                        hole_name = hole.name,
                        plan_cost = wellHole.plan_cost,
                        plan_volume = wellHole.plan_volume,
                        hole_type = hole.hole_type,
                    };

                    var wellCasings = wellCasingResults.Where(x => x.well_hole_id == wellHole.id).ToList();
                    List<WellCasingResponse> wellCasingResultsList = new List<WellCasingResponse>();
                    foreach (var wellCasing in wellCasings)
                    {
                        wellCasingResultsList.Add(new WellCasingResponse{
                            casing_connection = wellCasing.casing_connection,
                            casing_name = casings.First(x => x.id == wellCasing.casing_id).name,
                            casing_grade = wellCasing.casing_grade,
                            casing_setting_depth = wellCasing.casing_setting_depth,
                            casing_top_of_liner = wellCasing.casing_top_of_liner,
                            casing_type = wellCasing.casing_type,
                            casing_weight = wellCasing.casing_weight,
                        });
                    }
                    wellHoleResponse.casings = wellCasingResultsList;
                    wellHoleResultsList.Add(wellHoleResponse);
                }
                result.Data = wellHoleResultsList;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponse> Get(string wellHoleId)
        {
            var result = new ApiResponse();
            try
            {
                var wellHoleResult = await repo.GetById(wellHoleId);
                var wellCasingResults = await wellCasingRepository.Get($" and well_hole_id = '{wellHoleResult.id}'");
                var holes = await holeRepo.Get();
                var casings = await casingRepository.Get();
                
                var hole = holes.First(x => x.id == wellHoleResult.hole_id);
                var wellHoleResponse = new WellHoleResponse
                {
                    id = wellHoleResult.id,
                    hole_depth = wellHoleResult.hole_depth,
                    hole_id = wellHoleResult.hole_id,
                    hole_name = hole.name,
                    plan_cost = wellHoleResult.plan_cost,
                    plan_volume = wellHoleResult.plan_volume,
                    hole_type = hole.hole_type,
                };

                List<WellCasingResponse> wellCasingResultsList = new List<WellCasingResponse>();
                foreach (var wellCasing in wellCasingResults)
                {
                    var casing = casings.First(x => x.id == wellCasing.casing_id);
                    wellCasingResultsList.Add(new WellCasingResponse
                    {
                        casing_connection = wellCasing.casing_connection,
                        casing_id = casing.id,
                        casing_name = casing.name,
                        casing_grade = wellCasing.casing_grade,
                        casing_setting_depth = wellCasing.casing_setting_depth,
                        casing_top_of_liner = wellCasing.casing_top_of_liner,
                        casing_type = wellCasing.casing_type,
                        casing_weight = wellCasing.casing_weight,
                    });
                }
                wellCasingResultsList.Reverse();
                wellHoleResponse.casings = wellCasingResultsList;
                
                result.Data = wellHoleResponse;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponse> Delete(string welHoleId)
        {
            var result = new ApiResponse();
            try
            {
                wellCasingRepository.Clear(welHoleId);
                repo.Destroy(welHoleId);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponse> GetAllByWellId(string wellId)
        {
            var result = new ApiResponse();
            try
            {
                //result.Data = await repo.GetAllByWellId(wellId);
                result.Data = await repo.GetAllByWellID(wellId);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponse> getDataHoleCasing(string recordId)
        {
            var result = new ApiResponse();
            try
            {
                //result.Data = await repo.GetAllByWellId(wellId);
                result.Data = await repo.GetAllByWellId(recordId);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponsePage<WellHoleCasing>> DrillingLookUp(MarvelDataSourceRequest marvelDataSourceRequest, string wellID)
        {
            var result = new ApiResponsePage<WellHoleCasing>();

            try
            {
                var sb = new StringBuilder();
                sb.AppendLine($"where organization_id='{this._context.OrganizationId}' ");
                if (marvelDataSourceRequest.Filter != null)
                {
                    foreach (var filter in marvelDataSourceRequest.Filter.Filters)
                    {
                        if (filter.Value == "")
                        {
                            continue;
                        }

                        foreach (var f in filter.Filters)
                        {
                            if (f.Operator == "eq")
                            {
                                sb.Append($" and {f.Field} = '{f.Value}'");
                            }
                            else if (f.Operator == "contains" && f.Value != "")
                            {
                                sb.Append($" and {f.Field} like '%{f.Value}%'");
                            }
                        }
                    }
                }
                sb.AppendLine($" and well_id='{wellID}' ");
                sb.AppendLine($" and multicasing is null ");
                sb.AppendLine($" order by casing_setting_depth asc ");

                var resultRepo = await repo.GetAllDrilling(sb.ToString());

                result.CurrentPage = 1;
                result.TotalPages = 1;
                result.TotalItems = resultRepo.Count();
                result.ItemsPerPage = 100;
                result.Items = resultRepo.ToList();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public async Task<ApiResponsePage<WellHoleCasing>> MulticasingLookUp(MarvelDataSourceRequest marvelDataSourceRequest, string wellID)
        {
            var result = new ApiResponsePage<WellHoleCasing>();
            var num = 1;
            try
            {
                var sb = new StringBuilder();
                sb.AppendLine($"where organization_id='{this._context.OrganizationId}' ");
                if (marvelDataSourceRequest.Filter != null)
                {
                    foreach (var filter in marvelDataSourceRequest.Filter.Filters)
                    {
                        if (filter.Value == "")
                        {
                            continue;
                        }

                        foreach (var f in filter.Filters)
                        {
                            if (f.Operator == "eq")
                            {
                                sb.Append($" and {f.Field} = '{f.Value}'");
                            }
                            else if (f.Operator == "contains" && f.Value != "")
                            {
                                sb.Append($" and {f.Field} like '%{f.Value}%'");
                            }
                        }
                    }
                }
                sb.AppendLine($" and well_id='{wellID}' ");
                sb.AppendLine($" and multicasing='{num}' ");
                sb.AppendLine($" order by casing_setting_depth asc ");

                var resultRepo = await repo.GetAllDrilling(sb.ToString());

                result.CurrentPage = 1;
                result.TotalPages = 1;
                result.TotalItems = resultRepo.Count();
                result.ItemsPerPage = 100;
                result.Items = resultRepo.ToList();
                return result;
            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}
