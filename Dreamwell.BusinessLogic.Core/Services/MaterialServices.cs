﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using CommonTools.Helper;
using System.Text.RegularExpressions;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class MaterialServices : MaterialRepository
    {
        public MaterialServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(material record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public Page<vw_material> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_material> LookupByAfeLineId(MarvelDataSourceRequest marvelDataSourceRequest, string afeLindId)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 AND r.afe_line_id = @1 ", _services.dataContext.OrganizationId, afeLindId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_material> LookupByAfeLineAndType(MarvelDataSourceRequest marvelDataSourceRequest, string afeLindId, int itemType)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 AND r.afe_line_id = @1 AND r.item_type = @2 ",
                _services.dataContext.OrganizationId, afeLindId, itemType);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            sqlFilter.Append("ORDER BY r.material_type ASC, r.description ASC ");
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }


        public Node<vw_material> GetTree()
        {
            var userId = BusinessLogic.Core.SysConfig.UserSetup.sysAdminId;
            Node<vw_material> results = new Node<vw_material>(null);
            try
            {
                var afeLineParent = afe_line.Fetch(" WHERE is_active=1 and organization_id=@0 and parent_id is null ORDER BY line asc ", _services.dataContext.OrganizationId);

                foreach (var item in afeLineParent)
                {
                    var record = new vw_material();
                    record.id = item.id;
                    record.description = item.description;
                    record.icon_type = "fw-500 text-uppercase";
                    Node<vw_material> parent = results.Add(record);
                    GetTree(item.id, parent);
                }
                //appLogger.Debug(JsonConvert.SerializeObject(results));

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        private static string PadNumbers(string input)
        {
            return Regex.Replace(input, "[0-9]+", match => match.Value.PadLeft(10, '0'));
        }

        protected vw_material GetTree(String parentId, Node<vw_material> parent)
        {
            var results = new vw_material();
            var children = parent;
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var data = afe_line.Fetch(" WHERE parent_id=@0 ORDER BY line asc", parentId);
                if (data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        appLogger.Debug(item.description);
                        var record = new vw_material();
                        record.id = item.id;
                        record.description = item.description;
                        record.icon_type = "fw-500 text-uppercase";
                        GetTree(item.id, children.Add(record));
                    }
                }
                else
                {

                    var itemType = children;
                    GetMaterial(parentId, itemType.Add(new vw_material
                    {
                        id = string.Format("{0}_DYRHOLEBASED", parentId),
                        description = "DRY HOLE BASED",
                        icon_type = "fw-900 text-uppercase font-italic"
                    }), 1);
                    GetMaterial(parentId, itemType.Add(new vw_material
                    {
                        id = string.Format("{0}_COMPLETIONBASED", parentId),
                        description = "COMPLETION BASED",
                        icon_type = "fw-900 text-uppercase font-italic"
                    }), 2);

                    //GetMaterial(parentId, children);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        protected vw_material GetMaterial(String parentId, Node<vw_material> parent, int itemType)
        {
            var results = new vw_material();
            var children = parent;
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var qry = Sql.Builder.Append(material.DefaultView);
                qry.Append(" WHERE r.afe_line_id = @0 AND item_type=@1 ORDER BY r.description ASC, r.material_type ASC ", parentId, itemType);
                var materialRecord = vw_material.Fetch(qry);
                materialRecord = materialRecord.OrderBy(x => PadNumbers(x.description)).ToList();
                if (materialRecord.Count > 0)
                {
                    foreach (var item in materialRecord)
                    {
                        item.icon_type = "material";
                        children.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

    }
}
