﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using System.Collections.Generic;
using System.Transactions;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;
using Omu.ValueInjecter;
using System.Linq;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingDeviationServices : DrillingDeviationRepository
    {
        public DrillingDeviationServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(drilling_deviation record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_deviation>> getDrillingDeviationDate(string wellId, DateTime startdate, DateTime enddate)
        {
            var result = new ApiResponse<List<vw_drilling_deviation>>();
            try
            {
                var sql = Sql.Builder.Append(@"SELECT  r.*,d.drilling_date
                            FROM drilling_deviation r
							LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
                            WHERE d.well_id =  @0 AND (d.drilling_date BETWEEN @1 AND @2)  Order by d.drilling_date, r.measured_depth", wellId, startdate, enddate);
                result.Data = this._services.db.Fetch<vw_drilling_deviation>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse<List<drilling_deviation>> Save(List<drilling_deviation> records, string drillingId)
        {
            var result = new ApiResponse<List<drilling_deviation>>();
            var service_drilling = new DrillingServices(_services.dataContext);
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    var sql = Sql.Builder.Append("");
                    records = records.Where(o => o != null).ToList();
                    if (records?.Count == 0)
                    {
                        sql = Sql.Builder.Append(@"DELETE FROM drilling_deviation where drilling_id = @0", drillingId);
                        this._services.db.Execute(sql);
                        scope.Complete();
                        result.Status.Success = true;
                        return result;
                    }

                    var ddrRecord = drilling.FirstOrDefault("WHERE id=@0", drillingId);
                    var wellRecord = well.FirstOrDefault("WHERE id=@0", ddrRecord.well_id);

                    sql = Sql.Builder.Append(@"DELETE FROM drilling_deviation where drilling_id = @0", ddrRecord.id);
                    this._services.db.Execute(sql);

                    sql = Sql.Builder.Append(@"
                            SELECT TOP 1 r.*
                            FROM drilling_deviation AS r
                            INNER JOIN drilling d ON r.drilling_id = d.id
                            WHERE d.well_id = @0 AND d.drilling_date < @1 
                            ORDER BY r.created_on DESC",
                            ddrRecord.well_id, ddrRecord.drilling_date);
                    //ORDER BY d.drilling_date, r.seq DESC", 

                    var lastRecordDeviation = this._services.db.SingleOrDefault<drilling_deviation>(sql);
                    if (lastRecordDeviation == null)
                    {
                        lastRecordDeviation = new drilling_deviation();
                        lastRecordDeviation.azimuth = 0;
                        lastRecordDeviation.dls = 0;
                        lastRecordDeviation.e_w = 0;
                        lastRecordDeviation.inclination = 0;
                        lastRecordDeviation.measured_depth = 0;
                        lastRecordDeviation.n_s = 0;
                        lastRecordDeviation.phase = true;
                        lastRecordDeviation.seq = 0;
                        lastRecordDeviation.tvd = 0;
                        lastRecordDeviation.v_section = 0;
                        lastRecordDeviation.drilling_id = ddrRecord.id;
                    }

                    result.Status.Success = true;
                    for (var i = 0; i < records.Count; i++)
                    {
                        var newRecord = new drilling_deviation();
                        var upper = new MWD();
                        if (i == 0)
                        {
                            //upper.InjectFrom(lastRecordDeviation);
                            upper.SurveyDepth = (double)lastRecordDeviation.measured_depth;
                            upper.Azimuth = (double)lastRecordDeviation.azimuth;
                            upper.Inclination = (double)lastRecordDeviation.inclination;
                            upper.TVD = (double)lastRecordDeviation.tvd;
                            upper.EastWest = (double)lastRecordDeviation.e_w;
                            upper.NorthSouth = (double)lastRecordDeviation.n_s;
                        }
                        else
                        {
                            upper.SurveyDepth = (double)records[i - 1].measured_depth;
                            upper.Azimuth = (double)records[i - 1].azimuth;
                            upper.Inclination = (double)records[i - 1].inclination;
                            upper.TVD = (double)records[i - 1].tvd;
                            upper.EastWest = (double)records[i - 1].e_w;
                            upper.NorthSouth = (double)records[i - 1].n_s;
                        }

                        var lower = new MWD();
                        lower.SurveyDepth = (double)records[i].measured_depth;
                        lower.Azimuth = (double)records[i].azimuth;
                        lower.Inclination = (double)records[i].inclination;

                        var service_minimumCurvature = new MinimumCurvature();

                        MinimumCurvatureResult resultMinimumCurvature = new MinimumCurvatureResult();
                        if (lower.SurveyDepth > 0)
                            resultMinimumCurvature = service_minimumCurvature.Calculate(upper, lower, wellRecord.target_direction ?? 0);
                        records[i].tvd = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.TVD : 0;
                        records[i].v_section = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.VerticalSection : 0;
                        records[i].n_s = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.North : 0;
                        records[i].e_w = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.East : 0;
                        records[i].dls = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.DLS : 0;

                        newRecord.InjectFrom(records[i]);
                        newRecord.seq = lastRecordDeviation == null ? i : (i + 1);
                        newRecord.phase = true;
                        newRecord.drilling_id = ddrRecord.id;

                        var isNewDeviation = false;
                        result.Status.Success &= SaveEntity(newRecord, ref isNewDeviation, (r) => records[i].id = r.id);

                    }

                    //-- Update Current Depth MD on Drilling
                    var isNewDrilling = false;
                    var getLastOperationActivityRecord = records.LastOrDefault();
                    var drillingRecord = new drilling();
                    drillingRecord.id = drillingId;
                    drillingRecord.current_depth_tvd = getLastOperationActivityRecord.tvd;
                    result.Status.Success &= service_drilling.SaveEntity(drillingRecord, ref isNewDrilling, (r) => drillingRecord.id = r.id);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = records;
                        result.Status.Message = "The data has been saved.";
                    }
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    appLogger.Error(ex);
                }
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_deviation>> getByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_deviation>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                            SELECT r.*, d.drilling_date
                            FROM drilling_deviation r
                            LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
                            WHERE d.well_id = @0
                            ORDER BY d.drilling_date, r.seq ASC", wellId);
                result.Data = this._services.db.Fetch<vw_drilling_deviation>(sql);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
