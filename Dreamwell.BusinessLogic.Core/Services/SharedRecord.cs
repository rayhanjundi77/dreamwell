﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class SharedRecordServices : SharedRecordRepository
    {
        public SharedRecordServices(DataContext dataContext) : base(dataContext) { }


        public bool ShareTo<T>(string recordId, string sharedTo, bool canWrite = false) where T : IEntity
        {
            var result = false;
            var appEntity = Activator.CreateInstance<T>();
            var record = new shared_record();
            record.application_entity_id = ((IEntity)appEntity).GetEntityId();
            record.record_id = recordId;
            record.shared_to = sharedTo;
            record.can_read = true;
            record.can_write = canWrite;

            //Check Exist
            if (shared_record.FirstOrDefault("WHERE application_entity_id=@0 AND record_id=@1 AND shared_to=@2 ", ((IEntity)appEntity).GetEntityId()
                , recordId, sharedTo) != null)
            {
                return true;
            }
            result = this._services.dataContext.SaveEntity<shared_record>(record, true);
            return result;
        }

    }
}
