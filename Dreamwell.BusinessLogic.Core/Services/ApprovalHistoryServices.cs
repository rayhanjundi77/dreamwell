﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ApprovalHistoryServices : ApprovalHistoryRepository
    {
        public ApprovalHistoryServices(DataContext dataContext) : base(dataContext) { }
        
        /*
        public int GetList()
        {
            var qry = PetaPoco.Sql.Builder.Append(" WHERE is_active= 1 AND record_owning_team= @0 and approval_status =  200 and approval_level=2",  this._services.dataContext.PrimaryTeam);
            List<vw_drilling> list = vw_drilling.Fetch(qry);
            return list.Count();
        }
        */

        public int GetList()
        {
            var qry = PetaPoco.Sql.Builder.Append(" WHERE is_active= 1 AND record_owning_team= @0 and approval_status =  200 and approval_level=2", this._services.dataContext.PrimaryTeam);
            if (this._services.dataContext.IsSystemAdministrator)
            {
                qry = PetaPoco.Sql.Builder.Append("WHERE is_active = 1 AND approval_status = 200 AND approval_level = 2");
            }
            List<vw_drilling> list = vw_drilling.Fetch(qry);
            return list.Count();
        }
    }
}
