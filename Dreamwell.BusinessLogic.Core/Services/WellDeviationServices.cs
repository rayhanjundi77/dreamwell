﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;
using System.Transactions;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellDeviationServices : WellDeviationRepository
    {
        public WellDeviationServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(WellViewModel record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                var recordId = record.well.id;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    var service_minimumCurvature = new MinimumCurvature();

                    if (record.well_deviation != null && record.well_deviation.Count > 0)
                    {
                        var isNewDeviation = false;
                        var sql = Sql.Builder.Append(@"DELETE FROM well_deviation where well_id = @0", record.well.id);
                        this._services.db.Execute(sql);

                        well_deviation firstRecord = new well_deviation();
                        firstRecord.azimuth = 0;
                        firstRecord.dls = 0;
                        firstRecord.e_w = 0;
                        firstRecord.inclination = 0;
                        firstRecord.measured_depth = 0;
                        firstRecord.n_s = 0;
                        firstRecord.phase = true;
                        firstRecord.seq = 0;
                        firstRecord.tvd = 0;
                        firstRecord.v_section = 0;
                        firstRecord.well_id = recordId;
                        result.Status.Success &= SaveEntity(firstRecord, ref isNewDeviation, (r) => firstRecord.id = r.id);

                        if (result.Status.Success)
                        {
                            for (var i = 0; i < record.well_deviation.Count; i++)
                            {
                                var newRecord = new well_deviation();
                                isNewDeviation = false;
                                var upper = new MWD();
                                if (i == 0)
                                {
                                    upper.InjectFrom(firstRecord);
                                }
                                else
                                {
                                    upper.SurveyDepth = (double)record.well_deviation[i - 1].measured_depth;
                                    upper.Azimuth = (double)record.well_deviation[i - 1].azimuth;
                                    upper.Inclination = (double)record.well_deviation[i - 1].inclination;
                                    upper.TVD = (double)record.well_deviation[i - 1].tvd;
                                    upper.EastWest = (double)record.well_deviation[i - 1].e_w;
                                    upper.NorthSouth = (double)record.well_deviation[i - 1].n_s;
                                }

                                var lower = new MWD();
                                lower.SurveyDepth = (double)record.well_deviation[i].measured_depth;
                                lower.Azimuth = (double)record.well_deviation[i].azimuth;
                                lower.Inclination = (double)record.well_deviation[i].inclination;

                                if (result.Status.Success)
                                {
                                    MinimumCurvatureResult resultMinimumCurvature = new MinimumCurvatureResult();
                                    if (lower.SurveyDepth > 0)
                                        resultMinimumCurvature = service_minimumCurvature.Calculate(upper, lower, record.well.target_direction);
                                    record.well_deviation[i].tvd = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.TVD : 0;
                                    record.well_deviation[i].v_section = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.VerticalSection : 0;
                                    record.well_deviation[i].n_s = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.North : 0;
                                    record.well_deviation[i].e_w = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.East : 0;
                                    record.well_deviation[i].dls = lower.SurveyDepth > 0 ? (decimal?)resultMinimumCurvature.DLS : 0;

                                    newRecord.InjectFrom(record.well_deviation[i]);
                                    newRecord.seq = i + 1;
                                    newRecord.phase = true;
                                    newRecord.well_id = recordId;
                                    result.Status.Success &= SaveEntity(newRecord, ref isNewDeviation, (r) => record.well_deviation[i].id = r.id);
                                }
                            }
                        }
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "The Program Directional Survey has been updated.";
                        
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }
    }
}
