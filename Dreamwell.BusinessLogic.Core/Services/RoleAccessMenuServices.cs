﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using DataTables.Mvc;
using Omu.ValueInjecter;
using PetaPoco;
using Dreamwell.DataAccess;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class RoleAccessMenuServices : RoleAccessMenuRepository
    {
        public RoleAccessMenuServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(role_access_menu record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";

                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";

                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse Delete(role_access_menu record)
        {
            var result = new ApiResponse();
            result.Status.Success = false;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    this._services.db.Execute("DELETE dbo.role_access_menu WHERE application_role_id=@0 AND application_menu_id=@1 ", record.application_role_id, record.application_menu_id);
                    result.Status.Success = true;
                    if (result.Status.Success)
                        scope.Complete();

                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    appLogger.Error(ex);
                }
            }

            return result;
        }


        public bool Duplicate(string copyFrom, string copyTo)
        {
            bool success = false;
            bool isNew = false;


            using (var db = new dreamwellRepo())
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    try
                    {
                        var fromRecord = db.Fetch<role_access_menu>(" WHERE application_role_id=@0 ", copyFrom); //GetAll(" AND application_role_id = @0  ", copyFrom);
                        if (fromRecord.Count > 0)
                        {
                            success = true;
                        }
                        else
                        {
                            throw new Exception("Record doesnt have role");
                        }

                        foreach (var r in fromRecord)
                        {
                            var roleAccessMenu = new role_access_menu();
                            roleAccessMenu.InjectFrom(r);
                            roleAccessMenu.id = Guid.NewGuid().ToString();
                            roleAccessMenu.application_role_id = copyTo;
                            success &= SaveEntity(roleAccessMenu, ref isNew);
                        }

                        if (success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                        throw;
                    }
                }
            }

            return success;
        }

    }
}
