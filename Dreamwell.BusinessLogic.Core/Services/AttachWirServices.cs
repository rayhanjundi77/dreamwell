﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using System.Collections.Generic;
using System.Transactions;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;
using Omu.ValueInjecter;
using System.Linq;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class AttachWirServices : AttachWirRepository
    {
        private readonly DataContext _dataContext;

        public AttachWirServices(DataContext dataContext) : base(dataContext)
        {

            _dataContext = dataContext;
        }

        public ApiResponse Save(AttachWirPayload payload)
        {
            var result = new ApiResponse();
            try
            {
                if (payload == null || payload.attachments == null || !payload.attachments.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No attachments to save.";
                    return result;
                }

                using (var scope = new TransactionScope())
                {
                    foreach (var record in payload.attachments)
                    {
                        var isNew = false;
                        var recordId = record.id ?? Guid.NewGuid().ToString(); // Generate ID jika null

                        record.well_id = payload.well_id; // Tambahkan well_id
                        record.comment_wir = payload.wli_comments; // Tambahkan WLI comments
                        record.created_on = DateTime.UtcNow;

                        result.Status.Success = SaveEntity(record, ref isNew, this._services.dataContext.PrimaryTeamId, (r) => recordId = r.id);

                        if (!result.Status.Success)
                        {
                            result.Status.Message = "Failed to save attachment.";
                            return result;
                        }
                    }

                    scope.Complete();
                }

                result.Status.Success = true;
                result.Status.Message = "Attachments saved successfully.";
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_attach_wir>> GetByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_attach_wir>>();
            try
            {
                if (string.IsNullOrWhiteSpace(wellId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Well ID cannot be empty.";
                    return result;
                }

                var sql = Sql.Builder.Append(@"
                    SELECT *
                    FROM vw_attach_wir
                    WHERE well_id = @0
                    ORDER BY created_on DESC", wellId);

                result.Data = this._services.db.Fetch<vw_attach_wir>(sql);
                appLogger.Info($"Retrieved {result.Data.Count} records.");
                result.Status.Success = true;
                result.Status.Message = "Data retrieved successfully.";
            }
            catch (Exception ex)
            {
                appLogger.Error($"Error in GetByWellId: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"Error retrieving data: {ex.Message}";
            }
            return result;
        }

        public bool RemoveAll(string[] ids)
        {
            using (var scope = new TransactionScope())
            {
                try
                {
                    if (ids == null || ids.Length == 0)
                    {
                        appLogger.Error("No IDs provided for deletion.");
                        return false;
                    }

                    // Gabungkan semua ID menjadi parameter IN di SQL
                    var sql = Sql.Builder.Append("DELETE FROM attach_wir WHERE id IN (@0)", ids);
                    int rowsAffected = this._services.db.Execute(sql);

                    if (rowsAffected == 0)
                    {
                        appLogger.Warn("No rows were deleted. IDs may not exist.");
                        return false;
                    }

                    appLogger.Info($"Successfully deleted {rowsAffected} record(s).");
                    scope.Complete(); // Commit transaksi
                    return true;
                }
                catch (Exception ex)
                {
                    appLogger.Error($"Error in RemoveAll: {ex.Message}");
                    return false;
                }
            }
        }
    }

}
