﻿using Dreamwell.BusinessLogic.Core.Common;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.Session;
using NLog;
using PetaPoco;
using System;
using System.Linq;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class Authentication
    {
        Logger appLogger = LogManager.GetCurrentClassLogger();

        public void Authenticate(UserCredential credential, out string Token, Action<bool, string> ActionResult = null)
        {
            bool result = false;
            var log = string.Empty;
            Token = string.Empty;
            using (var db = new dreamwellRepo())
            {
                try
                {
                    Sql sql = Sql.Builder.Append(application_user.DefaultView);
                    sql.Append(" WHERE r.app_username = @0 or r.email = @0 ", credential.Username); /*Email or username*/
                    var user = db.FirstOrDefault<vw_application_user>(sql);
                    if (user == null) throw new Exception("User not registered on database");

                    var orgData = db.FirstOrDefault<organization>("WHERE id=@0", user.organization_id);
                    if (orgData == null) throw new Exception("Organization not registered on database");

                    if ((user.is_ldap ?? false) == true)
                    {
                        result = ActiveDirectory.Authentication(credential.Username, credential.Password, orgData.domain_name);
                        if (!result)
                        {
                            log = "Please check username or password on active directory !";
                        }
                    }
                    else
                    {
                        var passwordHash = db.FirstOrDefault<application_user>("WHERE id=@0", user.id).app_password;
                        result = CommonTools.PasswordHash.ValidatePassword(credential.Password, passwordHash);
                        if (!result)
                        {
                            log = "Wrong Password !";
                        }
                    }

                    if (result)
                    {
                        sql = Sql.Builder.Append(team_member.DefaultView);
                        sql.Append("WHERE r.application_user_id=@0 ", user.id);

                        var teams = vw_team_member.Fetch(sql);

                        sql = Sql.Builder.Append(user_role.DefaultView);
                        sql.Append("WHERE r.application_user_id=@0 ", user.id);

                        var roles = vw_user_role.Fetch(sql);
                        var BUnit = business_unit.FirstOrDefault("WHERE id=@0", user.business_unit_id);
                        if (BUnit == null)
                            throw new Exception("User doesn't have business unit, Please set business unit and try again");

                        Token = JwtWrapper.GenerateToken(credential.Username, new Dreamwell.Infrastructure.Session.UserSession()
                        {
                            AppUserId = user.id,
                            AppUsername = user.app_username,
                            Firstname = user.first_name,
                            Lastname = user.last_name,
                            OrganizationId = user.organization_id,
                            OrganizationName = user.organization_name,
                            OrganizationDomain = orgData.domain_name,
                            PrimaryBusinessUnitId = user.business_unit_id,
                            PrimaryBusinessUnit = user.unit_name,
                            PrimaryBusinessUnitCode = BUnit.unit_code,
                            PrimaryTeamId = user.primary_team_id,
                            PrimaryTeam = user.team_name,
                            Gender = user.gender,
                            Email = user.email,
                            SysAdmin = user.is_sysadmin ?? false,
                            Teams = (from a in teams
                                     select new object_description
                                     {
                                         id = a.team_id,
                                         description = a.team_name
                                     }).ToList(),
                            Roles = (from a in roles
                                     select new object_description
                                     {
                                         id = a.application_role_id,
                                         description = a.role_name
                                     }).ToList()
                        });
                        UserSession userSession = new UserSession();
                        userSession.AppUserId = user.id;
                    }

                    ActionResult?.Invoke(result, log);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    log = ex.Message;
                    ActionResult?.Invoke(false, log);
                }
            }
        }
    }
}
