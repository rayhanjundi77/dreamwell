﻿using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using CommonTools.JWT;
using PetaPoco;
using Hangfire;
using System.Diagnostics;
using Dreamwell.Infrastructure.Session;
using CommonTools;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ExcelTemplateServices: ExcelTemplateRepository
    {
        public ExcelTemplateServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(excel_template record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = this.SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }


       

    }
}
