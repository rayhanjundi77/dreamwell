﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellDrillingParameterServices : WellDrillingParameterRepository
    {
        public WellDrillingParameterServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(well_drilling_parameter record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                //var wellId = record.well_id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_well_drilling_parameter>> GetByIdDP(string wellId)
        {
            var result = new ApiResponse<List<vw_well_drilling_parameter>>();
            try
            {
                // Fetch the data using the provided wellId
                result.Data = this.GetViewAll(" AND well_id=@0 ", wellId);

                // Log the fetched data
                appLogger.Info($"Fetched Data: {JsonConvert.SerializeObject(result.Data)}");

                // Mark the operation as successful
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                // Log the error and set the status to failure
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }


        public ApiResponse<List<vw_well_drilling_parameter>> GetDrillingParamByWell(string wellId)
        {
            var result = new ApiResponse<List<vw_well_drilling_parameter>>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder.Append(well_drilling_parameter.DefaultView);
                    sql.Append(" WHERE r.well_id=@0", wellId);

                    // Log the SQL query for debugging
                    appLogger.Info($"SQL Query: {sql.SQL}");

                    var fetchedData = db.Fetch<vw_well_drilling_parameter>(sql);

                    // Log the fetched data
                    appLogger.Info($"Fetched Data: {JsonConvert.SerializeObject(fetchedData)}");

                    result.Data = fetchedData;
                }
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }


        public Page<vw_well_drilling_parameter> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 ", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            sqlFilter.Append("ORDER BY r.created_on DESC");
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
