﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellBhaServices : WellBhaRepository
    {
        public WellBhaServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(WellBhaViewModel record)
        {
            var service_wellBhaComponent = new WellBhaComponentServices(_services.dataContext);
            var result = new ApiResponse();
            result.Status.Success = true;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;

                    if (record.well_bha != null)
                    {
                        var lastRecord = this.GetFirstOrDefault(" AND well_id = @0 ORDER BY bha_no DESC ", record.well_bha.well_id);
                        if (record.well_bha.bha_no == null)
                        {
                            record.well_bha.bha_no = lastRecord == null ? 1 : lastRecord.bha_no + 1;
                        }
                        result.Status.Success &= this.SaveEntity(record.well_bha, ref isNew, (r) => record.well_bha.id = r.id);

                        if (record.well_bha_component.Count > 0)
                        {
                            foreach (var item in record.well_bha_component)
                            {
                                var isNewBhaComp = false;
                                item.well_bha_id = record.well_bha.id;
                                item.ref_drilling_id = record.drilling_id;
                                result.Status.Success &= service_wellBhaComponent.SaveEntity(item, ref isNewBhaComp, (r) => item.id = r.id);
                            }
                        }
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                            result.Status.Message = "The data has been added.";
                        else
                            result.Status.Message = "The data has been updated.";
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse Delete(string id)
        {
            var service_wellBhaComponent = new WellBhaComponentServices(_services.dataContext);
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                var isNewBhaComp = false;
                var component = service_wellBhaComponent.GetFirstOrDefault(" and id = @0", id);
                component.is_active = false;
                result.Status.Success &= service_wellBhaComponent.SaveEntity(component, ref isNewBhaComp, (r) => component.id = r.id);

                if (result.Status.Success)
                {
                    result.Status.Message = "The data has been deleted.";
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        //public Page<vw_well_bha> Lookup(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        //{

        //    #region Use Filters
        //    var sqlFilter = Sql.Builder.Append(" AND r.well_id=@0 ", wellId);
        //    if (marvelDataSourceRequest.Filter != null)
        //    {
        //        Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
        //        if (filterBuilder != null)
        //            sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
        //    }
        //    sqlFilter.Append(" ORDER BY r.bha_no ASC ");
        //    #endregion
        //    return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        //}

        public ApiResponsePage<vw_well_bha> Lookup(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            var result = new ApiResponsePage<vw_well_bha>();
            result.Items = this._services.db.Fetch<vw_well_bha>(@"select * from vw_well_bha where is_active=1 and well_id=@0 ", wellId);
            return result;
        }
        //public ApiResponsePage<vw_well_bha> LookupByHoleAndCasing(MarvelDataSourceRequest marvelDataSourceRequest, string wellId, string holeAndCasingId)
        //{
        //    var result = new ApiResponsePage<vw_well_bha>();
        //    result.Items = this._services.db.Fetch<vw_well_bha>(@"select * from vw_well_bha where is_active=1 and well_id=@0 ", wellId, holeAndCasingId);
        //    return result;
        //}
        public ApiResponsePage<vw_well_bha> LookupByHoleAndCasing(MarvelDataSourceRequest marvelDataSourceRequest, string wellId, string holeAndCasingId)
        {
            var result = new ApiResponsePage<vw_well_bha>();
            try
            {
                if (_services == null || _services.db == null)
                {
                    throw new InvalidOperationException("Database service is not available.");
                }

                var sqlFilter = Sql.Builder
                    .Append("SELECT * FROM vw_well_bha")
                    .Append("WHERE is_active = 1")
                    .Append("AND well_id = @0", wellId)
                    .Append("AND well_hole_and_casing_id = @0", holeAndCasingId);

                if (marvelDataSourceRequest?.Filter != null)
                {
                    var filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                    if (filterBuilder != null)
                    {
                        sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
                    }
                }

                sqlFilter.Append("ORDER BY bha_no ASC");

                var page = marvelDataSourceRequest.Page > 0 ? marvelDataSourceRequest.Page : 1;
                var pageSize = marvelDataSourceRequest.PageSize > 0 ? marvelDataSourceRequest.PageSize : 10;

                var pagedResult = _services.db.Page<vw_well_bha>(page, pageSize, sqlFilter);

                result.Items = pagedResult.Items;
                result.CurrentPage = pagedResult.CurrentPage;
                result.TotalPages = pagedResult.TotalPages;
                result.TotalItems = pagedResult.TotalItems;
                result.ItemsPerPage = pagedResult.ItemsPerPage;
                //result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }

            return result;
        }

        /*
        public ApiResponse<drilling_bha> Save(DrillingBhaViewModel record)
        {
            var result = new ApiResponse<drilling_bha>();
            var service_drillingBhaComponent = new DrillingBhaComponentServices(_services.dataContext);
            var service_drillingBhaComponentDetail = new DrillingBhaComponentDetailServices(_services.dataContext);
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var recordId = record.drilling_bha.id;

                    var sqlDrillingBha = Sql.Builder.Append(@"SELECT * FROM drilling_bha WHERE well_id=@0 AND is_active=@1 ORDER BY bha_no DESC ", record.drilling_bha.well_id, true);
                    var recordDrillingBha = this._services.db.SingleOrDefault<drilling_bha>(sqlDrillingBha);

                    if (record.is_new)
                    {
                        var nonactive = Sql.Builder.Append("UPDATE dbo.drilling_bha SET is_active = 0 WHERE well_id = @0 ", record.drilling_bha.well_id);
                        _services.db.Execute(nonactive);
                    }
                    else
                    {
                        var sqlNonactiveDrillingBhaComp = Sql.Builder.Append("UPDATE dbo.drilling_bha_comp SET is_active = 0 WHERE drilling_bha_id = @0 ", recordId);
                        _services.db.Execute(sqlNonactiveDrillingBhaComp);
                    }

                    var drilling_bha = new drilling_bha();
                    drilling_bha.InjectFrom(record.drilling_bha);
                    if (recordDrillingBha == null)
                    {
                        drilling_bha.bha_no = 1;
                        drilling_bha.bha_run = 1;
                    }
                    else
                    {
                        if (record.is_new)
                        {
                            drilling_bha.bha_no = recordDrillingBha.bha_no + 1;
                            drilling_bha.bha_run = 1;
                        }
                        else if (!record.is_new)
                        {
                            drilling_bha.bha_no = recordDrillingBha.bha_no;
                            drilling_bha.bha_run = recordDrillingBha.bha_run + 1;
                        }
                    }


                    result.Status.Success = SaveEntity(drilling_bha, ref isNew, (r) => drilling_bha.id = r.id);

                    if (record.drilling_bha_comp == null || record.drilling_bha_comp.Count <= 0)
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Please input BHA Components to create a new BHA";
                        return result;
                    }
                    if (record.drilling_bha_comp.Count > 0)
                    {
                        foreach(var item in record.drilling_bha_comp)
                        {
                            if (!record.is_new)
                            {
                                var sqlNonactiveDrillingBhaCompDetail = Sql.Builder.Append("UPDATE dbo.drilling_bha_comp_detail SET is_active = 0 WHERE drilling_bha_comp_id = @0 ", item.drilling_bha_comp_id);
                                _services.db.Execute(sqlNonactiveDrillingBhaCompDetail);
                            }
                            var isNewComponent = false;
                            var drilling_bha_comp = new drilling_bha_comp();
                            drilling_bha_comp.id = null;
                            drilling_bha_comp.bha_component_id = item.bha_component_id;
                            drilling_bha_comp.drilling_bha_id = drilling_bha.id;
                            result.Status.Success &= service_drillingBhaComponent.SaveEntity(drilling_bha_comp, ref isNewComponent, (r) => drilling_bha_comp.id = r.id);

                            var isNewComponentDetail = false;
                            var drilling_bha_comp_detail = new drilling_bha_comp_detail();
                            drilling_bha_comp_detail.InjectFrom(item);
                            drilling_bha_comp_detail.id = null;
                            drilling_bha_comp_detail.drilling_id = record.drilling_id;
                            drilling_bha_comp_detail.drilling_bha_comp_id = drilling_bha_comp.id;
                            result.Status.Success &= service_drillingBhaComponentDetail.SaveEntity(drilling_bha_comp_detail, ref isNewComponentDetail, (r) => drilling_bha_comp_detail.id = r.id);
                        }
                    } 

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                            result.Status.Message = "The data has been added.";
                        else
                            result.Status.Message = "The data has been updated.";
                        result.Data = drilling_bha;
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }


    */
    }
}
