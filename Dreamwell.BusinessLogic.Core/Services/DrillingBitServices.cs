﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingBitServices : DrillingBitRepository
    {
        public DrillingBitServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse<drilling_bit> Save(DrillingBitViewModel record)
        {
            var result = new ApiResponse<drilling_bit>();
            var service_drillingBit = new DrillingBitServices(_services.dataContext);
            var service_drillingBhaBit = new DrillingBhaBitServices(_services.dataContext);
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result.Status.Success = true;
                    var isNew = false;
                    var recordId = record.drilling_bit.id;

                    var sqlDrillingBit = Sql.Builder.Append(@"SELECT r.* FROM drilling_bit r 
                                                              INNER JOIN drilling_bha d ON r.drilling_bha_id = d.id
                                                              WHERE well_id=@0 AND d.id=@1 AND r.is_active=@2 ORDER BY r.bit_number DESC ", record.well_id, record.drilling_bha_id, true);
                    var recordDrillingBha = this._services.db.SingleOrDefault<drilling_bit>(sqlDrillingBit);

                    if (record.is_new)
                    {
                        var nonactive = Sql.Builder.Append("UPDATE dbo.drilling_bit SET is_active = 0 WHERE drilling_bha_id = @0 ", record.drilling_bha_id);
                        _services.db.Execute(nonactive);
                    }
                    else
                    {
                        var sqlNonactiveDrillingBhaComp = Sql.Builder.Append("UPDATE dbo.drilling_bit SET bit_run = @0 WHERE id = @1 ", (int)recordDrillingBha.bit_run + 1, record.drilling_bit.id);
                        _services.db.Execute(sqlNonactiveDrillingBhaComp);
                    }

                    var drilling_bit = new drilling_bit();
                    drilling_bit.InjectFrom(record.drilling_bit);
                    drilling_bit.drilling_id = record.drilling_id;
                    drilling_bit.drilling_bha_id = record.drilling_bha_id;
                    if (record.is_new)
                    {
                        if (recordDrillingBha == null)
                            drilling_bit.bit_number = 1;
                        else
                            drilling_bit.bit_number = recordDrillingBha.bit_number + 1;

                        drilling_bit.bit_run = 1;
                    }
                    else
                    {
                        drilling_bit.bit_run = recordDrillingBha.bit_run + 1;
                        drilling_bit.bit_number = recordDrillingBha.bit_number;
                    }
                    result.Status.Success &= SaveEntity(drilling_bit, ref isNew, (r) => drilling_bit.id = r.id);

                    if (record.is_new)
                    {
                        var isNewDrillingBhaBit = false;
                        var drilling_bha_bit = new drilling_bha_bit();
                        drilling_bha_bit.drilling_bha_id = record.drilling_bha_id;
                        drilling_bha_bit.drilling_bit_id = drilling_bit.id;
                        drilling_bha_bit.well_id = record.well_id;
                        result.Status.Success &= service_drillingBhaBit.SaveEntity(drilling_bha_bit, ref isNewDrillingBhaBit, (r) => drilling_bha_bit.id = r.id);
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                            result.Status.Message = "The data has been added.";
                        else
                            result.Status.Message = "The data has been updated.";
                        result.Data = drilling_bit;
                    }

                    //if (recordDrillingBha == null)
                    //{
                    //    drilling_bha.bha_no = 1;
                    //    drilling_bha.bha_run = 1;
                    //}
                    //else
                    //{
                    //    if (record.is_new)
                    //    {
                    //        drilling_bha.bha_no = recordDrillingBha.bha_no + 1;
                    //        drilling_bha.bha_run = 1;
                    //    }
                    //    else if (!record.is_new)
                    //    {
                    //        drilling_bha.bha_no = recordDrillingBha.bha_no;
                    //        drilling_bha.bha_run = recordDrillingBha.bha_run + 1;
                    //    }
                    //}


                    //if (record.drilling_bha_comp.Count > 0)
                    //{
                    //    foreach(var item in record.drilling_bha_comp)
                    //    {
                    //        if (!record.is_new)
                    //        {
                    //            var sqlNonactiveDrillingBhaCompDetail = Sql.Builder.Append("UPDATE dbo.drilling_bha_comp_detail SET is_active = 0 WHERE drilling_bha_comp_id = @0 ", item.drilling_bha_comp_id);
                    //            _services.db.Execute(sqlNonactiveDrillingBhaCompDetail);
                    //        }
                    //        var isNewComponent = false;
                    //        var drilling_bha_comp = new drilling_bha_comp();
                    //        drilling_bha_comp.id = null;
                    //        drilling_bha_comp.bha_component_id = item.bha_component_id;
                    //        drilling_bha_comp.drilling_bha_id = drilling_bha.id;
                    //        result.Status.Success &= service_drillingBhaComponent.SaveEntity(drilling_bha_comp, ref isNewComponent, (r) => drilling_bha_comp.id = r.id);

                    //        var isNewComponentDetail = false;
                    //        var drilling_bha_comp_detail = new drilling_bha_comp_detail();
                    //        drilling_bha_comp_detail.InjectFrom(item);
                    //        drilling_bha_comp_detail.id = null;
                    //        drilling_bha_comp_detail.drilling_id = record.drilling_id;
                    //        drilling_bha_comp_detail.drilling_bha_comp_id = drilling_bha_comp.id;
                    //        result.Status.Success &= service_drillingBhaComponentDetail.SaveEntity(drilling_bha_comp_detail, ref isNewComponentDetail, (r) => drilling_bha_comp_detail.id = r.id);
                    //    }
                    //} 


                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_bit>> getByDrilling(string drillingId)
        {
            var result = new ApiResponse<List<vw_drilling_bit>>();
            try
            {
                var sql = Sql.Builder.Append(@"SELECT * FROM vw_drilling_bit WHERE drilling_id = @0", drillingId);
                result.Data = this._services.db.Fetch<vw_drilling_bit>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
