﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellHoleAndCasingServices : WellHoleAndCasingRepository
    {
        public WellHoleAndCasingServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(well_hole_and_casing record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "Successfully create hole and casing";
                    else
                        result.Status.Message = "Successfully update hole and casing";
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse SaveCasing(well_hole_and_casing record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = true;
                record.parent_well_hole_casing_id = record.id;
                var idNew = Guid.NewGuid().ToString();
                record.id = idNew;

                var recordId = record.id;
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    result.Status.Message = "Successfully created a new Casing";
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse UpdateCasing(well_hole_and_casing record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    result.Status.Message = "Successfully Update Casing";
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }


        public Page<vw_well_hole_and_casing> Lookup(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.well_id = @0 AND r.organization_id=@1 AND r.is_active=1 ", wellId, _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            sqlFilter.Append(" ORDER BY r.created_on ASC ");
            appLogger.Error(sqlFilter.Arguments);
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_well_hole_and_casing> LookupByWell(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.well_id = @0 AND r.organization_id=@1 AND r.is_active=1 ", wellId, _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            sqlFilter.Append(" ORDER BY r.hole_name, casing_name ASC ");
            appLogger.Error(sqlFilter.Arguments);
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_well_hole_and_casing> MultiLookup(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.well_id = @0 AND r.organization_id=@1 AND r.is_active=1 AND r.multicasing=1", wellId, _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            sqlFilter.Append(" ORDER BY r.created_on ASC ");
            appLogger.Error(sqlFilter.Arguments);
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_well_hole_and_casing> LookupHoleCasingByDrilling(MarvelDataSourceRequest marvelDataSourceRequest, string drillingId)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.id IN (SELECT DISTINCT whc.id FROM drilling_operation_activity doa INNER JOIN drilling_hole_and_casing dhc ON dhc.id = doa.drilling_hole_and_casing_id INNER JOIN well_hole_and_casing whc ON whc.id = dhc.well_hole_and_casing_id WHERE doa.drilling_id = @0) AND r.is_active=1 ", drillingId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            sqlFilter.Append(" ORDER BY r.created_on ASC ");
            appLogger.Error(sqlFilter.Arguments);
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
