﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess.Models.Entity;
using System.Data.SqlClient;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class RkapServ : RkapRepo
    {
        public RkapServ(DataContext dataContext) : base(dataContext) { }


        public Page<vw_rkap> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var sqlFilter = Sql.Builder.Append(" AND status = @0", 1);
            sqlFilter.Append(" ORDER BY well_name ASC ");
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }


    }



}
