﻿using Dreamwell.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class CustomDataContext : DataContext
    {
        public CustomDataContext(string applicationUserId) : base(applicationUserId)
        {
        }
            
        public new bool IsSystemAdministrator => true; // Selalu mengembalikan true
    }
}
