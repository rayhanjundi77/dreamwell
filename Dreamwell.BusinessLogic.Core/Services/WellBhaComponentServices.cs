﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellBhaComponentServices : WellBhaComponentRepository
    {
        public WellBhaComponentServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(well_bha_component record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var isNewRecord = false;
                var recordId = record.id;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                {
                    if (string.IsNullOrEmpty(record.id) ||
                        string.IsNullOrWhiteSpace(record.id))
                    {
                        //Set oldrecord to in-active on the same bha component in well_bha
                        var oldRecord = well_bha_component.FirstOrDefault("WHERE id=@0", record.id);
                        oldRecord.is_active = false;
                        result.Status.Success = _services.dataContext.SaveEntity<well_bha_component>(oldRecord, false);

                        //Set record.id to null, and renew GUID on save Entity
                        record.id = null;
                        isNew = false;
                    }
                    else
                    {
                        result.Status.Success = true;
                        isNew = true;
                    }

                    result.Status.Success &= SaveEntity(record, ref isNewRecord, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                        {
                            result.Status.Message = "The data has been added.";
                        }
                        else
                        {
                            result.Status.Message = "The data has been updated.";
                        }
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_well_bha_component>> GetByWellBha(string wellBhaId)
        {
            var result = new ApiResponse<List<vw_well_bha_component>>();
            try
            {
                //var qry = Sql.Builder.Append(well_bha_component.DefaultView);
                //qry.Append("WHERE r.well_bha_id=@0 AND r.is_active=1 ", wellBhaId);
                //qry.Append("ORDER BY r.created_on ASC");

                //result.Data = GetViewAll("r.well_bha_id=@0 AND r.is_active=1 ORDER BY r.created_on ASC", wellBhaId);
                result.Data = this._services.db.Fetch<vw_well_bha_component>("select * from vw_well_bha_component where well_bha_id=@0 AND is_active=1 ORDER BY created_on ASC", wellBhaId);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return result;
        }

    }
}
