﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using System.Collections.Generic;
using Dreamwell.DataAccess.ViewModels;
using System.Transactions;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingBulkServices : DrillingBulkRepository
    {
        public DrillingBulkServices(DataContext dataContext) : base(dataContext) { }

        /*Do not use this function on datatables*/
        public Page<vw_drilling_bulk> GetViewPerPage(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }


        public ApiResponse<List<vw_drilling_bulk>> getByBulkId(string drillingId)
        {
            var result = new ApiResponse<List<vw_drilling_bulk>>();
            try
            {
                var sqlQuery = Sql.Builder.Append(@"
                                select 
                                      r.id, 
                                      r.owner_id, 
                                      r.drilling_id, 
                                      i.id AS item_id, 
                                      i.item_name AS item_name, 
                                      i.item_name AS item_name, 
                                      COALESCE(vw.received,0) - COALESCE(vw.consumed,0) as previous, 
                                      COALESCE(r.received, 0) as received, 
                                      COALESCE(r.consumed, 0) as consumed, 
                                      COALESCE(vw.received,0)+COALESCE(r.received,0) - COALESCE(vw.consumed,0)-COALESCE(r.consumed,0) as balance 
                                    from 
                                      well_item wi 
                                      inner join item i on wi.item_id = i.id 
                                      inner join well w on w.id = wi.well_id 
                                      inner join drilling d on d.well_id = w.id 
                                      left join drilling_bulk r on r.drilling_id = d.id 
                                      and r.well_item_id = i.id 
                                      left join (
                                        select 
                                          i.id AS item_id, 
                                          COALESCE(
                                            sum(r.received), 
                                            0
                                          ) as received, 
                                          COALESCE(
                                            sum(r.consumed), 
                                            0
                                          ) as consumed 
                                        from 
                                          well_item wi 
                                          inner join item i on wi.item_id = i.id 
                                          inner join well w on w.id = wi.well_id 
                                          inner join drilling d on d.well_id = w.id 
                                          left join drilling_bulk r on r.drilling_id = d.id 
                                          and r.well_item_id = i.id 
                                        where 
                                          d.drilling_date < (
                                            select 
                                              drilling_date 
                                            from 
                                              drilling dd 
                                            where 
                                              id = @0 
                                              and d.well_id = dd.well_id
                                          ) 
                                        group by 
                                          i.id
                                      ) vw on vw.item_id = i.id 
                                    where 
                                      d.id = @0
                                    ORDER BY 
                                      r.created_on DESC", drillingId);
                result.Data = this._services.db.Fetch<vw_drilling_bulk>(sqlQuery);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_bulk>> getItemByWellId(string drillingId, string wellId, string drillingDate)
        {
            var result = new ApiResponse<List<vw_drilling_bulk>>();
            var service_drilling = new DrillingServices(_services.dataContext);
            try
            {
                var listData = new List<vw_drilling_bulk>();

                var sqlQuery = Sql.Builder.Append(@"
                                select 
                                      r.id, 
                                      r.owner_id, 
                                      r.drilling_id, 
                                      i.id AS item_id, 
                                      i.item_name AS item_name, 
                                      i.item_name AS item_name, 
                                      COALESCE(vw.received,0) - COALESCE(vw.consumed,0) as previous, 
                                      COALESCE(r.received, 0) as received, 
                                      COALESCE(r.consumed, 0) as consumed, 
                                      COALESCE(vw.received,0)+COALESCE(r.received,0) - COALESCE(vw.consumed,0)-COALESCE(r.consumed,0) as balance 
                                    from 
                                      well_item wi 
                                      inner join item i on wi.item_id = i.id 
                                      inner join well w on w.id = wi.well_id 
                                      inner join drilling d on d.well_id = w.id 
                                      left join drilling_bulk r on r.drilling_id = d.id 
                                      and r.well_item_id = i.id 
                                      left join (
                                        select 
                                          i.id AS item_id, 
                                          COALESCE(
                                            sum(r.received), 
                                            0
                                          ) as received, 
                                          COALESCE(
                                            sum(r.consumed), 
                                            0
                                          ) as consumed 
                                        from 
                                          well_item wi 
                                          inner join item i on wi.item_id = i.id 
                                          inner join well w on w.id = wi.well_id 
                                          inner join drilling d on d.well_id = w.id 
                                          left join drilling_bulk r on r.drilling_id = d.id 
                                          and r.well_item_id = i.id 
                                        where 
                                          d.drilling_date < (
                                            select 
                                              drilling_date 
                                            from 
                                              drilling dd 
                                            where 
                                              id = @0 
                                              and d.well_id = dd.well_id
                                          ) 
                                        group by 
                                          i.id
                                      ) vw on vw.item_id = i.id 
                                    where 
                                      d.id = @0
                                    ORDER BY 
                                      i.item_name ASC", drillingId);
                listData = this._services.db.Fetch<vw_drilling_bulk>(sqlQuery);

                if (listData.Count > 0)
                {
                    result.Data = listData;
                    result.Status.Success = true;
                    result.Status.Message = "Successfully";
                }
                else
                {
                    sqlQuery = Sql.Builder.Append(@"
                        SELECT TOP 1 r.drilling_id
                        FROM drilling_bulk r
                        LEFT JOIN drilling AS d ON d.id = r.drilling_id
                        LEFT JOIN item i ON i.id = r.well_item_id
                        WHERE d.drilling_date < @0 AND d.well_id = @1
                        ORDER BY d.drilling_date DESC, i.item_name ASC", drillingDate, wellId);
                    var drillingIdHasValue = this._services.db.SingleOrDefault<string>(sqlQuery);


                    if (drillingIdHasValue == null)
                    {
                        sqlQuery = Sql.Builder.Append(@"
                            SELECT id = null, owner_id = null, r.item_id AS item_id, drilling_id = @0, i.item_name AS item_name, i.item_name AS item_name, previous = 0, received = 0, consumed = 0, balance = 0
                            FROM well_item r
                            LEFT JOIN item i ON i.id = r.item_id
                            WHERE r.well_id  = @1
                            ORDER BY i.item_name ASC ", drillingId, wellId);
                        listData = this._services.db.Fetch<vw_drilling_bulk>(sqlQuery);
                    }
                    else
                    {
                        sqlQuery = Sql.Builder.Append(@"
                            SELECT r.id, r.owner_id, r.drilling_id, i.id AS item_id, i.item_name AS item_name, i.item_name AS item_name, COALESCE(r.balance, 0) AS previous, received = 0, consumed = 0, COALESCE(r.balance, 0) AS balance
                            FROM drilling_bulk r 
                            LEFT JOIN item i ON i.id = r.well_item_id
                            WHERE drilling_id = @0
                            ORDER BY i.item_name ASC", drillingIdHasValue);
                        listData = this._services.db.Fetch<vw_drilling_bulk>(sqlQuery);
                    }
                }
                result.Data = listData;
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse<vw_drilling_bulk> GetUserDetail(string recordId)
        {
            var result = new ApiResponse<vw_drilling_bulk>();
            try
            {
                var serviceProject = new DrillingBulkServices(this._services.dataContext);
                var record = serviceProject.GetViewById(recordId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = record;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse Save(DrillingViewModel record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                var recordId = record.drilling.id;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    if (record.drilling_bulk != null)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_bulk where drilling_id = @0", record.drilling.id);
                        this._services.db.Execute(sql);

                        foreach (var item in record.drilling_bulk)
                        {
                            bool isNew = false;
                            var drillingBulk = new drilling_bulk();
                            drillingBulk.InjectFrom(item);
                            result.Status.Success &= SaveEntity(drillingBulk, ref isNew, (r) => item.id = r.id);
                        }
                        if (result.Status.Success)
                        {
                            scope.Complete();
                            result.Status.Message = "Drilling Bulk Item has been changed.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }
    }
}
