﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingMudServices : DrillingMudRepository
    {
        public DrillingMudServices(DataContext dataContext) : base(dataContext) { }
        public ApiResponse<List<vw_drilling_mud>> getDrillingmudDeviationDataDate(string wellId, DateTime startdate, DateTime enddate)
        {
            var result = new ApiResponse<List<vw_drilling_mud>>();
            try
            {
                var sql = Sql.Builder.Append(@"SELECT  r.*,d.drilling_date, mud.*
                            FROM drilling_mud r
							LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
                            LEFT OUTER JOIN mud_type mud ON r.mud_type = mud.id
                            WHERE d.well_id = @0 AND (d.drilling_date BETWEEN  @1 AND  @2)", wellId, startdate, enddate);
                result.Data = this._services.db.Fetch<vw_drilling_mud>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse Save(drilling_mud record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    result.Status.Message = "Successfully Save Drilling Mud.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
