﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class AnnulusServices : AnnulusRepository
    {
        public AnnulusServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Saves(annulus record)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var recordId = record.id;

                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {

                        if (isNew)
                        {
                            result.Status.Message = "The data has been added.";
                        }
                        else
                        {
                            //var last = service_history.GetAll(" AND is_active=@0 AND iadcwi_id=@1 ORDER BY rev DESC ", true, recordId).SingleOrDefault();
                            //history.rev = last.rev + 1;
                            result.Status.Message = "The data has been updated.";
                        }
                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse Save(WIPViewModel record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                //System.Diagnostics.Debug.WriteLine("inininin bangsatd", record.annulus);
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    if (record.annulus != null)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM annulus_pressure_status where wip_id = @0", record.wip.id);
                        this._services.db.Execute(sql);

                        foreach (var item in record.annulus)
                        {
                            bool isNew = false;
                            item.wip_id = record.wip.id;
                            result.Status.Success &= SaveEntity(item, ref isNew, (r) => item.id = r.id);
                        }
                    }
                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Annulus has been updated.";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse<List<vw_annulus>> getByDetail(string wipid)
        {
            var result = new ApiResponse<List<vw_annulus>>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder
                        .Append("SELECT * FROM vw_annulus_pressure_status")
                        .Append("WHERE wip_id = @0", wipid);

                    result.Data = db.Fetch<vw_annulus>(sql);
                }
                result.Status.Success = true;
                result.Status.Message = "Data retrieval successful";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = "Failed to retrieve data: " + ex.Message;
            }
            return result;
        }

        public Page<vw_annulus> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            var result = base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
            return result;
        }

    }
}
