﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingOperationServices : DrillingOperationRepository
    {
        public DrillingOperationServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(drilling_operation record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_operation>> getByWellId(string wellId, DateTime drillingDate)
        {
            var result = new ApiResponse<List<vw_drilling_operation>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                            SELECT r.*
                            FROM drilling_operation r
                            LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
                            WHERE d.well_id = @0 AND d.drilling_date = @1", wellId, drillingDate);
                result.Data = this._services.db.Fetch<vw_drilling_operation>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
