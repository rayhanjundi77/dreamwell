﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class AfeServices : AfeRepository
    {
        DataContext _dataContext;
        public AfeServices(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public ApiResponse Save(afe record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                var isNew = false;
                var recordId = record.id;

                if (record.business_unit_id == null)
                    result.Status.Success = SaveEntity(record, ref isNew, _services.dataContext.PrimaryTeamId, (r) => recordId = r.id);
                else
                {
                    var anakPerusahaanRecord = business_unit.FirstOrDefault("WHERE id=@0", record.business_unit_id);
                    if (anakPerusahaanRecord == null)
                        throw new Exception("Anak Perusahaan Record doesn't found.");
                    result.Status.Success &= SaveEntity(record, ref isNew, anakPerusahaanRecord.default_team, (r) => recordId = r.id);
                }

                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public Page<vw_afe> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public ApiResponse<afe> getDetailByWell(string wellId)
        {
            var result = new ApiResponse<afe>();
            try
            {
                result.Data = this.GetFirstOrDefault(" AND well_id=@0 ", wellId);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_afe>> getDetail(string recordId)
        {
            var result = new ApiResponse<List<vw_afe>>();
            try
            {
                var sql = Sql.Builder
                        .Append("SELECT vw_afe.*, w.well_status")
                        .Append("FROM vw_afe")
                        .Append("JOIN vw_well AS w ON vw_afe.well_id = w.id")
                        .Append("WHERE vw_afe.id = @0", recordId);
                result.Data = this._services.db.Fetch<vw_afe>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<afe_rkap_plan>> getafeall()
        {
            var result = new ApiResponse<List<afe_rkap_plan>>();
            try
            {
                var sql = Sql.Builder.Append(@"select count(we.id) as total_well , SUM(we.afe_cost) as plan_price, bu.unit_name , max(bu.id) as bussines_unit_id 
                                                from dbo.business_unit bu right join
                                                vw_afe af
                                                on bu.id = af.business_unit_id inner join
                                                        dbo.well we
                                                on af.well_id = we.id");
                if (!this._dataContext.IsSystemAdministrator)
                {
                    sql = sql.Append("where bu.id = @0", this._dataContext.PrimaryBusinessUnitId);
                }
                sql = sql.Append(" group by bu.unit_name order by bu.unit_name ");

                //var sql = Sql.Builder.Append(@"select count(we.id) as total_well , SUM(we.afe_cost) as plan_price, bu.unit_name , max(bu.id) as bussines_unit_id 
                //                                from dbo.business_unit bu right join
                //                                vw_afe af
                //                                on bu.id = af.business_unit_id inner join
                //                                        dbo.well we
                //                                on af.well_id = we.id
                //                                 where bu.id = @0
                //                                group by bu.unit_name
                //                            order by bu.unit_name 
                //           ", bussinesunitid);

                result.Data = this._services.db.Fetch<afe_rkap_plan>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<afe_rkap_field_plan>> getAFEFieldRekap(string bussinesunitid)
        {
            var result = new ApiResponse<List<afe_rkap_field_plan>>();
            try
            {
                var sql = Sql.Builder.Append(@"select count(we.id) as total_well , SUM(we.afe_cost) as plan_price, af.field_name, max(bu.id) as bussines_unit_id ,max(af.field_id) as field_id  
                                                from dbo.business_unit bu right join
                                                vw_afe af
                                                on bu.id = af.business_unit_id 
												inner join
                                                dbo.well we
                                                on af.well_id = we.id 
												where bu.id = @0
                                                group by af.field_name
											
                           ",bussinesunitid);
                result.Data = this._services.db.Fetch<afe_rkap_field_plan>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse Submit(string recordId)
        {
            var result = new ApiResponse();
            var isNew = false;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    afe record = new afe();
                    record.id = recordId;
                    record.submitted_by = _services.dataContext.AppUserId;
                    record.submitted_on = DateTime.Now;
                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "The data has been submitted.";
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse Approval(approvalRequestBody request)
        {
            var result = new ApiResponse();
            var isNew = false;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    if (request.IsAccepted)
                    {
                        afe record = new afe();
                        record.id = request.RecordId;
                        record.approved_by = _services.dataContext.AppUserId;
                        record.approved_on = DateTime.Now;
                        result.Status.Success = SaveEntity(record, ref isNew, (r) => request.RecordId = r.id);

                        if (result.Status.Success)
                        {
                            scope.Complete();
                            result.Status.Message = "The data has been Approved.";
                            result.Data = new
                            {
                                request.RecordId
                            };
                        }
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse AfeClose(string afeId)
        {
            var result = new ApiResponse();
            try
            {
                var qry = Sql.Builder.Append(vw_afe_closure.DefaultView, afeId);
                var remaining = _services.db.Fetch<vw_afe_closure>(qry);
                result.Status.Success = true;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    var afeRecord = afe.FirstOrDefault("WHERE id=@0", afeId);
                    if (afeRecord?.is_closed == true)
                        throw new Exception("This AFE has been closed");
                    try
                    {
                        foreach (var r in remaining)
                        {
                            var contractRecord = contract_detail.FirstOrDefault("WHERE id=@0", r.contract_detail_id);
                            contractRecord.remaining_unit = r.new_contract_unit;
                            contractRecord.afe_locked_id = null;
                            result.Status.Success &= _services.dataContext.SaveEntity<contract_detail>(contractRecord, false);
                        }

                        afeRecord.is_closed = true;
                        result.Status.Success &= Save(afeRecord).Status.Success;

                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        result.Status.Success = false;
                        result.Status.Message = ex.Message;
                        appLogger.Error(ex);
                    }
                }

            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<afe_plan>> GetAfeDetails()
        {
            var result = new ApiResponse<List<afe_plan>>();
            try
            {
                var sql = Sql.Builder
                    .Append(@"SELECT 
                        t2.afe_id, 
                        SUM(t2.unit) AS total_units
                      FROM 
                        vw_afe_line t1
                      INNER JOIN 
                        vw_afe_plan t2 ON t1.id = t2.afe_line_id
                      WHERE 
                        t1.parent_name = 'TANGIBLE COST' 
                      GROUP BY 
                        t2.afe_id;");

                result.Data = this._services.db.Fetch<afe_plan>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }


    }
}
