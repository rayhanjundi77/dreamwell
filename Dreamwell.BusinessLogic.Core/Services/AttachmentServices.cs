﻿using Dreamwell.DataAccess;
using Dreamwell.DataAccess.Repository;
using Dreamwell.Infrastructure;
using System;
using System.IO;
using System.Web;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class AttachmentServices : AttachmentRepository
    {
        private readonly CustomDataContext dataContext;

        public AttachmentServices(CustomDataContext customDataContext) : base(customDataContext)
        {
            this.dataContext = customDataContext;
        }

        public ApiResponse SaveAttachment(HttpFileCollection files, string wellId, string wellFinal)
        {
            var result = new ApiResponse();

            try
            {
                System.Diagnostics.Debug.WriteLine($"IsSystemAdministrator: {this.dataContext.IsSystemAdministrator}");

                var isNew = false;
                var record = new tbl_pdf
                {
                    well_id = wellId,
                    well_final_report_id = wellFinal
                };

                if (files["pdf_content_1"] != null && files["pdf_content_1"].ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        files["pdf_content_1"].InputStream.CopyTo(memoryStream);
                        record.pdf_content_1 = memoryStream.ToArray();
                    }
                }
                if (files["pdf_content_2"] != null && files["pdf_content_2"].ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        files["pdf_content_2"].InputStream.CopyTo(memoryStream);
                        record.pdf_content_2 = memoryStream.ToArray();
                    }
                }
                if (files["pdf_content_3"] != null && files["pdf_content_3"].ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        files["pdf_content_3"].InputStream.CopyTo(memoryStream);
                        record.pdf_content_3 = memoryStream.ToArray();
                    }
                }
                if (files["pdf_content_4"] != null && files["pdf_content_4"].ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        files["pdf_content_4"].InputStream.CopyTo(memoryStream);
                        record.pdf_content_4 = memoryStream.ToArray();
                    }
                }
                var recordId = record.well_final_report_id;
                if (string.IsNullOrEmpty(recordId))
                    record.id = CommonTools.GuidHash.ConvertToMd5HashGUID(record.well_final_report_id).ToString();

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.well_final_report_id);

                if (result.Status.Success)
                {
                    result.Status.Message = isNew ? "The data has been added." : "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
                System.Diagnostics.Debug.WriteLine("Error during SaveLampiran: " + ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }

            return result;
        }
    }
}
