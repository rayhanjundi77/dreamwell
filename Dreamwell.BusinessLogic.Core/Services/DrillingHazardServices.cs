﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using System.Transactions;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingHazardServices : DrillingHazardRepository
    {
        public DrillingHazardServices(DataContext dataContext) : base(dataContext) { }

        /*Do not use this function on datatables*/
        public Page<vw_drilling_hazard> GetViewPerPage(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public ApiResponse<vw_drilling_hazard> GetUserDetail(string recordId)
        {
            var result = new ApiResponse<vw_drilling_hazard>();
            try
            {
                var serviceProject = new DrillingHazardServices(this._services.dataContext);
                var record = serviceProject.GetViewById(recordId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = record;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse Save(WellViewModel record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    if (record.drilling_hazard != null)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drilling_hazard where well_id = @0", record.well.id);
                        this._services.db.Execute(sql);

                        foreach (var item in record.drilling_hazard)
                        {
                            bool isNew = false;
                            item.well_id = record.well.id;
                            result.Status.Success &= SaveEntity(item, ref isNew, (r) => item.id = r.id);
                        }
                    }
                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Drilling Hazard has been updated.";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public Page<vw_drilling_hazard> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            var kosong = Sql.Builder.Append("  \n AND (\n ) ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null && filterBuilder.SQL != kosong.SQL)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_drilling_hazard> LookupUser(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 AND r.is_sysadmin IS NULL AND r.is_ldap=@0", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            sqlFilter.Append(" ORDER BY COALESCE(r.last_name, r.first_name) ASC ");
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
