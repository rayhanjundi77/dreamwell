﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using CommonTools.Helper;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ContractServices : ContractRepository
    {
        private DataContext _dataContext;
        public ContractServices(DataContext dataContext) : base(dataContext) 
        {
            this._dataContext = dataContext;
        }

        public ApiResponse Save(contract record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                var interval = record.end_date - record.start_date;
                record.duration = interval.Value.Days;

                if (record.business_unit_id == null)
                    result.Status.Success = SaveEntity(record, ref isNew, _services.dataContext.PrimaryTeamId, (r) => recordId = r.id);
                else
                {
                    var anakPerusahaanRecord = business_unit.FirstOrDefault("WHERE id=@0", record.business_unit_id);
                    if (anakPerusahaanRecord == null)
                        throw new Exception("Anak Perusahaan Record doesn't found.");
                    result.Status.Success &= SaveEntity(record, ref isNew, anakPerusahaanRecord.default_team, (r) => recordId = r.id);
                }

                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex.Message);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public Page<vw_contract> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Node<vw_contract_detail> GetTree(string materialId)
        {
            Node<vw_contract_detail> results = new Node<vw_contract_detail>(null);
            try
            {
                var contracts = vw_contract_detail.Fetch(" WHERE is_active=1 AND material_id = @0 ", materialId);
                foreach (var item in contracts)
                {
                    var record = new vw_contract_detail();
                    record.InjectFrom(item);
                    //record.description = item.material_name;
                    //record.unit = item.unit;
                    //record.unit_price = item.unit_price;

                    Node<vw_contract_detail> parent = results.Add(record);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }


        public ApiResponse<List<vw_contract>> GetByContractNumber(string contractNumber)
        {
            var result = new ApiResponse<List<vw_contract>>();
            var service_detail = new ContractDetailServices(_services.dataContext);
            try
            {
                var data = this.GetViewAll(" AND r.is_active=@0 AND r.organization_id=@1 AND r.contract_no LIKE @2 AND r.submitted_by IS NOT NULL AND r.submitted_on IS NOT NULL ", true, _services.dataContext.OrganizationId, '%'+ contractNumber + '%');
                appLogger.Error(data);
                result.Data = data;
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse<List<vw_contract>> GetAllContract()
        {
            var result = new ApiResponse<List<vw_contract>>();
            var service_detail = new ContractDetailServices(_services.dataContext);
            try
            {
                var data = this.GetViewAll(" AND r.is_active=@0 AND r.organization_id=@1 AND r.submitted_by IS NOT NULL AND r.submitted_on IS NOT NULL ", true, _services.dataContext.OrganizationId);
                appLogger.Error(data);
                result.Data = data;
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        //public ApiResponse<List<vw_afe_detail>> getContractByAfeId(string afeId)
        //{
        //    var result = new ApiResponse<List<vw_afe_detail>>();
        //    try
        //    {
        //        var sql = Sql.Builder.Append(@"
        //                    SELECT r.id, r.contract_no, v.name as vendor_name, r.total, r.start_date, r.end_date 
        //                    FROM contract r
        //                    INNER JOIN vendor v ON r.vendor_contract_id = v.id
        //                    WHERE r.id IN (
	       //                     SELECT ad.contract_id FROM afe_detail ad WHERE ad.afe_id = @0
        //                    )", afeId);
        //        var item = this._services.db.Fetch<vw_afe_detail>(sql);
        //        result.Data = item;
        //        result.Status.Success = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        result.Status.Success = false;
        //        result.Status.Message = ex.Message;
        //    }
        //    return result;
        //}

        public ApiResponse<List<vw_contract>> GetList()
        {
            var result = new ApiResponse<List<vw_contract>>();
            var service_detail = new ContractDetailServices(_services.dataContext);
            try
            {
                var data = this.GetViewAll();
                
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        item.ContractDetail = service_detail.GetViewAll(" AND r.contract_id=@0 ", item.id);
                    }
                }

                result.Data = data;
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse Submit(string recordId)
        {
            var result = new ApiResponse();
            var isNew = false;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    contract record = new contract();
                    record.id = recordId;
                    record.submitted_by = _services.dataContext.AppUserId;
                    record.submitted_on = DateTime.Now;
                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "The data has been submitted.";
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponseCount<vw_contract> CountTotalContract(string isadmin)
        {
            var result = new ApiResponseCount<vw_contract>();
            try
            {
                var recordData = new vw_contract();

                var Qry = Sql.Builder.Append(string.Format("select count (id) as countId from vw_contract where is_active=1"));
                if (isadmin == "NO")
                {
                    Qry = Sql.Builder.Append(string.Format("select count (id) as countId from vw_contract where is_active=1 and business_unit_id=@0"), _services.dataContext.PrimaryBusinessUnitId);
                }
                this._services.db.CommandTimeout = 3000;
                int totalDDR = this._services.db.FirstOrDefault<int>(Qry);
                //recordData.drilling_operation = service_drillingOperation.GetViewAll(" AND r.drilling_id=@0 ", recordData.drilling.id);
                //recordData.drilling = this.GetViewById(recordId);
                //recordData.drilling_operation = service_drillingOperation.GetViewAll(" AND r.drilling_id=@0 ", recordId);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = totalDDR;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                result.Data = 0;
            }

            return result;
        }


        public ApiResponseCount<vw_contract> CountTotalExpiredContract()
        {
            var result = new ApiResponseCount<vw_contract>();
            try
            {
                var recordData = new vw_contract();

                var Qry = Sql.Builder.Append(string.Format("select count (id) as countId from vw_contract where is_active=1 and end_date < CAST( getdate() AS DATE) "));
                if (!this._dataContext.IsSystemAdministrator)
                {
                    Qry = Sql.Builder.Append(string.Format("select count (id) as countId from vw_contract where is_active=1  and end_date < CAST( getdate() AS DATE) and business_unit_id=@0"), _services.dataContext.PrimaryBusinessUnitId);
                }
                this._services.db.CommandTimeout = 3000;
                int total = this._services.db.FirstOrDefault<int>(Qry);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = total;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                result.Data = 0;
            }

            return result;
        }


        public ApiResponseCount<vw_contract> CountTotalNearlyExpiredContract()
        {
            var result = new ApiResponseCount<vw_contract>();
            try
            {
                var recordData = new vw_contract();

                var Qry = Sql.Builder.Append(string.Format("select count (id) as countId from vw_contract where is_active=1  and end_date > CAST( getdate() AS DATE) and DATEDIFF(DAY, end_date, CAST( getdate() AS DATE)) between -240 and 0 "));
                if (!this._dataContext.IsSystemAdministrator)
                {
                    Qry = Sql.Builder.Append(string.Format("select count (id) as countId from vw_contract where is_active=1 and business_unit_id=@0  and end_date > CAST( getdate() AS DATE) and DATEDIFF(DAY, end_date, CAST( getdate() AS DATE)) between -240 and 0 "), _services.dataContext.PrimaryBusinessUnitId);
                }
                this._services.db.CommandTimeout = 3000;
                int total = this._services.db.FirstOrDefault<int>(Qry);
                //recordData.drilling_operation = service_drillingOperation.GetViewAll(" AND r.drilling_id=@0 ", recordData.drilling.id);
                //recordData.drilling = this.GetViewById(recordId);
                //recordData.drilling_operation = service_drillingOperation.GetViewAll(" AND r.drilling_id=@0 ", recordId);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
                result.Data = total;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                result.Data = 0;
            }

            return result;
        }
    }
}
