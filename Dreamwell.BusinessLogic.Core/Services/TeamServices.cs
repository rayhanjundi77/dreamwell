﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using CommonTools;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class TeamServices : TeamRepository
    {
        public TeamServices(DataContext dataContext) : base(dataContext) { }

        public team GetTeamByBusinessUnit(string businessUnitId)
        {
            var result = new team();
            try
            {
                var bu = business_unit.FirstOrDefault("WHERE id=@0", businessUnitId);
                result = team.FirstOrDefault("WHERE id=@0", bu.default_team);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return result;
        }

        public ApiResponse Save(team record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                record.id = CommonTools.GuidHash.ConvertToMd5HashGUID(record.team_name).ToString();
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                if (ex.Message.Contains("Violation of UNIQUE KEY constraint"))
                    result.Status.Message = "Team " + record.team_name + " has been exist on this Anak Perusahaan Hulu.";
            }

            return result;
        }

        public DataTablesResponse GetListDataWithDataTables(DataTableRequest dataTablesRequest)
        {
            var userLogin = _services.dataContext;
            //var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 AND bu.id IS NOT NULL ", true);
            var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
            return this.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
        }

        public Page<vw_team> GetViewPerPage(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

    }
}
