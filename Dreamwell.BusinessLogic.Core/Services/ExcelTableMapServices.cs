﻿using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using CommonTools.JWT;
using PetaPoco;
using Hangfire;
using System.Diagnostics;
using Dreamwell.Infrastructure.Session;
using CommonTools;
using CommonTools.JSTree;
using Newtonsoft.Json;
using System.Dynamic;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ExcelTableMapServices : ExcelTableMapRepository
    {
        public ExcelTableMapServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(excel_table_map record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                var entityInfo = application_entity.FirstOrDefault("WHERE id=@0", record.application_entity_id);
                if (entityInfo == null)
                    throw new Exception("Table / Application Entity not found !");

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = $"{entityInfo.display_name} has been related";
                    }
                    else
                    {
                        result.Status.Message = $"{entityInfo.display_name} has been updated";
                    }

                }
                else
                {
                    result.Status.Message = $"{entityInfo.display_name} failed create table relation";
                }
                result.Data = new
                {
                    recordId
                };
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse<List<string>> GetPropertiesName(string applicationEntityId)
        {
            var result = new ApiResponse<List<string>>();

            try
            {
                var service = new ApplicationEntityServices(this._services.dataContext);
                var entity = service.GetById(applicationEntityId);
                if (entity == null)
                    throw new Exception("Application Entity / Table not found !");

                var columns = _services.db.Fetch<string>(@"SELECT COLUMN_NAME
                                FROM INFORMATION_SCHEMA.COLUMNS
                                WHERE CONCAT(TABLE_SCHEMA, '.', TABLE_NAME) = @0
                                ORDER BY ORDINAL_POSITION", entity.entity_name);


                result.Data = columns;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public Page<vw_excel_table_map> Lookup(MarvelDataSourceRequest marvelDataSourceRequest, string templateId)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            sqlFilter.Append(" AND r.excel_template_id=@0 ", templateId);
            var kosong = Sql.Builder.Append("  \n AND (\n ) ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null && filterBuilder.SQL != kosong.SQL)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }



        public List<dynamic> GetTreeMap(string templateId, string parentId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                var sql = Sql.Builder.Append(excel_table_map.DefaultView);
                sql.Append("WHERE r.excel_template_id=@0 ", templateId);
                if (string.IsNullOrEmpty(parentId))
                {
                    sql.Append("AND r.organization_id=@0 AND r.is_active=1 AND r.parent_table_map_id IS NULL ", _services.dataContext.OrganizationId);
                }
                else
                {
                    sql.Append("AND r.organization_id=@0 AND r.is_active=1 AND r.parent_table_map_id =@1 ", _services.dataContext.OrganizationId,parentId);
                }
                sql.Append("ORDER BY r.seq_index asc ");
                var data = _services.db.Fetch<vw_excel_table_map>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.entity_name;
                    itemList.NodeType = "TABLE";
                    itemList.Icon = "fal fa-table";

                    itemList.Children = new List<object>() {
                        new {
                            NodeType = "FOLDER_TABLES",
                            Text = "TABLES",
                            State = "closed",
                            Children=true

                        },
                        new {
                            NodeType = "FOLDER_COLUMNS",
                            Text = "COLUMNS",
                            State = "closed",
                            Children=true
                        }
                    };
                    results.Add(itemList);
                    row++;
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<dynamic> GetTreeColumnMap(string tableMapId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                var sql = Sql.Builder.Append(excel_column_map.DefaultView);
                sql.Append("WHERE r.excel_table_map_id=@0 ORDER BY r.seq_index asc", tableMapId);
                var data = _services.db.Fetch<vw_excel_column_map>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.entity_column;
                    itemList.NodeType = "COLUMN";
                    itemList.Icon = "fal fa-circle";
                    results.Add(itemList);
                    row++;
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }


    }
}
