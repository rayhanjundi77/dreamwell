﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Omu.ValueInjecter;
using System.Dynamic;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ApprovalServices : ApprovalRepository
    {
        public ApprovalServices(DataContext dataContext) : base(dataContext) { }

        public bool Save(approval record)
        {
            var result = false;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    string recordId = record.id;

                    result = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result = false;
                }
            }
            return result;
        }

        public Page<vw_approval> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 ", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }


        public bool CreateApproval(string recordId, string approverId, short level, approvalStatus sts)
        {
            bool ret = false;

            try
            {
                approval data = approval.FirstOrDefault("WHERE application_entity_id=@0 AND record_id=@1 AND [level]=@2", drilling.EntityId, recordId, level);
                if (data == null)
                {
                    data = new approval()
                    {
                        record_id = recordId,
                        application_entity_id = drilling.EntityId,
                        level = level,
                        status = (short)sts
                    };

                    ret = this._services.dataContext.SaveEntity<approval>(data, true);

                    approver approverdata = new approver()
                    {
                        approval_id = data.id,
                        approver_id = approverId
                    };

                    ret &= this._services.dataContext.SaveEntity<approver>(approverdata, true);

                    if (level == 0)
                    {
                        approval_history apphistory = new approval_history()
                        {
                            approval_id = data.id,
                            status = (short)approvalStatus.Accepted,
                            approved_by = this._services.dataContext.AppUserId,
                            approved_on = DateTime.Now
                        };
                        ret &= CreateApprovalHistory(apphistory);
                    }
                }
                else
                {
                    data.status = (short)sts;
                    ret = this._services.dataContext.SaveEntity<approval>(data, false);
                    ret &= UpdateApproval(data, sts, string.Empty);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return ret;
        }

        public bool UpdateApproval(approval approvaldata, approvalStatus apprstatus, string description)
        {
            bool ret = false;

            try
            {
                if (apprstatus == approvalStatus.Rejected || apprstatus == approvalStatus.Accepted)
                {
                    approval_history appr_history = new approval_history();
                    appr_history.approval_id = approvaldata.id;
                    appr_history.status = (short)apprstatus;
                    appr_history.description = description;
                    appr_history.approved_by = this._services.dataContext.AppUserId;
                    appr_history.approved_on = DateTime.Now;

                    ret = CreateApprovalHistory(appr_history);

                    approvaldata.approved_by = this._services.dataContext.AppUserId;
                    approvaldata.approved_on = appr_history.approved_on;
                    approvaldata.last_approval_history_id = appr_history.id;

                    ret = this._services.dataContext.SaveEntity<approval>(approvaldata, false);
                }
                else
                {
                    ret = this._services.dataContext.SaveEntity<approval>(approvaldata, false);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return ret;
        }

        public bool CreateApprovalHistory(approval_history data)
        {
            bool ret = false;

            try
            {
                ret = this._services.dataContext.SaveEntity<approval_history>(data, true);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return ret;
        }

        public string GetManager()
        {
            string ret = string.Empty;

            try
            {
                team t_data = team.FirstOrDefault("WHERE id=@0", this._services.dataContext.PrimaryTeamId);

                ret = t_data?.team_leader;

                if (string.IsNullOrEmpty(ret))
                {
                    throw new Exception("Team Leader not found");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return ret;
        }

    }
}
