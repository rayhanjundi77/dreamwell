﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingMudPitServices : DrillingMudPitRepository
    {
        public DrillingMudPitServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse<List<drilling_mud_pit>> Save(List<drilling_mud_pit> records, string drillingId)
        {
            var result = new ApiResponse<List<drilling_mud_pit>>();
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    var sql = Sql.Builder.Append("");
                    sql = Sql.Builder.Append(@"DELETE FROM drilling_mud_pit where drilling_id = @0", drillingId);
                    this._services.db.Execute(sql);

                    records = records.Where(o => o != null).ToList();
                    result.Status.Success = true;
                    int seq = 1;
                    foreach (var record in records)
                    {
                        var isNew = false;
                        record.seq = seq;
                        result.Status.Success &= SaveEntity(record, ref isNew, (r) => record.id = r.id);
                        seq++;
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = records;
                    }

                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    appLogger.Error(ex);
                }

            }
            return result;
            //try
            //{
            //    var isNew = false;
            //    var recordId = record.id;

            //    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
            //    if (result.Status.Success)
            //    {
            //        if (isNew)
            //        {
            //            result.Status.Message = "The data has been added.";
            //        }
            //        else
            //        {
            //            result.Status.Message = "The data has been updated.";
            //        }
            //        result.Data = new
            //        {
            //            recordId
            //        };
            //    }
            //}
            //catch (Exception ex)
            //{
            //    appLogger.Error(ex);
            //    result.Status.Success = false;
            //    result.Status.Message = ex.Message;
            //}
            //return result;
        }
    }
}
