﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Newtonsoft.Json;
using Dreamwell.BusinessLogic.Core.SysConfig;
using Omu.ValueInjecter;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using CommonTools.Helper;
using CommonTools.EeasyUI;
using System.Reflection;
using System.Runtime.ConstrainedExecution;
using System.Text.RegularExpressions;
using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess.Models.Entity;
using System.Security.Policy;
using System.Drawing.Drawing2D;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class BusinessUnitServices : BusinessUnitRepository
    {
        DataContext dataContext;
        public BusinessUnitServices(DataContext dataContext) : base(dataContext)
        {
            this.dataContext = dataContext;
        }

        public Node<vw_business_unit_field> GetComboTreeAssetByUnit(ITreeRequest request, string businessUnitId)
        {
            Node<vw_business_unit_field> results = new Node<vw_business_unit_field>(null);
            try
            {
                var qry = Sql.Builder.Append(string.Format("SELECT distinct asset_id ,asset_name as field_name FROM ({0} WHERE r.business_unit_id = '{1}') AS r ",
                    business_unit_field.DefaultView, businessUnitId));

                var records = vw_business_unit_field.Fetch(qry);
                foreach (var record in records)
                {
                    Node<vw_business_unit_field> parent = results.Add(record);
                    GetComboTreeField(businessUnitId, record.asset_id, parent);
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return results;
        }
        protected void GetComboTreeField(String businessUnitId, String assetId, Node<vw_business_unit_field> parent)
        {
            var children = parent;
            try
            {
                var qry = Sql.Builder.Append(business_unit_field.DefaultView);
                qry.Append(" WHERE r.business_unit_id=@0 AND f.asset_id=@1 ", businessUnitId, assetId);

                var records = vw_business_unit_field.Fetch(qry).OrderBy(x => x.field_name).ToList();
                if (records.Count > 0)
                {
                    foreach (var record in records)
                    {
                        var data = new vw_business_unit_field();
                        data.id = record.field_id;
                        data.field_id = record.field_id;
                        data.field_name = record.field_name;
                        children.Add(data);
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);

            }

        }


        public Node<vw_business_unit> GetComboTree(ITreeRequest request)
        {
            Node<vw_business_unit> results = new Node<vw_business_unit>(null);
            try
            {   
                var qry = Sql.Builder.Append(" WHERE is_active=1 and organization_id=@0 AND parent_unit IS NULL", _services.dataContext.OrganizationId);
                
                if (!this._services.dataContext.IsSystemAdministrator)
                {
                    qry = Sql.Builder.Append("WHERE id = @0 AND is_active = 1", _services.dataContext.PrimaryBusinessUnitId);
                }
                
                qry.Append(" ORDER BY unit_name asc ");
                var records = business_unit.Fetch(qry);
                foreach (var record in records)
                {
                    var data = new vw_business_unit();
                    data.id = record.id;
                    data.unit_name = record.unit_name;
                    Node<vw_business_unit> parent = results.Add(data);
                    GetComboTreeByParent(data.id, parent);
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return results;
        }

        protected void GetComboTreeByParent(String parentId, Node<vw_business_unit> parent)
        {
            var children = parent;
            try
            {
                var records = business_unit.Fetch(" WHERE parent_unit=@0 ORDER BY unit_name asc", parentId);
                if (records.Count > 0)
                {
                    foreach (var record in records)
                    {
                        var data = new vw_business_unit();
                        data.id = record.id;
                        data.unit_name = record.unit_name;
                        GetComboTreeByParent(data.id, children.Add(data));
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);

            }

        }

        public async Task<ApiResponse> SaveAndCreateTeam(business_unit record)
        {
            var result = new ApiResponse();
            //using (var scope = new TransactionScope(TransactionScopeOption.Required))
            //{
            record.unit_name = Regex.Replace(record.unit_name, @"<(.|\n)*?>", string.Empty);
            record.unit_code = Regex.Replace(record.unit_code, @"<(.|\n)*?>", string.Empty);
            try
            {
                var isNew = false;
                var recordId = record.id;

                var recordMapEntity = MapToEntity(record, ref isNew);

                if (isNew)
                {
                    //Saat ini baru bisa cover 1 organization, kalau multi talent ganti hash guid team id unit_code + organization
                    var oldTeam = team.FirstOrDefault("WHERE organization_id=@0 AND team_name =@1", this._services.dataContext.OrganizationId, string.Format("{0} Team", record.unit_code));
                    if (oldTeam == null)
                    {
                        oldTeam = new team();
                        oldTeam.organization_id = this._services.dataContext.OrganizationId;
                        oldTeam.id = CommonTools.GuidHash.ConvertToMd5HashGUID(string.Format("{0} Team", record.unit_code)).ToString();
                        oldTeam.team_name = string.Format("{0} Team", record.unit_code);
                        result.Status.Success = this._services.dataContext.SaveEntity<team>(oldTeam, true);
                    }
                    recordMapEntity.default_team = oldTeam.id;
                }
                else result.Status.Success = true;
                //result.Status.Success &= SaveEntity(recordMapEntity, ref isNew, (r) => recordId = r.id);

                var businessUnit = new BusinessUnit();
                businessUnit.default_team = recordMapEntity.default_team;
                businessUnit.unit_code = recordMapEntity.unit_code;
                businessUnit.unit_name = recordMapEntity.unit_name;
                businessUnit.parent_unit = recordMapEntity.parent_unit;

                var repo = new BaseRepository<BusinessUnit>(this.dataContext);
                var repoResult = await repo.Save(businessUnit, true);
                if (repoResult != null)
                {
                    result.Status.Success = true;
                }

                if (result.Status.Success)
                {

                    //scope.Complete();
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            //}
            return result;
        }

        public ApiResponse Save(business_unit record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }
        public ApiResponse UpadateBussiessUnit(business_unit record)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                record.unit_name = Regex.Replace(record.unit_name, @"<(.|\n)*?>", string.Empty);
                record.unit_code = Regex.Replace(record.unit_code, @"<(.|\n)*?>", string.Empty);
                try
                {
                    var recordId = record.id;
                    int situasi = 0;
                 
                    situasi = _services.db.Execute("UPDATE dbo.business_unit SET unit_name =@0,unit_code=@1 WHERE id = @2 ", record.unit_name, record.unit_code, record.id);

                    if (situasi >= 1)
                    {
                        scope.Complete();
                        result.Status.Success = true;
                    }
                    

                    if (result.Status.Success)
                    {
                      
                        result.Status.Message = "The data has been updated.";
                        result.Data = new
                        {
                            recordId
                        };
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "The data failed updated.";
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public Page<vw_business_unit> NewLookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize);
        }


        public Page<vw_business_unit> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var userLogin = _services.dataContext;
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 AND (r.organization_id=@0 OR r.organization_id IS NULL ) ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public Page<vw_business_unit> LookupParent(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var userLogin = _services.dataContext;

            var sqlFilter = Sql.Builder
                .Append("is_active = 1 AND parent_unit IS NULL")
                .OrderBy("unit_name ASC");

            if (marvelDataSourceRequest.Filter != null)
            {
                var filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }

            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }


        public List<JSTreeResponse<business_unit>> GetTreeBusinessUnit()
        {
            List<JSTreeResponse<business_unit>> results = new List<JSTreeResponse<business_unit>>();
            try
            {
                var data = business_unit.Fetch(" WHERE is_active=1 and organization_id=@0 and parent_unit is null AND is_default is null ORDER BY order_index asc ", _services.dataContext.OrganizationId);
                List<string> childNodes = null;
                int row = 0;
                foreach (var item in data)
                {
                    var menu = new JSTreeResponse<business_unit>();
                    if (row == 0)
                        menu.State.Opened = true;

                    var children = GetDetailBusinessUnitByParentUnit(item.id, ref childNodes);
                    menu.Id = item.id;
                    menu.Text = item.unit_name;
                    menu.Icon = "fa fa-folder";
                    menu.Children = children;
                    menu.data = item;
                    results.Add(menu);
                    row++;
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public bool OrderTree(String recordId, String parentTargetId, int newOrder)
        {
            var result = false;

            try
            {
                _services.db.Execute(";EXEC usp_business_unit_order @@id = @0 , @@order_index = @1 ,@@parent_unit_id = @2",
                    recordId, newOrder, parentTargetId);
                result = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }


        protected List<JSTreeResponse<business_unit>> GetDetailBusinessUnitByParentUnit(String parentId, ref List<string> childNodes)
        {
            List<JSTreeResponse<business_unit>> results = new List<JSTreeResponse<business_unit>>();
            if (childNodes == null)
            {
                childNodes = new List<string>();
                childNodes.Add(parentId);
            }
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var data = business_unit.Fetch(" WHERE parent_unit=@0 ORDER BY order_index asc", parentId);
                foreach (var item in data)
                {
                    if (childNodes.Contains(item.id)) continue;
                    var menu = new JSTreeResponse<business_unit>();
                    menu.Id = item.id;
                    menu.Text = item.unit_name;
                    menu.Icon = "fa fa-folder";
                    menu.State = null;
                    var children = GetDetailBusinessUnitByParentUnit(item.id, ref childNodes);
                    if (children != null)
                    {
                        menu.Children = children;
                    }
                    menu.data = item;
                    results.Add(menu);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }


        public ApiResponse BusinessUnitFieldSave(string businessUnitId, string[] fieldIds)
        {
            var result = new ApiResponse();
            var log = string.Empty;
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                try
                {
                    int rowDeleted = 0;
                    rowDeleted = business_unit_field.Delete("WHERE business_unit_id=@0", businessUnitId);
                    log = $"Business Unit {businessUnitId},  {rowDeleted} field has been deleted";
                    result.Status.Success = true;
                    appLogger.Debug(log);

                    //var fields = business_unit_field.Fetch(" WHERE business_unit_id=@0 ");

                    foreach (var fid in fieldIds)
                    {
                        var record = new business_unit_field();
                        record.business_unit_id = businessUnitId;
                        record.field_id = fid;
                        result.Status.Success &= _services.dataContext.SaveEntity<business_unit_field>(record, true);
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                    }
                    result.Status.Message = "Assign Field to Anak Perusahaan Hulu Succesffuly";
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            return result;
        }


        public ApiResponse UpdateLeader(string businessUnitId, string appuserid)
        {
            var result = new ApiResponse();
            var log = string.Empty;
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
                try
                {
                    var record = GetById(businessUnitId);
                    var service = new TeamServices(this._services.dataContext);

                    var teamrecord = service.GetById(record.default_team);

                    teamrecord.team_leader = appuserid;

                    result.Status.Success = this._services.dataContext.SaveEntity<team>(teamrecord, false);

                    result.Status.Message = "Assign Team Leader Succesffuly";
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            return result;
        }

        // Region

        public ApiResponse<List<region>> getRegion(string businessUnitId = null)
        {
            var result = new ApiResponse<List<region>>();
            try
            {
                Console.WriteLine("Executing SQL query for region data.");

                var sql = Sql.Builder;

                // Conditional logic for non-admin vs. admin access
                if (!string.IsNullOrEmpty(businessUnitId))
                {
                    // Non-admin: Filter by parent of specified businessUnitId
                    sql.Append(@"
                WITH RecursiveHierarchy AS (
                    SELECT 
                        bu.id AS business_unit_id,
                        bu.unit_name,
                        bu.parent_unit
                    FROM 
                        vw_business_unit bu
                    WHERE 
                        bu.id = @0

                    UNION ALL

                    SELECT 
                        parent.id,
                        parent.unit_name,
                        parent.parent_unit
                    FROM 
                        vw_business_unit parent
                    INNER JOIN RecursiveHierarchy child ON child.parent_unit = parent.id
                ),
                Data AS (
                    SELECT 
                        h.business_unit_id,
                        h.unit_name,
                        h.parent_unit,
                        MIN(we.id) AS well_id,
                        SUM(we.afe_cost) AS planning_cost,
                        COUNT(we.id) AS planning_well,
                        COALESCE(SUM(daily.daily_cost), 0) AS actual_cost,
                        COUNT(d.id) AS actual_well
                    FROM 
                        RecursiveHierarchy h
                    LEFT JOIN 
                        dbo.well we ON h.business_unit_id = we.business_unit_id 
                    LEFT JOIN 
                        drilling r ON r.well_id = we.id
                    OUTER APPLY 
                        (SELECT COALESCE(SUM(daily_cost), 0) AS daily_cost 
                         FROM [dbo].[udf_get_daily_cost](r.id)) AS daily
                    LEFT JOIN 
                        tvd_plan d ON d.well_id = we.id
                    GROUP BY 
                        h.business_unit_id, h.unit_name, h.parent_unit
                )
                SELECT 
                    business_unit_id,
                    unit_name,
                    parent_unit,
                    well_id,
                    planning_cost,
                    planning_well,
                    actual_cost,
                    actual_well
                FROM 
                    Data
                WHERE business_unit_id = (
                    SELECT TOP 1 parent_unit
                    FROM vw_business_unit 
                    WHERE id = @0
                    ORDER BY parent_unit DESC
                ) OR parent_unit is null
                ORDER BY 
                    unit_name;
            ", businessUnitId);
                    Console.WriteLine($"Filtered by top-level region for business unit ID: {businessUnitId}");
                }
                else
                {
                    // Admin: Retrieve only top-level parent units
                    sql.Append(@"
                SELECT 
                    bu.id AS business_unit_id,
                    bu.unit_name,
                    bu.parent_unit,
                    MIN(we.id) AS well_id,
                    SUM(we.afe_cost) AS planning_cost,
                    COUNT(we.id) AS planning_well,
                    COALESCE(SUM(daily.daily_cost), 0) AS actual_cost,
                    COUNT(d.id) AS actual_well
                FROM 
                    vw_business_unit bu
                LEFT JOIN 
                    dbo.well we ON bu.id = we.business_unit_id 
                LEFT JOIN 
                    drilling r ON r.well_id = we.id
                OUTER APPLY 
                    (SELECT COALESCE(SUM(daily_cost), 0) AS daily_cost 
                     FROM [dbo].[udf_get_daily_cost](r.id)) AS daily
                LEFT JOIN 
                    tvd_plan d ON d.well_id = we.id
                WHERE 
                    bu.parent_unit IS NULL  
                GROUP BY 
                    bu.id, bu.unit_name, bu.parent_unit
                ORDER BY 
                    bu.unit_name;
            ");
                    Console.WriteLine("Admin access - retrieving all top-level parent units.");
                }

                // Execute the query and fetch results
                result.Data = this._services.db.Fetch<region>(sql);

                if (result.Data == null || result.Data.Count == 0)
                {
                    result.Status.Success = true;
                    result.Status.Message = "No data found.";
                    result.Data = new List<region>();
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                Console.WriteLine($"Error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }
            return result;
        }

        //public ApiResponse<List<chart_region>> getchartRegion(string businessUnitId = null)
        //{
        //    var result = new ApiResponse<List<chart_region>>();
        //    try
        //    {
        //        Console.WriteLine("Executing SQL query for region data.");

        //        // Build the base SQL query to retrieve only the top-level region for the specified business unit hierarchy
        //        var sql = Sql.Builder.Append(@"
        //    WITH Data AS (
        //        SELECT 
        //            bu.id AS business_unit_id, 
        //            bu.unit_name AS unit_name, 
        //            bu.parent_unit,  
        //            COALESCE(SUM(pt.total_hours), 0) AS hours_pt, 
        //            COALESCE(SUM(npt.total_hours), 0) AS hours_npt
        //        FROM 
        //            vw_business_unit bu
        //        LEFT JOIN 
        //            dbo.well we ON bu.id = we.business_unit_id
        //        LEFT JOIN 
        //            drilling r ON r.well_id = we.id
        //        CROSS APPLY (
        //            SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours
        //            FROM drilling_operation_activity doa
        //            INNER JOIN iadc AS i ON i.id = doa.iadc_id
        //            CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s
        //            WHERE doa.drilling_id = r.id AND i.type = 1
        //        ) AS pt
        //        CROSS APPLY (
        //            SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours
        //            FROM drilling_operation_activity doa
        //            INNER JOIN iadc AS i ON i.id = doa.iadc_id
        //            CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s
        //            WHERE doa.drilling_id = r.id AND i.type = 2
        //        ) AS npt
        //        GROUP BY 
        //            bu.id, 
        //            bu.unit_name, 
        //            bu.parent_unit
        //    )
        //    SELECT 
        //        parent.business_unit_id,
        //        parent.unit_name,
        //        COALESCE(SUM(child.hours_pt), 0) AS total_hours_pt,
        //        COALESCE(SUM(child.hours_npt), 0) AS total_hours_npt
        //    FROM Data parent
        //    LEFT JOIN Data child ON child.parent_unit = parent.business_unit_id
        //");

        //        if (!string.IsNullOrEmpty(businessUnitId))
        //        {
        //            sql.Append(@"
        //WHERE parent.business_unit_id = (
        //    SELECT TOP 1 parent_unit
        //    FROM vw_business_unit 
        //    WHERE id = @0
        //    ORDER BY parent_unit DESC
        //)", businessUnitId);
        //            Console.WriteLine($"Filtered by top-level region for business unit ID: {businessUnitId}");
        //        }
        //        else
        //        {
        //            sql.Append("WHERE parent.parent_unit IS NULL");
        //            Console.WriteLine("Admin access - retrieving all top-level parent units.");
        //        }


        //        // Add GROUP BY and ORDER BY clauses
        //        sql.Append(@"
        //    GROUP BY 
        //        parent.business_unit_id, 
        //        parent.unit_name
        //    ORDER BY parent.unit_name;
        //");

        //        // Execute the query and fetch results
        //        result.Data = this._services.db.Fetch<chart_region>(sql);

        //        if (result.Data == null || result.Data.Count == 0)
        //        {
        //            result.Status.Success = true;
        //            result.Status.Message = "No data found.";
        //            result.Data = new List<chart_region>();
        //            Console.WriteLine("No data found.");
        //        }
        //        else
        //        {
        //            result.Status.Success = true;
        //            result.Status.Message = $"Fetched {result.Data.Count} records.";
        //            Console.WriteLine($"Fetched {result.Data.Count} records.");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        Console.WriteLine($"Error occurred: {ex.Message}");
        //        result.Status.Success = false;
        //        result.Status.Message = $"An error occurred: {ex.Message}";
        //    }
        //    return result;
        //}

        public ApiResponse<List<chart_region>> getchartRegion(string businessUnitId = null)
        {
            var result = new ApiResponse<List<chart_region>>();
            try
            {
                Console.WriteLine("Executing SQL query for region data.");

                var sql = Sql.Builder;

                if (!string.IsNullOrEmpty(businessUnitId))
                {
                    // Non-admin: Filter hierarchy based on specified business unit and its relevant parents
                    sql.Append(@"
                WITH Data AS (
                    SELECT 
                        bu.id AS business_unit_id, 
                        bu.unit_name AS unit_name, 
                        bu.parent_unit,  
                        COALESCE(SUM(pt.total_hours), 0) AS hours_pt, 
                        COALESCE(SUM(npt.total_hours), 0) AS hours_npt
                    FROM 
                        vw_business_unit bu
                    LEFT JOIN 
                        dbo.well we ON bu.id = we.business_unit_id
                    LEFT JOIN 
                        drilling r ON r.well_id = we.id
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours
                        FROM drilling_operation_activity doa
                        INNER JOIN iadc AS i ON i.id = doa.iadc_id
                        CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s
                        WHERE doa.drilling_id = r.id AND i.type = 1
                    ) AS pt
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours
                        FROM drilling_operation_activity doa
                        INNER JOIN iadc AS i ON i.id = doa.iadc_id
                        CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s
                        WHERE doa.drilling_id = r.id AND i.type = 2
                    ) AS npt
                    GROUP BY 
                        bu.id, 
                        bu.unit_name, 
                        bu.parent_unit
                )
                SELECT 
                    parent.business_unit_id,
                    parent.unit_name,
                    COALESCE(SUM(child.hours_pt), 0) AS total_hours_pt,
                    COALESCE(SUM(child.hours_npt), 0) AS total_hours_npt
                FROM Data parent
                LEFT JOIN Data child ON child.parent_unit = parent.business_unit_id
                WHERE parent.business_unit_id = (
                    SELECT TOP 1 parent_unit
                    FROM vw_business_unit 
                    WHERE id = @0
                    ORDER BY parent_unit DESC
                ) OR parent.business_unit_id = @0
                GROUP BY 
                    parent.business_unit_id, 
                    parent.unit_name
                ORDER BY parent.unit_name;
            ", businessUnitId);
                    Console.WriteLine($"Filtered by hierarchy for business unit ID: {businessUnitId}");
                }
                else
                {
                    // Admin access - Retrieve only top-level parent units
                    sql.Append(@"
                WITH Data AS (
                    SELECT 
                        bu.id AS business_unit_id, 
                        bu.unit_name AS unit_name, 
                        bu.parent_unit,  
                        COALESCE(SUM(pt.total_hours), 0) AS hours_pt, 
                        COALESCE(SUM(npt.total_hours), 0) AS hours_npt
                    FROM 
                        vw_business_unit bu
                    LEFT JOIN 
                        dbo.well we ON bu.id = we.business_unit_id
                    LEFT JOIN 
                        drilling r ON r.well_id = we.id
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours
                        FROM drilling_operation_activity doa
                        INNER JOIN iadc AS i ON i.id = doa.iadc_id
                        CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s
                        WHERE doa.drilling_id = r.id AND i.type = 1
                    ) AS pt
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours
                        FROM drilling_operation_activity doa
                        INNER JOIN iadc AS i ON i.id = doa.iadc_id
                        CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s
                        WHERE doa.drilling_id = r.id AND i.type = 2
                    ) AS npt
                    GROUP BY 
                        bu.id, 
                        bu.unit_name, 
                        bu.parent_unit
                )
                SELECT 
                    parent.business_unit_id,
                    parent.unit_name,
                    COALESCE(SUM(child.hours_pt), 0) AS total_hours_pt,
                    COALESCE(SUM(child.hours_npt), 0) AS total_hours_npt
                FROM Data parent
                LEFT JOIN Data child ON child.parent_unit = parent.business_unit_id
                WHERE parent.parent_unit IS NULL
                GROUP BY 
                    parent.business_unit_id, 
                    parent.unit_name    
                ORDER BY parent.unit_name;
            ");
                    Console.WriteLine("Admin access - retrieving all top-level parent units.");
                }

                // Execute the query and fetch results
                result.Data = this._services.db.Fetch<chart_region>(sql);

                if (result.Data == null || result.Data.Count == 0)
                {
                    result.Status.Success = true;
                    result.Status.Message = "No data found.";
                    result.Data = new List<chart_region>();
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                Console.WriteLine($"Error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }
            return result;
        }


        public ApiResponse<List<DrillingSummary>> getActivityDate(DateTime startDate, DateTime endDate)
        {
            var result = new ApiResponse<List<DrillingSummary>>();
            try
            {
                Sql sql;

                // Check if the user is an admin
                if (this.dataContext.IsSystemAdministrator)
                {
                    // Admin query: No need to filter by business_unit_id
                    sql = Sql.Builder.Append(@"
                    SELECT 
                        MIN(id) AS id, 
                        MIN(well_id) AS well_id, 
                        MIN(field_id) AS field_id, 
                        MIN(drilling_date) AS start_drilling_date,
                        MAX(drilling_date) AS end_drilling_date,
                        COUNT(CASE WHEN well_classification = 'Exploration' THEN 1 END) AS exploration_count,
                        COUNT(CASE WHEN well_classification <> 'Exploration' AND well_classification <> 'Work_Over' THEN 1 END) AS exploitation_count,
                        COUNT(CASE WHEN well_classification = 'Work_Over' THEN 1 END) AS workover_count,
                        COUNT(*) AS total_well_classification
                    FROM 
                        vw_drilling
                    WHERE 
                        drilling_date BETWEEN @0 AND @1
                        AND well_classification IS NOT NULL", startDate, endDate);
                }
                else
                {
                    sql = Sql.Builder.Append(@"
                    SELECT 
                        MIN(d.id) AS id, 
                        we.business_unit_id AS business_unit_id,
                        MIN(d.well_id) AS well_id, 
                        MIN(d.field_id) AS field_id, 
                        MIN(d.drilling_date) AS start_drilling_date,
                        MAX(d.drilling_date) AS end_drilling_date,
                        COUNT(CASE WHEN d.well_classification = 'Exploration' THEN 1 END) AS exploration_count,
                        COUNT(CASE WHEN d.well_classification <> 'Exploration' AND d.well_classification <> 'Work_Over' THEN 1 END) AS exploitation_count,
                        COUNT(CASE WHEN d.well_classification = 'Work_Over' THEN 1 END) AS workover_count,
                        COUNT(*) AS total_well_classification
                    FROM 
                        vw_drilling d
                    JOIN 
                        vw_well we ON we.id = d.well_id
                    WHERE 
                        d.drilling_date BETWEEN @0 AND @1
                        AND d.well_classification IS NOT NULL
                        AND we.business_unit_id = @2", startDate, endDate, this.dataContext.PrimaryBusinessUnitId);

                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Grouping logic, if needed
                if (!this.dataContext.IsSystemAdministrator)
                {
                    // Non-admins require grouping by business_unit_id
                    sql.Append("GROUP BY we.business_unit_id");
                }

                // Fetch the data
                result.Data = this._services.db.Fetch<DrillingSummary>(sql);
                result.Status.Success = true;
                result.Status.Message = "Data fetched successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = $"Error fetching drilling data: {ex.Message}";
            }

            return result;
        }


        // Zona
        public ApiResponse<List<zona>> getZona(string parentId)
        {
            var result = new ApiResponse<List<zona>>();
            try
            {
                Console.WriteLine("Executing SQL query for zone data.");

                if (string.IsNullOrWhiteSpace(parentId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid parentId.";
                    return result;
                }

                Console.WriteLine($"Parameters: parentId = {parentId}");

                var sql = Sql.Builder.Append(@"
                    SELECT 
                        bu.id AS business_unit_id,
                        bu.unit_name,
                        COALESCE(SUM(DISTINCT we.afe_cost), 0) AS cummulative_cost_plan,
                        COALESCE(SUM(daily.daily_cost), 0) AS cummulative_cost_actual,
	                    SUM(DISTINCT 
                            CASE 
                                WHEN we.planned_td > 0 THEN we.afe_cost / we.planned_td
                                ELSE 0 
                            END) AS cummulative_cost_plan_ft,
                        SUM(DISTINCT 
                            CASE 
                                WHEN dd.measured_depth > 0 THEN COALESCE(p.total_cost, 0) / COALESCE(dd.measured_depth, 0)
                                ELSE 0 
                            END) AS cummulative_cost_actual_ft,
                        bu.parent_unit,
                        CASE 
                            WHEN COALESCE(SUM(d.depth), 0) > 0 THEN 
                            ((COALESCE(SUM(DISTINCT we.afe_cost), 0) - COALESCE(SUM(daily.daily_cost), 0)) / COALESCE(SUM(d.depth), 0))
                            ELSE 0 
                        END AS cost_ft_persen
                    FROM 
                        business_unit bu
                    LEFT JOIN 
                        vw_well we ON bu.id = we.business_unit_id 
                    LEFT JOIN 
                        drilling r ON r.well_id = we.id
                    LEFT JOIN 
                        tvd_plan d ON d.well_id = we.id
                    OUTER APPLY 
                        (SELECT COALESCE(SUM(daily_cost), 0) AS daily_cost 
                         FROM [dbo].[udf_get_daily_cost](r.id)) AS daily
                    OUTER APPLY (
                        SELECT 
                            COALESCE(SUM(cd.unit_price / cd.current_rate_value * dc.unit), 0) AS total_cost
                        FROM 
                            drilling d
                        INNER JOIN 
                            daily_cost dc ON dc.drilling_id = d.id
                        INNER JOIN 
                            afe_contract ac ON ac.id = dc.afe_contract_id
                        INNER JOIN 
                            contract_detail cd ON cd.id = ac.contract_detail_id
                        WHERE 
                            d.well_id = we.id
                    ) AS p
                    OUTER APPLY (
                        SELECT TOP 1 
                            dd.measured_depth
                        FROM 
                            drilling_deviation dd
                        INNER JOIN 
                            drilling d ON d.id = dd.drilling_id
                        WHERE 
                            d.well_id = we.id
                        ORDER BY 
                            d.drilling_date DESC, 
                            dd.created_by DESC, 
                            dd.seq DESC
                    ) AS dd
                    WHERE 
                        bu.parent_unit = @0", parentId);

                if (!this.dataContext.IsSystemAdministrator)
                {
                    //sql.Append("AND bu.id = @0", this.dataContext.PrimaryBusinessUnitId);  // Use @1 for the second parameter
                    sql.Append(" AND (bu.id = @0 OR bu.parent_unit = @0)", this._services.dataContext.PrimaryBusinessUnitId);
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                    GROUP BY 
                        bu.id, bu.unit_name, bu.parent_unit 
                    ORDER BY 
                        bu.unit_name ASC");

                // Execute the query
                result.Data = this._services.db.Fetch<zona>(sql);

                // Check if data was found
                if (result.Data == null || !result.Data.Any())
                {
                    result.Status.Success = true;  // Optionally set success to true for empty results
                    result.Status.Message = "No zone data found.";
                    result.Data = new List<zona>(); // Return an empty list instead of null
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} zone records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                // Handle and log exceptions
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        public ApiResponse<List<chart_zona>> getchartZona(string parentId)
        {
            var result = new ApiResponse<List<chart_zona>>();
            try
            {
                Console.WriteLine("Executing SQL query for region data.");

                // Membuat query dasar
                var sql = Sql.Builder.Append(@"
                        SELECT 
                            bu.id AS business_unit_id,
                            bu.unit_name,
                            bu.parent_unit,
                            COALESCE(SUM(d.current_depth_md), 0) AS depth_actual,
                            COALESCE(SUM(tvd_plan.depth), 0) AS depth_plan
                        FROM 
                            business_unit bu
                        LEFT JOIN dbo.well we ON we.business_unit_id = bu.id
                        LEFT JOIN dbo.tvd_plan tvd_plan ON tvd_plan.well_id = we.id
                        LEFT JOIN dbo.drilling d ON d.well_id = we.id
                        WHERE 
                            bu.parent_unit = @0", parentId);

                // Filter untuk non-admin berdasarkan PrimaryBusinessUnitId
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND bu.id = @0", this.dataContext.PrimaryBusinessUnitId);
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Tambahkan GROUP BY dan ORDER BY di akhir query
                sql.Append(@"
                    GROUP BY 
                        bu.id, bu.unit_name, bu.parent_unit
                    ORDER BY 
                        bu.unit_name ASC
                ");

                // Eksekusi query
                result.Data = this._services.db.Fetch<chart_zona>(sql);

                // Cek apakah ada data yang dikembalikan
                if (result.Data == null || result.Data.Count == 0)
                {
                    result.Status.Success = false;
                    result.Status.Message = "Data tidak ditemukan.";
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                // Logging error dan update status result
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        public ApiResponse<List<chart_region>> ptNptZona(string parentId)
        {
            var result = new ApiResponse<List<chart_region>>();
            try
            {
                Console.WriteLine("Executing SQL query for region data.");

                // Membuat query dasar
                var sql = Sql.Builder.Append(@"
                    SELECT 
                        vw.business_unit_id,                   
                        vw.unit_name,                          
                        MIN(r.well_id) AS well_id,             
                        SUM(pt.total_hours) AS total_hours_pt, 
                        SUM(npt.total_hours) AS total_hours_npt, 
                        SUM(pt.total_hours + npt.total_hours) AS total_hours, 
                        MIN(vw.well_name) AS well_name,
                        bu.parent_unit         
                    FROM drilling r
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours
                        FROM drilling_operation_activity doa
                        INNER JOIN iadc AS i ON i.id = doa.iadc_id
                        CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s
                        WHERE doa.drilling_id = r.id AND i.type = 1
                    ) AS pt
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours
                        FROM drilling_operation_activity doa
                        INNER JOIN iadc AS i ON i.id = doa.iadc_id
                        CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s
                        WHERE doa.drilling_id = r.id AND i.type = 2
                    ) AS npt
                    INNER JOIN vw_well vw ON r.well_id = vw.id
                    INNER JOIN dbo.business_unit bu on vw.business_unit_id = bu.id
                    WHERE bu.parent_unit = @0", parentId);

                // Filter untuk non-admin berdasarkan PrimaryBusinessUnitId
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND bu.id = @0", this.dataContext.PrimaryBusinessUnitId);
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Tambahkan GROUP BY dan ORDER BY di akhir query
                sql.Append(@"
                    GROUP BY 
                        vw.business_unit_id, 
                        vw.unit_name,
                        bu.parent_unit
                    ORDER BY vw.unit_name");

                // Eksekusi query
                result.Data = this._services.db.Fetch<chart_region>(sql);

                // Cek apakah ada data yang dikembalikan
                if (result.Data == null || result.Data.Count == 0)
                {
                    result.Status.Success = false;
                    result.Status.Message = "Data tidak ditemukan.";
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                // Logging error dan update status result
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        // Field
        public ApiResponse<List<dashboard_field>> getField(string unitId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            try
            {
                Console.WriteLine("Executing SQL query for field data.");

                // Validate parameters
                if (string.IsNullOrWhiteSpace(unitId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid unitId.";
                    return result;
                }

                Console.WriteLine($"Parameters: unitId = {unitId}");

                // Build the SQL query
                var sql = Sql.Builder.Append(@"
                SELECT 
                    vbuf.id, 
                    vbuf.business_unit_id, 
                    vbuf.unit_name, 
                    vbuf.field_id, 
                    vbuf.field_name, 
                    w.owner_id,
                    COALESCE(SUM(DISTINCT w.afe_cost), 0) AS cummulative_cost_plan,
                    COALESCE(SUM(daily.daily_cost), 0) AS cummulative_cost_actual,
                    SUM(DISTINCT 
                        CASE 
                            WHEN w.planned_td > 0 THEN w.afe_cost / w.planned_td
                            ELSE 0 
                        END) AS cummulative_cost_plan_ft,
                    SUM(DISTINCT 
                        CASE 
                            WHEN dd.measured_depth > 0 THEN COALESCE(p.total_cost, 0) / COALESCE(dd.measured_depth, 0)
                            ELSE 0 
                        END) AS cummulative_cost_actual_ft,
                    CASE 
                        WHEN COALESCE(SUM(tvd_plan.depth), 0) > 0 THEN 
                            ((COALESCE(SUM(DISTINCT w.afe_cost), 0) - COALESCE(SUM(daily.daily_cost), 0)) / COALESCE(SUM(tvd_plan.depth), 0))
                        ELSE 0 
                    END AS cost_ft_persen
                FROM 
                    vw_business_unit_field vbuf
                LEFT JOIN 
                    vw_well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                LEFT JOIN 
                    tvd_plan ON tvd_plan.well_id = w.id
                LEFT JOIN 
                    drilling d ON d.well_id = w.id
                OUTER APPLY 
                    (SELECT COALESCE(SUM(daily_cost), 0) AS daily_cost 
                     FROM [dbo].[udf_get_daily_cost](d.id)) AS daily
                OUTER APPLY (
                    SELECT 
                        COALESCE(SUM(cd.unit_price / cd.current_rate_value * dc.unit), 0) AS total_cost
                    FROM 
                        drilling d
                    INNER JOIN 
                        daily_cost dc ON dc.drilling_id = d.id
                    INNER JOIN 
                        afe_contract ac ON ac.id = dc.afe_contract_id
                    INNER JOIN 
                        contract_detail cd ON cd.id = ac.contract_detail_id
                    WHERE 
                        d.well_id = w.id
                ) AS p
                OUTER APPLY (
                    SELECT TOP 1 
                        dd.measured_depth
                    FROM 
                        drilling_deviation dd
                    INNER JOIN 
                        drilling d ON d.id = dd.drilling_id
                    WHERE 
                        d.well_id = w.id
                    ORDER BY 
                        d.drilling_date DESC, 
                        dd.created_by DESC, 
                        dd.seq DESC
                ) AS dd
                WHERE 
                    vbuf.business_unit_id = @0", unitId);

                // Apply filter for non-administrative users
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND vbuf.business_unit_id = @0", this.dataContext.PrimaryBusinessUnitId);  // Use @1 for non-admin users
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                    GROUP BY 
                        vbuf.id, 
                        vbuf.business_unit_id, 
                        vbuf.unit_name, 
                        vbuf.field_id, 
                        vbuf.field_name, 
                        w.owner_id
                    ORDER BY 
                        vbuf.field_name ASC");

                // Execute the query
                result.Data = this._services.db.Fetch<dashboard_field>(sql);

                // Check if data was found
                if (result.Data == null || !result.Data.Any())
                {
                    result.Status.Success = true;  // Optionally set success to true for empty results
                    result.Status.Message = "No field data found.";
                    result.Data = new List<dashboard_field>(); // Return an empty list instead of null
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} field records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                // Handle and log exceptions
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        public ApiResponse<List<chart_zona>> getchartField(string unitId)
        {
            var result = new ApiResponse<List<chart_zona>>();
            try
            {
                Console.WriteLine("Executing SQL query for region data.");

                // Build the SQL query
                var sql = Sql.Builder.Append(@"
                WITH PlannedDaysCTE AS (
                    SELECT 
                        w.business_unit_id,
                        w.field_id,
                        SUM(w.planned_days) AS total_planned_days
                    FROM well w
                    WHERE w.business_unit_id = @0
                    GROUP BY w.business_unit_id, w.field_id
                ),
                ActualDaysCTE AS (
                    SELECT 
                        w.business_unit_id,
                        w.field_id,
                        COUNT(d.id) AS total_actual_days 
                    FROM drilling d
                    LEFT JOIN well w ON d.well_id = w.id
                    WHERE w.business_unit_id = @0
                    GROUP BY w.business_unit_id, w.field_id
                )
                SELECT 
                    vbuf.id,    
                    vbuf.business_unit_id, 
                    vbuf.unit_name, 
                    vbuf.field_id, 
                    vbuf.field_name, 
                    COALESCE(SUM(d.current_depth_md), 0) AS depth_actual,
                    COALESCE(SUM(tvd_plan.depth), 0) AS depth_plan,
                    COALESCE(PlannedDaysCTE.total_planned_days, 0) AS planned_days,
                    COALESCE(ActualDaysCTE.total_actual_days, 0) AS actual_days, 
                    COALESCE(SUM(DISTINCT w.afe_cost), 0) AS plan_cost,
                    COALESCE(SUM(daily.daily_cost), 0) AS actual_cost
                FROM 
                    vw_business_unit_field vbuf
                LEFT JOIN 
                    PlannedDaysCTE ON vbuf.business_unit_id = PlannedDaysCTE.business_unit_id AND vbuf.field_id = PlannedDaysCTE.field_id
                LEFT JOIN 
                    ActualDaysCTE ON vbuf.business_unit_id = ActualDaysCTE.business_unit_id AND vbuf.field_id = ActualDaysCTE.field_id
                LEFT JOIN 
                    well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                LEFT JOIN 
                    drilling d ON d.well_id = w.id
                OUTER APPLY 
                    (SELECT COALESCE(SUM(daily_cost), 0) AS daily_cost 
                     FROM [dbo].[udf_get_daily_cost](d.id)) AS daily
                LEFT JOIN 
                    tvd_plan tvd_plan ON tvd_plan.well_id = w.id
                WHERE 
                    vbuf.business_unit_id = @0", unitId);

                // Apply filter for non-administrative users
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND vbuf.business_unit_id = @0", this.dataContext.PrimaryBusinessUnitId);
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                    GROUP BY 
                        vbuf.id, 
                        vbuf.business_unit_id, 
                        vbuf.unit_name, 
                        vbuf.field_id, 
                        vbuf.field_name, 
                        PlannedDaysCTE.total_planned_days,
                        ActualDaysCTE.total_actual_days
                    ORDER BY 
                        vbuf.field_name ASC
                ");

                // Execute the query
                result.Data = this._services.db.Fetch<chart_zona>(sql);

                // Check if data was found
                if (result.Data == null || result.Data.Count == 0)
                {
                    result.Status.Success = false;
                    result.Status.Message = "Data tidak ditemukan.";
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                // Logging error and update status result
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        public ApiResponse<List<dashboard_field>> getpieField(string unitId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            try
            {
                Console.WriteLine("Executing SQL query for field data.");

                // Validate parameters
                if (string.IsNullOrWhiteSpace(unitId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid unitId.";
                    return result;
                }

                Console.WriteLine($"Parameters: unitId = {unitId}");

                // Build the SQL query
                var sql = Sql.Builder.Append(@"
                    SELECT 
                        vbuf.business_unit_id, 
                        vbuf.unit_name, 
                        MIN(doa.iadc_code) AS iadc_code,          
                        COUNT(DISTINCT d.well_id) AS well_count,  
                        SUM(DATEDIFF(HOUR, doa.operation_start_date, doa.operation_end_date)) AS total_duration_hours 
                    FROM 
                        vw_business_unit_field vbuf
                    LEFT JOIN 
                        well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                    LEFT JOIN 
                        vw_drilling d ON d.well_id = w.id
                    LEFT JOIN 
                        vw_drilling_operation_activity doa ON doa.drilling_id = d.id
                    WHERE 
                        doa.iadc_code is not null and vbuf.business_unit_id = @0", unitId);

                // Apply filter for non-administrative users
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND vbuf.business_unit_id = @0", this.dataContext.PrimaryBusinessUnitId);  // Use @1 for non-admin users
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                    GROUP BY 
                        vbuf.business_unit_id, 
                        vbuf.unit_name, 
                        doa.iadc_code
                    ORDER BY 
                        vbuf.unit_name ASC");

                // Execute the query
                result.Data = this._services.db.Fetch<dashboard_field>(sql);

                // Check if data was found
                if (result.Data?.Any() ?? false)
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} field records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = "No field data found.";
                    result.Data = new List<dashboard_field>(); // Return an empty list instead of null
                    Console.WriteLine("No data found.");
                }
            }
            catch (Exception ex)
            {
                // Handle and log exceptions
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        public ApiResponse<List<dashboard_field>> getnptField(string unitId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            try
            {
                Console.WriteLine("Executing SQL query for field data.");

                // Validate parameters
                if (string.IsNullOrWhiteSpace(unitId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid unitId.";
                    return result;
                }

                Console.WriteLine($"Parameters: unitId = {unitId}");

                // Build the SQL query
                var sql = Sql.Builder.Append(@"
                    WITH WellCounts AS (
                        SELECT 
                            vbuf.business_unit_id,
                            COUNT(DISTINCT w.id) AS finished_wellbore  
                        FROM 
                            vw_business_unit_field vbuf
                        LEFT JOIN 
                            well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                        WHERE 
                            w.well_name IS NOT NULL
                        GROUP BY 
                            vbuf.business_unit_id
                    )
                    SELECT 
                        vbuf.business_unit_id, 
                        vbuf.unit_name, 
	                    wc.finished_wellbore,  
                        SUM(COALESCE(npt.total_npt_days, 0)) AS total_npt_days, 
                        SUM(COALESCE(operation.total_operation_days, 0)) AS total_operation_days 
                    FROM 
                        vw_business_unit_field vbuf
                    INNER JOIN 
                        well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                    INNER JOIN 
                        vw_drilling d ON d.well_id = w.id
                    INNER JOIN 
                        vw_drilling_operation_activity doa ON doa.drilling_id = d.id
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours) / 24.0, 0) AS total_npt_days  
                        FROM drilling_operation_activity doa_sub
                        INNER JOIN iadc AS i ON i.id = doa_sub.iadc_id
                        CROSS APPLY (
                            SELECT * 
                            FROM [dbo].[udf_iadc_interval_operation_activity](doa_sub.iadc_id, doa_sub.operation_start_date, doa_sub.operation_end_date)
                        ) AS s
                        WHERE doa_sub.drilling_id = d.id AND i.type = 2  
                    ) AS npt
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours) / 24.0, 0) AS total_operation_days  
                        FROM drilling_operation_activity doa_sub
                        INNER JOIN iadc AS i ON i.id = doa_sub.iadc_id
                        CROSS APPLY (
                            SELECT * 
                            FROM [dbo].[udf_iadc_interval_operation_activity](doa_sub.iadc_id, doa_sub.operation_start_date, doa_sub.operation_end_date)
                        ) AS s
                        WHERE doa_sub.drilling_id = d.id 
                    ) AS operation
                    LEFT JOIN 
                        WellCounts wc ON vbuf.business_unit_id = wc.business_unit_id 
                    WHERE 
                        vbuf.business_unit_id = @0", unitId);

                // Apply filter for non-administrative users
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND vbuf.business_unit_id = @0", this.dataContext.PrimaryBusinessUnitId);  // Use @1 for non-admin users
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                    GROUP BY vbuf.business_unit_id, wc.finished_wellbore, vbuf.unit_name");

                // Execute the query
                result.Data = this._services.db.Fetch<dashboard_field>(sql);

                // Check if data was found
                if (result.Data?.Any() ?? false)
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} field records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = "No field data found.";
                    result.Data = new List<dashboard_field>(); // Return an empty list instead of null
                    Console.WriteLine("No data found.");
                }
            }
            catch (Exception ex)
            {
                // Handle and log exceptions
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        // Well
        public ApiResponse<List<dashboard_field>> getWell(string FieldId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            try
            {
                Console.WriteLine("Executing SQL query for well data.");

                // Validate parameters
                if (string.IsNullOrWhiteSpace(FieldId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid FieldId.";
                    return result;
                }

                Console.WriteLine($"Parameters: FieldId = {FieldId}");

                // Build the SQL query
                var sql = Sql.Builder.Append(@"
                SELECT 
                    we.id,
                    we.business_unit_id as business_unit_id,
                    we.unit_name,
                    we.field_id,  
                    we.field_name,
                    we.well_name, 
                    we.owner_id,
                    we.afe_cost AS cummulative_cost_plan, 
                    COALESCE(SUM(daily.daily_cost), 0) AS cummulative_cost_actual,
                    CASE 
                        WHEN we.planned_td > 0 THEN we.afe_cost / we.planned_td
                        ELSE 0 
                    END AS cummulative_cost_plan_ft,
                    CASE 
                        WHEN dd.measured_depth > 0 THEN COALESCE(p.total_cost, 0) / COALESCE(dd.measured_depth, 0)
                        ELSE 0 
                    END AS cummulative_cost_actual_ft,
                    CASE 
                        WHEN COALESCE(SUM(tvd_plan.depth), 0) > 0 THEN 
                            ((COALESCE(SUM(DISTINCT we.afe_cost), 0) - COALESCE(SUM(daily.daily_cost), 0)) / COALESCE(SUM(tvd_plan.depth), 0))
                        ELSE 0 
                    END AS cost_ft_persen
                FROM 
                    vw_well we
                LEFT JOIN 
                    tvd_plan ON tvd_plan.well_id = we.id
                LEFT JOIN 
                    drilling d ON d.well_id = we.id
                OUTER APPLY 
                    (SELECT COALESCE(SUM(daily_cost), 0) AS daily_cost 
                     FROM [dbo].[udf_get_daily_cost](d.id)) AS daily
                OUTER APPLY (
                    SELECT 
                        COALESCE(SUM(cd.unit_price / cd.current_rate_value * dc.unit), 0) AS total_cost
                    FROM 
                        drilling d
                    INNER JOIN 
                        daily_cost dc ON dc.drilling_id = d.id
                    INNER JOIN 
                        afe_contract ac ON ac.id = dc.afe_contract_id
                    INNER JOIN 
                        contract_detail cd ON cd.id = ac.contract_detail_id
                    WHERE 
                        d.well_id = we.id
                ) AS p
                OUTER APPLY (
                    SELECT TOP 1 
                        dd.measured_depth
                    FROM 
                        drilling_deviation dd
                    INNER JOIN 
                        drilling d ON d.id = dd.drilling_id
                    WHERE 
                        d.well_id = we.id
                    ORDER BY 
                        d.drilling_date DESC, 
                        dd.created_by DESC, 
                        dd.seq DESC
                ) AS dd
                WHERE 
                    we.field_id = @0", FieldId);

                // Apply filter for non-administrative users
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND we.business_unit_id = @0", this.dataContext.PrimaryBusinessUnitId);
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                GROUP BY 
                    we.id, 
                    we.business_unit_id,
                    we.well_name, 
                    we.field_id, 
                    we.field_name, 
                    we.afe_cost, 
                    we.unit_name, 
                    we.daily_cost,
                    we.planned_td,
                    we.owner_id,
                    p.total_cost,
                    dd.measured_depth
                ORDER BY 
                    we.well_name ASC");

                // Execute the query
                result.Data = this._services.db.Fetch<dashboard_field>(sql);

                // Check if data was found
                if (result.Data == null || !result.Data.Any())
                {
                    result.Status.Success = true;
                    result.Status.Message = "No well data found.";
                    result.Data = new List<dashboard_field>(); // Return an empty list instead of null
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} well records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                // Handle and log exceptions
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        public ApiResponse<List<chart_zona>> getchartWell(string FieldId)
        {
            var result = new ApiResponse<List<chart_zona>>();
            try
            {
                Console.WriteLine("Executing SQL query for well data.");

                // Validate parameters
                if (string.IsNullOrWhiteSpace(FieldId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid FieldId.";
                    return result;
                }

                Console.WriteLine($"Parameters: FieldId = {FieldId}");

                // Build the SQL query
                var sql = Sql.Builder.Append(@"
                SELECT 
                    w.id, 
                    vbuf.business_unit_id, 
                    vbuf.unit_name, 
                    vbuf.field_id, 
                    vbuf.field_name, 
                    w.well_name,
                    vbuf.asset_id,
                    COALESCE(SUM(d.current_depth_md), 0) AS depth_actual,
                    COALESCE(SUM(tvd_plan.depth), 0) AS depth_plan,
                    COALESCE(w.planned_days, 0) AS planned_days,
                    COALESCE(act_days.total_days, 0) AS actual_days,
                    w.afe_cost AS plan_cost,
                    COALESCE(SUM(daily.daily_cost), 0) AS actual_cost
                FROM 
                    vw_business_unit_field vbuf
                LEFT JOIN 
                    well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                LEFT JOIN 
                    drilling d ON d.well_id = w.id
                OUTER APPLY 
                    (SELECT COALESCE(SUM(daily_cost), 0) AS daily_cost 
                     FROM [dbo].[udf_get_daily_cost](d.id)) AS daily
                LEFT JOIN 
                    tvd_plan tvd_plan ON tvd_plan.well_id = w.id
                OUTER APPLY (
                    SELECT COUNT(d2.id) AS total_days
                    FROM drilling d2
                    WHERE d2.well_id = w.id
                ) AS act_days
                WHERE 
                    w.well_name is not null AND
                    vbuf.field_id = @0", FieldId);

                // Apply filter for non-administrative users
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND vbuf.business_unit_id = @0", this.dataContext.PrimaryBusinessUnitId);
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                    GROUP BY 
                        w.id, 
                        vbuf.business_unit_id, 
                        vbuf.unit_name, 
                        vbuf.field_id, 
                        vbuf.field_name, 
                        w.well_name,
                        vbuf.asset_id,
                        w.planned_days,
	                    w.afe_cost,
                        act_days.total_days
                    ORDER BY 
                        w.well_name ASC
                ");

                // Execute the query
                result.Data = this._services.db.Fetch<chart_zona>(sql);

                // Check if data was found
                if (result.Data == null || result.Data.Count == 0)
                {
                    result.Status.Success = false;
                    result.Status.Message = "Data tidak ditemukan.";
                    Console.WriteLine("No data found.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
            }
            catch (Exception ex)
            {
                // Logging error and update status result
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        public ApiResponse<List<dashboard_field>> getpieWell(string FieldId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            try
            {
                Console.WriteLine("Executing SQL query for field data.");

                // Validate parameters
                if (string.IsNullOrWhiteSpace(FieldId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid FieldId.";
                    return result;
                }

                Console.WriteLine($"Parameters: FieldId = {FieldId}");

                // Build the SQL query
                var sql = Sql.Builder.Append(@"
                    SELECT 
                        vbuf.business_unit_id, 
                        vbuf.unit_name, 
                        vbuf.field_name,
	                    w.well_name,
	                    vbuf.field_id, 
                        MIN(doa.iadc_code) AS iadc_code,          
                        COUNT(DISTINCT d.well_id) AS well_count,  
                        SUM(DATEDIFF(HOUR, doa.operation_start_date, doa.operation_end_date)) AS total_duration_hours 
                    FROM 
                        vw_business_unit_field vbuf
                    LEFT JOIN 
                        well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                    LEFT JOIN 
                        vw_drilling d ON d.well_id = w.id
                    LEFT JOIN 
                        vw_drilling_operation_activity doa ON doa.drilling_id = d.id
                    WHERE 
                        w.well_name is not null AND doa.iadc_code is not null and 
                        vbuf.field_id = @0", FieldId);

                // Apply filter for non-administrative users
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND vbuf.business_unit_id = @0", this.dataContext.PrimaryBusinessUnitId);  // Use @1 for non-admin users
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                    GROUP BY 
                        vbuf.business_unit_id, 
                        vbuf.unit_name,
                        vbuf.field_name, 
                        w.well_name,
                        vbuf.field_id,
                        doa.iadc_code
                    ORDER BY 
                        w.well_name ASC");

                // Execute the query
                result.Data = this._services.db.Fetch<dashboard_field>(sql);

                // Check if data was found
                if (result.Data?.Any() ?? false)
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} field records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = "No field data found.";
                    result.Data = new List<dashboard_field>(); // Return an empty list instead of null
                    Console.WriteLine("No data found.");
                }
            }
            catch (Exception ex)
            {
                // Handle and log exceptions
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }

        public ApiResponse<List<dashboard_field>> getnptWell(string FieldId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            try
            {
                Console.WriteLine("Executing SQL query for field data.");

                // Validate parameters
                if (string.IsNullOrWhiteSpace(FieldId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid FieldId.";
                    return result;
                }

                Console.WriteLine($"Parameters: FieldId = {FieldId}");

                // Build the SQL query
                var sql = Sql.Builder.Append(@"
                    WITH WellCounts AS (
                        SELECT 
                            vbuf.field_id,
                            COUNT(DISTINCT w.id) AS finished_wellbore  
                        FROM 
                            vw_business_unit_field vbuf
                        LEFT JOIN 
                            well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                        WHERE 
                            w.well_name IS NOT NULL
                        GROUP BY 
                            vbuf.field_id
                    )
                    SELECT 
                        vbuf.business_unit_id, 
                        vbuf.unit_name, 
                        vbuf.field_name,
                        vbuf.field_id, 
                        wc.finished_wellbore,  
	                    COALESCE(SUM(npt.total_npt_days), 0) AS total_npt_days,  
                        COALESCE(SUM(operation.total_operation_days), 0) AS total_operation_days  
                    FROM 
                        vw_business_unit_field vbuf
                    INNER JOIN 
                        well w ON vbuf.business_unit_id = w.business_unit_id AND vbuf.field_id = w.field_id
                    INNER JOIN 
                        vw_drilling d ON d.well_id = w.id
                    INNER JOIN 
                        vw_drilling_operation_activity doa ON doa.drilling_id = d.id
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours) / 24.0, 0) AS total_npt_days  
                        FROM drilling_operation_activity doa_sub
                        INNER JOIN iadc AS i ON i.id = doa_sub.iadc_id
                        CROSS APPLY (
                            SELECT * 
                            FROM [dbo].[udf_iadc_interval_operation_activity](doa_sub.iadc_id, doa_sub.operation_start_date, doa_sub.operation_end_date)
                        ) AS s
                        WHERE doa_sub.drilling_id = d.id AND i.type = 2  
                    ) AS npt
                    CROSS APPLY (
                        SELECT COALESCE(SUM(s.interval_hours) / 24.0, 0) AS total_operation_days  
                        FROM drilling_operation_activity doa_sub
                        INNER JOIN iadc AS i ON i.id = doa_sub.iadc_id
                        CROSS APPLY (
                            SELECT * 
                            FROM [dbo].[udf_iadc_interval_operation_activity](doa_sub.iadc_id, doa_sub.operation_start_date, doa_sub.operation_end_date)
                        ) AS s
                        WHERE doa_sub.drilling_id = d.id 
                    ) AS operation
                    LEFT JOIN 
                        WellCounts wc ON vbuf.field_id = wc.field_id 
                    WHERE 
                        vbuf.field_id = @0", FieldId);

                // Apply filter for non-administrative users
                if (!this.dataContext.IsSystemAdministrator)
                {
                    sql.Append("AND vbuf.business_unit_id = @0", this.dataContext.PrimaryBusinessUnitId);  // Use @1 for non-admin users
                    Console.WriteLine($"Filtered by business unit ID: {this.dataContext.PrimaryBusinessUnitId}");
                }

                // Add GROUP BY and ORDER BY clauses
                sql.Append(@"
                    GROUP BY 
                        vbuf.business_unit_id, 
                        vbuf.unit_name,
                        vbuf.field_name, 
                        vbuf.field_id,
                        wc.finished_wellbore
                    ORDER BY 
                        vbuf.field_name ASC");

                // Execute the query
                result.Data = this._services.db.Fetch<dashboard_field>(sql);

                // Check if data was found
                if (result.Data?.Any() ?? false)
                {
                    result.Status.Success = true;
                    result.Status.Message = $"Fetched {result.Data.Count} field records.";
                    Console.WriteLine($"Fetched {result.Data.Count} records.");
                }
                else
                {
                    result.Status.Success = true;
                    result.Status.Message = "No field data found.";
                    result.Data = new List<dashboard_field>(); // Return an empty list instead of null
                    Console.WriteLine("No data found.");
                }
            }
            catch (Exception ex)
            {
                // Handle and log exceptions
                appLogger.Error(ex);
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            return result;
        }


    }
}
