﻿using CommonTools.Helper;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess.ViewModels;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class BhaComponentServicesNew
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();

        public Node<Base19Format> Bs19FormatByBhaComp()
        {
            Node<Base19Format> data = new Node<Base19Format>(null);
            try
            {
                var service = new BhaComponentRepositoryNew();
                var bha = service.ListBha();

                var afeLineParents = bha.ToList();
                
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return data;
        }
    }
}
