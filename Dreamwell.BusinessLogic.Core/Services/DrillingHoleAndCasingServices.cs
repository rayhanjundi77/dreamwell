﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingHoleAndCasingServices : DrillingHoleAndCasingRepository
    {
        public DrillingHoleAndCasingServices(DataContext dataContext) : base(dataContext) { }

        //public ApiResponse<List<vw_drilling_hole_and_casing>> getHoleAndCasing(string drillingId)
        //{
        //    var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
        //    try
        //    {
        //        var sql = Sql.Builder.Append(@"
        //                    SELECT dh.id, dh.hole_and_casing_id, dh.hole_val, dh.casing_val, dh.seq, dh.lot_fit, dh.lot_fit_val, dh.cement_val, dh.hole_name, dh.casing_name 
        //                    FROM (
        //                      SELECT id = null, hole_and_casing_id = r.id, hole_val = 0, casing_val = 0, seq = NULL, lot_fit = NULL, lot_fit_val = 0, cement_val = 0, r.created_on, r.hole_name, r.casing_name
        //                      FROM hole_and_casing r
        //                      LEFT OUTER JOIN drilling_hole_and_casing AS d ON r.id = d.hole_and_casing_id
        //                      WHERE r.id NOT IN (
        //                        SELECT i.hole_and_casing_id
        //                        FROM drilling_hole_and_casing i
        //                        WHERE i.drilling_id = @0
        //                      )
        //                      --ORDER BY r.hole_name DESC
        //                      UNION
        //                      SELECT d.id, d.hole_and_casing_id, d.hole_val, d.casing_val, d.seq, d.lot_fit, d.lot_fit_val, d.cement_val, r.created_on, r.hole_name, r.casing_name
        //                      FROM hole_and_casing r
        //                      LEFT OUTER JOIN drilling_hole_and_casing AS d ON r.id = d.hole_and_casing_id
        //                      WHERE r.id IN (
        //                        SELECT i.hole_and_casing_id
        //                        FROM drilling_hole_and_casing i
        //                        WHERE i.drilling_id = @0
        //                      )
        //                    ) AS dh
        //                    ORDER BY dh.created_on ASC ", drillingId);
        //        var item = this._services.db.Fetch<vw_drilling_hole_and_casing>(sql);
        //        result.Data = item;
        //        result.Status.Success = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        result.Status.Success = false;
        //        result.Status.Message = ex.Message;
        //    }
        //    return result;
        //}

        public ApiResponse<List<vw_drilling_hole_and_casing>> getDetailId(string recordId)
        {
            var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
            try
            {
                var Qry = Sql.Builder.Append(@"SELECT * FROM vw_drilling_hole_and_casing WHERE id = @0", recordId);
                result.Data = this._services.db.Fetch<vw_drilling_hole_and_casing>(Qry);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_hole_and_casing>> getListMulticasing(string recordId)
        {
            var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
            try
            {
                var Qry = Sql.Builder.Append(@"select dh.id, dh.casing_val ,ha.casing_name from drilling_hole_and_casing dh
                                            join well_hole_and_casing w on dh.well_hole_and_casing_id = w.id
                                            join hole_and_casing ha on w.hole_and_casing_id = ha.id
                                            where dh.well_hole_and_casing_id =@0
                                            and dh.multicasing =@1", recordId,1);
                result.Data = this._services.db.Fetch<vw_drilling_hole_and_casing>(Qry);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }


        public ApiResponse<drilling_hole_and_casing> New(string wellId)
        {
            var result = new ApiResponse<drilling_hole_and_casing>();
            var service_wellHoleCasing = new WellHoleAndCasingServices(_services.dataContext);
            try
            {
                #region Validation:: is Hole and Casing on Well
                var wellHoleCasingRecord = service_wellHoleCasing.GetFirstOrDefault(" AND well_id = @0 ", wellId);
                if (wellHoleCasingRecord == null)
                    throw new Exception("This well doesn't have any hole and casing records. Please create hole and casing on Well.");
                #endregion

                var isNew = false;
                var newRecord = new drilling_hole_and_casing();
                var existRecord = this.GetFirstOrDefault(" AND well_id = @0 AND is_locked = 1 ", wellId);

                if (existRecord == null)
                {
                    newRecord = new drilling_hole_and_casing();
                    newRecord.is_locked = true;
                    result.Status.Success = SaveEntity(newRecord, ref isNew, (r) => newRecord.id = r.id);
                }
                else
                {
                    existRecord.is_locked = false;
                    result.Status.Success = this.SaveEntity(existRecord, ref isNew, (r) => existRecord.id = r.id);

                    newRecord = new drilling_hole_and_casing();
                    newRecord.is_locked = true;
                    result.Status.Success = SaveEntity(newRecord, ref isNew, (r) => newRecord.id = r.id);
                }


                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                    result.Data = newRecord;
                }

            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<drilling> Save(DrillingHoleAndCasingViewModel records)
        {
            var result = new ApiResponse<drilling>();
            result.Status.Success = true;
            var service_drillingOperationActivity = new DrillingOperationActivityServices(_services.dataContext);
            var service_drilling = new DrillingServices(_services.dataContext);
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    //var existRecord = this.GetFirstOrDefault(" AND well_id = @0 AND is_locked = 1 ", wellId);

                    if (records == null || records.drilling_hole_and_casing == null || records.drilling_hole_and_casing.Count == 0)
                        throw new Exception("There is no hole and casing on drilling. Please choose hole and casing first.");

                    var sql = Sql.Builder.Append(@"DELETE FROM drilling_operation_activity WHERE drilling_id = @0", records.drilling_id);
                    this._services.db.Execute(sql);
                    foreach (var item in records.drilling_hole_and_casing)
                    {
                        //-- Update Drilling Hole and Casing Records. 
                        var newDrillingHoleRecord = new drilling_hole_and_casing();
                        newDrillingHoleRecord.InjectFrom(item);
                        newDrillingHoleRecord.well_id = records.well_id;
                        result.Status.Success &= this.SaveEntity(newDrillingHoleRecord, ref isNew, (r) => newDrillingHoleRecord.id = r.id);

                        if (item.drilling_operation_activity != null && item.drilling_operation_activity.Count > 0)
                        {
                            foreach (var activity in item.drilling_operation_activity)
                            {
                                var isNewActivity = false;
                                var newRecord = new drilling_operation_activity();
                                newRecord.InjectFrom(activity);
                                newRecord.drilling_id = records.drilling_id;
                                newRecord.drilling_hole_and_casing_id = item.id;
                                result.Status.Success &= service_drillingOperationActivity.SaveEntity(newRecord, ref isNewActivity, (r) => newRecord.id = r.id);
                            }
                        }
                        else
                        {
                            result.Status.Success = false;
                            result.Status.Message = "Operation activity records cannot be emtpy. Please check your actitivy.";
                            return result;
                        }
                    }


                    var isNewDrilling = false;
                    var getLastOperationActivityRecord = records.drilling_hole_and_casing.LastOrDefault().drilling_operation_activity.LastOrDefault();
                    var drillingRecord = service_drilling.GetById(records.drilling_id);

                    var lastDrillingRecord = service_drilling.GetAll(" AND well_id = @0 AND drilling_date < @1 ORDER BY drilling_date ASC", drillingRecord.well_id, drillingRecord.drilling_date).FirstOrDefault();

                    drillingRecord.current_depth_md = getLastOperationActivityRecord.depth;
                    drillingRecord.previous_depth_md = lastDrillingRecord == null ? 0 : lastDrillingRecord.current_depth_md;
                    drillingRecord.progress = drillingRecord.current_depth_md - drillingRecord.previous_depth_md;
                    result.Status.Success &= service_drilling.SaveEntity(drillingRecord, ref isNewDrilling, (r) => drillingRecord.id = r.id);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Successfully save operation activity records.";
                        result.Data = drillingRecord;
                    }

                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse SaveCasing(drilling_hole_and_casing record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = true;
                var idNew = Guid.NewGuid().ToString();
                record.id = idNew;

                var recordId = record.id;
                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    result.Status.Message = "Successfully created a new Casing";
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
