﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class IadcServices : IadcRepository
    {
        public IadcServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(iadc record)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var recordId = record.id;

                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {
                        var service_history = new IadcHistoryServices(_services.dataContext);
                        var isNewHistory = false;
                        var history = new iadc_history();
                        history.iadc_id = recordId;
                        history.activity = record.activity;
                        history.description = record.description;
                        history.iadc_code = record.iadc_code;
                        history.parent_id = record.parent_id;
                        history.phase = record.phase;
                        history.remark = record.remark;
                        history.task = record.task;
                        history.type = record.type;
                        if (isNew)
                        {
                            history.rev = 0;
                            result.Status.Message = "The data has been added.";
                        }
                        else
                        {
                            //var last = service_history.GetAll(" AND is_active=@0 AND iadc_id=@1 ORDER BY rev DESC ", true, recordId).SingleOrDefault();
                            //history.rev = last.rev + 1;
                            result.Status.Message = "The data has been updated.";
                        }
                        result.Status.Success = service_history.SaveEntity(history, ref isNewHistory, (r) => history.id = r.id);

                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public Page<vw_iadc> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 AND r.parent_id IS NULL ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            var result = base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
            return result;
        }

        public List<vw_iadc_analysis_npt> GetNPTDetailsByWellId(string wellId)
        {
            var result = new List<vw_iadc_analysis_npt>();
            try
            {
                var qry = Sql.Builder.Append(@"SELECT r.id,r.description,r.iadc_code,parent.iadc_code AS parent_code, m.total,m.interval 
                            FROM iadc r
                            INNER JOIN iadc parent ON r.parent_id=parent.id
                            OUTER APPLY (
                                SELECT COUNT(*) AS total,SUM(DATEDIFF(MINUTE, do.operation_start_date, do.operation_end_date) / 60.0) AS interval
                                FROM iadc i
                                INNER JOIN drilling_operation_activity do ON r.id = do.iadc_id
	                            INNER JOIN drilling_hole_and_casing dhc ON dhc.id = do.drilling_hole_and_casing_id

                                WHERE i.type = 2 AND i.id = r.id AND 
	                            dhc.well_id = @0
                            ) AS m
                            WHERE m.total>0
                            ORDER BY r.description ASC ", wellId);
                result = this._services.db.Fetch<vw_iadc_analysis_npt>(qry);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }

        public List<JSTreeResponse<vw_iadc>> GetIadc(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            List<JSTreeResponse<vw_iadc>> results = new List<JSTreeResponse<vw_iadc>>();
            try
            {
                var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 AND r.parent_id IS NULL ", _services.dataContext.OrganizationId);
                if (marvelDataSourceRequest.Filter != null)
                {
                    Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                    if (filterBuilder != null)
                        sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
                }

                //_services.db.Page<TEntityView>(page, itemsPerPage, sql);
                var data = base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
                if (data.Items.Count > 0 && data.Items != null)
                {
                    foreach (var item in data.Items)
                    {
                        var records = vw_iadc.Fetch(" WHERE parent_id=@0", item.id);
                        if (records.Count > 0)
                        {
                            item.Children = new List<vw_iadc>();
                            item.Children = records;
                        }
                    }
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<JSTreeResponse<iadc>> GetTreeIadc()
        {
            List<JSTreeResponse<iadc>> results = new List<JSTreeResponse<iadc>>();
            try
            {
                var data = iadc.Fetch(" WHERE organization_id=@0 AND is_active=1 AND parent_id IS NULL ORDER BY CASE IsNumeric(iadc_code) WHEN 1 THEN Replicate('0', 100 - Len(iadc_code)) + iadc_code ELSE iadc_code END", _services.dataContext.OrganizationId);
                List<string> childNodes = null;
                int row = 0;
                foreach (var item in data)
                {
                    var menu = new JSTreeResponse<iadc>();
                    if (row == 0)
                        menu.State.Opened = true;
                    var children = GetChildren(item.id, ref childNodes);
                    menu.Id = item.id;
                    menu.Text = item.iadc_code + " - " + item.description;
                    if (item.type == "1")
                        menu.Text += " (PT)";
                    else
                        menu.Text += " (NPT)";
                    menu.Type = item.type;
                    menu.Category = item.category;
                    menu.Icon = "fa fa-folder";
                    menu.Children = children;
                    results.Add(menu);
                    row++;
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<JSTreeResponse<iadc>> GetTreeNPTIadc()
        {
            List<JSTreeResponse<iadc>> results = new List<JSTreeResponse<iadc>>();
            try
            {
                var data = iadc.Fetch(" WHERE organization_id=@0 AND is_active=1 AND parent_id IS NULL ORDER BY CASE IsNumeric(iadc_code) WHEN 1 THEN Replicate('0', 100 - Len(iadc_code)) + iadc_code ELSE iadc_code END", _services.dataContext.OrganizationId);
                List<string> childNodes = null;
                int row = 0;
                foreach (var item in data)
                {
                    var menu = new JSTreeResponse<iadc>();
                    if (row == 0)
                        menu.State.Opened = true;
                    var children = GetChildrenNPT(item.id, ref childNodes);
                    menu.Id = item.id;
                    menu.Text = item.iadc_code + " - " + item.description;
                    if (item.type == "1")
                        menu.Text += " (PT)";
                    else
                        menu.Text += " (NPT)";
                    menu.Type = item.type;
                    menu.Category = item.category;
                    menu.Icon = "fa fa-folder";
                    menu.Children = children;
                    if (menu.Children.Count(r => r.Type == "2") > 0)
                        results.Add(menu);
                    row++;
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        protected List<JSTreeResponse<iadc>> GetChildren(String parentId, ref List<string> childNodes)
        {
            List<JSTreeResponse<iadc>> results = new List<JSTreeResponse<iadc>>();
            if (childNodes == null)
            {
                childNodes = new List<string>();
                childNodes.Add(parentId);
            }
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var data = iadc.Fetch(" WHERE parent_id=@0 ORDER BY CASE IsNumeric(iadc_code) WHEN 1 THEN Replicate('0', 100 - Len(iadc_code)) + iadc_code ELSE iadc_code END ASC", parentId);
                foreach (var item in data)
                {
                    if (childNodes.Contains(item.id)) continue;
                    var menu = new JSTreeResponse<iadc>();
                    menu.Id = item.id;
                    menu.Text = item.iadc_code + " - " + item.description;
                    if (item.type == "1")
                        menu.Text += " (PT)";
                    else
                        menu.Text += " (NPT)";
                    menu.Type = item.type;
                    menu.Category = item.category;
                    menu.Icon = "fa fa-folder";
                    menu.State = null;
                    var children = GetChildren(item.id, ref childNodes);
                    if (children != null)
                    {
                        menu.Children = children;
                    }
                    results.Add(menu);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        protected List<JSTreeResponse<iadc>> GetChildrenNPT(String parentId, ref List<string> childNodes)
        {
            List<JSTreeResponse<iadc>> results = new List<JSTreeResponse<iadc>>();
            if (childNodes == null)
            {
                childNodes = new List<string>();
                childNodes.Add(parentId);
            }
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var data = iadc.Fetch(" WHERE parent_id=@0 AND type = @1 ORDER BY CASE IsNumeric(iadc_code) WHEN 1 THEN Replicate('0', 100 - Len(iadc_code)) + iadc_code ELSE iadc_code END ASC", parentId, 2);
                foreach (var item in data)
                {
                    if (childNodes.Contains(item.id)) continue;
                    var menu = new JSTreeResponse<iadc>();
                    menu.Id = item.id;
                    menu.Text = item.iadc_code + " - " + item.description;
                    if (item.type == "1")
                        menu.Text += " (PT)";
                    else
                        menu.Text += " (NPT)";
                    menu.Type = item.type;
                    menu.Category = item.category;
                    menu.Icon = "fa fa-folder";
                    menu.State = null;
                    var children = GetChildrenNPT(item.id, ref childNodes);
                    if (children != null)
                    {
                        menu.Children = children;
                    }
                    results.Add(menu);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }
    }
}
