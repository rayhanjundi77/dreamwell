﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class IadcWIServices : IadcWIRepository
    {
        public IadcWIServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(iadcwi record)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var recordId = record.id;

                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                    if (result.Status.Success)
                    {
                      
                        if (isNew)
                        {
                            result.Status.Message = "The data has been added.";
                        }
                        else
                        {
                            //var last = service_history.GetAll(" AND is_active=@0 AND iadcwi_id=@1 ORDER BY rev DESC ", true, recordId).SingleOrDefault();
                            //history.rev = last.rev + 1;
                            result.Status.Message = "The data has been updated.";
                        }
                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public Page<vw_iadcwi> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append("AND r.is_active=1", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            var result = base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
            return result;
        }

    }
}
