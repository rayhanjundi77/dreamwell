﻿using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dreamwell.DataAccess.Models.Entity;
using Dreamwell.Infrastructure;
using CommonTools;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class CasingService
    {
        CasingRepository repo;
        HoleRepository repoHole;
        DataContext context;

        public CasingService(DataContext context)
        {
            repo = new CasingRepository(context);
            repoHole = new HoleRepository(context);
            this.context = context;
        }

        public async Task<ApiResponse> Get(string id)
        {
            var result = new ApiResponse();
            try
            {
                var casing = await repo.GetById(id);
                if(casing == null)
                {
                    throw new Exception("not found");
                }
                var hole = await repoHole.GetById(casing.hole_id);

                var casingResult = new CasingViewModel()
                {
                    id =casing.id,
                    name= casing.name,
                    hole_id=hole.id,
                    hole_name=hole.name,
                };
                result.Data = casingResult;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponsePage<Casing>> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var sb = new StringBuilder();
            if (marvelDataSourceRequest.Filter != null)
            {
                foreach (var filter in marvelDataSourceRequest.Filter.Filters)
                {
                    if (filter.Value == "")
                    {
                        continue;
                    }
                
                    foreach(var f in filter.Filters)
                    {
                        if (f.Operator == "eq")
                        {
                            sb.Append($" and {f.Field} = '{f.Value}'");
                        } 
                        else if(f.Operator == "contains" && f.Value != "")
                        {
                            sb.Append($" and {f.Field} like '%{f.Value}%'");
                        }
                    }
                }
            }
            sb.Append(" order by name asc ");
            var items = await repo.Get(sb.ToString());
            var result = new ApiResponsePage<Casing>();
            result.Items = items.ToList();
            result.TotalItems = items.Count();
            result.CurrentPage = 1;
            result.ItemsPerPage = 100;
            return result;
        }

        public async Task<ApiResponse> Save(Casing model, bool isNew)
        {
            var result = new ApiResponse();
            try
            {
                model = await repo.Save(model, isNew);
                result.Status.Success = model != null;
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = model;
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponse> Delete(string[] ids)
        {
            var result = new ApiResponse();
            try
            {
                result.Status.Success = await repo.Delete(ids);
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
