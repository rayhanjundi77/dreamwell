﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using System.Transactions;
using Omu.ValueInjecter;
using System.Collections.Generic;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;
using System.Linq;
using CommonTools.JSTree;
using Newtonsoft.Json;
using System.IO;
using System.Web;
using Dreamwell.DataAccess.Repository;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class LampiranServices : LampiranRepository
    {
        public LampiranServices(DataContext dataContext) : base(dataContext) { }
        public ApiResponse Save(tbl_pdf record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.well_final_report_id);
                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse SaveLampiran(HttpFileCollection files, string wellId, string wellFinal)
        {
            var result = new ApiResponse();

            try
            {
                var isNew = false;
                var record = new tbl_pdf
                {
                    well_id = wellId,
                    well_final_report_id = wellFinal
                };

                if (files["pdf_content_1"] != null && files["pdf_content_1"].ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        files["pdf_content_1"].InputStream.CopyTo(memoryStream);
                        record.pdf_content_1 = memoryStream.ToArray();
                    }
                }
                if (files["pdf_content_2"] != null && files["pdf_content_2"].ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        files["pdf_content_2"].InputStream.CopyTo(memoryStream);
                        record.pdf_content_2 = memoryStream.ToArray();
                    }
                }
                if (files["pdf_content_3"] != null && files["pdf_content_3"].ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        files["pdf_content_3"].InputStream.CopyTo(memoryStream);
                        record.pdf_content_3 = memoryStream.ToArray();
                    }
                }
                if (files["pdf_content_4"] != null && files["pdf_content_4"].ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        files["pdf_content_4"].InputStream.CopyTo(memoryStream);
                        record.pdf_content_4 = memoryStream.ToArray();
                    }
                }
                var recordId = record.well_final_report_id;
                if (string.IsNullOrEmpty(recordId))
                    record.id = CommonTools.GuidHash.ConvertToMd5HashGUID(record.well_final_report_id).ToString();

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.well_final_report_id);

                if (result.Status.Success)
                {
                    result.Status.Message = isNew ? "The data has been added." : "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
                System.Diagnostics.Debug.WriteLine("Error during SaveLampiran: " + ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }

            return result;
        }


        public bool CheckWellFinalReportExists(string wellFinalReportId)
        {
            var Qry = Sql.Builder.Append(@"SELECT * FROM tbl_pdf WHERE well_final_report_id = @0", wellFinalReportId);
            var result = this._services.db.Fetch<tbl_pdf>(Qry);
            if (result != null)
            {
                System.Diagnostics.Debug.WriteLine("Record found for well_final_report_id: " + wellFinalReportId);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("No record found for well_final_report_id: " + wellFinalReportId);
            }
            return result != null;
        }

        public ApiResponse<vw_tbl_pdf> GetDetailLampiran(string recordId)
        {
            var result = new ApiResponse<vw_tbl_pdf>();
            try
            {
                var Qry = Sql.Builder.Append(@"SELECT TOP 1 * FROM vw_tbl_pdf WHERE well_final_report_id = @0 order by created_on desc", recordId);
                result.Data = this._services.db.FirstOrDefault<vw_tbl_pdf>(Qry);
                if (result.Data != null)
                {
                    result.Status.Success = true;
                }
                else
                {
                    result.Status.Success = false;
                    result.Status.Message = "Record not found.";
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = "Error retrieving record: " + ex.Message;
            }
            return result;
        }

        public ApiResponse SaveData(tbl_pdf record, bool isNewRecord)
        {
            var result = new ApiResponse();
            try
            {
                // Jika record baru, buat ID unik jika belum ada ID
                if (isNewRecord && string.IsNullOrEmpty(record.id))
                {
                    record.id = Guid.NewGuid().ToString();
                }

                // Simpan atau perbarui record melalui metode SaveEntity
                result.Status.Success = SaveEntity(record, ref isNewRecord, (Action<tbl_pdf>)null);

                if (result.Status.Success)
                {
                    // Berikan pesan sukses berdasarkan apakah data baru atau diperbarui
                    result.Status.Message = isNewRecord ? "The data has been added." : "The data has been updated.";
                    result.Data = new { recordId = record.id };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = "Error saving pdf pdf: " + ex.Message;
            }
            return result;
        }

    }
}
