﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using CommonTools.UnitOfMeasurement;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ObjectUomMapServices : ObjectUOMMapRepository
    {
        public ObjectUomMapServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(object_uom_map record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                if (String.IsNullOrEmpty(recordId))
                {
                    record.id = CommonTools.GuidHash.ConvertToMd5HashGUID(record.object_name).ToString();
                    result.Status.Message = "The data has been added.";
                }
                else
                {
                    result.Status.Message = "The data has been updated.";
                }

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public DataTablesResponse GetListDataTables(DataTableRequest DataTableRequest, string uomCode)
        {
            var qry = Sql.Builder.Append(" WHERE r.is_active=@0 ANd r.uom_code=@0", true, uomCode);
            return base.GetListDataTables(DataTableRequest, qry);
        }

        public string ConvertToPercentageString(string value)
        {
            if (decimal.TryParse(value, out decimal decimalValue))
            {
                return $"{decimalValue:F2}%";
            }
            else
            {
                return "Invalid value";
            }
        }

    }

}

