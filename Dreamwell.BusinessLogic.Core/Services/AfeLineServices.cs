﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using NLog.LayoutRenderers;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class AfeLineServices : AfeLineRepository
    {
        public AfeLineServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(afe_line record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        
        public Page<vw_afe_line> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
        public Page<vw_afe_line> LookupByParent(MarvelDataSourceRequest marvelDataSourceRequest, string parentId)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 AND r.afe_line_parent_id=@1 ", _services.dataContext.OrganizationId, parentId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        public List<JSTreeResponse<afe_line>> GetTree()
        {
            List<JSTreeResponse<afe_line>> results = new List<JSTreeResponse<afe_line>>();
            try
            {
                //var data = afe_line.Fetch(" WHERE is_active=1 and organization_id=@0 and parent_id is null ORDER BY line asc ", _services.dataContext.OrganizationId);
                var data = afe_line.Fetch(" WHERE is_active=1 and organization_id=@0 ORDER BY line asc ", _services.dataContext.OrganizationId);
                //List<string> childNodes = null;
                int row = 0;
                int step = 1;
                var parents = data.Where(x => x.parent_id == null).OrderByDescending(x => x.description).ToList();
                foreach (var item in parents)
                {
                    var menu = new JSTreeResponse<afe_line>();
                    if (row == 0)
                        menu.State.Opened = true;

                    //var children = GetTree(item.id, ref childNodes,step);
                    menu.Id = item.id;
                    menu.Text = step++ + ". " + item.description;
                    menu.Icon = "fa fa-folder";
                    menu.Children = GetTree(data, item.id, ref step);
                    results.Add(menu);
                }
                appLogger.Debug(JsonConvert.SerializeObject(results));
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        protected List<JSTreeResponse<afe_line>> GetTree(List<afe_line> data, string parentId, ref int step)
        {
            List<JSTreeResponse<afe_line>> results = new List<JSTreeResponse<afe_line>>();
            var items = data.Where(item => item.parent_id == parentId).OrderBy(x => x.line).ToList();
            foreach(var item in items)
            {
                var menu = new JSTreeResponse<afe_line>();
                menu.Id = item.id;
                if((bool)item.is_number)
                {
                    menu.Text = step++ + ". " + item.description;
                }
                else
                {
                    menu.Text = item.description;
                }
                menu.Icon = "fa fa-folder";
                menu.Children = GetTree(data, item.id, ref step);
                results.Add(menu);
            }

            return results;
            //var menu = new JSTreeResponse<afe_line>();
            //if (string.IsNullOrEmpty(parentId)) return results;
            //var data = afe_line.Fetch(" WHERE parent_id=@0 ORDER BY line asc", parentId);
            //foreach (var item in data)
            //{
            //    if (childNodes.Contains(item.id)) continue;
            //    var menu = new JSTreeResponse<afe_line>();
            //    menu.Id = item.id;
            //    menu.Text = item.description + " [" + step + "]";
            //    menu.Icon = "fa fa-folder";
            //    menu.State = null;
            //    var children = GetTree(item.id, ref childNodes, step);
            //    if (children != null)
            //    {
            //        menu.Children = children;
            //    }
            //    step++;
            //    results.Add(menu);
            //}

            //return results;
        }

        //protected List<JSTreeResponse<afe_line>> GetTree(String parentId, ref List<string> childNodes,int step)
        //{
        //List<JSTreeResponse<afe_line>> results = new List<JSTreeResponse<afe_line>>();
        //if (childNodes == null)
        //{
        //    childNodes = new List<string>();
        //    childNodes.Add(parentId);
        //}
        //try     
        //{
        //    if (string.IsNullOrEmpty(parentId)) return results;
        //    var data = afe_line.Fetch(" WHERE parent_id=@0 ORDER BY line asc", parentId);
        //    foreach (var item in data)
        //    {
        //        if (childNodes.Contains(item.id)) continue;
        //        var menu = new JSTreeResponse<afe_line>();
        //        menu.Id = item.id;
        //        menu.Text = item.description + " [" + step + "]";
        //        menu.Icon = "fa fa-folder";
        //        menu.State = null;
        //        var children = GetTree(item.id, ref childNodes,step);
        //        if (children != null)
        //        {
        //            menu.Children = children;
        //        }
        //        step++;
        //        results.Add(menu);
        //    }
        //}
        //catch (Exception ex)
        //{
        //    appLogger.Error(ex);
        //    throw;
        //}
        //return results;
        //}


        public bool OrderTree(String recordId, String parentTargetId, int newOrder)
        {
            var result = false;

            try
            {
                _services.db.Execute(";EXEC usp_afe_line_order @@id = @0 , @@order_index = @1 ,@@parent_id = @2",
                    recordId, newOrder, parentTargetId);
                result = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }

    }
}
