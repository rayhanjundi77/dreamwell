﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using System.Collections.Generic;
using System.Transactions;
using Dreamwell.BusinessLogic.Core.CurvatureMethod;
using Omu.ValueInjecter;
using System.Linq;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class CostWirServices : CostWirRepository
    {
        private readonly DataContext _dataContext;

        public CostWirServices(DataContext dataContext) : base(dataContext)
        {

            _dataContext = dataContext;
        }

        public ApiResponse Save(cost_wir record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, this._services.dataContext.PrimaryTeamId, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_cost_wir>> GetByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_cost_wir>>();
            try
            {
                if (string.IsNullOrWhiteSpace(wellId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Well ID cannot be empty.";
                    return result;
                }

                var sql = Sql.Builder.Append(@"
                SELECT *
                FROM vw_cost_wir
                WHERE well_id = @0
                ORDER BY created_on DESC", wellId);


                result.Data = this._services.db.Fetch<vw_cost_wir>(sql);
                appLogger.Info($"Retrieved {result.Data.Count} records.");
                result.Status.Success = true;
                result.Status.Message = "Data retrieved successfully.";
            }
            catch (Exception ex)
            {
                appLogger.Error($"Error in GetByWellId: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"Error retrieving data: {ex.Message}";
            }
            return result;
        }
    }

}
