﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Omu.ValueInjecter;
using System.Dynamic;
using CommonTools.Helper;
using CommonTools.EeasyUI;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class AssetServices : AssetRepository
    {
        public AssetServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(asset record)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    string recordId = record.id;

                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "The data has been saved.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public List<dynamic> GetAsset(string countryId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                var sql = Sql.Builder.Append(asset.DefaultView);

                sql.Append("WHERE r.is_active=1 ");
                if (string.IsNullOrEmpty(countryId))
                {
                    //sql.Append("AND r.country_id=@0 ", countryId);
                }

                sql.Append("ORDER BY r.asset_name asc ");
                var data = _services.db.Fetch<vw_asset>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.asset_name;
                    itemList.NodeType = "ASSET";
                    itemList.Icon = "fal fa-compress-wide";
                    itemList.Children = true;
                    itemList.Data = new ExpandoObject();
                    itemList.Data = item;
                    results.Add(itemList);
                    row++;
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<dynamic> GetAsset(string countryId, string businessUnitId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                var sql = Sql.Builder.Append(@"select r.id, r.asset_code, r.asset_name, CASE  WHEN s.asset_id IS NULL THEN 0 ELSE 1 END AS is_active
                            from asset r
                            left join
                            (
	                            SELECT f.asset_id FROM dbo.business_unit_field uf 
	                            INNER JOIN dbo.field f ON ISNULL(uf.field_id,'')=f.id
	                            WHERE uf.business_unit_id=@0 GROUP BY f.asset_id
                            )
                            s ON r.id=s.asset_id
                            ORDER BY r.asset_name ASC ", businessUnitId);

                var data = _services.db.Fetch<dynamic>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.asset_name;
                    itemList.NodeType = "ASSET";
                    itemList.Icon = "fal fa-compress-wide";
                    //itemList.Children = true;
                    itemList.Data = new ExpandoObject();
                    itemList.Data = item;
                    itemList.State = new ExpandoObject();
                    if (Convert.ToBoolean(item.is_active) == true)
                    {
                        itemList.State.undetermined = item.is_active;
                        //itemList.State.Checked = item.is_active;
                    }

                    var children = GetFieldTreeByBusinessUnit(item.id, businessUnitId);
                    itemList.Children = children;



                    results.Add(itemList);
                    row++;
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<dynamic> GetAssetWithField(string countryId, string businessUnitId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                var sql = Sql.Builder.Append(@"SELECT r.id,r.asset_code,r.asset_name, is_active = 0
                        FROM dbo.asset as r 
                        WHERE r.is_active=1 AND r.id NOT IN (
	                        SELECT f.asset_id FROM dbo.business_unit_field uf 
	                        INNER JOIN dbo.field f ON uf.field_id=f.id
	                        WHERE uf.business_unit_id=@0 AND f.asset_id=r.id  GROUP BY f.asset_id)
                        UNION ALL
                        SELECT r.id,r.asset_code,r.asset_name, is_active = 1
                        FROM dbo.asset as r 
                        WHERE r.is_active=1 AND r.id IN (
	                        SELECT f.asset_id FROM dbo.business_unit_field uf 
	                        INNER JOIN dbo.field f ON uf.field_id=f.id
	                        WHERE uf.business_unit_id=@0 AND f.asset_id=r.id  GROUP BY f.asset_id)
                        ORDER BY r.asset_name ASC", businessUnitId);
                var data = _services.db.Fetch<asset>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.asset_name;
                    itemList.NodeType = "ASSET";
                    itemList.Icon = "fal fa-compress-wide";
                    //itemList.Children = true;
                    itemList.Data = new ExpandoObject();
                    itemList.Data = item;
                    itemList.State = new ExpandoObject();
                    if (item.is_active ?? false)
                    {
                        itemList.State.undetermined = item.is_active;
                        //itemList.State.Checked = item.is_active;
                    }

                    var children = GetFieldTreeByBusinessUnit(item.id, businessUnitId);
                    itemList.Children = children;



                    results.Add(itemList);
                    row++;
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<dynamic> GetAssetByBusinessUnit(string businessUnitId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                var sql = Sql.Builder.Append(@"
                        SELECT r.id,r.asset_code,r.asset_name, is_active = 1
                        FROM dbo.asset as r 
                        WHERE r.is_active=1 AND r.id IN (
	                        SELECT f.asset_id FROM dbo.business_unit_field uf 
	                        INNER JOIN dbo.field f ON uf.field_id=f.id
	                        WHERE uf.business_unit_id=@0 AND f.asset_id=r.id  GROUP BY f.asset_id)
                        ORDER BY r.asset_name ASC", businessUnitId);
                var data = _services.db.Fetch<asset>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.asset_name;
                    itemList.NodeType = "ASSET";
                    itemList.Icon = "fal fa-compress-wide";
                    //itemList.Children = true;
                    itemList.Data = new ExpandoObject();
                    itemList.Data = item;
                    itemList.State = new ExpandoObject();
                    if (item.is_active ?? false)
                    {
                        itemList.State.undetermined = item.is_active;
                        //itemList.State.Checked = item.is_active;
                    }

                    var children = GetFieldTreeByBusinessUnitSelected(item.id, businessUnitId);
                    itemList.Children = children;



                    results.Add(itemList);
                    row++;
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<dynamic> GetField(string assetId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                var sql = Sql.Builder.Append(field.DefaultView);

                sql.Append("WHERE r.is_active=1 ");
                if (string.IsNullOrEmpty(assetId))
                {
                    return null;
                }

                sql.Append("AND r.asset_id=@0 ", assetId);
                sql.Append("ORDER BY r.field_name asc ");
                var data = _services.db.Fetch<vw_field>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.field_name;
                    itemList.NodeType = "FIELD";
                    itemList.Icon = "fal fa-expand-wide";
                    itemList.Data = new ExpandoObject();
                    itemList.Data = item;
                    results.Add(itemList);
                    row++;
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<dynamic> GetFieldTreeByBusinessUnit(string assetId, string businessUnitId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                if (string.IsNullOrEmpty(assetId) || string.IsNullOrEmpty(businessUnitId))
                {
                    return results;
                }

                //var sql = Sql.Builder.Append(@"SELECT r.id,r.field_name,r.asset_id, is_active = 0
                //        FROM dbo.field as r 
                //        WHERE r.id NOT IN (SELECT uf.field_id FROM dbo.business_unit_field uf WHERE uf.business_unit_id=@1 AND uf.field_id=r.id)
                //        AND r.asset_id=@0 AND r.is_active=1
                //        UNION ALL
                //        SELECT r.id,r.field_name,r.asset_id, is_active = 1
                //        FROM dbo.field as r
                //        WHERE r.id IN (SELECT uf.field_id FROM dbo.business_unit_field uf WHERE uf.business_unit_id=@1 AND uf.field_id=r.id)
                //        AND r.asset_id=@0 AND r.is_active=1
                //        ", assetId, businessUnitId);

                var sql = Sql.Builder.Append(@"SELECT r.id,r.field_name,r.asset_id, CASE WHEN ISNULL(s.field_id,'') = '' THEN 0 ELSE 1 END is_active
                    FROM dbo.field as r 
                    LEFT JOIN 
                    (
                    SELECT uf.field_id FROM dbo.business_unit_field uf WHERE uf.business_unit_id=@1
                    )
                    s
                    ON r.id=s.field_id
                    WHERE r.asset_id=@0 AND r.is_active=1", assetId, businessUnitId);


                sql.Append("ORDER BY r.field_name asc ");
                var data = _services.db.Fetch<dynamic>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.field_name;
                    itemList.State = new ExpandoObject();
                    itemList.State.Selected = Convert.ToBoolean(item.is_active);
                    itemList.State.Checked = Convert.ToBoolean(item.is_active);


                    itemList.NodeType = "FIELD";
                    itemList.Icon = "fal fa-expand-wide";
                    itemList.Data = new ExpandoObject();
                    itemList.Data = item;
                    results.Add(itemList);
                    row++;
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public List<dynamic> GetFieldTreeByBusinessUnitSelected(string assetId, string businessUnitId)
        {
            List<dynamic> results = new List<dynamic>();
            try
            {
                if (string.IsNullOrEmpty(assetId) || string.IsNullOrEmpty(businessUnitId))
                {
                    return null;
                }

                var sql = Sql.Builder.Append(@"
                        SELECT r.id,r.field_name,r.asset_id, is_active = 1
                        FROM dbo.field as r
                        WHERE r.id IN (SELECT uf.field_id FROM dbo.business_unit_field uf WHERE uf.business_unit_id=@1 AND uf.field_id=r.id)
                        AND r.asset_id=@0 AND r.is_active=1
                        ", assetId, businessUnitId);


                sql.Append("ORDER BY r.field_name asc ");
                var data = _services.db.Fetch<field>(sql);
                int row = 0;
                foreach (var item in data)
                {
                    dynamic itemList = new ExpandoObject();
                    if (row == 0)
                        itemList.State = true;

                    itemList.Id = item.id;
                    itemList.Text = item.field_name;
                    itemList.State = new ExpandoObject();
                    itemList.State.Selected = item.is_active;
                    itemList.State.Checked = item.is_active;


                    itemList.NodeType = "FIELD";
                    itemList.Icon = "fal fa-expand-wide";
                    itemList.Data = new ExpandoObject();
                    itemList.Data = item;
                    results.Add(itemList);
                    row++;
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public Node<vw_asset> GetComboTree(ITreeRequest request, string businessUnitId)
        {
            Node<vw_asset> results = new Node<vw_asset>(null);
            try
            {
                var qry = Sql.Builder.Append(@" SELECT r.id,r.asset_code,r.asset_name, is_active = 1
                                                FROM dbo.asset as r
                                                WHERE r.is_active = 1 AND r.id IN(
                                                 SELECT f.asset_id FROM dbo.business_unit_field uf
                                                 INNER JOIN dbo.field f ON uf.field_id = f.id
                                                 WHERE uf.business_unit_id =@0 AND f.asset_id = r.id  GROUP BY f.asset_id)
                                                ORDER BY r.asset_name ASC", businessUnitId);
                var records = vw_asset.Fetch(qry);

                foreach (var record in records)
                {
                    var data = new vw_asset();
                    data.id = record.id;
                    data.asset_name = record.asset_name;
                    Node<vw_asset> parent = results.Add(data);
                    GetComboTreeByParent(data.id, parent);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return results;
        }

        protected void GetComboTreeByParent(String parentId, Node<vw_asset> parent)
        {
            var children = parent;
            try
            {
                var records = field.Fetch(" WHERE asset_id = @0 ORDER BY field_name ASC ", parentId);
                if (records.Count > 0)
                {
                    foreach (var record in records)
                    {
                        var data = new vw_asset();
                        data.id = record.id;
                        data.asset_name = record.field_name;
                        GetComboTreeByParent(data.id, children.Add(data));
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
        }

        public Page<vw_asset> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 ", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
