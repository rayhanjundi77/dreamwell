﻿using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.Models.Entity;
using Dreamwell.DataAccess.Repository;
using Dreamwell.Infrastructure;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class HoleService
    {
        HoleRepository repo;
        CasingRepository casingRepository;
        DataContext context;
        public HoleService(DataContext context) {
            repo = new HoleRepository(context);
            casingRepository = new CasingRepository(context);
            this.context = context;
        }

        public async Task<ApiResponse> Get(string id)
        {
            var result = new ApiResponse();
            try
            {
                result.Data = await repo.GetById(id);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponsePage<Hole>> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var sb = new StringBuilder();
            if (marvelDataSourceRequest.Filter != null)
            {
                foreach (var filter in marvelDataSourceRequest.Filter.Filters)
                {
                    if (filter.Value == "")
                    {
                        continue;
                    }
                    sb.Append($" and {filter.Field} like '%{filter.Value}%'");
                }
            }
            sb.Append(" order by name asc ");
            var items = await repo.Get(sb.ToString());
            var result = new ApiResponsePage<Hole>();
            result.Items = items.ToList();
            result.TotalItems = items.Count();
            result.CurrentPage = 1;
            result.ItemsPerPage = 100;
            return result;
        }

        public async Task<ApiResponsePage<Hole>> LookupCasing(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var sb = new StringBuilder();
            sb.Append($" and hole_type=0 ");
            if (marvelDataSourceRequest.Filter != null)
            {
                foreach(var filter in marvelDataSourceRequest.Filter.Filters)
                {
                    if (filter.Value == "")
                    {
                        continue;
                    }
                    sb.Append($" and {filter.Field} like '%{filter.Value}%'");
                }
            }
            sb.Append(" order by name asc ");
            var items = await repo.Get(sb.ToString());
            var result = new ApiResponsePage<Hole>();
            result.Items = items.ToList();
            result.TotalItems = items.Count();
            result.CurrentPage = 1;
            result.ItemsPerPage = 100;
            return result;
        }

        public async Task<ApiResponse> Save(Hole model, bool isNew)
        {
            var result = new ApiResponse();
            try
            {
                model.hole_type = model.hole_type != null;
                model = await repo.Save(model, isNew);
                result.Status.Success = model != null;
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = model;
                }

                if (model.hole_type == true)
                {
                    var casing = new Casing();
                    casing.name = "0";
                    casing.hole_id = model.id;
                    await casingRepository.Save(casing, true);
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponse> Delete(string[] ids)
        {
            var result = new ApiResponse();
            try
            {
                result.Status.Success = await repo.Delete(ids);
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
