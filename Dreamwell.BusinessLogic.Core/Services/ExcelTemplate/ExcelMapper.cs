﻿using Dreamwell.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.Services.ExcelTemplate
{
    public class ExcelMapper : ExcelTemplateRepository
    {
        List<TableMap> TableMaps { get; set; }

        public ExcelMapper(DataContext dataContext) : base(dataContext) { }

        public void Load(string templateId)
        {

        }


        public class TableMap
        {
            bool ListofObject { get; set; }
            string TableName { get; set; }
            Dictionary<string, string> ColumnMaps { get; set; }
            public TableMap()
            {
                ListofObject = false;

                var mapper = new Func<Type, string, PropertyInfo>((type, columnName) =>
                {
                    return type.GetProperty(ColumnMaps[columnName]);
                });
            }
           

            Type GetTypeOfObject
            {
                get
                {
                    return Type.GetType(TableName);
                }
            }

            Type GetTypeWithNameSpace(string nameSpaceOfType)
            {
                return Type.GetType(string.Format("{0}, {1}", nameSpaceOfType, TableName));
            }


        }




    }
}
