﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using System.Web;
using System.IO;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class FileMasterServices : FileMasterRepository
    {
        public FileMasterServices(DataContext dataContext) : base(dataContext) { }


        public ApiResponse<vw_filemaster> viewFileMaster(string record_id)
        {
            var result = new ApiResponse<vw_filemaster>();
            try
            {
                result.Data = this.GetViewById(record_id);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        public ApiResponse<vw_filemaster> GetDetailFile(string record_id)
        {

            var result = new ApiResponse<vw_filemaster>();
            try
            {

                result.Data = this.GetViewFirstOrDefault(" AND r.record_id = @0 order by created_on desc", record_id);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;

        }

        public ApiResponse<List<vw_filemaster>> GetbyrecordId(string recordId)
        {
            var result = new ApiResponse<List<vw_filemaster>>();
            try
            {
                var Qry = Sql.Builder.Append(@"SELECT * FROM vw_filemaster WHERE record_id = @0", recordId);
                result.Data = this._services.db.Fetch<vw_filemaster>(Qry);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<vw_filemaster> GetDetailFileById(string record_id)
        {

            var result = new ApiResponse<vw_filemaster>();
            try
            {

                result.Data = this.GetViewFirstOrDefault(" AND r.id = @0 ", record_id);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;

        }
    }
}
