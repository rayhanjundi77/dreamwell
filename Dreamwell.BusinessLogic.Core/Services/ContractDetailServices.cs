﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ContractDetailServices : ContractDetailRepository
    {
        public ContractDetailServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(contract_detail record)
        {
            var result = new ApiResponse();
            var service_contract = new ContractServices(_services.dataContext);
            var service_currency = new CurrencyServices(_services.dataContext);
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var isNew = false;
                    var isNewContract = false;
                    var recordId = record.id;
                    decimal actualPrice = 0;

                    //var currencyList = service_currency.GetViewAll(" AND r.base_rate=@0 ", true);
                    //var baseRate = currencyList.Where(r => r.base_rate == true).SingleOrDefault();

                    //record.total_price = record.unit * record.unit_price;
                    //if (baseRate.rate_value == record.current_rate_value)
                    //    actualPrice = record.total_price * record.current_rate_value;
                    //else if (baseRate.rate_value < record.current_rate_value)
                    //    actualPrice = record.total_price / record.current_rate_value;


                    record.total_price = record.unit * record.unit_price;
                    record.actual_price = record.total_price / record.current_rate_value;


                    //record.actual_price = actualPrice;
                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                    var totalprice = this.GetAll(" AND is_active=@0 AND contract_id=@1 ", true, record.contract_id).Sum(r => r.actual_price);

                    var contractRecord = new contract();
                    contractRecord.id = record.contract_id;
                    contractRecord.total = totalprice;
                    result.Status.Success &= service_contract.SaveEntity(contractRecord, ref isNewContract);

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        if (isNew)
                        {
                            result.Status.Message = "The data has been added.";
                        }
                        else
                        {
                            result.Status.Message = "The data has been updated.";
                        }
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse Delete(string contractId, string recordId)
        {
            var result = new ApiResponse();
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result.Status.Success = this.Remove(recordId);
                    var totalprice = this.GetAll(" AND is_active=@0 AND contract_id=@1 ", true, contractId).Sum(r => r.total_price);

                    var qryUpdate = Sql.Builder.Append("UPDATE contract SET total = @0 WHERE id = @1 ", totalprice, contractId);
                    _services.db.Execute(qryUpdate);
                    result.Status.Success = true;

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "The data has been deleted.";
                        result.Data = new
                        {
                            recordId
                        };
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public ApiResponse<List<vw_contract_detail>> getListById(string contractId)
        {
            var result = new ApiResponse<List<vw_contract_detail>>();
            try
            {
                var data = this.GetViewAll(" AND r.is_active=@0 AND r.contract_id=@1 ", true, contractId);
                appLogger.Error(data);
                result.Data = data;
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        //public ApiResponse<List<vw_contract_detail>> getListByMaterial(string materialId)
        //{
        //    var result = new ApiResponse<List<vw_contract_detail>>();
        //    try
        //    {
        //        var data = this.GetViewAll(" AND r.is_active=@0 AND r.material_id=@1 ", true, materialId);
        //        result.Data = data;
        //        result.Status.Success = true;
        //        result.Status.Message = "Successfully";
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        result.Status.Success = false;
        //        result.Status.Message = ex.Message;
        //    }
        //    return result;
        //}

        public ApiResponse<List<vw_afe_manage_contract>> getListByMaterial(string materialId, string afeId, bool isNewPurchase)
        {
            var result = new ApiResponse<List<vw_afe_manage_contract>>();
            try
            {
               
                var service = new AfeManageContractRepository(_services.dataContext, materialId, afeId);
                if (isNewPurchase)
                    result.Data = service.GetViewAll(" AND r.material_id = @0 AND ((c.afe_id IS NULL OR c.afe_id = @1)) ", materialId, afeId);
                else
                    result.Data = service.GetViewAll(" AND r.material_id = @0 AND c.afe_id IS NOT NULL AND c.afe_id != @1 AND r.afe_locked_id IS NULL ", materialId, afeId);                //
                appLogger.Error("material id: " + materialId);
                appLogger.Error("afe id " + afeId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public DataTablesResponse GetListByMaterial(DataTableRequest dataTablesRequest, string materialId, string afeId, bool isNewPurchase)
        {

            DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe_manage_contract>(), 0, 0);
            try
            {
                var service = new AfeManageContractRepository(_services.dataContext, materialId, afeId);
                if (isNewPurchase)
                {
                    var qry = Sql.Builder.Append(" WHERE r.material_id = @0 AND ((c.afe_id IS NULL OR c.afe_id = @1)) ", materialId, afeId);
                    result = service.GetListDataTables(dataTablesRequest, qry);
                }
                else
                {
                    var qry = Sql.Builder.Append(" WHERE r.material_id = @0 AND c.afe_id IS NOT NULL AND c.afe_id != @1 AND r.afe_locked_id IS NULL ", materialId, afeId);
                    result = service.GetListDataTables(dataTablesRequest, qry);
                }
                appLogger.Error("material id: " + materialId);
                appLogger.Error("afe id " + afeId);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse<List<vw_afe_manage_issued_stock>> getListIssuedStock(string materialId, string afeId)
        {
            var result = new ApiResponse<List<vw_afe_manage_issued_stock>>();
            try
            {
                var service = new AfeManageIssuedStockRepository(_services.dataContext, materialId, afeId);
                result.Data = service.GetViewAll(" AND r.material_id = @0 AND c.afe_id IS NOT NULL AND c.afe_id != @1 AND (r.afe_locked_id = @1 OR r.afe_locked_id IS NULL) ", materialId, afeId);
                appLogger.Error("material id: " + materialId);
                appLogger.Error("afe id " + afeId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public DataTablesResponse GetListIssuedStock(DataTableRequest dataTablesRequest, string materialId, string afeId)
        {

            DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe_manage_issued_stock>(), 0, 0);

            try
            {
                var service = new AfeManageIssuedStockRepository(_services.dataContext, materialId, afeId);
                var qry = Sql.Builder.Append(" WHERE r.material_id = @0 AND c.afe_id IS NOT NULL AND c.afe_id != @1 AND (r.afe_locked_id = @1 OR r.afe_locked_id IS NULL) ", materialId, afeId);
                result = service.GetListDataTables(dataTablesRequest, qry);
                appLogger.Error("material id: " + materialId);
                appLogger.Error("afe id " + afeId);                
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return result;
        }
    }
}
