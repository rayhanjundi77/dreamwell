﻿using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using CommonTools.JWT;
using PetaPoco;
using Hangfire;
using System.Diagnostics;
using Dreamwell.Infrastructure.Session;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ExcelColumnMapServices : ExcelColumnMapRepository
    {
        public ExcelColumnMapServices(DataContext dataContext) : base(dataContext) { }


        public ApiResponse Save(excel_column_map record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "Column mapper has been created.";
                    }
                    else
                    {
                        result.Status.Message = "Column mapper has been modified.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }


            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }
    }
}
