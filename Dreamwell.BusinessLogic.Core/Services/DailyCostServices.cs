﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Dreamwell.Infrastructure.DataTables;
using DataTables.Mvc;
using CommonTools.Helper;
using CommonTools.EeasyUI;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DailyCostServices : DailyCostRepository
    {
        public DailyCostServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(daily_cost record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            var service_afecontract = new AfeContractServices(_services.dataContext);
            try
            {
                var isNew = false;
                var recordId = record.id;

                #region Validation Using Material 
                //var stockRecord = service_afecontract.GetById(record.afe_contract_id);
                var stockRecord = _services.db.First<afe_contract>(" where id = @0", record.afe_contract_id);


                var usingMaterialRecord = this.GetAll(" AND afe_contract_id = @0 AND material_id = @1 AND afe_id = @2 ", record.afe_contract_id, record.material_id, record.afe_id);

                if (usingMaterialRecord.Count > 0)
                {
                    //-- Cek Current Using
                    var onDrilling = usingMaterialRecord.Where(r => r.drilling_id == record.drilling_id).ToList();
                    var onAnotherDrilling = usingMaterialRecord.Where(r => r.drilling_id != record.drilling_id).ToList();

                    var unitOnDrilling = onDrilling.Sum(r => r.unit);
                    var unitOnAnotherDrilling = onAnotherDrilling.Sum(r => r.unit);

                    if (unitOnDrilling > 0)
                    {

                    }

                    if (stockRecord.unit < record.unit + unitOnAnotherDrilling)
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Not enough material on this contract";
                    }
                }
                else
                {
                    if (record.unit > stockRecord.unit)
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Not enough material on this contract";
                    }
                }
                #endregion

                if (result.Status.Success)
                    result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);

                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public Node<vw_afe_manage_usage> GetAfeLineTree(ITreeRequest request, String afeId, String drillingId)
        {
            var userId = BusinessLogic.Core.SysConfig.UserSetup.sysAdminId;
            Node<vw_afe_manage_usage> results = new Node<vw_afe_manage_usage>(null);
            try
            {
                if (!string.IsNullOrEmpty(request.ReloadByParentId) && !string.IsNullOrWhiteSpace(request.ReloadByParentId))
                {
                    GetAfeLineTree(drillingId, afeId, request.ReloadByParentId, results);
                }
                else
                {
                    var qry = Sql.Builder.Append(" WHERE is_active=1 and organization_id=@0 ", _services.dataContext.OrganizationId);
                    qry.Append(" AND parent_id IS NULL ");
                    qry.Append(" ORDER BY line ASC ");
                    var afeLineParent = afe_line.Fetch(qry);
                    foreach (var item in afeLineParent)
                    {
                        var record = new vw_afe_manage_usage();
                        record.id = item.id;
                        record.description = item.description;
                        record.icon_type = "fw-500 text-uppercase";
                        Node<vw_afe_manage_usage> parent = results.Add(record);
                        GetAfeLineTree(drillingId, afeId, item.id, parent);
                    }
                }
                //appLogger.Debug(JsonConvert.SerializeObject(results));

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        protected vw_afe_manage_usage GetAfeLineTree(String drillingId, String afeId, String parentId, Node<vw_afe_manage_usage> parent)
        {
            var results = new vw_afe_manage_usage();
            var children = parent;
            try
            {
                if (string.IsNullOrEmpty(parentId)) return results;
                var data = afe_line.Fetch(" WHERE parent_id=@0 ORDER BY line asc", parentId);
                if (data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        var record = new vw_afe_manage_usage();
                        record.id = item.id;
                        record.description = item.description;
                        record.icon_type = "fw-500 text-uppercase";
                        GetAfeLineTree(drillingId, afeId, item.id, children.Add(record));
                    }
                }
                else
                {

                    var itemType = children;
                    GetMaterial(drillingId, afeId, parentId, itemType.Add(new vw_afe_manage_usage
                    {
                        id = string.Format("{0}_DYRHOLEBASED", parentId),
                        description = "DRY HOLE BASED",
                        icon_type = "fw-900 text-uppercase font-italic"
                    }), 1);
                    GetMaterial(drillingId, afeId, parentId, itemType.Add(new vw_afe_manage_usage
                    {
                        id = string.Format("{0}_COMPLETIONBASED", parentId),
                        description = "COMPLETION BASED",
                        icon_type = "fw-900 text-uppercase font-italic"
                    }), 2);

                    //GetMaterial(parentId, children);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        protected vw_afe_manage_usage GetMaterial(String drillingId, String afeId, String afeLineId, Node<vw_afe_manage_usage> parent, int itemType)
        {
            var results = new vw_afe_manage_usage();
            var children = parent;
            try
            {
                var service = new AfeManageUsageRepository(_services.dataContext, afeId, afeLineId, itemType, drillingId);
                var materialRecord = service.GetViewAll().OrderBy(x => x.created_on).ToList();
                if (materialRecord.Count > 0)
                {
                    foreach (var item in materialRecord)
                    {
                        item.icon_type = "material";
                        children.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return results;
        }

        public DataTablesResponse GetAfeManageUsageDetail(DataTableRequest DataTableRequest, string afeId, string drillingId, string materialId)
        {
            DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe_manage_usage_detail>(), 0, 0);
            var service = new AfeManageUsageDetailRepository(_services.dataContext, drillingId);
            try
            {
                Sql sql = new Sql();
                sql.Append(" WHERE r.afe_id = @0 AND r.material_id = @1 ", afeId, materialId);
                result = service.GetListDataTables(DataTableRequest, sql);
            } 
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse<DailyCostViewModel> GetDailyCost(string wellId, string drillingId)
        {
            var result = new ApiResponse<DailyCostViewModel>();
            result.Data = new DailyCostViewModel();
            result.Status.Success = true;
            try
            {
                var service_drilling = new DrillingServices(_services.dataContext);
                var drillingRecord = service_drilling.GetById(drillingId);

                var sql = Sql.Builder.Append(@" SELECT COALESCE(SUM(cd.actual_price / cd.unit * r.unit), 0) AS daily
                                                FROM dbo.daily_cost AS r
                                                LEFT OUTER JOIN dbo.afe_contract AS a
                                                  ON r.afe_contract_id = a.id
                                                LEFT OUTER JOIN dbo.contract_detail AS cd
                                                  ON a.contract_detail_id = cd.id
                                                WHERE r.drilling_id = @0 ", drillingId);
                result.Data.daily_cost = this._services.db.First<decimal>(sql);

                sql = new Sql();
                sql = Sql.Builder.Append(@" SELECT COALESCE(SUM(cd.actual_price / cd.unit * r.unit), 0) AS cumm_cost
                                                FROM dbo.daily_cost AS r
                                                LEFT OUTER JOIN dbo.afe_contract AS a
                                                  ON r.afe_contract_id = a.id
                                                LEFT OUTER JOIN dbo.drilling AS d
                                                  ON r.drilling_id = d.id
                                                LEFT OUTER JOIN dbo.contract_detail AS cd
                                                  ON a.contract_detail_id = cd.id
                                                WHERE d.well_id = @0 AND d.drilling_date <= @1 ", wellId, drillingRecord.drilling_date);
                result.Data.cummulative_cost = this._services.db.First<decimal>(sql);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public DataTablesResponse GetListByQuery(DataTableRequest DataTableRequest, string afe_id, string drilling_id)
        {
            Sql sql = new Sql();
            try
            {
                var sb = new System.Text.StringBuilder();
                sb.AppendLine(@"SELECT r.*, usage.total_unit");
                sb.AppendLine(@"FROM (");
                sb.AppendLine(@"	SELECT	r.id, afe_id = null, r.id AS material_id, r.afe_line_id, currency_id = null, currency_code = null, current_rate_value = null, unit = 0, unit_price = 0, total_price = 0, actual_price = 0,");
                sb.AppendLine(@"			owner_id = null, r.description AS material_name, al.description AS afe_line_description, alp.name AS afe_line_parent_name");
                sb.AppendLine(@"	FROM material r");
                sb.AppendLine(@"	LEFT OUTER JOIN afe_line AS al ON r.afe_line_id = al.id");
                sb.AppendLine(@"	LEFT OUTER JOIN afe_line_parent AS alp ON al.afe_line_parent_id = alp.id");
                sb.AppendLine(@"	WHERE r.id NOT IN ");
                sb.AppendLine(@"				  (");
                sb.AppendLine(@"					SELECT ap.material_id ");
                sb.AppendLine(@"					FROM afe_plan ap ");
                sb.AppendLine(@"				  )");
                sb.AppendLine(@"	UNION");
                sb.AppendLine(@"    SELECT	r.id, ap.afe_id, ap.material_id, r.afe_line_id, currency_id = null, currency_code = null, current_rate_value = null, ap.total_unit AS unit, unit_price = 0, total_price = 0, actual_price = 0,");
                sb.AppendLine(@"            ap.owner_id, r.description AS material_name, al.description AS afe_line_description, alp.name AS afe_line_parent_name");
                sb.AppendLine(@"	FROM material r");
                sb.AppendLine(@"	INNER JOIN afe_line AS al ON r.afe_line_id = al.id");
                sb.AppendLine(@"	INNER JOIN afe_line_parent AS alp ON al.afe_line_parent_id = alp.id");
                sb.AppendLine(@"	CROSS APPLY (");
                sb.AppendLine(@"					SELECT m.id AS material_id, af.id AS afe_id, SUM(cd.unit) AS total_unit, af.owner_id");
                sb.AppendLine(@"					FROM afe_contract af");
                sb.AppendLine(@"					INNER JOIN contract c ON c.id = af.contract_id");
                sb.AppendLine(@"					INNER JOIN contract_detail cd ON cd.contract_id = c.id");
                sb.AppendLine(@"					INNER JOIN material m ON m.id = cd.material_id");
                sb.AppendLine(@"					WHERE af.afe_id = @0 AND cd.material_id = r.id");
                sb.AppendLine(@"					GROUP BY m.id, af.id, af.owner_id");
                sb.AppendLine(@"				) AS ap");
                sb.AppendLine(@") AS r");
                sb.AppendLine(@"OUTER APPLY (SELECT total_unit FROM [dbo].[udf_get_usage_by_drilling](@1, r.material_id)) AS usage");


                sql = Sql.Builder.Append(sb.ToString(), afe_id, drilling_id);
                sql.Append("WHERE 1=1 ");
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            appLogger.Error(sql.SQL);
            appLogger.Error(sql.Arguments);
            return base.GetListDataTablesByQuery(DataTableRequest, sql.SQL, " r.afe_line_parent_name ASC, r.afe_line_description ASC, r.material_name ", sql.Arguments);
        }

    }
}
