﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ExcelDataValidationServices : ExcelDataValidationRepository
    {
        public ExcelDataValidationServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(excel_data_validation record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "Data validation has been created.";
                    }
                    else
                    {
                        result.Status.Message = "Data validation has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }


    }
}
