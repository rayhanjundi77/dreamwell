﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillingChemicalUsedServices : DrillingChemicalUsedRepository
    {
        public DrillingChemicalUsedServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse<List<drilling_chemical_used>> Save(List<drilling_chemical_used> records, string drillingId)
        {
            var result = new ApiResponse<List<drilling_chemical_used>>();
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                try
                {
                    var sql = Sql.Builder.Append(@"DELETE FROM drilling_chemical_used where drilling_id = @0", drillingId);
                    this._services.db.Execute(sql);

                    records = records.Where(o => o != null).ToList();
                    result.Status.Success = true;
                    foreach (var record in records)
                    {
                        var isNew = false;
                        record.drilling_id = drillingId;
                        result.Status.Success &= SaveEntity(record, ref isNew, (r) => record.id = r.id);
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Data = records;
                        result.Status.Message = "Successfully Save Chemical Used";
                    }

                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    appLogger.Error(ex);
                }

            }
            return result;
        }
    }
}
