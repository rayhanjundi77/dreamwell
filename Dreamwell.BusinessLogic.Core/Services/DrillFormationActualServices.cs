﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using System.Transactions;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DrillFormationActualServices : DrillFormationsActualRepository
    {
        public DrillFormationActualServices(DataContext dataContext) : base(dataContext){}

        public ApiResponse Save(DrillingViewModel record)
        {
            var result = new ApiResponse();
            result.Status.Success = true;
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    if (record.drill_Formations_Actual != null)
                    {
                        var sql = Sql.Builder.Append(@"DELETE FROM drill_formations_actual WHERE well_id = @0", record.drilling.well_id);
                        this._services.db.Execute(sql);

                        int seq = 1;
                        foreach (drill_formations_actual item in record.drill_Formations_Actual)
                        {
                            bool isNew = false;
                            item.well_id = record.drilling.well_id;
                            item.seq = seq;
                            result.Status.Success &= SaveEntity(item, ref isNew, (r) => item.id = r.id);
                            seq++;
                        }
                    }

                    if (result.Status.Success)
                    {
                        scope.Complete();
                        result.Status.Message = "Drilling Actual Formation has been updated.";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }

            return result;
        }

        public ApiResponse GetList(string wellid)
        {
            ApiResponse result = new ApiResponse();
            try
            {

                var qry = Sql.Builder.Append("Select * from drill_formations_actual where well_id=@0 ", wellid);
                var records = drill_formations_actual.Fetch(qry);

                result.Data = records;
            }
            catch (Exception ex)
            {
                string ff = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }


        public System.Collections.Generic.List<vw_drill_formations_actual> GetListView(string wellid)
        {
            var qry = Sql.Builder.Append("Select * from vw_drill_formations_actual where well_id=@0 ", wellid);
            System.Collections.Generic.List<vw_drill_formations_actual> result = vw_drill_formations_actual.Fetch(qry);
            return result;
        }
    }
}
