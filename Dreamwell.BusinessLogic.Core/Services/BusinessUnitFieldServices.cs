﻿using System;
using Dreamwell.Infrastructure;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using System.Collections.Generic;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class BusinessUnitFieldsServices : BusinessUnitFieldRepository
    {
        public BusinessUnitFieldsServices(DataContext dataContext) : base(dataContext) { }

        public Page<vw_business_unit_field> IsLookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            var kosong = Sql.Builder.Append("  \n AND (\n ) ");

            // Apply filter for non-administrative users
            if (!this._services.dataContext.IsSystemAdministrator)
            {
                sqlFilter.Append(" AND r.business_unit_id=@0", this._services.dataContext.PrimaryBusinessUnitId);
                Console.WriteLine($"Filtered by business unit ID: {this._services.dataContext.PrimaryBusinessUnitId}");
            }

            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null && filterBuilder.SQL != kosong.SQL)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion

            sqlFilter.Append(" ORDER BY r.field_name ASC ");
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}