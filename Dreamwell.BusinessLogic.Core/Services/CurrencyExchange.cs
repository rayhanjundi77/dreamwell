﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class CurrencyExchange : CurrencyExchangeRepository
    {
        public CurrencyExchange(DataContext dataContext) : base(dataContext) { }

        public ApiResponse<bool> UpdateRecords(currency_exchange[] record)
        {
            var service_currency = new CurrencyServices(_services.dataContext);
            var result = new ApiResponse<bool>();
            result.Status.Success = true;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {

                    //var currentBaseRate = service_currency.GetViewAll(" AND r.base_rate=@0 ", true).FirstOrDefault();

                    var nonactive = Sql.Builder.Append("UPDATE dbo.currency_exchange SET is_active = 0 ");
                    _services.db.Execute(nonactive);

                    //var recordExchange = new currency_exchange();
                    //recordExchange.id = null;
                    //recordExchange.start_date = DateTime.Now;
                    //recordExchange.end_date = DateTime.Now.AddYears(5);
                    //recordExchange.target_id = currentBaseRate.id;
                    //recordExchange.source_id = currentBaseRate.id;
                    //recordExchange.rate_value = currentBaseRate.rate_value;
                    //result.Status.Success = SaveEntity(recordExchange, ref isNew, (r) => recordExchange.id = r.id);

                    //currencyExchangeRecord.start_date = ;
                    ////    currencyExchangeRecord.end_date = ;

                    foreach (var item in record)
                    {
                        var isNew = false;
                        var recordId = "";
                        item.start_date = DateTime.Now; 
                        item.end_date = DateTime.Now.AddYears(5);
                        result.Status.Success &= this.SaveEntity(item, ref isNew, (r) => recordId = r.id);
                    }

                    if (result.Status.Success)
                    {
                        result.Data = true;
                        scope.Complete();
                        result.Status.Message = "Exchange rate has been updated.";
                    }

                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
            }
            return result;
        }

        public Page<vw_currency_exchange> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=@0 ", true);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
