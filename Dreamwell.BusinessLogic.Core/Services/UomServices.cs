﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using CommonTools.UnitOfMeasurement;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using System.Data.SqlClient;
namespace Dreamwell.BusinessLogic.Core.Services
{
    public class UomServices : UomRepository
    {
        public UomServices(DataContext dataContext) : base(dataContext) { }

        public ApiResponse Save(uom record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                if (String.IsNullOrEmpty(recordId))
                {
                    record.id = CommonTools.GuidHash.ConvertToMd5HashGUID(record.uom_code).ToString();
                    result.Status.Message = "The data has been added.";
                }
                else
                {
                    result.Status.Message = "The data has been updated.";
                }

                result.Status.Success = SaveEntity(record, ref isNew, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse SaveUnitOfMeasurements()
        {
            var result = new ApiResponse();
            var log = string.Empty;
            var success = 0;
            var failed = 0;
            try
            {
                #region UOM Category
                var categoryRecord = new uom_category();
                result.Status.Success = true;
                /*AREA*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Area));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Area).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Area
                    };

                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*LENGTH*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Length));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Length).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Length
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Volume*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Volume));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Volume).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Volume
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Time*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Time));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Time).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Time
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Mass*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Mass));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Mass).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Mass
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Power*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Power));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Power).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Power
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Temperature*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Temperature));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Temperature).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Temperature
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Unit*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Unit));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Unit).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Unit
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Angle*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Angle));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Angle).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Angle
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Acceleration*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Acceleration));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Acceleration).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Acceleration
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Velocity*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Velocity));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Velocity).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Velocity
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*MudWeight*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.MudWeight));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.MudWeight).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.MudWeight
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Percent*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Percent));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Percent).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Percent
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*FlowRate*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.FlowRate));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.FlowRate).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.FlowRate
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*MassFlow*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.MassFlow));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.MassFlow).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.MassFlow
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Pressure*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Pressure));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Pressure).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Pressure
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Force*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Force));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Force).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Force
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                /*Diameter*/
                categoryRecord = uom_category.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Diameter));
                if (categoryRecord == null)
                {
                    categoryRecord = new uom_category()
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(Category.Diameter).ToString(),
                        organization_id = _services.dataContext.OrganizationId,
                        category_name = Category.Diameter
                    };
                    result.Status.Success &= _services.dataContext.SaveEntity<uom_category>(categoryRecord, true);
                }

                #endregion

                foreach (var unit in MeasurementUnits.Units)
                {
                    try
                    {
                        var record = uom.FirstOrDefault("WHERE id=@0", CommonTools.GuidHash.ConvertToMd5HashGUID(unit.Description).ToString());
                        if (record == null)
                        {
                            record = new uom();
                            record.id = CommonTools.GuidHash.ConvertToMd5HashGUID(unit.Description).ToString();
                            record.uom_code = unit.Id;
                            record.description = unit.Description;
                            record.is_std_international = unit.SI;
                            record.is_unit_default = unit.Default;
                            record.unit_alias = string.Join(",", unit.Aliases);
                            record.uom_category_id = CommonTools.GuidHash.ConvertToMd5HashGUID(unit.CategoryName).ToString();
                            record.organization_id = _services.dataContext.OrganizationId;
                            result.Status.Success &= _services.dataContext.SaveEntity<uom>(record, true);
                            success = (result.Status.Success ? success + 1 : 0);
                            failed = (result.Status.Success ? 0 : failed + 1);
                        }
                        else
                        {
                            record = new uom();
                            record.id = CommonTools.GuidHash.ConvertToMd5HashGUID(unit.Description).ToString();
                            record.uom_code = unit.Id;
                            record.description = unit.Description;
                            record.is_std_international = unit.SI;
                            record.is_unit_default = unit.Default;
                            record.unit_alias = string.Join(",", unit.Aliases);
                            record.uom_category_id = CommonTools.GuidHash.ConvertToMd5HashGUID(unit.CategoryName).ToString();
                            record.organization_id = _services.dataContext.OrganizationId;
                            record.is_active = true;
                            result.Status.Success &= _services.dataContext.SaveEntity<uom>(record, false);
                            success = (result.Status.Success ? success + 1 : 0);
                            failed = (result.Status.Success ? 0 : failed + 1);
                        }

                    }
                    catch (Exception ex)
                    {
                        appLogger.Error($"Unit {unit.Id}");
                        appLogger.Error(ex);
                        failed++;
                    }

                }


                #region Remove UOM on database if doesn't match on system 
                var deleteRecords = uom.Fetch("WHERE id NOT IN (@0)", MeasurementUnits.Units.Select(o => CommonTools.GuidHash.ConvertToMd5HashGUID(o.Description)).ToArray());
                foreach (var r in deleteRecords)
                {
                    _services.dataContext.DeleteEntity<uom>(r.id);
                }
                #endregion

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }

            return result;
        }
        public Page<vw_uom> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            //var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1 ", _services.dataContext.OrganizationId);
            var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }

        //public Page<vw_uom> Lookup(MarvelDataSourceRequest marvelDataSourceRequest, string searchValue)
        //{
        //    #region Use Filters
        //    var sqlFilter = Sql.Builder.Append(" AND r.is_active=1 ");

        //    // Apply search value if provided
        //    if (!string.IsNullOrEmpty(searchValue))
        //    {
        //        sqlFilter.Append(" AND (uom_code LIKE @0 OR object_name LIKE @0)", $"%{searchValue}%");
        //    }

        //    if (marvelDataSourceRequest.Filter != null)
        //    {
        //        Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
        //        if (filterBuilder != null)
        //        {
        //            sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
        //        }
        //    }
        //    #endregion

        //    return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        //}

        //private Sql MultipleFilterable(Filterable.FilterDataSource filterDataSource)
        //{
        //    var sql = Sql.Builder;
        //    foreach (var filter in filterDataSource.Filters)
        //    {
        //        if (filter.Field == "description" && filter.Operator == "contains")
        //        {
        //            sql.Append(" AND r.description LIKE @0", $"%{filter.Value}%");
        //        }
        //        else if (filter.Field == "uom_code" && filter.Operator == "contains")
        //        {
        //            sql.Append(" AND r.uom_code LIKE @0", $"%{filter.Value}%");
        //        }
        //        else
        //        {
        //            throw new NotImplementedException($"Operator {filter.Operator} belum diimplementasikan.");
        //        }
        //    }
        //    return sql;
        //}

        //private Page<vw_uom> GetViewPerPage(long page, long pageSize, string sql, params object[] args)
        //{
        //    Implementation for fetching the data from database based on the given SQL and parameters.
        //    This is a placeholder and should be replaced with actual data access logic.

        //   throw new NotImplementedException();
        //}

        public Page<vw_uom> LookupLength(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filter
            var sqlFilter = Sql.Builder.Append("AND r.is_active=1 AND uom_category_id='6c9c2aba-778c-3fe0-83ef-8bf543612275' ");
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            return base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
        }
    }
}
