﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using DataTables.Mvc;
using Omu.ValueInjecter;
using PetaPoco;
using Dreamwell.DataAccess;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class RoleAccessServices : RoleAccessRepository
    {
        public RoleAccessServices(DataContext dataContext) : base(dataContext) { }

        public DataTablesResponse GetListDataTablesByRoleId(DataTableRequest DataTableRequest, string roleId)
        {
            var qry = Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
            qry.Append(" AND r.application_role_id=@0 ", roleId);
            return base.GetListDataTables(DataTableRequest, qry);
        }

        public ApiResponse setRoleAllEntity(string roleId, string access)
        {
            var result = new ApiResponse();

            try
            {
                long dataValue = 0;
                switch (access)
                {
                    case "ACCESS_NOACCESS":
                        dataValue = (long)DataAccessControl.ACCESS_NOACCESS;
                        break;
                    case "ACCESS_OWNRECORD":
                        dataValue = (long)DataAccessControl.ACCESS_OWNRECORD;
                        break;
                    case "ACCESS_BUSINESSUNIT":
                        dataValue = (long)DataAccessControl.ACCESS_BUSINESSUNIT;
                        break;
                    case "ACCESS_PARENTCHILD":
                        dataValue = (long)DataAccessControl.ACCESS_PARENTCHILD;
                        break;
                    case "ACCESS_ORGANIZATION":
                        dataValue = (long)DataAccessControl.ACCESS_ORGANIZATION;
                        break;
                    default:
                        dataValue = 0;
                        break;
                }

                var appRoleService = new ApplicationRoleServices(this._services.dataContext);
                var roleRecord = appRoleService.GetById(roleId);
                if (roleRecord == null)
                    throw new Exception("This role not found on database");
                result.Status.Success = true;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    try
                    {
                        var roleAccessRecords = GetAll(" AND application_role_id=@0 AND is_active=1 ", roleId);
                        roleAccessRecords.ForEach(x =>
                        {
                            x.access_create = dataValue;
                            x.access_read = dataValue;
                            x.access_update = dataValue;
                            x.access_delete = dataValue;
                            x.access_approve = dataValue;
                            x.access_share = dataValue;
                            x.access_append = dataValue;
                        });
                        foreach (var r in roleAccessRecords)
                        {
                            result.Status.Success &= (this._services.db.Update(r) == 1);
                        }
                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        result.Status.Success = false;
                        result.Status.Message = ex.Message;
                        appLogger.Error(ex);
                    }
                }

            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Debug($"Role Id {roleId}");
                appLogger.Error(ex);
            }
            return result;
        }

        public bool Copy(string copyFrom, string copyTo)
        {
            bool success = false;
            bool isNew = false;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    var fromRecord = GetAll(" AND application_role_id = @0  ", copyFrom);
                    var toRecord = GetAll(" AND application_role_id = @0 ", copyTo);
                    if (fromRecord.Count > 0)
                    {
                        success = true;
                    }
                    else
                    {
                        throw new Exception("Record doesnt have role");
                    }

                    RegisterRoleAccessEntity(copyTo);

                    foreach (var r in fromRecord)
                    {
                        
                        var toId = toRecord.Where(c => c.application_entity_id == r.application_entity_id).FirstOrDefault();

                        var record = new role_access();
                        record.InjectFrom(r);

                        record.id = toId.id;
                        record.application_role_id = copyTo;
                        success &= SaveEntity(record, ref isNew);
                    }

                    if (success)
                    {
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    throw;
                }
            }
            return success;
        }

        public bool RegisterRoleAccessEntity(String AppRoleId)
        {
            bool success = false;

            using (var db = new DataAccess.dreamwellRepo())
            {
                using (var scope = db.GetTransaction())
                {
                    try
                    {
                        var qry = application_entity.DefaultView;
                        qry += " WHERE r.id NOT IN (SELECT m.application_entity_id FROM role_access m WHERE m.application_role_id=@0 ) ";
                        var ListEntity = db.Fetch<vw_application_entity>(qry, AppRoleId);

                        success = true;
                        if (ListEntity != null)
                        {
                            foreach (var item in ListEntity)
                            {
                                var record = new role_access();
                                record.application_entity_id = item.id;
                                record.application_role_id = AppRoleId;
                                record.access_create = 0;
                                record.access_read = 0;
                                record.access_update = 0;
                                record.access_delete = 0;
                                record.access_approve = 0;
                                record.access_lock = 0;
                                record.access_activate = 0;
                                record.access_append = 0;
                                record.access_share = 0;
                                record.access_level = 0;

                                success &= _services.dataContext.SaveEntity<role_access>(record, true);
                            }


                        }

                        if (success)
                        {
                            scope.Complete();
                        }

                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                        throw;
                    }
                }
            }
            return success;
        }
    }
}
