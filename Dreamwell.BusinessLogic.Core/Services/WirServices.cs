﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dreamwell.Infrastructure;
using CommonTools.JSTree;
using Dreamwell.DataAccess;
using Newtonsoft.Json;
using CommonTools;
using PetaPoco;
using CommonTools.UnitOfMeasurement;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using System.Data.SqlClient;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WirServices : WirRepository
    {
        private readonly DataContext _dataContext;

        public WirServices(DataContext dataContext) : base(dataContext)
        {
            _dataContext = dataContext;
        }

        public ApiResponse Save(wir record)
        {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;

                result.Status.Success = SaveEntity(record, ref isNew, this._services.dataContext.PrimaryTeamId, (r) => recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                    {
                        result.Status.Message = "The data has been added.";
                    }
                    else
                    {
                        result.Status.Message = "The data has been updated.";
                    }
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public Page<vw_wir> Lookup(MarvelDataSourceRequest marvelDataSourceRequest)
        {
            #region Use Filters
            var sqlFilter = Sql.Builder.Append(" AND r.organization_id=@0 AND r.is_active=1", _services.dataContext.OrganizationId);
            if (marvelDataSourceRequest.Filter != null)
            {
                Sql filterBuilder = MultipleFilterable(marvelDataSourceRequest.Filter);
                if (filterBuilder != null)
                    sqlFilter.Append(filterBuilder.SQL, filterBuilder.Arguments);
            }
            #endregion
            var result = base.GetViewPerPage(marvelDataSourceRequest.Page, marvelDataSourceRequest.PageSize, sqlFilter.SQL, sqlFilter.Arguments);
            return result;
        }

        //public ApiResponse<List<vw_wir>> GetByWellId(string wellId)
        //{
        //    var result = new ApiResponse<List<vw_wir>>();
        //    try
        //    {
        //        if (string.IsNullOrWhiteSpace(wellId))
        //        {
        //            result.Status.Success = false;
        //            result.Status.Message = "Well ID cannot be empty.";
        //            return result;
        //        }

        //        var sql = Sql.Builder.Append(@"
        //            SELECT wi.*, w.field_name
        //            FROM vw_wir wi
        //            LEFT JOIN vw_wellwi w ON w.id = wi.well_id
        //            WHERE wi.well_id = @0
        //            ORDER BY wi.created_on DESC", wellId);

        //        result.Data = this._services.db.Fetch<vw_wir>(sql);
        //        appLogger.Info($"Retrieved {result.Data.Count} records.");
        //        result.Status.Success = true;
        //        result.Status.Message = "Data retrieved successfully.";
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error($"Error in GetByWellId: {ex.Message}");
        //        result.Status.Success = false;
        //        result.Status.Message = $"Error retrieving data: {ex.Message}";
        //    }
        //    return result;
        //}

        public ApiResponse<List<WirViewModel>> GetByWellId(string wellId)
        {
            var result = new ApiResponse<List<WirViewModel>>();
            try
            {
                if (string.IsNullOrWhiteSpace(wellId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Well ID cannot be empty.";
                    return result;
                }

                // Fetch all WIR data for the given Well ID
                var sql = Sql.Builder.Append(@"
            SELECT wi.*, w.field_name
            FROM vw_wir wi
            LEFT JOIN vw_wellwi w ON w.id = wi.well_id
            WHERE wi.well_id = @0
            ORDER BY wi.created_on DESC", wellId);

                var wirDataList = this._services.db.Fetch<vw_wir>(sql);

                if (wirDataList == null || !wirDataList.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No WIR data found for the given Well ID.";
                    return result;
                }

                var viewModelList = new List<WirViewModel>();

                // Loop through each WIR record
                foreach (var wirData in wirDataList)
                {
                    // Fetch related wellwi data
                    var wellwiData = this._services.db.Fetch<vw_wellwi>(
                        "SELECT * FROM vw_wellwi WHERE id = @0", wirData.well_id);

                    // Map WIR data to ViewModel
                    var viewModel = new WirViewModel
                    {
                        wir = wirData,
                        wellwi = wellwiData ?? new List<vw_wellwi>()
                    };

                    viewModelList.Add(viewModel);
                }

                result.Data = viewModelList;
                result.Status.Success = true;
                result.Status.Message = "Data retrieved successfully.";
            }
            catch (Exception ex)
            {
                appLogger.Error($"Error in GetByWellId: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"Error retrieving data: {ex.Message}";
            }

            return result;
        }

        public ApiResponse Delete(params string[] ids)
        {
            var result = new ApiResponse();
            try
            {
                result.Status.Success = true;
                using (var scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    try
                    {
                        foreach (var recordId in ids)
                        {
                            var record = well.FirstOrDefault("WHERE id=@0", recordId);
                            record.is_active = false;
                            result.Status.Success &= _services.dataContext.SaveEntity<well>(record, false);
                        }
                        if (result.Status.Success)
                        {
                            scope.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponse<List<RKAPWIViewModel>>> GetWirData()
        {
            var result = new ApiResponse<List<RKAPWIViewModel>>();
            try
            {
                appLogger.Info("Fetching WIR data.");

                // Query SQL yang diperbarui untuk mengambil kolom yang diinginkan
                var sql = @"
                SELECT 
                    w.issues_date,
                    w.well_id, 
                    ISNULL(we.well_name, '') AS well_name, 
                    w.wir_no,      
                    ISNULL(w.request_status, '') AS status,  
                    ISNULL(pd.estimated_hour, 0) AS job_day,
                    ISNULL(pd.workload_id, '') AS main_job,
                    ISNULL(pd.comment_program, '') AS remarks
                FROM wir w 
                LEFT JOIN vw_wellwi we ON we.id = w.well_id
                LEFT JOIN vw_well_intervention_program wip ON wip.wir_id = w.id
                LEFT JOIN (
                    SELECT 
                        wip_id, 
                        MAX(comment_program) AS comment_program,
                        MAX(estimated_hour) AS estimated_hour,
                        MAX(workload_id) AS workload_id
                    FROM program_detail_wip
                    GROUP BY wip_id
                ) pd ON wip.id = pd.wip_id
                ORDER BY w.issues_date DESC";

                // Asynchronously fetch data
                result.Data = await Task.Run(() => this._services.db.Fetch<RKAPWIViewModel>(sql));

                appLogger.Info($"Retrieved {result.Data.Count} records.");
                result.Status.Success = true;
                result.Status.Message = "Data retrieved successfully.";
            }
            catch (Exception ex)
            {
                appLogger.Error($"Error in GetWirData: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"Error retrieving data: {ex.Message}";
            }

            return result;
        }

    }

}
