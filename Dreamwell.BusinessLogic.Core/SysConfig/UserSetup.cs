﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dreamwell.DataAccess;
using NLog;
using CommonTools;
namespace Dreamwell.BusinessLogic.Core.SysConfig
{
    public class UserSetup
    {
        Logger appLogger = LogManager.GetCurrentClassLogger();

        public const string sysAdmin = "sys_admin";
        public static string sysAdminId = CommonTools.GuidHash.ConvertToMd5HashGUID(sysAdmin).ToString();
        private const bool isActiveDirectory = false;
        public static string defaultPassword = string.Format("{0}", PasswordHash.CreateHash("Pertamina"));

        public void PreparingSetup() {
            var log = string.Empty;
            using (var db = new dreamwellRepo())
            {
                try
                {
                    log = $"Checking user {sysAdmin} .";
                    Console.WriteLine(log);
                    appLogger.Info(log);
                    var oldRecord = application_user.FirstOrDefault("WHERE id=@0", sysAdminId);
                    if (oldRecord == null)
                    {
                        log = $"Creating user {sysAdmin} .";
                        Console.WriteLine(log);
                        appLogger.Info(log);

                        var record = new application_user();
                        record.id = sysAdminId;
                        record.created_by = sysAdminId;
                        record.created_on = DateTime.Now;

                        record.app_username = sysAdmin;
                        record.app_password = defaultPassword;
                        record.is_sysadmin = true;
                        record.first_name = "System";
                        record.last_name = "Administrator";
                        record.is_ldap = isActiveDirectory;
                        record.organization_id = OrganizationSetup.OrganizationId;
                        record.is_active = true;
                        record.is_default = true;

                        db.Insert(record);

                        log = $"{sysAdmin} successfully registered by system.";
                        Console.WriteLine(log);
                        appLogger.Info(log);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    appLogger.Error(ex);
                }
            }
        }

    }
}
