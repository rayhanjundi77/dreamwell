﻿using Dreamwell.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Hangfire;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;
using CommonTools.SmartNavigation;
using System.IO;

namespace Dreamwell.BusinessLogic.Core.SysConfig
{

    internal static class NavigationBuilder
    {
        private static JsonSerializerSettings DefaultSettings => SerializerSettings();

        private static JsonSerializerSettings SerializerSettings(bool indented = true) => new JsonSerializerSettings
        {
            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            Formatting = indented ? Formatting.Indented : Formatting.None,
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> { new StringEnumConverter() }
        };

        public static SmartNavigation FromJson(string json) => JsonConvert.DeserializeObject<SmartNavigation>(json, DefaultSettings);
    }

    public class ApplicationMenuSetup
    {

        Logger appLogger = LogManager.GetCurrentClassLogger();


        public void InstallMenu()
        {
            var jsonText = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "nav.json");
            var navigation = NavigationBuilder.FromJson(jsonText);

            appLogger.Debug($"Total Menu : {navigation.Lists.Count}");
            RegisterMenu(navigation.Lists);
        }

        private void RegisterMenu(List<ListItem> menu,string parentId = null)
        {
            try
            {
                DataContext dc = new DataContext(UserSetup.sysAdminId);
                int row = 1;
                foreach (var m in menu)
                {
                    
                    var record = new application_menu();
                    record.menu_name = m.Title;
                    record.organization_id = SysConfig.OrganizationSetup.OrganizationId;
                    record.is_group = (m.Items.Count > 0);
                    record.icon_class = m.Icon;
                    record.parent_menu_id = parentId;
                    record.application_menu_category_id = application_menu_category.GetCategoryId("SIDEBAR");
                    record.order_index = row;
                    if (record.is_group ?? false == true)
                    {
                        record.action_url = "#";
                        dc.SaveEntity<application_menu>(record, true);
                        RegisterMenu(m.Items,record.id);
                    }
                    else
                    {
                        record.action_url = m.Href;
                        dc.SaveEntity<application_menu>(record, true);
                    }

                    row++;

                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
        }


        public void InstallCategory()
        {
            using (var db = new dreamwellRepo())
            {
                try
                {
                    DataContext dc = new DataContext(UserSetup.sysAdminId);
                    var result = false;
                    appLogger.Debug($"Install Application Menu Category");
                    foreach (var category in application_menu_category.DefaultCategory)
                    {
                        var oldRecord = application_menu_category.FirstOrDefault("WHERE id=@0", category.id);
                        if (oldRecord == null)
                        {
                            var record = new application_menu_category();
                            record.id = category.id;
                            record.category_name = category.category_name;
                            record.created_by = UserSetup.sysAdminId;
                            record.created_on = DateTime.Now;

                            result = dc.SaveEntity<application_menu_category>(record, true);

                            if (result)
                            {
                                appLogger.Debug($"Category {category.category_name} has been installed");
                            }
                        }
                    }
                    appLogger.Debug($"Completed install Application Menu Category");
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                }

            }
        }

        public void InstallModule()
        {
            using (var db = new dreamwellRepo())
            {
                try
                {
                    DataContext dc = new DataContext(UserSetup.sysAdminId);
                    var result = false;
                    appLogger.Debug($"Install Application Module");
                    foreach (var category in application_module.DefaultData)
                    {
                        var oldRecord = application_module.FirstOrDefault("WHERE id=@0", category.id);
                        if (oldRecord == null)
                        {
                            var record = new application_module();
                            record.id = category.id;
                            record.organization_id = SysConfig.OrganizationSetup.OrganizationId;
                            record.module_name = category.module_name;
                            record.created_by = UserSetup.sysAdminId;
                            record.created_on = DateTime.Now;

                            result = dc.SaveEntity<application_module>(record, true);

                            if (result)
                            {
                                appLogger.Debug($"Module {category.module_name} has been installed");
                            }
                        }
                    }
                    appLogger.Debug($"Completed install Application Module");
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                }

            }
        }


        public void Setup()
        {
            //BackgroundJob.ContinueWith(InstallCategory,() => Console.WriteLine("Continuation!"));
            new ApplicationEntitySetup().Synchronize();
            InstallCategory();
            InstallModule();
            //InstallMenu();
        }

    }
}
