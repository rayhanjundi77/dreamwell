﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.Core.SysConfig
{
    public static class TeamSetup
    {
        public static class Default {
            const string Companyman = "Companyman";
            const string DrillingEngineer = "Drilling Engineer";
        }
    }
}
