﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonTools;
using Dreamwell.DataAccess;
namespace Dreamwell.BusinessLogic.Core.SysConfig
{
    public class ApplicationEntitySetup
    {
        public void Synchronize()
        {
            int New = 0, Update = 0, Fail = 0;
            using (var db = dreamwellRepo.GetInstance())
            {
                using (var scope = db.GetTransaction())
                    try
                    {
                        TextInfo ti = CultureInfo.CurrentCulture.TextInfo;

                        var qry = @" SELECT TABLE_CATALOG,TABLE_SCHEMA,TABLE_NAME,TABLE_TYPE
		                    FROM  INFORMATION_SCHEMA.TABLES
		                    WHERE TABLE_TYPE='BASE TABLE'
		                    ORDER BY TABLE_NAME ";

                        var data = db.Fetch<dynamic>(qry);
                        Console.WriteLine(db.LastCommand);

                        foreach (var i in data)
                        {
                            try
                            {
                                var isNew = false;
                                var id = GuidHash.ConvertToMd5HashGUID(
                                    i.TABLE_SCHEMA + "." + i.TABLE_NAME).ToString();
                                application_entity record =
                                    db.FirstOrDefault<application_entity>("WHERE id = @0", id);
                                Console.WriteLine(db.LastCommand);

                                if (record == null)
                                {
                                    isNew = true;
                                    record = new application_entity
                                    {
                                        id = id,
                                        created_by = "189f3ca3-83b0-41aa-8af6-9c0a346d087c",
                                        created_on = DateTime.Now,
                                        is_active = true,
                                        is_default = true,
                                        is_locked = true,
                                        owner_id = "189f3ca3-83b0-41aa-8af6-9c0a346d087c",
                                        organization_id = "9EE4834225E334380667DDCF2F60F6DA"
                                    };
                                }

                                record.entity_name = i.TABLE_SCHEMA + "." + i.TABLE_NAME;
                                record.display_name = ti.ToTitleCase(((string)i.TABLE_NAME).Replace("_", " "));

                                if (isNew)
                                {
                                    db.Insert(record);
                                    Console.WriteLine(db.LastCommand);
                                    New++;
                                }
                                else
                                {
                                    record.modified_by = record.created_by;
                                    record.modified_on = DateTime.Now;
                                    db.Update(record);
                                    Console.WriteLine(db.LastCommand);
                                    Update++;
                                }
                            }
                            catch (Exception ex)
                            {
                                Fail++;
                                Console.WriteLine(ex.Message);
                                continue;
                            }
                        }

                        scope.Complete();
                        Console.WriteLine("Failed = {0}", Fail);
                        Console.WriteLine("Done");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
            }
        }
    }
}
