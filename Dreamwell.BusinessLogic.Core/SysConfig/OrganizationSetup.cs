﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dreamwell.DataAccess;
using NLog;
using System.Transactions;
using CommonTools.JSTree;
//using Dreamwell.DataAccess.Repository;
using Newtonsoft.Json;
using CommonTools;

namespace Dreamwell.BusinessLogic.Core.SysConfig
{

    public class OrganizationSetup
    {
        Logger appLogger = LogManager.GetCurrentClassLogger();

        public const string AxaDomain = "pertamina.id";
        public const string OrganizationName = "PERTAMINA";
        public static string OrganizationId = CommonTools.GuidHash.ConvertToMd5HashGUID(OrganizationName).ToString();
        public const bool IsDefaultOrganization = true;

        public void PreparingSetup()
        {

            var log = string.Empty;
            using (var db = new dreamwellRepo())
            {
                try
                {
                    log = $"Checking Organization {OrganizationName} .";
                    Console.WriteLine(log);
                    appLogger.Info(log);

                    var oldRecord = organization.FirstOrDefault("WHERE id=@0", OrganizationId);
                    if (oldRecord == null)
                    {
                        log = $"Creating Organization {OrganizationName} .";
                        Console.WriteLine(log);
                        appLogger.Info(log);

                        var record = new organization();
                        record.id = OrganizationId;
                        record.created_by = UserSetup.sysAdminId;
                        record.created_on = DateTime.Now;
                        record.is_active = true;
                        record.is_default = true;
                        record.is_default_organization = IsDefaultOrganization;
                        record.organization_name = OrganizationName;

                        db.Insert(record);
                        log = $"Organization {OrganizationName} successfully registered by system.";
                        Console.WriteLine(log);
                        appLogger.Info(log);
                    }
                    else
                    {
                        log = $"Organization {OrganizationName} already exists. Enjoyed!";
                        Console.WriteLine(log);
                        appLogger.Info(log);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    appLogger.Error(ex);
                }
            }

        }

        public void GenerateOrganization(string newOrganizationName)
        {
            Logger appLogger = LogManager.GetCurrentClassLogger();
            string newOrganizationId = "";
            bool IsDefaultOrganizationNow = false;

            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    newOrganizationId = CommonTools.GuidHash.ConvertToMd5HashGUID(newOrganizationName).ToString();
                    if (newOrganizationName == OrganizationName) IsDefaultOrganizationNow = true;
                    else IsDefaultOrganizationNow = false;

                    var log = string.Empty;
                    using (var db = new dreamwellRepo())
                    {
                        try
                        {
                            log = $"Checking Organization {newOrganizationName} .";
                            Console.WriteLine(log);
                            appLogger.Info(log);

                            var oldRecord = organization.FirstOrDefault("WHERE id=@0", newOrganizationId);
                            if (oldRecord == null)
                            {
                                log = $"Creating Organization {newOrganizationName} .";
                                Console.WriteLine(log);
                                appLogger.Info(log);

                                #region Organization

                                var record = new organization();
                                record.id = newOrganizationId;
                                record.created_by = UserSetup.sysAdminId;
                                record.created_on = DateTime.Now;
                                record.is_active = true;
                                record.is_default = true;
                                record.is_default_organization = IsDefaultOrganizationNow;
                                record.organization_name = newOrganizationName;
                                record.owner_id = UserSetup.sysAdminId;


                                db.Insert(record);
                                #endregion

                                #region Create Team
                                team teamRecord = new team();
                                teamRecord.team_name = string.Format("{0} Team", newOrganizationName);
                                teamRecord.id = CommonTools.GuidHash.ConvertToMd5HashGUID(teamRecord.team_name).ToString();
                                teamRecord.is_active = true;
                                teamRecord.is_default = true;
                                teamRecord.created_on = DateTime.Now;
                                teamRecord.created_by = UserSetup.sysAdminId;
                                teamRecord.owner_id = UserSetup.sysAdminId;
                                teamRecord.organization_id = record.id;

                                db.Insert(teamRecord);
                                #endregion

                                #region Create Default Business Unit
                                var newBusinessUnit = new business_unit();
                                newBusinessUnit.unit_name = string.Format("{0} Business Unit", newOrganizationName);
                                newBusinessUnit.id = CommonTools.GuidHash.ConvertToMd5HashGUID(newBusinessUnit.unit_name).ToString();
                                newBusinessUnit.created_on = DateTime.Now;
                                newBusinessUnit.created_by = UserSetup.sysAdminId;
                                newBusinessUnit.is_active = true;
                                newBusinessUnit.is_default = true;
                                newBusinessUnit.owner_id = UserSetup.sysAdminId;
                                newBusinessUnit.organization_id = record.id;
                                newBusinessUnit.default_team = teamRecord.id;
                                db.Insert(newBusinessUnit);
                                #endregion

                                #region Create Default Application User
                                var newUser = new application_user();
                                newUser.created_on = DateTime.Now;
                                newUser.created_by = UserSetup.sysAdminId;
                                newUser.is_active = true;
                                newUser.is_default= true;
                                newUser.owner_id = UserSetup.sysAdminId;
                                newUser.business_unit_id = newBusinessUnit.id;
                                newUser.gender = true;
                                newUser.is_ldap = false;
                                newUser.is_sysadmin = true;
                                newUser.app_password = UserSetup.defaultPassword;
                                newUser.email = string.Format("admin.{0}@pertamina.com", newOrganizationName.ToLower());
                                newUser.organization_id = OrganizationSetup.OrganizationId;
                                if (IsDefaultOrganizationNow)
                                {
                                    newUser.app_username = UserSetup.sysAdmin;
                                    newUser.first_name = "System";
                                    newUser.last_name = "Administrator";
                                    newUser.id = UserSetup.sysAdminId;
                                }
                                else
                                {
                                    newUser.app_username = string.Format("admin {0}", newOrganizationName.ToLower());
                                    newUser.first_name = newUser.app_username;
                                    newUser.last_name = "Administrator";
                                    newUser.organization_id = record.id;
                                    newUser.id = CommonTools.GuidHash.ConvertToMd5HashGUID(newUser.app_username).ToString();
                                }

                                db.Insert(newUser);

                                #endregion

                                scope.Complete();

                                log = $"Organization {newOrganizationName} successfully registered by system.";
                                Console.WriteLine(log);
                                log = $"Organization {newOrganizationName} successfully registered a new user admin.";
                                Console.WriteLine(log);
                            }
                            else
                            {
                                log = $"Organization {newOrganizationName} already exists!";
                                Console.WriteLine(log);
                            }
                            appLogger.Info(log);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            appLogger.Error(ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

    }
}
