﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using NLog;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Dreamwell.BusinessLogic.Core.Common
{
    public class JwtWrapper
    {
        static Logger appLogger = LogManager.GetCurrentClassLogger();

        private static string Key = "DVcdrftgyhjvgcfxdrTFYGUHIRXfcgvh76eudgGOA*ytFGturIGhj^&%^#4758769youGHGJDy7850769*PYOULGKHJGHJFsrer5";
        private static string Issuer = "http://www.dreamweel-issuer.com";
        private static string Audience = "http://www.dreamweel-audience.com";

        public static string GenerateToken(string username, object payload, int expireMinutes = 86400)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            JwtSecurityTokenHandler securityTokenHandler = new JwtSecurityTokenHandler();
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, username),
                    new Claim("userData", JsonConvert.SerializeObject(payload))
                }),
                Expires = DateTime.UtcNow.AddMinutes((double)expireMinutes),
                SigningCredentials = credentials,
                Issuer = Issuer,
                Audience = Audience,
            };

            SecurityToken token = securityTokenHandler.CreateToken(tokenDescriptor);

            return securityTokenHandler.WriteToken(token);
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
               return tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    RequireExpirationTime = true,
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = Issuer,
                    ValidAudience = Audience,
                    IssuerSigningKey = securityKey
                }, out SecurityToken validatedToken);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex, $"Error on validate token {token}");
                return null;
            }

        }
    }
}
