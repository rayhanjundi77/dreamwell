﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace Dreamwell.Common.Cryptography
{
    public class AesEncryption
    {
        private const int Iterations = 10000;
        private const int KeySize = 256;
        private const int IvSize = 128;
        private static string password = "Dre4mWell2023";

        public static string Encrypt(string plainText)
        {
            byte[] salt = GenerateRandomSalt();

            using (Aes aesAlg = Aes.Create())
            {
                byte[] key = GenerateKey(password, salt, KeySize);
                byte[] iv = GenerateKey(password, salt, IvSize);

                aesAlg.Key = key;
                aesAlg.IV = iv;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor();

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        byte[] encryptedBytes = msEncrypt.ToArray();
                        byte[] result = new byte[salt.Length + encryptedBytes.Length];
                        Buffer.BlockCopy(salt, 0, result, 0, salt.Length);
                        Buffer.BlockCopy(encryptedBytes, 0, result, salt.Length, encryptedBytes.Length);
                        return Convert.ToBase64String(result);
                    }
                }
            }
        }

        public static string Decrypt(string cipherText)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            byte[] salt = new byte[16];
            byte[] encryptedBytes = new byte[cipherBytes.Length - salt.Length];

            Buffer.BlockCopy(cipherBytes, 0, salt, 0, salt.Length);
            Buffer.BlockCopy(cipherBytes, salt.Length, encryptedBytes, 0, encryptedBytes.Length);

            using (Aes aesAlg = Aes.Create())
            {
                byte[] key = GenerateKey(password, salt, KeySize);
                byte[] iv = GenerateKey(password, salt, IvSize);

                aesAlg.Key = key;
                aesAlg.IV = iv;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor();

                using (MemoryStream msDecrypt = new MemoryStream(encryptedBytes))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            return srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
        }

        private static byte[] GenerateKey(string password, byte[] salt, int keySize)
        {
            using (Rfc2898DeriveBytes keyGenerator = new Rfc2898DeriveBytes(password, salt, Iterations))
            {
                return keyGenerator.GetBytes(keySize / 8);
            }
        }

        private static byte[] GenerateRandomSalt()
        {
            byte[] salt = new byte[16];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return salt;
        }
    }
}
