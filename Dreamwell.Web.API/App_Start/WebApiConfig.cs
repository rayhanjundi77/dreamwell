﻿using Dreamwell.Infrastructure.Acl;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApiContrib.Formatting.Jsonp;
namespace Axa.Web.API
{

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            //config.BindParameter(typeof(DataTableRequest), new DataTableModelBinder());
            // Web API configuration and services
            config.Filters.Add(new WebApiAclFilter());
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();


            // Web API routes
           // var cors = new EnableCorsAttribute("*", "*", "*");
           // cors.SupportsCredentials = true;
           // config.EnableCors(cors);
            config.EnableCors();
            config.MapHttpAttributeRoutes();
            config.Formatters.XmlFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("multipart/form-data"));

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Routes.MapHttpRoute(
            //    name: "ApiOnBusinessUnit",
            //    routeTemplate: "api/{unit}/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            //var jsonFormatter = new JsonpMediaTypeFormatter(config.Formatters.JsonFormatter);
            //config.Formatters.Insert(0, jsonFormatter);
            //Add CORS Handler
            //GlobalConfiguration.Configuration.MessageHandlers.Add(new CorsHandler());
            //config.EnsureInitialized();
        }
    }
}
