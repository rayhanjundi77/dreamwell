﻿using CommonTools;
using CommonTools.JSTree;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/WellDrillingParameter")]
    public class WellDrillingParameterController : ApiBaseController
    {
        [Route("getAll")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_drilling_parameter>>> getAll()
        {
            var services = new WellDrillingParameterServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_drilling_parameter>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_drilling_parameter>>();
                var record = services.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }
        // GET api/<controller>
        [Route("{recordId}/detailDpByWell")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_drilling_parameter>>> GetdetailDpByWell(string recordId)
        {
            var services = new WellDrillingParameterServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_drilling_parameter>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_drilling_parameter>>();
                return services.GetByIdDP(recordId); ;
            });
            return response;
        }

        [Route("savedp")]
        [HttpPost]
        public async Task<ApiResponse> Savedp(well_drilling_parameter record)
        {
            var services = new WellDrillingParameterServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("deletedp")]
        [HttpPost]
        public async Task<ApiResponse> Deletedp([FromBody] string[] ids)
        {
            var services = new WellDrillingParameterServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }
    }
}