﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Drilling")]

    public class DrillingController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_drilling>> GetPage(long page, long itemsPerPage)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_drilling>>(() =>
            {
                var result = new ApiResponsePage<vw_drilling>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_drilling>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_drilling>>(() =>
            {
                var result = new ApiResponsePage<vw_drilling>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling>> GetDetail(string recordId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling>>(() =>
            {
                return services.Detail(recordId, userSession);
            });
            return response;
        }

        [Route("{wellId}/detailbyId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> getMonitorBudget(string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                try
                {
                    result = services.getByDetail(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("{wellId}/detailbyIdRkap")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling>> GetDataForRkap(string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling>>(() =>
            {
                var result = new ApiResponse<vw_drilling>();
                try
                {
                    result = services.getForRkap(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("{wellId}/lastDrilling")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> lastDrilling(string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                try
                {
                    result = services.getLastDrilling(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("{wellId}/getFirst")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling>> getFirst(string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling>>(() =>
            {
                var result = new ApiResponse<vw_drilling>();
                try
                {
                    result = services.getFirstdril(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }




        [Route("getDol/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> getDOL(string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                try
                {
                    result = services.GetDOl(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("{wellId}/getlatest")]
        [HttpGet]
        public async Task<ApiResponse<DrillingViewModel>> GetLatest(string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<DrillingViewModel>>(() =>
            {
                return services.GetLatest(wellId);
            });
            return response;
        }

        [Route("{recordId}/detailDrilling")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling>> GetDetailDrilling(string recordId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling>>(() =>
            {
                return services.ViewDrilling(recordId);
            });
            return response;
        }

        [Route("newdrilling")]
        [HttpPost]
        public async Task<ApiResponse> CreateNew(drilling record)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.CreateNew(record);
            });
            return response;
        }

        //[Route("save")]
        //[HttpPost]
        //public async Task<ApiResponse> Save(DrillingViewModel record)
        //{
        //    var services = new DrillingServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse>(() =>
        //    {
        //        return services.Save(record);
        //    });
        //    return response;
        //}

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(drilling record)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("getViewAll")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> getViewAll()
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                var record = services.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getViewAllNew")]
        [HttpGet]
        public async Task<ApiResponseCount<List<vw_drilling>>> getViewAllNew(string isadmin)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponseCount<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponseCount<List<vw_drilling>>();
                var record = services.CountTotalDDR(isadmin);
                result.Status.Success = true;
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("getByWellId/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> getByWellId(string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                var record = services.GetViewAll(" AND r.well_id=@0 ", wellId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getByWellIdOrderDesc/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> getByWellIdOrderDesc(string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                var record = services.GetViewAll(" AND r.well_id=@0 ORDER BY r.drilling_date ASC ", wellId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getListDrilling/{wellId}/{startDate}/{endDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> getListDrilling(string wellId, DateTime startDate, DateTime endDate)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                var record = services.GetViewAll(" AND r.well_id=@0 AND (r.drilling_date BETWEEN @1 AND @2) ", wellId, startDate, endDate);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("submit")]
        [HttpPost]
        public async Task<ApiResponse> Submit([FromUri] string id)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.Submit(id);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        [Route("approve")]
        [HttpPost]
        public async Task<ApiResponse> Approve(ApprovalRequest data)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.Approve(data, userSession);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        [Route("getDailyMudCost/{wellId}/{date}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> getDailyMudCost(string wellId, DateTime date)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                var record = services.GetViewAll(" AND r.well_id=@0 AND r.drilling_date<=@1 ", wellId, date);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getDrillingApproval/{wellId}")]
        [HttpPost]
        public async Task<DataTablesResponse> getDrillingApproval([FromBody] DataTableRequest dataTableRequest, string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active= @0 AND r.well_id= @1 AND r.submitted_by IS NOT NULL AND aps.approval_status = @2 ", true, wellId, 2);
                result = services.GetListDataTables(dataTableRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }



        [Route("getAllApproval")]
        [HttpPost]
        public async Task<DataTablesResponse> getAllApproval([FromBody] DataTableRequest dataTableRequest)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active= @0 AND r.submitted_by IS NOT NULL AND aps.approval_status = @1 ", true, 2);
                if (!this.dataContext.IsSystemAdministrator)
                {
                    qry.Append(" AND t.team_name = @0", this.dataContext.PrimaryTeam);
                }
                result = services.GetListDataTables(dataTableRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("getDrillingApproved/{wellId}")]
        [HttpPost]
        public async Task<DataTablesResponse> getDrillingApproved([FromBody] DataTableRequest dataTableRequest, string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active= @0 AND r.well_id= @1 AND r.submitted_by IS NOT NULL AND aps.approval_level = 2 ", true, wellId);
                result = services.GetListDataTables(dataTableRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("getAllApproved")]
        [HttpPost]
        public async Task<DataTablesResponse> getAllApproved([FromBody] DataTableRequest dataTableRequest)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active= @0 AND r.submitted_by IS NOT NULL AND aps.approval_level = 2 AND aps.approval_status = 200 ", true);
                if (!this.dataContext.IsSystemAdministrator)
                {
                    qry.Append(" AND t.team_name = @0", this.dataContext.PrimaryTeam);
                }
                result = services.GetListDataTables(dataTableRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }


        [Route("getTotalApproved")]
        [HttpGet]
        public async Task<ApiResponse<int>> getTotalApproved()
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<int>>(() =>
            {
                var result = new ApiResponse<int>();
                var countResponse = services.CountTotalApproved();

                result.Status.Success = countResponse.Status.Success;
                result.Status.Message = countResponse.Status.Message;
                result.Data = countResponse.Data;

                return result;
            });
            return response;
        }

        [Route("getTotalApproval")]
        [HttpGet]
        public async Task<ApiResponse<int>> getTotalApproval()
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<int>>(() =>
            {
                var result = new ApiResponse<int>();
                var countResponse = services.CountTotalApproval();

                result.Status.Success = countResponse.Status.Success;
                result.Status.Message = countResponse.Status.Message;
                result.Data = countResponse.Data;

                return result;
            });
            return response;
        }


        [Route("getTotalRejected")]
        [HttpGet]
        public async Task<ApiResponse<int>> getTotalRejected()
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<int>>(() =>
            {
                var result = new ApiResponse<int>();
                var countResponse = services.CountRejected();

                result.Status.Success = countResponse.Status.Success;
                result.Status.Message = countResponse.Status.Message;
                result.Data = countResponse.Data;

                return result;
            });
            return response;
        }


        [Route("getDrillingHistory/{wellId}")]
        [HttpPost]
        public async Task<DataTablesResponse> getDrillingHistory([FromBody] DataTableRequest dataTableRequest, string wellId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active= @0 AND r.well_id= @1 AND r.submitted_by IS NOT NULL AND aps.approval_status IS NOT NULL ", true, wellId);
                result = services.GetListDataTables(dataTableRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        //[Route("getLatestDrilling/{fieldId}")]
        //[HttpGet]
        //public async Task<ApiResponse<DrillingViewModel>> GetLatestDrillingData(string fieldId)
        //{
        //    var services = new DrillingServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse<DrillingViewModel>>(() =>
        //    {
        //        return services.GetLatestDrillingDataAsync(fieldId);
        //    });
        //    return response;
        //}

        [Route("getLatestDrilling/{fieldId}")]
        [HttpGet]
        public async Task<ApiResponse<List<DrillingViewModel>>> GetLatestDrillingData(string fieldId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<DrillingViewModel>>>(() =>
            {
                return services.GetLatestDrillingDataAsync(fieldId);
            });
            return response;
        }

        [Route("getLatestDrillingPTNPT/{fieldId}")]
        [HttpGet]
        public async Task<ApiResponse<List<DrillingViewModel>>> GetLatestDrillingPTNPT(string fieldId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<DrillingViewModel>>>(() =>
            {
                return services.GetLatestDrillingPTNPT(fieldId);
            });
            return response;
        }

        [Route("getDrillingPipe/{fieldId}")]
        [HttpGet]
        public async Task<ApiResponse<List<DrillingViewModel>>> GetDrillingPipe(string fieldId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<DrillingViewModel>>>(() =>
            {
                return services.GetDrillingPipe(fieldId);
            });
            return response;
        }
    }
}
