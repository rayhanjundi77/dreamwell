﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using CommonTools;
using Dreamwell.Infrastructure.Options;
using System.Collections.Generic;
using System;
using Dreamwell.Infrastructure.DataTables;
using DataTables.Mvc;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{

    [RoutePrefix("api/core/ApplicationRole")]
    public class ApplicationRoleController : ApiBaseController
    {
        #region User Role
        [Route("{roleId}/member")]
        [HttpPost]
        public async Task<DataTablesResponse> GetMember(string roleId, [FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new UserRoleServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_user_role>(), 0, 0);
                result = services.GetListDataTablesMember(dataTablesRequest, roleId);
                return result;
            });
            return response;
        }

        [Route("addMember")]
        [HttpPost]
        public async Task<ApiResponse> AddMember([FromBody] user_role record)
        {
            var services = new UserRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    var isNew = false;
                    result.Status.Success = services.SaveEntity(record, ref isNew);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    if (ex.Message.Contains("UK_user_role"))
                    {
                        result.Status.Message = "This user has registered of the role.";
                    }
                    else
                    {
                        result.Status.Message = ex.Message;
                    }
                }
                return result;
            });
            return response;
        }


        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_role>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ApplicationRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_role>>(() =>
            {
                var result = new ApiResponsePage<vw_application_role>();
                var data = services.GetViewPerPage(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }


        [Route("removeMember")]
        [HttpPost]
        public async Task<ApiResponse> RemoveMember([FromBody] string[] userRoleIds)
        {
            var services = new UserRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(userRoleIds);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        [Route("{roleId}/userAvailable")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_user>> GetUserAvailable(string roleId, [FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new UserRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_user>>(() =>
            {
                var result = new ApiResponsePage<vw_application_user>();
                var data = services.GetUserAvailable(marvelDataSourceRequest, roleId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        #endregion  

        #region Role Access

        

        [Route("{roleId}/roleAccess")]
        [HttpPost]
        public async Task<DataTablesResponse> GetRoleAccess(string roleId, [FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new RoleAccessServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_role_access>(), 0, 0);
                result = services.GetListDataTablesByRoleId(dataTablesRequest, roleId);
                return result;
            });
            return response;
        }

        [Route("roleAccess/update")]
        [HttpPost]
        public async Task<ApiResponse> UpdateRoleAccess([FromBody] role_access record)
        {
            var services = new RoleAccessServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var isNew = false;
                result.Status.Success = services.SaveEntity(record,ref isNew);
                return result;
            });
            return response;
        }

        #endregion

        [Route("synchronizeEntityRole")]
        [HttpPost]
        public async Task<ApiResponse> SynchronizeEntityRoleAccess()
        {
            var services = new ApplicationRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                result.Status.Success = services.ApplicationEntitySetupAndRoleAccess();
                return result;
            });
            return response;
        }

        [Route("dataSource")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_role>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ApplicationRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_role>>(() =>
            {
                var result = new ApiResponsePage<vw_application_role>();
                var data = services.GetViewPerPage(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_application_role>> GetDetail(string recordId)
        {
            var services = new ApplicationRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_application_role>>(() =>
            {
                var result = new ApiResponse<vw_application_role>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(application_role record)
        {
            var services = new ApplicationRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("setRoleAllEntity")]
        [HttpPost]
        public async Task<ApiResponse> SetRoleAllEntity([FromUri] string roleId, [FromUri] string access)
        {
            var services = new RoleAccessServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.setRoleAllEntity(roleId,access);
            });
            return response;
        }

    }
}
