﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using Dreamwell.DataAccess;
using CommonTools;
using System;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/ApplicationUser")]
    public class ApplicationUserController : ApiBaseController
    {

        [Route("dataSource")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_user>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ApplicationUserServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_user>>(() =>
            {
                var result = new ApiResponsePage<vw_application_user>();
                var data = services.GetViewPerPage(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_user>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ApplicationUserServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_user>>(() =>
            {
                var result = new ApiResponsePage<vw_application_user>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookupUser")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_user>> LookupUser([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ApplicationUserServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_user>>(() =>
            {
                var result = new ApiResponsePage<vw_application_user>();
                var data = services.LookupUser(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }


        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_application_user>> GetDetail(string recordId)
        {
            var services = new ApplicationUserServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_application_user>>(() =>
            {
                var result = new ApiResponse<vw_application_user>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(application_user record)
        {
            var services = new ApplicationUserServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("uploadImagePicture")]
        [HttpPost]
        public async Task<ApiResponse> Upload([FromUri(Name = "d")] string iduser)
        {
            return await UploadImagePicture<application_user>(iduser, "image_id");
        }

        [Route("updatePasssword")]
        [HttpPost]
        public async Task<ApiResponse> updatePassword(application_user record)
        {
            var services = new ApplicationUserServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.updatePassword(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete(string[] ids)
        {
            var services = new ApplicationUserServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    throw;
                }
                return result;
            });
            return response;
        }

    }
}
