﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingMud")]

    public class DrillingMudController : ApiBaseController
    {
        [Route("")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_mud>> Detail([FromUri]string drillingId,[FromUri]string dataMudType)
        {
            var services = new DrillingMudServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_mud>>(() =>
            {
                var result = new ApiResponse<vw_drilling_mud>();
                var record = services.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", drillingId,dataMudType);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{drillingId}/detailbyId")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_mud>> DetailbyId(string drillingId)
        {
            var services = new DrillingMudServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_mud>>(() =>
            {
                var result = new ApiResponse<vw_drilling_mud>();
                var record = services.GetViewFirstOrDefault(" AND r.drilling_id=@0", drillingId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }



        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(drilling_mud record)
        {
            var services = new DrillingMudServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("getDrillingMudRangeDate/{wellId}/{startDate}/{endDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_mud>>> getDrillingmudDetailDate(string wellId, DateTime startDate, DateTime endDate)
        {
            var services = new DrillingMudServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_mud>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_mud>>();
                var record = services.getDrillingmudDeviationDataDate(wellId, startDate, endDate);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }
        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingMudServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }

    }
}
