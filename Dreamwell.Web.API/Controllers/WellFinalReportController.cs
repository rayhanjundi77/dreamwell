﻿using System;
using CommonTools;
using DataTables.Mvc;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web;
using Dreamwell.BusinessLogic.Core.Services;
using System.Net.NetworkInformation;
using System.Net.Http;
using System.Net;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/WellFinalReport")]
    public class WellFinalReportController : ApiBaseController
    {
        [Route("{recordId}/detail")]
        [HttpGet]
        public Task<ApiResponse<well_final_report>> GetDetail(string recordId)
        {
            var services = new WellFinalReportServices(this.dataContext);
            var response = Task.Run<ApiResponse<well_final_report>>(() =>
            {
                var result = new ApiResponse<well_final_report>();
                result.Data = services.GetById(recordId);
                result.Status.Success = true;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detailByWellId")]
        [HttpGet]
        public Task<ApiResponse<vw_well_final_report>> GetDetailByWellId(string recordId)
        {
            var services = new WellFinalReportServices(this.dataContext);
            var response = Task.Run<ApiResponse<vw_well_final_report>>(() =>
            {
                var result = new ApiResponse<vw_well_final_report>();
                result = services.GetByWellId(recordId);
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(well_final_report record)
        {
            var services = new WellFinalReportServices(this.dataContext);
            //var httpRequest = HttpContext.Current.Request;
          
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.Save(record);
                }
                catch (Exception ex)
                {
                    result.Status.Message = ex.Message;
                }
                return result;

            });


            return response;
        }

        [Route("upload")]
        [HttpPost]
        public async Task<ApiResponse> Upload([FromUri] string id)
        {
            return await MinIOUploadWithReturn<well_final_report>(id);
        }

        [Route("uploadLampiran")]
        [HttpPost]
        public async Task<ApiResponse> UploadLampiran([FromUri] string recordId)
        {
            var files = HttpContext.Current.Request.Files;
            var response = new ApiResponse();

            foreach (string fileName in files)
            {
                var file = files[fileName];
                var uploadResult = await LampiranUploadPDF<well_final_report>(recordId, file);
                if (!uploadResult.Status.Success)
                {
                    response.Status.Success = false;
                    response.Status.Message = $"Failed to upload {file.FileName}";
                    return response;
                }
            }

            response.Status.Success = true;
            response.Status.Message = "Files uploaded successfully";
            return response;
        }

        [Route("downloadLampiran")]
        [HttpGet]
        public async Task<HttpResponseMessage> Download(string id)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            var filePath = System.Web.HttpContext.Current.Server.MapPath($"~/lampiran/{id}/{id}{id}.xlsx");
            var services = new WellFinalReportServices(this.dataContext);
            var data = await Task.Run<HttpResponseMessage>(() =>
            {
                return services.GenerateLampiran(filePath, id);
            });

            return data;
        }






    }
}
