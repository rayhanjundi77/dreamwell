﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Axa.Web.API.Controllers
{
    [RoutePrefix("api/core/ObjectUomMap")]

    public class ObjectUomMapController : ApiBaseController
    {
        [Route("GetViewAll")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_object_uom_map>>> GetAll()
        {
            var services = new ObjectUomMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_object_uom_map>>>(() =>
            {
                var result = new ApiResponse<List<vw_object_uom_map>>();
                var record = services.GetViewAll(" AND r.is_active = 1");
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
