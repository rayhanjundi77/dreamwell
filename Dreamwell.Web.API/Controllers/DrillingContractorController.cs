﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DataTables.Mvc;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingContractor")]
    public class DrillingContractorController : ApiBaseController
    {
        //DrillingContractorServices services;

        public DrillingContractorController()
        {

        }
        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_drilling_contractor>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new DrillingContractorServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_drilling_contractor>>(() =>
            {
                var result = new ApiResponsePage<vw_drilling_contractor>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_contractor>> GetDetail(string recordId)
        {
            var services = new DrillingContractorServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_contractor>>(() =>
            {
                var result = new ApiResponse<vw_drilling_contractor>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new DrillingContractorServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<drilling_contractor>(), 0, 0);
                
                result = services.GetListDataTables(dataTablesRequest);
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(drilling_contractor record)
        {
            var services = new DrillingContractorServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingContractorServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }
    }
}
