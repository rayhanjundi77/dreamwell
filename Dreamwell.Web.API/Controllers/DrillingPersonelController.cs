﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingPersonel")]

    public class DrillingPersonelController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_drilling_personel>> GetPage(long page, long itemsPerPage)
        {
            var services = new DrillingPersonelServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_drilling_personel>>(() =>
            {
                var result = new ApiResponsePage<vw_drilling_personel>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_personel>> GetDetail(string recordId)
        {
            var services = new DrillingPersonelServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_personel>>(() =>
            {
                var result = new ApiResponse<vw_drilling_personel>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/getByDrillingId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_personel>>> getByDrillingId(string recordId)
        {
            var services = new DrillingPersonelServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_personel>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_personel>>();
                var record = services.GetViewAll(" AND r.drilling_id=@0 ORDER BY r.created_on ASC ", recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("saveAll")]
        [HttpPost]
        public async Task<ApiResponse<List<drilling_personel>>> Save([FromBody] List<drilling_personel> records, [FromUri] string drillingId)
        {
            var services = new DrillingPersonelServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<drilling_personel>>>(() =>
            {
                return services.Save(records, drillingId);
            });
            return response;
        }

        //[Route("save")]
        //[HttpPost]
        //public async Task<ApiResponse> Save(drilling_personel record)
        //{
        //    var services = new DrillingPersonelServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse>(() =>
        //    {
        //        return services.Save(record);
        //    });
        //    return response;
        //}

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingPersonelServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new DrillingPersonelServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling_personel>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }
    }
}
