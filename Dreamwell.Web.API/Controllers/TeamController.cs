﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core;
using CommonTools;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using System.Collections.Generic;
using System;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Team")]

    public class TeamController : ApiBaseController
    {

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_team>> GetDetail(string recordId)
        {
            var services = new TeamServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_team>>(() =>
            {
                var result = new ApiResponse<vw_team>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new TeamServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_team>(), 0, 0);
                result = services.GetListDataWithDataTables(dataTablesRequest);
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_team>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new TeamServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_team>>(() =>
            {
                var result = new ApiResponsePage<vw_team>();
                var data = services.GetViewPerPage(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(team record)
        {
            var services = new TeamServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new TeamServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("{teamId}/userAvailable")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_user>> GetUserAvailable(string teamId, [FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new TeamMemberServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_user>>(() =>
            {
                var result = new ApiResponsePage<vw_application_user>();
                var data = services.GetUserAvailable(marvelDataSourceRequest, teamId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{teamId}/{businessUnitId}/userAvailable")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_user>> GetUserAvailable(string teamId, string businessUnitId, [FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new TeamMemberServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_user>>(() =>
            {
                var result = new ApiResponsePage<vw_application_user>();
                var data = services.GetUserAvailable(marvelDataSourceRequest, teamId, businessUnitId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{teamId}/roleAvailable")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_role>> GetRoleAvailable(string teamId, [FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new TeamRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_role>>(() =>
            {
                var result = new ApiResponsePage<vw_application_role>();
                var data = services.GetRoleAvailable(marvelDataSourceRequest, teamId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{team_member}/GetProjectManager")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_team_member>> GetProjectManager(string team_member, [FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new TeamMemberServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_team_member>>(() =>
            {
                var result = new ApiResponsePage<vw_team_member>();
                var data = services.GetProjectManager(marvelDataSourceRequest, team_member);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("removeMember")]
        [HttpPost]
        public async Task<ApiResponse> RemoveMember([FromBody] string[] userRoleIds)
        {
            var services = new TeamMemberServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(userRoleIds);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        [Route("{teamId}/member")]
        [HttpPost]
        public async Task<DataTablesResponse> GetMember(string teamId, [FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new TeamMemberServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_team_member>(), 0, 0);
                result = services.GetListDataTablesMember(dataTablesRequest, teamId);
                return result;
            });
            return response;
        }

        [Route("addMember")]
        [HttpPost]
        public async Task<ApiResponse> AddMember([FromBody] team_member record)
        {
            var services = new TeamMemberServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    var isNew = false;
                    result.Status.Success = services.SaveEntity(record, ref isNew);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    if (ex.Message.Contains("UK_user_role"))
                    {
                        result.Status.Message = "This user has registered of the role.";
                    }
                    else
                    {
                        result.Status.Message = ex.Message;
                    }
                }
                return result;
            });
            return response;
        }


    }
}
