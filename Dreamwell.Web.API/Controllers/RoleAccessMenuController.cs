﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/RoleAccessMenu")]

    public class RoleAccessMenuController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(role_access_menu record)
        {
            var services = new RoleAccessMenuServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }
        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete(role_access_menu record)
        {
            var services = new RoleAccessMenuServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Delete(record);
            });
            return response;
        }

    }
}
