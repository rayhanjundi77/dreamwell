﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.Models.Entity;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/casing")]
    public class CasingController : ApiBaseController
    {
        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTableService dataTableService = new DataTableService();
                var result = dataTableService.generate<Vw_Casing>(dataTablesRequest);
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<Casing>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new CasingService(this.dataContext);
            var response = await Task.Run<ApiResponsePage<Casing>>(() =>
            {
                return services.Lookup(marvelDataSourceRequest);
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse> GetDetail(string recordId)
        {
            var services = new CasingService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Get(recordId);
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(Casing record)
        {
            var services = new CasingService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record, string.IsNullOrWhiteSpace(record.id));
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new CasingService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Delete(ids);
            });

            return response;
        }
    }
}