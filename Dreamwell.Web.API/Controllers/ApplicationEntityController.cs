﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{ 
    [RoutePrefix("api/core/ApplicationEntity")]
    public class ApplicationEntityController : ApiBaseController
    {

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_entity>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ApplicationEntityServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_entity>>(() =>
            {
                var result = new ApiResponsePage<vw_application_entity>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }
    }
}
