﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.EeasyUI;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using CommonTools.Helper;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using PetaPoco;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Contract")]

    public class ContractController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_contract>> GetPage(long page, long itemsPerPage)
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_contract>>(() =>
            {
                var result = new ApiResponsePage<vw_contract>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var context = this.dataContext;
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                //DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe>(), 0, 0);
                //var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                //result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                Sql qry = null;
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_contract>(), 0, 0);
                if (!context.IsSystemAdministrator)
                {
                    qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 and( r.business_unit_id=@1 or b.parent_unit=@1) ", true, context.PrimaryBusinessUnitId);
                }
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);

                //DataTableService dataTableService = new DataTableService();
                //var result = dataTableService.generate<vw_afe>(dataTablesRequest);

                return result;
            });
            return response;
        }

        /*
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            //var services = new ContractServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                //dataTablesRequest.Order[0].SortDirection = (DataTableOrder.OrderDirection)1;
                //DataTablesResponse result = new DataTablesResponse(0, new List<vw_contract>(), 0, 0);
                //var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                //result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                var query = "";
                if (!this.dataContext.IsSystemAdministrator)
                {
                    query = $" and business_unit_id='{this.dataContext.PrimaryBusinessUnitId}'";
                }
                DataTableService dataTableService = new DataTableService();
                var result = dataTableService.generate<vw_contract>(dataTablesRequest, query);
                return result;
            });
            return response;
        }
        */

        [Route("expired/dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTableExpired([FromBody] DataTableRequest dataTablesRequest)
        {
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                var query = " and end_date < CAST( getdate() AS DATE) ";
                if (!this.dataContext.IsSystemAdministrator)
                {
                    query += $" and business_unit_id='{this.dataContext.PrimaryBusinessUnitId}'";
                }
                DataTableService dataTableService = new DataTableService();
                var result = dataTableService.generate<vw_contract>(dataTablesRequest, query);
                return result;
            });
            return response;
        }

        [Route("nearexpired/dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTableNearExpired([FromBody] DataTableRequest dataTablesRequest)
        {
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                var query = " and end_date > CAST( getdate() AS DATE) and DATEDIFF(DAY, end_date, CAST( getdate() AS DATE)) between -240 and 0 ";
                if (!this.dataContext.IsSystemAdministrator)
                {
                    query += $" and business_unit_id='{this.dataContext.PrimaryBusinessUnitId}'";
                }
                DataTableService dataTableService = new DataTableService();
                var result = dataTableService.generate<vw_contract>(dataTablesRequest, query);
                return result;
            });
            return response;
        }

        [Route("newNear/dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> getNewNearlyContractExpired([FromBody] DataTableRequest dataTablesRequest)
        {
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                var Qry = "select * from vw_contract where is_active=1  and end_date > CAST( getdate() AS DATE) and DATEDIFF(DAY, end_date, CAST( getdate() AS DATE)) between -240 and 0 ";
                if (!this.dataContext.IsSystemAdministrator)
                {
                    Qry += $" and business_unit_id='{this.dataContext.PrimaryBusinessUnitId}'";
                }
                DataTableService dataTableService = new DataTableService();
                var result = dataTableService.generate<vw_contract>(dataTablesRequest, Qry);
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_contract>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_contract>>(() =>
            {
                var result = new ApiResponsePage<vw_contract>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_contract>> GetDetail(string recordId)
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_contract>>(() =>
            {
                var result = new ApiResponse<vw_contract>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(contract record)
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }

     


        [Route("GetContractByMaterial")]
        [HttpPost]
        public async Task<ApiResponse> GetListMaterialByMaterialDataTable([FromBody] ITreeRequest request, [FromUri(SuppressPrefixCheck = false)] string materialId)
        {
            var services = new ContractServices(this.dataContext);

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var data = services.GetTree(materialId);
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_contract_detail>() },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });


                return result;
            });
            return response;
        }


        [Route("getList")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_contract>>> getList()
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_contract>>>(() =>
            {
                var result = new ApiResponse<List<vw_contract>>();
                result = services.GetList();
                return result;
            });
            return response;
        }
        [Route("getByContractNumber/{contractNumber}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_contract>>> getByContractNumber(string contractNumber)
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_contract>>>(() =>
            {
                var result = new ApiResponse<List<vw_contract>>();
                result = services.GetByContractNumber(contractNumber);
                return result;
            });
            return response;
        }
        [Route("getAllContract")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_contract>>> getAllContract()
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_contract>>>(() =>
            {
                var result = new ApiResponse<List<vw_contract>>();
                result = services.GetAllContract();
                return result;
            });
            return response;
        }

        //[Route("getContractByAfeId/{afeId}")]
        //[HttpGet]
        //public async Task<ApiResponse<List<vw_afe_detail>>> getContractByAfeId(string afeId)
        //{
        //    var services = new ContractServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse<List<vw_afe_detail>>>(() =>
        //    {
        //        return services.getContractByAfeId(afeId);
        //    });
        //    return response;
        //}

        [Route("getAll")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_contract>>> getAll()
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_contract>>>(() =>
            {
                var result = new ApiResponse<List<vw_contract>>();
                var record = services.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getAllNew")]
        [HttpGet]
        public async Task<ApiResponseCount<List<vw_contract>>> getAllNew(string isadmin)
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponseCount<List<vw_contract>>>(() =>
            {
                var result = new ApiResponseCount<List<vw_contract>>();
                var record = services.CountTotalContract(isadmin);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("getAllExpired")]
        [HttpGet]
        public async Task<ApiResponseCount<List<vw_contract>>> getAllExpired()
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponseCount<List<vw_contract>>>(() =>
            {
                var result = new ApiResponseCount<List<vw_contract>>();
                var record = services.CountTotalExpiredContract();
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("getAllNearExpired")]
        [HttpGet]
        public async Task<ApiResponseCount<List<vw_contract>>> getAllNearExpired()
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponseCount<List<vw_contract>>>(() =>
            {
                var result = new ApiResponseCount<List<vw_contract>>();
                var record = services.CountTotalNearlyExpiredContract();
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("{recordId}/submit")]
        [HttpPost]
        public async Task<ApiResponse> Submit(string recordId)
        {
            var services = new ContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Submit(recordId);
            });
            return response;
        }
    }
}
