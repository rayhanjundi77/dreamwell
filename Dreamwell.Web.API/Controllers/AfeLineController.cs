﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.JSTree;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/AfeLine")]

    public class AfeLineController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_afe_line>> GetPage(long page, long itemsPerPage)
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_afe_line>>(() =>
            {
                var result = new ApiResponsePage<vw_afe_line>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_afe_line>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_afe_line>>(() =>
            {
                var result = new ApiResponsePage<vw_afe_line>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }


        [Route("tree")]
        [HttpPost]
        public async Task<List<JSTreeResponse<afe_line>>> GetTree()
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<List<JSTreeResponse<afe_line>>>(() =>
            {
                var result = new List<JSTreeResponse<afe_line>>();
                result = services.GetTree();
                return result;
            });
            return response;
        }

        [Route("order")]
        [HttpGet]
        public async Task<ApiResponse> OrderTree([FromUri] String recordId, [FromUri] String parentTargetId, [FromUri] int newOrder)
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.OrderTree(recordId, parentTargetId, newOrder);
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }

                return result;
            });
            return response;
        }

        [Route("lookupByParent/{parentId}")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_afe_line>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string parentId)
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_afe_line>>(() =>
            {
                var result = new ApiResponsePage<vw_afe_line>();
                var data = services.LookupByParent(marvelDataSourceRequest, parentId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_afe_line>> GetDetail(string recordId)
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_afe_line>>(() =>
            {
                var result = new ApiResponse<vw_afe_line>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(afe_line record)
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new AfeLineServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe_line>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }


    }
}
