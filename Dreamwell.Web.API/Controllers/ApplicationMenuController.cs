﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.JSTree;
using CommonTools.SmartNavigation;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/ApplicationMenu")]

    public class ApplicationMenuController : ApiBaseController
    {

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_application_menu>> GetDetail(string recordId)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_application_menu>>(() =>
            {
                var result = new ApiResponse<vw_application_menu>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(application_menu record)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("getTreeByApplicationRoleId/{applicationRoleId}")]
        [HttpPost]
        public async Task<application_menu_category> getTreeByApplicationRoleId(string applicationRoleId, [FromUri(Name = "moduleId")] string applicationModuleId, [FromUri] string categoryId)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<application_menu_category>(() =>
            {
                var result = new application_menu_category();
                result = services.GetApplicationMenu(applicationRoleId, applicationModuleId, categoryId);
                return result;
            });
            return response;
        }

        [Route("GetTree")]
        [HttpPost]
        public async Task<List<JSTreeResponse<application_menu>>> GetTree([FromUri(Name = "moduleId")] string applicationModuleId, [FromUri] string categoryId)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<List<JSTreeResponse<application_menu>>>(() =>
            {
                var result = new List<JSTreeResponse<application_menu>>();
                result = services.GetTreeApplicationMenu(applicationModuleId, categoryId);
                return result;
            });
            return response;
        }

        //[Route("getMenu")]
        //[HttpPost]
        //public async Task<List<application_menu_category>> getMenu(string applicationRoleId)
        //{
        //    var services = new ApplicationMenuServices(this.dataContext);
        //    var response = await Task.Run<List<application_menu_category>>(() =>
        //    {
        //        var result = new List<application_menu_category>();
        //        result = services.GetApplicationMenu(applicationRoleId);
        //        return result;
        //    });
        //    return response;
        //}

        //[Route("getMenuByLogin")]
        //[HttpPost]
        //public async Task<List<application_menu_category>> getMenuByLogin()
        //{
        //    var services = new ApplicationMenu(this.dataContext);
        //    var response = await Task.Run<List<application_menu_category>>(() =>
        //    {
        //        var result = new List<application_menu_category>();
        //        result = services.GetMenuByLogin();
        //        return result;
        //    });
        //    return response;
        //}


        [Route("order")]
        [HttpGet]
        public async Task<ApiResponse> OrderTree([FromUri] String recordId, [FromUri] String parentTargetId, [FromUri] int newOrder)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.OrderTree(recordId, parentTargetId, newOrder);
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }

                return result;
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_application_menu>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("GetByUserId")]
        [HttpPost]
        public async Task<SmartNavigation> GetPage([FromUri] string moduleName, [FromUri] string category)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<SmartNavigation>(() =>
            {
                var result = services.GetNavigationByUserId(moduleName);
                return result;
            });
            return response;
        }

        [Route("{businessUnitRoute}/GetByUserId")]
        [HttpPost]
        public async Task<SmartNavigation> GetPage([FromUri] string moduleName, string businessUnitRoute, [FromUri] string category)
        {
            var services = new ApplicationMenuServices(this.dataContext);
            var response = await Task.Run<SmartNavigation>(() =>
            {
                var result = new SmartNavigation();
                try
                {
                    if (businessUnitRoute.Contains("APH"))
                    {
                        var BUnit = business_unit.FirstOrDefault("WHERE id=@0", userSession.PrimaryBusinessUnitId);

                        if (string.IsNullOrEmpty(BUnit?.unit_code))
                            throw new Exception($"Business unit code is null on unit id {userSession.PrimaryBusinessUnitId}");
                        if (userSession.SysAdmin)
                        {
                            result = services.GetNavigationByUserId(moduleName, null, "SIDEBAR");
                        
                        }
                        else
                        {
                            result = services.GetNavigationByUserId(moduleName, BUnit?.unit_code, "SIDEBAR");
                        }
                    }
                    else
                    {
                        var BUnit = business_unit.FirstOrDefault("WHERE unit_code=@0", businessUnitRoute);
                        if (!userSession.SysAdmin && !userSession.PrimaryBusinessUnitId.Equals(BUnit?.id))
                            throw new Exception($"This user doesn't cross business unit, current is {userSession.PrimaryBusinessUnit} this route is {businessUnitRoute}");
                        result = services.GetNavigationByUserId(moduleName, businessUnitRoute, "SIDEBAR");

                    }
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                }

                return result;
            });
            return response;
        }
    }
}
