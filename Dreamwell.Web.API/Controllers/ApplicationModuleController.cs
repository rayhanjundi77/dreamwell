﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.JSTree;
using CommonTools.SmartNavigation;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/ApplicationModule")]

    public class ApplicationModuleController : ApiBaseController
    {

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_application_module>> GetDetail(string recordId)
        {
            var services = new ApplicationModuleRepository(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_application_module>>(() =>
            {
                var result = new ApiResponse<vw_application_module>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(application_module record)
        {
            var services = new ApplicationModuleServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

     
        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new ApplicationModuleRepository(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }


        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_application_module>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ApplicationModuleServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_application_module>>(() =>
            {
                var result = new ApiResponsePage<vw_application_module>();
                var data = services.GetViewPerPage(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("GetData")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_application_module>>> Get([FromUri] string moduleName)
        {
            var services = new ApplicationModuleServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_application_module>>>(() =>
            {
                var result = new ApiResponse<List<vw_application_module>>();
                try
                {
                    var data = services.GetViewAll(" AND r.module_name like @0 ", $"%{moduleName}%");
                    result.Status.Success = true;
                    result.Data = data;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                }
                return result;
            });
            return response;
        }

    }
}
