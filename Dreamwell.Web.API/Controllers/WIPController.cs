﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.JSTree;
using static Dreamwell.DataAccess.dreamwellRepo;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/wip")]

    public class WIPController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_wip>> GetPage(long page, long itemsPerPage)
        {
            var services = new WIPServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_wip>>(() =>
            {
                var result = new ApiResponsePage<vw_wip>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_wip>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new WIPServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_wip>>(() =>
            {
                var result = new ApiResponsePage<vw_wip>();
                //var temp = services.GetIadc(marvelDataSourceRequest);
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }


        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_wip>> GetDetail(string recordId)
        {
            var services = new WIPServices(this.dataContext);
            var result = new ApiResponse<vw_wip>();

            try
            {
                var record = services.GetViewById(recordId); 
                result.Status.Success = (record != null);
                result.Data = record;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.ToString();
            }

            return result;
        }


        [Route("{wirid}/detailbyId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_wip>>> getbywirId(string wirid)
        {
            var services = new WIPServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_wip>>>(() =>
            {
                var result = new ApiResponse<List<vw_wip>>();
                try
                {
                    result = services.getByDetail(wirid);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(wip record)
        {
            var services = new WIPServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("wellInformation/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveWellInformation(well_information record)
        {
            var services = new WellInformationServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("{wipid}/detailwibyId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_information>>> getwibywipid(string wipid)
        {
            var services = new WellInformationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_information>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_information>>();
                try
                {
                    result = services.getByDetail(wipid);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("annulus/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveAnnulus(WIPViewModel record)
        {
            var services = new AnnulusServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("{wipid}/detailannulusbyId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_annulus>>> getannulusbywipid(string wipid)
        {
            var services = new AnnulusServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_annulus>>>(() =>
            {
                var result = new ApiResponse<List<vw_annulus>>();
                try
                {
                    result = services.getByDetail(wipid);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }
        /*
        [Route("workload/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveWokload(workload record)
        {
            var services = new WorkloadServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }
        */
        [Route("approvalwip/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveApproval(approval_wip record)
        {
            var services = new WIPApprovalServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }


        [Route("{wipid}/detailapprovalwibyId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_approval_wip>>> getapprovalwibywipid(string wipid)
        {
            var services = new WIPApprovalServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_approval_wip>>>(() =>
            {
                var result = new ApiResponse<List<vw_approval_wip>>();
                try
                {
                    result = services.getByDetail(wipid);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("commentswip/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveComments(comments_wip record)
        {
            var services = new CommentsWIPServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }


        [Route("{wipid}/detailcommentbyid")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_comments_wip>>> getcommentbywipid(string wipid)
        {
            var services = new CommentsWIPServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_comments_wip>>>(() =>
            {
                var result = new ApiResponse<List<vw_comments_wip>>();
                try
                {
                    result = services.getByDetail(wipid);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("attachmentwip/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveAttachmentWip(attachment_wip record)
        {
            var services = new WIPAttachServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                System.Diagnostics.Debug.WriteLine("ini attach",record);
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new WIPServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("moc/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveMOC(management_of_change record)
        {
            var services = new MOCServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new WIPServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_wip>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }



    }
}
