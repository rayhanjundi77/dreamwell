﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingBhaBit")]

    public class DrillingBhaBitController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save([FromBody] List<drilling_bha_bit> record)
        {
            var services = new DrillingBhaBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("getBHAandBITDataDetail/{wellId}/{startDate}/{endDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bha_bit_component_detail>>> getBHAandBitData(string wellId, DateTime startDate, DateTime endDate)
        {
            var services = new DrillingBhaBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit_component_detail>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_bit_component_detail>>();
                var record = services.GetBhaAndBitDetail(wellId, startDate, endDate);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("getBHAmudlogingdata/{wellId}/{startDate}/{endDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bha_bit>>> getBHAMudlogingData(string wellId, DateTime startDate, DateTime endDate)
        {
            var services = new DrillingBhaBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_bit>>();
                var record = services.getBhaMudloginData(wellId, startDate, endDate);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }


        [Route("getDetail/{recordId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bha_bit>>> GetDetail(string recordId)
        {
            var services = new DrillingBhaBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_bit>>();
                result = services.GetDetail(recordId);
                return result;
            });
            return response;
        }

        [Route("getDetailMud/{recordId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bha_bit>>> GetDetailMud(string recordId)
        {
            var services = new DrillingBhaBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_bit>>();
                result = services.GetDetailMud(recordId);
                return result;
            });
            return response;
        }

        [Route("getDetailMudPdf/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bha_bit>>> GetDetailPdf(string wellId)
        {
            var services = new DrillingBhaBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_bit>>();
                result = services.GetDetailMudPdf(wellId);
                return result;
            });
            return response;
        }
    }
}
