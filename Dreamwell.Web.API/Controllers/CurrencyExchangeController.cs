﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using Dreamwell.Infrastructure.DataTables;
using DataTables.Mvc;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/CurrencyExchange")]

    public class CurrencyExchangeController : ApiBaseController
    {

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_currency>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_currency>>(() =>
            {
                var result = new ApiResponsePage<vw_currency>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_currency>> GetPage(long page, long itemsPerPage)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_currency>>(() =>
            {
                var result = new ApiResponsePage<vw_currency>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detailCurrency")]
        [HttpGet]       
        public async Task<ApiResponse<vw_currency_exchange>> GetDetailCurrency(string recordId)
        {           
            var services = new CurrencyExchange(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_currency_exchange>>(() =>
            {
                var result = new ApiResponse<vw_currency_exchange>();              
                var sqlqry = PetaPoco.Sql.Builder.Append(" where r.is_active=@0 and r.currency_id=@1", true, recordId);
                var data = services.GetViewFirstOrDefault(sqlqry);
                //result.Status.Success = true;
                result.Status.Success = (data != null);
                // if isnull(data) 
                result.Data = data;
                return result;                
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_currency_exchange>> GetDetail(string recordId)
        {
            var services = new CurrencyExchange(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_currency_exchange>>(() =>
            {
                var result = new ApiResponse<vw_currency_exchange>();
                var sqlFilter = Sql.Builder.Append(" WHERE r.is_active=@0 AND r.source_id=@1 ", true, recordId);
                var getAllDetail = services.GetViewFirstOrDefault(sqlFilter);
                result.Status.Success = true;
                result.Data = getAllDetail;
                return result;
            });
            return response;
        }

        [Route("saveall")]
        [HttpPost]
        public async Task<ApiResponse<bool>> SaveAll([FromBody] currency_exchange[] record)
        {
            var services = new CurrencyExchange(this.dataContext);
            var response = await Task.Run<ApiResponse<bool>>(() =>
            {
                var result = services.UpdateRecords(record);
                return result;
            });
            return response;
        }

        [Route("delete/{id}")]
        [HttpPost]
        public async Task<ApiResponse> delete(string id)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.DeleteById(id);
            });
            return response;
        }

        [Route("dataTable/{recordId}")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest, string recordId)
        {
            var services = new CurrencyExchange(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_currency_exchange>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.source_id=@0 AND r.is_active=@1 ", recordId, false);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }
    }
}
