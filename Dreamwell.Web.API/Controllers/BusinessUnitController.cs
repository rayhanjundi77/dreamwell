﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using CommonTools.JSTree;
using System.Collections.Generic;
using System;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.EeasyUI;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using CommonTools.Helper;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/BusinessUnit")]
    public class BusinessUnitController : ApiBaseController
    {
        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_business_unit>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_business_unit>>(() =>
            {
                var result = new ApiResponsePage<vw_business_unit>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("Newlookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_business_unit>> GetPageNew([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_business_unit>>(() =>
            {
                var result = new ApiResponsePage<vw_business_unit>();
                var data = services.NewLookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookupParent")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_business_unit>> GetPageParent([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_business_unit>>(() =>
            {
                var result = new ApiResponsePage<vw_business_unit>();
                var data = services.LookupParent(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }


        [Route("tree")]
        [HttpPost]
        public async Task<List<JSTreeResponse<business_unit>>> GetTree()
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<List<JSTreeResponse<business_unit>>>(() =>
            {
                var result = new List<JSTreeResponse<business_unit>>();
                result = services.GetTreeBusinessUnit();
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_business_unit>> GetDetail(string recordId)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_business_unit>>(() =>
            {
                var result = new ApiResponse<vw_business_unit>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("GetId")]
        [HttpGet]
        public async Task<ApiResponse> GetId([FromUri]string aphName)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var record = business_unit.FirstOrDefault("WHERE unit_code=@0",aphName);
                result.Status.Success = (record != null);
                result.Data = record?.id;
                return result;
            });
            return response;
        }



        [Route("{recordId}/UpdateLeader")]
        [HttpPost]
        public async Task<ApiResponse> UpdateLeader(string recordId, [FromUri] string appuserid)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.UpdateLeader(recordId, appuserid);
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(business_unit record)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.SaveAndCreateTeam(record);
            });
            return response;
        }

        [Route("update")]
        [HttpPost]
        public async Task<ApiResponse> Update(business_unit record)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.UpadateBussiessUnit(record);
                // return services.Save(record);
            });
            return response;
        }
        [Route("{businessUnitId}/field/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveFields(String businessUnitId, [FromBody] string[] Ids)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.BusinessUnitFieldSave(businessUnitId, Ids);
            });
            return response;
        }


        [Route("order")]
        [HttpGet]
        public async Task<ApiResponse> OrderTree([FromUri] String recordId, [FromUri] String parentTargetId, [FromUri] int newOrder)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.OrderTree(recordId, parentTargetId, newOrder);
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }

                return result;
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete(string[] ids)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    throw;
                }
                return result;
            });
            return response;
        }

        [Route("combotree")]
        [HttpPost]
        public async Task<ApiResponse> ComboTree([FromBody] TreeRequest request)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var data = services.GetComboTree(request);
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_business_unit>("unit_name") },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });


                return result;
            });
            return response;
        }

        [Route("combotree/field")]
        [HttpPost]
        public async Task<ApiResponse> ComboTreeField([FromBody] TreeRequest request,[FromUri] string unitId)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var data = services.GetComboTreeAssetByUnit(request, unitId);
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_business_unit_field>("field_name") },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });


                return result;
            });
            return response;
        }

        

    }
}
