﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingBhaComponent")]

    public class DrillingBhaComponentController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_drilling_bha_comp>> GetPage(long page, long itemsPerPage)
        {
            var services = new DrillingBhaComponentServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_drilling_bha_comp>>(() =>
            {
                var result = new ApiResponsePage<vw_drilling_bha_comp>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_bha_comp>> GetDetail(string recordId)
        {
            var services = new DrillingBhaComponentServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_bha_comp>>(() =>
            {
                var result = new ApiResponse<vw_drilling_bha_comp>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/getByDrillingBha")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bha_comp_detail>>> getByDrillingBha(string recordId)
        {
            var services = new DrillingBhaComponentDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_comp_detail>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_comp_detail>>();
                var record = services.GetViewAll(" AND d.drilling_bha_id=@0 AND r.is_active=@1 ORDER BY r.created_on ASC ", recordId, true);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(drilling_bha_comp record)
        {
            var services = new DrillingBhaComponentServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingBhaComponentServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }
    }
}
