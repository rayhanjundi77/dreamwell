﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingHoleAndCasing")]

    public class DrillingHoleAndCasingController : ApiBaseController
    {

      
        [Route("new/{wellId}")]
        [HttpPost]
        public async Task<ApiResponse<drilling_hole_and_casing>> New(string wellId)
        {
            var services = new DrillingHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<drilling_hole_and_casing>>(() =>
            {
                return services.New(wellId);
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse<drilling>> Save(DrillingHoleAndCasingViewModel record)
        {
            var services = new DrillingHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<drilling>>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("savecasing")]
        [HttpPost]
        public async Task<ApiResponse> SaveCasing(drilling_hole_and_casing records)
        {
            var services = new DrillingHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.SaveCasing(records);
            });
            return response;
        }

        [Route("{recordId}/multicasing")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetById(string recordId)
        {
            var services = new DrillingHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
                return services.getDetailId(recordId);
                //var record = services.getByField(fieldId);
                //result.Status.Success = (record != null);
                //result.Data = record.Data;
                //return result;
            });
            return response;
        }

        [Route("{dataId}/ListMulticasing")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetId(string dataId)
        {
            var services = new DrillingHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
                return services.getListMulticasing(dataId);
                //var record = services.getByField(fieldId);
                //result.Status.Success = (record != null);
                //result.Data = record.Data;
                //return result;
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }
    }
}
