﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Cors;
using Dreamwell.Infrastructure.Acl;
using Dreamwell.Infrastructure.Session;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.BusinessLogic.Core.Services;

using Dreamwell.Infrastructure.WebApi;

namespace Dreamwell.Web.API.Controllers
{

    [JWTAuthenticationFilter]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FileResultById : IHttpActionResult 
    {
        public string RecordId { get; }
        DataContext Dc { get; }

        public FileResultById(DataContext dc, string recordId)
        {
            RecordId = recordId;
            Dc = dc;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            BusinessLogic.Core.Minio.MinioStorage<filemaster> u = new BusinessLogic.Core.Minio.MinioStorage<filemaster>(this.Dc);
            var minioFile = u.GetFile(RecordId);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var ms = minioFile.StreamFile;
            ms.Seek(0, SeekOrigin.Begin);
            result.Content = new StreamContent(ms);
            result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            //{
            //    FileName = minioFile.FileName
            //};
            //result.Content.Headers.ContentType =
            //    new MediaTypeHeaderValue(minioFile.MimeType);
            //result.Content.Headers.ContentLength = ms.Length;
            return Task.FromResult(result);
        }
    }

    [RoutePrefix("api/core/GetFile")]
    public class GetFileController : ApiBaseController
    {

     


        [Route("MinIO/GetImage")]
        [HttpGet]
        public IHttpActionResult Get([FromUri(Name ="d")] string fileId)
        {
            DataContext  dc = new DataContext(Dreamwell.BusinessLogic.Core.SysConfig.UserSetup.sysAdminId);
            return new FileResultById(dc, fileId);
        }

        [Route("getFileMaster")]
        [HttpGet]
        public IHttpActionResult GetByID([FromUri(Name = "d")] string fileId)
        {
            DataContext dc = new DataContext(Dreamwell.BusinessLogic.Core.SysConfig.UserSetup.sysAdminId);
            return new FileResultById(dc, fileId);
        }

        [HttpGet]
        [Route("getFileMaster/{fieldId}")]
        public IHttpActionResult GetByIdMaster(string fieldId)
        {
          
            DataContext dc = new DataContext(Dreamwell.BusinessLogic.Core.SysConfig.UserSetup.sysAdminId);
            return new FileResultById(dc, fieldId);
        }
 
        [Route("{recordId}/detailImageFile")]
        [HttpGet]
        public async Task<ApiResponse<vw_filemaster>> GetNewMasterFile(string recordId)
        {

            var services = new FileMasterServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_filemaster>>(() =>
            {
                var result = new ApiResponse<vw_filemaster>();
                var record = services.GetDetailFile(recordId);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;

        }

        [Route("{recordId}/detailImageFileById")]
        [HttpGet]
        public async Task<ApiResponse<vw_filemaster>> GetNewMasterFileById(string recordId)
        {

            var services = new FileMasterServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_filemaster>>(() =>
            {
                var result = new ApiResponse<vw_filemaster>();
                var record = services.GetDetailFileById(recordId);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;

        }


    }
}
