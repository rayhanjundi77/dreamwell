﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{ 
    [RoutePrefix("api/core/TVDPlan")]
    public class TVDPlanController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse<well>> Save(WellViewModel record)
        {
            var services = new TVDPlanServices(this.dataContext);
            var response = await Task.Run<ApiResponse<well>>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("{recordId}/detailbywellid")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_tvd_plan>>> GetTVDbyWellID(string recordId)
        {
            var services = new TVDPlanServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_tvd_plan>>>(() =>
            {
                var result = new ApiResponse<List<vw_tvd_plan>>();
                var record = services.GetViewAll(" AND r.well_id=@0 ", recordId);
                result.Status.Success = (record != null);
                record.Sort((a, b) => a.no.CompareTo(b.no));
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
