﻿using System;
using CommonTools;
using DataTables.Mvc;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web;
using System.IO;
using PetaPoco;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Stone")]
    public class StoneController : ApiBaseController
    {
        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new StoneServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_stone>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_stone>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new StoneServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_stone>>(() =>
            {
                var result = new ApiResponsePage<vw_stone>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookupAll")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_stone>> LookupAll([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new StoneServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_stone>>(() =>
            {
                var result = new ApiResponsePage<vw_stone>();
                var data = services.LookupAll(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = 1000;
                result.TotalItems = 1000;
                result.ItemsPerPage = 1000;
                result.Items = data.Items;
                return result;
            });
            return response;
        }


        [Route("lookupUser")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_stone>> LookupUser([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new StoneServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_stone>>(() =>
            {
                var result = new ApiResponsePage<vw_stone>();
                var data = services.LookupUser(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }


        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_stone>> GetDetail(string recordId)
        {
            var services = new StoneServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_stone>>(() =>
            {
                var result = new ApiResponse<vw_stone>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(stone record)
        {
            var services = new StoneServices(this.dataContext);
            //var httpRequest = HttpContext.Current.Request;
          
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    //var isNew = false;
                    //var record = dataContext.MapToEntity<stone>(httpRequest.Form, ref isNew) as stone;
                    result = services.Save(record);

                }
                catch (Exception ex)
                {
                    result.Status.Message = ex.Message;
                }
                return result;

            });


            return response;
        }

        [Route("upload")]
        [HttpPost]
        public async Task<ApiResponse> Upload([FromUri] string stoneId)
        {
        
         
             return await LithologyComponentUpload<stone>(stoneId);
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new StoneServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

    }
}
