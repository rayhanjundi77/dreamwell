﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/WellBit")]

    public class WellBitController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(well_bit record)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("lookupByWell/{wellId}")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_well_bit>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well_bit>>(() =>
            {
                var result = new ApiResponsePage<vw_well_bit>();
                var data = services.Lookup(marvelDataSourceRequest, wellId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("getByWell/{wellId}")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_well_bit>> GetLookupBit([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well_bit>>(() =>
            {
                var result = new ApiResponsePage<vw_well_bit>();
                var data = services.Lookup(marvelDataSourceRequest, wellId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest, [FromUri] string wellId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_well_bit>(), 0, 0);
                var qry = PetaPoco.Sql.Builder;
                qry.Append(" WHERE r.is_active=@0 AND r.well_id =@1",true, wellId);
                //qry.Append(String.Format("ORDER BY {0} {1}", "r.bit_number", "ASC"));
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("getDetail/{recordId}")]
        [HttpGet]
        public async Task<ApiResponse<vw_well_bit>> GetDetail(string recordId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_well_bit>>(() =>
            {
                var result = new ApiResponse<vw_well_bit>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("GetLastRun/{wellId}/{drillingId}/{wellBitId}")]
        [HttpGet]
        public async Task<ApiResponse<int>> GetLastRun(string wellId, string drillingId, string wellBitId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<int>>(() =>
            {
                var result = new ApiResponse<int>();
                result = services.GetLastRun(wellId, drillingId, wellBitId);
                return result;
            });
            return response;
        }

        [Route("isExistBit/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<bool>> isExistBit(string wellId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<bool>>(() =>
            {
                var result = new ApiResponse<bool>();
                var record = services.GetFirstOrDefault(" AND well_id=@0 AND is_active = @1 ", wellId, true);
                result.Status.Success = true;
                result.Data = record == null ? false : true;
                return result;
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("getWellBitByWell/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_bit>>> getWellBhaByWell(string wellId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_bit>>();
                var record = services.GetViewAll(" AND r.well_id=@0 AND r.is_active = @1 ", wellId, true);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getWellBitId/{recordId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_bit>>> getWellBitId(string recordId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_bit>>();
                var record = services.GetViewAll(" AND r.id=@0 AND r.is_active = @1 ", recordId, true);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
