﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/WellBha")]

    public class WellBhaController : ApiBaseController
    {

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest, [FromUri] string wellId)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_well_bha>(), 0, 0);

                // var qry = PetaPoco.Sql.Builder;
                // qry.Append(String.Format("SELECT * from vw_well_bha"));
                //  qry.Append("WHERE r.is_active=@0 AND r.well_id = @1 ", true, wellId);
                //  qry.Append("ORDER BY r.bit_number ASC");


                var qry = PetaPoco.Sql.Builder;
                qry.Append("WHERE r.is_active=@0 AND r.well_id =@1", true, wellId);
                // var qry = PetaPoco.Sql.Builder.Append("WHERE r.is_active=@0 AND r.well_id =@1", true, wellId);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("getWellBhaByWell/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_bha>>> getWellBhaByWell(string wellId)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_bha>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_bha>>();
                var record = services.GetViewAll(" AND r.well_id=@0 AND r.is_active = @1 ", wellId, true);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("isExistBha/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<bool>> isExistBha(string wellId)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<ApiResponse<bool>>(() =>
            {
                var result = new ApiResponse<bool>();
                var record = services.GetFirstOrDefault(" AND well_id=@0 AND is_active = @1 ", wellId, true);
                result.Status.Success = true;
                result.Data = record == null ? false : true;
                return result;
            });
            return response;
        }

        [Route("getDetail/{recordId}")]
        [HttpGet]
        public async Task<ApiResponse<vw_well_bha>> GetDetail(string recordId)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_well_bha>>(() =>
            {
                var result = new ApiResponse<vw_well_bha>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(WellBhaViewModel record)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("deletecomponent/{id}")]
        [HttpPost]
        public async Task<ApiResponse> Save(string id)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Delete(id);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }

        [Route("lookupByWell/{wellId}")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_well_bha>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well_bha>>(() =>
            {
                var result = new ApiResponsePage<vw_well_bha>();
                var data = services.Lookup(marvelDataSourceRequest, wellId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("getByWell/{wellId}")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_well_bha>> GetLookupBha([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        {
            var services = new WellBhaServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well_bha>>(() =>
            {
                var result = new ApiResponsePage<vw_well_bha>();
                var data = services.Lookup(marvelDataSourceRequest, wellId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

    }
}
