﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{ 
    [RoutePrefix("api/core/DrillingHazard")]
    public class DrillingHazardController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(WellViewModel record)
        {
            var services = new DrillingHazardServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("{recordId}/detailbywellid")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hazard>>> GetDatabyWellID(string recordId)
        {
            var services = new DrillingHazardServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hazard>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_hazard>>();
                var record = services.GetViewAll(" AND r.well_id=@0 ORDER BY r.depth ASC ", recordId);
                result.Status.Success = (record != null);
                //record.Sort((a, b) => a.id.CompareTo(b.id));
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
