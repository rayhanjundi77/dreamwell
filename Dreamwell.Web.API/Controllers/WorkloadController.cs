﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System;
using System.Linq;
using System.Net.NetworkInformation;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/WIRWorkload")]
    public class WorkloadController : ApiBaseController
    {
        //[Route("save")]
        //[HttpPost]
        //public async Task<ApiResponse> Save([FromBody] WorkloadWirRequest record)
        //{
        //    if (record == null)
        //    {
        //        return new ApiResponse
        //        {
        //            Status = new ResponseStatus { Success = false, Message = "Record is null" }
        //        };
        //    }

        //    var services = new WorkloadServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse>(() =>
        //    {
        //        return services.Save(record); // Memanggil service untuk menyimpan data
        //    });

        //    if (response.Status == null)
        //    {
        //        response.Status = new ResponseStatus { Success = false, Message = "Unknown error occurred" };
        //    }

        //    return response;
        //}


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(workload_wir record)
        {
            if (record == null)
            {
                return new ApiResponse
                {
                    Status = new ResponseStatus { Success = false, Message = "Record is null" }
                };
            }

            var services = new WorkloadServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });

            // Periksa apakah response dari service sudah memiliki status yang sesuai
            if (response.Status == null)
            {
                response.Status = new ResponseStatus { Success = false, Message = "Unknown error occurred" };
            }

            return response;
        }


        [Route("listByWellId/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_workload_wir>>> GetByWellId(string wellId)
        {
            var services = new WorkloadServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_workload_wir>>>(() =>
            {
                return services.GetByWellId(wellId);
            });
            return response;
        }

    }
}