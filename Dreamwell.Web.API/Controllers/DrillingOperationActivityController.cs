﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using System.Dynamic;
using System.Linq;
using static Dreamwell.DataAccess.dreamwellRepo;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingOperationActivity")]

    public class DrillingOperationActivityController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_drilling_operation_activity>> GetPage(long page, long itemsPerPage)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_drilling_operation_activity>>(() =>
            {
                var result = new ApiResponsePage<vw_drilling_operation_activity>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_operation_activity>> GetDetail(string recordId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_operation_activity>>(() =>
            {
                var result = new ApiResponse<vw_drilling_operation_activity>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/getByDrillingId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_operation_activity>>> getByDrillingId(string recordId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_operation_activity>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_operation_activity>>();
                var record = services.GetViewAll(" AND r.drilling_id=@0 ", recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getByDrillingId/{recordId}/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetByDrillingId(string recordId, string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                return services.GetActivityByDrillingId(recordId, wellId);
                //return services.newDrilingActivity(recordId, wellId);
            });
            return response;
        }

        [Route("getByDrillingOperationId/{recordId}/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetByDrillingOpertaionId(string recordId, string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                return services.GetDrillingOperationActivityes(recordId, wellId);
            });
            return response;
        }

        [Route("getByDrillingWellIdas/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetByDrillingWellIdas(string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                return services.GetActivityByDrillingWellId(wellId);
            });
            return response;
        }


        [Route("getHoleandCasing/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetHoleandCasing(string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                return services.GetActivityByDrillingWellId(wellId);
            });
            return response;
        }




        [Route("getByDrillingWellIds/{recordId}/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetByDrillingWellId(string recordId, string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                return services.GetActivityByDrillingWellIds(recordId, wellId);
            });
            return response;
        }


        [Route("GetByDrillingDescWellId/{recordId}/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_operation_activity>>> GetByDrillingDescWellId(string recordId, string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_operation_activity>>>(() =>
            {
                return services.GetDescOperation(recordId, wellId);
            });
            return response;
        }

        [Route("getByWellId")]
        [HttpGet]
        public async Task<ApiResponse<dynamic>> GetByWellId([FromUri] string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<dynamic>>(() =>
            {
                var result = new ApiResponse<dynamic>();
                result.Data = new ExpandoObject();
                var data = services.GetViewAll(" AND d.well_id=@0 ", wellId);

                var query = data?.GroupBy(p => p.drilling_date, (key, g) => new
                {
                    drilling_date = key,
                    data = g
                });

                result.Data = query;
                result.Status.Success = true;
                return result;
            });
            return response;
        }

        [Route("getByWellIdNew")]
        [HttpGet]
        public async Task<ApiResponse<dynamic>> GetByWellIdNew([FromUri] string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<dynamic>>(() =>
            {
                var result = new ApiResponse<dynamic>();
                result.Data = new ExpandoObject();
                //var data = services.GetViewAll(" AND d.well_id=@0 ", wellId);
                var data = services.getByWellIdOnly(wellId);
                List<vw_drilling_operation_activity> doa = data.Data;
                var query = doa?.GroupBy(p => p.drilling_date, (key, g) => new
                {
                    drilling_date = key,
                    data = g
                });

                result.Data = query;
                result.Status.Success = true;
                return result;
            });
            return response;
        }

        [Route("getByWellId/{wellId}/{drillingDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_operation_activity>>> getByWellId(string wellId, DateTime drillingDate)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_operation_activity>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_operation_activity>>();
                var record = services.getByWellId(wellId, drillingDate);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("getbydrillingstartdateandwellid/{wellId}/{startDate}/{endDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_operation_activity>>> getOperationAndActivityByStartDateAndEnddate(string wellId, DateTime startDate, DateTime endDate)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_operation_activity>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_operation_activity>>();
                var record = services.getStartDateAndAroundDate(wellId, startDate, endDate);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(drilling_operation_activity record)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling_operation_activity>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }
    }
}
