﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using System.Net.Http;
using System.Net;
using System.Web;
using System.IO;
using System.Linq;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/FileMaster")]

    public class FileMasterController : ApiBaseController
    {

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_filemaster>>> GetFilemaster(string recordId)
        {
            var services = new FileMasterServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_filemaster>>>(() =>
            {
                var result = new ApiResponse<List<vw_filemaster>>();
                return services.GetbyrecordId(recordId);
                //var record = services.getByField(fieldId);
                //result.Status.Success = (record != null);
                //result.Data = record.Data;
                //return result;
            });
            return response;
        }
        //[Route("upload/{recordId}")]
        //[HttpPost]
        //public async Task<ApiResponse> Upload(string recordId,[FromUri] string entityName)
        //{
        //    var httpRequest = HttpContext.Current.Request;

        //    HttpRequestMessage request = this.Request;
        //    if (!request.Content.IsMimeMultipartContent())
        //    {
        //        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //    }

        //    if (httpRequest.Files.Count < 1)
        //    {
        //        throw new HttpResponseException(HttpStatusCode.BadRequest);
        //    }

        //    var response = await Task.Run<ApiResponse>(() =>
        //    {
        //        var result = new ApiResponse();
        //        try
        //        {
        //            var services = new FileMasterServices(this.dataContext);
        //            string root = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
        //            root = Path.Combine(root, "Uploads\\Excel Template\\");
        //            var provider = new MultipartFormDataStreamProvider(root);
        //            foreach (string file in httpRequest.Files)
        //            {
        //                var postedFile = httpRequest.Files[file];

        //                var realFileName = postedFile.FileName;
        //                var extfile = Path.GetExtension(realFileName);
        //                var fileName = string.Format("{0}_{1}{2}", Path.GetFileNameWithoutExtension(postedFile.FileName),
        //                    Guid.NewGuid().ToString("D"), extfile);

        //                string mimeType = MimeMapping.GetMimeMapping(realFileName);

        //                var servicesEntity = new ApplicationEntityServices(this.dataContext);
        //                var entityId = servicesEntity.GetEntityIdByName(entityName);
        //                if (string.IsNullOrEmpty(entityId))
        //                    throw new Exception("EntityName not found");


        //                postedFile.SaveAs(Path.Combine(root, postedFile.FileName));
        //                result = services.SaveUpload(fileName, realFileName, extfile, mimeType, postedFile.ContentLength, entityId, recordId);

        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            appLogger.Error(ex);
        //            result.Status.Success = false;
        //            result.Status.Message = ex.Message;
        //        }
        //        return result;
        //    });
        //    return response;
        //}



    }
}
