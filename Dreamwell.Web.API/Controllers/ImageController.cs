﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using System.IO;
using System.Web;


namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Image")]

    public class ImageController : ApiBaseController
    {
        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_tbl_image>> GetPage(long page, long itemsPerPage)
        {
            var services = new ImageServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_tbl_image>>(() =>
            {
                var result = new ApiResponsePage<vw_tbl_image>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_tbl_image>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ImageServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_tbl_image>>(() =>
            {
                var result = new ApiResponsePage<vw_tbl_image>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_tbl_image>> GetDetail(string recordId)
        {
            var services = new ImageServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_tbl_image>>(() =>
            {
                var result = new ApiResponse<vw_tbl_image>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("uploadImage")]
        [HttpPost]
        public async Task<ApiResponse> UploadImage()
        {
            var response = new ApiResponse();
            var file = HttpContext.Current.Request.Files["imageFile"]; // Mengambil file dari FormData

            if (file != null && file.ContentLength > 0)
            {
                var bhaComponentId = HttpContext.Current.Request.Form["bha_component_id"]; // Mendapatkan bha_component_id dari FormData

                // Membuat instance baru dari tbl_image untuk data baru
                var record = new tbl_image
                {
                    id = Guid.NewGuid().ToString(),  // Buat ID baru jika diperlukan
                    bha_component_id = bhaComponentId,
                };

                using (var memoryStream = new MemoryStream())
                {
                    file.InputStream.CopyTo(memoryStream);
                    record.image = memoryStream.ToArray(); // Menyimpan file sebagai byte array
                }

                var services = new ImageServices(this.dataContext);
                response =  services.Save(record); // Simpan data baru ke database
            }
            else
            {
                response.Status.Message = "No image file provided.";
            }

            return response;
        }


        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new ImageServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new ImageServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_tbl_image>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }



    }
}
