﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.EeasyUI;
using Newtonsoft.Json;
using CommonTools.Helper;
using Newtonsoft.Json.Serialization;
using System.Linq;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Material")]

    public class MaterialController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_material>> GetPage(long page, long itemsPerPage)
        {
            var services = new MaterialServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_material>>(() =>
            {
                var result = new ApiResponsePage<vw_material>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_material>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new MaterialServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_material>>(() =>
            {
                var result = new ApiResponsePage<vw_material>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookupByAfeLine/{afeLineId}")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_material>> GetPageByAfeLine([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string afeLineId)
        {
            var services = new MaterialServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_material>>(() =>
            {
                var result = new ApiResponsePage<vw_material>();
                var data = services.LookupByAfeLineId(marvelDataSourceRequest, afeLineId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items.OrderBy(x => x.description).ToList();
                return result;
            });
            return response;
        }


        [Route("LookupByAfeLineAndType/{afeLineId}/{itemType}")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_material>> GetPageByAfeLineAndType([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string afeLineId, int itemType)
        {
            var services = new MaterialServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_material>>(() =>
            {
                var result = new ApiResponsePage<vw_material>();
                var data = services.LookupByAfeLineAndType(marvelDataSourceRequest, afeLineId, itemType);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_material>> GetDetail(string recordId)
        {
            var services = new MaterialServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_material>>(() =>
            {
                var result = new ApiResponse<vw_material>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(material record)
        {
            var services = new MaterialServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new MaterialServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new MaterialServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_material>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, qry);
                return result;
            });
            return response;
        }


        [Route("Preview/BS20")]
        [HttpPost]
        public async Task<ApiResponse> GetListDataTable([FromBody] ITreeRequest request, [FromUri(SuppressPrefixCheck = false)] string parentId)
        {
            var services = new MaterialServices(this.dataContext);
            //CommonTools.Helper.Node<material>

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var data = services.GetTree();
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_material>() },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });


                return result;
            });
            return response;
        }


    }
}
