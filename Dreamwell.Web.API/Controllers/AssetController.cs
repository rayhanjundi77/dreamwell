﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.EeasyUI;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using CommonTools.Helper;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Asset")]

    public class AssetController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(asset record)
        {
            var services = new AssetServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_asset>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new AssetServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_asset>>(() =>
            {
                var result = new ApiResponsePage<vw_asset>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("tree/assets")]
        [HttpPost]
        public async Task<List<object>> GetTreeAsset([FromUri] string countryId)
        {
            var services = new AssetServices(this.dataContext);
            var response = await Task.Run<List<object>>(() =>
            {
                var result = new List<object>();
                result = services.GetAsset(countryId);
                return result;
            });
            return response;
        }

        [Route("tree/assets/BusinessUnit")]
        [HttpPost]
        public async Task<List<object>> GetTreeAssetBusinessUnit([FromUri(Name = "businessUnitId")] string businessUnitId)
        {
            var services = new AssetServices(this.dataContext);
            var response = await Task.Run<List<object>>(() =>
            {
                var result = new List<object>();
                result = services.GetAsset(string.Empty, businessUnitId);
                return result;
            });
            return response;
        }

        [Route("tree/assets/GetTreeAssetBusinessUnitAndField/{businessUnitId}")]
        [HttpPost]
        public async Task<List<object>> GetTreeAssetBusinessUnitAndField(string businessUnitId)
        {
            var services = new AssetServices(this.dataContext);
            var response = await Task.Run<List<object>>(() =>
            {
                var result = new List<object>();
                result = services.GetAssetByBusinessUnit(businessUnitId);
                return result;
            });
            return response;
        }

        [Route("GetFieldNode")]
        [HttpPost]
        public async Task<ApiResponse> ComboTree([FromBody] TreeRequest request, [FromUri] string countryId, [FromUri] string businessUnitId)
        {
            var services = new AssetServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var data = services.GetComboTree(request, businessUnitId);
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_asset>("asset_name") },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });


                return result;
            });
            return response;
        }

        [Route("tree/fields")]
        [HttpPost]
        public async Task<List<object>> GetTreeField([FromUri] string assetId)
        {
            var services = new AssetServices(this.dataContext);
            var response = await Task.Run<List<object>>(() =>
            {
                var result = new List<object>();
                result = services.GetField(assetId);
                return result;
            });
            return response;
        }

        //[Route("tree/fields/BusinessUnit")]
        //[HttpPost]
        //public async Task<List<object>> GetTreeFieldByBusinessUnit([FromUri] string assetId, [FromUri] string businessUnitId)
        //{
        //    var services = new AssetServices(this.dataContext);
        //    var response = await Task.Run<List<object>>(() =>
        //    {
        //        var result = new List<object>();
        //        result = services.GetFieldTreeByBusinessUnit(assetId, businessUnitId);
        //        return result;
        //    });
        //    return response;
        //}

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new AssetServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }



    }
}
