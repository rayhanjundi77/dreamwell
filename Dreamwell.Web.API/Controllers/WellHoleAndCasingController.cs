﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.Models.Entity;
using static Dreamwell.DataAccess.dreamwellRepo;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/WellHoleAndCasing")]

    public class WellHoleAndCasingController : ApiBaseController
    {
        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<WellHoleCasing>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, [FromUri] string wellId)
        {
            //var services = new WellHoleAndCasingServices(this.dataContext);
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponsePage<WellHoleCasing>>(() =>
            {
                //var result = new ApiResponsePage<vw_well_hole_and_casing>();
                //var data = services.Lookup(marvelDataSourceRequest, wellId);
                //result.CurrentPage = data.CurrentPage;
                //result.TotalPages = data.TotalPages;
                //result.TotalItems = data.TotalItems;
                //result.ItemsPerPage = data.ItemsPerPage;
                //result.Items = data.Items;
                //return result;
                return services.DrillingLookUp(marvelDataSourceRequest, wellId);
            });
            return response;
        }

        [Route("Multilookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_well_hole_and_casing>> GetPages([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, [FromUri] string wellId)
        {
            var services = new WellHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well_hole_and_casing>>(() =>
            {
                var result = new ApiResponsePage<vw_well_hole_and_casing>();
                var data = services.MultiLookup(marvelDataSourceRequest,wellId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("multicasingLookup")]
        [HttpPost]
        public async Task<ApiResponsePage<WellHoleCasing>> GetPageMulticasing([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, [FromUri] string wellId)
        {
            //var services = new WellHoleAndCasingServices(this.dataContext);
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponsePage<WellHoleCasing>>(() =>
            {
                //var result = new ApiResponsePage<vw_well_hole_and_casing>();
                //var data = services.Lookup(marvelDataSourceRequest, wellId);
                //result.CurrentPage = data.CurrentPage;
                //result.TotalPages = data.TotalPages;
                //result.TotalItems = data.TotalItems;
                //result.ItemsPerPage = data.ItemsPerPage;
                //result.Items = data.Items;
                //return result;
                return services.MulticasingLookUp(marvelDataSourceRequest, wellId);
            });
            return response;
        }



        [Route("lookupHoleCasingByDrilling")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_well_hole_and_casing>> GetPageByDrilling([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, [FromUri] string drillingId)
        {
            var services = new WellHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well_hole_and_casing>>(() =>
            {
                var result = new ApiResponsePage<vw_well_hole_and_casing>();
                var data = services.LookupHoleCasingByDrilling(marvelDataSourceRequest, drillingId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/getAllByWellId")]
        [HttpGet]
        //public async Task<ApiResponse<List<vw_well_hole_and_casing>>> getAllByWellId(string recordId)
        public async Task<ApiResponse> getAllByWellId(string recordId)
        {
            //var services = new WellHoleAndCasingServices(this.dataContext);
            //var response = await Task.Run<ApiResponse<List<vw_well_hole_and_casing>>>(() =>
            //{
            //    var result = new ApiResponse<List<vw_well_hole_and_casing>>();
            //    var record = services.GetViewAll(" AND r.well_id = @0 AND r.is_active=1 ORDER BY r.created_on ASC ", recordId);
            //    result.Status.Success = (record != null);
            //    result.Data = record;
            //    return result;
            //});
            //return response;
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.GetAllByWellId(recordId);
            });
            return response;
        }

        [Route("{recordId}/getAllHoleCasingByWellId")]
        [HttpGet]
        //public async Task<ApiResponse<List<vw_well_hole_and_casing>>> getAllByWellId(string recordId)
        public async Task<ApiResponse> getAllHoleCasingByWellId(string recordId)
        {
            //var services = new WellHoleAndCasingServices(this.dataContext);
            //var response = await Task.Run<ApiResponse<List<vw_well_hole_and_casing>>>(() =>
            //{
            //    var result = new ApiResponse<List<vw_well_hole_and_casing>>();
            //    var record = services.GetViewAll(" AND r.well_id = @0 AND r.is_active=1 ORDER BY r.created_on ASC ", recordId);
            //    result.Status.Success = (record != null);
            //    result.Data = record;
            //    return result;
            //});
            //return response;
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.getDataHoleCasing(recordId);
            });
            return response;
        }



        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_well_hole_and_casing>> GetDetail(string recordId)
        {
            var services = new WellHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_well_hole_and_casing>>(() =>
            {
                var result = new ApiResponse<vw_well_hole_and_casing>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(well_hole_and_casing records)
        {
            var services = new WellHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(records);
            });
            return response;
        }

        [Route("savecasing")]
        [HttpPost]
        public async Task<ApiResponse> SaveCasing(well_hole_and_casing records)
        {
            var services = new WellHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.SaveCasing(records);
            });
            return response;
        }

        [Route("updatecasing")]
        [HttpPost]
        public async Task<ApiResponse> UpadateCasing(well_hole_and_casing records)
        {
            var services = new WellHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.UpdateCasing(records);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new WellHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }

        [Route("savenew")]
        [HttpPost]
        public async Task<ApiResponse> SaveNew([FromBody] WellHoleRequest wellHole)
        {
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(wellHole);
            });
            return response;
        }

        [Route("{wellId}/getAllByWellIdNew")]
        [HttpGet]
        public async Task<ApiResponse> getAllByWellIdNew(string wellId)
        {
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.GetAll(wellId);
            });
            return response;
        }

      

        [Route("{recordId}/detailnew")]
        [HttpGet]
        public async Task<ApiResponse> GetDetailNew(string recordId)
        {
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Get(recordId);
            });
            return response;
        }

        [Route("deletenew")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string id)
        {
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Delete(id);
            });
            return response;
        }

        [Route("{id}/getAllByWellSchemanticId")]
        public async Task<ApiResponse> getAllByWellSchemanticId([FromUri] string id)
        {
            var services = new WellHoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.GetAllByWellId(id);
            });
            return response;
        }
    }
}
