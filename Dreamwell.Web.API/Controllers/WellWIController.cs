﻿using CommonTools;
using CommonTools.JSTree;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/wellwi")]
    public class WellWIController : ApiBaseController
    {
        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new WellWIServices(this.dataContext);
            var context = this.dataContext;
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_wellwi>(), 0, 0);

                var qry = context.IsSystemAdministrator ? PetaPoco.Sql.Builder.Append("WHERE r.is_active=@0", true) : PetaPoco.Sql.Builder.Append("WHERE r.is_active=@0 AND (r.business_unit_id = @1 OR b.parent_unit = @1)", true, context.PrimaryBusinessUnitId);

                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });

            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_wellwi>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_wellwi>>(() =>
            {
                var result = new ApiResponsePage<vw_wellwi>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_wellwi>> GetDetail(string recordId)
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_wellwi>>(() =>
            {
                var result = new ApiResponse<vw_wellwi>();
                return services.getDetail(recordId); ;
            });
            return response;
        }

        [Route("submit")]
        [HttpPost]
        public async Task<ApiResponse> Submit(wellwi record)
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Submit(record);
            });
            return response;
        }
        [Route("closed")]
        [HttpPost]
        public async Task<ApiResponse> Closed(wellwi record)
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Closed(record);
            });
            return response;
        }

        [Route("new")]
        [HttpPost]
        public async Task<ApiResponse> New(wellwi record)
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.New(record);
            });
            return response;
        }

        //Tandain
        [Route("objectUomMapper/change")]
        [HttpPost]
        public async Task<ApiResponse> ChangeObjectUOMMapper(wellwi record)
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.ChangeUnitOfMeasurement(record.id, record.is_std_international);
            });
            return response;
        }
        //Tandain

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(wellwi record)
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("SaveAll")]
        [HttpPost]
        public async Task<ApiResponse> SaveAll(WellViewModel record)
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.SaveAll(record);
            });
            return response;
        }

        [Route("getAll")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_wellwi>>> getAll()
        {
            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_wellwi>>>(() =>
            {
                var result = new ApiResponse<List<vw_wellwi>>();
                var record = services.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {

            var services = new WellWIServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

    }
}
