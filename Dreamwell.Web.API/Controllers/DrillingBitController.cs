﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingBit")]

    public class DrillingBitController : ApiBaseController
    {
        [Route("{wellId}/{drillingBhaId}/detailByDrillingBhaId")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_bit>> GetDetail(string wellId, string drillingBhaId)
        {
            var services = new DrillingBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_bit>>(() =>
            {
                var result = new ApiResponse<vw_drilling_bit>();
                var record = services.GetViewFirstOrDefault(" AND d.well_id=@0 AND r.is_active=@1 AND r.drilling_bha_id=@2 ", wellId, true, drillingBhaId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{drillingId}/detailByDrillingId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bit>>> GetByDrillingId(string drillingId)
        {
            var services = new DrillingBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bit>>();
                var record = services.getByDrilling(drillingId);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse<drilling_bit>> Save(DrillingBitViewModel record)
        {
            var services = new DrillingBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<drilling_bit>>(() =>
            {
                return services.Save(record);
            });
            return response;
        }
    }
}
