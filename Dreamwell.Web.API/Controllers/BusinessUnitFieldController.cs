﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using CommonTools.JSTree;
using System.Collections.Generic;
using System;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.EeasyUI;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using CommonTools.Helper;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/BusinessUnitField")]
    public class BusinessUnitFieldController : ApiBaseController
    {
        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_business_unit_field>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new BusinessUnitFieldsServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_business_unit_field>>(() =>
            {
                var result = new ApiResponsePage<vw_business_unit_field>();
                var data = services.IsLookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }
    }
}