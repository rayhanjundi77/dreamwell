﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;
using CommonTools.EeasyUI;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using CommonTools.Helper;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DailyCost")]

    public class DailyCostController : ApiBaseController
    {

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(daily_cost record)
        {
            var services = new DailyCostServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        //[Route("dataTable/{afe_id}/{drilling_id}")]
        //[HttpPost]
        //public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest, string afe_id, string drilling_id)
        //{
        //    var services = new DailyCostServices(this.dataContext);
        //    var response = await Task.Run<DataTablesResponse>(() =>
        //    {
        //        DataTablesResponse result = new DataTablesResponse(0, new List<DailyCostViewModel>(), 0, 0);
        //        result = services.GetListByQuery(dataTablesRequest, afe_id, drilling_id);
        //        return result;
        //    });
        //    return response;
        //}

        [Route("{afe_id}/{drilling_id}/GetManageUsage")]
        [HttpPost]
        public async Task<ApiResponse> GetManageUsage([FromBody] TreeRequest request, string afe_id, string drilling_id)
        {
            var services = new DailyCostServices(this.dataContext);

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var data = services.GetAfeLineTree(request, afe_id, drilling_id);
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_afe_manage_usage>() },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });


                return result;
            });
            return response;
        }

        [Route("GetManageUsageDetail/{afeId}/{drillingId}/{materialId}")]
        [HttpPost]
        public async Task<DataTablesResponse> GetManageUsageDetail([FromBody] DataTableRequest dataTablesRequest, string afeId, string drillingId, string materialId)
        {
            var services = new DailyCostServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe_manage_usage_detail>(), 0, 0);
                result = services.GetAfeManageUsageDetail(dataTablesRequest, afeId, drillingId, materialId);
                return result;
            });
            return response;
        }

        [Route("GetDailyCost/{wellId}/{drillingId}")]
        [HttpGet]
        public async Task<ApiResponse<DailyCostViewModel>> GetDailyCost(string wellId, string drillingId)
        {
            var service = new DailyCostServices(this.dataContext);
            var response = await Task.Run<ApiResponse<DailyCostViewModel>>(() =>
            {
                var result = new ApiResponse<DailyCostViewModel>();
                result = service.GetDailyCost(wellId, drillingId);
                return result;
            });
            return response;
        }

        [Route("getAll")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_daily_cost>>> getAll()
        {
            var service = new DailyCostServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_daily_cost>>>(() =>
            {
                var result = new ApiResponse<List<vw_daily_cost>>();
                var record = service.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
