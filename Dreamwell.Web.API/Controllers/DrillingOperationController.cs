﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingOperation")]

    public class DrillingOperationController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_drilling_operation>> GetPage(long page, long itemsPerPage)
        {
            var services = new DrillingOperationServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_drilling_operation>>(() =>
            {
                var result = new ApiResponsePage<vw_drilling_operation>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_operation>> GetDetail(string recordId)
        {
            var services = new DrillingOperationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_operation>>(() =>
            {
                var result = new ApiResponse<vw_drilling_operation>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getByWellId/{wellId}/{dateDrilling}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_operation>>> getByWellId(string wellId, DateTime dateDrilling)
        {
            var services = new DrillingOperationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_operation>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_operation>>();
                var record = services.getByWellId(wellId, dateDrilling);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(drilling_operation record)
        {
            var services = new DrillingOperationServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingOperationServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new DrillingOperationServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling_operation>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }
    }
}
