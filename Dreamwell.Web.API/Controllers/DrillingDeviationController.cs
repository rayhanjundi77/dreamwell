﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System;

namespace Dreamwell.Web.API.Controllers
{ 
    [RoutePrefix("api/core/DrillingDeviation")]
    public class DrillingDeviationController : ApiBaseController
    {
        [Route("{recordId}/detailbywellid")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_deviation>>> GetDatabyWellID(string recordId)
        {
            var services = new DrillingDeviationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_deviation>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_deviation>>();
                var record = services.GetViewAll(" AND r.well_id=@0 ", recordId);
                result.Status.Success = (record != null);
                record.Sort((a, b) => a.seq.CompareTo(b.seq));
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("saveAll")]
        [HttpPost]
        public async Task<ApiResponse<List<drilling_deviation>>> Save([FromBody] List<drilling_deviation> records,[FromUri] string drillingId)
        {
            var services = new DrillingDeviationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<drilling_deviation>>>(() =>
            {
                return services.Save(records, drillingId);
            });
            return response;
        }
        [Route("{recordId}/detailByDrillingId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_deviation>>> GetDataByDrillingId(string recordId)
        {
            var services = new DrillingDeviationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_deviation>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_deviation>>();
                var record = services.GetViewAll(" AND r.drilling_id=@0 ", recordId);
                result.Status.Success = (record != null);
                record.Sort((a, b) => a.seq.CompareTo(b.seq));
                result.Data = record;
                return result;
            });
            return response;
        }
        [Route("getdeviationrangedate/{wellId}/{startDate}/{endDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_deviation>>> getDrillingdeviationrangedate(string wellId, DateTime startDate, DateTime endDate)
        {
            var services = new DrillingDeviationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_deviation>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_deviation>>();
                var record = services.getDrillingDeviationDate(wellId, startDate, endDate);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }
        [Route("{recordId}/listByWellId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_deviation>>> GetDataByWellId(string recordId)
        {
            var services = new DrillingDeviationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_deviation>>>(() =>
            {
                return services.getByWellId(recordId);
            });
            return response;
        }

        [Route("{recordId}/dataDeviByWellId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_deviation>>> GetDataDevByWellId(string recordId)
        {
            var services = new DrillingDeviationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_deviation>>>(() =>
            {
                return services.getDevByWellId(recordId);
            });
            return response;
        }
    }
}
