﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using System.IO;
using System.Web;


namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Lampiran")]

    public class LampiranController : ApiBaseController
    {
        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_tbl_pdf>> GetPage(long page, long itemsPerPage)
        {
            var services = new LampiranServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_tbl_pdf>>(() =>
            {
                var result = new ApiResponsePage<vw_tbl_pdf>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_tbl_pdf>> GetDetail(string recordId)
        {
            var services = new LampiranServices(this.dataContext);
            var response = await Task.Run(() => services.GetDetailLampiran(recordId));

            return response;
        }


        [Route("uploadLamp")]
        [HttpPost]
        public async Task<ApiResponse> UploadLamp(tbl_pdf record)
        {
            var services = new LampiranServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(tbl_pdf record)
        {
            var services = new LampiranServices(this.dataContext);
            //var httpRequest = HttpContext.Current.Request;

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.Save(record);
                }
                catch (Exception ex)
                {
                    result.Status.Message = ex.Message;
                }
                return result;

            });


            return response;
        }

        [Route("uploadLampiranPD")]
        [HttpPost]
        public async Task<ApiResponse> UploadLampiranPD([FromUri] string id)
        {
            return await LampiranUpload<tbl_pdf>(id);
        }


        [Route("uploadLampiranPDF")]
        [HttpPost]
        public async Task<ApiResponse> UploadLampiranPDF()
        {
            var response = new ApiResponse();

            try
            {
                var pdf_1 = HttpContext.Current.Request.Files["pdf_content_1"];
                var pdf_2 = HttpContext.Current.Request.Files["pdf_content_2"];
                var pdf_3 = HttpContext.Current.Request.Files["pdf_content_3"];
                var pdf_4 = HttpContext.Current.Request.Files["pdf_content_4"];

                var wellId = HttpContext.Current.Request.Form["well_id"];
                var wellFinal = HttpContext.Current.Request.Form["well_final_report_id"];

                    var record = new tbl_pdf
                    {
                        id = CommonTools.GuidHash.ConvertToMd5HashGUID(wellFinal).ToString(),
                        well_id = wellId,
                        well_final_report_id = wellFinal,
                        is_active = true
                    };

                if (pdf_1 != null && pdf_1.ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        pdf_1.InputStream.CopyTo(memoryStream);
                        record.pdf_content_1 = memoryStream.ToArray();
                    }
                }
                if (pdf_2 != null && pdf_2.ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        pdf_2.InputStream.CopyTo(memoryStream);
                        record.pdf_content_2 = memoryStream.ToArray();
                    }
                }
                if (pdf_3 != null && pdf_3.ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        pdf_3.InputStream.CopyTo(memoryStream);
                        record.pdf_content_3 = memoryStream.ToArray();
                    }
                }
                if (pdf_4 != null && pdf_4.ContentLength > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        pdf_4.InputStream.CopyTo(memoryStream);
                        record.pdf_content_4 = memoryStream.ToArray();
                    }
                }

                var services = new LampiranServices(this.dataContext);
                response = services.Save(record); 
                if (response.Status.Success)
                {
                    response.Status.Message = $"PDF files uploaded successfully.{response.Data}";
                }
                else
                {
                    response.Status.Message = $"Error while uploading files: {response.Status.Message} . Please try again.";
                }
            }
            catch (Exception ex)
            {
                response.Status.Success = false;
                response.Status.Message = $"An error occurred while processing your request: {ex.Message}";
                System.Diagnostics.Debug.WriteLine("Error during PDF upload: " + ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }

            return response;
        }



        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new LampiranServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new LampiranServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_tbl_pdf>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }



    }
}
