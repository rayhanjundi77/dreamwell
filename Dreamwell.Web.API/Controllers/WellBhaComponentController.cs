﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/WellBhaComponent")]

    public class WellBhaComponentController : ApiBaseController
    {
        [Route("GetByWellBha/{wellBhaId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_bha_component>>> GetByWellBha(string wellBhaId)
        {
            var services = new WellBhaComponentServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_bha_component>>>(() =>
            {
                //var result = new ApiResponse<List<vw_well_bha_component>>();
                //var record = services.GetViewAll(" AND r.well_bha_id = @0 AND r.is_active=@1 ORDER BY r.created_on ASC ", wellBhaId, true);
                //result.Status.Success = (record != null);
                //result.Data = record;
                //return result;
                return services.GetByWellBha(wellBhaId);
            });
            return response;
        }
    }
}
