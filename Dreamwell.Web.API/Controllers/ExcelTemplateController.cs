﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DataTables.Mvc;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.JSTree;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/ExcelTemplate")]
    public class ExcelTemplateController : ApiBaseController
    {
        //DrillingContractorServices services;

        public ExcelTemplateController()
        {

        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_excel_template>> GetDetail(string recordId)
        {
            var services = new ExcelTemplateServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_excel_template>>(() =>
            {
                var result = new ApiResponse<vw_excel_template>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("List")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_excel_template>>> GetList()
        {
            var services = new ExcelTemplateServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_excel_template>>>(() =>
            {
                var result = new ApiResponse<List<vw_excel_template>>();
                var record = services.GetViewAll(" AND r.is_active=1 ");
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("datavalidation/List")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_excel_data_validation>>> GetListDataValidation([FromUri] string templateId)
        {
            var services = new ExcelDataValidationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_excel_data_validation>>>(() =>
            {
                var result = new ApiResponse<List<vw_excel_data_validation>>();
                var record = services.GetViewAll(" AND r.is_active=1 AND r.excel_template_id=@0 ",templateId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }



        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(excel_template record)
        {
            var services = new ExcelTemplateServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }
        [Route("datavalidation/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveDataValidation(excel_data_validation record)
        {
            var services = new ExcelDataValidationServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }


        [Route("tree")]
        [HttpPost]
        public async Task<List<object>> GetTreeTable([FromUri] string templateId, [FromUri] string parentId)
        {
            var services = new ExcelTableMapServices(this.dataContext);
            var response = await Task.Run<List<object>>(() =>
            {
                var result = new List<object>();
                result = services.GetTreeMap(templateId,parentId);
                return result;
            });
            return response;
        }

        [Route("tree/column")]
        [HttpPost]
        public async Task<List<object>> GetTreeColumnByTableMap([FromUri] string tableMapId)
        {
            var services = new ExcelTableMapServices(this.dataContext);
            var response = await Task.Run<List<object>>(() =>
            {
                var result = new List<object>();
                result = services.GetTreeColumnMap(tableMapId);
                return result;
            });
            return response;
        }


    }
}
