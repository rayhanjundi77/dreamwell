﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillingTransport")]

    public class DrillingTransportController : ApiBaseController
    {
        [Route("{recordId}/getByDrillingId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_transport>>> getByDrillingId(string recordId)
        {
            var services = new DrillingTransportServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_transport>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_transport>>();
                var record = services.GetViewAll(" AND r.drilling_id=@0 ", recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        
        [Route("saveAll")]
        [HttpPost]
        public async Task<ApiResponse> Save(DrillingViewModel records, [FromUri] string drillingId)
        {
            var services = new DrillingTransportServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(records.drilling_transport, drillingId);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingTransportServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }


    }
}
