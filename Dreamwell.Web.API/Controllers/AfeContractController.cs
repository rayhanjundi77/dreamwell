﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using ClosedXML;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/AfeContract")]

    public class AfeContractController : ApiBaseController
    {

        [Route("AssignContractToAfe")]
        [HttpPost]
        public async Task<ApiResponse> AssignContractToAfe(afe_contract record)
        {
            var services = new AfeContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.AssignContractToAfe(record);
            });

            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest,[FromUri] string afeId)
        {
            var services = new AfeContractServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                return services.GetListDataTables(dataTablesRequest, afeId);
            });
            return response;
        }
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse<afe_contract>> Save(afe_contract record)
        {
            var services = new AfeContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse<afe_contract>>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete/{recordId}/{afeId}/{contractId}/{contractDetailId}/{isNew}")]
        [HttpPost]
        public async Task<ApiResponse> Delete(string recordId, string afeId, string contractId, string contractDetailId, bool isNew)
        {
            var services = new AfeContractServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.Delete(recordId, afeId, contractId, contractDetailId, isNew);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }
    }
}
