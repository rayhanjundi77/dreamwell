﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess.Models.Entity;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/hole")]
    public class HoleController : ApiBaseController
    {
        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTableService dataTableService = new DataTableService();
                var result = dataTableService.generate<Hole>(dataTablesRequest);
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<Hole>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new HoleService(this.dataContext);
            var response = await Task.Run<ApiResponsePage<Hole>>(() =>
            {
                return services.Lookup(marvelDataSourceRequest);
            });
            return response;
        }

        [Route("lookupcasing")]
        [HttpPost]
        public async Task<ApiResponsePage<Hole>> LookupCasing([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new HoleService(this.dataContext);
            var response = await Task.Run<ApiResponsePage<Hole>>(() =>
            {
                return services.LookupCasing(marvelDataSourceRequest);
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse> GetDetail(string recordId)
        {
            var services = new HoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Get(recordId);
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(Hole record)
        {
            var services = new HoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record, string.IsNullOrWhiteSpace(record.id));
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new HoleService(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Delete(ids);
            });
            
            return response;
        }
    }
}