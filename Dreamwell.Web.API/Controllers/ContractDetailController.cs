﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/ContractDetail")]

    public class ContractDetailController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_contract_detail>> GetPage(long page, long itemsPerPage)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_contract_detail>>(() =>
            {
                var result = new ApiResponsePage<vw_contract_detail>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_contract_detail>> GetDetail(string recordId)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_contract_detail>>(() =>
            {
                var result = new ApiResponse<vw_contract_detail>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(contract_detail record)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("deleteById/{contractId}/{recordId}")]
        [HttpPost]
        public async Task<ApiResponse> SingleDelete(string contractId, string recordId)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {

                return services.Delete(contractId, recordId);
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_contract_detail>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("getListById/{contractId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_contract_detail>>> getByContractNumber(string contractId)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_contract_detail>>>(() =>
            {
                var result = new ApiResponse<List<vw_contract_detail>>();
                result = services.getListById(contractId);
                return result;
            });
            return response;
        }

        [Route("dataTableByContractId/{contractId}")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest, string contractId)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_contract_detail>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 AND r.contract_id=@1 ", true, contractId);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("getListByMaterial/{materialId}/{afeId}/{isNewPurchase}")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest, string materialId, string afeId, bool isNewPurchase)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe_manage_contract>(), 0, 0);
                result = services.GetListByMaterial(dataTablesRequest, materialId, afeId, isNewPurchase);
                return result;
            });
            return response;
        }
        //public async Task<ApiResponse<List<vw_afe_manage_contract>>> getListByMaterial(string materialId, string afeId, bool isNewPurchase)
        //{
        //    var services = new ContractDetailServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse<List<vw_afe_manage_contract>>>(() =>
        //    {
        //        var result = new ApiResponse<List<vw_afe_manage_contract>>();
        //        result = services.getListByMaterial(materialId, afeId, isNewPurchase);
        //        return result;
        //    });
        //    return response;
        //}
        [Route("getListIssuedStock/{materialId}/{afeId}")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest, string materialId, string afeId)
        {
            var services = new ContractDetailServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe_manage_issued_stock>(), 0, 0);
                result = services.GetListIssuedStock(dataTablesRequest, materialId, afeId);
                return result;
            });
            return response;
        }
        //public async Task<ApiResponse<List<vw_afe_manage_issued_stock>>> getListIssuedStock(string materialId, string afeId)
        //{
        //    var services = new ContractDetailServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse<List<vw_afe_manage_issued_stock>>>(() =>
        //    {
        //        var result = new ApiResponse<List<vw_afe_manage_issued_stock>>();
        //        result = services.getListIssuedStock(materialId, afeId);
        //        return result;
        //    });
        //    return response;
        //}
    }
}
