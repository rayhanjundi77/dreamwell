﻿using CommonTools;
using CommonTools.JSTree;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Well")]
    public class WellController : ApiBaseController
    {
        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new WellServices(this.dataContext);
            var context = this.dataContext;
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_well>(), 0, 0);

                var qry = context.IsSystemAdministrator ? PetaPoco.Sql.Builder.Append("WHERE r.is_active=@0", true) : PetaPoco.Sql.Builder.Append("WHERE r.is_active=@0 AND (r.business_unit_id = @1 OR b.parent_unit = @1)", true, context.PrimaryBusinessUnitId);

                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });

            return response;
        }




        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_well>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well>>(() =>
            {
                var result = new ApiResponsePage<vw_well>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }
        [Route("{wellname}/lookupwellname")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well>>> GetByWellName(string wellname)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                //var result = new ApiResponse<List<vw_well>>();
                // return services.getBywellName(wellname);

                var result = new ApiResponse<List<vw_well>>();
                var record = services.getBywellName(wellname);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;

            });
            return response;
        }
        [Route("lookupWellField")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_well>> lookupWellField([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well>>(() =>
            {
                var result = new ApiResponsePage<vw_well>();
                var data = services.LookupWellField(marvelDataSourceRequest);
                    result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items.Where(x => x.is_active == true).ToList();
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_well>> GetDetail(string recordId)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_well>>(() =>
            {
                var result = new ApiResponse<vw_well>();
                return services.getDetail(recordId); ;
            });
            return response;
        }
    /*
        [Route("{recordId}/image-Pipe")]
        [HttpGet]
        public async Task<ApiResponse<vw_well>> GetDataPipe(string recordId)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_well>>(() =>
            {
               // var result = new ApiResponse<vw_well>();
                //return services.getPipe(recordId); ;
            });
            return response;
        }
    */
        [Route("{fieldId}/detailByField")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well>>> GetByField(string fieldId)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                return services.getByField(fieldId);
                //var record = services.getByField(fieldId);
                //result.Status.Success = (record != null);
                //result.Data = record.Data;
                //return result;
            });
            return response;
        }

        [Route("{unitID}/detailByBussinessUnit")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well>>> GetByUnitID(string unitID)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                return services.getByBussinUnitID(unitID);
                //var record = services.getByField(fieldId);
                //result.Status.Success = (record != null);
                //result.Data = record.Data;
                //return result;
            });
            return response;
        }

        [Route("{recordId}/detailForDefaultDrilling")]
        [HttpGet]
        public async Task<ApiResponse<vw_well>> detailForDefaultDrilling(string recordId)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_well>>(() =>
            {
                var result = new ApiResponse<vw_well>();
                return services.getDetailById(recordId);
            });
            return response;
        }

        [Route("submit")]
        [HttpPost]
        public async Task<ApiResponse> Submit(well record)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Submit(record);
            });
            return response;
        }
        [Route("closed")]
        [HttpPost]
        public async Task<ApiResponse> Closed(well record)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Closed(record);
            });
            return response;
        }

        [Route("new")]
        [HttpPost]
        public async Task<ApiResponse> New(well record)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.New(record);
            });
            return response;
        }

        [Route("objectUomMapper/change")]
        [HttpPost]
        public async Task<ApiResponse> ChangeObjectUOMMapper(well record)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.ChangeUnitOfMeasurement(record.id, record.is_std_international);
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(well record)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }  
                      
        [Route("SaveAll")]
        [HttpPost]
        public async Task<ApiResponse> SaveAll(WellViewModel record)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.SaveAll(record);
            });
            return response;
        }

        [Route("getAll")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well>>> getAll()
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                var record = services.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getAllNew")]
        [HttpGet]
        public async Task<ApiResponseCount<List<vw_well>>> getAllNew(string isadmin)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponseCount<List<vw_well>>>(() =>
            {
                var result = new ApiResponseCount<List<vw_well>>();
                var record = services.CountTotalWell(isadmin);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            //var services = new WellServices(this.dataContext);
            //var response = await Task.Run<ApiResponse>(() =>
            //{
            //    return services.Delete(ids);
            //});
            //return response;

            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("tree")]
        [HttpPost]
        public async Task<List<JSTreeResponse<well>>> GetTree([FromUri] string field_id)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<List<JSTreeResponse<well>>>(() =>
            {
                var result = new List<JSTreeResponse<well>>();
                result = services.GetWellTreeByField(field_id);
                return result;
            });
            return response;
        }

        [Route("newTree/{field_id}")]
        public async Task<ApiResponse<List<vw_well>>> GetTrees(string field_id)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                //var result = new ApiResponse<List<vw_well>>();
                // return services.getBywellName(wellname);

                var result = new ApiResponse<List<vw_well>>();
                var record = services.GetWellTreeByFields(field_id);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;

            });
            return response;
        }


        //public async Task<ApiResponse> Delete([FromBody] string[] ids)
        //{
        //    var services = new WellServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse>(() =>
        //    {
        //        var result = new ApiResponse();
        //        try
        //        {
        //            result.Status.Success = services.RemoveAll(ids);
        //        }
        //        catch (Exception ex)
        //        {
        //            appLogger.Error(ex);
        //            result.Status.Success = false;
        //            result.Status.Message = ex.Message;
        //        }
        //        return result;
        //    });
        //    return response;
        //}

        [Route("getAllDp")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_drilling_parameter>>> getAllDp()
        {
            var services = new WellDrillingParameterServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_drilling_parameter>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_drilling_parameter>>();
                var record = services.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detailDpByWell")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_drilling_parameter>>> GetdetailDpByWell(string recordId)
        {
            var services = new WellDrillingParameterServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_drilling_parameter>>>(() =>
            {
                var result = new ApiResponse<vw_well_drilling_parameter>();
                return services.GetByIdDP(recordId); ;
            });
            return response;
        }



        [Route("savedp")]
        [HttpPost]
        public async Task<ApiResponse> Savedp(well_drilling_parameter record)
        {
            var services = new WellDrillingParameterServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("deletedp")]
        [HttpPost]
        public async Task<ApiResponse> Deletedp([FromBody] string[] ids)
        {
            var services = new WellDrillingParameterServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }


    }
}
