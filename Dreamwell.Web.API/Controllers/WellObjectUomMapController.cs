﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Axa.Web.API.Controllers
{
    [RoutePrefix("api/core/WellObjectUomMap")]

    public class WellObjectUomMapController : ApiBaseController
    {

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> SaveObjectMapper(well_object_uom_map record)
        {
            var services = new WellObjectUomMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("dataTable/objectMapper")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListObjectMapping([FromBody] DataTableRequest dataTablesRequest, string wellId)
        {
            //var services = new WellObjectUomMapServices(this.dataContext);
            //var response = await Task.Run<DataTablesResponse>(() =>
            //{
            //    DataTablesResponse result = new DataTablesResponse(0, new List<vw_well_object_uom_map>(), 0, 0);
            //    var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 AND r.well_id = @1 ", true, wellId);
            //    result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
            //    return result;
            //});
            //return response;
            var services = new WellObjectUomMapServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_well_object_uom_map>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 AND r.well_id = @1 ", true, wellId);

                if (dataTablesRequest.Search != null && !string.IsNullOrEmpty(dataTablesRequest.Search.Value))
                {
                    appLogger.Info($"Original Search Value: {dataTablesRequest.Search.Value}");
                    dataTablesRequest.Search.Value = dataTablesRequest.Search.Value.Replace("%25", "%"); // Decode %25 back to %
                    appLogger.Info($"Decoded Search Value: {dataTablesRequest.Search.Value}");

                    // If necessary, escape '%' for SQL LIKE clause
                    string searchValue = dataTablesRequest.Search.Value.Replace("%", "[%]");
                    qry.Append(" AND (uom_code LIKE @0 OR object_name LIKE @0)", $"%{searchValue}%");
                }

                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });

            return response;
        }

        [Route("GetAllByWellId/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_object_uom_map>>> GetAll(string wellId)
        {
            var services = new WellObjectUomMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_object_uom_map>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_object_uom_map>>();
                var record = services.GetViewAll(" AND r.is_active = 1 AND r.well_id = @0 ", wellId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_well_object_uom_map>> GetDetail(string recordId)
        {
            var services = new WellObjectUomMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_well_object_uom_map>>(() =>
            {
                var result = new ApiResponse<vw_well_object_uom_map>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
