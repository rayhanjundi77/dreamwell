﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System;
using System.Linq;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Potential")]
    public class PotentialController : ApiBaseController
    {

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(potential record)
        {
            var services = new PotentialServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("listByWellId/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_potential>>> GetByWellId(string wellId)
        {
            var services = new PotentialServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_potential>>>(() =>
            {
                return services.GetByWellId(wellId);
            });
            return response;
        }

    }
}