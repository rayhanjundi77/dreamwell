﻿using DocumentFormat.OpenXml.Office2021.DocumentTasks;
using Dreamwell.DataAccess.Models.Entity;
using Dreamwell.DataAccess.Repository;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/applicationexternal")]
    public class ApplicationExternalController : ApiController
    {
        ApplicationExternalRepository repository;

        public ApplicationExternalController()
        {
            repository = new ApplicationExternalRepository();
        }

        protected override void Dispose(bool disposing)
        {
            repository.Dispose();
            base.Dispose(disposing);
        }

        [AllowAnonymous]
        [Route("getbyclientid")]
        [HttpGet]
        public async Task<ApplicationExternal> GetByClientId(string clientId)
        {
            return await repository.GetById(clientId);
        }

        [Route("update")]
        [HttpGet]
        public async Task<bool> Update(string clientId)
        {
            var data = await repository.GetById(clientId);
            return await repository.Update(data);
        }
    }
}
