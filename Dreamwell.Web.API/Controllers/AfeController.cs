﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using System.Data.Entity.Infrastructure;
using System.Text;
using PetaPoco;
using CommonTools.EeasyUI;
using CommonTools.Helper;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Afe")]

    public class AfeController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_afe>> GetPage(long page, long itemsPerPage)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_afe>>(() =>
            {
                var result = new ApiResponsePage<vw_afe>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_afe>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_afe>>(() =>
            {
                var result = new ApiResponsePage<vw_afe>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }



        [Route("{recordId}/GetBS20ManageDetailFromAfe")]
        [HttpPost]
        public async Task<ApiResponse> GetBS20ManageDetail([FromBody] TreeRequest request, string recordId)
        {
            var services = new AfePlanServices(this.dataContext);
            //CommonTools.Helper.Node<material>

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var data = services.GetAfeLineTree(request, recordId);
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_afe_manage_detail>("description") },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });


                return result;
            });
            return response;
        }


        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_afe>> GetDetail(string recordId)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_afe>>(() =>
            {
                var result = new ApiResponse<vw_afe>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("{wellId}/detailByWell")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_afe>>> GetDetailbyWell(string wellId)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_afe>>>(() =>
            {
                var result = new ApiResponse<List<vw_afe>>();
                var record = services.getDetailByWell(wellId);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detailNew")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_afe>>> GetDetailNew(string recordId)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_afe>>>(() =>
            {
                var result = new ApiResponse<List<vw_afe>>();
                var record = services.getDetail(recordId);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("rkapPlan")]
        [HttpPost]
        public async Task<ApiResponse<List<afe_rkap_plan>>> getaferRKAP()
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<afe_rkap_plan>>>(() =>
            {
                var result = new ApiResponse<List<afe_rkap_plan>>();
                var record = services.getafeall();
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(afe record)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success =  services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                 
                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var context = this.dataContext;
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                //DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe>(), 0, 0);
                //var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                //result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                Sql qry = null;
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe>(), 0, 0);
                if (!context.IsSystemAdministrator)
                {
                   qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 and (r.business_unit_id=@1 or b.parent_unit=@1) ", true, context.PrimaryBusinessUnitId);
                }
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);

                //DataTableService dataTableService = new DataTableService();
                //var result = dataTableService.generate<vw_afe>(dataTablesRequest);

                return result;
            });
            return response;
        }


        [Route("dataTableByUnit")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListByBussinessUnitID([FromBody] DataTableRequest dataTablesRequest, [FromUri] string bussinessUnitID)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 and r.business_unit_id=@1 ",true, bussinessUnitID);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }


        [Route("rkapPlanField")]
        [HttpPost]
        public async Task<ApiResponse<List<afe_rkap_field_plan>>> getAfeRkapField([FromUri] string bussinessUnitID)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<afe_rkap_field_plan>>>(() =>
            {
                var result = new ApiResponse<List<afe_rkap_field_plan>>();
                var record = services.getAFEFieldRekap(bussinessUnitID);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }


        [Route("listapproval")]
        [HttpPost]
        public async Task<DataTablesResponse> GetApproval([FromBody] DataTableRequest dataTableRequest)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 AND r.approved_by IS NULL AND r.approved_on IS NULL ", true);
                result = services.GetListDataTables(dataTableRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("approved")]
        [HttpPost]
        public async Task<DataTablesResponse> GetApproved([FromBody] DataTableRequest dataTableRequest)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 AND r.approved_by IS NOT NULL AND r.approved_on IS NOT NULL ", true);
                result = services.GetListDataTables(dataTableRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("{recordId}/submit")]
        [HttpPost]
        public async Task<ApiResponse> Submit(string recordId)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Submit(recordId);
            });
            return response;
        }

        [Route("approval")]
        [HttpPost]
        public async Task<ApiResponse> Approval(approvalRequestBody request)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() => 
            {
                return services.Approval(request);
            });
            return response;
        }

        [Route("{afeId}/close")]
        [HttpPost]
        public async Task<ApiResponse> AfeClose(string afeId)
        {
            var services = new AfeServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.AfeClose(afeId);
            });
            return response;
        }

    }
}
