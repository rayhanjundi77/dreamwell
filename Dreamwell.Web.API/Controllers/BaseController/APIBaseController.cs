﻿using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure.Acl;
using Dreamwell.Infrastructure.Session;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Dreamwell.Infrastructure.WebApi
{

    [JWTAuthenticationFilter]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public partial class ApiBaseController : ApiController
    {
        public Logger appLogger = LogManager.GetCurrentClassLogger();
        public UserSession userSession;
        public DataContext dataContext;

        public async Task<ApiResponse> MinIOUpload<T>(string recordId,string columnTarget= "filemaster_id") 
        {
            var services = new FileMasterServices(this.dataContext);
            var httpRequest = System.Web.HttpContext.Current.Request;

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                if (httpRequest.Files.Count > 0)
                {
                    BusinessLogic.Core.Minio.MinioStorage<T> u = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        result.Status.Success = u.Upload(null, postedFile, recordId, columnTarget).Result;
                        if (result.Status.Success)
                        {
                            result.Status.Message = "Successfull upload the file";
                        }
                        else
                        {
                            result.Status.Message = "Failed upload the file. File already existed. Try again.";
                        }
                    }
                }
                return result;
            });
            return response;
        }

       
        public async Task<ApiResponse> BHAComponentUploads<T>(string recordId, string columnTarget = "image_id") //OLD
        {
            var services = new FileMasterServices(this.dataContext);
            var httpRequest = System.Web.HttpContext.Current.Request;

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                if (httpRequest.Files.Count > 0)
                {
                    BusinessLogic.Core.Minio.MinioStorage<T> u = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        result.Status.Message = u.UploadBHANew(postedFile, recordId, columnTarget).Result;
                        if (result.Status.Message == "success")
                        {
                            result.Status.Success = true;
                        }
                        else
                        {
                            result.Status.Success = false;
                        }
                    }
                }
                return result;
            });
            return response;
        }

         public async Task<ApiResponse> LithologyComponentUpload<T>(string recordId, string columnTarget = "filemaster_id")
         {
            var services = new FileMasterServices(this.dataContext);
            var httpRequest = System.Web.HttpContext.Current.Request;

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                if (httpRequest.Files.Count > 0)
                {
                    BusinessLogic.Core.Minio.MinioStorage<T> u = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        result.Status.Message = u.UploadLithology(null, postedFile, recordId, columnTarget).Result;
                        //if (result.Status.Success)
                        //{
                        //    result.Status.Message = "Successfull upload the file";
                        //}
                        //else
                        //{
                        //    result.Status.Message = "Failed upload the file. File already existed. Try again.";
                        //}
                    }
                }
                return result;
            });
            return response;
         }

        //public async Task<ApiResponse> AttachmentWIRUpload<T>(string recordId, string columnTarget = "filemaster_id")
        //{
        //    var services = new FileMasterServices(this.dataContext);
        //    var httpRequest = System.Web.HttpContext.Current.Request;

        //    var response = await Task.Run<ApiResponse>(() =>
        //    {
        //        var result = new ApiResponse();
        //        if (httpRequest.Files.Count > 0)
        //        {
        //            BusinessLogic.Core.Minio.MinioStorage<T> u = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);
        //            foreach (string file in httpRequest.Files)
        //            {
        //                var postedFile = httpRequest.Files[file];
        //                result.Status.Message = u.UploadAttachmentWIR(null, postedFile, recordId, columnTarget).Result;
        //                //if (result.Status.Success)
        //                //{
        //                //    result.Status.Message = "Successfull upload the file";
        //                //}
        //                //else
        //                //{
        //                //    result.Status.Message = "Failed upload the file. File already existed. Try again.";
        //                //}
        //            }
        //        }
        //        return result;
        //    });
        //    return response;
        //}

        //public async Task<ApiResponse> AttachmentWIRUpload<T>(string recordId, string columnTarget = "filemaster_id")
        //{
        //    var services = new FileMasterServices(this.dataContext);
        //    var httpRequest = System.Web.HttpContext.Current.Request;

        //    var response = new ApiResponse();
        //    var uploadedFiles = new List<object>();

        //    if (httpRequest.Files.Count > 0)
        //    {
        //        var storage = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);

        //        foreach (string file in httpRequest.Files)
        //        {
        //            var postedFile = httpRequest.Files[file];
        //            var fileId = await storage.UploadAttachmentWIR(null, postedFile, recordId, columnTarget);

        //            uploadedFiles.Add(new
        //            {
        //                fileId,
        //                fileName = postedFile.FileName
        //            });
        //        }

        //        response.Data = uploadedFiles;
        //        response.Status.Success = true;
        //        response.Status.Message = "Files uploaded successfully.";
        //    }
        //    else
        //    {
        //        response.Status.Success = false;
        //        response.Status.Message = "No files uploaded.";
        //    }

        //    return response;
        //}

        public async Task<ApiResponse> AttachmentWIRUpload<T>(string recordId, string columnTarget = "filemaster_id")
        {
            var response = new ApiResponse();
            var uploadedFiles = new List<object>();

            var httpRequest = System.Web.HttpContext.Current.Request;

            if (httpRequest.Files.Count > 0)
            {
                var storage = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);

                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var fileId = await storage.UploadAttachmentWIR(null, postedFile, recordId, columnTarget);

                    uploadedFiles.Add(new
                    {
                        fileId,
                        fileName = postedFile.FileName
                    });
                }

                response.Data = uploadedFiles;
                response.Status.Success = true;
                response.Status.Message = "Files uploaded successfully.";
            }
            else
            {
                response.Status.Success = false;
                response.Status.Message = "No files uploaded.";
            }

            return response;
        }


        public async Task<ApiResponse> LampiranUploadPDF<T>(string recordId, HttpPostedFile postedFile, string columnTarget = "filemaster_id")
        {
            var services = new FileMasterServices(this.dataContext);

            var response = await Task.Run(async () =>
            {
                var result = new ApiResponse();
                if (postedFile != null)
                {
                    BusinessLogic.Core.Minio.MinioStorage<T> u = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);
                    result.Status.Message = await u.UploadLampiranPDF(null, postedFile, recordId, columnTarget);

                    if (result.Status.Message.Contains("Success"))
                    {
                        result.Status.Success = true;
                    }
                    else
                    {
                        result.Status.Success = false;
                    }
                }
                else
                {
                    result.Status.Success = false;
                    result.Status.Message = "No file uploaded.";
                }
                return result;
            });

            return response;
        }

        public async Task<ApiResponse> LampiranUpload<T>(string recordId)
        {
            var response = new ApiResponse();

            try
            {
                // Validasi Input
                var httpRequest = System.Web.HttpContext.Current.Request;

                if (httpRequest.Files.Count == 0)
                {
                    response.Status.Success = false;
                    response.Status.Message = "No files found in the request.";
                    return response;
                }

                var wellId = HttpContext.Current.Request.Form["well_id"];
                var wellFinal = HttpContext.Current.Request.Form["well_final_report_id"];

                if (string.IsNullOrEmpty(wellId) || string.IsNullOrEmpty(wellFinal))
                {
                    response.Status.Success = false;
                    response.Status.Message = "Well ID and Well Final Report ID are required.";
                    return response;
                }

                // Inisialisasi Record
                var record = new tbl_pdf
                {
                    id = CommonTools.GuidHash.ConvertToMd5HashGUID(wellFinal).ToString(),
                    well_id = wellId,
                    well_final_report_id = wellFinal,
                    created_by = this.dataContext.AppUserId,
                    owner_id = this.dataContext.AppUserId,
                    is_active = true
                };

                // Proses File dengan Fungsi Modular
                record.pdf_content_1 = await ReadFileAsync(httpRequest.Files["pdf_content_1"]);
                record.pdf_content_2 = await ReadFileAsync(httpRequest.Files["pdf_content_2"]);
                record.pdf_content_3 = await ReadFileAsync(httpRequest.Files["pdf_content_3"]);
                record.pdf_content_4 = await ReadFileAsync(httpRequest.Files["pdf_content_4"]);

                // Simpan Data
                var services = new LampiranServices(this.dataContext);
                response = services.Save(record);

                if (response.Status.Success)
                {
                    response.Status.Message = "PDF files uploaded successfully.";
                }
                else
                {
                    response.Status.Message = $"Error while uploading files: {response.Status.Message}. Please try again.";
                }
            }
            catch (Exception ex)
            {
                // Tangani Error
                response.Status.Success = false;
                response.Status.Message = $"An unexpected error occurred: {ex.Message}";

                // Log Error untuk Debugging
                System.Diagnostics.Debug.WriteLine($"Error in LampiranUpload: {ex.Message}");
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }

            return response;
        }

        // Fungsi untuk Membaca File ke Byte Array
        private async Task<byte[]> ReadFileAsync(HttpPostedFile file)
        {
            if (file == null || file.ContentLength == 0) return null;

            using (var memoryStream = new MemoryStream())
            {
                await file.InputStream.CopyToAsync(memoryStream);
                return memoryStream.ToArray();
            }
        }


        public async Task<ApiResponse> BHAComponentUpload<T>(string recordId, string columnTarget = "image_id")
        {
            var services = new FileMasterServices(this.dataContext);
            var httpRequest = System.Web.HttpContext.Current.Request;

            BusinessLogic.Core.Services.BhaComponentServicesNew uu = new BusinessLogic.Core.Services.BhaComponentServicesNew();
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                if (httpRequest.Files.Count > 0)
                {
                    var data = uu.Bs19FormatByBhaComp();

                    //foreach (string file in httpRequest.Files)
                    //{
                    //    var postedFile = httpRequest.Files[file];
                    //    result.Status.Message = u.UploadBHANew(postedFile, recordId, columnTarget).Result;
                    //    if (result.Status.Message == "success")
                    //    {
                    //        result.Status.Success = true;
                    //    }
                    //    else
                    //    {
                    //        result.Status.Success = false;
                    //    }
                    //}
                }
                return result;
            });
            return response;
        }

        public async Task<ApiResponse> UploadImagePicture<T>(string recordId, string columnTarget = "filemaster_id")
        {
            var services = new FileMasterServices(this.dataContext);
            var httpRequest = System.Web.HttpContext.Current.Request;

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                if (httpRequest.Files.Count > 0)
                {
                    BusinessLogic.Core.Minio.MinioStorage<T> u = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        result.Status.Success = u.uploadPicture(null, postedFile, recordId, columnTarget).Result;
                        if (result.Status.Success)
                        {
                            result.Status.Message = "Successfull upload the file";
                        }
                        else
                        {
                            result.Status.Message = "Failed upload the file. File already existed. Try again.";
                        }
                    }
                }
                return result;
            });
            return response;
        }
        public async Task<ApiResponse> MinIOUploadWithReturn<T>(string recordId, string columnTarget = "filemaster_id")
        {
            var services = new FileMasterServices(this.dataContext);
            var httpRequest = System.Web.HttpContext.Current.Request;

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                if (httpRequest.Files.Count > 0)
                {
                    BusinessLogic.Core.Minio.MinioStorage<T> u = new BusinessLogic.Core.Minio.MinioStorage<T>(this.dataContext);

                    var file = httpRequest.Files[0];

                    result = u.UploadWithReturn(null, file, recordId, columnTarget).Result;
                    if (result.Status.Success)
                        result.Status.Message = "Successfully upload the file";
                    else
                        result.Status.Message = "Failed upload the file. File already existed. Try again.";
                }
                return result;
            });
            return response;
        }
    }

}