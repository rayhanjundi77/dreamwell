﻿using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dreamwell.Infrastructure;
using System.Threading.Tasks;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using CommonTools;
using CommonTools.Helper;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/utc/WellData")]
    public class WellDataController : ApiBaseController
    {
        [Route("LookupByWellBha/{wellBhaId}")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_well_bit>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string wellBhaId)
        {
            var services = new WellBitServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_well_bit>>(() =>
            {
                var result = new ApiResponsePage<vw_well_bit>();
                var data = services.LookupByWellBha(marvelDataSourceRequest, wellBhaId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        //[Route("LookupHoleAndCasing/{wellId}")]
        //[HttpPost]
        //public async Task<ApiResponsePage<vw_hole_and_casing>> LookupHoleAndCasing([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        //{
        //    var services = new HoleAndCasingServices(this.dataContext);
        //    var response = await Task.Run<ApiResponsePage<vw_hole_and_casing>>(() =>
        //    {
        //        var result = new ApiResponsePage<vw_hole_and_casing>();
        //        var data = services.LookupHoleAndCasing(marvelDataSourceRequest, wellId);
        //        result.CurrentPage = data.CurrentPage;
        //        result.TotalPages = data.TotalPages;
        //        result.TotalItems = data.TotalItems;
        //        result.ItemsPerPage = data.ItemsPerPage;
        //        result.Items = data.Items;
        //        return result;
        //    });
        //    return response;
        //}

        [HttpGet]
        [Route("chart/actualDepth")]
        public async Task<ApiResponse> GetActualDepth([FromUri] string wellId)
        {
            var services = new WellDataServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.GetActualDepth(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }


        [HttpGet]
        [Route("chart/actualCost")]
        public async Task<ApiResponse> GetActualCost([FromUri] string wellId)
        {
            var services = new WellDataServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.GetActualCost(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("chart/tvd/planningDepth")]
        public async Task<ApiResponse> GetPlanningDepth([FromUri] string wellId)
        {
            var services = new WellDataServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.GetPlanningDepth(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("chart/tvd/planningCost")]
        public async Task<ApiResponse> GetPlanningCost([FromUri] string wellId)
        {
            var services = new WellDataServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.GetPlanningCost(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("getByWellId/{wellId}/{drillingDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_operation_activity>>> getByWellId(string wellId, DateTime drillingDate)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_operation_activity>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_operation_activity>>();
                var record = services.getByWellId(wellId, drillingDate);
                result.Status.Success = (record != null);
                result.Data = record.Data;
                return result;
            });
            return response;
        }

        [Route("getmud")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_mud>> Detail([FromUri]string drillingId, [FromUri]string dataMudType)
        {
            var services = new DrillingMudServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_mud>>(() =>
            {
                var result = new ApiResponse<vw_drilling_mud>();
                var record = services.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", drillingId, dataMudType);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getByDrillingId/{recordId}/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetByDrillingId(string recordId, string wellId)
        {
            var services = new DrillingOperationActivityServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                return services.GetActivityByDrillingId(recordId, wellId);
            });
            return response;
        }

        [Route("getBhaBit/{recordId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bha_bit>>> GetDetail(string recordId)
        {
            var services = new DrillingBhaBitServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_bit>>();
                result = services.GetDetail(recordId);
                return result;
            });
            return response;
        }

        [Route("{wellId}/dailyCost")]
        [HttpGet]
        public async Task<ApiResponse> GetDailyCost([FromUri] string wellId)
        {
            var services = new BusinessLogic.UTC.Services.WellDashboardServices();
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
               
                var data = services.Bs19FormatByWellID(wellId);
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<Base19Format>("description") },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
                return result;
            });
            return response;
        }
    }
}

