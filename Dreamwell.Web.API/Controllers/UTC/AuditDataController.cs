﻿using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dreamwell.Infrastructure;
using System.Threading.Tasks;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/utc/Auditdata")]
    public class AuditDataController : ApiBaseController
    {
        [Route("getListDrillingSubmitted/{wellId}/{startDate}/{endDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling>>> getListManageUsage(string wellId, [FromUri] DateTime startDate, [FromUri] DateTime endDate)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                var record = services.GetViewAll(" AND r.well_id=@0 AND (r.drilling_date BETWEEN @1 AND @2)  ", wellId, startDate, endDate);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling>> GetDetail(string recordId)
        {
            var services = new DrillingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling>>(() =>
            {
                return services.Detail(recordId, userSession);
            });
            return response;
        }

        [Route("getmud")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_mud>> Detail([FromUri]string drillingId, [FromUri]string dataMudType)
        {
            var services = new DrillingMudServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_mud>>(() =>
            {
                var result = new ApiResponse<vw_drilling_mud>();
                var record = services.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", drillingId, dataMudType);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
