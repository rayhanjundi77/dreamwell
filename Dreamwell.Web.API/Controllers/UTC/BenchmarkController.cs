﻿using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dreamwell.Infrastructure;
using System.Threading.Tasks;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.BusinessLogic.UTC.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/utc/Benchmark")]
    public class BenchmarkController : ApiBaseController
    {

        [HttpPost]
        [Route("getPlanVsActualDepth")]
        public async Task<ApiResponse<List<vw_well>>> getPlanVsActualDepth(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                try
                {
                    result = services.GetPlanVsActualDepth(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("getTotalWell")]
        public async Task<ApiResponse<List<ReportTotalWellByYears>>> getTotalWell(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<ReportTotalWellByYears>>>(() =>
            {
                var result = new ApiResponse<List<ReportTotalWellByYears>>();
                try
                {
                    result = services.getTotalWell(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("getPlanVsCostFiltering")]
        public async Task<ApiResponse<List<vw_well>>> getPlanVsCost(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                try
                {
                    result = services.getPlanVsCostFiltering(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("getRigFiltering")]
        public async Task<ApiResponse<List<vw_drilling_contractor>>> getRigFiltering(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_contractor>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_contractor>>();
                try
                {
                    result = services.getRigFiltering(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }


        [HttpPost]
        [Route("getPlanVsActualDrilledDays")]
        public async Task<ApiResponse<List<vw_well>>> getPlanVsActualDrilledDays(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                try
                {
                    result = services.getPlanVsActualDrilledDays(data);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("getCummulativeCost")]
        public async Task<ApiResponse<List<vw_well>>> getCummulativeCost(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                try
                {
                    result = services.getCummulativeCost(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("getCostPerDepth")]
        public async Task<ApiResponse<List<vw_report_well_cost_per_depth>>> getCostPerDepth(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_report_well_cost_per_depth>>>(() =>
            {
                var result = new ApiResponse<List<vw_report_well_cost_per_depth>>();
                try
                {
                    result = services.getCostPerDepth(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("GetWellIadc")]
        public async Task<ApiResponse<List<vw_benchmark_report_iadc>>> GetWellIadc(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_benchmark_report_iadc>>>(() =>
            {
                var result = new ApiResponse<List<vw_benchmark_report_iadc>>();
                try
                {
                    result = services.GetWellIadc(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("getPTandNPThours")]
        public async Task<ApiResponse<List<vw_well>>> getPTandNPThours(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                try
                {
                    result = services.getPTandNPThours(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("getRop")]
        public async Task<ApiResponse<List<vw_well>>> getRop(BenchmarkReportViewModel data)
        {
            var services = new BenchmarkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                try
                {
                    result = services.getRop(data);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }


        [HttpGet]
        [Route("getWellBit/{wellId}")]
        public async Task<ApiResponse<object>> getWellBit([FromUri] string wellId)
        {
            var response = await Task.Run<ApiResponse<object>>(() =>
            {
                var result = new ApiResponse<object>();
                try
                {
                    List<vw_well_bit> wellbitdata = new List<vw_well_bit>();

                    var qry = PetaPoco.Sql.Builder.Append(@"select distinct id, bit_size from dbo.well_bit
                                WHERE well_id=@0 and bit_size is not null", wellId);

                    wellbitdata = vw_well_bit.Fetch(qry);

                    var welldata = well.FirstOrDefault(" WHERE id=@0 ", wellId);


                    var data = new
                    {
                        datawell = welldata,
                        databit = wellbitdata
                    };


                    result.Status.Success = true;
                    result.Data = data;
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("getMultiWellBit")]
        public async Task<ApiResponse<object>> getMultiWellBit(BenchmarkReportViewModel param)
        {
            var response = await Task.Run<ApiResponse<object>>(() =>
            {
                var result = new ApiResponse<object>();
                try
                {
                    List<vw_well_bit> wellbitdata = new List<vw_well_bit>();

                    var qry = PetaPoco.Sql.Builder.Append(@"select distinct bit_size from dbo.vw_drilling_bha_bit
                                WHERE well_id in (@listwell) and bit_size is not null ORDER BY bit_size ", new { listwell = param.well_id });

                    wellbitdata = vw_well_bit.Fetch(qry);

                    var data = new
                    {
                        databit = wellbitdata
                    };


                    result.Status.Success = true;
                    result.Data = data;
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }


        [HttpPost]
        [Route("getWellRopData1")]
        public async Task<DataTablesResponse> getWellBitRopData1([FromBody] DataTableRequest dataTablesRequest, BenchmarkReportViewModel data)
        {
            var services = new DrillingBhaBitServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling_bha_bit>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("getWellRopData")]
        [HttpPost]
        public async Task<ApiResponse<List<vw_drilling_bha_bit>>> getWellRopData([FromBody] BenchmarkReportViewModel param)
        {
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_bit>>();
                try
                {
                    var qryfilter = PetaPoco.Sql.Builder.Append(@" WHERE bit_size IS NOT NULL ");

                    if (param.well_year != null)
                    {
                        if (param.well_year.Count > 0)
                        {
                            qryfilter.Append(@" AND YEAR(drilling_date) IN (@years) ", new { years = param.well_year });
                        }
                    }                    

                    if (param.well_id.Count > 0)
                    {
                        qryfilter.Append(@" AND well_bit_id IN (@wellbitid) ", new { wellbitid = param.well_id });
                    }

                    if (param.well_status.Count > 0)
                    {
                        qryfilter.Append(@" AND ISNULL(well_status,0) IN (@wellstatus) ", new { wellstatus = param.well_status });
                    }

                    qryfilter.Append(@" ORDER BY well_name ASC, bit_size DESC");


                    var listdata = vw_drilling_bha_bit.Fetch(qryfilter);
                    if (listdata == null)
                    {
                        listdata = new List<vw_drilling_bha_bit>();
                    }
                    result.Data = listdata;
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        //[Route("getAvgWellBitRopChart")]
        //[HttpPost]
        //public async Task<ApiResponse<object>> getAvgWellBitRopChart([FromBody] BenchmarkReportViewModel param)
        //{
        //    var response = await Task.Run<ApiResponse<object>>(() =>
        //    {
        //        var result = new ApiResponse<object>();
        //        try
        //        {
        //            var qry = getQueryWellBitRop(param);
        //            List<vw_drilling_bha_bit> wellbitdata = new List<vw_drilling_bha_bit>();

        //            wellbitdata = vw_drilling_bha_bit.Fetch(qry);

        //            if (wellbitdata == null)
        //            {
        //                wellbitdata = new List<vw_drilling_bha_bit>();
        //            }

        //            result.Status.Success = true;
        //            result.Data = wellbitdata;
        //            return result;
        //        }
        //        catch (Exception ex)
        //        {
        //            result.Status.Success = false;
        //            result.Status.Message = ex.ToString();
        //        }
        //        return result;
        //    });
        //    return response;
        //}

        [Route("getWellBitRopData")]
        [HttpPost]
        public async Task<ApiResponse<List<vw_drilling_bha_bit>>> getWellBitRopData([FromBody] BenchmarkReportViewModel param)
        {
            var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bha_bit>>();
                try
                {
                    var qryfilter = PetaPoco.Sql.Builder.Append(@" WHERE bit_size IS NOT NULL ");

                    if (param.well_year != null)
                    {
                        if (param.well_year.Count > 0)
                        {
                            qryfilter.Append(@" AND YEAR(drilling_date) IN (@years) ", new { years = param.well_year });
                        }
                    }

                    if (param.well_id.Count > 0)
                    {
                        qryfilter.Append(@" AND well_id IN (@well_id) ", new { well_id = param.well_id });
                    }

                    if (param.bit_size.Count > 0)
                    {
                        qryfilter.Append(@" AND bit_size IN (@bit_size) ", new { bit_size = param.bit_size });
                    }

                    if (param.well_status.Count > 0)
                    {
                        qryfilter.Append(@" AND ISNULL(well_status,0) IN (@wellstatus) ", new { wellstatus = param.well_status });
                    }


                    qryfilter.Append(@" ORDER BY well_name ASC, bit_size DESC");


                    var listdata = vw_drilling_bha_bit.Fetch(qryfilter);
                    if (listdata == null)
                    {
                        listdata = new List<vw_drilling_bha_bit>();
                    }
                    result.Data = listdata;
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        //[Route("getWellBitRopData")]
        //[HttpPost]
        //public async Task<ApiResponse<List<vw_drilling_bha_bit>>> getWellBitRopData([FromBody] BenchmarkReportViewModel param)
        //{
        //    var response = await Task.Run<ApiResponse<List<vw_drilling_bha_bit>>>(() =>
        //    {
        //        var result = new ApiResponse<List<vw_drilling_bha_bit>>();
        //        try
        //        {
        //            var qryfilter = getQueryWellBitRop(param);

        //            var listdata = vw_drilling_bha_bit.Fetch(qryfilter);

        //            if (listdata == null)
        //            {
        //                listdata = new List<vw_drilling_bha_bit>();
        //            }

        //            result.Data = listdata;
        //            result.Status.Success = true;
        //            return result;
        //        }
        //        catch (Exception ex)
        //        {
        //            appLogger.Error(ex);
        //            result.Status.Success = false;
        //            result.Status.Message = ex.Message;
        //        }
        //        return result;
        //    });
        //    return response;
        //}

        public PetaPoco.Sql getQueryWellBitRop(BenchmarkReportViewModel param)
        {
            var qry = PetaPoco.Sql.Builder.Append(@"SELECT r.id, r.well_name, rd.* 
                                                            , vd.bit_size, vd.mudlogging_wob_min
                                                            , vd.mudlogging_wob_max, mudlogging_rpm_min, mudlogging_rpm_max
                                                            , mudlogging_flowrate_min, mudlogging_flowrate_max, dg_footage
                                                            , mudlogging_hkld, mudlogging_mwi, mudlogging_mwo
                                                            FROM well r
                                                            OUTER APPLY
                                                            (
                                                                SELECT TOP 1 A.* FROM
                                                                (
                                                                    SELECT B.* FROM
                                                                    (
                                                                        SELECT id, well_id, serial_number, ISNULL(AVG(dg_rop_overall),0) AS dg_rop_overall, ISNULL(MIN(depth_in),0) AS depth_in, 
                                                                        ISNULL(MAX(depth_out),0) AS depth_out, ISNULL(AVG(duration),0) AS duration
                                                                        FROM dbo.vw_drilling_bha_bit
                                                                        WHERE  bit_size =  @bitsize --AND dg_rop_overall > 0
                                                                        AND YEAR(drilling_date) IN (@years) 
                                                                        AND ISNULL(well_status,0) IN (@wellstatus)
                                                                        GROUP BY id, well_id,serial_number
                                                                    )
                                                                    B
                                                                    WHERE B.well_id=r.id
                                                                ) A
                                                                ORDER BY A.dg_rop_overall DESC
                                                            ) rd
                                                            LEFT JOIN dbo.vw_drilling_bha_bit vd ON rd.id=vd.id
                                                            WHERE r.is_active=1
                                                            AND r.id IN (@listwell)
                                                            ORDER BY r.well_name", new { bitsize = param.bit_size }, new { years = param.well_year }, new { wellstatus = param.well_status }, new { listwell = param.well_id });

            return qry;
        }

        [HttpGet]
        [Route("GetBitByWellId/{wellId}")]
        public async Task<ApiResponse<List<vw_report_bit_record>>> getByWellId(string wellId)
        {
            var userContext = new DataAccess.DataContext(BusinessLogic.Core.SysConfig.UserSetup.sysAdminId);
            var services = new BitRecordServices(userContext);
            var response = await Task.Run<ApiResponse<List<vw_report_bit_record>>>(() =>
            {
                var result = new ApiResponse<List<vw_report_bit_record>>();
                try
                {
                    result = services.GetBitByWellId(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("GetUserCreated/{wellId}")]
        public async Task<ApiResponse<List<vw_report_bit_record>>> getUserCreated(string wellId)
        {
            var userContext = new DataAccess.DataContext(BusinessLogic.Core.SysConfig.UserSetup.sysAdminId);
            var services = new BitRecordServices(userContext);
            var response = await Task.Run<ApiResponse<List<vw_report_bit_record>>>(() =>
            {
                var result = new ApiResponse<List<vw_report_bit_record>>();
                try
                {
                    result = services.getRecordByWellId(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }
    }
}

