﻿using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dreamwell.Infrastructure;
using System.Threading.Tasks;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/utc/ServicePerformance")]
    public class ServicePerformanceController : ApiBaseController
    {
        [Route("{recordId}/JobPersonel")]
        [HttpPost]
        public async Task<List<vw_drilling_personel>> JobPersonel([FromUri] string recordId)
        {
            var response = await Task.Run<List<vw_drilling_personel>>(() =>
            {
                var vwdrillingpersonel = new List<vw_drilling_personel>();

                var qry = PetaPoco.Sql.Builder.Append(@"select distinct personel, company from vw_drilling_personel
                where drilling_id in
                (
	                select id from drilling where well_id=@0

                )", recordId);

                vwdrillingpersonel = vw_drilling_personel.Fetch(qry);

                return vwdrillingpersonel;
            });
            return response;
        }


        [Route("NPT/Details/{recordId}")]
        [HttpPost]
        public async Task<ApiResponse<List<vw_iadc_analysis_npt>>> GetNPTDetails([FromUri] string recordId)
        {
            var services = new IadcServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_iadc_analysis_npt>>>(() =>
            {
                var result = new ApiResponse<List<vw_iadc_analysis_npt>>();
                try
                {
                    result.Data = services.GetNPTDetailsByWellId(recordId);
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }


        [Route("Trayek/{fieldId}")]
        [HttpPost]
        public async Task<ApiResponse<List<vw_service_performance_group>>> GetTrayek([FromUri] string fieldId, ServicePerformanceFilterWell filterwell)
        {
            var services = new ServicePerformanceServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_service_performance_group>>>(() =>
            {
                var result = new ApiResponse<List<vw_service_performance_group>>();
                try
                {
                    result = services.getServicePerformancetrayek(fieldId, filterwell.well_list);
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }


        [Route("PTNPT/Details")]
        [HttpPost]
        public async Task<ApiResponse<object>> GetPTNPTDetails([FromBody] ReportServicePerformanceModel param)
        {

            var services = new ServicePerformanceServices(this.dataContext);
            var response = await Task.Run<ApiResponse<object>>(() =>
            {
                var result = new ApiResponse<object>();
                try
                {
                    string recordId = string.Empty;
                    string[] ayear = { };

                    if (param != null)
                    {
                        recordId = param.recordId;
                        ayear = param.ayear;

                        List<vw_iadc_analysis_npt> dataPTNPT = services.GetPTNPTDetailsByAPH(recordId, ayear);

                        List<vw_iadc_analysis_npt> dataNPT = dataPTNPT.Where(w => w.type == "2").OrderBy(o => o.description).ToList();

                        ServicePerformanceOperationalModel operationaldata = new ServicePerformanceOperationalModel();
                        operationaldata.total_npt = Convert.ToDecimal(dataNPT.Sum(s => s.hours));
                        operationaldata.total_opr = Convert.ToDecimal(dataPTNPT.Sum(s => s.hours));

                        var objdata = new
                        {
                            data = dataNPT,
                            datadesc = operationaldata

                        };

                        result.Data = objdata;
                        result.Status.Success = true;
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Invalid Parameter";
                    }

                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        [Route("PTNPT/DetailsYear")]
        [HttpPost]
        public async Task<ApiResponse<object>> GetPTNPTDetailsYear([FromBody] ReportServicePerformanceModel param)
        {

            var services = new ServicePerformanceServices(this.dataContext);
            var response = await Task.Run<ApiResponse<object>>(() =>
            {
                var result = new ApiResponse<object>();
                try
                {
                    string recordId = string.Empty;
                    string[] ayear = { };

                    if (param != null)
                    {
                        recordId = param.recordId;
                        ayear = param.ayear;

                        List<vw_iadc_analysis_npt> dataPTNPT = services.GetPTNPTDetailsYearByAPH(recordId, ayear);
                        List<vw_iadc_analysis_npt> dataNPT = dataPTNPT.Where(w => w.type == "2").OrderBy(o => o.description).ToList();
                        List<string> dataNPTList = dataNPT.DistinctBy(x => x.description).Select(s => s.description).OrderBy(x => x).ToList();

                        List<ServicePerformanceNptByYear> DataYear = new List<ServicePerformanceNptByYear>();
                        DataYear = dataNPT.GroupBy(g => g.iyear, (key, g) => new ServicePerformanceNptByYear
                        {
                            name = key.ToString(),
                            data = (from a in dataNPTList
                                    join npt in g on a equals npt.description into j
                                    from j1 in j.DefaultIfEmpty()
                                    select new
                                    {
                                        npt_name = a,
                                        value = j1 != null ? j1.hours : 0
                                    } into selection
                                    orderby selection.npt_name
                                    select selection.value).ToArray()
                        }).ToList();



                        List<ServicePerformanceOperationalModel> listoperationaldata = new List<ServicePerformanceOperationalModel>();
                        int iyear = 0;
                        for (int i = 0; i < DataYear.Count; i++)
                        {
                            iyear = Convert.ToInt32(DataYear[i].name);
                            ServicePerformanceOperationalModel operationaldata = new ServicePerformanceOperationalModel();
                            operationaldata.syear = DataYear[i].name;
                            operationaldata.total_npt = Convert.ToDecimal(dataNPT.Where(w => w.iyear == iyear).Sum(s => s.hours));
                            operationaldata.total_opr = Convert.ToDecimal(dataPTNPT.Where(w => w.iyear == iyear).Sum(s => s.hours));

                            listoperationaldata.Add(operationaldata);
                        }



                        var objdata = new
                        {
                            data = dataNPT,
                            listNPT = dataNPTList,
                            hoursPerYear = DataYear,
                            listdatadesc = listoperationaldata,

                        };

                        result.Data = objdata;
                        result.Status.Success = true;
                    }
                    else
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Invalid Parameter";
                    }

                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }


        #region Drilling Performance

        [Route("DrillingPerformance")]
        [HttpPost]
        public async Task<ApiResponse<List<vw_drilling_performance>>> GetDrillingPerformance([FromBody] ReportServicePerformanceModel param)
        {
            var services = new ServicePerformanceServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_performance>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_performance>>();
                try
                {
                    result.Data = services.GetDrillingPerformance(param.recordId, param.ayear);
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }


        [Route("WellDrillingPerformance")]
        [HttpPost]
        public async Task<ApiResponse<List<vw_drilling_performance>>> GetWellDrillingPerformance([FromBody] ReportServicePerformanceModel param)
        {
            var services = new ServicePerformanceServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_performance>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_performance>>();
                try
                {
                    result.Data = services.GetWellDrillingPerformance(param.recordId, param.ayear);
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        [Route("MaterialUssagePerformance")]
        [HttpPost]
        public async Task<ApiResponse<dynamic>> GetMaterialUssagePerformance([FromBody] ReportServicePerformanceModel param)
        {
            var services = new ServicePerformanceServices(this.dataContext);
            var response = await Task.Run<ApiResponse<dynamic>>(() =>
            {
                var result = new ApiResponse<dynamic>();
                try
                {
                    result.Data = services.GetMaterialUssagePerformance(param.recordId, param.ayear);
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        
        #endregion
    }
}

