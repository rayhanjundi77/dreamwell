﻿using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dreamwell.Infrastructure;
using System.Threading.Tasks;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using PetaPoco;
using System.Web.Http.Results;
using static Dreamwell.DataAccess.dreamwellRepo;
using System.Configuration;
using MoreLinq;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Bibliography;
using System.Numerics;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Math;
using CommonTools.UnitOfMeasurement.Length;
using DocumentFormat.OpenXml.InkML;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.VariantTypes;
using System.Collections;
using CommonTools;
using System.Reflection;
using DocumentFormat.OpenXml.Office2013.Excel;
using System.Web.Services.Description;
using DocumentFormat.OpenXml.EMMA;
using System.Windows.Media.Media3D;
using CommonTools.UnitOfMeasurement.Time;
using DocumentFormat.OpenXml.ExtendedProperties;
using System.Windows.Interop;
using CommonTools.Kendo.SpreadSheet;
using CommonTools.UnitOfMeasurement.Pressure;
using OfficeOpenXml.Drawing;
using CommonTools.Helper;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/utc/SingleWell")]
    public class SingleWellController : ApiBaseController
    {
        [HttpGet]
        [Route("chart/PTNPT")]
        public async Task<ApiResponse> GetPTNPT([FromUri] string wellId)
        {
            var services = new SingleWellServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.GetPTNPTChart(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("GetDrillingByWell/{wellId}")]
        public async Task<ApiResponse<List<vw_drilling>>> GetDrillingByWell(string wellId)
        {
            var services = new SingleWellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling>>();
                try
                {
                    result = services.GetDrillingByWell(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("GetDrillingStuckPipe/{wellId}")]
        public async Task<ApiResponse<List<vw_report_drilling_stuck_pipe>>> GetDrillingStuckPipe(string wellId)
        {
            var services = new SingleWellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_report_drilling_stuck_pipe>>>(() =>
            {
                var result = new ApiResponse<List<vw_report_drilling_stuck_pipe>>();
                try
                {
                    result = services.GetDrillingStuckPipe(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("GetWellIadc/{wellId}")]
        public async Task<ApiResponse<List<vw_report_well_iadc>>> GetWellIadc(string wellId)
        {
            var services = new SingleWellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_report_well_iadc>>>(() =>
            {
                var result = new ApiResponse<List<vw_report_well_iadc>>();
                try
                {
                    result = services.GetWellIadc(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("GetTataWaktuPengeboran/{wellId}")]
        public async Task<ApiResponse<ReportTataWaktuPengeboranViewModel>> GetTataWaktuPengeboran(string wellId)
        {
            var services = new SingleWellServices(this.dataContext);
            var services_tvdPlan = new TVDPlanServices(this.dataContext);
            var response = await Task.Run<ApiResponse<ReportTataWaktuPengeboranViewModel>>(() =>
            {
                var result = new ApiResponse<ReportTataWaktuPengeboranViewModel>();
                result.Data = new ReportTataWaktuPengeboranViewModel();
                try
                {
                    result.Data.well_id = wellId;
                    result.Data.report_drilling = services.GetDrillingCostAndDepth(wellId).Data;
                    result.Data.tvd_plan = services_tvdPlan.GetAll(" AND well_id = @0 ORDER BY created_on ASC ", wellId);
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("GetTataWaktuPengeboranNew/{wellId}")]
        public async Task<ApiResponse<ReportTataWaktuPengeboranViewModel>> GetTataWaktuPengeboranNew(string wellId)
        {
            var services = new SingleWellServices(this.dataContext);
            var services_tvdPlan = new TVDPlanServices(this.dataContext);
            var service_drilling = new DrillingServices(this.dataContext);

            var response = await Task.Run<ApiResponse<ReportTataWaktuPengeboranViewModel>>(() =>
            {
                var result = new ApiResponse<ReportTataWaktuPengeboranViewModel>();
                result.Data = new ReportTataWaktuPengeboranViewModel();
                try
                {
                    result.Data.well_id = wellId;
                    result.Data.report_drilling = services.GetDrillingCostAndDepth(wellId).Data;
                    result.Data.drilling_data = service_drilling.GetViewAll(" AND r.well_id = @0 ORDER BY r.drilling_date ASC ", wellId);
                    result.Data.tvd_plan = services_tvdPlan.GetAll(" AND well_id = @0 ORDER BY no ASC ", wellId);
                    result.Status.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("Get2DTrajectory/{wellId}")]
        public async Task<ApiResponse<Report2DTrajectoryViewModel>> Get2DTrajectory(string wellId)
        {
            var services = new SingleWellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<Report2DTrajectoryViewModel>>(() =>
            {
                var result = new ApiResponse<Report2DTrajectoryViewModel>();
                try
                {
                    result = services.Get2DTrajectory(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        //[Route("lookupHoleAndCasing")]
        //[HttpPost]
        //public async Task<ApiResponsePage<vw_well_hole_and_casing>> LookupHoleAndCasing([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        //{
        //    var services = new HoleAndCasingServices(this.dataContext);
        //    var response = await Task.Run<ApiResponsePage<vw_well_hole_and_casing>>(() =>
        //    {
        //        var result = new ApiResponsePage<vw_well_hole_and_casing>();
        //        var data = services.Lookup(marvelDataSourceRequest);
        //        result.CurrentPage = data.CurrentPage;
        //        result.TotalPages = data.TotalPages;
        //        result.TotalItems = data.TotalItems;
        //        result.ItemsPerPage = data.ItemsPerPage;
        //        result.Items = data.Items;
        //        return result;
        //    });
        //    return response;
        //}

        //[Route("lookupBhaByHoleAndCasing/{holeAndCasingId}")]
        //[HttpPost]
        //public async Task<ApiResponsePage<vw_well_bha>> LookupBhaByHoleAndCasing([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string holeAndCasingId)
        //{
        //    var services = new WellBhaServices(this.dataContext);
        //    var response = await Task.Run<ApiResponsePage<vw_well_bha>>(() =>
        //    {
        //        var result = new ApiResponsePage<vw_well_bha>();
        //        var data = services.LookupByHoleAndCasing(marvelDataSourceRequest, holeAndCasingId);
        //        result.CurrentPage = data.CurrentPage;
        //        result.TotalPages = data.TotalPages;
        //        result.TotalItems = data.TotalItems;
        //        result.ItemsPerPage = data.ItemsPerPage;
        //        result.Items = data.Items;
        //        return result;
        //    });
        //    return response;
        //}

        //[Route("lookupBitByHoleAndCasing/{holeAndCasingId}")]
        //[HttpPost]
        //public async Task<ApiResponsePage<vw_well_bit>> LookupBitByHoleAndCasing([FromBody] MarvelDataSourceRequest marvelDataSourceRequest, string holeAndCasingId)
        //{
        //    var services = new WellBitServices(this.dataContext);
        //    var response = await Task.Run<ApiResponsePage<vw_well_bit>>(() =>
        //    {
        //        var result = new ApiResponsePage<vw_well_bit>();
        //        var data = services.LookupByHoleAndCasing(marvelDataSourceRequest, holeAndCasingId);
        //        result.CurrentPage = data.CurrentPage;
        //        result.TotalPages = data.TotalPages;
        //        result.TotalItems = data.TotalItems;
        //        result.ItemsPerPage = data.ItemsPerPage;
        //        result.Items = data.Items;
        //        return result;
        //    });
        //    return response;
        //}

        [HttpGet]
        [Route("GetIadcAnalysis/{wellId}")]
        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetIadcAnalysis(string wellId)
        {
            var services = new SingleWellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_hole_and_casing>>>(() =>
            {
                //var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
                //try
                //{
                //    result = services.GetIadcAnalysis(wellId);
                //    return result;
                //}
                //catch (Exception ex)
                //{
                //    appLogger.Error(ex);
                //    result.Status.Success = false;
                //    result.Status.Message = ex.ToString();
                //}
                //return result;
                return services.GetIadcAnalysis(wellId);
            });
            return response;
        }

        [HttpGet]
        [Route("GetIadcAnalysisNpt/{drillingHoleCasingId}")]
        public async Task<ApiResponse<List<vw_iadc_analysis_npt>>> GetIadcAnalysisNpt(string drillingHoleCasingId)
        {
            var services = new SingleWellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_iadc_analysis_npt>>>(() =>
            {
                //var result = new ApiResponse<List<vw_iadc_analysis_npt>>();
                //try
                //{
                //    result = services.GetIadcAnalysisNpt(drillingHoleCasingId);
                //    return result;
                //}
                //catch (Exception ex)
                //{
                //    appLogger.Error(ex);
                //    result.Status.Success = false;
                //    result.Status.Message = ex.ToString();
                //}
                //return result;
                return services.GetIadcAnalysisNpt(drillingHoleCasingId);
            });
            return response;
        }

        [HttpGet]
        [Route("GetBitByWellId/{wellId}")]
        public async Task<ApiResponse<List<vw_report_well_bit>>> GetBitByWellId(string wellId)
        {
            var userContext = new DataAccess.DataContext(BusinessLogic.Core.SysConfig.UserSetup.sysAdminId);
            var services = new WellFinalReportServices(userContext);
            var response = await Task.Run<ApiResponse<List<vw_report_well_bit>>>(() =>
            {
                var result = new ApiResponse<List<vw_report_well_bit>>();
                try
                {
                    result = services.GetBitByWellId(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpGet]
        [Route("GetHoleCasingActual/{wellId}")]
        public async Task<ApiResponse<List<vw_report_hole_casing_actual>>> GetHoleCasingActual(string wellId)
        {
            var userContext = new DataAccess.DataContext(BusinessLogic.Core.SysConfig.UserSetup.sysAdminId);
            var services = new WellFinalReportServices(userContext);
            var response = await Task.Run<ApiResponse<List<vw_report_hole_casing_actual>>>(() =>
            {
                var result = new ApiResponse<List<vw_report_hole_casing_actual>>();
                try
                {
                    result = services.GetHoleCasingActual(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("{fieldId}/detailByField")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well>>> GetByField(string fieldId)
        {
            var services = new WellServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well>>>(() =>
            {
                var result = new ApiResponse<List<vw_well>>();
                return services.getByField(fieldId);
                //var record = services.getByField(fieldId);
                //result.Status.Success = (record != null);
                //result.Data = record.Data;
                //return result;
            });
            return response;
        }

        [Route("report/ddr-pertamina/{wellId}/{fieldId}")]
        [HttpGet]
        public async Task<ApiResponse<string>> GetDdrPertamina(string wellId, string fieldId)
        {
            var services = new WellServices(this.dataContext);
            var drillingService = new DrillingServices(this.dataContext);
            var wellObjectUomMapservices = new WellObjectUomMapServices(this.dataContext);
            var currencyServices = new CurrencyServices(this.dataContext);
            var drillingOperationActivityServices = new DrillingOperationActivityServices(this.dataContext);
            var drillingBhaBitServices = new DrillingBhaBitServices(this.dataContext);
            var wellBhaComponentServices = new WellBhaComponentServices(this.dataContext);
            var wellBitServices = new WellBitServices(this.dataContext);
            var drillingTransportServices = new DrillingTransportServices(this.dataContext);
            var drillingBulkServices = new DrillingBulkServices(this.dataContext);
            var drillingAeratedServices = new DrillingAeratedServices(this.dataContext);
            var drillFormationActualServices = new DrillFormationActualServices(this.dataContext);
            var drillFormationActualDetailServices = new DrillFormationActualDetailServices(this.dataContext);
            var stoneServices = new StoneServices(this.dataContext);
            var drillingDeviationServices = new DrillingDeviationServices(this.dataContext);
            var drillingMudServices = new DrillingMudServices(this.dataContext);
            var drillingTotalVolumeServices = new DrillingTotalVolumeServices(this.dataContext);
            var drillingMudPitServices = new DrillingMudPitServices(this.dataContext);
            var drillingChemicalUsedServices = new DrillingChemicalUsedServices(this.dataContext);
            var drillingPersonelServices = new DrillingPersonelServices(this.dataContext);
            var response = await Task.Run<ApiResponse<string>>(() =>
            {
                var result = new ApiResponse<string>();
                try
                {
                    var detail = drillingService.Detail(fieldId, userSession);
                    var uomMap = wellObjectUomMapservices.GetViewAll(" AND r.is_active = 1 AND r.well_id = @0 ", wellId);
                    var currency = currencyServices.GetById(detail.Data.cummulative_cost_currency_id);
                    var drillingActivityResult = drillingOperationActivityServices.GetActivityByDrillingId(fieldId, wellId);
                    var drillingBhaBitRresult = drillingBhaBitServices.GetDetail(fieldId);
                    var drillingBhaBit = (drillingBhaBitRresult.Data.Count() == 0 ? null : drillingBhaBitRresult.Data.First());
                    var wellBhaComponent = new List<vw_well_bha_component>();
                    var wellBit = new vw_well_bit();
                    if (drillingBhaBit != null)
                    {
                        wellBhaComponent = wellBhaComponentServices.GetViewAll(" AND r.well_bha_id = @0 AND r.is_active=@1 ORDER BY r.created_on ASC ", drillingBhaBit.well_bha_id, true);
                        wellBit = wellBitServices.GetViewById(drillingBhaBit.well_bit_id);
                    }
                    var drillingTransport = drillingTransportServices.GetViewAll(" AND r.drilling_id=@0 ", fieldId);
                    var drillingBulk = drillingBulkServices.getItemByWellId(fieldId, wellId, detail.Data.drilling_date?.ToString("yyyy-MM-dd"));
                    var aerated = drillingAeratedServices.GetViewFirstOrDefault(" AND r.drilling_id=@0 ", fieldId);
                    List<drill_formations_actual> drillActual = ((IEnumerable)drillFormationActualServices.GetList(wellId).Data).Cast<drill_formations_actual>().ToList();
                    var stones = stoneServices.LookupAll(new MarvelDataSourceRequest() { PageSize = 1000 });
                    var deviations = drillingDeviationServices.GetViewAll(" AND r.drilling_id=@0 ORDER BY measured_depth ASC ", fieldId);
                    var mud1 = drillingMudServices.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", fieldId, "mud_1");
                    var mud2 = drillingMudServices.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", fieldId, "mud_2");
                    var mud3 = drillingMudServices.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", fieldId, "mud_3");
                    var volumeDrillingResult = drillingTotalVolumeServices.GetViewAll(" AND r.drilling_id=@0 ", fieldId);
                    var volumeDrilling = new vw_drilling_total_volume();
                    if (volumeDrillingResult.Count() > 0)
                    {
                        volumeDrilling = volumeDrillingResult.First();
                    }
                    var mudPit = drillingMudPitServices.GetViewAll(" AND r.drilling_id=@0 ", fieldId);
                    var chemical = drillingChemicalUsedServices.GetViewAll(" AND r.drilling_id=@0 ", fieldId);
                    var personels = drillingPersonelServices.GetViewAll(" AND r.drilling_id=@0 ORDER BY r.created_on ASC ", fieldId);

                    var nameFile = "DDR Pertamina " + detail.Data.well_name + " " + (int)detail.Data.dfs + ".xlsx";
                    var path = ConfigurationManager.AppSettings["reportPath"].ToString() + nameFile;
                    using (var workbook = new XLWorkbook())
                    {
                        var logo1 = ConfigurationManager.AppSettings["logoReportPath"].ToString() + "report-ddr-pertamina-1.jpg";
                        var logo2 = ConfigurationManager.AppSettings["logoReportPath"].ToString() + "report-ddr-pertamina-2.jpg";

                        //var logo1 = @"D:\Project\Pertamina\Dreamwell\Dreamwell.Web\Content\img\report-excel\report -ddr-pertamina-1.jpg";
                        //var logo2 = @"D:\Project\Pertamina\Dreamwell\Dreamwell.Web\Content\img\report-excel\report -ddr-pertamina-2.jpg";

                        var ws = workbook.Worksheets.Add("Report");
                        ws.AddPicture(logo1).MoveTo(ws.Cell("A1")).Scale(0.4);
                        ws.AddPicture(logo2).MoveTo(ws.Cell("Q1")).Scale(0.4);
                        ws.Range("F2:N3").Row(1).Merge();
                        ws.Cell("F2").Value = detail.Data.well_name + " Daily Drilling Report " + (int)detail.Data.dfs;
                        ws.Cell("F2").Style.Font.Bold = true;
                        ws.Cell("F2").Style.Font.FontSize = 20;
                        ws.Cell("F2").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("A4").Value = "1. General Well Data";
                        ws.Cell("A4").Style.Font.Bold = true;
                        ws.Cell("A4").Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A4:S4").Row(1).Merge();

                        ws.Range("A5:S13").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A5:S13").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A5:S13").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A5:S13").Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        var fields = new string[,]
                        {
                            {"A", "B" },
                            {"C", "D"},
                            {"F", "G" },
                            {"H", "I" },
                            {"K", "L" },
                            {"M", "N" },
                            {"P", "Q" },
                            {"R", "S" },
                        };
                        for (int j = 0; j < fields.GetLength(0); j++)
                        {
                            for (int i = 5; i < 14; i++)
                            {
                                ws.Range(fields[j, 0] + i + ":" + fields[j, 1] + i).Row(1).Merge();
                            }
                        }

                        ws.Cell("A5").Value = "Well Name";
                        ws.Cell("A5").Style.Font.Bold = true;
                        ws.Cell("C5").Value = detail.Data.well_name;
                        ws.Cell("C5").Style.Font.Bold = true;


                        ws.Cell("A6").Value = "Country";
                        ws.Cell("C6").Value = detail.Data.country_name;

                        ws.Cell("A7").Value = "Field";
                        ws.Cell("C7").Value = detail.Data.field_name;

                        ws.Cell("A8").Value = "Well Clasification";
                        ws.Cell("C8").Value = detail.Data.well_classification;

                        ws.Cell("A9").Value = "Well Type";
                        ws.Cell("C9").Value = detail.Data.well_type;

                        ws.Cell("A10").Value = "Latitude";
                        ws.Cell("C10").Value = detail.Data.latitude;

                        ws.Cell("A11").Value = "Longitude";
                        ws.Cell("C11").Value = detail.Data.longitude;

                        ws.Cell("A12").Value = "Event";
                        ws.Cell("A12").Style.Font.Bold = true;
                        ws.Cell("C12").Value = detail.Data.@event;

                        ws.Cell("F5").Value = "Drilling Contractor";
                        ws.Cell("H5").Value = detail.Data.contractor_name;

                        ws.Cell("F6").Value = "Rig Type";
                        ws.Cell("H6").Value = detail.Data.rig_type;

                        ws.Cell("F7").Value = "Rig Name";
                        ws.Cell("H7").Value = detail.Data.rig_name;

                        ws.Cell("F8").Value = "Rig Rating";
                        ws.Cell("H8").Value = detail.Data.rig_rating + " " + uomMap.Where(x => x.object_name == "Rig Rating").First().uom_code;

                        ws.Cell("F9").Value = "Water Depth";
                        ws.Cell("H9").Value = detail.Data.water_depth + " " + uomMap.Where(x => x.object_name == "Water Depth").First().uom_code;

                        ws.Cell("F10").Value = "RKB Elevation";
                        ws.Cell("H10").Value = detail.Data.rkb_elevation + " " + uomMap.Where(x => x.object_name == "RKB Elevation").First().uom_code;

                        ws.Cell("F11").Value = "RT to Seabed";
                        ws.Cell("H11").Value = detail.Data.rt_to_seabed + " " + uomMap.Where(x => x.object_name == "RT to Seabed").First().uom_code;

                        ws.Cell("F12").Value = "Rig Heading";
                        ws.Cell("H12").Value = detail.Data.rig_heading + " " + uomMap.Where(x => x.object_name == "Rig Heading").First().uom_code;

                        ws.Cell("K5").Value = "AFE Number";
                        ws.Cell("M5").Value = detail.Data.afe_no;

                        ws.Cell("K6").Value = "AFE Cost";
                        ws.Cell("M6").Value = (detail.Data.afe_cost < 1 ? "0" : detail.Data.afe_cost.ToString()) + " " + (currency == null ? "USD" : currency.currency_code);

                        ws.Cell("K7").Value = "Daily Cost";
                        ws.Cell("M7").Value = (detail.Data.daily_cost < 1 ? "0" : detail.Data.daily_cost.ToString()) + " " + (currency == null ? "USD" : currency.currency_code);

                        ws.Cell("K8").Value = "Cummulative Cost";
                        ws.Cell("M8").Value = (detail.Data.cummulative_cost < 1 ? "0" : detail.Data.cummulative_cost.ToString()) + " " + (currency == null ? "USD" : currency.currency_code);

                        ws.Cell("K9").Value = "Daily Mud Cost";
                        ws.Cell("M9").Value = (detail.Data.daily_mud_cost < 1 ? "0" : detail.Data.daily_mud_cost.ToString()) + " " + (currency == null ? "USD" : currency.currency_code);

                        ws.Cell("K10").Value = "Cummulative Mud Cost";
                        ws.Cell("M10").Value = (detail.Data.cummulative_mud_cost < 1 ? "0" : detail.Data.cummulative_mud_cost.ToString()) + " " + (currency == null ? "USD" : currency.currency_code);

                        ws.Cell("K11").Value = "Planned TD";
                        ws.Cell("M11").Value = detail.Data.planned_td + " " + GetUom(uomMap, "Planned TD");

                        ws.Cell("K12").Value = "Planned Days";
                        ws.Cell("M12").Value = detail.Data.planned_days + " Days";

                        ws.Cell("K13").Value = "DOL";
                        ws.Cell("M13").Value = detail.Data.dol + " " + GetUom(uomMap, "DOL");

                        ws.Cell("P5").Value = "Date";
                        ws.Cell("R5").Value = detail.Data.drilling_date;
                        ws.Cell("P5").Style.Font.Bold = true;
                        ws.Cell("R5").Style.Font.Bold = true;

                        ws.Cell("P6").Value = "Spud Date";
                        ws.Cell("R6").Value = detail.Data.spud_date;

                        ws.Cell("P7").Value = "DFS";
                        ws.Cell("R7").Value = detail.Data.dfs + " " + GetUom(uomMap, "DFS");

                        ws.Cell("P8").Value = "Previous Depth MD";
                        ws.Cell("R8").Value = detail.Data.previous_depth_md + " " + GetUom(uomMap, "Previous Depth MD");

                        ws.Cell("P9").Value = "Current Depth MD";
                        ws.Cell("R9").Value = detail.Data.current_depth_md + " " + GetUom(uomMap, "Current Depth MD");

                        ws.Cell("P10").Value = "Previous Depth TVD";
                        ws.Cell("R10").Value = detail.Data.previous_depth_tvd + " " + GetUom(uomMap, "Previous Depth TVD");

                        ws.Cell("P11").Value = "Current Depth TVD";
                        ws.Cell("R11").Value = detail.Data.current_depth_tvd + " " + GetUom(uomMap, "Current Depth TVD");

                        ws.Cell("P12").Value = "Progress";
                        ws.Cell("R12").Value = (detail.Data.current_depth_md - detail.Data.previous_depth_md) + " " + GetUom(uomMap, "Current Depth MD");

                        ws.Cell("A15").Value = "2. Operations Data";
                        ws.Cell("A15").Style.Font.Bold = true;
                        ws.Cell("A15").Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A15:S15").Row(1).Merge();

                        ws.Cell("A16").Value = "Operations for Period 00:00 Hrs to 24:00 Hrs";
                        ws.Cell("A16").Style.Font.Bold = true;
                        ws.Cell("A16").Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        ws.Range("A16:J16").Row(1).Merge();

                        ws.Cell("K16").Value = "Current Operation @06:00 Hrs";
                        ws.Cell("K16").Style.Font.Bold = true;
                        ws.Cell("K16").Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        ws.Range("K16:N16").Row(1).Merge();

                        ws.Cell("O16").Value = "Planned Operations";
                        ws.Cell("O16").Style.Font.Bold = true;
                        ws.Cell("O16").Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        ws.Range("O16:S16").Row(1).Merge();

                        ws.Range("A17:J20").Merge();
                        ws.Range("K17:N20").Merge();
                        ws.Range("O17:S20").Merge();


                        ws.Cell("A17").Value = detail.Data.operation_data_period;
                        ws.Cell("K17").Value = detail.Data.operation_data_early;
                        ws.Cell("O17").Value = detail.Data.operation_data_planned;
                        ws.Cell("A17").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);
                        ws.Cell("A17").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                        ws.Cell("K17").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);
                        ws.Cell("K17").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                        ws.Cell("O17").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Top);
                        ws.Cell("O17").Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                        
                        ws.Range("A21:S21").Row(1).Merge();
                        ws.Cell("A21").Value = "(00:00 Hrs to 24:00 Hrs) " + detail.Data.drilling_date?.ToString("MMM dd, yyyy");
                        ws.Cell("A21").Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        ws.Cell("A21").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        ws.Range("G22:H22").Row(1).Merge();
                        ws.Range("I22:J22").Row(1).Merge();
                        ws.Range("K22:S22").Row(1).Merge();

                        ws.Cell("A22").Value = "Start";
                        ws.Cell("B22").Value = "End";
                        ws.Cell("C22").Value = "Hrs";
                        ws.Cell("D22").Value = "Depth( " + GetUom(uomMap, "Depth") + ")";
                        ws.Cell("E22").Value = "Phase(" + GetUom(uomMap, "Hole Diameter") + ")";
                        ws.Cell("F22").Value = "IADC";
                        ws.Cell("G22").Value = "Category";
                        ws.Cell("I22").Value = "PT/NPT";
                        ws.Cell("K22").Value = "Details";

                        var index = 23;
                        bool isFirstArray = true;
                        var drillingActivityVal = drillingActivityResult.Data.First();

                        foreach (var drillingActivity in drillingActivityResult.Data)
                        {
                            if (!isFirstArray)
                            {
                                // Menambahkan format pembatas untuk setiap array kecuali yang pertama
                                ws.Range("A" + index + ":S" + index).Row(1).Merge();
                                ws.Cell("A" + index).Value = "(00:00 Hrs to 06:00 Hrs) " + detail.Data.drilling_date?.ToString("MMM dd, yyyy");
                                ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                                ws.Cell("A" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                                ws.Cell("A" + index).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                                // Tambahkan baris untuk data berikutnya
                                index++;
                            }

                            var drillingActivityDetail = drillingActivity.DrillingOperationActivities;

                            for (int i = 0; i < drillingActivityDetail.Count; i++)
                            {
                                // Menambahkan data ke sel
                                ws.Cell("A" + index).Value = drillingActivityDetail[i].operation_start_date?.ToString("HH:mm");
                                ws.Cell("B" + index).Value = drillingActivityDetail[i].operation_end_date?.ToString("HH:mm");
                                ws.Cell("C" + index).SetValue<string>(drillingActivityDetail[i].Hours.ToString());
                                ws.Cell("D" + index).Value = drillingActivityDetail[i].depth;
                                ws.Cell("E" + index).Value = drillingActivity.hole_name;
                                ws.Cell("F" + index).Value = drillingActivityDetail[i].iadc_code + " - " + drillingActivityDetail[i].iadc_description;
                                ws.Cell("G" + index).Value = drillingActivityDetail[i].category;
                                ws.Range("G" + index + ":H" + index).Row(1).Merge();
                                ws.Cell("I" + index).Value = drillingActivityDetail[i].type_desc;
                                ws.Range("I" + index + ":J" + index).Row(1).Merge();
                                ws.Cell("K" + index).Value = drillingActivityDetail[i].description;
                                ws.Cell("K" + index).Style.Alignment.WrapText = true;
                                ws.Range("K" + index + ":S" + index).Row(1).Merge();

                                // Increment index untuk baris berikutnya
                                index++;
                            }
                            isFirstArray = false;
                        }

                        index--;
                        ws.Range("A16:S" + index).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A16:S" + index).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A16:S" + index).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A16:S" + index).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A22:K" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        index += 2;
                        ws.Cell("A" + index).Value = "3. HSSE";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        var cellIndex = index;
                        ws.Cell("A" + cellIndex).Value = "Incident / Acident";
                        ws.Cell("C" + cellIndex).Value = detail.Data.hsse_incident_acident;

                        cellIndex = index + 1;
                        ws.Cell("A" + cellIndex).Value = "Environtmental Spills";
                        ws.Cell("C" + cellIndex).Value = detail.Data.hsse_environtmental_spills;

                        cellIndex = index + 2;
                        ws.Cell("A" + cellIndex).Value = "Safety Alert Received";
                        ws.Cell("C" + cellIndex).Value = detail.Data.hsse_safety_alert_received;

                        cellIndex = index + 3;
                        ws.Cell("A" + cellIndex).Value = "Proactive Safety";
                        ws.Cell("C" + cellIndex).Value = detail.Data.hsse_proactive_safety;

                        cellIndex = index + 4;
                        ws.Cell("A" + cellIndex).Value = "Near Miss Report";
                        ws.Cell("C" + cellIndex).Value = detail.Data.hsse_near_miss_report;

                        cellIndex = index + 5;
                        ws.Cell("A" + cellIndex).Value = "Exercise";
                        ws.Cell("C" + cellIndex).Value = detail.Data.hsse_exercise;

                        cellIndex = index + 6;
                        ws.Cell("A" + cellIndex).Value = "Social Issues";
                        ws.Cell("C" + cellIndex).Value = detail.Data.hsse_social_issues;

                        cellIndex = index;
                        ws.Cell("F" + cellIndex).Value = "Safety Meeting";
                        ws.Cell("H" + cellIndex).Value = detail.Data.hsse_safety_meeting;

                        cellIndex = index + 1;
                        ws.Cell("F" + cellIndex).Value = "STOP Cards";
                        ws.Cell("H" + cellIndex).Value = detail.Data.hsse_stop_cards;

                        cellIndex = index + 2;
                        ws.Cell("F" + cellIndex).Value = "TOFS";
                        ws.Cell("H" + cellIndex).Value = detail.Data.hsse_tofs;

                        cellIndex = index + 3;
                        ws.Cell("F" + cellIndex).Value = "JSA";
                        ws.Cell("H" + cellIndex).Value = detail.Data.hsse_jsa;

                        cellIndex = index + 4;
                        ws.Cell("F" + cellIndex).Value = "Inductions";
                        ws.Cell("H" + cellIndex).Value = detail.Data.hsse_inductions;

                        cellIndex = index + 5;
                        ws.Cell("F" + cellIndex).Value = "Audits";
                        ws.Cell("H" + cellIndex).Value = detail.Data.hsse_audits;

                        cellIndex = index + 6;
                        ws.Cell("F" + cellIndex).Value = "Days Since LTI";
                        ws.Cell("H" + cellIndex).Value = detail.Data.hsse_days_since_lti;

                        cellIndex = index;
                        ws.Cell("K" + cellIndex).Value = "T - BOP Press";
                        ws.Cell("M" + cellIndex).Value = detail.Data.hsse_tbop_press;

                        cellIndex = index + 1;
                        ws.Cell("K" + cellIndex).Value = "T - BOP Func";
                        ws.Cell("M" + cellIndex).Value = detail.Data.hsse_tbop_func;

                        cellIndex = index + 2;
                        ws.Cell("K" + cellIndex).Value = "D - Kick";
                        ws.Cell("M" + cellIndex).Value = detail.Data.hsse_dkick;

                        cellIndex = index + 3;
                        ws.Cell("K" + cellIndex).Value = "D - Strip";
                        ws.Cell("M" + cellIndex).Value = detail.Data.hsse_dstrip;

                        cellIndex = index + 4;
                        ws.Cell("K" + cellIndex).Value = "D - Fire";
                        ws.Cell("M" + cellIndex).Value = detail.Data.hsse_dfire;

                        cellIndex = index + 5;
                        ws.Cell("K" + cellIndex).Value = "D - Mis.Pers.";
                        ws.Cell("M" + cellIndex).Value = detail.Data.hsse_dmis_pers;

                        cellIndex = index + 6;
                        ws.Cell("K" + cellIndex).Value = "D - Aband.Rig";
                        ws.Cell("M" + cellIndex).Value = detail.Data.hsse_aband_rig;

                        cellIndex = index + 7;
                        ws.Cell("K" + cellIndex).Value = "D - H2S";
                        ws.Cell("M" + cellIndex).Value = detail.Data.hsse_dh2s;

                        var cellStart = index;
                        var cellEnd = index + 7;

                        ws.Cell("P" + cellStart).Value = detail.Data.hsse_description;
                        ws.Range("P" + cellStart + ":S" + cellEnd).Merge();

                        fields = new string[,]
                        {
                            {"A", "B" },
                            {"C", "D"},
                            {"F", "G" },
                            {"H", "I" },
                            {"K", "L" },
                            {"M", "N" },
                        };
                        for (int j = 0; j < fields.GetLength(0); j++)
                        {
                            for (int i = index; i < index + 8; i++)
                            {
                                ws.Range(fields[j, 0] + i + ":" + fields[j, 1] + i).Row(1).Merge();
                            }
                        }

                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        index += 9;
                        ws.Cell("A" + index).Value = "4.Hole & Casing Data";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;
                     
                            ws.Cell("A" + index).Value = drillingActivityVal.hole_name + " " + GetUom(uomMap, "Hole Diameter") + " Open Hole TD";
                            ws.Range("A" + index + ":B" + index).Row(1).Merge();
                            ws.Cell("C" + index).Value = drillingActivityVal.hole_depth + " " + GetUom(uomMap, "Depth");
                            ws.Range("C" + index + ":D" + index).Row(1).Merge();
                            ws.Cell("F" + index).Value = "Casing " + drillingActivityVal.casing_name + " " + GetUom(uomMap, "Casing Diameter");
                            ws.Range("F" + index + ":G" + index).Row(1).Merge();
                            ws.Cell("H" + index).Value = drillingActivityVal.casing_val + " " + GetUom(uomMap, "Depth");
                            ws.Range("H" + index + ":I" + index).Row(1).Merge();
                            ws.Cell("K" + index).Value = drillingActivityVal.lot_fit == 1 ? "LOT" : "FIT";
                            ws.Cell("L" + index).Value = drillingActivityVal.lot_fit_val + " kg/m3 EMW";
                            ws.Range("L" + index + ":N" + index).Row(1).Merge();
                            ws.Cell("P" + index).Value = "Top of Cement";
                            ws.Range("P" + index + ":Q" + index).Row(1).Merge();
                            ws.Cell("R" + index).Value = drillingActivityVal.cement_val + " " + GetUom(uomMap, "Top of Cement");
                            ws.Range("R" + index + ":S" + index).Row(1).Merge();
                        
                        ws.Range("A" + index + ":S" + index).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":S" + index).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":S" + index).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":S" + index).Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        index += 2;
                        ws.Cell("A" + index).Value = "5.Bit & BHA Data";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        ws.Cell("A" + index).Value = "BHA No " + (drillingBhaBit == null ? "" : drillingBhaBit.bha_no.ToString());
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        cellIndex = index;
                        ws.Cell("A" + cellIndex).Value = "BHA Weight (air)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.weight_air.ToString()) + " " + GetUom(uomMap, "Weight");
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "String Weight";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.string_weight.ToString()) + " " + GetUom(uomMap, "Weight");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        cellIndex = index + 1;
                        ws.Cell("A" + cellIndex).Value = "BHA Weight (mud)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.weight_mud.ToString()) + " " + GetUom(uomMap, "Weight");
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Weight Pick Up";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.weight_pick_up.ToString()) + " " + GetUom(uomMap, "Weight");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        cellIndex = index + 2;
                        ws.Cell("A" + cellIndex).Value = "WT. Below jars (air)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.wt_below_jars_air.ToString()) + " " + GetUom(uomMap, "Weight");
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Weight Slack Down";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.weight_slack_down.ToString()) + " " + GetUom(uomMap, "Weight");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        cellIndex = index + 3;
                        ws.Cell("A" + cellIndex).Value = "WT. Below jars (mud)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.wt_below_jars_mud.ToString()) + " " + GetUom(uomMap, "Weight");
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Weight Rotate";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.weight_rotate.ToString()) + " " + GetUom(uomMap, "Weight");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();

                        index += 5;

                        ws.Range("A" + index + ":S" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFF2F2F2");
                        ws.Range("A" + index + ":S" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center); ;
                        ws.Cell("A" + index).Value = "BHA Comp.";
                        ws.Range("A" + index + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + index).Value = "Component Detail";
                        ws.Range("C" + index + ":F" + index).Row(1).Merge();
                        ws.Cell("G" + index).Value = "Joints";
                        ws.Cell("H" + index).Value = "ID (" + GetUom(uomMap, "Length") + ")";
                        ws.Cell("I" + index).Value = "OD (" + GetUom(uomMap, "Length") + ")";
                        ws.Cell("J" + index).Value = "Weight (" + GetUom(uomMap, "Weight") + ")";
                        ws.Cell("K" + index).Value = "Length (" + GetUom(uomMap, "Length") + ")";
                        ws.Cell("L" + index).Value = "Cum. Length (" + GetUom(uomMap, "Cummulative Length") + ")";
                        ws.Cell("M" + index).Value = "Con. Type";
                        ws.Range("M" + index + ":N" + index).Row(1).Merge();
                        ws.Cell("P" + index).Value = "Con. Size (" + GetUom(uomMap, "Length") + ")";
                        ws.Range("P" + index + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + index).Value = "Serial Number";
                        ws.Range("R" + index + ":S" + index).Row(1).Merge();
                        cellStart = index;
                        cellEnd = index;
                        index++;

                        foreach (var bha in wellBhaComponent)
                        {
                            ws.Cell("A" + index).Value = bha.component_name;
                            ws.Range("A" + index + ":B" + index).Row(1).Merge();
                            ws.Cell("C" + index).Value = bha.component_detail;
                            ws.Range("C" + index + ":F" + index).Row(1).Merge();
                            ws.Cell("G" + index).Value = bha.joint;
                            ws.Cell("H" + index).Value = bha.n_id;
                            ws.Cell("I" + index).Value = bha.n_od;
                            ws.Cell("J" + index).Value = bha.weight;
                            ws.Cell("K" + index).Value = bha.length;
                            ws.Cell("L" + index).Value = bha.cumm_length;
                            ws.Cell("M" + index).Value = bha.con_type;
                            ws.Range("M" + index + ":N" + index).Row(1).Merge();
                            ws.Cell("P" + index).Value = bha.con_size;
                            ws.Range("P" + index + ":Q" + index).Row(1).Merge();
                            ws.Cell("R" + index).Value = bha.serial_number;
                            ws.Range("R" + index + ":S" + index).Row(1).Merge();
                            cellEnd = index;
                            index++;
                        }
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index++;

                        ws.Cell("A" + index).Value = "BIT No " + (drillingBhaBit == null ? "" : drillingBhaBit.bit_number?.ToString());
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        cellIndex = index;
                        ws.Cell("A" + cellIndex).Value = "Bit Size (" + GetUom(uomMap, "Bit Size") + ")";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = wellBit.bit_size;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Serial Number";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = wellBit.serial_number;
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "Dull Grading";
                        ws.Cell("K" + index).Style.Font.Bold = true;
                        ws.Range("K" + index + ":N" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center); ;
                        ws.Range("K" + cellIndex + ":N" + index).Row(1).Merge();
                        ws.Cell("P" + cellIndex).Value = "Depth In";
                        ws.Range("P" + cellIndex + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.depth_in?.ToString()) + " " + GetUom(uomMap, "Depth In");
                        ws.Range("R" + cellIndex + ":S" + index).Row(1).Merge();

                        cellIndex = index + 1;
                        ws.Cell("A" + cellIndex).Value = "Bit Run";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = wellBit.bit_run;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "IADC";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = wellBit.iadc_code;
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "In Rows";
                        ws.Cell("L" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_in_rows?.ToString());
                        ws.Cell("M" + cellIndex).Value = "Seals";
                        ws.Cell("N" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_seals?.ToString());
                        ws.Cell("P" + cellIndex).Value = "Depth Out";
                        ws.Range("P" + cellIndex + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.depth_out?.ToString()) + " " + GetUom(uomMap, "Depth Out");
                        ws.Range("R" + cellIndex + ":S" + index).Row(1).Merge();

                        cellIndex = index + 2;
                        ws.Cell("A" + cellIndex).Value = "Manufacture";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = wellBit.manufacturer;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Nozzle";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = wellBit.noozle_1 + " " + GetUom(uomMap, "Nozzle Size");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "Out Rows";
                        ws.Cell("L" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_out_rows?.ToString());
                        ws.Cell("M" + cellIndex).Value = "Gauge";
                        ws.Cell("N" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_gauge?.ToString());
                        ws.Cell("P" + cellIndex).Value = "Hours on Bottom";
                        ws.Range("P" + cellIndex + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.duration?.ToString()) + " " + GetUom(uomMap, "Hours on Bottom");
                        ws.Range("R" + cellIndex + ":S" + index).Row(1).Merge();

                        cellIndex = index + 3;
                        ws.Cell("A" + cellIndex).Value = "Bit Type";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = wellBit.bit_type;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Nozzle";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = wellBit.noozle_2 + " " + GetUom(uomMap, "Nozzle Size");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "Dull Char";
                        ws.Cell("L" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_dull_char?.ToString());
                        ws.Cell("M" + cellIndex).Value = "Other char";
                        ws.Cell("N" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_other_char?.ToString());
                        ws.Cell("P" + cellIndex).Value = "Footage";
                        ws.Range("P" + cellIndex + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_footage?.ToString()) + " " + GetUom(uomMap, "Footage");
                        ws.Range("R" + cellIndex + ":S" + index).Row(1).Merge();

                        cellIndex = index + 4;
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Nozzle";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = wellBit.noozle_3 + " " + GetUom(uomMap, "Nozzle Size");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "Loc Cone";
                        ws.Cell("L" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_loc_cone?.ToString());
                        ws.Cell("M" + cellIndex).Value = "Reason Pulled";
                        ws.Cell("N" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_reason_pulled?.ToString());
                        ws.Cell("P" + cellIndex).Value = "ROP Overall";
                        ws.Range("P" + cellIndex + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + cellIndex).Value = (drillingBhaBit == null ? "" : drillingBhaBit.dg_rop_overall?.ToString()) + " " + GetUom(uomMap, "ROP Overall");
                        ws.Range("R" + cellIndex + ":S" + index).Row(1).Merge();

                        cellIndex = index + 5;
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Nozzle";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = wellBit.noozle_4 + " " + GetUom(uomMap, "Nozzle Size");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "";
                        ws.Cell("L" + cellIndex).Value = "";
                        ws.Cell("M" + cellIndex).Value = "";
                        ws.Cell("N" + cellIndex).Value = "";
                        ws.Cell("P" + cellIndex).Value = "";
                        ws.Range("P" + cellIndex + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + cellIndex).Value = "";
                        ws.Range("R" + cellIndex + ":S" + index).Row(1).Merge();

                        cellIndex = index + 6;
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Nozzle";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = wellBit.noozle_5 + " " + GetUom(uomMap, "Nozzle Size");
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "";
                        ws.Cell("L" + cellIndex).Value = "";
                        ws.Cell("M" + cellIndex).Value = "";
                        ws.Cell("N" + cellIndex).Value = "";
                        ws.Cell("P" + cellIndex).Value = "";
                        ws.Range("P" + cellIndex + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + cellIndex).Value = "";
                        ws.Range("R" + cellIndex + ":S" + index).Row(1).Merge();

                        cellIndex = index + 7;
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "TFA";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = wellBit.TFA;
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "";
                        ws.Cell("L" + cellIndex).Value = "";
                        ws.Cell("M" + cellIndex).Value = "";
                        ws.Cell("N" + cellIndex).Value = "";
                        ws.Cell("P" + cellIndex).Value = "";
                        ws.Range("P" + cellIndex + ":Q" + index).Row(1).Merge();
                        ws.Cell("R" + cellIndex).Value = "";
                        ws.Range("R" + cellIndex + ":S" + index).Row(1).Merge();

                        ws.Range("A" + index + ":S" + cellIndex).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":S" + cellIndex).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":S" + cellIndex).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":S" + cellIndex).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index += 9;

                        ws.Cell("A" + index).Value = "6. Mudlogging Data";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        ws.Cell("A" + index).Value = "Parameter";
                        ws.Cell("B" + index).Value = "Min";
                        ws.Cell("C" + index).Value = "Max";
                        ws.Cell("D" + index).Value = "Unit";
                        ws.Range("A" + index + ":D" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        cellIndex = index + 2;
                        ws.Range("A" + index + ":D" + cellIndex).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":D" + cellIndex).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":D" + cellIndex).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":D" + cellIndex).Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        ws.Cell("F" + index).Value = "Parameter";
                        ws.Cell("G" + index).Value = "Min";
                        ws.Cell("H" + index).Value = "Max";
                        ws.Cell("I" + index).Value = "Unit";
                        ws.Range("F" + index + ":I" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        ws.Range("F" + index + ":I" + cellIndex).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("F" + index + ":I" + cellIndex).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("F" + index + ":I" + cellIndex).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("F" + index + ":I" + cellIndex).Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        ws.Cell("K" + index).Value = "Parameter";
                        ws.Cell("L" + index).Value = "Min";
                        ws.Cell("M" + index).Value = "Max";
                        ws.Cell("N" + index).Value = "Unit";
                        ws.Range("K" + index + ":N" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        ws.Range("K" + index + ":N" + cellIndex).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("K" + index + ":N" + cellIndex).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("K" + index + ":N" + cellIndex).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("K" + index + ":N" + cellIndex).Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        ws.Cell("P" + index).Value = "Parameter";
                        ws.Cell("Q" + index).Value = "Min";
                        ws.Cell("R" + index).Value = "Max";
                        ws.Cell("S" + index).Value = "Unit";
                        ws.Range("P" + index + ":S" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        cellIndex = index + 3;
                        ws.Range("P" + index + ":S" + cellIndex).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("P" + index + ":S" + cellIndex).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("P" + index + ":S" + cellIndex).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("P" + index + ":S" + cellIndex).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index++;

                        ws.Cell("A" + index).Value = "WOB";
                        ws.Cell("B" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_wob_min.ToString());
                        ws.Cell("C" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_wob_max.ToString());
                        ws.Cell("D" + index).Value = "klbs";
                        ws.Cell("F" + index).Value = "DHRPM";
                        ws.Cell("G" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_dhrpm_min.ToString());
                        ws.Cell("H" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_dhrpm_max.ToString());
                        ws.Cell("I" + index).Value = "RPM";
                        ws.Cell("K" + index).Value = "Flowrate";
                        ws.Cell("L" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_flowrate_min.ToString());
                        ws.Cell("M" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_flowrate_max.ToString());
                        ws.Cell("N" + index).Value = "gpm";
                        ws.Cell("P" + index).Value = "SPM";
                        ws.Cell("Q" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_spm_min.ToString());
                        ws.Cell("R" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_spm_max.ToString());
                        ws.Cell("S" + index).Value = "Stks";
                        index++;

                        ws.Cell("A" + index).Value = "RPM";
                        ws.Cell("B" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_rpm_min.ToString());
                        ws.Cell("C" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_rpm_max.ToString());
                        ws.Cell("D" + index).Value = "RRPM";
                        ws.Cell("F" + index).Value = "Torque";
                        ws.Cell("G" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_torque_min.ToString());
                        ws.Cell("H" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_torque_max.ToString());
                        ws.Cell("I" + index).Value = "K lb.ft";
                        ws.Cell("K" + index).Value = "SPP";
                        ws.Cell("L" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_spp_min.ToString());
                        ws.Cell("M" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_spp_max.ToString());
                        ws.Cell("N" + index).Value = "psi";
                        ws.Cell("P" + index).Value = "SPM PRESS";
                        ws.Cell("Q" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_spmpress_min.ToString());
                        ws.Cell("R" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_spmpress_max.ToString());
                        ws.Cell("S" + index).Value = "psi";
                        index++;

                        ws.Cell("P" + index).Value = "AVG ROP";
                        ws.Cell("Q" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_avg_rop_min.ToString());
                        ws.Cell("R" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_avg_rop_max.ToString());
                        ws.Cell("S" + index).Value = "ft/Hrs";
                        index += 2;

                        ws.Cell("A" + index).Value = "Parameter";
                        ws.Cell("B" + index).Value = "Value";
                        ws.Range("B" + index + ":C" + index).Row(1).Merge();
                        ws.Cell("D" + index).Value = "Unit";
                        ws.Range("A" + index + ":D" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        cellIndex = index + 1;
                        ws.Range("A" + index + ":D" + cellIndex).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":D" + cellIndex).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":D" + cellIndex).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":D" + cellIndex).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index++;

                        ws.Cell("A" + index).Value = "HKLD";
                        ws.Cell("B" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.mudlogging_hkld.ToString());
                        ws.Range("B" + index + ":C" + index).Row(1).Merge();
                        ws.Cell("D" + index).Value = GetUom(uomMap, "HKLD");
                        index += 2;

                        ws.Cell("A" + index).Value = "7. Transport/ Shipping Data";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        cellIndex = index + 1;
                        cellStart = index - 1;
                        cellEnd = cellIndex;

                        ws.Cell("A" + index).Value = "Name";
                        ws.Range("A" + index + ":B" + cellIndex).Merge();
                        ws.Cell("C" + index).Value = "Type";
                        ws.Range("C" + index + ":E" + cellIndex).Merge();
                        ws.Cell("F" + index).Value = "Departed";
                        ws.Range("F" + index + ":G" + index).Merge();
                        ws.Cell("I" + index).Value = "ETA";
                        ws.Range("I" + index + ":J" + index).Merge();
                        ws.Cell("K" + index).Value = "Days";
                        ws.Range("K" + index + ":L" + cellIndex).Merge();
                        ws.Cell("M" + index).Value = "Comments";
                        ws.Range("M" + index + ":S" + cellIndex).Merge();
                        ws.Cell("F" + cellIndex).Value = "Date";
                        ws.Cell("G" + cellIndex).Value = "Time";
                        ws.Cell("I" + cellIndex).Value = "Date";
                        ws.Cell("J" + cellIndex).Value = "Time";

                        ws.Range("A" + index + ":S" + cellIndex).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center); ;
                        ws.Range("A" + index + ":S" + cellIndex).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        index += 2;
                        foreach (var transport in drillingTransport)
                        {
                            ws.Cell("A" + index).Value = transport.name;
                            ws.Range("A" + index + ":B" + index).Merge();
                            ws.Cell("C" + index).Value = transport.type;
                            ws.Range("C" + index + ":E" + index).Merge();
                            ws.Cell("F" + index).Value = transport.departed_date?.ToString("MM/dd/yyyy");
                            ws.Cell("G" + index).Value = transport.departed_date?.ToString("HH:mm");
                            ws.Cell("I" + index).Value = transport.eta_date?.ToString("MM/dd/yyyy");
                            ws.Cell("J" + index).Value = transport.eta_date?.ToString("HH:mm");
                            ws.Cell("K" + index).Value = (transport.eta_date - transport.departed_date)?.TotalDays;
                            ws.Range("K" + index + ":L" + index).Merge();
                            ws.Cell("M" + index).Value = transport.comment;
                            ws.Range("M" + index + ":S" + index).Merge();
                            cellEnd = index;
                            index++;
                        }
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index++;

                        ws.Cell("A" + index).Value = "8. Bulk Data";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        cellStart = index;
                        ws.Cell("A" + index).Value = "Item";
                        ws.Cell("B" + index).Value = "Previous";
                        ws.Cell("C" + index).Value = "Consumed";
                        ws.Cell("D" + index).Value = "Received";
                        ws.Cell("E" + index).Value = "Balance";
                        index++;

                        foreach (var bulk in drillingBulk.Data)
                        {
                            cellEnd = index;
                            ws.Cell("A" + index).Value = bulk.item_name;
                            ws.Cell("B" + index).Value = bulk.previous;
                            ws.Cell("C" + index).Value = bulk.consumed;
                            ws.Cell("D" + index).Value = bulk.received;
                            ws.Cell("E" + index).Value = bulk.balance;
                            index++;
                        }
                        index++;

                        ws.Range("A" + cellStart + ":E" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":E" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":E" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":E" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        ws.Cell("A" + index).Value = "9.Aerated Drilling";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        ws.Cell("A" + index).Value = "Parameter";
                        ws.Cell("B" + index).Value = "Min";
                        ws.Cell("C" + index).Value = "Max";
                        ws.Cell("D" + index).Value = "Unit";
                        ws.Range("A" + index + ":D" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        cellStart = index;
                        cellEnd = index;
                        index++;

                        if (aerated != null)
                        {
                            ws.Cell("A" + index).Value = "Gas Flow Rate";
                            ws.Cell("B" + index).Value = aerated.aerated_min;
                            ws.Cell("C" + index).Value = aerated.aerated_max;
                            ws.Cell("D" + index).Value = "scmm";
                            cellEnd = index;
                            index++;
                        }
                        ws.Range("A" + cellStart + ":D" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":D" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":D" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":D" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index++;

                        ws.Cell("A" + index).Value = "10. Actual Formation";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        ws.Cell("A" + index).Value = "No";
                        ws.Cell("B" + index).Value = "Depth(m)";
                        ws.Cell("C" + index).Value = "Formation";
                        ws.Cell("D" + index).Value = "Stone";
                        ws.Cell("E" + index).Value = "%";
                        cellStart = index;
                        index++;

                        if (drillActual.Count() > 0)
                        {
                            var i = 1;
                            foreach (var actual in drillActual)
                            {
                                cellIndex = index;
                                cellEnd = index;
                                ws.Cell("A" + index).Value = i;
                                ws.Cell("B" + index).Value = actual.depth;
                                ws.Cell("C" + index).Value = actual.formation_data;
                                List<drill_formations_actual_detail> detailActuals = drillFormationActualDetailServices.GetAll(" AND drill_formation_actual_id=@0 ORDER BY seq ASC ", actual.id);
                                foreach (var detailActual in detailActuals)
                                {
                                    ws.Cell("D" + index).Value = stones.Items.Where(x => x.id == detailActual.stone_id).First().name;
                                    ws.Cell("E" + index).Value = detailActual.percentage + "%";
                                    cellEnd = index;
                                    index++;
                                }
                                ws.Range("A" + cellIndex + ":A" + cellEnd).Merge();
                                ws.Range("B" + cellIndex + ":B" + cellEnd).Merge();
                                ws.Range("C" + cellIndex + ":C" + cellEnd).Merge();
                                ws.Range("A" + cellIndex + ":E" + cellEnd).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center); ;
                                i++;
                                index++;
                            }
                        }
                        ws.Range("A" + cellStart + ":E" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":E" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":E" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":E" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index++;

                        ws.Cell("A" + index).Value = "11. Deviation Data";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;
                        cellStart = index;
                        cellEnd = index;
                        ws.Cell("A" + index).Value = "Measured Depth(" + GetUom(uomMap, "Measured Depth ") + ")";
                        ws.Cell("B" + index).Value = "Inclination(" + GetUom(uomMap, "Inclination") + ")";
                        ws.Cell("C" + index).Value = "Azimuth(" + GetUom(uomMap, "Azimuth") + ")";
                        ws.Cell("D" + index).Value = "TVD(" + GetUom(uomMap, "TVD") + ")";
                        ws.Cell("E" + index).Value = "V Section(" + GetUom(uomMap, "V Section") + ")";
                        ws.Cell("F" + index).Value = "N / S(" + GetUom(uomMap, "N/S") + ")";
                        ws.Cell("G" + index).Value = "E / W(" + GetUom(uomMap, "E/W") + ")";
                        ws.Cell("H" + index).Value = "DLS(" + GetUom(uomMap, "DLS") + ")";
                        ws.Cell("I" + index).Value = "Survey Tool";
                        index++;

                        foreach (var deviation in deviations)
                        {
                            ws.Cell("A" + index).Value = deviation.measured_depth;
                            ws.Cell("B" + index).Value = deviation.inclination;
                            ws.Cell("C" + index).Value = deviation.azimuth;
                            ws.Cell("D" + index).Value = deviation.tvd;
                            ws.Cell("E" + index).Value = deviation.v_section;
                            ws.Cell("F" + index).Value = deviation.n_s;
                            ws.Cell("G" + index).Value = deviation.e_w;
                            ws.Cell("H" + index).Value = deviation.dls;
                            ws.Cell("I" + index).Value = deviation.survey_tool;
                            cellEnd = index;
                            index++;
                        }
                        ws.Range("A" + cellStart + ":I" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":I" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":I" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":I" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index++;

                        ws.Cell("A" + index).Value = "12. Mud Data";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;
                        cellStart = index;
                        cellEnd = index + 14;

                        ws.Range("A" + cellStart + ":N" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":N" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":N" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":N" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        ws.Cell("a" + index).Value = "Mud Type";
                        ws.Cell("b" + index).Value = "";
                        ws.Cell("c" + index).Value = (mud1 == null ? "" : mud1.mud_name);
                        ws.Cell("d" + index).Value = (mud2 == null ? "" : mud2.mud_name);
                        ws.Cell("e" + index).Value = (mud3 == null ? "" : mud3.mud_name);
                        ws.Cell("f" + index).Value = "Mud Type";
                        ws.Cell("g" + index).Value = "";
                        ws.Cell("h" + index).Value = (mud1 == null ? "" : mud1.mud_name);
                        ws.Cell("i" + index).Value = (mud2 == null ? "" : mud2.mud_name);
                        ws.Cell("j" + index).Value = (mud3 == null ? "" : mud3.mud_name);
                        ws.Cell("k" + index).Value = "Hole & Casing";
                        ws.Range("k" + index + ":l" + index).Row(1).Merge();
                        ws.Cell("m" + index).Value = drillingActivityVal.hole_name + " " + GetUom(uomMap, "Hole Diameter");
                        ws.Cell("n" + index).Value = drillingActivityVal.casing_name + " " + GetUom(uomMap, "Casing Diameter");
                        ws.Cell("p" + index).Value = "Chemical Used";
                        ws.Range("p" + index + ":s" + index).Row(1).Merge();

                        cellIndex = index + 1;
                        ws.Cell("a" + cellIndex).Value = "Time";
                        ws.Cell("b" + cellIndex).Value = "HH:MM";
                        ws.Cell("c" + cellIndex).SetValue<string>((mud1 == null ? "" : mud1.mud_time?.ToString("HH:mm")));
                        ws.Cell("d" + cellIndex).SetValue<string>((mud2 == null ? "" : mud2.mud_time?.ToString("HH:mm")));
                        ws.Cell("e" + cellIndex).SetValue<string>((mud3 == null ? "" : mud3.mud_time?.ToString("HH:mm")));
                        ws.Cell("f" + cellIndex).Value = "Time";
                        ws.Cell("g" + cellIndex).Value = "HH:MM";
                        ws.Cell("h" + cellIndex).SetValue<string>((mud1 == null ? "" : mud1.mud_time?.ToString("HH:mm")));
                        ws.Cell("i" + cellIndex).SetValue<string>((mud2 == null ? "" : mud2.mud_time?.ToString("HH:mm")));
                        ws.Cell("j" + cellIndex).SetValue<string>((mud3 == null ? "" : mud3.mud_time?.ToString("HH:mm")));
                        ws.Cell("k" + cellIndex).Value = "Total Vol Start";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (volumeDrilling != null ? volumeDrilling.total_volume_start : 0);
                        ws.Cell("n" + cellIndex).Value = GetUom(uomMap, "Plan Volume");
                        ws.Cell("p" + cellIndex).Value = "Type";
                        ws.Range("p" + cellIndex + ":q" + cellIndex).Row(1).Merge();
                        ws.Cell("r" + cellIndex).Value = "Amount";
                        ws.Cell("s" + cellIndex).Value = "Unit";

                        cellIndex = index + 2;
                        ws.Cell("a" + cellIndex).Value = "MW In";
                        ws.Cell("b" + cellIndex).Value = GetUom(uomMap, "MW In");
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.mw_in);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.mw_in);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.mw_in);
                        ws.Cell("f" + cellIndex).Value = "Solids";
                        ws.Cell("g" + cellIndex).Value = "%";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.solids);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.solids);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.solids);
                        ws.Cell("k" + cellIndex).Value = "Mud Cost";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (volumeDrilling != null ? volumeDrilling.mud_cost : 0);
                        ws.Cell("n" + cellIndex).Value = "usd";

                        cellIndex = index + 3;
                        ws.Cell("a" + cellIndex).Value = "MW Out";
                        ws.Cell("b" + cellIndex).Value = GetUom(uomMap, "MW Out");
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.mw_out);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.mw_out);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.mw_out);
                        ws.Cell("f" + cellIndex).Value = "Sand";
                        ws.Cell("g" + cellIndex).Value = "%";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.sand);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.sand);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.sand);
                        ws.Cell("k" + cellIndex).Value = "Lost Surface";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (volumeDrilling != null ? volumeDrilling.lost_surface : 0);
                        ws.Cell("n" + cellIndex).Value = GetUom(uomMap, "Plan Volume");

                        cellIndex = index + 4;
                        ws.Cell("a" + cellIndex).Value = "Temp In";
                        ws.Cell("b" + cellIndex).Value = GetUom(uomMap, "Temperature");
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.temp_in);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.temp_in);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.temp_in);
                        ws.Cell("f" + cellIndex).Value = "Water";
                        ws.Cell("g" + cellIndex).Value = "%";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.water);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.water);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.water);
                        ws.Cell("k" + cellIndex).Value = "Lost Downhole";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (volumeDrilling != null ? volumeDrilling.lost_downhole : 0);
                        ws.Cell("n" + cellIndex).Value = GetUom(uomMap, "Plan Volume");

                        cellIndex = index + 5;
                        ws.Cell("a" + cellIndex).Value = "Temp Out";
                        ws.Cell("b" + cellIndex).Value = GetUom(uomMap, "Temperature");
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.temp_out);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.temp_out);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.temp_out);
                        ws.Cell("f" + cellIndex).Value = "Oil";
                        ws.Cell("g" + cellIndex).Value = "%";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.oil);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.oil);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.oil);
                        ws.Cell("k" + cellIndex).Value = "Dumped";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (volumeDrilling != null ? volumeDrilling.dumped : 0);
                        ws.Cell("n" + cellIndex).Value = GetUom(uomMap, "Plan Volume");

                        cellIndex = index + 6;
                        ws.Cell("a" + cellIndex).Value = "Pres. Grad";
                        ws.Cell("b" + cellIndex).Value = GetUom(uomMap, "PRESSURE GRADIENT");
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.pres_grad);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.pres_grad);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.pres_grad);
                        ws.Cell("f" + cellIndex).Value = "HGS";
                        ws.Cell("g" + cellIndex).Value = "%";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.hgs);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.hgs);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.hgs);
                        ws.Cell("k" + cellIndex).Value = "Build";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (volumeDrilling != null ? volumeDrilling.build : 0);
                        ws.Cell("n" + cellIndex).Value = GetUom(uomMap, "Plan Volume");

                        cellIndex = index + 7;
                        ws.Cell("a" + cellIndex).Value = "Visc";
                        ws.Cell("b" + cellIndex).Value = GetUom(uomMap, "VISCOSITY");
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.visc);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.visc);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.visc);
                        ws.Cell("f" + cellIndex).Value = "LGS";
                        ws.Cell("g" + cellIndex).Value = "%";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.lgs);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.lgs);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.lgs);
                        ws.Cell("k" + cellIndex).Value = "Ending";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (volumeDrilling != null ? volumeDrilling.ending : 0);
                        ws.Cell("n" + cellIndex).Value = GetUom(uomMap, "Plan Volume");

                        cellIndex = index + 8;
                        ws.Cell("a" + cellIndex).Value = "PV";
                        ws.Cell("b" + cellIndex).Value = "mpa*s";
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.pv);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.pv);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.pv);
                        ws.Cell("f" + cellIndex).Value = "LTLP";
                        ws.Cell("g" + cellIndex).Value = "";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.ltlp);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.ltlp);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.ltlp);
                        ws.Cell("k" + cellIndex).Value = "Linear Size";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (volumeDrilling != null ? volumeDrilling.linear : 0); ;
                        ws.Cell("n" + cellIndex).Value = GetUom(uomMap, "Linear Size");

                        cellIndex = index + 9;
                        ws.Cell("a" + cellIndex).Value = "YP";
                        ws.Cell("b" + cellIndex).Value = "Pa";
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.yp);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.yp);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.yp);
                        ws.Cell("f" + cellIndex).Value = "HTHP";
                        ws.Cell("g" + cellIndex).Value = "";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.hthp);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.hthp);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.hthp);
                        ws.Cell("k" + cellIndex).Value = "PIT";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = "Pit Vol. (m3)";
                        ws.Cell("n" + cellIndex).Value = "Density";

                        cellIndex = index + 10;
                        ws.Cell("a" + cellIndex).Value = "Gels 10 sec";
                        ws.Cell("b" + cellIndex).Value = "Pa";
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.gels_10sec);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.gels_10sec);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.gels_10sec);
                        ws.Cell("f" + cellIndex).Value = "E Stb";
                        ws.Cell("g" + cellIndex).Value = "";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.estb);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.estb);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.estb);
                        ws.Cell("k" + cellIndex).Value = "PIT-1";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (mudPit.Where(x => x.pit_name == "PIT-1").Count() > 0 ? mudPit.Where(x => x.pit_name == "PIT-1").First().volume.ToString() : "");
                        ws.Cell("n" + cellIndex).Value = "10 kg/m3";

                        cellIndex = index + 11;
                        ws.Cell("a" + cellIndex).Value = "Gels 10 min";
                        ws.Cell("b" + cellIndex).Value = "Pa";
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.gels_10min);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.gels_10min);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.gels_10min);
                        ws.Cell("f" + cellIndex).Value = "PF";
                        ws.Cell("g" + cellIndex).Value = "cc";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.pf);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.pf);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.pf);
                        ws.Cell("k" + cellIndex).Value = "PIT-2";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (mudPit.Where(x => x.pit_name == "PIT-2").Count() > 0 ? mudPit.Where(x => x.pit_name == "PIT-2").First().volume.ToString() : ""); ;
                        ws.Cell("n" + cellIndex).Value = "10 kg/m3";

                        cellIndex = index + 12;
                        ws.Cell("a" + cellIndex).Value = "Fluid Loss";
                        ws.Cell("b" + cellIndex).Value = "mL/30min";
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.fluid_loss);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.fluid_loss);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.fluid_loss);
                        ws.Cell("f" + cellIndex).Value = "MF";
                        ws.Cell("g" + cellIndex).Value = "cc";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.mf);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.mf);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.mf);
                        ws.Cell("k" + cellIndex).Value = "PIT-3";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (mudPit.Where(x => x.pit_name == "PIT-3").Count() > 0 ? mudPit.Where(x => x.pit_name == "PIT-3").First().volume.ToString() : ""); ;
                        ws.Cell("n" + cellIndex).Value = "10 kg/m3";

                        cellIndex = index + 13;
                        ws.Cell("a" + cellIndex).Value = "pH";
                        ws.Cell("b" + cellIndex).Value = "";
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.ph);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.ph);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.ph);
                        ws.Cell("f" + cellIndex).Value = "PM";
                        ws.Cell("g" + cellIndex).Value = "cc";
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.pm);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.pm);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.pm);
                        ws.Cell("k" + cellIndex).Value = "PIT-4";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (mudPit.Where(x => x.pit_name == "PIT-4").Count() > 0 ? mudPit.Where(x => x.pit_name == "PIT-4").First().volume.ToString() : ""); ;
                        ws.Cell("n" + cellIndex).Value = "10 kg/m3";

                        cellIndex = index + 14;
                        ws.Cell("a" + cellIndex).Value = "Cake";
                        ws.Cell("b" + cellIndex).Value = GetUom(uomMap, "Cake");
                        ws.Cell("c" + cellIndex).Value = (mud1 == null ? 0 : mud1.cake);
                        ws.Cell("d" + cellIndex).Value = (mud2 == null ? 0 : mud2.cake);
                        ws.Cell("e" + cellIndex).Value = (mud3 == null ? 0 : mud3.cake);
                        ws.Cell("f" + cellIndex).Value = "ECD";
                        ws.Cell("g" + cellIndex).Value = GetUom(uomMap, "ECD");
                        ws.Cell("h" + cellIndex).Value = (mud1 == null ? 0 : mud1.ecd);
                        ws.Cell("i" + cellIndex).Value = (mud2 == null ? 0 : mud2.ecd);
                        ws.Cell("j" + cellIndex).Value = (mud3 == null ? 0 : mud3.ecd);
                        ws.Cell("k" + cellIndex).Value = "SLUG PIT";
                        ws.Range("k" + cellIndex + ":l" + cellIndex).Row(1).Merge();
                        ws.Cell("m" + cellIndex).Value = (mudPit.Where(x => x.pit_name == "SLUG PIT").Count() > 0 ? mudPit.Where(x => x.pit_name == "SLUG PIT").First().volume.ToString() : ""); ;
                        ws.Cell("n" + cellIndex).Value = "9 kg/m3";
                        cellEnd = cellStart + 2;
                        cellIndex = cellEnd;
                        foreach (var cem in chemical)
                        {
                            ws.Cell("p" + cellIndex).Value = cem.chemical_type;
                            ws.Range("p" + cellIndex + ":q" + cellIndex).Row(1).Merge();
                            ws.Cell("r" + cellIndex).Value = cem.amount;
                            ws.Cell("s" + cellIndex).Value = cem.uom_code;
                            cellEnd = cellIndex;
                            cellIndex++;
                        }

                        ws.Range("P" + cellStart + ":S" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("P" + cellStart + ":S" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("P" + cellStart + ":S" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("P" + cellStart + ":S" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("P" + cellStart + ":S" + cellEnd).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center); ;


                        if (cellEnd > index + 14)
                        {
                            index = cellIndex + 1;
                        }
                        else
                        {
                            index += 16;
                        }

                        ws.Cell("A" + index).Value = "13. Weather Data";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        cellIndex = index;
                        cellStart = cellIndex;
                        ws.Cell("A" + cellIndex).Value = "General";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = detail.Data.weather_general;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Barometer (kPa)";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = detail.Data.weather_barometer;
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "Pitch, half amp (°)";
                        ws.Range("K" + cellIndex + ":L" + index).Row(1).Merge();
                        ws.Cell("M" + cellIndex).Value = detail.Data.weather_pitch;
                        ws.Range("M" + cellIndex + ":N" + index).Row(1).Merge();

                        cellIndex = index + 1;
                        ws.Cell("A" + cellIndex).Value = "Wind Speed (m/s)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = detail.Data.weather_wind_speed;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Wave (m)";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = detail.Data.weather_wave;
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "Roll, half amp (°)";
                        ws.Range("K" + cellIndex + ":L" + index).Row(1).Merge();
                        ws.Cell("M" + cellIndex).Value = detail.Data.weather_roll;
                        ws.Range("M" + cellIndex + ":N" + index).Row(1).Merge();

                        cellIndex = index + 2;
                        ws.Cell("A" + cellIndex).Value = "Wind Direction (°)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = detail.Data.weather_wind_direction;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Wave Period (s)";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = detail.Data.weather_wave_period;
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Cell("K" + cellIndex).Value = "Heave (m)";
                        ws.Range("K" + cellIndex + ":L" + index).Row(1).Merge();
                        ws.Cell("M" + cellIndex).Value = detail.Data.weather_heave;
                        ws.Range("M" + cellIndex + ":N" + index).Row(1).Merge();

                        cellIndex = index + 3;
                        ws.Cell("A" + cellIndex).Value = "Temperature  (°C)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = detail.Data.weather_temperature;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Wave Direction (°)";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = detail.Data.weather_wave_direction;
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Range("K" + cellIndex + ":L" + index).Row(1).Merge();
                        ws.Range("M" + cellIndex + ":N" + index).Row(1).Merge();

                        cellIndex = index + 4;
                        ws.Cell("A" + cellIndex).Value = "Visibility (km)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = detail.Data.weather_visibility;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Cell("F" + cellIndex).Value = "Height (m)";
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + cellIndex).Value = detail.Data.weather_height;
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Range("K" + cellIndex + ":L" + index).Row(1).Merge();
                        ws.Range("M" + cellIndex + ":N" + index).Row(1).Merge();

                        cellIndex = index + 5;
                        ws.Cell("A" + cellIndex).Value = "Cloud (%)";
                        ws.Range("A" + cellIndex + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + cellIndex).Value = detail.Data.weather_cloud;
                        ws.Range("C" + cellIndex + ":D" + index).Row(1).Merge();
                        ws.Range("F" + cellIndex + ":G" + index).Row(1).Merge();
                        ws.Range("H" + cellIndex + ":I" + index).Row(1).Merge();
                        ws.Range("K" + cellIndex + ":L" + index).Row(1).Merge();
                        ws.Range("M" + cellIndex + ":N" + index).Row(1).Merge();

                        ws.Cell("P" + index).Value = detail.Data.weather_comments;
                        ws.Range("P" + cellStart + ":S" + cellIndex).Merge();

                        ws.Range("A" + cellStart + ":S" + cellIndex).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellIndex).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellIndex).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":S" + cellIndex).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        index += 7;

                        ws.Cell("A" + index).Value = "14. Personnel On Board";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        cellStart = index;
                        ws.Cell("A" + index).Value = "Job Title";
                        ws.Range("A" + index + ":B" + index).Row(1).Merge();
                        ws.Cell("C" + index).Value = "Personnel";
                        ws.Range("C" + index + ":D" + index).Row(1).Merge();
                        ws.Cell("E" + index).Value = "Company";
                        ws.Range("E" + index + ":G" + index).Row(1).Merge();
                        ws.Cell("H" + index).Value = "Pax";
                        ws.Range("H" + index + ":I" + index).Row(1).Merge();
                        ws.Range("A" + index + ":S" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD0CECE");
                        index++;

                        foreach (var personel in personels)
                        {
                            ws.Cell("A" + index).Value = personel.job_title;
                            ws.Range("A" + index + ":B" + index).Row(1).Merge();
                            ws.Cell("C" + index).Value = personel.personel;
                            ws.Range("C" + index + ":D" + index).Row(1).Merge();
                            ws.Cell("E" + index).Value = personel.company;
                            ws.Range("E" + index + ":G" + index).Row(1).Merge();
                            ws.Cell("H" + index).Value = personel.pax;
                            ws.Range("H" + index + ":I" + index).Row(1).Merge();
                            index++;
                        }
                        cellEnd = index - 1;
                        ws.Range("A" + cellStart + ":I" + cellEnd).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":I" + cellEnd).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":I" + cellEnd).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + cellStart + ":I" + cellEnd).Style.Border.LeftBorder = XLBorderStyleValues.Thin;

                        ws.Cell("A" + index).Value = "15. Day Vs Depth & Cost";
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Style.Fill.BackgroundColor = XLColor.FromHtml("#FFD9E1F2");
                        ws.Range("A" + index + ":S" + index).Row(1).Merge();
                        index++;

                        ws.Cell("A" + index).Value = "Well Profile";
                        ws.Cell("A" + index).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        ws.Cell("A" + index).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        ws.Range("A" + index + ":I" + index).Row(1).Merge();

                        ws.Cell("J" + index).Value = "Grafik Time vs Depth ";
                        ws.Cell("J" + index).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        ws.Cell("J" + index).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        ws.Range("J" + index + ":S" + index).Row(1).Merge();

                        var chartIndex = index + 1;
                        var chartTVD = ConfigurationManager.AppSettings["logoReportPath"].ToString() + "chart-ddr-pertamina-1.png";
                        var profile = ConfigurationManager.AppSettings["logoReportPath"].ToString() + "profile-ddr-pertamina-1.png";
                        ws.AddPicture(profile).MoveTo(ws.Cell("A" + chartIndex)).Scale(0.4);
                        ws.AddPicture(chartTVD).MoveTo(ws.Cell("K" + chartIndex)).Scale(0.4);

                        workbook.SaveAs(path);
                    }

                    result.Data = nameFile;
                    result.Status.Success = true;
                    result.Status.Message = "";
                }

                catch (Exception e)
                {

                    result.Data = "";
                    result.Status.Success = false;
                    result.Status.Message = "";
                    appLogger.Error(e);
                }
                return result;
            });
            return response;
        }

        [Route("report/ddr-migas/{wellId}/{fieldId}")]
        [HttpGet]
        public async Task<ApiResponse<string>> GetDdrMigas(string wellId, string fieldId)
        {
            var services = new WellServices(this.dataContext);
            var drillingService = new DrillingServices(this.dataContext);
            var wellObjectUomMapservices = new WellObjectUomMapServices(this.dataContext);
            var currencyServices = new CurrencyServices(this.dataContext);
            var drillingOperationActivityServices = new DrillingOperationActivityServices(this.dataContext);
            var drillingBhaBitServices = new DrillingBhaBitServices(this.dataContext);
            var wellBhaComponentServices = new WellBhaComponentServices(this.dataContext);
            var wellBitServices = new WellBitServices(this.dataContext);
            var drillingTransportServices = new DrillingTransportServices(this.dataContext);
            var drillingBulkServices = new DrillingBulkServices(this.dataContext);
            var drillingAeratedServices = new DrillingAeratedServices(this.dataContext);
            var drillFormationActualServices = new DrillFormationActualServices(this.dataContext);
            var drillFormationActualDetailServices = new DrillFormationActualDetailServices(this.dataContext);
            var stoneServices = new StoneServices(this.dataContext);
            var drillingDeviationServices = new DrillingDeviationServices(this.dataContext);
            var drillingMudServices = new DrillingMudServices(this.dataContext);
            var drillingTotalVolumeServices = new DrillingTotalVolumeServices(this.dataContext);
            var drillingMudPitServices = new DrillingMudPitServices(this.dataContext);
            var drillingChemicalUsedServices = new DrillingChemicalUsedServices(this.dataContext);
            var drillingPersonelServices = new DrillingPersonelServices(this.dataContext);
            var wellHoleAndCasingServices = new WellHoleAndCasingServices(this.dataContext);
            var response = await Task.Run<ApiResponse<string>>(() =>
            {
                var result = new ApiResponse<string>();
                try
                {
                    var well = services.getDetail(wellId).Data;
                    var detail = drillingService.Detail(fieldId, userSession).Data;
                    var detailNextDay = drillingService.DetailNextDay(fieldId, userSession).Data;
                    var uomMap = wellObjectUomMapservices.GetViewAll(" AND r.is_active = 1 AND r.well_id = @0 ", wellId);
                    var currency = currencyServices.GetById(detail.cummulative_cost_currency_id);
                    var drillingActivityResult = drillingOperationActivityServices.GetActivityByDrillingId(fieldId, wellId);
                    var drillingActivityResultNextDay = drillingOperationActivityServices.GetActivityByDrillingId(detailNextDay.id, wellId);
                    var drillingBhaBitRresult = drillingBhaBitServices.GetDetail(fieldId);
                    var drillingBhaBit = (drillingBhaBitRresult.Data.Count() == 0 ? null : drillingBhaBitRresult.Data);
                    var wellBhaComponent = new List<vw_well_bha_component>();
                    var wellBit = new vw_well_bit();
                    if (drillingBhaBit != null)
                    {
                        wellBhaComponent = wellBhaComponentServices.GetViewAll(" AND r.well_bha_id = @0 AND r.is_active=@1 ORDER BY r.created_on ASC ", drillingBhaBit.First().well_bha_id, true);
                        wellBit = wellBitServices.GetViewById(drillingBhaBit.First().well_bit_id);
                    }

                    vw_drilling_bha_bit drillingBhaBitSecond = null;
                    List<vw_well_bha_component> wellBhaComponentSecond = null;
                    vw_well_bit wellBitSecond = null;
                    if (drillingBhaBitRresult.Data.Count() > 1)
                    {
                        drillingBhaBitSecond = drillingBhaBitRresult.Data.Skip(1).Single();
                        wellBhaComponentSecond = wellBhaComponentServices.GetViewAll(" AND r.well_bha_id = @0 AND r.is_active=@1 ORDER BY r.created_on ASC ", drillingBhaBitSecond.well_bha_id, true);
                        wellBitSecond = wellBitServices.GetViewById(drillingBhaBitSecond.well_bit_id);
                    }

                    var drillingTransport = drillingTransportServices.GetViewAll(" AND r.drilling_id=@0 ", fieldId);
                    var drillingBulk = drillingBulkServices.getItemByWellId(fieldId, wellId, detail.drilling_date?.ToString("yyyy-MM-dd"));
                    var aerated = drillingAeratedServices.GetViewFirstOrDefault(" AND r.drilling_id=@0 ", fieldId);
                    List<drill_formations_actual> drillActual = ((IEnumerable)drillFormationActualServices.GetList(wellId).Data).Cast<drill_formations_actual>().ToList();
                    var stones = stoneServices.LookupAll(new MarvelDataSourceRequest() { PageSize = 1000 });
                    var deviations = drillingDeviationServices.GetViewAll(" AND r.drilling_id=@0 ORDER BY r.measured_depth ASC ", fieldId);
                    var mud1 = drillingMudServices.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", fieldId, "mud_1");
                    var mud2 = drillingMudServices.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", fieldId, "mud_2");
                    var mud3 = drillingMudServices.GetViewFirstOrDefault(" AND r.drilling_id=@0 AND r.data_type=@1 ", fieldId, "mud_3");
                    var volumeDrillingResult = drillingTotalVolumeServices.GetViewAll(" AND r.drilling_id=@0 ", fieldId);
                    var volumeDrilling = new vw_drilling_total_volume();
                    if (volumeDrillingResult.Count() > 0)
                    {
                        volumeDrilling = volumeDrillingResult.First();
                    }
                    var mudPit = drillingMudPitServices.GetViewAll(" AND r.drilling_id=@0 ", fieldId);
                    var chemicals = drillingChemicalUsedServices.GetViewAll(" AND r.drilling_id=@0 ", fieldId);
                    var personels = drillingPersonelServices.GetViewAll(" AND r.drilling_id=@0 ORDER BY r.created_on ASC ", fieldId);
                    var wellHoleAndCasing = wellHoleAndCasingServices.Lookup(new MarvelDataSourceRequest(), wellId).Items;

                    var nameFile = "SKK Migas " + detail.well_name + " " + (int)detail.dfs + ".xlsx";
                    var path = ConfigurationManager.AppSettings["reportPath"].ToString() + nameFile;
                    using (var workbook = new XLWorkbook())
                    {
                        //var logo1 = ConfigurationManager.AppSettings["logoReportPath"].ToString() + "report-ddr-pertamina-1.jpg";
                        var logo2 = ConfigurationManager.AppSettings["logoReportPath"].ToString() + "report-ddr-pertamina-2.jpg";

                        //var logo1 = @"D:\Project\Pertamina\Dreamwell\Dreamwell.Web\Content\img\report-excel\report-ddr-pertamina-1.jpg";
                        //var logo2 = @"D:\Project\Pertamina\Dreamwell\Dreamwell.Web\Content\img\report-excel\report -ddr-pertamina-2.jpg";

                        var ws = workbook.Worksheets.Add("Report");
                        ws.AddPicture(logo2).MoveTo(ws.Cell("A1")).Scale(0.4);
                        //ws.AddPicture(logo2).MoveTo(ws.Cell("Q1")).Scale(0.4);
                        ws.Range("A1:P800").Style.Font.FontName = "Arial";
                        ws.Range("A1:P800").Style.Font.FontSize = 9;

                        ws.Range("C1:M1").Row(1).Merge();
                        ws.Range("C2:M2").Row(1).Merge();
                        ws.Range("A1:B2").Merge();
                        ws.Range("N1:P2").Merge();
                        ws.Cell("C1").Value = "Daily Operations Report";
                        ws.Cell("C2").Value = detail.drilling_date;
                        ws.Range("C1:M2").Style.Font.Bold = true;
                        ws.Range("C1:M1").Style.Font.FontSize = 16;
                        ws.Range("C2:M2").Style.Font.FontSize = 12;
                        ws.Range("C1:M2").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Range("C2:M2").Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        ws.Range("M1:M2").Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        ws.Range("C1:C2").Style.Border.LeftBorder = XLBorderStyleValues.Medium;

                        ws.Range("A2:B2").Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        ws.Range("A2:B2").Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        ws.Range("A2:B2").Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        ws.Range("N2:P2").Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        ws.Range("N1:P1").Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        ws.Range("P1:P2").Style.Border.RightBorder = XLBorderStyleValues.Medium;

                        for (int i = 3; i <= 5; i++)
                        {
                            ws.Range("A" + i + ":B" + i).Row(1).Merge();
                            if (i != 4)
                            {
                                ws.Range("C" + i + ":G" + i).Row(1).Merge();
                                ws.Range("O" + i + ":P" + i).Row(1).Merge();
                            }
                            else
                            {
                                ws.Range("C" + i + ":D" + i).Merge();
                                ws.Range("F" + i + ":G" + i).Merge();
                                ws.Range("O" + i + ":P" + i).Merge();
                            }
                            ws.Range("H" + i + ":I" + i).Row(1).Merge();
                            ws.Range("J" + i + ":M" + i).Row(1).Merge();
                        }
                        ws.Range("O" + 5 + ":P" + 5).Unmerge();
                        ws.Range("A3:P5").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A3:P5").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A3:P5").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A3:P5").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A3:A5").Style.Font.FontSize = 10;

                        ws.Cell("A3").Value = "Operator";
                        ws.Cell("A4").Value = "Well Name";
                        ws.Cell("A5").Value = "Latitude";

                        ws.Cell("C4").Value = detail.well_name;
                        ws.Cell("F4").Value = detail.well_type;
                        ws.Cell("C5").Value = detail.latitude;


                        ws.Cell("H3").Value = "Contractor";
                        ws.Cell("H4").Value = "Field";
                        ws.Cell("H5").Value = "Longitude";

                        ws.Cell("J3").Value = detail.contractor_name;
                        ws.Cell("J4").Value = detail.field_name;
                        ws.Cell("J5").Value = detail.longitude;

                        ws.Cell("N3").Value = "Rpt. #";
                        ws.Cell("N4").Value = "Env.";
                        ws.Cell("N5").Value = "WD";

                        ws.Cell("O3").Value = detail.report_no;
                        ws.Cell("O4").Value = well.environment;
                        ws.Cell("O5").Value = well.water_depth;
                        ws.Cell("P5").Value = GetUom(uomMap, "Measured Depth ");

                        ws.Range("A6:E6").Merge();
                        ws.Range("F6:K6").Merge();
                        ws.Range("L6:P6").Merge();
                        ws.Range("A6:P6").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Range("A6:P6").Style.Font.Bold = true;
                        ws.Range("A6:P6").Style.Font.FontSize = 10;

                        ws.Range("A6:P6").Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        ws.Range("A6:P6").Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        ws.Range("A6:P6").Style.Border.LeftBorder = XLBorderStyleValues.Medium;

                        ws.Cell("A6").Value = "General";
                        ws.Cell("F6").Value = "Drilling Parameters";
                        ws.Cell("L6").Value = "AFE";

                        for (int i = 7; i <= 16; i++)
                        {
                            ws.Range("A" + i + ":B" + i).Merge();
                            ws.Range("D" + i + ":E" + i).Merge();
                            ws.Range("F" + i + ":H" + i).Merge();
                            ws.Range("J" + i + ":K" + i).Merge();
                            ws.Range("L" + i + ":M" + i).Merge();
                            ws.Range("O" + i + ":P" + i).Merge();
                        }
                        ws.Range("A7:P16").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A7:P16").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A7:P16").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A7:P16").Style.Border.BottomBorder = XLBorderStyleValues.Thin;

                        ws.Range("F7:F16").Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        ws.Range("L7:L16").Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        ws.Range("P7:P16").Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        ws.Range("A7:A16").Style.Border.LeftBorder = XLBorderStyleValues.Medium;
                        ws.Range("A16:P16").Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                        for (int i = 17; i <= 18; i++)
                        {
                            ws.Range("A" + i + ":C" + i).Merge();
                            ws.Range("D" + i + ":P" + i).Merge();
                        }
                        ws.Range("A17:P18").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A17:P18").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A17:P18").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A17:P18").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A17:P18").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        ws.Range("A19:P19").Merge();
                        ws.Range("A19:P19").Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        ws.Range("A19:P19").Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("A19").Value = "Time Breakdown";
                        ws.Range("A19").Style.Font.Bold = true;
                        ws.Range("A19").Style.Font.FontSize = 10;

                        var labels = new string[,] {
                            { "Rig Type / Name", "Avg WOB", "AFE Number" },
                            { "Rig Power", "Avg ROP", "AFE Cost" },
                            { "KB Elevation", "Avg RPM", "Daily Cost" },
                            { "Current MD", "Torque", "Cum. Cost" },
                            { "Progress MD", "Stand Pipe Press", "Daily Mud Cost" },
                            { "PTMD", "Flow Rate", "Cum. Mud Cost" },
                            { "Spud Date", "String Weight", "Day Supervisor" },
                            { "Release Date", "Rotating Weight", "Night Supervisor" },
                            { "Planned Days", "Total Drilling Time", "Engineer" },
                            { "Days from Spud", "Circulating Press", "Geologist" },
                        };

                        var index = 0;
                        for (int i = 7; i <= 16; i++)
                        {
                            ws.Cell("A" + i).Value = labels[index, 0];
                            ws.Cell("F" + i).Value = labels[index, 1];
                            ws.Cell("L" + i).Value = labels[index, 2];
                            index++;
                        }

                        ws.Cell("C7").Value = detail.rig_type;
                        ws.Cell("D7").Value = detail.rig_name;

                        ws.Cell("C8").Value = GetUom(uomMap, "Rig Rating");
                        ws.Cell("D8").Value = detail.rig_rating;

                        ws.Cell("C9").Value = GetUom(uomMap, "RKB Elevation");
                        ws.Cell("D9").Value = well.rkb_elevation;

                        ws.Cell("C10").Value = GetUom(uomMap, "Measured Depth ");
                        ws.Cell("D10").Value = detail.current_depth_md;

                        ws.Cell("C11").Value = GetUom(uomMap, "Measured Depth ");
                        ws.Cell("D11").Value = detail.current_depth_md - detail.previous_depth_md;

                        ws.Cell("C12").Value = GetUom(uomMap, "Measured Depth "); ;
                        ws.Cell("D12").Value = "";

                        ws.Cell("C13").Value = (detail.current_depth_md - detail.previous_depth_md) - detail.current_depth_md;
                        ws.Cell("D13").Value = detail.spud_date;

                        ws.Cell("C14").Value = "";
                        ws.Cell("D14").Value = detail.release_date;

                        ws.Cell("C15").Value = GetUom(uomMap, "DOL");
                        ws.Cell("D15").Value = detail.planned_days;

                        ws.Cell("C16").Value = GetUom(uomMap, "DOL");
                        ws.Cell("D16").Value = detail.dfs;

                        ws.Cell("I7").Value = GetUom(uomMap, "Weight Rotate");
                        ws.Cell("I8").Value = GetUom(uomMap, "Average ROP");
                        ws.Cell("I9").Value = "RPM";
                        ws.Cell("I10").Value = GetUom(uomMap, "Torque");
                        ws.Cell("I11").Value = GetUom(uomMap, "spp");
                        ws.Cell("I12").Value = GetUom(uomMap, "Flowrate");
                        ws.Cell("I13").Value = GetUom(uomMap, "String Weight");
                        ws.Cell("I14").Value = GetUom(uomMap, "Weight Rotate");

                        if (drillingBhaBit != null)
                        {
                            ws.Cell("J7").Value = drillingBhaBit.Min(x => x.mudlogging_wob_min).Value + " - " + drillingBhaBit.Max(x => x.mudlogging_wob_max).Value;
                            ws.Cell("J8").Value = drillingBhaBit.Min(x => x.mudlogging_avg_rop_min).Value + " - " + drillingBhaBit.Max(x => x.mudlogging_avg_rop_max).Value;
                            ws.Cell("J9").Value = drillingBhaBit.Min(x => x.mudlogging_rpm_min).Value + " - " + drillingBhaBit.Max(x => x.mudlogging_rpm_max).Value;
                            ws.Cell("J10").Value = drillingBhaBit.Min(x => x.mudlogging_torque_min).Value + " - " + drillingBhaBit.Max(x => x.mudlogging_torque_max).Value;
                            ws.Cell("J11").Value = drillingBhaBit.Min(x => x.mudlogging_spp_min).Value + " - " + drillingBhaBit.Max(x => x.mudlogging_spp_max).Value;
                            ws.Cell("J12").Value = drillingBhaBit.Min(x => x.mudlogging_flowrate_min).Value + " - " + drillingBhaBit.Max(x => x.mudlogging_flowrate_max).Value;
                            if (drillingBhaBit.Count > 1)
                            {
                                ws.Cell("J13").Value = drillingBhaBit.Min(x => x.string_weight).Value + " - " + drillingBhaBit.Max(x => x.string_weight).Value;
                                ws.Cell("J14").Value = drillingBhaBit.Min(x => x.weight_rotate).Value + " - " + drillingBhaBit.Max(x => x.weight_rotate).Value;
                            }
                            else
                            {
                                ws.Cell("J13").Value = drillingBhaBit.First().string_weight;
                                ws.Cell("J14").Value = drillingBhaBit.First().weight_rotate;
                            }
                        }

                        ws.Cell("I15").Value = "";
                        ws.Cell("J15").Value = "";

                        ws.Cell("I16").Value = "";
                        ws.Cell("J16").Value = "";

                        ws.Cell("N7").Value = "";
                        ws.Cell("O7").Value = detail.afe_no;

                        ws.Cell("N8").Value = "USD";
                        ws.Cell("O8").Value = detail.afe_cost;

                        ws.Cell("N9").Value = "USD";
                        ws.Cell("O9").Value = detail.daily_cost;

                        ws.Cell("N10").Value = "USD";
                        ws.Cell("O10").Value = detail.cummulative_cost;

                        ws.Cell("N11").Value = "USD";
                        ws.Cell("O11").Value = detail.daily_mud_cost ?? 0;

                        ws.Cell("N12").Value = "USD";
                        ws.Cell("O12").Value = detail.cummulative_mud_cost ?? 0;

                        var personel = personels.Where(x => x.job_title == "Day Supervisor" || x.job_title == "Drilling Rig Supervisor" || x.job_title == "Drilling Supervisor");
                        ws.Cell("N13").Value = "";
                        if (personel.Count() > 0)
                        {
                            ws.Cell("O13").Value = personel.First().personel;
                        }

                        ws.Cell("N14").Value = "";
                        personel = personels.Where(x => x.job_title == "Night Supervisor" || x.job_title == "Drilling Supervisor" || x.job_title == "Drilling Rig Supervisor");
                        if (personel.Count() > 0)
                        {
                            ws.Cell("O14").Value = personel.First().personel;
                        }

                        ws.Cell("N15").Value = "";
                        personel = personels.Where(x => x.job_title == "Drilling Engineer" || x.job_title == "Engineer");
                        if (personel.Count() > 0)
                        {
                            ws.Cell("O15").Value = personel.First().personel;
                        }

                        ws.Cell("N16").Value = "";
                        personel = personels.Where(x => x.job_title == "Geologist");
                        if (personel.Count() > 0)
                        {
                            ws.Cell("O16").Value = personel.First().personel;
                        }

                        ws.Cell("A17").Value = "24 Hours Summary";
                        ws.Cell("A18").Value = "24 Hours Forecast";

                        ws.Cell("D17").Value = detail.operation_data_period;
                        ws.Row(17).AdjustToContents();

                        ws.Cell("D18").Value = detail.operation_data_planned;
                        ws.Row(18).AdjustToContents();


                        ws.Range("A20:C20").Merge();
                        ws.Range("D20:E20").Merge();
                        ws.Range("G20:K20").Merge();
                        ws.Range("L20:P21").Merge();
                        ws.Range("G21:H21").Merge();

                        ws.Cell("A20").Value = "Time hh:mm";
                        ws.Cell("D20").Value = "Measured Depth";
                        ws.Cell("F20").Value = "Length";
                        ws.Cell("G20").Value = "Task";
                        ws.Cell("L20").Value = "Operation";
                        ws.Cell("L20").Style.Font.Bold = true;
                        ws.Cell("L20").Style.Font.FontSize = 10;

                        ws.Cell("A21").Value = "Start";
                        ws.Cell("B21").Value = "End";
                        ws.Cell("C21").Value = "Elapsed";
                        ws.Cell("D21").Value = "Start";
                        ws.Cell("E21").Value = "End";
                        ws.Cell("F21").Value = GetUom(uomMap, "Depth");
                        ws.Cell("G21").Value = "Category";
                        ws.Cell("I21").Value = "P";
                        ws.Cell("J21").Value = "NPT";
                        ws.Cell("K21").Value = "Code";

                        index = 22;
                        var drillingActivity = drillingActivityResult.Data.First();
                        //var drillingActivityDetail = drillingActivity.DrillingOperationActivities;
                        decimal? currentDepth = 0;
                        var isFirstArray = true;

                        foreach (var drillingActivities in drillingActivityResult.Data)
                        {
                            if (!isFirstArray)
                            {
                                ws.Cell("A" + index).Value = "Start";
                                ws.Cell("B" + index).Value = "End";
                                ws.Cell("C" + index).Value = "Elapsed";
                                ws.Cell("D" + index).Value = "Start";
                                ws.Cell("E" + index).Value = "End";
                                ws.Cell("F" + index).Value = GetUom(uomMap, "Depth");
                                ws.Cell("G" + index).Value = "Category";
                                ws.Cell("I" + index).Value = "P";
                                ws.Cell("J" + index).Value = "NPT";
                                ws.Cell("K" + index).Value = "Code";
                                ws.Cell("L" + index).Value = "Operation";
                                ws.Cell("L" + index).Style.Font.Bold = true;
                                ws.Cell("L" + index).Style.Font.FontSize = 10;

                                // Tambahkan baris untuk data berikutnya
                                index++;
                            }
                            var drillingActivityDetail = drillingActivities.DrillingOperationActivities;

                            for (int i = 0; i < drillingActivityDetail.Count; i++)
                            {
                                if (drillingActivityDetail[i].operation_end_date?.ToString("HH:mm") == "23:59")
                                {
                                    drillingActivityDetail[i].operation_end_date = drillingActivityDetail[i].operation_end_date?.AddMinutes(1);
                                }
                                var start = drillingActivityDetail[i].operation_start_date?.TimeOfDay;
                                var end = drillingActivityDetail[i].operation_end_date?.TimeOfDay;
                                var hours = end - start;
                                ws.Cell("A" + index).SetValue<string>(start.Value.ToString(@"hh\:mm"));
                                ws.Cell("B" + index).SetValue<string>(end.Value.ToString(@"hh\:mm"));
                                ws.Cell("C" + index).SetValue<string>(hours.Value.ToString(@"hh\:mm"));
                                ws.Cell("D" + index).Value = currentDepth;
                                ws.Cell("E" + index).Value = drillingActivityDetail[i].depth;
                                ws.Cell("F" + index).Value = drillingActivityDetail[i].depth - currentDepth;
                                currentDepth = drillingActivityDetail[i].depth;
                                ws.Cell("G" + index).Value = drillingActivityDetail[i].category;
                                ws.Range("G" + index + ":H" + index).Row(1).Merge();
                                ws.Cell("I" + index).Value = drillingActivityDetail[i].type_desc == "PT" ? "Y" : "N";
                                ws.Cell("J" + index).Value = drillingActivityDetail[i].type_desc;
                                ws.Cell("K" + index).Value = drillingActivityDetail[i].iadc_code + " - " + drillingActivityDetail[i].iadc_description;
                                ws.Cell("L" + index).Value = drillingActivityDetail[i].description;
                                ws.Range("L" + index + ":P" + index).Row(1).Merge();
                                ws.Cell("L" + index).Style.Alignment.WrapText = true;
                                ws.Row(index).AdjustToContents();
                                index++;
                            }
                            isFirstArray = false;
                        }

                        if (detailNextDay != null)
                        {
                            ws.Cell("A" + index).Value = "Status Pagi," + detailNextDay.drilling_date.Value.ToString("dd MMMM yyyy");
                            ws.Cell("A" + index).Style.Font.Bold = true;
                            ws.Range("A" + index + ":P" + index).Row(1).Merge();

                            index++;
                            var drillingActivityPagi = drillingActivityResultNextDay.Data.First().DrillingOperationActivities.First();
                            var start = drillingActivityPagi.operation_start_date?.TimeOfDay;
                            var end = drillingActivityPagi.operation_end_date?.TimeOfDay;
                            var hours = end - start;
                            ws.Cell("A" + index).SetValue<string>(start.Value.ToString(@"hh\:mm"));
                            ws.Cell("B" + index).SetValue<string>(end.Value.ToString(@"hh\:mm"));
                            ws.Cell("C" + index).SetValue<string>(hours.Value.ToString(@"hh\:mm"));
                            ws.Cell("D" + index).Value = 0;
                            ws.Cell("E" + index).Value = drillingActivityPagi.depth;
                            ws.Cell("F" + index).Value = drillingActivityPagi.depth;
                            ws.Range("G" + index + ":H" + index).Row(1).Merge();
                            ws.Range("L" + index + ":P" + index).Row(1).Merge();
                        }
                        ws.Range("A20:P" + index).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A20:P" + index).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A20:P" + index).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A20:P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A20:P" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        index++;
                        ws.Range("A" + index + ":D" + index).Merge();
                        ws.Range("E" + index + ":H" + index).Merge();
                        ws.Range("I" + index + ":L" + index).Merge();
                        ws.Range("M" + index + ":P" + index).Merge();

                        ws.Range("A" + index + ":P" + index).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        ws.Range("A" + index + ":P" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Range("A" + index + ":P" + index).Style.Font.Bold = true;
                        ws.Range("A" + index + ":P" + index).Style.Font.FontSize = 10;

                        ws.Cell("A" + index).Value = "Bit Record";
                        ws.Cell("E" + index).Value = "BHA - 1";
                        ws.Cell("I" + index).Value = "BHA - 2";
                        ws.Cell("M" + index).Value = "Casing";
                        index++;

                        ws.Range("A" + index + ":D" + index).Style.Font.Bold = true;
                        ws.Range("A" + index + ":D" + index).Style.Font.FontSize = 10;
                        ws.Range("A" + index + ":B" + index).Merge();
                        ws.Cell("C" + index).Value = "Bit - 1";
                        ws.Cell("D" + index).Value = "Bit - 2";
                        ws.Cell("C" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("D" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        ws.Cell("E" + index).Value = "BHA Number";
                        ws.Cell("F" + index).Value = (drillingBhaBit == null ? "" : drillingBhaBit.First().bha_no.ToString());
                        ws.Cell("G" + index).Value = "BHA Run";
                        ws.Cell("H" + index).Value = "";

                        ws.Cell("I" + index).Value = "BHA Number";
                        ws.Cell("J" + index).Value = (drillingBhaBitSecond == null ? "" : drillingBhaBitSecond.bha_no.ToString());
                        ws.Cell("K" + index).Value = "BHA Run";
                        ws.Cell("L" + index).Value = "";

                        labels = new string[,] {
                            {"", "Last Size" },
                            {"Cumulative", "Set MD"},
                            {"Bit Size", "Next Size"},
                            {"Bit Run", "Set MD"},
                            {"Manufacturer", "Last LOT EMW"},
                            {"IADC Code", "TOL"},
                            {"Jets", "Mud Volumes" },
                            {"Serial #", "Start"},
                            {"Depth Out","Lost Surface"},
                            {"Depth In","Lost DH" },
                            {"Meterage","Dumped"},
                            {"Bit Hours","Built" },
                            {"Nozzles","Ending" },
                            {"Dull Grade","Gas"},
                            {"Cumulative","Max. Gas" },
                            {"Footage","Conn. Gas"},
                            {"Bit Hours","Trip Gas" },
                            {"ROP","Back Gas"}
                        };
                        var tempIndex = 0;
                        for (int i = index; i < (index + 18); i++)
                        {
                            if (i == (index + 6) || i == (index + 13))
                            {
                                ws.Cell("A" + i).Value = labels[tempIndex, 0];
                                ws.Range("M" + i + ":P" + i).Merge();
                                ws.Range("M" + i + ":P" + i).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                ws.Cell("M" + i).Style.Font.Bold = true;
                                ws.Cell("M" + i).Value = labels[tempIndex, 1];
                            }
                            else if (i == (index + 14))
                            {
                                ws.Range("A" + i + ":D" + i).Merge();
                                ws.Cell("A" + i).Value = labels[tempIndex, 0];
                                ws.Range("A" + i + ":D" + i).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                                ws.Cell("A" + i).Style.Font.Bold = true;

                                ws.Range("M" + i + ":N" + i).Merge();
                                ws.Cell("M" + i).Value = labels[tempIndex, 1];
                            }
                            else
                            {
                                ws.Cell("A" + i).Value = labels[tempIndex, 0];
                                ws.Range("M" + i + ":N" + i).Merge();
                                ws.Cell("M" + i).Value = labels[tempIndex, 1];
                            }
                            tempIndex++;
                        }

                        ws.Cell("O" + index).Value = GetUom(uomMap, "Casing Diameter");
                        ws.Cell("P" + index).Value = drillingActivity.casing_name;

                        var indexHoleCasing = -1;
                        for (int i = 0; i <= wellHoleAndCasing.Count(); i++)
                        {
                            if (wellHoleAndCasing[i].id == drillingActivity.well_hole_and_casing_id)
                            {
                                indexHoleCasing = i + 1;
                                break;
                            }
                        }

                        ws.Range("E" + (index + 1) + ":F" + (index + 2)).Merge();
                        ws.Range("E" + (index + 1)).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Range("E" + (index + 1)).Style.Font.Bold = true;
                        ws.Range("E" + (index + 1)).Style.Font.FontSize = 10;
                        ws.Cell("E" + (index + 1)).Value = "Component";

                        ws.Range("I" + (index + 1) + ":J" + (index + 2)).Merge();
                        ws.Range("I" + (index + 1)).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Range("I" + (index + 1)).Style.Font.Bold = true;
                        ws.Range("I" + (index + 1)).Style.Font.FontSize = 10;
                        ws.Cell("I" + (index + 1)).Value = "Component";

                        ws.Cell("A" + (index + 1)).Value = "Bit Number";
                        ws.Cell("C" + (index + 1)).Value = (drillingBhaBit == null ? "" : drillingBhaBit.First().bha_no.ToString());
                        ws.Cell("D" + (index + 1)).Value = (drillingBhaBitSecond == null ? "" : drillingBhaBitSecond.bha_no.ToString());
                        ws.Cell("G" + (index + 1)).Value = "OD";
                        ws.Cell("H" + (index + 1)).Value = "Length";
                        ws.Cell("K" + (index + 1)).Value = "OD";
                        ws.Cell("L" + (index + 1)).Value = "Length";

                        ws.Cell("O" + (index + 1)).Value = GetUom(uomMap, "Depth");
                        ws.Cell("P" + (index + 1)).Value = drillingActivity.casing_val;


                        ws.Cell("B" + (index + 2)).Value = GetUom(uomMap, "Bit Size");
                        ws.Cell("C" + (index + 2)).Value = (drillingBhaBit == null ? "" : drillingBhaBit.First().bit_size.ToString()); ;
                        ws.Cell("D" + (index + 2)).Value = (drillingBhaBitSecond == null ? "" : drillingBhaBitSecond.bit_size.ToString());


                        if (indexHoleCasing >= 0)
                        {
                            var nextWellHoleCasing = wellHoleAndCasing[indexHoleCasing];
                            ws.Cell("O" + (index + 2)).Value = GetUom(uomMap, "Casing Diameter");
                            ws.Cell("P" + (index + 2)).Value = nextWellHoleCasing.casing_name;
                        }


                        ws.Cell("G" + (index + 2)).Value = GetUom(uomMap, "OD");
                        ws.Cell("H" + (index + 2)).Value = GetUom(uomMap, "Length");
                        ws.Cell("K" + (index + 2)).Value = GetUom(uomMap, "OD");
                        ws.Cell("L" + (index + 2)).Value = GetUom(uomMap, "Length");

                        ws.Cell("C" + (index + 3)).Value = (drillingBhaBit == null ? "" : drillingBhaBit.First().bit_run.ToString()); ;
                        ws.Cell("D" + (index + 3)).Value = (drillingBhaBitSecond == null ? "" : drillingBhaBitSecond.bit_run.ToString());

                        if (indexHoleCasing >= 0)
                        {
                            var nextWellHoleCasing = wellHoleAndCasing[indexHoleCasing];
                            ws.Cell("O" + (index + 3)).Value = GetUom(uomMap, "Depth");
                            ws.Cell("P" + (index + 3)).Value = nextWellHoleCasing.casing_setting_depth;
                        }

                        ws.Cell("C" + (index + 4)).Value = (wellBit == null ? "" : wellBit.manufacturer);
                        ws.Cell("D" + (index + 4)).Value = (wellBitSecond == null ? "" : wellBitSecond.manufacturer);

                        ws.Cell("O" + (index + 4)).Value = GetUom(uomMap, "MW In");
                        ws.Cell("P" + (index + 4)).Value = drillingActivity.lot_fit_val;


                        ws.Cell("C" + (index + 5)).Value = (wellBit == null ? "" : wellBit.iadc_code);
                        ws.Cell("D" + (index + 5)).Value = (wellBitSecond == null ? "" : wellBitSecond.iadc_code);

                        if (indexHoleCasing >= 0)
                        {
                            var nextWellHoleCasing = wellHoleAndCasing[indexHoleCasing];
                            ws.Cell("O" + (index + 5)).Value = GetUom(uomMap, "Depth");
                            ws.Cell("P" + (index + 5)).Value = nextWellHoleCasing.casing_top_of_liner;
                        }

                        ws.Cell("C" + (index + 6)).Value = "";
                        ws.Cell("D" + (index + 6)).Value = "";

                        ws.Cell("C" + (index + 7)).Value = (wellBit == null ? "" : wellBit.serial_number);
                        ws.Cell("D" + (index + 7)).Value = (wellBitSecond == null ? "" : wellBitSecond.serial_number);

                        ws.Cell("O" + (index + 7)).Value = GetUom(uomMap, "Plan Volume");
                        ws.Cell("P" + (index + 7)).Value = volumeDrilling.total_volume_start;

                        ws.Cell("B" + (index + 8)).Value = GetUom(uomMap, "Depth");
                        ws.Cell("C" + (index + 8)).Value = (drillingBhaBit == null ? "" : drillingBhaBit.First().depth_out.ToString());
                        ws.Cell("D" + (index + 8)).Value = (drillingBhaBitSecond == null ? "" : drillingBhaBitSecond.depth_out.ToString());

                        ws.Cell("O" + (index + 8)).Value = GetUom(uomMap, "Plan Volume");
                        ws.Cell("P" + (index + 8)).Value = volumeDrilling.lost_surface;

                        ws.Cell("B" + (index + 9)).Value = GetUom(uomMap, "Depth");
                        ws.Cell("C" + (index + 9)).Value = (drillingBhaBit == null ? "" : drillingBhaBit.First().depth_in.ToString());
                        ws.Cell("D" + (index + 9)).Value = (drillingBhaBitSecond == null ? "" : drillingBhaBitSecond.depth_in.ToString());

                        ws.Cell("O" + (index + 9)).Value = GetUom(uomMap, "Plan Volume");
                        ws.Cell("P" + (index + 9)).Value = volumeDrilling.lost_downhole;

                        ws.Cell("B" + (index + 10)).Value = GetUom(uomMap, "Depth");
                        if (drillingBhaBit != null)
                        {
                            ws.Cell("C" + (index + 10)).Value = drillingBhaBit.First().depth_out - drillingBhaBit.First().depth_in;
                        }

                        if (drillingBhaBitSecond != null)
                        {
                            ws.Cell("D" + (index + 10)).Value = drillingBhaBitSecond.depth_out - drillingBhaBitSecond.depth_in;
                        }

                        ws.Cell("O" + (index + 10)).Value = GetUom(uomMap, "Plan Volume");
                        ws.Cell("P" + (index + 10)).Value = volumeDrilling.dumped;

                        ws.Cell("B" + (index + 11)).Value = "Hrs";
                        ws.Cell("C" + (index + 11)).Value = (drillingBhaBit == null ? "" : drillingBhaBit.First().duration.ToString());
                        ws.Cell("D" + (index + 11)).Value = (drillingBhaBitSecond == null ? "" : drillingBhaBitSecond.duration.ToString());

                        ws.Cell("O" + (index + 11)).Value = GetUom(uomMap, "Plan Volume");
                        ws.Cell("P" + (index + 11)).Value = volumeDrilling.build;
                        ws.Cell("B" + (index + 12)).Value = GetUom(uomMap, "Nozzle Size");
                        var noozle = "";
                        if (wellBit != null)
                        {
                            for (int i = 1; i <= 5; i++)
                            {
                                var objectValue = wellBit.GetPropValue("noozle_" + i);
                                if (objectValue != null)
                                {
                                    noozle += objectValue.ToString().Replace(",", ".") + ",";
                                }
                            }
                        }
                        noozle = noozle.Length > 0 ? noozle.Remove(noozle.Length - 1) : "";
                        ws.Cell("C" + (index + 12)).Value = noozle;

                        noozle = "";
                        if (wellBitSecond != null)
                        {
                            for (int i = 1; i <= 5; i++)
                            {
                                var objectValue = wellBitSecond.GetPropValue("noozle_" + i);
                                if (objectValue != null)
                                {
                                    noozle += objectValue.ToString().Replace(",", ".") + ",";
                                }
                            }
                        }
                        noozle = noozle.Length > 0 ? noozle.Remove(noozle.Length - 1) : "";
                        ws.Cell("D" + (index + 12)).Value = noozle;

                        ws.Cell("O" + (index + 12)).Value = GetUom(uomMap, "Plan Volume");
                        ws.Cell("P" + (index + 12)).Value = volumeDrilling.ending;

                        ws.Cell("C" + (index + 13)).Value = "";
                        ws.Cell("D" + (index + 13)).Value = "";

                        ws.Range("O" + (index + 14) + ":P" + (index + 14)).Merge();
                        ws.Cell("O" + (index + 14)).Value = "";

                        ws.Cell("B" + (index + 15)).Value = "Hrs";
                        ws.Range("C" + (index + 15) + ":D" + (index + 15)).Merge();

                        ws.Range("O" + (index + 15) + ":P" + (index + 15)).Merge();
                        ws.Cell("O" + (index + 15)).Value = "";

                        ws.Cell("B" + (index + 16)).Value = GetUom(uomMap, "Depth");
                        ws.Range("C" + (index + 16) + ":D" + (index + 16)).Merge();

                        ws.Range("O" + (index + 16) + ":P" + (index + 16)).Merge();
                        ws.Cell("O" + (index + 16)).Value = "";

                        if (drillingBhaBit != null)
                        {
                            ws.Cell("C" + (index + 15)).Value = drillingBhaBit.First().duration;
                            ws.Cell("C" + (index + 16)).Value = drillingBhaBit.First().dg_footage;
                            ws.Cell("C" + (index + 17)).Value = drillingBhaBit.First().dg_footage == 0 && drillingBhaBit.First().duration == 0 ? 0 : drillingBhaBit.First().dg_footage / drillingBhaBit.First().duration;
                        }

                        ws.Cell("B" + (index + 17)).Value = GetUom(uomMap, "ROP Overall");
                        ws.Range("C" + (index + 17) + ":D" + (index + 17)).Merge();

                        ws.Range("O" + (index + 17) + ":P" + (index + 17)).Merge();
                        ws.Cell("O" + (index + 17)).Value = "";

                        var indexComponent = index + 3;

                        tempIndex = 0;
                        for (int i = (index + 3); i < (index + 3 + wellBhaComponent.Count); i++)
                        {
                            ws.Range("E" + i + ":F" + i).Merge();
                            ws.Cell("E" + i).Value = wellBhaComponent[tempIndex].component_name;
                            ws.Cell("G" + i).Value = wellBhaComponent[tempIndex].n_od;
                            ws.Cell("H" + i).Value = wellBhaComponent[tempIndex].length;
                            indexComponent++;
                            tempIndex++;
                        }

                        var indexComponentSecond = index + 3;
                        if (wellBhaComponentSecond != null)
                        {
                            tempIndex = 0;
                            for (int i = (index + 3); i < (index + 3 + wellBhaComponentSecond.Count); i++)
                            {
                                ws.Range("I" + i + ":J" + i).Merge();
                                ws.Cell("I" + i).Value = wellBhaComponentSecond[tempIndex].component_name;
                                ws.Cell("K" + i).Value = wellBhaComponentSecond[tempIndex].n_od;
                                ws.Cell("L" + i).Value = wellBhaComponentSecond[tempIndex].length;
                                indexComponentSecond++;
                                tempIndex++;
                            }
                        }

                        var lastIndex = (indexComponent > indexComponentSecond ? indexComponent - 1 : indexComponentSecond - 1);
                        lastIndex = lastIndex > (index + 18) ? lastIndex : index + 18;
                        ws.Range("A" + index + ":P" + lastIndex).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":P" + lastIndex).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":P" + lastIndex).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":P" + lastIndex).Style.Border.BottomBorder = XLBorderStyleValues.Thin;

                        ws.Range("D" + (index - 1) + ":D" + lastIndex).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        ws.Range("H" + (index - 1) + ":H" + lastIndex).Style.Border.RightBorder = XLBorderStyleValues.Medium;
                        ws.Range("L" + (index - 1) + ":L" + lastIndex).Style.Border.RightBorder = XLBorderStyleValues.Medium;

                        ws.Range("M" + (index + 6) + ":P" + (index + 6)).Style.Border.TopBorder = XLBorderStyleValues.Medium;
                        ws.Range("M" + (index + 13) + ":P" + (index + 13)).Style.Border.TopBorder = XLBorderStyleValues.Medium;

                        ws.Range("A" + lastIndex + ":P" + lastIndex).Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                        index = lastIndex;
                        ws.Range("A" + index + ":J" + index).Merge();
                        ws.Cell("A" + index).Value = "Drilling Fluid";
                        ws.Range("K" + index + ":L" + index).Merge();
                        ws.Cell("K" + index).Value = "Mud Additive";
                        ws.Range("M" + index + ":P" + (index + 1)).Merge();
                        ws.Range("A" + index + ":P" + (index + 1)).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("M" + index).Value = "Hydraulic Analysis";
                        ws.Range("A" + index + ":P" + index).Style.Font.FontSize = 10;
                        ws.Range("A" + index + ":P" + index).Style.Font.Bold = true;

                        index++;
                        ws.Range("A" + index + ":B" + index).Merge();
                        ws.Cell("C" + index).Value = "1";
                        ws.Cell("D" + index).Value = "2";
                        ws.Cell("E" + index).Value = "3";
                        ws.Range("F" + index + ":G" + index).Merge();
                        ws.Cell("H" + index).Value = "1";
                        ws.Cell("I" + index).Value = "2";
                        ws.Cell("J" + index).Value = "3";
                        ws.Cell("K" + index).Value = "Type";
                        ws.Cell("L" + index).Value = "Amount";
                        index++;
                        var chemicalIndex = index;
                        foreach (var item in chemicals)
                        {
                            ws.Cell("K" + chemicalIndex).Value = item.chemical_type;
                            ws.Cell("L" + chemicalIndex).Value = item.amount;
                            chemicalIndex++;
                        }
                        ws.Range("a" + index + ":b" + index).Merge();
                        ws.Cell("A" + index).Value = "Mud Type";
                        ws.Cell("c" + index).Value = (mud1 == null ? "" : mud1.mud_name);
                        ws.Cell("d" + index).Value = (mud2 == null ? "" : mud2.mud_name);
                        ws.Cell("e" + index).Value = (mud3 == null ? "" : mud3.mud_name);
                        ws.Cell("f" + index).Value = "Solids";
                        ws.Cell("g" + index).Value = "%";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.solids);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.solids);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.solids);
                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "Annular Velocity";
                        ws.Cell("o" + index).Value = "ft/min";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "Time";
                        ws.Cell("b" + index).Value = "HH:MM";
                        ws.Cell("c" + index).SetValue<string>((mud1 == null ? "" : mud1.mud_time?.ToString("HH:mm")));
                        ws.Cell("d" + index).SetValue<string>((mud2 == null ? "" : mud2.mud_time?.ToString("HH:mm")));
                        ws.Cell("e" + index).SetValue<string>((mud3 == null ? "" : mud3.mud_time?.ToString("HH:mm")));
                        ws.Cell("f" + index).Value = "Sand";
                        ws.Cell("g" + index).Value = "%";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.sand);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.sand);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.sand);


                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "Pb";
                        ws.Cell("o" + index).Value = "psi";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "MW In";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "MW In");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.mw_in);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.mw_in);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.mw_in);
                        ws.Cell("f" + index).Value = "Water";
                        ws.Cell("g" + index).Value = "%";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.water);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.water);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.water);


                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "Sys HHP";
                        ws.Cell("o" + index).Value = "";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "MW Out";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "MW Out");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.mw_out);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.mw_out);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.mw_out);
                        ws.Cell("f" + index).Value = "Oil";
                        ws.Cell("g" + index).Value = "%";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.oil);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.oil);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.oil);


                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "HHPb";
                        ws.Cell("o" + index).Value = "hp";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "Temp In";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "Temperature");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.temp_in);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.temp_in);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.temp_in);
                        ws.Cell("f" + index).Value = "HGS";
                        ws.Cell("g" + index).Value = "%";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.hgs);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.hgs);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.hgs);


                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "HSI";
                        ws.Cell("o" + index).Value = "hp/in2";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "Temp Out";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "Temperature");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.mw_out);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.mw_out);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.mw_out);
                        ws.Cell("f" + index).Value = "LGS";
                        ws.Cell("g" + index).Value = "%";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.lgs);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.lgs);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.lgs);


                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "%psib";
                        ws.Cell("o" + index).Value = "%";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "Pres. Grad";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "PRESSURE GRADIENT");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.pres_grad);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.pres_grad);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.pres_grad);
                        ws.Cell("f" + index).Value = "LTLP";
                        ws.Cell("g" + index).Value = "";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.ltlp);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.ltlp);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.ltlp);


                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "Jet Velocity";
                        ws.Cell("o" + index).Value = "ft/sec";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "Visc";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "VISCOSITY");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.visc);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.visc);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.visc);
                        ws.Cell("f" + index).Value = "HTHP";
                        ws.Cell("g" + index).Value = "";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.hthp);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.hthp);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.hthp);


                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "Impact force";
                        ws.Cell("o" + index).Value = "lbs";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "PV";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "PV");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.pv);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.pv);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.pv);
                        ws.Cell("f" + index).Value = "Cake";
                        ws.Cell("g" + index).Value = GetUom(uomMap, "Nozzle Size");
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.cake);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.cake);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.cake);


                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("m" + index).Value = "IF/area";
                        ws.Cell("o" + index).Value = "lb/in2";
                        ws.Cell("p" + index).Value = "";
                        index++;
                        ws.Cell("A" + index).Value = "YP";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "PRESSURE");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.yp);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.yp);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.yp);
                        ws.Cell("f" + index).Value = "E Stb";
                        ws.Cell("g" + index).Value = "";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.estb);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.estb);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.estb);


                        ws.Range("M" + index + ":N" + index).Merge();
                        index++;
                        ws.Cell("A" + index).Value = "Gels 10 sec";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "PRESSURE");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.gels_10sec);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.gels_10sec);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.gels_10sec);
                        ws.Cell("f" + index).Value = "PF";
                        ws.Cell("g" + index).Value = "";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.pf);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.pf);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.pf);


                        ws.Range("M" + index + ":N" + index).Merge();
                        index++;
                        ws.Cell("A" + index).Value = "Gels 10 min";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "PRESSURE");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.gels_10min);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.gels_10min);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.gels_10min);
                        ws.Cell("f" + index).Value = "MF";
                        ws.Cell("g" + index).Value = "";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.mf);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.mf);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.mf);


                        ws.Range("M" + index + ":N" + index).Merge();
                        index++;
                        ws.Cell("A" + index).Value = "Fluid Loss";
                        ws.Cell("b" + index).Value = GetUom(uomMap, "WATER LOSS");
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.fluid_loss);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.fluid_loss);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.fluid_loss);
                        ws.Cell("f" + index).Value = "PM";
                        ws.Cell("g" + index).Value = "";
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.pm);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.pm);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.pm);


                        ws.Range("M" + index + ":N" + index).Merge();
                        index++;
                        ws.Cell("A" + index).Value = "pH";
                        ws.Cell("b" + index).Value = "";
                        ws.Cell("c" + index).Value = (mud1 == null ? 0 : mud1.ph);
                        ws.Cell("d" + index).Value = (mud2 == null ? 0 : mud2.ph);
                        ws.Cell("e" + index).Value = (mud3 == null ? 0 : mud3.ph);
                        ws.Cell("f" + index).Value = "ECD";
                        ws.Cell("g" + index).Value = GetUom(uomMap, "ECD");
                        ws.Cell("h" + index).Value = (mud1 == null ? 0 : mud1.ecd);
                        ws.Cell("i" + index).Value = (mud2 == null ? 0 : mud2.ecd);
                        ws.Cell("j" + index).Value = (mud3 == null ? 0 : mud3.ecd);
                        ws.Range("M" + index + ":N" + index).Merge();

                        ws.Range("A" + (index - 16) + ":P" + index).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 16) + ":P" + index).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 16) + ":P" + index).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 16) + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        index++;

                        ws.Range("A" + index + ":P" + index).Merge();
                        ws.Range("A" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("A" + index).Value = "Bulk Materials";
                        ws.Cell("A" + index).Style.Font.FontSize = 10;
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        index++;

                        tempIndex = index;
                        ws.Cell("A" + index).Value = "Type";
                        ws.Cell("B" + index).Value = "Material Name";
                        ws.Range("B" + index + ":C" + index).Merge();
                        ws.Cell("D" + index).Value = "UOM";
                        ws.Cell("E" + index).Value = "Received";
                        ws.Range("E" + index + ":F" + index).Merge();
                        ws.Cell("G" + index).Value = "Consumed";
                        ws.Range("G" + index + ":H" + index).Merge();
                        ws.Cell("I" + index).Value = "Returned";
                        ws.Range("I" + index + ":J" + index).Merge();
                        ws.Cell("K" + index).Value = "Adjust";
                        ws.Range("K" + index + ":L" + index).Merge();
                        ws.Cell("M" + index).Value = "Ending";
                        ws.Range("M" + index + ":P" + index).Merge();
                        ws.Range("A" + index + ":p" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        foreach (var item in drillingBulk.Data)
                        {
                            index++;
                            ws.Cell("A" + index).Value = "";
                            ws.Cell("B" + index).Value = item.item_name;
                            ws.Range("B" + index + ":C" + index).Merge();
                            ws.Cell("D" + index).Value = "";
                            ws.Cell("E" + index).Value = item.received;
                            ws.Range("E" + index + ":F" + index).Merge();
                            ws.Cell("G" + index).Value = item.consumed;
                            ws.Range("G" + index + ":H" + index).Merge();
                            ws.Cell("I" + index).Value = "";
                            ws.Range("I" + index + ":J" + index).Merge();
                            ws.Cell("K" + index).Value = item.previous;
                            ws.Range("K" + index + ":L" + index).Merge();
                            ws.Cell("M" + index).Value = item.balance;
                            ws.Range("M" + index + ":P" + index).Merge();
                        }
                        ws.Range("A" + tempIndex + ":P" + index).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + tempIndex + ":P" + index).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + tempIndex + ":P" + index).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + tempIndex + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        index++;

                        ws.Range("A" + index + ":P" + index).Merge();
                        ws.Range("A" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("A" + index).Style.Font.FontSize = 10;
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Cell("A" + index).Value = "HSE";
                        index++;
                        ws.Range("B" + index + ":P" + index).Merge();
                        ws.Range("A" + index + ":P" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("B" + index).Value = "Safety Flag";
                        ws.Cell("A" + index).Value = "Stop";
                        index++;
                        ws.Range("A" + index + ":P" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("A" + index).Value = "Cards";
                        ws.Cell("B" + index).Value = "LTA";
                        ws.Range("B" + index + ":C" + index).Merge();
                        ws.Cell("D" + index).Value = "Spill";
                        ws.Range("D" + index + ":E" + index).Merge();
                        ws.Cell("F" + index).Value = "H2S Test";
                        ws.Range("F" + index + ":G" + index).Merge();
                        ws.Cell("H" + index).Value = "HSE Mtg";
                        ws.Range("H" + index + ":I" + index).Merge();
                        ws.Cell("J" + index).Value = "Kick-Trip";
                        ws.Range("J" + index + ":K" + index).Merge();
                        ws.Cell("L" + index).Value = "Kick-Drill";
                        ws.Range("L" + index + ":M" + index).Merge();
                        ws.Cell("N" + index).Value = "Fire";
                        ws.Range("N" + index + ":P" + index).Merge();
                        index++;
                        ws.Cell("A" + index).Value = detail.hsse_stop_cards;
                        ws.Cell("B" + index).Value = "";
                        ws.Range("B" + index + ":C" + index).Merge();
                        ws.Cell("D" + index).Value = "";
                        ws.Range("D" + index + ":E" + index).Merge();
                        ws.Cell("F" + index).Value = detail.hsse_dh2s != null ? detail.hsse_dh2s.Value.ToString("dd/MM/yyyy") : "N";
                        ws.Range("F" + index + ":G" + index).Merge();
                        ws.Cell("H" + index).Value = detail.hsse_safety_meeting;
                        ws.Range("H" + index + ":I" + index).Merge();
                        ws.Cell("J" + index).Value = "";
                        ws.Range("J" + index + ":K" + index).Merge();
                        ws.Cell("L" + index).Value = detail.hsse_dkick != null ? detail.hsse_dkick.Value.ToString("dd/MM/yyyy") : "N";
                        ws.Range("L" + index + ":M" + index).Merge();
                        ws.Cell("N" + index).Value = detail.hsse_dfire != null ? detail.hsse_dfire.Value.ToString("dd/MM/yyyy") : "N";
                        ws.Range("N" + index + ":P" + index).Merge();
                        index++;
                        ws.Range("A" + index + ":P" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("A" + index).Value = "Accidents/Incidents Time";
                        ws.Range("A" + index + ":C" + index).Merge();
                        ws.Cell("D" + index).Value = "Incident";
                        ws.Range("D" + index + ":G" + index).Merge();
                        ws.Cell("H" + index).Value = "Type";
                        ws.Range("H" + index + ":I" + index).Merge();
                        ws.Cell("J" + index).Value = "Comments";
                        ws.Range("J" + index + ":P" + index).Merge();
                        index++;
                        ws.Cell("A" + index).Value = "";
                        ws.Range("A" + index + ":C" + index).Merge();
                        ws.Cell("D" + index).Value = "";
                        ws.Range("D" + index + ":G" + index).Merge();
                        ws.Cell("H" + index).Value = "";
                        ws.Range("H" + index + ":I" + index).Merge();
                        ws.Cell("J" + index).Value = detail.hsse_description;
                        ws.Range("J" + index + ":P" + index).Merge();

                        ws.Range("A" + (index - 4) + ":P" + index).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 4) + ":P" + index).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 4) + ":P" + index).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 4) + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        index++;

                        ws.Range("A" + index + ":P" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("A" + index).Value = "Directional Survey";
                        ws.Cell("A" + index).Style.Font.FontSize = 10;
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        ws.Range("A" + index + ":P" + index).Merge();
                        ws.Range("A" + index + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        index++;

                        ws.Cell("A" + index).Value = "MD (ft)";
                        ws.Range("A" + index + ":B" + index).Merge();
                        ws.Cell("C" + index).Value = "Incl. (deg)";
                        ws.Cell("D" + index).Value = "Azm (deg)";
                        ws.Cell("E" + index).Value = "MD (ft)";
                        ws.Range("E" + index + ":F" + index).Merge();
                        ws.Cell("G" + index).Value = "Incl. (deg)";
                        ws.Cell("H" + index).Value = "Azm (deg)";
                        ws.Cell("I" + index).Value = "MD (ft)";
                        ws.Range("I" + index + ":J" + index).Merge();
                        ws.Cell("K" + index).Value = "Incl. (deg)";
                        ws.Cell("L" + index).Value = "Azm (deg)";
                        ws.Cell("M" + index).Value = "MD (ft)";
                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("O" + index).Value = "Incl. (deg)";
                        ws.Cell("P" + index).Value = "Azm (deg)";
                        ws.Range("A" + index + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                        for (int i = 0; i <= 4; i++)
                        {
                            index++;
                            ws.Cell("A" + index).SetValue<string>(deviations.Count > i ? deviations[i].measured_depth.Value.ToString() : "");
                            ws.Range("A" + index + ":B" + index).Merge();
                            ws.Cell("C" + index).SetValue<string>(deviations.Count > i ? deviations[i].inclination.Value.ToString() : "");
                            ws.Cell("D" + index).SetValue<string>(deviations.Count > i ? deviations[i].azimuth.Value.ToString() : "");
                            ws.Cell("E" + index).SetValue<string>(deviations.Count > (i + 5) ? deviations[(i + 5)].measured_depth.Value.ToString() : "");
                            ws.Range("E" + index + ":F" + index).Merge();
                            ws.Cell("G" + index).SetValue<string>(deviations.Count > (i + 5) ? deviations[(i + 5)].inclination.Value.ToString() : "");
                            ws.Cell("H" + index).SetValue<string>(deviations.Count > (i + 5) ? deviations[(i + 5)].azimuth.Value.ToString() : "");
                            ws.Cell("I" + index).SetValue<string>(deviations.Count > (i + 10) ? deviations[(i + 10)].measured_depth.Value.ToString() : "");
                            ws.Range("I" + index + ":J" + index).Merge();
                            ws.Cell("K" + index).SetValue<string>(deviations.Count > (i + 10) ? deviations[(i + 10)].inclination.Value.ToString() : "");
                            ws.Cell("L" + index).SetValue<string>(deviations.Count > (i + 10) ? deviations[(i + 10)].azimuth.Value.ToString() : "");
                            ws.Cell("M" + index).SetValue<string>(deviations.Count > (i + 15) ? deviations[(i + 15)].measured_depth.Value.ToString() : "");
                            ws.Range("M" + index + ":N" + index).Merge();
                            ws.Cell("O" + index).SetValue<string>(deviations.Count > (i + 15) ? deviations[(i + 15)].measured_depth.Value.ToString() : "");
                            ws.Cell("P" + index).SetValue<string>(deviations.Count > (i + 15) ? deviations[(i + 15)].measured_depth.Value.ToString() : "");
                        }
                        //index += 4;
                        ws.Range("A" + index + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        ws.Range("A" + (index - 4) + ":P" + index).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 4) + ":P" + index).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 4) + ":P" + index).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 4) + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Medium;
                        index++;

                        ws.Cell("A" + index).Value = "Personnel";
                        ws.Range("A" + index + ":G" + index).Merge();
                        ws.Cell("H" + index).Value = "Pumps";
                        ws.Range("H" + index + ":P" + index).Merge();
                        ws.Range("A" + index + ":p" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Range("A" + index + ":p" + index).Style.Font.FontSize = 10;
                        ws.Range("A" + index + ":p" + index).Style.Font.Bold = true;
                        index++;

                        ws.Cell("A" + index).Value = "Company";
                        ws.Range("A" + index + ":B" + index).Merge();
                        ws.Cell("C" + index).Value = "People";
                        ws.Cell("D" + index).Value = "Company";
                        ws.Range("D" + index + ":F" + index).Merge();
                        ws.Cell("G" + index).Value = "People";
                        ws.Cell("H" + index).Value = "Pump Name";
                        ws.Range("H" + index + ":J" + index).Merge();
                        ws.Range("K" + index + ":L" + index).Merge();
                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Range("O" + index + ":P" + index).Merge();
                        index++;

                        var indexTemp = 0;
                        labels = new string[,] {
                            { "Time", "hh:mm" },
                            {"Slow Speed?", "y/n" },
                            {"Circulate", "" },
                            {"Strokes", "SPM" },
                            {"Pressure", "psi" },
                            {"Liner Size", "in" },
                            {"Efficiency", "%" },
                        };

                        for (int i = 0; i < 8; i++)
                        {
                            ws.Cell("A" + index).Value = personels.Count > i ? personels[i].company : "";
                            ws.Range("A" + index + ":B" + index).Merge();
                            ws.Cell("C" + index).Value = personels.Count > i ? personels[i].job_title : "";
                            ws.Cell("D" + index).Value = personels.Count > (i + 8) ? personels[i + 8].company : "";
                            ws.Range("D" + index + ":F" + index).Merge();
                            ws.Cell("G" + index).Value = personels.Count > (i + 8) ? personels[i + 8].job_title : "";
                            ws.Cell("H" + index).Value = i < 7 ? labels[indexTemp, 0] : "";
                            ws.Cell("J" + index).Value = i < 7 ? labels[indexTemp, 1] : "";
                            ws.Cell("K" + index).Value = "";
                            ws.Cell("M" + index).Value = "";
                            ws.Cell("O" + index).Value = "";
                            index++;
                            indexTemp++;
                        }

                        ws.Range("A" + (index - indexTemp - 2) + ":P" + (index - 1)).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - indexTemp - 2) + ":P" + (index - 1)).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - indexTemp - 2) + ":P" + (index - 1)).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - indexTemp - 2) + ":P" + (index - 1)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 1) + ":P" + (index - 1)).Style.Border.BottomBorder = XLBorderStyleValues.Medium;

                        ws.Range("A" + index + ":P" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        ws.Cell("A" + index).Value = "Weather";
                        ws.Range("A" + index + ":P" + index).Merge();
                        ws.Cell("A" + index).Style.Font.FontSize = 10;
                        ws.Cell("A" + index).Style.Font.Bold = true;
                        index++;

                        ws.Cell("A" + index).Value = "Temperature (degF)";
                        ws.Range("A" + index + ":B" + index).Merge();
                        ws.Cell("C" + index).Value = "Chill";
                        ws.Cell("D" + index).Value = "Wind";
                        ws.Range("D" + index + ":H" + index).Merge();
                        ws.Cell("I" + index).Value = "Barometric";
                        ws.Range("I" + index + ":J" + index).Merge();
                        ws.Cell("K" + index).Value = "Sea Condition";
                        ws.Range("K" + index + ":L" + index).Merge();
                        ws.Cell("M" + index).Value = "Road Condition";
                        ws.Range("M" + index + ":N" + (index + 1)).Merge();
                        ws.Cell("O" + index).Value = "Visibility";
                        ws.Range("O" + index + ":P" + (index + 1)).Merge();
                        index++;

                        ws.Cell("A" + index).Value = "High";
                        ws.Cell("B" + index).Value = "Low";
                        ws.Cell("C" + index).Value = "Factor";
                        ws.Cell("D" + index).Value = "Speed (" + GetUom(uomMap, "Wind Speed") + ")";
                        ws.Range("D" + index + ":E" + index).Merge();
                        ws.Cell("F" + index).Value = "Direction (" + GetUom(uomMap, "Rig Heading") + ")";
                        ws.Range("F" + index + ":H" + index).Merge();
                        ws.Cell("I" + index).Value = "Pres(" + GetUom(uomMap, "Barometer") + ")";
                        ws.Range("I" + index + ":J" + index).Merge();
                        ws.Cell("K" + index).Value = "Wave Height";
                        ws.Cell("L" + index).Value = "Current Speed";
                        ws.Range("A" + (index - 2) + ":P" + index).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center).Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                        index++;

                        ws.Cell("A" + index).Value = detail.weather_temperature;
                        ws.Cell("B" + index).Value = detail.weather_temperature;
                        ws.Cell("C" + index).Value = "";
                        ws.Cell("D" + index).Value = detail.weather_wind_speed;
                        ws.Range("D" + index + ":E" + index).Merge();
                        ws.Cell("F" + index).Value = detail.weather_wind_direction;
                        ws.Range("F" + index + ":H" + index).Merge();
                        ws.Cell("I" + index).Value = "";
                        ws.Range("I" + index + ":J" + index).Merge();
                        ws.Cell("K" + index).Value = detail.weather_height;
                        ws.Cell("L" + index).Value = "";
                        ws.Cell("M" + index).Value = "";
                        ws.Range("M" + index + ":N" + index).Merge();
                        ws.Cell("O" + index).Value = "";
                        ws.Range("O" + index + ":P" + index).Merge();

                        ws.Range("A" + (index - 3) + ":P" + index).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 3) + ":P" + index).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 3) + ":P" + index).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + (index - 3) + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        ws.Range("A" + index + ":P" + index).Style.Border.BottomBorder = XLBorderStyleValues.Medium;


                        for (int i = 1; i <= 17; i++)
                        {
                            ws.Column(i).AdjustToContents();
                        }

                        workbook.SaveAs(path);
                    }

                    result.Data = nameFile;
                    result.Status.Success = true;
                    result.Status.Message = "";
                }
                catch (Exception e)
                {

                    result.Data = "";
                    result.Status.Success = false;
                    appLogger.Error(e);
                }
                return result;
            });
            return response;
        }

        private string GetUom(List<vw_well_object_uom_map> data, string keyword)
        {
            var searchResult = data.Where(x => x.object_name == keyword);
            if (searchResult.Count() > 0)
            {
                return searchResult.First().uom_code;
            }
            return "";
        }
    }

}

