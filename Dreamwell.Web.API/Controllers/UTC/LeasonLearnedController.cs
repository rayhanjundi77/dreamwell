﻿using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dreamwell.Infrastructure;
using System.Threading.Tasks;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;
using DocumentFormat.OpenXml.EMMA;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/utc/LeasonLearned")]
    public class LeasonLearnedController : ApiBaseController
    {
        [HttpPost]
        [Route("GetAnalyze")]
        public Task<ApiResponse<List<vw_iadc_learned_interval>>> GetAnalyze(LeasonLearnedViewModel data)
        {
            var services = new LeasonLearnedServices(this.dataContext);
            var response = Task.Run<ApiResponse<List<vw_iadc_learned_interval>>>(() =>
            {
                var result = new ApiResponse<List<vw_iadc_learned_interval>>();
                try
                {
                    result = services.GetAnalyze(data);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [HttpPost]
        [Route("GetAnalyzeDetail")]
        public async Task<ApiResponse<List<AnalyzeDataDetailsViewModel>>> GetAnalyzeDetail(LeasonLearnedViewModel data)
        {
            var services = new LeasonLearnedServices(this.dataContext);
            var services_drilling = new DrillingServices(this.dataContext);
            var services_uom = new WellObjectUomMapServices(this.dataContext);

            var response = await Task.Run<ApiResponse<List<AnalyzeDataDetailsViewModel>>>(() =>
            {
                var result = new ApiResponse<List<AnalyzeDataDetailsViewModel>>();
                result.Data = new List<AnalyzeDataDetailsViewModel>();
                try
                {
                    if (data.well_id.Count > 0)
                    {
                        var wellIds = "";
                        foreach (var item in data.well_id.Select((value, i) => new { i, value }))
                        {
                            wellIds += "'" + item.value + "'" + (item.i != data.well_id.Count - 1 ? "," : "");
                        }

                        var uoms = services_uom.GetViewAll($" AND r.is_active = 1 and r.object_name = 'Depth' AND r.well_id in ({wellIds})");

                        var time_versus_cost = services.GetIadcLearnedDetail(wellIds).Data;
                        var drillings = services_drilling.GetViewAll($" AND r.well_id in ({wellIds}) ORDER BY r.drilling_date ASC ");
                        foreach (var well_id in data.well_id)
                        {
                            var uom = uoms.Where(x => x.well_id == well_id).Select(x => x.uom_code).First();
                            drillings.Where(x => x.well_id == well_id).ToList().ForEach(x =>
                            {
                                if (uom == "ft" && data.uom_select == "m")
                                {
                                    x.current_depth_md = Math.Round((x.current_depth_md ?? 0) / 3.281m, 2);
                                }
                                else if (uom == "m" && data.uom_select == "ft")
                                {
                                    x.current_depth_md = Math.Round((x.current_depth_md ?? 0) * 3.281m, 2);
                                }
                            });
                            var r = new AnalyzeDataDetailsViewModel();
                            r.time_versus_cost = time_versus_cost.Where(x => x.well_id == well_id).ToList();
                            r.drilling = drillings.Where(x => x.well_id == well_id).ToList();
                            result.Data.Add(r);
                        }
                    }
                    result.Status.Success = true;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }
    }
}

