﻿using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dreamwell.Infrastructure;
using System.Threading.Tasks;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using System.Security.Policy;
using System.Drawing.Drawing2D;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/utc/dashboard")]
    public class DashboardController : ApiBaseController
    {
        [HttpPost]
        [Route("total/projectarea")]
        public async Task<ApiResponse> GetTotalProjectArea()
        {
            var services = new DashboardServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.GetTotalProjectArea();
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("getRegion")]
        [HttpPost]
        public async Task<ApiResponse<List<region>>> getRegion()
        {
            var services = new BusinessUnitServices(this.dataContext);
            Console.WriteLine("getRegion API called."); // Log API call

            //var response = await Task.Run<ApiResponse<List<region>>>(() =>
            //{
            //    var result = new ApiResponse<List<region>>();
            //    var record = services.getRegion();

            //    Console.WriteLine("Service call returned data."); // Log service call result
            //    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}"); // Log success status and count of records

            //    result.Status.Success = (record != null);
            //    result.Data = record.Data;

            //    return result;
            //});

            ApiResponse<List<region>> response;

            // Check if the user is an administrator
            if (this.dataContext.IsSystemAdministrator)
            {
                // Admin access - no business unit filter needed
                response = await Task.Run(() => services.getRegion());
                Console.WriteLine("Admin access granted, no business unit filter applied.");
            }
            else
            {
                // Non-admin access - apply filter for the user's primary business unit
                var businessUnitId = this.dataContext.PrimaryBusinessUnitId;
                response = await Task.Run(() => services.getRegion(businessUnitId));
                Console.WriteLine($"Non-admin access with business unit filter: {businessUnitId}");
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return response;
        }

        //[Route("chartRegion")]
        //[HttpPost]
        //public async Task<ApiResponse<List<chart_region>>> chartRegion()
        //{
        //    var services = new BusinessUnitServices(this.dataContext);
        //    Console.WriteLine("getRegion API called."); // Log API call

        //    var response = await Task.Run<ApiResponse<List<chart_region>>>(() =>
        //    {
        //        var result = new ApiResponse<List<chart_region>>();
        //        var record = services.getchartRegion();

        //        Console.WriteLine("Service call returned data."); // Log service call result
        //        Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}"); // Log success status and count of records

        //        result.Status.Success = (record != null);
        //        result.Data = record.Data;

        //        return result;
        //    });

        //    Console.WriteLine("API response is ready."); // Log before returning response
        //    return response;
        //}

        [Route("chartRegion")]
        [HttpPost]
        public async Task<ApiResponse<List<chart_region>>> chartRegion()
        {
            var services = new BusinessUnitServices(this.dataContext);
            Console.WriteLine("chartRegion API called."); // Log API call

            ApiResponse<List<chart_region>> response;

            // Check if the user is an administrator
            if (this.dataContext.IsSystemAdministrator)
            {
                // Admin access - no business unit filter needed
                response = await Task.Run(() => services.getchartRegion());
                Console.WriteLine("Admin access granted, no business unit filter applied.");
            }
            else
            {
                // Non-admin access - apply filter for the user's primary business unit
                var businessUnitId = this.dataContext.PrimaryBusinessUnitId;
                response = await Task.Run(() => services.getchartRegion(businessUnitId));
                Console.WriteLine($"Non-admin access with business unit filter: {businessUnitId}");
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return response;
        }


        [Route("getActivityDate/{startDate}/{endDate}")]
        [HttpGet]
        public async Task<ApiResponse<List<DrillingSummary>>> GetActivityDate(DateTime startDate, DateTime endDate)
        {
            var services = new BusinessUnitServices(this.dataContext);
            var response = new ApiResponse<List<DrillingSummary>>();

            try
            {
                // Run the task asynchronously to fetch data from the services
                var record = await Task.Run(() => services.getActivityDate(startDate, endDate));

                // Ensure the record is not null before accessing the data
                if (record != null && record.Data != null)
                {
                    response.Status.Success = true;
                    response.Data = record.Data;
                    response.Status.Message = $"Fetched {record.Data.Count} records.";
                }
                else
                {
                    // Handle the case where no data is returned
                    response.Status.Success = false;
                    response.Status.Message = "No data found for the provided date range.";
                    response.Data = new List<DrillingSummary>(); // Return an empty list instead of null
                }
            }
            catch (Exception ex)
            {
                // Log the error and return an error message
                appLogger.Error(ex);
                response.Status.Success = false;
                response.Status.Message = $"An error occurred: {ex.Message}";
            }

            return response;  // Return the API response
        }



        // Zona
        [Route("getZona/{parentId}")]
        [HttpPost]
        public async Task<ApiResponse<List<zona>>> GetZona(string parentId)
        {
            var result = new ApiResponse<List<zona>>();
            var services = new BusinessUnitServices(this.dataContext);

            try
            {
                Console.WriteLine($"getZona API called with parentId: {parentId}"); // Log API call

                // Call the service method directly
                var record = services.getZona(parentId);

                if (record == null)
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data returned from service.";
                    Console.WriteLine("No data returned from service.");
                }
                else
                {
                    // Log the service call result
                    Console.WriteLine("Service call returned data.");
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}");

                    // Copy the service response to the result object
                    result.Status.Success = record.Status.Success;
                    result.Data = record.Data;
                    result.Status.Message = record.Status.Message;
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine($"An error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return await Task.FromResult(result); // Explicit async return
        }

        [Route("chartZona/{parentId}")]
        [HttpPost]
        public async Task<ApiResponse<List<chart_zona>>> chartZona(string parentId)
        {
            var result = new ApiResponse<List<chart_zona>>();
            try
            {
                var services = new BusinessUnitServices(this.dataContext);
                Console.WriteLine("chartZona API called."); // Log API call

                // Call the service method directly without Task.Run
                var record = await Task.FromResult(services.getchartZona(parentId));

                if (record != null && record.Status.Success)
                {
                    result.Status.Success = true;
                    result.Data = record.Data;
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}"); // Log success and count
                }
                else
                {
                    result.Status.Success = false;
                    result.Status.Message = "Failed to retrieve data.";
                    Console.WriteLine("Service returned no data or failed.");
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return result;
        }

        [Route("chartPtNptRegion/{parentId}")]
        [HttpPost]
        public async Task<ApiResponse<List<chart_region>>> ptNptZona(string parentId)
        {
            var services = new BusinessUnitServices(this.dataContext);
            Console.WriteLine("getRegion API called."); // Log API call

            var response = await Task.Run<ApiResponse<List<chart_region>>>(() =>
            {
                var result = new ApiResponse<List<chart_region>>();
                var record = services.ptNptZona(parentId);

                if (record != null)
                {
                    Console.WriteLine("Service call returned data."); // Log service call result
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}"); // Log success status and count of records

                    result.Status.Success = record.Status.Success;
                    result.Data = record.Data;
                }
                else
                {
                    result.Status.Success = false;
                    result.Status.Message = "Record is null.";
                    Console.WriteLine("Record is null.");
                }

                return result;
            });

            Console.WriteLine("API response is ready."); // Log before returning response
            return response;
        }

        //Field
        [Route("getField/{unitId}")]
        [HttpPost]
        public async Task<ApiResponse<List<dashboard_field>>> GetField(string unitId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            var services = new BusinessUnitServices(this.dataContext);

            try
            {
                Console.WriteLine($"getZona API called with fieldId: {unitId}"); // Log API call

                // Call the service method directly
                var record = services.getField(unitId);

                if (record == null)
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data returned from service.";
                    Console.WriteLine("No data returned from service.");
                }
                else
                {
                    // Log the service call result
                    Console.WriteLine("Service call returned data.");
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}");

                    // Copy the service response to the result object
                    result.Status.Success = record.Status.Success;
                    result.Data = record.Data;
                    result.Status.Message = record.Status.Message;
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine($"An error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return await Task.FromResult(result); // Explicit async return
        }

        [Route("chartField/{unitId}")]
        [HttpPost]
        public async Task<ApiResponse<List<chart_zona>>> chartField(string unitId)
        {
            var result = new ApiResponse<List<chart_zona>>();
            try
            {
                var services = new BusinessUnitServices(this.dataContext);
                Console.WriteLine($"chartField API called with unitId: {unitId}"); // Log the API call

                // Call the service method directly
                var record = services.getchartField(unitId);

                if (record != null && record.Status.Success && record.Data != null && record.Data.Count > 0)
                {
                    result.Status.Success = true;
                    result.Data = record.Data;
                    result.Status.Message = "Data successfully retrieved.";
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}"); // Log success and count
                }
                else
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data available or service failed.";
                    result.Data = new List<chart_zona>(); // Return empty list if no data found
                    Console.WriteLine("Service returned no data or failed.");
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return await Task.FromResult(result); // Explicit async return
        }

        [Route("getPieField/{unitId}")]
        [HttpPost]
        public async Task<ApiResponse<List<dashboard_field>>> GetPieField(string unitId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            var services = new BusinessUnitServices(this.dataContext);

            try
            {
                Console.WriteLine($"GetPieField API called with unitId: {unitId}"); // Log API call

                // Validate unitId
                if (string.IsNullOrWhiteSpace(unitId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid unitId parameter.";
                    Console.WriteLine("Invalid unitId provided.");
                    return result;
                }

                // Call the service method directly
                var record = await Task.Run(() => services.getpieField(unitId)); // Simulate async call

                if (record?.Data == null || !record.Data.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data returned from service.";
                    Console.WriteLine("No data returned from service.");
                }
                else
                {
                    // Log the service call result
                    Console.WriteLine("Service call returned data.");
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data.Count}");

                    // Copy the service response to the result object
                    result.Status.Success = record.Status.Success;
                    result.Data = record.Data;
                    result.Status.Message = record.Status.Message;
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine($"An error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return result; // Explicit async return
        }

        [Route("getNptField/{unitId}")]
        [HttpPost]
        public async Task<ApiResponse<List<dashboard_field>>> GetNptField(string unitId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            var services = new BusinessUnitServices(this.dataContext);

            try
            {
                Console.WriteLine($"getnptField API called with unitId: {unitId}"); // Log API call

                // Validate unitId
                if (string.IsNullOrWhiteSpace(unitId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid unitId parameter.";
                    Console.WriteLine("Invalid unitId provided.");
                    return result;
                }

                // Call the service method directly
                var record = await Task.Run(() => services.getnptField(unitId)); // Simulate async call

                if (record?.Data == null || !record.Data.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data returned from service.";
                    Console.WriteLine("No data returned from service.");
                }
                else
                {
                    // Log the service call result
                    Console.WriteLine("Service call returned data.");
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data.Count}");

                    // Copy the service response to the result object
                    result.Status.Success = record.Status.Success;
                    result.Data = record.Data;
                    result.Status.Message = record.Status.Message;
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine($"An error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return result; // Explicit async return
        }

        // Well
        [Route("getWell/{FieldId}")]
        [HttpPost]
        public async Task<ApiResponse<List<dashboard_field>>> GetWell(string FieldId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            var services = new BusinessUnitServices(this.dataContext);

            try
            {
                Console.WriteLine($"getZona API called with fieldId: {FieldId}"); // Log API call

                // Call the service method directly
                var record = services.getWell(FieldId);

                if (record == null)
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data returned from service.";
                    Console.WriteLine("No data returned from service.");
                }
                else
                {
                    // Log the service call result
                    Console.WriteLine("Service call returned data.");
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}");

                    // Copy the service response to the result object
                    result.Status.Success = record.Status.Success;
                    result.Data = record.Data;
                    result.Status.Message = record.Status.Message;
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine($"An error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return await Task.FromResult(result); // Explicit async return
        }

        [Route("chartWell/{FieldId}")]
        [HttpPost]
        public async Task<ApiResponse<List<chart_zona>>> chartWell(string FieldId)
        {
            var result = new ApiResponse<List<chart_zona>>();
            try
            {
                var services = new BusinessUnitServices(this.dataContext);
                Console.WriteLine($"chartField API called with FieldId: {FieldId}"); // Log the API call

                // Call the service method directly
                var record = services.getchartWell(FieldId);

                if (record != null && record.Status.Success && record.Data != null && record.Data.Count > 0)
                {
                    result.Status.Success = true;
                    result.Data = record.Data;
                    result.Status.Message = "Data successfully retrieved.";
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data?.Count ?? 0}"); // Log success and count
                }
                else
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data available or service failed.";
                    result.Data = new List<chart_zona>(); // Return empty list if no data found
                    Console.WriteLine("Service returned no data or failed.");
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine("Error occurred: " + ex.Message);
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return await Task.FromResult(result); // Explicit async return
        }

        [Route("getPieWell/{FieldId}")]
        [HttpPost]
        public async Task<ApiResponse<List<dashboard_field>>> GetPieWell(string FieldId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            var services = new BusinessUnitServices(this.dataContext);

            try
            {
                Console.WriteLine($"GetPieWell API called with FieldId: {FieldId}"); // Log API call

                // Validate FieldId
                if (string.IsNullOrWhiteSpace(FieldId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid FieldId parameter.";
                    Console.WriteLine("Invalid FieldId provided.");
                    return result;
                }

                // Call the service method directly
                var record = await Task.Run(() => services.getpieWell(FieldId)); // Simulate async call

                if (record?.Data == null || !record.Data.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data returned from service.";
                    Console.WriteLine("No data returned from service.");
                }
                else
                {
                    // Log the service call result
                    Console.WriteLine("Service call returned data.");
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data.Count}");

                    // Copy the service response to the result object
                    result.Status.Success = record.Status.Success;
                    result.Data = record.Data;
                    result.Status.Message = record.Status.Message;
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine($"An error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return result; // Explicit async return
        }

        [Route("getNptWell/{FieldId}")]
        [HttpPost]
        public async Task<ApiResponse<List<dashboard_field>>> GetNptWell(string FieldId)
        {
            var result = new ApiResponse<List<dashboard_field>>();
            var services = new BusinessUnitServices(this.dataContext);

            try
            {
                Console.WriteLine($"getnptWell API called with FieldId: {FieldId}"); // Log API call

                // Validate FieldId
                if (string.IsNullOrWhiteSpace(FieldId))
                {
                    result.Status.Success = false;
                    result.Status.Message = "Invalid FieldId parameter.";
                    Console.WriteLine("Invalid FieldId provided.");
                    return result;
                }

                // Call the service method directly
                var record = await Task.Run(() => services.getnptWell(FieldId)); // Simulate async call

                if (record?.Data == null || !record.Data.Any())
                {
                    result.Status.Success = false;
                    result.Status.Message = "No data returned from service.";
                    Console.WriteLine("No data returned from service.");
                }
                else
                {
                    // Log the service call result
                    Console.WriteLine("Service call returned data.");
                    Console.WriteLine($"Success: {record.Status.Success}, Records Count: {record.Data.Count}");

                    // Copy the service response to the result object
                    result.Status.Success = record.Status.Success;
                    result.Data = record.Data;
                    result.Status.Message = record.Status.Message;
                }
            }
            catch (Exception ex)
            {
                // Log any exceptions that occur during execution
                Console.WriteLine($"An error occurred: {ex.Message}");
                result.Status.Success = false;
                result.Status.Message = $"An error occurred: {ex.Message}";
            }

            Console.WriteLine("API response is ready."); // Log before returning response
            return result; // Explicit async return
        }
    }
}

