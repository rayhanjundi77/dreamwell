﻿using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dreamwell.Infrastructure;
using System.Threading.Tasks;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/utc/FinalReport")]
    public class FinalReportController : ApiController
    {
        [HttpGet]
        [Route("GetDrilling")]
        public async Task<ApiResponse> GetDrilling([FromUri] string wellId)
        {
            var userContext = new DataAccess.DataContext(BusinessLogic.Core.SysConfig.UserSetup.sysAdminId);
            var services = new DrillingServices(userContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    //result = services.Getvi
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

    }
}

