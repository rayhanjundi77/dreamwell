﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using PetaPoco;
using Newtonsoft.Json;

namespace Axa.Web.API.Controllers
{
    [RoutePrefix("api/core/UOM")]

    public class UOMController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_uom>> GetPage(long page, long itemsPerPage)
        {
            var services = new UomServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_uom>>(() =>
            {
                var result = new ApiResponsePage<vw_uom>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        //[Route("lookup")]
        //[HttpPost]
        //public async Task<ApiResponsePage<vw_uom>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        //{
        //    appLogger.Info("GetPage method called");
        //    appLogger.Info($"Received MarvelDataSourceRequest: {JsonConvert.SerializeObject(marvelDataSourceRequest)}");

        //    // Decode %25 to % in the request
        //    if (marvelDataSourceRequest != null && marvelDataSourceRequest.Filter != null && marvelDataSourceRequest.Filter.Filters != null)
        //    {
        //        foreach (var filter in marvelDataSourceRequest.Filter.Filters)
        //        {
        //            if (filter.Value != null)
        //            {
        //                appLogger.Info($"Original Filter Value: {filter.Value}");
        //                filter.Value = filter.Value.Replace("%25", "%");
        //                appLogger.Info($"Decoded Filter Value: {filter.Value}");
        //            }
        //        }
        //    }

        //    var services = new UomServices(this.dataContext);

        //    // If necessary, escape '%' for SQL LIKE clause
        //    string searchValue = marvelDataSourceRequest.Filter.Value.Replace("%", "[%]");

        //    // Use the search value in your service method
        //    var response = await Task.Run<ApiResponsePage<vw_uom>>(() =>
        //    {
        //        var result = new ApiResponsePage<vw_uom>();
        //        var data = services.Lookup(marvelDataSourceRequest, searchValue);
        //        result.CurrentPage = data.CurrentPage;
        //        result.TotalPages = data.TotalPages;
        //        result.TotalItems = data.TotalItems;
        //        result.ItemsPerPage = data.ItemsPerPage;
        //        result.Items = data.Items;
        //        return result;
        //    });

        //    appLogger.Info("Returning response");
        //    return response;
        //}


        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_uom>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new UomServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_uom>>(() =>
            {
                var result = new ApiResponsePage<vw_uom>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup/uomLength")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_uom>> GetPageLength([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new UomServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_uom>>(() =>
            {
                var result = new ApiResponsePage<vw_uom>();
                var data = services.LookupLength(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup/UomCategory")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_uom_category>> LookupUomCategory([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new UomCategoryServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_uom_category>>(() =>
            {
                var result = new ApiResponsePage<vw_uom_category>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("GetStandartInternational")]
        [HttpGet]
        public async Task<ApiResponse<List<uom>>> GetStandartInternational()
        {
            var services = new UomServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<uom>>>(() =>
            {
                var result = new ApiResponse<List<uom>>();
                var record = services.GetAll(" AND is_std_international = 1 ");
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_uom>> GetDetail(string recordId)
        {
            var services = new UomServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_uom>>(() =>
            {
                var result = new ApiResponse<vw_uom>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail/ObjectMapper")]
        [HttpGet]
        public async Task<ApiResponse<vw_object_uom_map>> GetDetailObjectMapper(string recordId)
        {
            var services = new ObjectUomMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_object_uom_map>>(() =>
            {
                var result = new ApiResponse<vw_object_uom_map>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }



        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(uom record)
        {
            var services = new UomServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("save/ObjectMapper")]
        [HttpPost]
        public async Task<ApiResponse> SaveObjectMapper(object_uom_map record)
        {
            var services = new ObjectUomMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new UomServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new UomServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_uom>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
            //var services = new UomServices(this.dataContext);
            //var response = await Task.Run<DataTablesResponse>(() =>
            //{
            //    DataTablesResponse result = new DataTablesResponse(0, new List<vw_uom>(), 0, 0);
            //    var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);

            //    if (dataTablesRequest.Search != null && !string.IsNullOrEmpty(dataTablesRequest.Search.Value))
            //    {
            //        appLogger.Info($"Original Search Value: {dataTablesRequest.Search.Value}");
            //        dataTablesRequest.Search.Value = dataTablesRequest.Search.Value.Replace("%25", "%"); // Decode %25 back to %
            //        appLogger.Info($"Decoded Search Value: {dataTablesRequest.Search.Value}");

            //        // If necessary, escape '%' for SQL LIKE clause
            //        string searchValue = dataTablesRequest.Search.Value.Replace("%", "[%]");
            //        qry.Append(" AND (uom_code LIKE @0 OR object_name LIKE @0)", $"%{searchValue}%");
            //    }

            //    result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
            //    return result;
            //});

            //return response;
        }


        [Route("dataTable/objectMapper")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListObjectMapping([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new ObjectUomMapServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_object_uom_map>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);

                if (dataTablesRequest.Search != null && !string.IsNullOrEmpty(dataTablesRequest.Search.Value))
                {
                    appLogger.Info($"Original Search Value: {dataTablesRequest.Search.Value}");
                    dataTablesRequest.Search.Value = dataTablesRequest.Search.Value.Replace("%25", "%"); // Decode %25 back to %
                    appLogger.Info($"Decoded Search Value: {dataTablesRequest.Search.Value}");

                    // If necessary, escape '%' for SQL LIKE clause
                    string searchValue = dataTablesRequest.Search.Value.Replace("%", "[%]");
                    qry.Append(" AND (uom_code LIKE @0 OR object_name LIKE @0)", $"%{searchValue}%");
                }

                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });

            return response;
        }

        [Route("delete/objectMapper")]
        [HttpPost]
        public async Task<ApiResponse> ObjectMapDelete([FromBody] string[] ids)
        {
            var services = new ObjectUomMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

    }
}
