﻿using CommonTools;
using CommonTools.JSTree;
using DataTables.Mvc;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;
using DocumentFormat.OpenXml.Wordprocessing;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/wir")]
    public class WirController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_wir>> GetPage(long page, long itemsPerPage)
        {
            var services = new WirServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_wir>>(() =>
            {
                var result = new ApiResponsePage<vw_wir>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_wir>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new WirServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_wir>>(() =>
            {
                var result = new ApiResponsePage<vw_wir>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new WirServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_wir>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_wir>> GetDetail(string recordId)
        {
            var services = new WirServices(this.dataContext);
            var result = new ApiResponse<vw_wir>();

            try
            {
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.ToString();
            }

            return result;
        }

        [Route("listByWellId/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<WirViewModel>>> GetByWellId(string wellId)
        {
            var services = new WirServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<WirViewModel>>>(() =>
            {
                return services.GetByWellId(wellId);
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(wir record)
        {
            var services = new WirServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("approvalwir/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveApproval(approval_wir record)
        {
            var services = new WIRApprovalServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }


        [Route("{wellId}/detailapprovalwirbyId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_approval_wir>>> getapprovalwibywipid(string wellId)
        {
            var services = new WIRApprovalServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_approval_wir>>>(() =>
            {
                var result = new ApiResponse<List<vw_approval_wir>>();
                try
                {
                    result = services.getByDetail(wellId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new WirServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }
    }
}
