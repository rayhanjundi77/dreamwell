﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{ 
    [RoutePrefix("api/core/WellDeviation")]
    public class WellDeviationController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(WellViewModel record)
        {
            var services = new WellDeviationServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;

        }
        [Route("{recordId}/detailByWellId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_deviation>>> GetDataByDrillingId(string recordId)
        {
            var services = new WellDeviationServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_well_deviation>>>(() =>
            {
                var result = new ApiResponse<List<vw_well_deviation>>();
                var record = services.GetViewAll(" AND r.well_id=@0 AND r.seq > 0 ", recordId);
                result.Status.Success = (record != null);
                record.Sort((a, b) => a.seq.CompareTo(b.seq));
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/coordinateImageHC")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_well_deviation>>> GetAllCordinateWithHC(string recordId)
        {

            var services = new WellDeviationServices(this.dataContext);
            var result = new ApiResponse<List<vw_well_deviation>>();

            var record = services.GetViewAll("SELECT r.inclination, r.measure_dept ","hc.casing_name" +
                                     "FROM your_table_name as r " +
                                     "JOIN vw_well_hole_and_casing as hc ON hc.well_id = r.well_id " +
                                     "WHERE r.well_id = @0 AND r.seq > 0", recordId);

            result.Status.Success = (record != null);
            if (result.Status.Success)
            {
                record.Sort((a, b) => a.seq.CompareTo(b.seq));
                result.Data = record;
            }

            return result;
        }

    }
}
