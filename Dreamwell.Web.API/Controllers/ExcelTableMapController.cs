﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DataTables.Mvc;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/ExcelTableMap")]
    public class ExcelTableMapController : ApiBaseController
    {
        //DrillingContractorServices services;

        public ExcelTableMapController()
        {

        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_excel_table_map>> GetDetail(string recordId)
        {
            var services = new ExcelTableMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_excel_table_map>>(() =>
            {
                var result = new ApiResponse<vw_excel_table_map>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("List")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_excel_table_map>>> GetList()
        {
            var services = new ExcelTableMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_excel_table_map>>>(() =>
            {
                var result = new ApiResponse<List<vw_excel_table_map>>();
                var record = services.GetViewAll(" AND r.is_active=1 ");
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }



        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(excel_table_map record)
        {
            var services = new ExcelTableMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("lookup/PropertiesName/{appEntityId}")]
        [HttpGet]
        public async Task<ApiResponse<List<string>>> LookupPropertiesName(string appEntityId)
        {
            var services = new ExcelTableMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<string>>>(() =>
            {
                var result = new ApiResponse<List<string>>();
                result = services.GetPropertiesName(appEntityId);
                return result;
            });
            return response;
        }

        [Route("lookup/{templateId}")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_excel_table_map>> Lookup(string templateId, [FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new ExcelTableMapServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_excel_table_map>>(() =>
            {
                var result = new ApiResponsePage<vw_excel_table_map>();
                var data = services.Lookup(marvelDataSourceRequest, templateId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        #region Excel Column
        [Route("columnMap/Save")]
        [HttpPost]
        public async Task<ApiResponse> Save(excel_column_map record)
        {
            var services = new ExcelColumnMapServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("columnMap/DataType")]
        [HttpGet]
        public async Task<ApiResponse<List<string>>> GetColumnDataType()
        {
            var response = await Task.Run<ApiResponse<List<string>>>(() =>
            {
                var result = new ApiResponse<List<string>>();
                result.Status.Success = true;
                result.Data = excel_column_map.GetDataTypeCollections();
                return result;

            });
            return response;
        }

        #endregion
    }
}
