﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using Dreamwell.Infrastructure.DataTables;
using DataTables.Mvc;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Currency")]

    public class CurrencyController : ApiBaseController
    {

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_currency>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_currency>>(() =>
            {
                var result = new ApiResponsePage<vw_currency>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<currency>> GetDetail(string recordId)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponse<currency>>(() =>
            {
                var result = new ApiResponse<currency>();
                var record = services.GetById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("getAllCurrency")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_currency>>> getAllCurrency()
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_currency>>>(() =>
            {
                var result = new ApiResponse<List<vw_currency>>();
                var record = services.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(vw_currency record)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("savenewrate")]
        [HttpPost]
        public async Task<ApiResponse> SaveNewRate(currency record)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.SaveNewRate(record);
            });
            return response;
        }

        [Route("delete/{id}")]
        [HttpPost]
        public async Task<ApiResponse> delete(string id)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.DeleteById(id);
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new CurrencyServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_currency>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }
    }
}
