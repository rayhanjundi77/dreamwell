﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Web.Http;
using System.Threading.Tasks;
using Dreamwell.Infrastructure;
using Dreamwell.BusinessLogic.Core.Services;
using NLog;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Authentication")]
    public class AuthenticationController : ApiController
    {
        Logger appLogger = LogManager.GetCurrentClassLogger();

        [Route("authenticate")]
        [HttpPost]
        public async Task<ApiResponse> Authenticate([FromBody] UserCredential credential)
        {
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    var services = new Authentication();
                    var token = string.Empty;
                    services.Authenticate(credential, out token, (status, message) =>
                    {
                        result.Status.Success = status;
                        result.Status.Message = message;
                    });

                    result.Data = new
                    {
                        Token = token
                    };
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                    appLogger.Error(ex);
                }

                return result;
            });
            return response;
        }
    }
}
