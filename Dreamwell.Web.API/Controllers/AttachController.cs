﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System;
using System.Linq;
using System.IO;
using System.Web;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/Attach")]
    public class AttachController : ApiBaseController
    {

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save([FromBody] AttachWirPayload payload)
        {
            var services = new AttachWirServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(payload);
            });
            return response;
        }

        [Route("listByWellId/{wellId}")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_attach_wir>>> GetByWellId(string wellId)
        {
            var services = new AttachWirServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_attach_wir>>>(() =>
            {
                return services.GetByWellId(wellId);
            });
            return response;
        }

        [Route("upload")]
        [HttpPost]
        public async Task<ApiResponse> Upload([FromUri] string wellId)
        {
            return await AttachmentWIRUpload<attach_wir>(wellId);
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var response = new ApiResponse();
            if (ids == null || ids.Length == 0)
            {
                response.Status.Success = false;
                response.Status.Message = "No IDs provided for deletion.";
                return response;
            }

            try
            {
                var services = new AttachWirServices(this.dataContext);
                response = await Task.Run(() =>
                {
                    var result = new ApiResponse();
                    try
                    {
                        result.Status.Success = services.RemoveAll(ids);
                        if (result.Status.Success)
                        {
                            result.Status.Message = "Selected records have been deleted.";
                        }
                        else
                        {
                            result.Status.Message = "Failed to delete some or all records.";
                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error("Error in Delete: " + ex.Message);
                        result.Status.Success = false;
                        result.Status.Message = "An error occurred during deletion.";
                        return result;
                    }
                });
            }
            catch (Exception ex)
            {
                appLogger.Error("Error in Delete: " + ex.Message);
                response.Status.Success = false;
                response.Status.Message = "An unexpected error occurred.";
            }

            return response;
        }

    }
}