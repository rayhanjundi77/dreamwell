﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using Dreamwell.DataAccess;
using CommonTools;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using System.Collections.Generic;
using System;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/TeamRole")]

    public class TeamRoleController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(team_role record)
        {
            var services = new TeamRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("addTeamRole")]
        [HttpPost]
        public async Task<ApiResponse> AddTeamRole([FromBody] team_role record)
        {
            var services = new TeamRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    var isNew = false;
                    result.Status.Success = services.SaveEntity(record, ref isNew);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }

        [Route("{teamId}/member")]
        [HttpPost]
        public async Task<DataTablesResponse> GetTeamRoleMember(string teamId, [FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new TeamRoleServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_team_role>(), 0, 0);
                result = services.GetListDataTablesRoleMember(dataTablesRequest, teamId);
                return result;
            });
            return response;
        }

        [Route("removeTeamRole")]
        [HttpPost]
        public async Task<ApiResponse> RemoveTeamRole([FromBody] string[] teamroleid)
        {
            var services = new TeamRoleServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(teamroleid);
                    return result;
                }
                catch (Exception ex)
                {
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;
                }
                return result;
            });
            return response;
        }
    }
}
