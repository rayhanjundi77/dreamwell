﻿using CommonTools;
using DataTables.Mvc;
using DocumentFormat.OpenXml.EMMA;
using DocumentFormat.OpenXml.Office2010.Excel;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.Models.Entity;
using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Services;

namespace Dreamwell.Web.API.Controllers
{

    [RoutePrefix("api/core/Rkap")]
    public class RkapController : ApiBaseController
    {
        RkapRepository repository;

        public RkapController()
        {
            repository = new RkapRepository();
        }

        protected override void Dispose(bool disposing)
        {
            repository.Dispose();
            base.Dispose(disposing);
        }


        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_rkap>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new RkapServ(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_rkap>>(() =>
            {
                var result = new ApiResponsePage<vw_rkap>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<IEnumerable<rkap>> GetPage()
        {
            return await repository.GetAll();
        }

        [Route("getSchedule")]
        [HttpGet]
        public async Task<IEnumerable<schedule_rkap>> GetSchedule(DateTime start, DateTime end)
        {
            return await repository.GetSchedulesAsync();
        }
        /*
        [Route("getChartData")]
        [HttpGet]
        public async Task<List<rkap_chart_ds>> GetChartData(int year)
        {
            return await repository.GetChartData(year);
        }
        */
        [Route("getChartDataDevelopment")]
        [HttpGet]
        public async Task<List<rkap_chart_ds>> GetChartDataDevelopment(int year)
        {
            if (userSession.SysAdmin)
            {
                return await repository.GetChartDataDevelopment(year, null);

            }
            else
            {
                return await repository.GetChartDataDevelopment(year, userSession.PrimaryBusinessUnitId);

            }
        }


        [Route("getChartBudgetDevelopment")]
        [HttpGet]
        public async Task<List<rkap_chart_ds>> GetChartBudgetDevelopment(int year)
        {
            if (userSession.SysAdmin)
            {
                return await repository.GetChartBudgetDevelopment(year, null);

            }
            else
            {
                return await repository.GetChartBudgetDevelopment(year, userSession.PrimaryBusinessUnitId);

            }
        }

        [Route("getChartDataExploration")]
        [HttpGet]
        public async Task<List<rkap_chart_ds>> GetChartDataExploration(int year)
        {
            if (userSession.SysAdmin)
            {
                return await repository.GetChartDataExploration(year, null);

            }
            else
            {
                return await repository.GetChartDataExploration(year, userSession.PrimaryBusinessUnitId);

            }
        }

        [Route("getChartBudgetExploration")]
        [HttpGet]
        public async Task<List<rkap_chart_ds>> GetChartBudgetExploration(int year)
        {
            if (userSession.SysAdmin)
            {
                return await repository.GetChartBudgetExploration(year, null);
            }
            else
            {
                return await repository.GetChartBudgetExploration(year, userSession.PrimaryBusinessUnitId);
            }
        }


        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTableRequest)
        {
            var repo = new RkapRepository();
            DataTablesResponse response = new DataTablesResponse(0, new List<rkap>(), 0, 0);
            try
            {
                if (userSession.SysAdmin)
                {
                    response = await repository.GetDataTableNew(dataTableRequest, null);

                }
                else
                {
                    response = await repository.GetDataTableNew(dataTableRequest, userSession.PrimaryBusinessUnitId);
                }
            }
            catch (Exception ex)
            { appLogger.Error(ex); }

            return response;
        }

        [Route("dataTables")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTableNew([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new RkapServ(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_rkap>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_rkap>> GetDetail(string recordId)
        {
            var result = new ApiResponse<vw_rkap>();

            try
            {
                var record = await repository.GetViewById(recordId);
                result.Data = record;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        // Controller untuk mendapatkan detail history berdasarkan ID
        [Route("{recordId}/detailHistory")]
        [HttpGet]
        public async Task<ApiResponse<IEnumerable<vw_rkap_history>>> GetDetailHistory(string recordId)
        {
            var result = new ApiResponse<IEnumerable<vw_rkap_history>>();

            try
            {
                var records = await repository.GetAllHistoryById(recordId);
                result.Data = records;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        [Route("{fieldId}/getWellRkap")]
        [HttpGet]
        public async Task<ApiResponse<IEnumerable<vw_well>>> GetDetailWell(string fieldId)
        {
            var result = new ApiResponse<IEnumerable<vw_well>>();

            try
            {
                var records = await repository.GetWellRkap(fieldId);
                result.Data = records;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        [Route("getRkapOptions")]
        [HttpPost]
        public async Task<List<vw_rkap>> GetRkapOptions()
        {
            var services = new RkapServ(this.dataContext);
            var data = await Task.Run<List<vw_rkap>>(() =>
            {
                var result = new List<vw_rkap>();
                var rkapList = services.Lookup(new MarvelDataSourceRequest());
                foreach (var rkap in rkapList.Items)
                {
                    result.Add(new vw_rkap
                    {
                        id = rkap.id.ToString(), // Sesuaikan dengan properti yang sesuai dengan ID RKAP
                        well_name = rkap.well_name // Sesuaikan dengan properti yang sesuai dengan nama RKAP
                    });
                }
                return result;
            });
            return data;
        }

        //[Route("save")]
        //[HttpPost]
        //public async Task<ApiResponse> Save(rkap model)
        //{
        //    var result = new ApiResponse();
        //    try
        //    {
        //        if (string.IsNullOrWhiteSpace(model.id))
        //        {
        //            model.created_on = DateTime.Now;
        //            model.created_by = userSession.AppUserId;
        //            model.organization_id = userSession.OrganizationId;
        //            model.planned_days = Convert.ToDecimal(model.planned_days); // Pastikan tipe datanya benar
        //            //model.planned_days = Convert.ToInt32(model.planned_days); // Pastikan tipe datanya benar
        //            var res = await repository.Insert(model);
        //        }
        //        else
        //        {
        //            model.modified_on = DateTime.Now;
        //            model.modified_by = userSession.AppUserId;
        //            model.status = 1;
        //            model.planned_days = Convert.ToDecimal(model.planned_days); // Pastikan tipe datanya benar
        //            //model.planned_days = Convert.ToInt32(model.planned_days); // Pastikan tipe datanya benar
        //            var res = await repository.Update(model);
        //        }
        //        await repository.AddHistory(model);
        //        result.Data = model.id;
        //        result.Status.Success = true;
        //        result.Status.Message = "The data has been added.";
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        result.Status.Success = false;
        //        result.Status.Message = ex.Message;
        //    }

        //    return result;
        //}

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(rkap model)
        {
            var result = new ApiResponse();
            try
            {
                if (string.IsNullOrWhiteSpace(model.id))
                {
                    model.created_on = DateTime.Now;
                    model.created_by = userSession.AppUserId;
                    model.organization_id = userSession.OrganizationId;

                    // Ensure business_unit_id is set
                    if (string.IsNullOrWhiteSpace(model.business_unit_id))
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Business Unit ID is required.";
                        return result;
                    }

                    model.planned_days = Convert.ToDecimal(model.planned_days); // Ensure data type is correct
                    var res = await repository.Insert(model);
                }
                else
                {
                    model.modified_on = DateTime.Now;
                    model.modified_by = userSession.AppUserId;
                    model.status = 1;

                    // Ensure business_unit_id is set
                    if (string.IsNullOrWhiteSpace(model.business_unit_id))
                    {
                        result.Status.Success = false;
                        result.Status.Message = "Business Unit ID is required.";
                        return result;
                    }

                    model.planned_days = Convert.ToDecimal(model.planned_days); // Ensure data type is correct
                    var res = await repository.Update(model);
                }

                await repository.AddHistory(model);
                result.Data = model.id;
                result.Status.Success = true;
                result.Status.Message = "The data has been added.";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }


        [Route("saveReal")]
        [HttpPost]
        public async Task<ApiResponse> SaveReal(rkap_history model)
        {
            var result = new ApiResponse();
            try
            {
                model.created_on = DateTime.Now;
                var res = await repository.RealRkap(model);

                result.Data = model.id;
                result.Status.Success = true;
                result.Status.Message = "The data has been added.";
                //await repository.isReal(model);

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] JObject data)
        {
            var result = new ApiResponse();
            try
            {
                string id = data["id"].ToString(); // Mendapatkan nilai ID dari properti 'id'
                var res = await repository.Delete(id);
                await repository.DeleteHistory(id);

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }


        [Route("realization")]
        [HttpPost]
        public async Task<ApiResponse> Realization(vw_rkap model)
        {
            var result = new ApiResponse();
            try
            {
                var wellService = new WellServices(this.dataContext);
                var wellModel = new well()
                {
                    business_unit_id = model.business_unit_id,
                    well_status = model.well_status,
                    well_name = model.well_name,
                    field_id = model.field_id,
                };

                var data = wellService.New(wellModel).Data;

                var rkapDt = await repository.GetById(model.id);
                if (model != null)
                {
                    rkapDt.created_on = DateTime.Now;
                    rkapDt.well_id = data?.GetType().GetProperty("recordId")?.GetValue(data, null).ToString();
                    rkapDt.status = 2;
                    rkapDt.modified_on = DateTime.Now;
                    rkapDt.modified_by = userSession.AppUserId;
                    await repository.Update(rkapDt);
                    await repository.AddHistory(rkapDt);
                }
                result.Status.Success = true;
                result.Status.Message = "The data has been added.";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }

            return result;
        }

        [Route("calendar")]
        [HttpGet]
        public async Task<calendar_response> GetCalendarResource(int year = 0)
        {
            var result = new calendar_response();

            try
            {
                if (year == 0)
                    year = DateTime.Now.Year;

                if (userSession.SysAdmin)
                {
                    result = await repository.GetCalendarResource(year, null);
                }
                else
                {
                    result = await repository.GetCalendarResource(year, userSession.PrimaryBusinessUnitId);
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }

            return result;
        }

        //[Route("calendar/event")]
        //[HttpGet]
        //public async Task<ApiResponse<List<rkap_timeline_event>>> GetCalendarEvent(DateTime start)
        //{
        //    var result = new ApiResponse<vw_rkap>();

        //    try
        //    {
        //        var record = await repository.GetViewById(recordId);
        //        result.Data = record;
        //        result.Status.Success = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //        result.Status.Success = false;
        //        result.Status.Message = ex.Message;
        //    }

        //    return result;
        //}

        //[Route("getWirData")]
        //[HttpGet]
        //public async Task<calendar_response> GetWirData(int year = 0)
        //{
        //    var result = new calendar_response();

        //    try
        //    {
        //        if (year == 0)
        //            year = DateTime.Now.Year;

        //        if (userSession.SysAdmin)
        //        {
        //            result = await repository.GetWIResource(year, null);
        //        }
        //        else
        //        {
        //            result = await repository.GetWIResource(year, userSession.PrimaryBusinessUnitId);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex);
        //    }

        //    return result;
        //}

        [Route("getWirData")]
        [HttpGet]
        public async Task<ApiResponse<List<RKAPWIViewModel>>> GetWirData()
        {
            var result = new ApiResponse<List<RKAPWIViewModel>>();
            try
            {
                var service = new WirServices(this.dataContext);
                result = await service.GetWirData(); // Await asynchronous service call
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = $"Error fetching WIR data: {ex.Message}";
            }

            return result;
        }

    }
}
