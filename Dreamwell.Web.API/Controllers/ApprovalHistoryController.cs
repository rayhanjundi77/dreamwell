﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess.ViewModels;
using static Dreamwell.DataAccess.dreamwellRepo;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/ApprovalHistory")]

    public class ApprovalHistoryController : ApiBaseController
    {
        [Route("getApprovalHistory/{recordId}")]
        [HttpPost]
        public async Task<DataTablesResponse> getDrillingApproval([FromBody] DataTableRequest dataTableRequest, string recordId)
        {
            var services = new ApprovalHistoryServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_approval_history>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active= @0 AND a.record_id= @1", true, recordId);
                result = services.GetListDataTables(dataTableRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("approvedHistory")]
        [HttpGet]
        public async Task<ApiResponse<int>> getViewApprovedHistory()
        {

            var services = new ApprovalHistoryServices(this.dataContext);
            var response = await Task.Run<ApiResponse<int>>(() =>
            {
                var result = new ApiResponse<int>();
                
                //var record = services.CountTotalDDR();
                var record = services.GetList();
                result.Status.Success = true;
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
