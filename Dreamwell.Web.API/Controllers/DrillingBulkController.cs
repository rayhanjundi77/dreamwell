﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{ 
    [RoutePrefix("api/core/DrillingBulk")]

    public class DrillingBulkController : ApiBaseController
    {

        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_drilling_bulk>> GetPage(long page, long itemsPerPage)
        {
            var services = new DrillingBulkServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_drilling_bulk>>(() =>
            {
                var result = new ApiResponsePage<vw_drilling_bulk>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_drilling_bulk>> GetDetail(string recordId)
        {
            var services = new DrillingBulkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_drilling_bulk>>(() =>
            {
                var result = new ApiResponse<vw_drilling_bulk>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{drillingId}/{wellId}/{drillingDate}/getByDrillingId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bulk>>> getByDrillingId(string drillingId, string wellId, string drillingDate)
        {
            var services = new DrillingBulkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bulk>>>(() =>
            {
                return services.getItemByWellId(drillingId, wellId, drillingDate);
            });
            return response;
        }

        [Route("{drillingId}/detailBulkId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drilling_bulk>>> GetDrillingByWell(string drillingId)
        {
            var services = new DrillingBulkServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drilling_bulk>>>(() =>
            {
                var result = new ApiResponse<List<vw_drilling_bulk>>();
                try
                {
                    result = services.getByBulkId(drillingId);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }


        //[Route("{recordId}/getByDrillingId")]
        //[HttpGet]
        //public async Task<ApiResponse<List<vw_drilling_bulk>>> getByDrillingId(string recordId)
        //{
        //    var services = new DrillingBulkServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse<List<vw_drilling_bulk>>>(() =>
        //    {
        //        var result = new ApiResponse<List<vw_drilling_bulk>>();
        //        var record = services.GetViewAll(" AND r.drilling_id=@0 ORDER BY r.created_on ASC ", recordId);
        //        result.Status.Success = (record != null);
        //        result.Data = record;
        //        return result;
        //    });
        //    return response;
        //}

        //[Route("save")]
        //[HttpPost]
        //public async Task<ApiResponse> Save(drilling_bulk record)
        //{
        //    var services = new DrillingBulkServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse>(() =>
        //    {
        //        return services.Save(record);
        //    });
        //    return response;
        //}

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(DrillingViewModel record)
        {
            var services = new DrillingBulkServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new DrillingBulkServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new DrillingBulkServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_drilling_bulk>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }
    }
}
