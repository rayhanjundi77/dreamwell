﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{ 
    [RoutePrefix("api/core/Field")]
    public class FieldController : ApiBaseController
    {
        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new FieldServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_field>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0",true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_field>> Lookup([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new FieldServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_field>>(() =>
            {
                var result = new ApiResponsePage<vw_field>();
                var data = services.LookupField(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookupByUnit")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_business_unit_field>> lookupByUnit([FromBody] MarvelDataSourceRequest marvelDataSourceRequest,
            [FromUri] string unitId, [FromUri] string assetId)
        {
            var services = new FieldServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_business_unit_field>>(() =>
            {
                var result = new ApiResponsePage<vw_business_unit_field>();
                var data = services.lookupByUnit(marvelDataSourceRequest,unitId,assetId);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        //[Route("lookupUser")]
        //[HttpPost]
        //public async Task<ApiResponsePage<vw_field>> LookupUser([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        //{
        //    var services = new FieldServices(this.dataContext);
        //    var response = await Task.Run<ApiResponsePage<vw_field>>(() =>
        //    {
        //        var result = new ApiResponsePage<vw_field>();
        //        var data = services.LookupUser(marvelDataSourceRequest);
        //        result.CurrentPage = data.CurrentPage;
        //        result.TotalPages = data.TotalPages;
        //        result.TotalItems = data.TotalItems;
        //        result.ItemsPerPage = data.ItemsPerPage;
        //        result.Items = data.Items;
        //        return result;
        //    });
        //    return response;
        //}


        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_field>> GetDetail(string recordId)
        {
            var services = new FieldServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_field>>(() =>
            {
                var result = new ApiResponse<vw_field>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }


        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(field record)
        {
            var services = new FieldServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("getAllField")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_field>>> getAllField()
        {
            var services = new FieldServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_field>>>(() =>
            {
                var result = new ApiResponse<List<vw_field>>();
                var record = services.GetViewAll();
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new FieldServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

    }
}
