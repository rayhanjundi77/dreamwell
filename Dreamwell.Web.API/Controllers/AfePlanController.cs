﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;
using CommonTools.EeasyUI;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using CommonTools.Helper;
using System.Net.Http;
using System.Net;
using System.Web.Hosting;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/AfePlan")]

    public class AfePlanController : ApiBaseController
    {
        //[Route("{afe_id}/GetTreeManageDetail")]
        //[HttpPost]
        //public async Task<DataTablesResponse> GetTreeManageDetail([FromBody] DataTableRequest dataTablesRequest, string afe_id)
        //{
        //    var services = new AfePlanServices(this.dataContext);
        //    var response = await Task.Run<DataTablesResponse>(() =>
        //    {
        //        DataTablesResponse result = new DataTablesResponse(0, new List<vw_afe_manage_detail>(), 0, 0);
        //        //var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
        //        result = services.GetListByQuery(dataTablesRequest, afe_id);
        //        return result;
        //    });
        //    return response;
        //}


        [Route("{recordId}/GetBS20ManageDetail")]
        [HttpPost]
        public async Task<ApiResponse> GetBS20ManageDetail([FromBody] TreeRequest request, string recordId)
        {
            var services = new AfePlanServices(this.dataContext);
            //CommonTools.Helper.Node<material>

            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                var data = services.GetAfeLineTree(request, recordId);
                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_afe_manage_detail>("description") },
                    Formatting = Formatting.Indented

                };
                result.Status.Success = true;
                result.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(data, settings), new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });


                return result;
            });
            return response;
        }


        [Route("{recordId}/GetBS20ManageDetailNew")]
        [HttpPost]
        public async Task<ApiResponse> GetBS20ManageDetailNew([FromBody] TreeRequest request, string recordId)
        {
            var services = new AfePlanServices(this.dataContext);

            var result = new ApiResponse();

            try
            {
                // Tunggu hasil dari GetAfeLineTreeAsync
                var data = await services.GetAfeLineTreeAsync(request, recordId);

                var settings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter> { new TreeConverter<vw_afe_manage_detail>("description") },
                    Formatting = Formatting.Indented
                };

                // Serialisasi data hasil rekursi ke JSON dengan settings
                var serializedData = JsonConvert.SerializeObject(data, settings);

                // Deserialisasi ulang jika dibutuhkan untuk penggunaan lebih lanjut
                result.Data = JsonConvert.DeserializeObject<object>(serializedData, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });

                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                // Tangani error yang terjadi
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }

            return result;
        }



        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse<vw_afe_plan>> Save(afe_plan record)
        {
            var services = new AfePlanServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_afe_plan>>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("download")]
        [HttpGet]
        public async Task<HttpResponseMessage> Download(string id)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            var filePath = System.Web.HttpContext.Current.Server.MapPath("~/bin/template_bs_19.xlsx");
            //string path = "C:\\template_bs_19.xlsx";
            var services = new AfePlanServices(this.dataContext);
            var data = await Task.Run<HttpResponseMessage>(() =>
            {
                return services.GenerateBS19(filePath, id);
            });

            return data;
        }

        [Route("{material_id}/deleteMaterial")]
        [HttpPost]
        public async Task<ApiResponse> DeleteMaterial(string material_id)
        {
            var services = new AfePlanServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result = services.DeleteAfe(material_id);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_afe_plan>> GetDetail(string recordId)
        {
            var services = new AfePlanServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_afe_plan>>(() =>
            {
                var result = new ApiResponse<vw_afe_plan>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
