﻿using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.WebApi;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections.Generic;
using System;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;
using CommonTools;
using Dreamwell.DataAccess;
using Dreamwell.BusinessLogic.Core.Services;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/moc")]

    public class MOCController : ApiBaseController
    {
        [Route("")]
        [HttpGet]
        public async Task<ApiResponsePage<vw_management_of_change>> GetPage(long page, long itemsPerPage)
        {
            var services = new MOCServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_management_of_change>>(() =>
            {
                var result = new ApiResponsePage<vw_management_of_change>();
                var data = services.GetViewPerPage(page, itemsPerPage);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("lookup")]
        [HttpPost]
        public async Task<ApiResponsePage<vw_management_of_change>> GetPage([FromBody] MarvelDataSourceRequest marvelDataSourceRequest)
        {
            var services = new MOCServices(this.dataContext);
            var response = await Task.Run<ApiResponsePage<vw_management_of_change>>(() =>
            {
                var result = new ApiResponsePage<vw_management_of_change>();
                var data = services.Lookup(marvelDataSourceRequest);
                result.CurrentPage = data.CurrentPage;
                result.TotalPages = data.TotalPages;
                result.TotalItems = data.TotalItems;
                result.ItemsPerPage = data.ItemsPerPage;
                result.Items = data.Items;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detail")]
        [HttpGet]
        public async Task<ApiResponse<vw_management_of_change>> GetDetail(string recordId)
        {
            var services = new MOCServices(this.dataContext);
            var response = await Task.Run<ApiResponse<vw_management_of_change>>(() =>
            {
                var result = new ApiResponse<vw_management_of_change>();
                var record = services.GetViewById(recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{wipid}/detailbyId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_management_of_change>>> getbywirId(string wipid)
        {
            var services = new MOCServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_management_of_change>>>(() =>
            {
                var result = new ApiResponse<List<vw_management_of_change>>();
                try
                {
                    result = services.getByDetail(wipid);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("approve/{mocid}/detailbyId")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_approval_moc>>> getApproveMoc(string mocid)
        {
            var services = new MOCApproveServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_approval_moc>>>(() =>
            {
                var result = new ApiResponse<List<vw_approval_moc>>();
                try
                {
                    result = services.getByDetail(mocid);
                    return result;
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.ToString();
                }
                return result;
            });
            return response;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(management_of_change record)
        {
            var services = new MOCServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("approver/save")]
        [HttpPost]
        public async Task<ApiResponse> SaveApprover(approval_moc record)
        {
            var services = new MOCApproveServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                return services.Save(record);
            });
            return response;
        }

        [Route("delete")]
        [HttpPost]
        public async Task<ApiResponse> Delete([FromBody] string[] ids)
        {
            var services = new MOCServices(this.dataContext);
            var response = await Task.Run<ApiResponse>(() =>
            {
                var result = new ApiResponse();
                try
                {
                    result.Status.Success = services.RemoveAll(ids);
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    result.Status.Success = false;
                    result.Status.Message = ex.Message;

                }
                return result;
            });
            return response;
        }

        [Route("dataTable")]
        [HttpPost]
        public async Task<DataTablesResponse> GetListDataTable([FromBody] DataTableRequest dataTablesRequest)
        {
            var services = new MOCServices(this.dataContext);
            var response = await Task.Run<DataTablesResponse>(() =>
            {
                DataTablesResponse result = new DataTablesResponse(0, new List<vw_management_of_change>(), 0, 0);
                var qry = PetaPoco.Sql.Builder.Append(" WHERE r.is_active=@0 ", true);
                result = services.GetListDataTables(dataTablesRequest, sqlOptionalFilter: qry);
                return result;
            });
            return response;
        }



    }
}
