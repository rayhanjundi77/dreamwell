﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{ 
    [RoutePrefix("api/core/CVDPlan")]
    public class CVDPlanController : ApiBaseController
    {
        [Route("{recordId}/detailbywellid")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_cvd_plan>>> GetCVDbyWellID(string recordId)
        {
            var services = new CVDPlanServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_cvd_plan>>>(() =>
            {
                var result = new ApiResponse<List<vw_cvd_plan>>();
                var record = services.GetViewAll(" AND r.well_id=@0 ", recordId);
                result.Status.Success = (record != null);
                record.Sort((a, b) => a.no.CompareTo(b.no));
                result.Data = record;
                return result;
            });
            return response;
        }
        
    }
}
