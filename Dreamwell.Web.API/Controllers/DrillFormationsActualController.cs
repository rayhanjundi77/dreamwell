﻿using CommonTools;
using DataTables.Mvc;
using Dreamwell.BusinessLogic.Core;
using Dreamwell.BusinessLogic.Core.Services;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure;
using Dreamwell.Infrastructure.DataTables;
using Dreamwell.Infrastructure.WebApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Dreamwell.Web.API.Controllers
{
    [RoutePrefix("api/core/DrillFormationsActual")]
    public class DrillFormationsActualController : ApiBaseController
    {
        [Route("save")]
        [HttpPost]
        public async Task<ApiResponse> Save(DrillingViewModel record)
        {
            var service = new DrillFormationActualServices(this.dataContext);
            var Response = await Task.Run<ApiResponse>(() =>
            {
                return service.Save(record);
            });
            return Response;
        }

        [Route("saveActualDetail")]
        [HttpPost]
        public async Task<ApiResponse> SaveActualDetail(DrillingViewModel record)
        {
            var service = new DrillFormationActualDetailServices(this.dataContext);
            var Response = await Task.Run<ApiResponse>(() =>
            {
                return service.SaveDetailActual(record);
            });
            return Response;
        }

        [Route("{recordId}/detailbydrillingid")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drill_formations_actual>>> GetByDrillingId(string recordId)
        {
            var services = new DrillFormationActualServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drill_formations_actual>>>(() => 
            {
                var result = new ApiResponse<List<vw_drill_formations_actual>>();
                var record = services.GetViewAll(" AND r.drilling_id=@0 ORDER BY r.seq ASC ", recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detailbywellid")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drill_formations_actual>>> GetByWellId(string recordId)
        {
            var services = new DrillFormationActualServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drill_formations_actual>>>(() =>
            {
                var result = new ApiResponse<List<vw_drill_formations_actual>>();
                var record = services.GetViewAll(" AND r.well_id=@0 ORDER BY r.seq ASC ", recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }

        [Route("{recordId}/detailbywellidNew")]
        [HttpGet]
        public async Task<ApiResponse<List<vw_drill_formations_actual>>> GetByWellIdNew(string recordId)
        {
            var services = new DrillFormationActualServices(this.dataContext);
            var servicesDetail = new DrillFormationActualDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<vw_drill_formations_actual>>>(() =>
            {
                var result = new ApiResponse<List<vw_drill_formations_actual>>();
                //var record = services.GetViewAll(" AND r.well_id=@0 ORDER BY r.seq ASC ", recordId);
                System.Collections.Generic.List<vw_drill_formations_actual> record = services.GetListView(recordId);
                result.Status.Success = (record != null);
                foreach (vw_drill_formations_actual actual in record) {
                    actual.detail = servicesDetail.GetAll(" AND drill_formation_actual_id=@0 ORDER BY seq ASC ", actual.id);
                    actual.detailObj = servicesDetail.GetDetailListByActualId(actual.id).Data;
                }
                result.Data = record;
                return result;
            });
            return response;
        }

        //[Route("{recordId}/detailbywellidNoRole")]
        //[HttpGet]
        //public async Task<ApiResponse<List<drill_formations_actual>>> GetByWellIdNoRole(string recordId)
        //{
        //    var services = new DrillFormationActualServices(this.dataContext);
        //    var response = await Task.Run<ApiResponse<List<drill_formations_actual>>>(() =>
        //    {
        //        var result = new ApiResponse<List<drill_formations_actual>>();
        //        var record = services.GetAll(" AND well_id=@0 ORDER BY seq ASC ", recordId);
        //        result.Status.Success = (record != null);
        //        result.Data = record;
        //        return result;
        //    });
        //    return response;
        //}

        [Route("{recordId}/detailbywellidNoRole")]
        [HttpGet]
        public async Task<ApiResponse<List<drill_formations_actual>>> GetByWellIdNoRole(string recordId)
        {
            var services = new DrillFormationActualServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<drill_formations_actual>>>(() =>
            {
                var result = new ApiResponse<List<drill_formations_actual>>();
                var record = services.GetList(recordId);
                result.Status.Success = (record != null);
                result.Data = (List<drill_formations_actual>)record.Data;
                return result;
            });
            return response;
        }

        [Route("getDetail/{recordId}")]
        [HttpGet]
        public async Task<ApiResponse<List<drill_formations_actual_detail>>> GetByDetail(string recordId)
        {
            var services = new DrillFormationActualDetailServices(this.dataContext);
            var response = await Task.Run<ApiResponse<List<drill_formations_actual_detail>>>(() =>
            {
                var result = new ApiResponse<List<drill_formations_actual_detail>>();
                var record = services.GetAll(" AND drill_formation_actual_id=@0 ORDER BY seq ASC ", recordId);
                result.Status.Success = (record != null);
                result.Data = record;
                return result;
            });
            return response;
        }
    }
}
