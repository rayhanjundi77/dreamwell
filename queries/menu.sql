select * from (SELECT
  r.id,
  r.created_by,
  r.created_on,
  r.modified_by,
  r.modified_on,
  r.approved_by,
  r.approved_on,
  r.is_active,
  r.is_locked,
  r.is_default,
  r.owner_id,
  r.is_group,
  r.menu_name,
  r.application_menu_category_id,
  mc.category_name,
  r.organization_id,
  o1.organization_name,
  r.parent_menu_id,
  p.menu_name AS parent_unit_name,
  r.action_url,
  r.icon_class,
  r.show_on_seed,
  r.order_index,

  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,
  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,
  t.team_name  AS record_owning_team
FROM dbo.application_menu AS r
LEFT OUTER JOIN dbo.application_menu AS p
  ON r.parent_menu_id = p.id
LEFT OUTER JOIN dbo.application_menu_category AS mc
  ON r.application_menu_category_id = mc.id
LEFT OUTER JOIN dbo.organization AS o1
  ON r.organization_id = o1.id
LEFT OUTER JOIN dbo.application_user AS u0
  ON r.created_by = u0.id
LEFT OUTER JOIN dbo.application_user AS u1
  ON r.modified_by = u1.id
LEFT OUTER JOIN dbo.application_user AS u2
  ON r.approved_by = u2.id
LEFT OUTER JOIN dbo.application_user AS u3
  ON r.owner_id = u3.id
LEFT OUTER JOIN dbo.team AS t
  ON r.owner_id = t.id) a
WHERE is_active=1 AND parent_menu_id= 'a0caf42f-5adf-44f3-b90a-88c377fa28ec';