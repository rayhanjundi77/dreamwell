﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class Report2DTrajectoryViewModel
    {
        public string well_id { get; set; }
        public List<well_deviation> well_deviation { get; set; }
        public List<drilling_deviation> drilling_deviation { get; set; }

        public Report2DTrajectoryViewModel()
        {
            well_deviation = new List<well_deviation>();
            drilling_deviation = new List<drilling_deviation>();
        }
    }

}
