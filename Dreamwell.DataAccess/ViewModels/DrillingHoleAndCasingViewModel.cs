﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class DrillingHoleAndCasingViewModel
    {
        public string drilling_id { get; set; }
        public string well_id { get; set; }
        public List<DrillingHoleAndCasing> drilling_hole_and_casing { get; set; }

        public DrillingHoleAndCasingViewModel()
        {
            drilling_hole_and_casing = new List<DrillingHoleAndCasing>();
        }
    }

    public class DrillingHoleAndCasing : drilling_hole_and_casing
    {
        public List<drilling_operation_activity> drilling_operation_activity { get; set; }
    }

}
