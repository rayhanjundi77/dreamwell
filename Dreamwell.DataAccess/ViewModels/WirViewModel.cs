﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class WirViewModel
    {
        public vw_wir wir { get; set; }
        public List<vw_wellwi> wellwi { get; set; }
    }
}
