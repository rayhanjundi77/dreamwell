﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.ViewModels
{
    public class RKAPWIViewModel
    {
        //public List<well> well { get; set; }
        //public List<wir> wir { get; set; }
        //public List<program_detail_wip> pd { get; set; }
        //public List<vw_well_intervention_program> wip { get; set; }

        public string well_name { get; set; }
        public string remarks { get; set; }
        public string well_id { get; set; }
        public string wir_no { get; set; }
        public DateTime? issues_date { get; set; }
        public string status { get; set; }
        public int job_day { get; set; }
        public int gas { get; set; }
        public int oil { get; set; }
        public string main_job { get; set; }
    }
}

