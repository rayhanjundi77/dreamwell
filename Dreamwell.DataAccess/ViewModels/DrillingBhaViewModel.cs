﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class WellBhaViewModel
    {
        public string drilling_id { get; set; }
        public well_bha well_bha { get; set; }
        public List<well_bha_component> well_bha_component { get; set; }
        public bool is_new { get; set; }
    }
}
