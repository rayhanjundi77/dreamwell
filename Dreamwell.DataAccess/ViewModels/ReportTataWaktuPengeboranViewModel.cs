﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class ReportTataWaktuPengeboranViewModel
    {
        public string well_id { get; set; }
        public List<tvd_plan> tvd_plan { get; set; }
        public List<vw_report_drilling> report_drilling { get; set; }
        public List<vw_drilling> drilling_data { get; set; }

        public ReportTataWaktuPengeboranViewModel()
        {
            tvd_plan = new List<tvd_plan>();
            report_drilling = new List<vw_report_drilling>();
            drilling_data = new List<vw_drilling>();
        }
    }
}
