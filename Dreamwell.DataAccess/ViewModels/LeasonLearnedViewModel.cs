﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class LeasonLearnedViewModel
    {
        public string field_id { get; set; }
        public List<string> well_id { get; set; }
        public string iadc_id { get; set; }
        public string uom_select { get; set; }
    }
}
