﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class AnalyzeDataDetailsViewModel
    {
        public string well_id { get; set; }
        public List<vw_drilling> drilling { get; set; }
        public List<vw_time_versus_cost> time_versus_cost { get; set; }

        public AnalyzeDataDetailsViewModel()
        {
            drilling = new List<vw_drilling>();
            time_versus_cost = new List<vw_time_versus_cost>();
        }
    }
}
