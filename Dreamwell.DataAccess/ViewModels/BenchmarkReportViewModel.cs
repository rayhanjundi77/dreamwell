﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class BenchmarkReportViewModel
    {
        public string business_unit_id { get; set; }
        public string field_id { get; set; }
        public int type { get; set; }
        public string uom_select { get; set; }
        public List<string> well_id { get; set; }
        public List<int> well_year { get; set; }
        public List<int> well_status { get; set; }
        public List<decimal> bit_size { get; set; }
    }
}
