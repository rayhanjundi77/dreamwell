﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.ViewModels
{
    public class Base19Format
    {
        public string Afe_Line_ID { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; }
        public double total_price_plan { get; set; }
        public double total_price_actual { get; set; }
    }
}
