﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class WellViewModel
    {
        public well well { get; set; }
        public wellwi wellwi { get; set; }
        public List<drill_formations_plan> drill_formations_plan { get; set; }
        public List<tvd_plan> tvd_plan { get; set; }
        public List<cvd_plan> cvd_plan { get; set; }
        public List<drilling_hazard> drilling_hazard { get; set; }
        public List<well_profile_program> well_profile_program { get; set; }
        public List<well_deviation> well_deviation { get; set; }
        public List<vw_well_item> well_item{ get; set; }
        public List<vw_hole_and_casing> hole_and_casing { get; set; }
    }
}
