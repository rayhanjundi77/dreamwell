﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class ReportTotalWellByYears
    {
        public int year { get; set; }
        public List<vw_field> field { get; set; }
        public int total_well { get; set; }

        public ReportTotalWellByYears()
        {
            field = new List<vw_field>();
            total_well = 0;
        }
    }
}
