﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class DrillingBitViewModel
    {
        public string drilling_id { get; set; }
        public string drilling_bha_id { get; set; }
        public string well_id { get; set; }
        //public vw_drilling_bit drilling_bit { get; set; }
        public bool is_new { get; set; }
    }
}
