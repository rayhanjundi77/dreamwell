﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class DrillingViewModel
    {
        public vw_drilling drilling { get; set; }
        //public List<vw_drilling_operation> drilling_operation { get; set;}
        public List<vw_drilling_operation_activity> drilling_operation_activity { get; set; }
        public List<vw_drilling_deviation> drilling_deviation { get; set; }
        public List<drilling_transport> drilling_transport { get; set; }
        public List<vw_drilling_bulk> drilling_bulk { get; set; }
        public vw_drilling_mud drilling_mud_1 { get; set; }
        public vw_drilling_mud drilling_mud_2 { get; set; }
        public vw_drilling_mud drilling_mud_3 { get; set; }
        public List<vw_drilling_mud_pit> drilling_mud_pit { get; set; }
        public vw_drilling_aerated drilling_aerated { get; set; }
        public List<vw_drilling_chemical_used> drilling_chemical_used { get; set; }
        public vw_drilling_total_volume drilling_total_volume { get; set; }
        public List<drilling_personel> drilling_personel { get; set; }
        public List<vw_drilling_hole_and_casing> drilling_hole_and_casing { get; set; }
        public string drilling_bha_id { get; set; }
        public List<drill_formations_actual> drill_Formations_Actual { get; set; }

        public List<drill_formations_actual_detail> drill_Formations_Actual_Detail { get; set; }

        //public List<drilling_bulk> drilling_Bulks { get; set; }
        public string Category { get; set; }
        public string PlannedTdUom { get; set; }
        public string DepthUom { get; set; }

        public vw_well well { get; set; }
        public string well_name { get; set; }
        public decimal? total_operation_duration_hours { get; set; }
        public decimal? total_hours_pt { get; set; }
        public decimal? total_hours_npt { get; set; }

    }
}
