﻿using System.Collections.Generic;

namespace Dreamwell.DataAccess.ViewModels
{
    public class DailyCostViewModel
    {
        public decimal daily_cost { get; set; }
        public decimal cummulative_cost { get; set; }
    }
}
