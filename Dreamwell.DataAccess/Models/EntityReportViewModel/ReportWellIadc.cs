﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_report_well_iadc
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string well_id { get; set; } //(nvarchar(50), null)
        public DateTime drilling_date { get; set; } //(nvarchar(50), null)
        public decimal total_hours_pt { get; set; }
        public decimal total_hours_npt { get; set; }
        public decimal total_hours { get; set; } 
    }

    public partial class vw_report_well_iadc : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_report_well_iadc()
        {
            EntityName = drilling.GetTableName();
            EntityDisplayName = drilling.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT r.id, r.owner_id, r.well_id, r.drilling_date, pt.total_hours AS total_hours_pt, npt.total_hours AS total_hours_npt, (pt.total_hours + npt.total_hours) AS total_hours");
            sb.AppendLine(@"FROM drilling r");
            sb.AppendLine(@"CROSS APPLY (");
            sb.AppendLine(@"	SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours");
            sb.AppendLine(@"	FROM drilling_operation_activity doa");
            sb.AppendLine(@"	INNER JOIN iadc AS i ON i.id = doa.iadc_id");
            sb.AppendLine(@"	CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s");
            sb.AppendLine(@"	WHERE doa.drilling_id = r.id AND i.type = 1");
            sb.AppendLine(@"	) AS pt");
            sb.AppendLine(@"CROSS APPLY (");
            sb.AppendLine(@"	SELECT COALESCE(SUM(s.interval_hours), 0) AS total_hours");
            sb.AppendLine(@"	FROM drilling_operation_activity doa");
            sb.AppendLine(@"	INNER JOIN iadc AS i ON i.id = doa.iadc_id");
            sb.AppendLine(@"	CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS s");
            sb.AppendLine(@"	WHERE doa.drilling_id = r.id AND i.type = 2");
            sb.AppendLine(@"	) AS npt");
            //sb.AppendLine(@"WHERE r.well_id = '4d9c7b72-40af-425f-9ab2-266cd009f6ac'");
            //sb.AppendLine(@"ORDER BY r.drilling_date ASC");
            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("drilling_date", "r.drilling_date");
            DefaultViewColumns.Add("total_hours_pt", "pt.total_hours");
            DefaultViewColumns.Add("total_hours_npt", "npt.total_hours");
            DefaultViewColumns.Add("total_hours", "(pt.total_hours + pt.total_hours)");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("drilling_date", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
