﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_report_bit_record
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string created_by { get; set; }
        public string record_created_by { get; set; }
        public string role_name { get; set; }
        public string well_id { get; set; }
        public string well_name { get; set; }
        public string rig_name { get; set; }
        public string field_name { get; set; }
        public int bit_run { get; set; }
        public int bha_no { get; set; }
        public string manufacturer { get; set; }
        public decimal bit_size { get; set; }
        public string bit_type { get; set; }
        public string IADC { get; set; }
        public string serial_number { get; set; }
        public decimal noozle_1 { get; set; }
        public decimal depth_in { get; set; }
        public decimal depth_out { get; set; }
        public DateTime? drilling_date { get; set; }
        public decimal dg_hours_on_bottom { get; set; }
        public decimal dg_footage { get; set; }
        public decimal dg_rop_overall { get; set; }
        public string WOB { get; set; }
        public string RPM { get; set; }
        public string RPM_MTR { get; set; }
        public string SPP { get; set; }
        public string GPM { get; set; }
        public string DULL_GRADE { get; set; }
    }

    public partial class vw_report_bit_record : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_report_bit_record()
        {
            EntityName = drilling_bha_bit.GetTableName();
            EntityDisplayName = drilling_bha_bit.GetTableName();
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"r.id, ");
            sb.AppendLine(@"r.owner_id, ");
            sb.AppendLine(@"r.well_id, ");
            sb.AppendLine(@"r.bit_run, ");
            sb.AppendLine(@"wh.bha_no, ");
            sb.AppendLine(@"wb.manufacturer, ");
            sb.AppendLine(@"wb.bit_size, ");
            sb.AppendLine(@"wb.bit_type, ");
            sb.AppendLine(@"i.description AS IADC, ");
            sb.AppendLine(@"wb.serial_number, ");
            sb.AppendLine(@"wb.noozle_1, ");
            sb.AppendLine(@"r.depth_in, ");
            sb.AppendLine(@"r.depth_out, ");
            sb.AppendLine(@"wb.dg_hours_on_bottom, ");
            sb.AppendLine(@"r.dg_footage, ");
            sb.AppendLine(@"r.dg_rop_overall, ");
            sb.AppendLine(@"CONCAT(r.mudlogging_wob_min, '-', r.mudlogging_wob_max) AS WOB, ");
            sb.AppendLine(@"CONCAT(r.mudlogging_rpm_min, '-', r.mudlogging_rpm_max) AS RPM, ");
            sb.AppendLine(@"CONCAT(r.mudlogging_dhrpm_min, '-', r.mudlogging_dhrpm_max) AS RPM_MTR, ");
            sb.AppendLine(@"CONCAT(r.mudlogging_spp_min, '-', r.mudlogging_spp_max) AS SPP, ");
            sb.AppendLine(@"CONCAT(r.mudlogging_flowrate_min, '-', r.mudlogging_flowrate_max) AS GPM, ");
            sb.AppendLine(@"CONCAT(r.dg_in_rows,'/',r.dg_out_rows,'/',r.dg_dull_char,'/',r.dg_loc_cone,'/',r.dg_seals,'/',r.dg_gauge,'/',r.dg_other_char,'/',r.dg_reason_pulled) AS DULL_GRADE ");
            sb.AppendLine(@"FROM drilling_bha_bit AS r ");
            sb.AppendLine(@"LEFT OUTER JOIN well_bit AS wb ON r.well_bit_id = wb.id ");
            sb.AppendLine(@"LEFT OUTER JOIN well_bha AS wh ON r.well_bha_id = wh.id ");
            sb.AppendLine(@"LEFT OUTER JOIN iadc  AS i ON wb.iadc_code = i.id ");

            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("bit_run", "r.bit_run");
            DefaultViewColumns.Add("bha_no", "wh.bha_no");
            DefaultViewColumns.Add("manufacturer", "wb.manufacturer");
            DefaultViewColumns.Add("bit_size", "wb.bit_size");
            DefaultViewColumns.Add("bit_type", "wb.bit_type");
            DefaultViewColumns.Add("IADC", "i.description");
            DefaultViewColumns.Add("serial_number", "wb.serial_number");
            DefaultViewColumns.Add("noozle_1", "wb.noozle_1");
            DefaultViewColumns.Add("depth_in", "r.depth_in");
            DefaultViewColumns.Add("depth_out", "r.depth_out");
            DefaultViewColumns.Add("dg_hours_on_bottom", "wb.dg_hours_on_bottom");
            DefaultViewColumns.Add("dg_footage", "r.dg_footage");
            DefaultViewColumns.Add("dg_rop_overall", "r.dg_rop_overall");
            DefaultViewColumns.Add("WOB", "CONCAT(r.mudlogging_wob_min, '-', r.mudlogging_wob_max)");
            DefaultViewColumns.Add("RPM", "CONCAT(r.mudlogging_rpm_min, '-', r.mudlogging_rpm_max)");
            DefaultViewColumns.Add("RPM_MTR", "CONCAT(r.mudlogging_dhrpm_min, '-', r.mudlogging_dhrpm_max)");
            DefaultViewColumns.Add("SPP", "CONCAT(r.mudlogging_spp_min, '-', r.mudlogging_spp_max)");
            DefaultViewColumns.Add("GPM", "CONCAT(r.mudlogging_flowrate_min, '-', r.mudlogging_flowrate_max)");
            DefaultViewColumns.Add("DULL_GRADE", "CONCAT(r.dg_in_rows,'/',r.dg_out_rows,'/',r.dg_dull_char,'/',r.dg_loc_cone,'/',r.dg_seals,'/',r.dg_gauge,'/',r.dg_other_char,'/',r.dg_reason_pulled)");
            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns )
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("bha_no", "ASC");
            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion
        }
        public string GetEntityId()
        {
            return EntityId;
        }
        public string GetEntityName()
        {
            return EntityName;
        }
        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }
        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {
            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }
    }
}
