﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_benchmark_report_iadc
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string well_name { get; set; }
        public decimal pt_interval_hours { get; set; }
        public decimal npt_interval_hours { get; set; }
    }

    public partial class vw_benchmark_report_iadc : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_benchmark_report_iadc()
        {
            EntityName = iadc.GetTableName();
            EntityDisplayName = iadc.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT r.id, r.owner_id, r.well_name, pt.interval_hours AS pt_interval_hours, npt.interval_hours AS npt_interval_hours");
            sb.AppendLine(@"FROM well r");
            sb.AppendLine(@"CROSS APPLY (");
            sb.AppendLine(@"	SELECT type = 1, SUM((interval.interval_hours)) AS interval_hours");
            sb.AppendLine(@"	FROM drilling_operation_activity doa");
            sb.AppendLine(@"	INNER JOIN drilling d ON d.id = doa.drilling_id");
            sb.AppendLine(@"	INNER JOIN iadc i ON i.id = doa.iadc_id");
            sb.AppendLine(@"	CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS interval");
            sb.AppendLine(@"	WHERE d.well_id = r.id and i.type = 1");
            sb.AppendLine(@"	) AS pt");
            sb.AppendLine(@"CROSS APPLY (");
            sb.AppendLine(@"	SELECT type = 2, SUM((interval.interval_hours)) AS interval_hours");
            sb.AppendLine(@"	FROM drilling_operation_activity doa");
            sb.AppendLine(@"	INNER JOIN drilling d ON d.id = doa.drilling_id");
            sb.AppendLine(@"	INNER JOIN iadc i ON i.id = doa.iadc_id");
            sb.AppendLine(@"	CROSS APPLY (SELECT * FROM [dbo].[udf_iadc_interval_operation_activity](doa.iadc_id, doa.operation_start_date, doa.operation_end_date)) AS interval");
            sb.AppendLine(@"	WHERE d.well_id = r.id and i.type = 2");
            sb.AppendLine(@"	) AS npt");
            //sb.AppendLine(@"WHERE r.id = '4d9c7b72-40af-425f-9ab2-266cd009f6ac'");
            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("well_name", "r.well_name");
            DefaultViewColumns.Add("pt_interval_hours", "pt.interval_hours");
            DefaultViewColumns.Add("npt_interval_hours", "npt.interval_hours");
            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("well_name", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
