﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_report_well_bit
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public int bha_no { get; set; }
        public int bit_number { get; set; }
        public int bit_run { get; set; }
        public decimal bit_size { get; set; }
        public string manufacturer { get; set; }
        public string bit_type { get; set; }
        public string serial_number { get; set; }
        public string iadc_code { get; set; }
        public decimal noozle_1 { get; set; }
        public decimal noozle_2 { get; set; }
        public decimal noozle_3 { get; set; }
        public decimal noozle_4 { get; set; }
        public decimal noozle_5 { get; set; }
        public decimal dg_depth_in { get; set; }
        public decimal dg_depth_out { get; set; }
        public decimal dg_rop_overall { get; set; }
    }

    public partial class vw_report_well_bit : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_report_well_bit()
        {
            EntityName = well_bit.GetTableName();
            EntityDisplayName = afe.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"r.id, ");
            sb.AppendLine(@"r.owner_id, ");
            sb.AppendLine(@"r.bha_no, ");
            sb.AppendLine(@"wb.bit_number, ");
            sb.AppendLine(@"dbb.bit_run, ");
            sb.AppendLine(@"wb.bit_size, ");
            sb.AppendLine(@"wb.manufacturer, ");
            sb.AppendLine(@"wb.bit_type, ");
            sb.AppendLine(@"wb.serial_number, ");
            sb.AppendLine(@"wb.iadc_code, ");
            sb.AppendLine(@"wb.noozle_1, ");
            sb.AppendLine(@"wb.noozle_2, ");
            sb.AppendLine(@"wb.noozle_3,");
            sb.AppendLine(@"wb.noozle_4, ");
            sb.AppendLine(@"wb.noozle_5, ");
            sb.AppendLine(@"wb.dg_depth_in, ");
            sb.AppendLine(@"wb.dg_depth_out, ");
            sb.AppendLine(@"wb.dg_rop_overall");
            sb.AppendLine(@"FROM well_bha r");
            sb.AppendLine(@"INNER JOIN drilling_bha_bit dbb ON dbb.well_bha_id = r.id");
            sb.AppendLine(@"INNER JOIN well_bit wb ON wb.id = dbb.well_bit_id");
            //sb.AppendLine(@"WHERE r.well_id = '4d9c7b72-40af-425f-9ab2-266cd009f6ac'");
            //sb.AppendLine(@"ORDER BY r.bha_no ASC");

            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("bha_no", "r.bha_no");
            DefaultViewColumns.Add("bit_number", "wb.bit_number");
            DefaultViewColumns.Add("bit_run", "dbb.bit_run");
            DefaultViewColumns.Add("bit_size", "wb.bit_size");
            DefaultViewColumns.Add("manufacturer", "wb.manufacturer");
            DefaultViewColumns.Add("bit_type", "wb.bit_type");
            DefaultViewColumns.Add("serial_number", "wb.serial_number");
            DefaultViewColumns.Add("iadc_code", "wb.iadc_code");
            DefaultViewColumns.Add("noozle_1", "wb.noozle_1");
            DefaultViewColumns.Add("noozle_2", "wb.noozle_2");
            DefaultViewColumns.Add("noozle_3", "wb.noozle_3");
            DefaultViewColumns.Add("noozle_4", "wb.noozle_4");
            DefaultViewColumns.Add("noozle_5", "wb.noozle_5");
            DefaultViewColumns.Add("dg_depth_in", "wb.dg_depth_in");
            DefaultViewColumns.Add("interval", "wb.dg_depth_out");
            DefaultViewColumns.Add("dg_rop_overall", "wb.dg_rop_overall");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("bha_no", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
