﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_time_versus_cost
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string well_id { get; set; } //(nvarchar(50), null)
        public int dfs { get; set; }
        public DateTime drilling_date { get; set; }
        public decimal daily_cost { get; set; }
        public decimal cumm_cost { get; set; }
        public decimal total_cost { get; set; }
        public decimal usage_percentage { get; set; }
    }

    public partial class vw_time_versus_cost : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_time_versus_cost()
        {
            EntityName = drilling.GetTableName();
            EntityDisplayName = drilling.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"	r.id, ");
            sb.AppendLine(@"	r.owner_id, ");
            sb.AppendLine(@"	r.well_id, ");
            sb.AppendLine(@"	r.dfs, ");
            sb.AppendLine(@"	r.drilling_date, ");
            sb.AppendLine(@"	r.daily_cost,");
            sb.AppendLine(@"	r.cumm_cost,");
            sb.AppendLine(@"	r.total_cost,");
            sb.AppendLine(@"	r.usage_percentage");
            sb.AppendLine(@"FROM (");
            sb.AppendLine(@"	SELECT r.id,  r.owner_id, r.well_id,  r.dfs, r.drilling_date, daily.daily_cost, n.cumm_cost, t.total_cost,");
            sb.AppendLine(@"		CASE");
            sb.AppendLine(@"			WHEN daily.daily_cost <= 0 THEN 0");
            sb.AppendLine(@"			ELSE (n.cumm_cost / w.afe_cost) * 100");
            sb.AppendLine(@"		END AS usage_percentage");
            sb.AppendLine(@"	FROM drilling r");
            sb.AppendLine(@"	inner join well w on w.id = r.well_id ");
            sb.AppendLine(@"	CROSS APPLY (");
            sb.AppendLine(@"					SELECT COALESCE(SUM(dc.unit * cd.unit_price / cd.current_rate_value), 0) AS daily_cost");
            sb.AppendLine(@"					FROM daily_cost AS dc");
            sb.AppendLine(@"					INNER JOIN afe_contract ac ON ac.id = dc.afe_contract_id");
            sb.AppendLine(@"					INNER JOIN contract_detail cd ON cd.id = ac.contract_detail_id");
            sb.AppendLine(@"					WHERE dc.drilling_id = r.id");
            sb.AppendLine(@"				) AS daily");
            sb.AppendLine(@"	CROSS APPLY (");
            sb.AppendLine(@"			SELECT COALESCE(SUM(cd.actual_price / cd.unit * dc.unit), 0) AS cumm_cost");
            sb.AppendLine(@"			FROM dbo.daily_cost AS dc");
            sb.AppendLine(@"			LEFT OUTER JOIN dbo.afe_contract AS a");
            sb.AppendLine(@"				ON dc.afe_contract_id = a.id");
            sb.AppendLine(@"			LEFT OUTER JOIN dbo.drilling AS d");
            sb.AppendLine(@"				ON dc.drilling_id = d.id");
            sb.AppendLine(@"			LEFT OUTER JOIN dbo.contract_detail AS cd");
            sb.AppendLine(@"				ON a.contract_detail_id = cd.id");
            sb.AppendLine(@"			WHERE d.well_id = r.well_id AND d.drilling_date <= r.drilling_date");
            sb.AppendLine(@"	) AS n");
            sb.AppendLine(@"	CROSS APPLY (");
            sb.AppendLine(@"			SELECT COALESCE(SUM(cd.actual_price / cd.unit * dc.unit), 0) AS total_cost");
            sb.AppendLine(@"			FROM dbo.daily_cost AS dc");
            sb.AppendLine(@"			LEFT OUTER JOIN dbo.afe_contract AS a");
            sb.AppendLine(@"				ON dc.afe_contract_id = a.id");
            sb.AppendLine(@"			LEFT OUTER JOIN dbo.drilling AS d");
            sb.AppendLine(@"				ON dc.drilling_id = d.id");
            sb.AppendLine(@"			LEFT OUTER JOIN dbo.contract_detail AS cd");
            sb.AppendLine(@"				ON a.contract_detail_id = cd.id");
            sb.AppendLine(@"			WHERE d.well_id = r.well_id");
            sb.AppendLine(@"	) AS t");
            sb.AppendLine(@"	WHERE 1=1");
            sb.AppendLine(@") AS r");



            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("dfs", "r.dfs");
            DefaultViewColumns.Add("drilling_date", "r.drilling_date");
            DefaultViewColumns.Add("daily_cost", "r.daily_cost");
            DefaultViewColumns.Add("actual_depth", "r.actual_depth");
            DefaultViewColumns.Add("cumm_cost", "r.cumm_cost");
            DefaultViewColumns.Add("total_cost", "r.total_cost");
            DefaultViewColumns.Add("usage_percentage", "r.usage_percentage");
            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("drilling_date", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
