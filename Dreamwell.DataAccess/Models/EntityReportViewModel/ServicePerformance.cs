﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class ReportServicePerformanceModel
    {
        public string recordId { get; set; }
        public string[] ayear { get; set; }
    }

    public class ServicePerformanceNptByYear {
       public string name { get; set; }
       public decimal[] data { get; set; }

    }


    public class ServicePerformanceFilterWell
    {
        public string[] well_list { get; set; }

    }

    public partial class ServicePerformanceOperationalModel
    {
        public decimal total_npt { get; set; }
        public int total_well { get; set; }
        public decimal total_opr { get; set; }

        public string syear { get; set; }

        public decimal npt_in_day
        {
            get
            {
                decimal ret = 0;

                if (total_npt > 0)
                {
                    ret = Math.Round(Convert.ToDecimal(total_npt / 24), 2);
                }

                return ret;
            }
        }

        public decimal opr_in_day
        {
            get
            {
                decimal ret = 0;

                if (total_opr > 0)
                {
                    ret = Math.Round(Convert.ToDecimal(total_opr / 24), 2);
                }

                return ret;
            }
        }

        public int npt_by_totalday
        {
            get
            {
                int ret = 0;

                if (opr_in_day > 0)
                {
                    ret = Convert.ToInt32(100 * npt_in_day / opr_in_day);
                }

                return ret;
            }
        }

       
    }
    public partial class vw_service_performance
    {
        public string id { get; set; }
        public string field_id { get; set; }
        public string well_name { get; set; }
        public string well_id { get; set; }
        public string hole_name { get; set; }
        public string uom_depth { get; set; }
        public decimal plan_depth { get; set; }
        public decimal actual_depth { get; set; }
        public string uom_volume { get; set; }
        public decimal plan_volume { get; set; }
        public decimal actual_volume { get; set; }
        public decimal plan_cost { get; set; }
        public decimal actual_mud_cost { get; set; }
        public decimal actual_build { get; set; }

        public decimal plan_mudcostdepth
        {
            get
            {
                decimal ret = 0;

                if (plan_depth > 0)
                {
                    ret = Math.Round(Convert.ToDecimal(Convert.ToDouble(plan_cost) / Convert.ToDouble(plan_depth)), 2);
                }

                return ret;
            }
        }


        public decimal actual_bbl_mud
        {
            get
            {
                decimal ret = 0;

                if (actual_depth > 0)
                {
                    ret = Math.Round(Convert.ToDecimal(Convert.ToDouble(actual_build) / Convert.ToDouble(actual_depth)), 3);
                }

                return ret;
            }
        }


        public decimal actual_mudcostdepth
        {
            get
            {
                decimal ret = 0;

                if (actual_depth > 0)
                {
                    ret = Math.Round(Convert.ToDecimal(Convert.ToDouble(actual_mud_cost) / Convert.ToDouble(actual_depth)), 2);
                }

                return ret;
            }
        }

        public decimal plan_mudcostvolume
        {
            get
            {
                decimal ret = 0;

                if (plan_volume > 0)
                {
                    ret = Math.Round(Convert.ToDecimal(Convert.ToDouble(plan_cost) / Convert.ToDouble(plan_volume)), 2);
                }

                return ret;
            }
        }

        public decimal actual_mudcostvolume
        {
            get
            {
                decimal ret = 0;

                if (actual_volume > 0)
                {
                    ret = Math.Round(Convert.ToDecimal(Convert.ToDouble(actual_mud_cost) / Convert.ToDouble(actual_volume)), 2);
                }

                return ret;
            }
        }
    }

    public partial class vw_service_performance_group
    {
        public string hole_name { get; set; }
        public List<vw_service_performance> data { get; set; }

        public decimal dhole_name
        {
            get
            {
                decimal ret = 0;
                decimal.TryParse((string.IsNullOrEmpty(hole_name) ? "" : hole_name.Replace("\"", "").Replace("'", "")), out ret);
                return ret;
            }
        }

    }

    public partial class vw_service_performance : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_service_performance()
        {
            EntityName = hole_and_casing.GetTableName();
            EntityDisplayName = hole_and_casing.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            //sb.AppendLine(@"SELECT hac.hole_name, u.uom_code, we.well_name, r.hole_depth, ISNULL(r.plan_cost,0) as plan_cost ");
            //sb.AppendLine(@"FROM dbo.well_hole_and_casing AS r ");
            //sb.AppendLine(@"INNER JOIN hole_and_casing AS hac ON r.hole_and_casing_id=hac.id ");
            //sb.AppendLine(@"INNER JOIN well AS we ON r.well_id=we.id ");
            //sb.AppendLine(@"INNER JOIN well_object_uom_map as wo ON r.well_id=wo.well_id AND wo.[object_name]='hole depth' ");
            //sb.AppendLine(@"INNER JOIN field AS fi ON we.field_id=fi.id ");
            //sb.AppendLine(@"INNER JOIN uom as u ON wo.uom_id=u.id ");
            sb.AppendLine(@"SELECT r.id, w.field_id, w.well_name, r.well_id, hc.hole_name ");
            sb.AppendLine(@", dbo.ufn_get_uom_well(w.id, 'hole depth') as uom_depth, ISNULL(r.hole_depth,0) as plan_depth , ISNULL(actual_depth.depth,0) as actual_depth ");
            sb.AppendLine(@", dbo.ufn_get_uom_well(w.id, 'hole volume') as uom_volume, ISNULL(r.plan_volume,0) as plan_volume, ISNULL(actual_volume.total_volume_start,0) as actual_volume ");
            sb.AppendLine(@", ISNULL(r.plan_cost,0) as plan_cost, ISNULL(mc.actual_mud_cost,0) actual_mud_cost, ISNULL(ai.actual_build,0) actual_build ");
            sb.AppendLine(@"FROM well_hole_and_casing r ");
            sb.AppendLine(@"INNER JOIN well w ON w.id = r.well_id ");
            sb.AppendLine(@"INNER JOIN hole_and_casing hc ON hc.id = r.hole_and_casing_id ");
            sb.AppendLine(@"OUTER APPLY ( ");
            sb.AppendLine(@"	SELECT TOP 1 doa.depth ");
            sb.AppendLine(@"	FROM drilling_operation_activity doa ");
            sb.AppendLine(@"	INNER JOIN drilling_hole_and_casing dhc ON dhc.id = doa.drilling_hole_and_casing_id ");
            sb.AppendLine(@"	WHERE dhc.well_hole_and_casing_id = r.id ");
            sb.AppendLine(@"	ORDER BY doa.created_on desc ");
            sb.AppendLine(@") AS actual_depth");
            sb.AppendLine(@"OUTER APPLY ( ");
            sb.AppendLine(@"	SELECT SUM(dtv.total_volume_start) as total_volume_start ");
            sb.AppendLine(@"	FROM drilling_total_volume dtv ");
            sb.AppendLine(@"	WHERE dtv.well_hole_casing_id = r.id ");
            sb.AppendLine(@") AS actual_volume ");
            sb.AppendLine(@"OUTER APPLY (select sum(mud_cost) as actual_mud_cost from drilling_total_volume WHERE well_hole_casing_id=r.id) mc ");
            sb.AppendLine(@"OUTER APPLY (select sum(build) as actual_build from drilling_total_volume WHERE well_hole_casing_id=r.id) ai ");



            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("field_id", "w.field_id");
            DefaultViewColumns.Add("well_name", "we.well_name");
            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("hole_name", "hc.hole_name");
            DefaultViewColumns.Add("plan_depth", "ISNULL(r.hole_depth,0)");
            DefaultViewColumns.Add("actual_depth", "ISNULL(actual.depth,0)");
            DefaultViewColumns.Add("plan_volume", "ISNULL(r.plan_volume,0)");
            DefaultViewColumns.Add("actual_volume", "ISNULL(actual_volume.total_volume_start,0)");
            DefaultViewColumns.Add("plan_cost", "ISNULL(r.plan_cost,0)");
            DefaultViewColumns.Add("actual_mud_cost", "ISNULL(mc.actual_mud_cost,0)");
            DefaultViewColumns.Add("actual_build", "ISNULL(ai.actual_build,0)");
            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("well_name", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
