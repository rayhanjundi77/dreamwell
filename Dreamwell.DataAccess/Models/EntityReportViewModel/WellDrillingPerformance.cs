﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_well_drilling_performance
    {
        public string asset_id { get; set; } //(nvarchar(50), null)
        public string asset_code { get; set; } //(nvarchar(50), null)

        public string asset_name { get; set; } //(nvarchar(50), null)
        public string field_id { get; set; } //(nvarchar(50), null)
        public string field_name { get; set; } //(nvarchar(50), null)
        public string well_name { get; set; } //(nvarchar(50), null)

        public decimal? pt_hr { get; set; }
        public decimal? npt_hr { get; set; }
        public decimal? cummulative_cost_plan { get; set; }
        public decimal? depth_plan { get; set; }
        public decimal? cummulative_cost_actual { get; set; }
        public int? depth_actual { get; set; }

    }

    public partial class vw_well_drilling_performance : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_well_drilling_performance()
        {
            EntityName = field.GetTableName();
            EntityDisplayName = field.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"	r.asset_id,");
            sb.AppendLine(@"	a.asset_code,");
            sb.AppendLine(@"	a.asset_name,	");
            sb.AppendLine(@"	r.id as field_id, r.field_name ,");
            sb.AppendLine(@"	pthr.interval as pt_hr,");
            sb.AppendLine(@"	npthr.interval as npt_hr,");
            sb.AppendLine(@"	w.well_name,");
            sb.AppendLine(@"	well_plan.cummulative_cost_plan,");
            sb.AppendLine(@"	well_plan.depth_plan,");
            sb.AppendLine(@"	well_actual.cummulative_cost_actual,");
            sb.AppendLine(@"	actual_depth.depth_actual");
            sb.AppendLine(@"	");
            sb.AppendLine(@"FROM dbo.field r");
            sb.AppendLine(@"INNER JOIN dbo.asset a ON r.asset_id=a.id");
            sb.AppendLine(@"INNER JOIN dbo.well w ON w.field_id =r.id");
            sb.AppendLine(@"OUTER APPLY (");
            sb.AppendLine(@"    SELECT ");
            sb.AppendLine(@"		ISNULL(SUM(DATEDIFF(MINUTE, doa.operation_start_date, doa.operation_end_date) / 60.0),0) AS interval ");
            sb.AppendLine(@"	FROM dbo.drilling_operation_activity doa");
            sb.AppendLine(@"	INNER JOIN dbo.drilling d ON doa.drilling_id=d.id");
            sb.AppendLine(@"	INNER JOIN dbo.iadc ON doa.iadc_id=iadc.id");
            sb.AppendLine(@"	WHERE d.well_id=w.id AND iadc.type='1' AND YEAR(d.drilling_date) IN (@years)");
            sb.AppendLine(@") AS pthr");
            sb.AppendLine(@"OUTER APPLY (");
            sb.AppendLine(@"    SELECT ");
            sb.AppendLine(@"		ISNULL(SUM(DATEDIFF(MINUTE, doa.operation_start_date, doa.operation_end_date) / 60.0),0) AS interval ");
            sb.AppendLine(@"	FROM dbo.drilling_operation_activity doa");
            sb.AppendLine(@"	INNER JOIN dbo.drilling d ON doa.drilling_id=d.id");
            sb.AppendLine(@"	INNER JOIN dbo.iadc ON doa.iadc_id=iadc.id");
            sb.AppendLine(@"	WHERE d.well_id=w .id AND iadc.type='2' AND YEAR(d.drilling_date) IN (@years)");
            sb.AppendLine(@"");
            sb.AppendLine(@") AS npthr");
            sb.AppendLine(@"OUTER APPLY (");
            sb.AppendLine(@"	SELECT ");
            sb.AppendLine(@"		COALESCE(SUM(tvd_plan.cumm_cost),0) AS cummulative_cost_plan,");
            sb.AppendLine(@"		COALESCE(SUM(tvd_plan.depth),0) AS depth_plan");
            sb.AppendLine(@"");
            sb.AppendLine(@"	FROM dbo.tvd_plan ");
            sb.AppendLine(@"	WHERE tvd_plan.well_id=w.id AND YEAR(w.spud_date) IN (@years)");
            sb.AppendLine(@"");
            sb.AppendLine(@") AS well_plan");
            sb.AppendLine(@"OUTER APPLY (");
            sb.AppendLine(@"	SELECT ");
            sb.AppendLine(@"		COALESCE(SUM(daily.daily_cost),0) AS cummulative_cost_actual");
            sb.AppendLine(@"	FROM dbo.drilling d ");
            sb.AppendLine(@"	OUTER APPLY (SELECT daily_cost FROM [dbo].[udf_get_daily_cost](d.id)) AS daily");
            sb.AppendLine(@"	WHERE d.well_id=w.id AND YEAR(d.drilling_date) IN (@years)");
            sb.AppendLine(@") AS well_actual");
            sb.AppendLine(@"");
            sb.AppendLine(@"OUTER APPLY (");
            sb.AppendLine(@"	SELECT COALESCE(SUM(w.current_depth_md),0) AS depth_actual FROM(");
            sb.AppendLine(@"		SELECT ");
            sb.AppendLine(@"				w.id, COALESCE(MAX(d.current_depth_md),0) AS current_depth_md");
            sb.AppendLine(@"		FROM dbo.drilling d ");
            sb.AppendLine(@"		WHERE d.well_id = w.id AND YEAR(d.drilling_date) IN (@years)");
            sb.AppendLine(@"");
            sb.AppendLine(@"	) AS w");
            sb.AppendLine(@") AS actual_depth ");

            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("asset_id", "r.asset_id");
            DefaultViewColumns.Add("asset_code", "a.asset_code");
            DefaultViewColumns.Add("asset_name", "a.asset_name");
            DefaultViewColumns.Add("field_id", "r.id");
            DefaultViewColumns.Add("field_name", "r.field_name");
            DefaultViewColumns.Add("pt_hr", "pthr.interval");
            DefaultViewColumns.Add("npt_hr", "npthr.interval");
            DefaultViewColumns.Add("cummulative_cost_plan", "well_plan.cummulative_cost_plan");
            DefaultViewColumns.Add("depth_plan", "well_plan.depth_plan");
            DefaultViewColumns.Add("cummulative_cost_actual", "well_actual.cummulative_cost_actual");
            DefaultViewColumns.Add("depth_actual", "actual_depth.depth_actual");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("field_name", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@years", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
