﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_report_hole_casing_actual
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string well_hole_and_casing_id { get; set; } //(nvarchar(50), null)
        public DateTime created_on { get; set; }
        public decimal hole_name { get; set; } //(nvarchar(50), null)
        public decimal depth { get; set; } //(nvarchar(50), null)
        public decimal casing_name { get; set; } //(nvarchar(50), null)
        public decimal casing_setting_depth { get; set; } //(nvarchar(50), null)
        public string casing_type { get; set; } //(nvarchar(50), null)
    }

    public partial class vw_report_hole_casing_actual : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_report_hole_casing_actual()
        {
            EntityName = hole_and_casing.GetTableName();
            EntityDisplayName = hole_and_casing.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"r.id,");
            sb.AppendLine(@"r.owner_id,");
            sb.AppendLine(@"r.created_on,");
            sb.AppendLine(@"r.well_hole_and_casing_id, ");
            sb.AppendLine(@"h.hole_name, ");
            sb.AppendLine(@"d.depth, ");
            sb.AppendLine(@"h.casing_name, ");
            sb.AppendLine(@"w.casing_setting_depth, ");
            sb.AppendLine(@"w.casing_type");
            sb.AppendLine(@"FROM drilling_hole_and_casing r");
            sb.AppendLine(@"INNER JOIN well_hole_and_casing AS w ON w.id = r.well_hole_and_casing_id");
            sb.AppendLine(@"INNER JOIN hole_and_casing h ON h.id = w.hole_and_casing_id");
            sb.AppendLine(@"CROSS APPLY (");
            sb.AppendLine(@"		SELECT TOP 1 doa.depth");
            sb.AppendLine(@"		FROM drilling_operation_activity doa");
            sb.AppendLine(@"		INNER JOIN drilling d ON d.id = doa.drilling_id");
            sb.AppendLine(@"		WHERE doa.drilling_hole_and_casing_id = r.id");
            sb.AppendLine(@"		ORDER BY d.drilling_date DESC, doa.created_on DESC");
            sb.AppendLine(@"	) AS d");



            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("well_hole_and_casing_id", "r.well_hole_and_casing_id");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("hole_name", "h.hole_name");
            DefaultViewColumns.Add("depth", "d.depth");
            DefaultViewColumns.Add("casing_name", "h.casing_name");
            DefaultViewColumns.Add("casing_setting_depth", "w.casing_setting_depth");
            DefaultViewColumns.Add("casing_type", "w.casing_type");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("created_on", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
