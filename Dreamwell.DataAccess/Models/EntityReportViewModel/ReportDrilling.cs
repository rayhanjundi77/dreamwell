﻿using CommonTools;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_report_drilling
    {
        [ResultColumn]
        public List<drilling_operation_activity> operation_activity { get; set; }
    }

    public partial class vw_report_drilling
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public int dfs { get; set; }
        public DateTime drilling_date { get; set; }
        public decimal daily_cost { get; set; }
        public decimal cummulative_cost { get; set; }
        public decimal current_depth_tvd { get; set; }
        public decimal current_depth_md { get; set; }
        public string drilling_operation_activity_id { get; set; }
        public string description { get; set; }
    }

    public partial class vw_report_drilling : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_report_drilling()
        {
            EntityName = drilling.GetTableName();
            EntityDisplayName = drilling.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            //sb.AppendLine(@"SELECT r.id, r.owner_id, r.dfs, r.drilling_date, daily.daily_cost, COALESCE(r.current_depth_tvd, 0) AS current_depth_tvd, COALESCE(r.current_depth_md, 0) AS current_depth_md ");
            //sb.AppendLine(@"FROM drilling r");
            //sb.AppendLine(@"CROSS APPLY (");
            //sb.AppendLine(@"				SELECT COALESCE(SUM(dc.unit * cd.unit_price / cd.current_rate_value), 0) AS daily_cost");
            //sb.AppendLine(@"				FROM daily_cost AS dc");
            //sb.AppendLine(@"				INNER JOIN afe_contract ac ON ac.id = dc.afe_contract_id");
            //sb.AppendLine(@"				INNER JOIN contract_detail cd ON cd.id = ac.contract_detail_id");
            //sb.AppendLine(@"				WHERE dc.drilling_id = r.id");
            //sb.AppendLine(@"			) AS daily");

            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"	r.id, ");
            sb.AppendLine(@"	r.owner_id, ");
            sb.AppendLine(@"	r.cummulative_cost, ");
            sb.AppendLine(@"	r.dfs, ");
            sb.AppendLine(@"	r.drilling_date, ");
            sb.AppendLine(@"	daily.daily_cost, ");
            sb.AppendLine(@"	COALESCE(r.current_depth_tvd, 0) AS current_depth_tvd, ");
            sb.AppendLine(@"	COALESCE(r.current_depth_md, 0) AS current_depth_md,");
            sb.AppendLine(@"	d.id AS drilling_operation_activity_id,");
            sb.AppendLine(@"	d.[description]");
            sb.AppendLine(@"FROM drilling r");
            sb.AppendLine(@"CROSS APPLY (");
            sb.AppendLine(@"				SELECT COALESCE(SUM(dc.unit * cd.unit_price / cd.current_rate_value), 0) AS daily_cost");
            sb.AppendLine(@"				FROM daily_cost AS dc");
            sb.AppendLine(@"				INNER JOIN afe_contract ac ON ac.id = dc.afe_contract_id");
            sb.AppendLine(@"				INNER JOIN contract_detail cd ON cd.id = ac.contract_detail_id");
            sb.AppendLine(@"				WHERE dc.drilling_id = r.id");
            sb.AppendLine(@"			) AS daily");
            sb.AppendLine(@"LEFT OUTER JOIN drilling_operation_activity d");
            sb.AppendLine(@"  ON r.id = d.drilling_id");
            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("dfs", "r.dfs");
            DefaultViewColumns.Add("drilling_date", "r.drilling_date");
            DefaultViewColumns.Add("daily_cost", "daily.daily_cost");
            DefaultViewColumns.Add("cummulative_cost", "r.cummulative_cost");
            DefaultViewColumns.Add("current_depth_tvd", "COALESCE(r.current_depth_tvd, 0)");
            DefaultViewColumns.Add("current_depth_md", "COALESCE(r.current_depth_md, 0)");
            DefaultViewColumns.Add("drilling_operation_activity_id", "d.id");
            DefaultViewColumns.Add("[description]", "d.[description]");
            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("drilling_date", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
