﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_report_cummulative_cost
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string well_name { get; set; } //(nvarchar(50), null)
        public int afe_cost { get; set; }
        public int total_cost { get; set; }
    }

    public partial class vw_report_cummulative_cost : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_report_cummulative_cost()
        {
            EntityName = well.GetTableName();
            EntityDisplayName = well.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT r.id, r.well_name, r.owner_id, r.afe_cost, p.total_cost");
            sb.AppendLine(@"FROM well r");
            sb.AppendLine(@"OUTER APPLY (	");
            sb.AppendLine(@"		SELECT COALESCE(SUM(cd.unit_price / cd.current_rate_value * dc.unit), 0) AS total_cost");
            sb.AppendLine(@"		FROM drilling d");
            sb.AppendLine(@"		INNER JOIN daily_cost dc ON dc.drilling_id = d.id");
            sb.AppendLine(@"		INNER JOIN afe_contract ac ON ac.id = dc.afe_contract_id");
            sb.AppendLine(@"		INNER JOIN contract_detail cd ON cd.id = ac.contract_detail_id");
            sb.AppendLine(@"		WHERE d.well_id = r.id");
            sb.AppendLine(@"			) AS p");
            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("well_name", "r.well_name");
            DefaultViewColumns.Add("afe_cost", "r.afe_cost");
            DefaultViewColumns.Add("total_cost", "p.total_cost");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("id", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
