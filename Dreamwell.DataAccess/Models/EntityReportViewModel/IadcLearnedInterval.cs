﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_iadc_learned_interval
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string well_name { get; set; } //(nvarchar(50), null)
        public string field_id { get; set; } //(nvarchar(50), null)
        public decimal interval { get; set; }
        public decimal actual_cost { get; set; }
        public decimal actual_depth { get; set; }
    }

    public partial class vw_iadc_learned_interval : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_iadc_learned_interval()
        {
            EntityName = well.GetTableName();
            EntityDisplayName = well.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.Append(@"SELECT r.id, r.owner_id, r.well_name, r.field_id, r.interval, r.actual_cost, cast(round(r.actual_depth,2) as numeric(36,2)) as actual_depth
                        FROM (
                            SELECT r.id, r.owner_id, r.well_name, r.field_id, COALESCE(i.interval, 0) AS interval, daily.daily_cost AS actual_cost, 
                    case 
                        when j.uom_code = 'm' and @1= 'm' then COALESCE(p.current_depth_md, 0)
                        when j.uom_code = 'm' and @1= 'ft' then COALESCE(p.current_depth_md, 0)*3.281
                        when j.uom_code = 'ft' and @1= 'ft' then COALESCE(p.current_depth_md, 0)
                        when j.uom_code = 'ft' and @1= 'm' then COALESCE(p.current_depth_md, 0)/3.281
                    end as actual_depth
                    FROM well r
                    OUTER APPLY (
                        SELECT uom.uom_code
                        FROM uom 
                        INNER JOIN well_object_uom_map AS wuom ON wuom.uom_id = uom.id
                        WHERE wuom.well_id = r.id and object_name = 'Depth'
                    ) AS j 
                    OUTER APPLY (
                        SELECT SUM(DATEDIFF(MINUTE, do.operation_start_date, do.operation_end_date) / 60.0) AS interval
                        FROM drilling_operation_activity do
                        INNER JOIN drilling AS d ON d.id = do.drilling_id
                        WHERE d.well_id = r.id AND do.iadc_id = @0
                    ) AS i
                    CROSS APPLY (
                        SELECT COALESCE(SUM(dc.unit * cd.unit_price / cd.current_rate_value), 0) AS daily_cost
                        FROM daily_cost AS dc
                        INNER JOIN afe_contract ac ON ac.id = dc.afe_contract_id
                        INNER JOIN contract_detail cd ON cd.id = ac.contract_detail_id
                        INNER JOIN afe a ON a.id = dc.afe_id
                        WHERE a.well_id = r.id
                    ) AS daily
                    OUTER APPLY (	
                        SELECT TOP 1 d.current_depth_md
                        FROM drilling d
                        WHERE d.well_id = r.id
                        ORDER BY d.drilling_date DESC
                    ) AS p
                ) AS r");
            //sb.AppendLine(@"SELECT r.id, r.owner_id, r.well_name, r.field_id, r.interval, r.actual_cost, r.actual_depth");
            //sb.AppendLine(@"FROM (");
            //sb.AppendLine(@"	SELECT r.id, r.owner_id, r.well_name, r.field_id, COALESCE(i.interval, 0) AS interval, daily.daily_cost AS actual_cost, COALESCE(p.current_depth_md, 0) AS actual_depth");
            //sb.AppendLine(@"	FROM well r");
            //sb.AppendLine(@"	OUTER APPLY (");
            //sb.AppendLine(@"		SELECT SUM(DATEDIFF(MINUTE, do.operation_start_date, do.operation_end_date) / 60.0) AS interval");
            //sb.AppendLine(@"		FROM drilling_operation_activity do");
            //sb.AppendLine(@"		INNER JOIN drilling AS d ON d.id = do.drilling_id");
            //sb.AppendLine(@"		WHERE d.well_id = r.id AND do.iadc_id = @0");
            //sb.AppendLine(@"	) AS i");
            //sb.AppendLine(@"	CROSS APPLY (");
            //sb.AppendLine(@"		SELECT COALESCE(SUM(dc.unit * cd.unit_price / cd.current_rate_value), 0) AS daily_cost");
            //sb.AppendLine(@"		FROM daily_cost AS dc");
            //sb.AppendLine(@"		INNER JOIN afe_contract ac ON ac.id = dc.afe_contract_id");
            //sb.AppendLine(@"		INNER JOIN contract_detail cd ON cd.id = ac.contract_detail_id");
            //sb.AppendLine(@"		INNER JOIN afe a ON a.id = dc.afe_id");
            //sb.AppendLine(@"		WHERE a.well_id = r.id");
            //sb.AppendLine(@"	) AS daily");
            //sb.AppendLine(@"	OUTER APPLY (	");
            //sb.AppendLine(@"		SELECT TOP 1 d.current_depth_md");
            //sb.AppendLine(@"		FROM drilling d");
            //sb.AppendLine(@"		WHERE d.well_id = r.id");
            //sb.AppendLine(@"		ORDER BY d.drilling_date DESC");
            //sb.AppendLine(@"	) AS p");
            //sb.AppendLine(@") AS r");


            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("well_name", "r.well_name");
            DefaultViewColumns.Add("field_id", "r.field_id");
            DefaultViewColumns.Add("interval", "r.interval");
            DefaultViewColumns.Add("actual_cost", "r.actual_cost");
            DefaultViewColumns.Add("actual_depth", "r.actual_depth");
            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("well_name", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
