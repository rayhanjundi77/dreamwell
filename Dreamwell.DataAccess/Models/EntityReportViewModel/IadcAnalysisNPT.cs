﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_iadc_analysis_npt
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string description { get; set; } //(nvarchar(50), null)
        public string iadc_code { get; set; } //(nvarchar(50), null)
        public string parent_code { get; set; } //(nvarchar(50), null)
        public string type { get; set; }
        public int total { get; set; }
        public decimal interval { get; set; }
        public int iyear { get; set; }

        public decimal days
        {
            get
            {
                decimal result = 0;
                if (interval > 0)
                {
                    result = Math.Round(interval / 24, 2);
                }
                return result;
            }
        }

        public decimal hours
        {
            get
            {
                decimal result = 0;
                if (interval > 0)
                {
                    result = Math.Round(interval, 2);
                }
                return result;
            }
        }

    }

    public partial class vw_iadc_analysis_npt : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_iadc_analysis_npt()
        {
            EntityName = afe.GetTableName();
            EntityDisplayName = afe.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT r.id, r.owner_id, r. description, r.type, r.total, r.interval ");
            sb.AppendLine(@"FROM (");
            sb.AppendLine(@"	SELECT DISTINCT r.id, r.owner_id, r. description, type = 'NPT', m.total AS total, i.interval AS interval");
            sb.AppendLine(@"	FROM iadc r");
            sb.AppendLine(@"	INNER JOIN drilling_operation_activity do ON r.id = do.iadc_id");
            sb.AppendLine(@"	INNER JOIN drilling_hole_and_casing dhc ON dhc.id = do.drilling_hole_and_casing_id");
            sb.AppendLine(@"	INNER JOIN drilling d ON d.id = do.drilling_id");
            sb.AppendLine(@"	OUTER APPLY (");
            sb.AppendLine(@"		SELECT COUNT(*) AS total");
            sb.AppendLine(@"		FROM iadc i");
            sb.AppendLine(@"		INNER JOIN drilling_operation_activity do ON r.id = do.iadc_id");
            sb.AppendLine(@"		WHERE i.type = 2 AND i.id = r.id AND do.drilling_hole_and_casing_id = dhc.id");
            sb.AppendLine(@"	) AS m");
            sb.AppendLine(@"	OUTER APPLY (");
            sb.AppendLine(@"		SELECT SUM(DATEDIFF(MINUTE, do.operation_start_date, do.operation_end_date) / 60.0) AS interval");
            sb.AppendLine(@"		FROM drilling_operation_activity do");
            sb.AppendLine(@"		INNER JOIN drilling AS d ON d.id = do.drilling_id");
            sb.AppendLine(@"		WHERE do.iadc_id = r.id AND do.drilling_hole_and_casing_id = dhc.id");
            sb.AppendLine(@"	) AS i");
            sb.AppendLine(@"	WHERE r.type = 2 AND dhc.id = @0");
            sb.AppendLine(@") AS r");
            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("description", "r.description");
            DefaultViewColumns.Add("type", "r.type");
            DefaultViewColumns.Add("total", "r.total");
            DefaultViewColumns.Add("interval", "r.interval");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("description", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
