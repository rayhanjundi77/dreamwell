﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_material_ussage_by_field
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string material_name { get; set; } //(nvarchar(50), null)
        public string field_id { get; set; } //(nvarchar(50), null)
        public string field_name { get; set; } //(nvarchar(50), null)
        public decimal? total_unit { get; set; } //(nvarchar(50), null)
        public decimal? unit_price { get; set; } //(nvarchar(50), null)
        public decimal? total_price { get; set; } //(nvarchar(50), null)

    }

    public partial class vw_material_ussage_by_field : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_material_ussage_by_field()
        {
            EntityName = material.GetTableName();
            EntityDisplayName = material.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();

            sb.AppendLine(@"SELECT r.* FROM ( ");

            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"	r.material_id AS id,");
            sb.AppendLine(@"	r.material_name,");
            sb.AppendLine(@"	f.id AS field_id,");
            sb.AppendLine(@"	f.field_name,");
            sb.AppendLine(@"	SUM(r.unit) AS total_unit,");
            sb.AppendLine(@"	AVG(r.unit_price) AS unit_price,");
            sb.AppendLine(@"	SUM(r.actual_price_sub_total) AS total_price, ");
            sb.AppendLine(@"	MAX(r.owner_id) AS owner_id");
            
            sb.AppendLine(@"");
            sb.AppendLine(@"FROM (");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  r.id,");
            sb.AppendLine(@"  r.created_by,");
            sb.AppendLine(@"  r.created_on,");
            sb.AppendLine(@"  r.modified_by,");
            sb.AppendLine(@"  r.modified_on,");
            sb.AppendLine(@"  r.approved_by,");
            sb.AppendLine(@"  r.approved_on,");
            sb.AppendLine(@"  r.is_active,");
            sb.AppendLine(@"  r.is_locked,");
            sb.AppendLine(@"  r.is_default,");
            sb.AppendLine(@"  well.owner_id,");
            sb.AppendLine(@"  r.organization_id,");
            sb.AppendLine(@"  r.drilling_id,");
            sb.AppendLine(@"  r.afe_id,");
            sb.AppendLine(@"  d.drilling_date,");
            sb.AppendLine(@"  r.afe_contract_id,");
            sb.AppendLine(@"  r.material_id,");
            sb.AppendLine(@"  m.description AS material_name,");
            sb.AppendLine(@"  m.afe_line_id,");
            sb.AppendLine(@"  al.description AS afe_line_description,");
            sb.AppendLine(@"  r.unit,");
            sb.AppendLine(@"  cd.unit AS contract_detail_unit,");
            sb.AppendLine(@"  cd.unit_price,");
            sb.AppendLine(@"  cd.unit_price * r.unit AS actual_price_sub_total,");
            sb.AppendLine(@"  well.field_id");

            sb.AppendLine(@"");
            sb.AppendLine(@"FROM dbo.daily_cost AS r");
            sb.AppendLine(@"INNER JOIN dbo.drilling AS d");
            sb.AppendLine(@"  ON r.drilling_id = d.id");
            sb.AppendLine(@"INNER JOIN dbo.well");
            sb.AppendLine(@"  ON d.well_id=well.id");
            sb.AppendLine(@"");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.afe_contract AS a");
            sb.AppendLine(@"  ON r.afe_contract_id = a.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.contract_detail AS cd");
            sb.AppendLine(@"  ON a.contract_detail_id = cd.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.material AS m");
            sb.AppendLine(@"  ON cd.material_id = m.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.afe_line AS al");
            sb.AppendLine(@"  ON m.afe_line_id = al.id");
            sb.AppendLine(@"");
            sb.AppendLine(@"WHERE YEAR(d.drilling_date) IN (@years)");
            sb.AppendLine(@") AS r");
            sb.AppendLine(@"INNER JOIN dbo.field f ON r.field_id=f.id");
            sb.AppendLine(@"GROUP BY r.material_id,r.material_name,f.id,f.field_name ");

            sb.AppendLine(@") AS r ");

            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("material_name", "r.material_name");
            DefaultViewColumns.Add("field_id", "r.field_id");
            DefaultViewColumns.Add("field_name", "r.field_name");
            DefaultViewColumns.Add("total_unit", "r.total_unit");
            DefaultViewColumns.Add("unit_price", "r.unit_price");
            DefaultViewColumns.Add("total_price", "r.total_price");


            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("material_name", "ASC");

            #endregion

            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@years", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
