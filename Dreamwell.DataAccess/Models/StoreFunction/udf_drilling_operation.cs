﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Models.StoreFunction
{
    
    public partial class udf_drilling_operation 
    {
        [ResultColumn]
        public string operation_period_desc { get; set; }
        [ResultColumn]
        public string operation_current_desc { get; set; }
        [ResultColumn]
        public string operation_planned_desc { get; set; }
        public static dreamwellRepo repo { get { return dreamwellRepo.GetInstance(); } }


        const bool IsProcedure = false;

        /// <summary>This method Store Function / Procedure
        ///    the given parameterized 
        /// <example>For example:
        /// <code>
        ///   [dbo].[udf_drilling_operation](@drilling_id  NVARCHAR(50))
        ///    
        /// </code>
        /// </example>
        /// </summary>

        public static Sql GetSql(Dictionary<string, object> args)
        {
            var sql = Sql.Builder.Append("SELECT * FROM [dbo].[udf_drilling_operation] (");
            int idx = 1;
            foreach (var param in args)
            {
                if (idx == args.Count)
                    sql.Append(" @0 ", param.Value);
                else
                    sql.Append(" @0 ,", param.Value);
                idx++;
            }
            sql.Append(")");
            return sql;
        }

        /// <summary>This method Store Function / Procedure
        ///    the given parameterized 
        /// <example>For example:
        /// <code>
        ///   [dbo].[udf_drilling_operation](@drilling_id  NVARCHAR(50))
        ///    
        /// </code>
        /// </example>
        /// </summary>
        public static udf_drilling_operation FirstOrDefault(Dictionary<string, object> arg)
        {
            udf_drilling_operation result = Activator.CreateInstance<udf_drilling_operation>();
            if (!IsProcedure)
            {
                return repo.FirstOrDefault<udf_drilling_operation>(GetSql(arg));
            }
        }


        /// <summary>This method Store Function / Procedure
        ///    the given parameterized 
        /// <example>For example:
        /// <code>
        ///   [dbo].[udf_drilling_operation](@drilling_id  NVARCHAR(50))
        ///    
        /// </code>
        /// </example>
        /// </summary>
        public static List<udf_drilling_operation> Fetch(Dictionary<string, object> arg)
        {
            List<udf_drilling_operation> result = new List<udf_drilling_operation>();
            if (!IsProcedure)
            {
                return repo.Fetch<udf_drilling_operation>(GetSql(arg));
            }
        }


    }
    
}
