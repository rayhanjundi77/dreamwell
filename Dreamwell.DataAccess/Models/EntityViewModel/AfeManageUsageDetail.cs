﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_afe_manage_usage_detail
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string created_by { get; set; } //(nvarchar(50), null)
        public DateTime? created_on { get; set; } //(datetime2(7), null)
        public string modified_by { get; set; } //(nvarchar(50), null)
        public DateTime? modified_on { get; set; } //(datetime2(7), null)
        public string approved_by { get; set; } //(nvarchar(50), null)
        public DateTime? approved_on { get; set; } //(datetime2(7), null)
        public bool? is_active { get; set; } //(bit, null)
        public bool? is_locked { get; set; } //(bit, null)
        public bool? is_default { get; set; } //(bit, null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string afe_contract_id { get; set; } //(nvarchar(50), not null)
        public string contract_id { get; set; } //(nvarchar(50), not null)
        public string contract_no { get; set; } //(nvarchar(50), not null)
        public string afe_id { get; set; } //(nvarchar(50), not null)
        public string afe_line_id { get; set; } //(nvarchar(50), not null)
        public string afe_line_description { get; set; } //(varchar(256), null)
        public string material_id { get; set; } //(varchar(256), null)
        public string material_name { get; set; } //(varchar(256), null)
        public string currency_code { get; set; } //(varchar(256), null)
        public Decimal? current_rate_value { get; set; } //(varchar(256), null)
        public Decimal? unit { get; set; } //(varchar(256), null)
        public Decimal? unit_price { get; set; } //(varchar(256), null)
        public Decimal? total_price { get; set; } //(varchar(256), null)
        public Decimal? actual_price { get; set; } //(varchar(256), null)
        public Decimal? remaining_unit { get; set; } //(varchar(256), null)
        public Decimal? daily_unit { get; set; } //(varchar(256), null)
    }


    public partial class vw_afe_manage_usage_detail : IEntity, IBaseRecord, IEntityParameterize
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion



        static vw_afe_manage_usage_detail()
        {
            EntityName = material.GetTableName();
            EntityDisplayName = material.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"	r.id,");
            sb.AppendLine(@"	r.created_by,");
            sb.AppendLine(@"	r.created_on,");
            sb.AppendLine(@"	r.modified_by,");
            sb.AppendLine(@"	r.modified_on,");
            sb.AppendLine(@"	r.approved_by,");
            sb.AppendLine(@"	r.approved_on,");
            sb.AppendLine(@"	r.is_active,");
            sb.AppendLine(@"	r.is_locked,");
            sb.AppendLine(@"	r.is_default,");
            sb.AppendLine(@"	r.owner_id,");
            sb.AppendLine(@"	r.afe_contract_id,");
            sb.AppendLine(@"	r.contract_id,");
            sb.AppendLine(@"	r.contract_no,");
            sb.AppendLine(@"	r.afe_id,");
            sb.AppendLine(@"	r.afe_line_id,");
            sb.AppendLine(@"	r.afe_line_description,");
            sb.AppendLine(@"	r.material_id,");
            sb.AppendLine(@"	r.material_name,");
            sb.AppendLine(@"	r.currency_code,");
            sb.AppendLine(@"	r.current_rate_value,");
            sb.AppendLine(@"	r.unit,");
            sb.AppendLine(@"	r.actual_price,");
            sb.AppendLine(@"	r.remaining_unit,");
            sb.AppendLine(@"	r.daily_unit");
            sb.AppendLine(@"FROM (");
            sb.AppendLine(@"	SELECT   ");
            sb.AppendLine(@"		daily.id,");
            sb.AppendLine(@"		r.created_by,");
            sb.AppendLine(@"		r.created_on,");
            sb.AppendLine(@"		r.modified_by,");
            sb.AppendLine(@"		r.modified_on,");
            sb.AppendLine(@"		r.approved_by,");
            sb.AppendLine(@"		r.approved_on,");
            sb.AppendLine(@"		r.is_active,");
            sb.AppendLine(@"		r.is_locked,");
            sb.AppendLine(@"		r.is_default,");
            sb.AppendLine(@"		r.owner_id,");
            sb.AppendLine(@"		r.id AS afe_contract_id,");
            sb.AppendLine(@"		c.id AS contract_id,");
            sb.AppendLine(@"		c.contract_no,");
            sb.AppendLine(@"		r.afe_id,");
            sb.AppendLine(@"		m.afe_line_id,");
            sb.AppendLine(@"		a.description AS afe_line_description,");
            sb.AppendLine(@"		cd.material_id,");
            sb.AppendLine(@"		m.description AS material_name,");
            sb.AppendLine(@"		cu.currency_code,");
            sb.AppendLine(@"		cd.current_rate_value,");
            sb.AppendLine(@"		r.unit,");
            sb.AppendLine(@"		(cd.actual_price / cd.unit * r.unit) AS actual_price,");
            sb.AppendLine(@"		r.unit - COALESCE(usage.total_unit, 0) AS remaining_unit,");
            sb.AppendLine(@"		daily.unit AS daily_unit");
            sb.AppendLine(@"	FROM dbo.afe_contract r");
            sb.AppendLine(@"	INNER JOIN contract_detail AS cd");
            sb.AppendLine(@"		ON cd.id = r.contract_detail_id");
            sb.AppendLine(@"	INNER JOIN currency AS cu");
            sb.AppendLine(@"		ON cu.id = cd.currency_id ");
            sb.AppendLine(@"	INNER JOIN contract AS c");
            sb.AppendLine(@"		ON c.id = cd.contract_id");
            sb.AppendLine(@"	INNER JOIN dbo.material AS m");
            sb.AppendLine(@"		ON m.id = cd.material_id");
            sb.AppendLine(@"	INNER JOIN dbo.afe_line AS a");
            sb.AppendLine(@"		ON m.afe_line_id = a.id");
            sb.AppendLine(@"	OUTER APPLY [dbo].[udf_get_usage_by_contract](r.id, cd.material_id) AS usage");
            sb.AppendLine(@"	OUTER APPLY (");
            sb.AppendLine(@"					SELECT d.id, d.unit ");
            sb.AppendLine(@"					FROM daily_cost d");
            sb.AppendLine(@"					WHERE d.afe_contract_id = r.id AND d.drilling_id = @0");
            sb.AppendLine(@"				) AS daily");
            sb.AppendLine(@"	WHERE r.id IN (");
            sb.AppendLine(@"		SELECT rr.afe_contract_id ");
            sb.AppendLine(@"		FROM daily_cost rr");
            sb.AppendLine(@"		WHERE rr.drilling_id = @0 AND rr.material_id = m.id");
            sb.AppendLine(@"	)");
            sb.AppendLine(@"	UNION");
            sb.AppendLine(@"	SELECT   ");
            sb.AppendLine(@"		id = NULL,");
            sb.AppendLine(@"		r.created_by,");
            sb.AppendLine(@"		r.created_on,");
            sb.AppendLine(@"		r.modified_by,");
            sb.AppendLine(@"		r.modified_on,");
            sb.AppendLine(@"		r.approved_by,");
            sb.AppendLine(@"		r.approved_on,");
            sb.AppendLine(@"		r.is_active,");
            sb.AppendLine(@"		r.is_locked,");
            sb.AppendLine(@"		r.is_default,");
            sb.AppendLine(@"		r.owner_id,");
            sb.AppendLine(@"		r.id AS afe_contract_id,");
            sb.AppendLine(@"		c.id AS contract_id,");
            sb.AppendLine(@"		c.contract_no,");
            sb.AppendLine(@"		r.afe_id,");
            sb.AppendLine(@"		m.afe_line_id,");
            sb.AppendLine(@"		a.description AS afe_line_description,");
            sb.AppendLine(@"		cd.material_id,");
            sb.AppendLine(@"		m.description AS material_name,");
            sb.AppendLine(@"		cu.currency_code,");
            sb.AppendLine(@"		cd.current_rate_value,");
            sb.AppendLine(@"		r.unit,");
            sb.AppendLine(@"		(cd.actual_price / cd.unit * r.unit) AS actual_price,");
            sb.AppendLine(@"		r.unit - COALESCE(usage.total_unit, 0) AS remaining_unit,");
            sb.AppendLine(@"		daily.unit AS daily_unit");
            sb.AppendLine(@"	FROM dbo.afe_contract r");
            sb.AppendLine(@"	INNER JOIN contract_detail AS cd");
            sb.AppendLine(@"		ON cd.id = r.contract_detail_id");
            sb.AppendLine(@"	INNER JOIN currency AS cu");
            sb.AppendLine(@"		ON cu.id = cd.currency_id ");
            sb.AppendLine(@"	INNER JOIN contract AS c");
            sb.AppendLine(@"		ON c.id = cd.contract_id");
            sb.AppendLine(@"	INNER JOIN dbo.material AS m");
            sb.AppendLine(@"		ON m.id = cd.material_id");
            sb.AppendLine(@"	INNER JOIN dbo.afe_line AS a");
            sb.AppendLine(@"		ON m.afe_line_id = a.id");
            sb.AppendLine(@"	OUTER APPLY [dbo].[udf_get_usage_by_contract](r.id, cd.material_id) AS usage");
            sb.AppendLine(@"	OUTER APPLY (");
            sb.AppendLine(@"					SELECT d.id, d.unit ");
            sb.AppendLine(@"					FROM daily_cost d");
            sb.AppendLine(@"					WHERE d.afe_contract_id = r.id AND d.drilling_id = @0");
            sb.AppendLine(@"				) AS daily");
            sb.AppendLine(@"	WHERE r.id NOT IN (");
            sb.AppendLine(@"		SELECT rr.afe_contract_id ");
            sb.AppendLine(@"		FROM daily_cost rr");
            sb.AppendLine(@"		WHERE rr.drilling_id = @0 AND rr.material_id = m.id");
            sb.AppendLine(@"	)");
            sb.AppendLine(@") AS r");
            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            DefaultViewColumns.Add("afe_contract_id", "r.afe_contract_id");
            DefaultViewColumns.Add("contract_id", "r.contract_id");
            DefaultViewColumns.Add("contract_no", "r.contract_no");
            DefaultViewColumns.Add("afe_id", "r.afe_id");
            DefaultViewColumns.Add("afe_line_id", "r.afe_line_id");
            DefaultViewColumns.Add("afe_line_description", "r.afe_line_description");
            DefaultViewColumns.Add("material_id", "r.material_id");
            DefaultViewColumns.Add("material_name", "r.material_name");
            DefaultViewColumns.Add("currency_code", "r.currency_code");
            DefaultViewColumns.Add("current_rate_value", "r.current_rate_value");
            DefaultViewColumns.Add("unit", "r.unit");
            DefaultViewColumns.Add("actual_price", "r.actual_price");
            DefaultViewColumns.Add("remaining_unit", "r.remaining_unit");
            DefaultViewColumns.Add("daily_unit", "r.daily_unit");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("contract_no", "ASC");
            
            #endregion


            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

        public Dictionary<string, object> GetParameterized()
        {
            throw new NotImplementedException();
        }
    }
}
