﻿using CommonTools;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_application_menu_access
    {
        [ResultColumn]
        public string id { get; set; }
        [ResultColumn]
        public string created_by { get; set; }
        [ResultColumn]
        public DateTime? created_on { get; set; }
        [ResultColumn]
        public string modified_by { get; set; }
        [ResultColumn]
        public DateTime? modified_on { get; set; }
        [ResultColumn]
        public string approved_by { get; set; }
        [ResultColumn]
        public DateTime? approved_on { get; set; }
        [ResultColumn]
        public bool? is_active { get; set; }
        [ResultColumn]
        public bool? is_locked { get; set; }
        [ResultColumn]
        public bool? is_default { get; set; }
        [ResultColumn]
        public string owner_id { get; set; }
        [ResultColumn]
        public string menu_name { get; set; }
        [ResultColumn]
        public string action_url { get; set; }
        [ResultColumn]
        public string icon_class { get; set; }
        [ResultColumn]
        public int? order_index { get; set; }
        [ResultColumn]
        public bool? is_checked { get; set; }
    }

    public partial class vw_application_menu_access : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_application_menu_access()
        {
            EntityName = application_menu.GetTableName();
            EntityDisplayName = application_menu.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  r.id,");
            sb.AppendLine(@"  r.created_by,");
            sb.AppendLine(@"  r.created_on,");
            sb.AppendLine(@"  r.modified_by,");
            sb.AppendLine(@"  r.modified_on,");
            sb.AppendLine(@"  r.approved_by,");
            sb.AppendLine(@"  r.approved_on,");
            sb.AppendLine(@"  r.is_active,");
            sb.AppendLine(@"  r.is_locked,");
            sb.AppendLine(@"  r.is_default,");
            sb.AppendLine(@"  r.owner_id,");
            sb.AppendLine(@"  r.menu_name,");
            sb.AppendLine(@"  r.action_url,");
            sb.AppendLine(@"  r.icon_class,");
            sb.AppendLine(@"  r.order_index,");
            sb.AppendLine(@"  r.is_checked");
            sb.AppendLine(@"FROM (");
            sb.AppendLine(@"	SELECT r.*, is_checked = 1");
            sb.AppendLine(@"	FROM application_menu r");
            sb.AppendLine(@"	INNER JOIN role_access_menu ram ON ram.application_menu_id = r.id");
            sb.AppendLine(@"	INNER JOIN application_role ar ON ar.id = ram.application_role_id");
            sb.AppendLine(@"	WHERE r.parent_menu_id IS NULL AND ram.application_role_id IN (");
            sb.AppendLine(@"		SELECT r.application_role_id ");
            sb.AppendLine(@"		FROM user_role r");
            sb.AppendLine(@"		WHERE r.application_user_id = @0 ");
            sb.AppendLine(@"	)");
            sb.AppendLine(@"	UNION");
            sb.AppendLine(@"	SELECT r.*, is_checked = 1");
            sb.AppendLine(@"	FROM application_menu r");
            sb.AppendLine(@"	INNER JOIN role_access_menu ram ON ram.application_menu_id = r.id");
            sb.AppendLine(@"	INNER JOIN application_role ar ON ar.id = ram.application_role_id");
            sb.AppendLine(@"	WHERE r.parent_menu_id IS NULL AND ram.application_role_id IN (");
            sb.AppendLine(@"		SELECT r.application_role_id");
            sb.AppendLine(@"		FROM team_role r");
            sb.AppendLine(@"		INNER JOIN team t ON t.id = r.team_id");
            sb.AppendLine(@"		INNER JOIN team_member tm ON t.id = tm.team_id");
            sb.AppendLine(@"		WHERE tm.application_user_id = @0 ");
            sb.AppendLine(@"	)");
            sb.AppendLine(@") AS r");

            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();
            DefaultViewColumns.Add("menu_name", "r.menu_name");
            DefaultViewColumns.Add("action_url", "r.action_url");
            DefaultViewColumns.Add("icon_class", "r.icon_class");
            DefaultViewColumns.Add("order_index", "r.order_index");
            DefaultViewColumns.Add("is_checked", "r.is_checked");
            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("order_index", "ASC");

            #endregion


            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
