﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_afe_manage_detail
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string created_by { get; set; } //(nvarchar(50), null)
        public DateTime? created_on { get; set; } //(datetime2(7), null)
        public string modified_by { get; set; } //(nvarchar(50), null)
        public DateTime? modified_on { get; set; } //(datetime2(7), null)
        public string approved_by { get; set; } //(nvarchar(50), null)
        public DateTime? approved_on { get; set; } //(datetime2(7), null)
        public bool? is_active { get; set; } //(bit, null)
        public bool? is_locked { get; set; } //(bit, null)
        public bool? is_default { get; set; } //(bit, null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string afe_line_id { get; set; } //(nvarchar(50), not null)
        public string afe_line_description { get; set; } //(varchar(256), null)

        public string parent_id { get; set; } //(nvarchar(50), not null)
        public string parent_name { get; set; } //(varchar(256), null)

        public string uom_id { get; set; } //(nvarchar(50), not null)
        public string uom_code { get; set; } //(varchar(50), null)
        public string material_type { get; set; } //(nvarchar(16), not null)
        public string material_id { get; set; } //(nvarchar(16), not null)

        public string description { get; set; } //AS FROM r.material_name (varchar(256), not null)
        public float? afe_unit { get; set; } //(decimal(8,0), null)
        public string afe_currency_id { get; set; } //(nvarchar(50), null)
        public string afe_currency_code { get; set; } //(nvarchar(3), null)
        public float? afe_currency_rate_value { get; set; } //(decimal(18,2), null)
        public float? afe_actual_price { get; set; } //(decimal(18,2), null)
        public float? afe_unit_price { get; set; } //(decimal(18,2), null)
        public float? afe_total_price { get; set; } //(decimal(18,2), null)

        public float? new_purchase_total_unit { get; set; } //(decimal(18,2), null)
        public float? new_purchase_total_price { get; set; } //(decimal(18,2), null)

        public float? issued_stock_total_unit { get; set; } //(decimal(18,2), null)
        public float? issued_stock_total_price { get; set; } //(decimal(18,2), null)

        public float? actual_total_unit { get; set; } //(decimal(18,2), null)
        public float? actual_total_price { get; set; } //(decimal(18,2), null)

        public string icon_type { get; set; }
    }


    public partial class vw_afe_manage_detail : IEntity, IBaseRecord, IEntityParameterize
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion



        static vw_afe_manage_detail()
        {
            EntityName = material.GetTableName();
            EntityDisplayName = material.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            //sb.AppendLine(@"DECLARE @@afe_id NVARCHAR(50) = @0 ");
            sb.AppendLine(@";WITH ");
            sb.AppendLine(@"    m (");
            sb.AppendLine(@"        id,");
            sb.AppendLine(@"		owner_id,");
            sb.AppendLine(@"        afe_line_id,");
            sb.AppendLine(@"        afe_line_description,");
            sb.AppendLine(@"        parent_id,");
            sb.AppendLine(@"        parent_name,");
            sb.AppendLine(@"        uom_id,");
            sb.AppendLine(@"        uom_code,");
            sb.AppendLine(@"        material_type,");
            sb.AppendLine(@"        material_name,");
            sb.AppendLine(@"        item_type,");
            sb.AppendLine(@"        is_active");
            sb.AppendLine(@"    )");
            sb.AppendLine(@"    AS ");
            sb.AppendLine(@"    (");
            sb.AppendLine(@"        SELECT");
            sb.AppendLine(@"                       ");
            sb.AppendLine(@"        r.id,");
            sb.AppendLine(@"		r.owner_id,");
            sb.AppendLine(@"        r.afe_line_id,");
            sb.AppendLine(@"        a.description AS afe_line_description,");
            sb.AppendLine(@"        a.parent_id,");
            sb.AppendLine(@"        COALESCE(p.description,'N/A') AS parent_name,");
            sb.AppendLine(@"        r.uom_id,");
            sb.AppendLine(@"        u.uom_code,");
            sb.AppendLine(@"        r.material_type,");
            sb.AppendLine(@"        r.description AS material_name,");
            sb.AppendLine(@"        r.item_type,");
            sb.AppendLine(@"        r.is_active");
            sb.AppendLine(@"		");
            sb.AppendLine(@"            					");
            sb.AppendLine(@"        FROM dbo.material AS r");
            sb.AppendLine(@"        LEFT OUTER JOIN dbo.organization AS o1");
            sb.AppendLine(@"			ON r.organization_id = o1.id");
            sb.AppendLine(@"		LEFT OUTER JOIN dbo.afe_line AS a");
            sb.AppendLine(@"			ON r.afe_line_id = a.id");
            sb.AppendLine(@"		LEFT OUTER JOIN dbo.afe_line AS p");
            sb.AppendLine(@"			ON a.parent_id = p.id");
            sb.AppendLine(@"");
            sb.AppendLine(@"		LEFT OUTER JOIN dbo.uom AS u");
            sb.AppendLine(@"			ON r.uom_id = u.id");
            sb.AppendLine(@"        ");
            sb.AppendLine(@"		");
            sb.AppendLine(@"    ),");
            sb.AppendLine(@"    ap (");
            sb.AppendLine(@"		id,");
            sb.AppendLine(@"		owner_id,");
            sb.AppendLine(@"        afe_id,");
            sb.AppendLine(@"        created_on,");
            sb.AppendLine(@"        material_id,");
            sb.AppendLine(@"        currency_id,");
            sb.AppendLine(@"        current_rate_value,");
            sb.AppendLine(@"        currency_code,");
            sb.AppendLine(@"        base_rate,");
            sb.AppendLine(@"        unit,");
            sb.AppendLine(@"        unit_price,");
            sb.AppendLine(@"        total_price,");
            sb.AppendLine(@"        actual_price,");
            sb.AppendLine(@"		is_active");
            sb.AppendLine(@"    )");
            sb.AppendLine(@"    AS (");
            sb.AppendLine(@"                        	");
            sb.AppendLine(@"        SELECT");
            sb.AppendLine(@"              ");
            sb.AppendLine(@"		r.id,");
            sb.AppendLine(@"		r.owner_id,");
            sb.AppendLine(@"        r.afe_id,");
            sb.AppendLine(@"        r.created_on,");
            sb.AppendLine(@"        r.material_id,");
            sb.AppendLine(@"        r.currency_id,");
            sb.AppendLine(@"        r.current_rate_value,");
            sb.AppendLine(@"        c.currency_code,");
            sb.AppendLine(@"        c.base_rate,");
            sb.AppendLine(@"        r.unit,");
            sb.AppendLine(@"        r.unit_price,");
            sb.AppendLine(@"        r.total_price,");
            sb.AppendLine(@"        r.actual_price,");
            sb.AppendLine(@"		r.is_active");
            sb.AppendLine(@"                        		");
            sb.AppendLine(@"        FROM dbo.afe_plan AS r");
            sb.AppendLine(@"        INNER JOIN m ON r.material_id=m.id ");
            sb.AppendLine(@"        LEFT OUTER JOIN dbo.currency AS c");
            sb.AppendLine(@"        ON r.currency_id = c.id");
            sb.AppendLine(@"        WHERE r.afe_id=@0 AND m.afe_line_id=@1 AND m.item_type=@2 ");
            sb.AppendLine(@"    ),");

            sb.AppendLine(@"    new_purchase");
            sb.AppendLine(@"				(");
            sb.AppendLine(@"					material_id ,");
            sb.AppendLine(@"					total_unit,");
            sb.AppendLine(@"					total_price	");
            sb.AppendLine(@"				) ");
            sb.AppendLine(@"				AS (");
            sb.AppendLine(@"					SELECT ");
            sb.AppendLine(@"						 r.material_id,");
            sb.AppendLine(@"						 SUM(r.unit) AS total_unit,");
            sb.AppendLine(@"						 SUM(r.total_price) AS total_price");
            sb.AppendLine(@"					FROM ");
            sb.AppendLine(@"					(");
            sb.AppendLine(@"						SELECT ");
            sb.AppendLine(@"							a.contract_detail_id,");
            sb.AppendLine(@"							cd.material_id ,");
            sb.AppendLine(@"							m.description,");
            sb.AppendLine(@"							a.unit,");
            sb.AppendLine(@"							cd.current_rate_value AS exchange_rate,");
            sb.AppendLine(@"							(COALESCE(cd.unit_price,0) / cd.current_rate_value) * COALESCE(a.unit,0) AS total_price");
            sb.AppendLine(@"						");
            sb.AppendLine(@"					");
            sb.AppendLine(@"						FROM dbo.afe_contract a");
            sb.AppendLine(@"						INNER JOIN dbo.contract_detail cd ON a.contract_detail_id=cd.id");
            sb.AppendLine(@"						INNER JOIN material m ON cd.material_id=m.id");
            sb.AppendLine(@"						INNER JOIN dbo.contract c ON cd.contract_id=c.id");
            sb.AppendLine(@"						WHERE  a.is_new_purchase = 1 AND  a.afe_id=@0 AND m.afe_line_id=@1 AND m.item_type=@2 ");
            sb.AppendLine(@"					) AS r");
            sb.AppendLine(@"					GROUP BY r.material_id ");
            sb.AppendLine(@"				),");

            sb.AppendLine(@"                issued_stock ");
            sb.AppendLine(@"             				( ");
            sb.AppendLine(@"             					material_id , ");
            sb.AppendLine(@"             					total_unit, ");
            sb.AppendLine(@"             					total_price	 ");
            sb.AppendLine(@"             				)  ");
            sb.AppendLine(@"             				AS ( ");
            sb.AppendLine(@"             					SELECT  ");
            sb.AppendLine(@"             						 r.material_id, ");
            sb.AppendLine(@"             						 SUM(r.unit) AS total_unit, ");
            sb.AppendLine(@"             						 SUM(r.total_price) AS total_price ");
            sb.AppendLine(@"             					FROM  ");
            sb.AppendLine(@"             					( ");
            sb.AppendLine(@"             						SELECT  ");
            sb.AppendLine(@"             							a.contract_detail_id, ");
            sb.AppendLine(@"             							cd.material_id , ");
            sb.AppendLine(@"             							m.description, ");
            sb.AppendLine(@"             							a.unit, ");
            sb.AppendLine(@"             							cd.current_rate_value AS exchange_rate, ");
            sb.AppendLine(@"             							(COALESCE(cd.unit_price,0) / cd.current_rate_value) * COALESCE(a.unit,0) AS total_price ");
            sb.AppendLine(@"             						 ");
            sb.AppendLine(@"             					 ");
            sb.AppendLine(@"             						FROM dbo.afe_contract a ");
            sb.AppendLine(@"             						INNER JOIN dbo.contract_detail cd ON a.contract_detail_id=cd.id ");
            sb.AppendLine(@"             						INNER JOIN material m ON cd.material_id=m.id ");
            sb.AppendLine(@"             						INNER JOIN dbo.contract c ON cd.contract_id=c.id ");
            sb.AppendLine(@"             						WHERE a.is_new_purchase = 0 AND a.afe_id=@0 AND m.afe_line_id=@1 AND m.item_type=@2  ");
            sb.AppendLine(@"             					) AS r ");
            sb.AppendLine(@"             					GROUP BY r.material_id  ");
            sb.AppendLine(@"             				), ");
            sb.AppendLine(@"                 actual_cost ");
            sb.AppendLine(@"             				( ");
            sb.AppendLine(@"             					material_id , ");
            sb.AppendLine(@"             					total_unit, ");
            sb.AppendLine(@"             					total_price	 ");
            sb.AppendLine(@"             				) ");
            sb.AppendLine(@"             				AS ( ");

            sb.AppendLine(@"                                SELECT  ");
            sb.AppendLine(@"										r.material_id, ");
            sb.AppendLine(@"										SUM(r.unit) AS total_unit, ");
            sb.AppendLine(@"										SUM(r.total_price) AS total_price ");
            sb.AppendLine(@"								FROM  ");
            sb.AppendLine(@"								( ");
            sb.AppendLine(@"									SELECT  ");
            sb.AppendLine(@"										a.contract_detail_id, ");
            sb.AppendLine(@"										cd.material_id , ");
            sb.AppendLine(@"										m.description, ");
            sb.AppendLine(@"										d.unit, ");
            sb.AppendLine(@"										cd.current_rate_value AS exchange_rate, ");
            sb.AppendLine(@"										(COALESCE(cd.unit_price,0) / cd.current_rate_value) * COALESCE(d.unit,0) AS total_price ");
            sb.AppendLine(@"									FROM dbo.daily_cost d");
            sb.AppendLine(@"									INNER JOIN dbo.afe_contract a ON a.id = d.afe_contract_id");
            sb.AppendLine(@"									INNER JOIN dbo.contract_detail cd ON a.contract_detail_id=cd.id ");
            sb.AppendLine(@"									INNER JOIN material m ON cd.material_id=m.id ");
            sb.AppendLine(@"									INNER JOIN dbo.contract c ON cd.contract_id=c.id ");
            sb.AppendLine(@"									WHERE d.afe_id=@0 AND m.afe_line_id=@1 AND m.item_type=@2  ");
            sb.AppendLine(@"								) AS r ");
            sb.AppendLine(@"								GROUP BY r.material_id");
            sb.AppendLine(@"							)");



            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"r.id,");
            sb.AppendLine(@"r.created_by,");
            sb.AppendLine(@"r.created_on,");
            sb.AppendLine(@"r.modified_by,");
            sb.AppendLine(@"r.modified_on,");
            sb.AppendLine(@"r.approved_by,");
            sb.AppendLine(@"r.approved_on,");
            sb.AppendLine(@"r.is_active,");
            sb.AppendLine(@"r.is_locked,");
            sb.AppendLine(@"r.is_default,");
            sb.AppendLine(@"r.owner_id,");
            sb.AppendLine(@"                        ");
            sb.AppendLine(@"                        ");
            sb.AppendLine(@"r.afe_line_id,");
            sb.AppendLine(@"r.afe_line_description,");
            sb.AppendLine(@"r.parent_id,");
            sb.AppendLine(@"r.parent_name,");
            sb.AppendLine(@"r.uom_id,");
            sb.AppendLine(@"r.uom_code,");
            sb.AppendLine(@"r.material_type,");
            sb.AppendLine(@"r.material_id,");

            sb.AppendLine(@"r.material_name as description,");

            sb.AppendLine(@" /*PLANNING*/");
            sb.AppendLine(@"r.afe_unit,");
            sb.AppendLine(@"r.afe_currency_id,");
            sb.AppendLine(@"r.afe_currency_code,");
            sb.AppendLine(@"r.afe_currency_rate_value,");
            sb.AppendLine(@"r.afe_actual_price,");
            sb.AppendLine(@"r.afe_unit_price, ");
            sb.AppendLine(@"r.afe_total_price, ");

            sb.AppendLine(@"r.new_purchase_total_unit,");
            sb.AppendLine(@"r.new_purchase_total_price,");

            sb.AppendLine(@"r.issued_stock_total_unit,");
            sb.AppendLine(@"r.issued_stock_total_price,");

            sb.AppendLine(@"r.actual_total_unit,");
            sb.AppendLine(@"r.actual_total_price");


            sb.AppendLine(@"FROM (");
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"                        	");
            sb.AppendLine(@"ap.id,");
            sb.AppendLine(@"CAST(NULL AS NVARCHAR(50)) created_by,");
            //sb.AppendLine(@"CAST(NULL AS DATETIME2(7)) created_on,");
            sb.AppendLine(@"ap.created_on,");
            sb.AppendLine(@"CAST(NULL AS NVARCHAR(50)) modified_by,");
            sb.AppendLine(@"CAST(NULL AS DATETIME2(7)) modified_on,");
            sb.AppendLine(@"CAST(NULL AS NVARCHAR(50)) approved_by,");
            sb.AppendLine(@"CAST(NULL AS DATETIME2(7)) approved_on,");
            sb.AppendLine(@"ap.is_active,");
            sb.AppendLine(@"CAST(NULL AS BIT) is_locked,");
            sb.AppendLine(@"CAST(NULL AS BIT) is_default,");
            sb.AppendLine(@"ap.owner_id,");
            sb.AppendLine(@"                        ");
            sb.AppendLine(@"                        ");
            sb.AppendLine(@"m.afe_line_id,");
            sb.AppendLine(@"m.afe_line_description,");
            sb.AppendLine(@"m.parent_id,");
            sb.AppendLine(@"m.parent_name,");
            sb.AppendLine(@"m.uom_id,");
            sb.AppendLine(@"m.uom_code,");
            sb.AppendLine(@"m.material_type,");
            sb.AppendLine(@"ap.material_id,");
            sb.AppendLine(@"m.material_name,");
            sb.AppendLine(@"                        ");
            sb.AppendLine(@"round(ap.unit,2) AS afe_unit,");
            sb.AppendLine(@"ap.currency_id AS afe_currency_id,");
            sb.AppendLine(@"ap.currency_code AS afe_currency_code,");
            sb.AppendLine(@"round(ap.current_rate_value,2) AS afe_currency_rate_value,");
            sb.AppendLine(@"round(ap.actual_price,2) AS afe_actual_price,");
            sb.AppendLine(@"round(ap.unit_price,2) AS afe_unit_price, ");
            sb.AppendLine(@"round(ap.total_price,2) AS afe_total_price, ");

            sb.AppendLine(@"/*NEW PURCHASE*/");
            sb.AppendLine(@"round(np.total_unit,2) AS new_purchase_total_unit,");
            sb.AppendLine(@"round(np.total_price,2) AS new_purchase_total_price,");

            sb.AppendLine(@"/*Issued From Stock*/");
            sb.AppendLine(@"round(i.total_unit,2) AS issued_stock_total_unit,");
            sb.AppendLine(@"round(i.total_price,2) AS issued_stock_total_price,");

            sb.AppendLine(@"/*Actual Cost*/");
            sb.AppendLine(@"round(a.total_unit,2) AS actual_total_unit,");
            sb.AppendLine(@"round(a.total_price,2) AS actual_total_price");

            sb.AppendLine(@"FROM ap");
            sb.AppendLine(@"INNER JOIN m ON m.id=ap.material_id");
            sb.AppendLine(@"LEFT JOIN new_purchase np ON m.id = np.material_id");
            sb.AppendLine(@"LEFT JOIN issued_stock i ON m.id = i.material_id");
            sb.AppendLine(@"LEFT JOIN actual_cost a ON m.id = a.material_id");

            sb.AppendLine(@") AS r");


            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            DefaultViewColumns.Add("afe_line_id", "r.afe_line_id");
            DefaultViewColumns.Add("afe_line_description", "r.afe_line_description");
            DefaultViewColumns.Add("parent_id", "r.parent_id");
            DefaultViewColumns.Add("parent_name", "r.parent_name");
            DefaultViewColumns.Add("uom_id", "r.uom_id");
            DefaultViewColumns.Add("uom_code", "r.uom_code");
            DefaultViewColumns.Add("material_type", "r.material_type");
            DefaultViewColumns.Add("description", "r.material_name");
            DefaultViewColumns.Add("afe_unit", "r.afe_unit");
            DefaultViewColumns.Add("afe_currency_id", "r.afe_currency_id");
            DefaultViewColumns.Add("afe_currency_code", "r.afe_currency_code");
            DefaultViewColumns.Add("afe_currency_rate_value", "r.afe_currency_rate_value");
            DefaultViewColumns.Add("afe_actual_price", "r.afe_actual_price");
            DefaultViewColumns.Add("afe_unit_price", "r.afe_unit_price");
            DefaultViewColumns.Add("afe_total_price", "r.afe_total_price");


            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("material_name", "ASC");

            #endregion


            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

        public Dictionary<string, object> GetParameterized()
        {
            throw new NotImplementedException();
        }
    }
}
