﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_afe_remaining
    {
        public string contract_detail_id { get; set; } //(nvarchar(50), null)
        public string material_id { get; set; }
        public decimal? unit_of_contract { get; set; }
        public decimal? unit_of_actual { get; set; }
        public decimal? remaining_unit { get; set; }

    }

    public partial class vw_afe_remaining : IEntity
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_afe_remaining()
        {
            EntityName = afe.GetTableName();
            EntityDisplayName = afe.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@";WITH");
            sb.AppendLine(@"	a (");
            sb.AppendLine(@"		contract_detail_id,");
            sb.AppendLine(@"		material_id,");
            sb.AppendLine(@"		unit,");
            sb.AppendLine(@"		remaining_unit_of_contract	");
            sb.AppendLine(@"	) AS ");
            sb.AppendLine(@"	(");
            sb.AppendLine(@"		SELECT ");
            sb.AppendLine(@"			r.contract_detail_id,");
            sb.AppendLine(@"			cd.material_id,");
            sb.AppendLine(@"			COALESCE(r.unit,0),");
            sb.AppendLine(@"			COALESCE(cd.remaining_unit,0) AS remaining_unit_of_contract");
            sb.AppendLine(@"		FROM dbo.afe_contract r");
            sb.AppendLine(@"		INNER JOIN dbo.contract_detail cd ON r.contract_detail_id=cd.id");
            sb.AppendLine(@"		WHERE r.afe_id=@0");
            sb.AppendLine(@"	),");
            sb.AppendLine(@"	d (");
            sb.AppendLine(@"		contract_detail_id,");
            sb.AppendLine(@"		material_id,");
            sb.AppendLine(@"		unit	");
            sb.AppendLine(@"	) AS ");
            sb.AppendLine(@"	(");
            sb.AppendLine(@"		SELECT ");
            sb.AppendLine(@"			ac.contract_detail_id,");
            sb.AppendLine(@"			cd.material_id,");
            sb.AppendLine(@"			COALESCE(r.unit,0)");
            sb.AppendLine(@"		FROM dbo.daily_cost r");
            sb.AppendLine(@"		INNER JOIN dbo.afe_contract ac ON r.afe_contract_id=ac.id");
            sb.AppendLine(@"		INNER JOIN dbo.contract_detail cd ON ac.contract_detail_id=cd.id");
            sb.AppendLine(@"		WHERE r.afe_id=@0");
            sb.AppendLine(@"	)");
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"	a.contract_detail_id, ");
            sb.AppendLine(@"	a.material_id,");
            sb.AppendLine(@"	COALESCE(a.unit,0) AS unit_of_contract,");
            sb.AppendLine(@"	COALESCE(d.unit,0) AS unit_of_actual,");
            sb.AppendLine(@"	COALESCE(remaining_unit_of_contract,0) - COALESCE(d.unit,0) AS remaining_unit");
            sb.AppendLine(@"FROM a");
            sb.AppendLine(@"LEFT JOIN d ON a.contract_detail_id=d.contract_detail_id AND a.material_id=d.material_id ");


            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            DefaultViewColumns.Add("contract_detail_id", "a.contract_detail_id");
            DefaultViewColumns.Add("material_id", "a.material_id");
            DefaultViewColumns.Add("unit_of_contract", "COALESCE(a.unit,0)");
            DefaultViewColumns.Add("unit_of_actual", "COALESCE(d.unit,0)");
            DefaultViewColumns.Add("remaining_unit", "COALESCE(remaining_unit_of_contract,0) - COALESCE(d.unit,0)");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("contract_detail_id", "ASC");
            DefaultViewOrders.Add("material_id", "ASC");

            #endregion


            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

    }
}
