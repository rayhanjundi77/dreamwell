﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_afe_manage_usage
    {
        public string id { get; set; } //(nvarchar(50), null)
        public string created_by { get; set; } //(nvarchar(50), null)
        public DateTime? created_on { get; set; } //(datetime2(7), null)
        public string modified_by { get; set; } //(nvarchar(50), null)
        public DateTime? modified_on { get; set; } //(datetime2(7), null)
        public string approved_by { get; set; } //(nvarchar(50), null)
        public DateTime? approved_on { get; set; } //(datetime2(7), null)
        public bool? is_active { get; set; } //(bit, null)
        public bool? is_locked { get; set; } //(bit, null)
        public bool? is_default { get; set; } //(bit, null)
        public string owner_id { get; set; } //(nvarchar(50), null)
        public string afe_line_id { get; set; } //(nvarchar(50), not null)
        public string afe_line_description { get; set; } //(varchar(256), null)

        public string parent_id { get; set; } //(nvarchar(50), not null)
        public string parent_name { get; set; } //(varchar(256), null)

        public string uom_id { get; set; } //(nvarchar(50), not null)
        public string uom_code { get; set; } //(varchar(50), null)
        public string material_type { get; set; } //(nvarchar(16), not null)
        public string material_id { get; set; } //(nvarchar(16), not null)
        public string description { get; set; } //AS FROM r.material_name (varchar(256), not null)
        public float? stock_total_unit { get; set; } //(decimal(18,2), null)
        public float? stock_total_price { get; set; } //(decimal(18,2), null)
        public float? usage_total_unit { get; set; } //(decimal(18,2), null)
        public float? usage_total_price { get; set; } //(decimal(18,2), null)
        public float? daily_total_unit { get; set; } //(decimal(18,2), null)
        public float? daily_total_price { get; set; } //(decimal(18,2), null)

        public string icon_type { get; set; }
    }

    public partial class vw_afe_manage_usage : IEntity, IBaseRecord, IEntityParameterize
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion

        static vw_afe_manage_usage()
        {
            EntityName = material.GetTableName();
            EntityDisplayName = material.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@";WITH  ");
            sb.AppendLine(@"    m ( ");
            sb.AppendLine(@"        id, ");
            sb.AppendLine(@"        owner_id, ");
            sb.AppendLine(@"        afe_line_id, ");
            sb.AppendLine(@"        afe_line_description, ");
            sb.AppendLine(@"        parent_id, ");
            sb.AppendLine(@"        parent_name, ");
            sb.AppendLine(@"        uom_id, ");
            sb.AppendLine(@"        uom_code, ");
            sb.AppendLine(@"        material_type, ");
            sb.AppendLine(@"        material_name, ");
            sb.AppendLine(@"        item_type, ");
            sb.AppendLine(@"        is_active ");
            sb.AppendLine(@"    ) ");
            sb.AppendLine(@"    AS  ");
            sb.AppendLine(@"    ( ");
            sb.AppendLine(@"        SELECT ");
            sb.AppendLine(@"        r.id, ");
            sb.AppendLine(@"        r.owner_id, ");
            sb.AppendLine(@"        r.afe_line_id, ");
            sb.AppendLine(@"        a.description AS afe_line_description, ");
            sb.AppendLine(@"        a.parent_id, ");
            sb.AppendLine(@"        COALESCE(p.description,'N/A') AS parent_name, ");
            sb.AppendLine(@"        r.uom_id, ");
            sb.AppendLine(@"        u.uom_code, ");
            sb.AppendLine(@"        r.material_type, ");
            sb.AppendLine(@"        r.description AS material_name, ");
            sb.AppendLine(@"        r.item_type, ");
            sb.AppendLine(@"        r.is_active ");
            sb.AppendLine(@"        FROM dbo.material AS r ");
            sb.AppendLine(@"        LEFT OUTER JOIN dbo.organization AS o1 ");
            sb.AppendLine(@"        ON r.organization_id = o1.id ");
            sb.AppendLine(@"        LEFT OUTER JOIN dbo.afe_line AS a ");
            sb.AppendLine(@"        ON r.afe_line_id = a.id ");
            sb.AppendLine(@"        LEFT OUTER JOIN dbo.afe_line AS p ");
            sb.AppendLine(@"        ON a.parent_id = p.id ");
            sb.AppendLine(@"        LEFT OUTER JOIN dbo.uom AS u ");
            sb.AppendLine(@"        ON r.uom_id = u.id ");
            sb.AppendLine(@"    ),");
            sb.AppendLine(@"	stock ");
            sb.AppendLine(@"    ( ");
            sb.AppendLine(@"        material_id , ");
            sb.AppendLine(@"        total_unit, ");
            sb.AppendLine(@"        total_price	 ");
            sb.AppendLine(@"    ) ");
            sb.AppendLine(@"    AS ( ");
            sb.AppendLine(@"		SELECT  ");
            sb.AppendLine(@"				r.material_id, ");
            sb.AppendLine(@"				SUM(r.unit) AS total_unit, ");
            sb.AppendLine(@"				SUM(r.total_price) AS total_price");
            sb.AppendLine(@"		FROM  ");
            sb.AppendLine(@"		( ");
            sb.AppendLine(@"			SELECT  ");
            sb.AppendLine(@"				a.contract_detail_id, ");
            sb.AppendLine(@"				cd.material_id , ");
            sb.AppendLine(@"				m.description, ");
            sb.AppendLine(@"				a.unit,");
            sb.AppendLine(@"				cd.current_rate_value AS exchange_rate, ");
            sb.AppendLine(@"				SUM((COALESCE(cd.unit_price,0) / NULLIF(cd.current_rate_value,0)) * COALESCE(a.unit,0)) AS total_price");
            sb.AppendLine(@"			FROM dbo.afe_contract a ");
            sb.AppendLine(@"			INNER JOIN dbo.contract_detail cd ON a.contract_detail_id=cd.id ");
            sb.AppendLine(@"			INNER JOIN material m ON cd.material_id=m.id ");
            sb.AppendLine(@"			INNER JOIN dbo.contract c ON cd.contract_id=c.id");
            sb.AppendLine(@"			WHERE a.afe_id=@0 AND m.afe_line_id=@1 AND m.item_type=@2 ");
            sb.AppendLine(@"			GROUP BY a.contract_detail_id, cd.material_id, m.description, a.unit, cd.current_rate_value--, usage.total_unit, usage.total_price");
            sb.AppendLine(@"		) AS r ");
            sb.AppendLine(@"		GROUP BY r.material_id--, r.current_usage, r.unit_price");
            sb.AppendLine(@"	),");
            sb.AppendLine(@"	daily_cost ");
            sb.AppendLine(@"    ( ");
            sb.AppendLine(@"        material_id , ");
            sb.AppendLine(@"        total_unit, ");
            sb.AppendLine(@"        total_price	 ");
            sb.AppendLine(@"    ) ");
            sb.AppendLine(@"    AS ( ");
            sb.AppendLine(@"		SELECT  ");
            sb.AppendLine(@"				r.material_id, ");
            sb.AppendLine(@"				SUM(r.unit) AS total_unit, ");
            sb.AppendLine(@"				SUM(r.total_price) AS total_price ");
            sb.AppendLine(@"		FROM  ");
            sb.AppendLine(@"		( ");
            sb.AppendLine(@"			SELECT  ");
            sb.AppendLine(@"				ac.contract_detail_id, ");
            sb.AppendLine(@"				cd.material_id , ");
            sb.AppendLine(@"				m.description, ");
            sb.AppendLine(@"				a.unit, ");
            sb.AppendLine(@"				cd.current_rate_value AS exchange_rate, ");
            sb.AppendLine(@"				COALESCE(AVG(cd.actual_price / NULLIF(cd.unit,0)), 0) * a.unit AS total_price");
            sb.AppendLine(@"				--(COALESCE(cd.unit_price,0) * cd.current_rate_value) * COALESCE(a.unit,0) AS total_price ");
            sb.AppendLine(@"			FROM dbo.daily_cost a ");
            sb.AppendLine(@"			INNER JOIN dbo.afe_contract AS ac ON ac.id = a.afe_contract_id");
            sb.AppendLine(@"			INNER JOIN dbo.contract_detail cd ON ac.contract_detail_id = cd.id ");
            sb.AppendLine(@"			INNER JOIN material m ON a.material_id=m.id ");
            sb.AppendLine(@"			WHERE a.drilling_id = @3 AND a.afe_id=@0 AND m.afe_line_id=@1 AND m.item_type=@2  ");
            sb.AppendLine(@"			GROUP BY ac.contract_detail_id, cd.material_id, m.description, a.unit, cd.current_rate_value, cd.unit_price");
            sb.AppendLine(@"		) AS r ");
            sb.AppendLine(@"		GROUP BY r.material_id  ");
            sb.AppendLine(@"	)");
            sb.AppendLine(@"");
            sb.AppendLine(@"");
            sb.AppendLine(@"SELECT  ");
            sb.AppendLine(@"	r.id, ");
            sb.AppendLine(@"	r.created_by, ");
            sb.AppendLine(@"	r.created_on, ");
            sb.AppendLine(@"	r.modified_by, ");
            sb.AppendLine(@"	r.modified_on, ");
            sb.AppendLine(@"	r.approved_by, ");
            sb.AppendLine(@"	r.approved_on, ");
            sb.AppendLine(@"	r.is_active, ");
            sb.AppendLine(@"	r.is_locked, ");
            sb.AppendLine(@"	r.is_default, ");
            sb.AppendLine(@"	r.owner_id, ");
            sb.AppendLine(@"	r.afe_line_id, ");
            sb.AppendLine(@"	r.afe_line_description, ");
            sb.AppendLine(@"	r.parent_id, ");
            sb.AppendLine(@"	r.parent_name, ");
            sb.AppendLine(@"	r.uom_id, ");
            sb.AppendLine(@"	r.uom_code, ");
            sb.AppendLine(@"	r.material_type, ");
            sb.AppendLine(@"	r.material_id, ");
            sb.AppendLine(@"	r.material_name as description, ");
            sb.AppendLine(@"	-- ## Stock ##");
            sb.AppendLine(@"	round(r.stock_total_unit,2) stock_total_unit,");
            //sb.AppendLine(@"	r.stock_total_price,");
            //sb.AppendLine(@"	round(r.stock_total_price,2) stock_total_price,");
            sb.AppendLine(@"	-- ## Remaining Unit ##");
            sb.AppendLine(@"	round(r.stock_total_unit - r.usage_total_unit,2) AS usage_total_unit,");
            // CAST(ISNULL(P.Profit, 0) - ISNULL(C.Cost, 0) AS MONEY)
            //CASE WHEN(Value1 -Value2) < 0 THEN 0 ELSE(Value1 - Value2) END AS ValueMix,
            sb.AppendLine(@"	CASE WHEN (ISNULL(r.stock_total_price,0) - ISNULL(r.usage_total_price,0)) < 0  THEN 0  ELSE   round(cast(cast(ISNULL(r.stock_total_price,0) - ISNULL(r.usage_total_price,0) as DECIMAL(18,0)) as float), 2) END  AS usage_total_price,");
            //sb.AppendLine(@"	r.stock_total_price - r.usage_total_price AS usage_total_price,");
            sb.AppendLine(@"	-- ## Daily Unit Usage ##");
            sb.AppendLine(@"	round(r.daily_total_unit,2) daily_total_unit,");
            sb.AppendLine(@"	round(r.daily_total_price,2) daily_total_price");

            sb.AppendLine(@"");
            sb.AppendLine(@"FROM ( ");
            sb.AppendLine(@"	SELECT  ");
            sb.AppendLine(@"	m.id, ");
            sb.AppendLine(@"	CAST(NULL AS NVARCHAR(50)) created_by, ");
            sb.AppendLine(@"	CAST(NULL AS DATETIME2(7)) created_on, ");
            sb.AppendLine(@"	CAST(NULL AS NVARCHAR(50)) modified_by, ");
            sb.AppendLine(@"	CAST(NULL AS DATETIME2(7)) modified_on, ");
            sb.AppendLine(@"	CAST(NULL AS NVARCHAR(50)) approved_by, ");
            sb.AppendLine(@"	CAST(NULL AS DATETIME2(7)) approved_on, ");
            sb.AppendLine(@"	m.is_active, ");
            sb.AppendLine(@"	CAST(NULL AS BIT) is_locked, ");
            sb.AppendLine(@"	CAST(NULL AS BIT) is_default, ");
            sb.AppendLine(@"	m.owner_id, ");
            sb.AppendLine(@"	m.afe_line_id, ");
            sb.AppendLine(@"	m.afe_line_description, ");
            sb.AppendLine(@"	m.parent_id, ");
            sb.AppendLine(@"	m.parent_name, ");
            sb.AppendLine(@"	m.uom_id, ");
            sb.AppendLine(@"	m.uom_code, ");
            sb.AppendLine(@"	m.material_type, ");
            sb.AppendLine(@"	m.material_name,");
            sb.AppendLine(@"	stock.material_id,");
            sb.AppendLine(@"	COALESCE(stock.total_unit, 0) AS stock_total_unit,");
            sb.AppendLine(@"	COALESCE(stock.total_price, 0) AS stock_total_price,");
            sb.AppendLine(@"	COALESCE(d.total_unit, 0) AS daily_total_unit,");
            sb.AppendLine(@"	COALESCE(d.total_price, 0) AS daily_total_price,");
            sb.AppendLine(@"	COALESCE(usage.total_unit, 0) AS usage_total_unit,");
            sb.AppendLine(@"	COALESCE(usage.total_price, 0) AS usage_total_price");
            sb.AppendLine(@"	FROM stock ");
            sb.AppendLine(@"	INNER JOIN m ON m.id=stock.material_id ");
            sb.AppendLine(@"	LEFT OUTER JOIN daily_cost d ON m.id = d.material_id");
            sb.AppendLine(@"    CROSS APPLY [dbo].[udf_get_usage_by_afe](@0, stock.material_id) AS usage");
            sb.AppendLine(@") AS r");





            //var sb = new System.Text.StringBuilder();
            //sb.AppendLine(@";WITH  ");
            //sb.AppendLine(@"    m ( ");
            //sb.AppendLine(@"        id, ");
            //sb.AppendLine(@"        owner_id, ");
            //sb.AppendLine(@"        afe_line_id, ");
            //sb.AppendLine(@"        afe_line_description, ");
            //sb.AppendLine(@"        parent_id, ");
            //sb.AppendLine(@"        parent_name, ");
            //sb.AppendLine(@"        uom_id, ");
            //sb.AppendLine(@"        uom_code, ");
            //sb.AppendLine(@"        material_type, ");
            //sb.AppendLine(@"        material_name, ");
            //sb.AppendLine(@"        item_type, ");
            //sb.AppendLine(@"        is_active ");
            //sb.AppendLine(@"    ) ");
            //sb.AppendLine(@"    AS  ");
            //sb.AppendLine(@"    ( ");
            //sb.AppendLine(@"        SELECT ");
            //sb.AppendLine(@"        r.id, ");
            //sb.AppendLine(@"        r.owner_id, ");
            //sb.AppendLine(@"        r.afe_line_id, ");
            //sb.AppendLine(@"        a.description AS afe_line_description, ");
            //sb.AppendLine(@"        a.parent_id, ");
            //sb.AppendLine(@"        COALESCE(p.description,'N/A') AS parent_name, ");
            //sb.AppendLine(@"        r.uom_id, ");
            //sb.AppendLine(@"        u.uom_code, ");
            //sb.AppendLine(@"        r.material_type, ");
            //sb.AppendLine(@"        r.description AS material_name, ");
            //sb.AppendLine(@"        r.item_type, ");
            //sb.AppendLine(@"        r.is_active ");
            //sb.AppendLine(@"        FROM dbo.material AS r ");
            //sb.AppendLine(@"        LEFT OUTER JOIN dbo.organization AS o1 ");
            //sb.AppendLine(@"        ON r.organization_id = o1.id ");
            //sb.AppendLine(@"        LEFT OUTER JOIN dbo.afe_line AS a ");
            //sb.AppendLine(@"        ON r.afe_line_id = a.id ");
            //sb.AppendLine(@"        LEFT OUTER JOIN dbo.afe_line AS p ");
            //sb.AppendLine(@"        ON a.parent_id = p.id ");
            //sb.AppendLine(@"        LEFT OUTER JOIN dbo.uom AS u ");
            //sb.AppendLine(@"        ON r.uom_id = u.id ");
            //sb.AppendLine(@"    ),");
            //sb.AppendLine(@"    stock ");
            //sb.AppendLine(@"    ( ");
            //sb.AppendLine(@"        material_id , ");
            //sb.AppendLine(@"        total_unit, ");
            //sb.AppendLine(@"        total_price	 ");
            //sb.AppendLine(@"    ) ");
            //sb.AppendLine(@"    AS ( ");
            //sb.AppendLine(@"		SELECT  ");
            //sb.AppendLine(@"				r.material_id, ");
            //sb.AppendLine(@"				SUM(r.unit) - r.current_usage AS total_unit, ");
            //sb.AppendLine(@"				(SUM(r.unit) - r.current_usage) * unit_price AS total_price ");
            //sb.AppendLine(@"		FROM  ");
            //sb.AppendLine(@"		( ");
            //sb.AppendLine(@"			SELECT  ");
            //sb.AppendLine(@"				a.contract_detail_id, ");
            //sb.AppendLine(@"				cd.material_id , ");
            //sb.AppendLine(@"				m.description, ");
            //sb.AppendLine(@"				a.unit,");
            //sb.AppendLine(@"				cd.current_rate_value AS exchange_rate, ");
            //sb.AppendLine(@"				usage.total_unit AS current_usage,");
            //sb.AppendLine(@"				usage.total_price AS unit_price,");
            //sb.AppendLine(@"				COALESCE(AVG(cd.actual_price / cd.unit), 0) AS total_price ");
            //sb.AppendLine(@"			FROM dbo.afe_contract a ");
            //sb.AppendLine(@"			INNER JOIN dbo.contract_detail cd ON a.contract_detail_id=cd.id ");
            //sb.AppendLine(@"			INNER JOIN material m ON cd.material_id=m.id ");
            //sb.AppendLine(@"			INNER JOIN dbo.contract c ON cd.contract_id=c.id");
            //sb.AppendLine(@"			CROSS APPLY [dbo].[udf_get_usage_by_afe](a.afe_id, cd.material_id) AS usage");
            //sb.AppendLine(@"			WHERE a.afe_id=@0 AND m.afe_line_id=@1 AND m.item_type=@2 ");
            //sb.AppendLine(@"			GROUP BY a.contract_detail_id, cd.material_id, m.description, a.unit, cd.current_rate_value, usage.total_unit, usage.total_price");
            //sb.AppendLine(@"		) AS r ");
            //sb.AppendLine(@"		GROUP BY r.material_id, r.current_usage, r.unit_price");
            //sb.AppendLine(@"	),");
            //sb.AppendLine(@"	daily_cost ");
            //sb.AppendLine(@"    ( ");
            //sb.AppendLine(@"        material_id , ");
            //sb.AppendLine(@"        total_unit, ");
            //sb.AppendLine(@"        total_price	 ");
            //sb.AppendLine(@"    ) ");
            //sb.AppendLine(@"    AS ( ");
            //sb.AppendLine(@"		SELECT  ");
            //sb.AppendLine(@"				r.material_id, ");
            //sb.AppendLine(@"				SUM(r.unit) AS total_unit, ");
            //sb.AppendLine(@"				SUM(r.total_price) AS total_price ");
            //sb.AppendLine(@"		FROM  ");
            //sb.AppendLine(@"		( ");
            //sb.AppendLine(@"			SELECT  ");
            //sb.AppendLine(@"				ac.contract_detail_id, ");
            //sb.AppendLine(@"				cd.material_id , ");
            //sb.AppendLine(@"				m.description, ");
            //sb.AppendLine(@"				a.unit, ");
            //sb.AppendLine(@"				cd.current_rate_value AS exchange_rate, ");
            //sb.AppendLine(@"				COALESCE(AVG(cd.actual_price / cd.unit), 0) * a.unit AS total_price");
            //sb.AppendLine(@"				--(COALESCE(cd.unit_price,0) * cd.current_rate_value) * COALESCE(a.unit,0) AS total_price ");
            //sb.AppendLine(@"			FROM dbo.daily_cost a ");
            //sb.AppendLine(@"			INNER JOIN dbo.afe_contract AS ac ON ac.id = a.afe_contract_id");
            //sb.AppendLine(@"			INNER JOIN dbo.contract_detail cd ON ac.contract_detail_id = cd.id ");
            //sb.AppendLine(@"			INNER JOIN material m ON a.material_id=m.id ");
            //sb.AppendLine(@"			WHERE a.drilling_id = @3 AND a.afe_id=@0 AND m.afe_line_id=@1 AND m.item_type=@2  ");
            //sb.AppendLine(@"			GROUP BY ac.contract_detail_id, cd.material_id, m.description, a.unit, cd.current_rate_value");
            //sb.AppendLine(@"		) AS r ");
            //sb.AppendLine(@"		GROUP BY r.material_id  ");
            //sb.AppendLine(@"	)");
            //sb.AppendLine(@"");
            //sb.AppendLine(@"SELECT  ");
            //sb.AppendLine(@"	r.id, ");
            //sb.AppendLine(@"	r.created_by, ");
            //sb.AppendLine(@"	r.created_on, ");
            //sb.AppendLine(@"	r.modified_by, ");
            //sb.AppendLine(@"	r.modified_on, ");
            //sb.AppendLine(@"	r.approved_by, ");
            //sb.AppendLine(@"	r.approved_on, ");
            //sb.AppendLine(@"	r.is_active, ");
            //sb.AppendLine(@"	r.is_locked, ");
            //sb.AppendLine(@"	r.is_default, ");
            //sb.AppendLine(@"	r.owner_id, ");
            //sb.AppendLine(@"	r.afe_line_id, ");
            //sb.AppendLine(@"	r.afe_line_description, ");
            //sb.AppendLine(@"	r.parent_id, ");
            //sb.AppendLine(@"	r.parent_name, ");
            //sb.AppendLine(@"	r.uom_id, ");
            //sb.AppendLine(@"	r.uom_code, ");
            //sb.AppendLine(@"	r.material_type, ");
            //sb.AppendLine(@"	r.material_id, ");
            //sb.AppendLine(@"	r.material_name as description, ");
            //sb.AppendLine(@"	-- ## Stock ##");
            //sb.AppendLine(@"	r.stock_total_unit,");
            //sb.AppendLine(@"	r.stock_total_price,");
            //sb.AppendLine(@"	-- ## Daily Cost ##");
            //sb.AppendLine(@"	r.daily_total_unit,");
            //sb.AppendLine(@"	r.daily_total_price");
            //sb.AppendLine(@"");
            //sb.AppendLine(@"FROM ( ");
            //sb.AppendLine(@"	SELECT  ");
            //sb.AppendLine(@"	m.id, ");
            //sb.AppendLine(@"	CAST(NULL AS NVARCHAR(50)) created_by, ");
            //sb.AppendLine(@"	CAST(NULL AS DATETIME2(7)) created_on, ");
            //sb.AppendLine(@"	CAST(NULL AS NVARCHAR(50)) modified_by, ");
            //sb.AppendLine(@"	CAST(NULL AS DATETIME2(7)) modified_on, ");
            //sb.AppendLine(@"	CAST(NULL AS NVARCHAR(50)) approved_by, ");
            //sb.AppendLine(@"	CAST(NULL AS DATETIME2(7)) approved_on, ");
            //sb.AppendLine(@"	m.is_active, ");
            //sb.AppendLine(@"	CAST(NULL AS BIT) is_locked, ");
            //sb.AppendLine(@"	CAST(NULL AS BIT) is_default, ");
            //sb.AppendLine(@"	m.owner_id, ");
            //sb.AppendLine(@"	m.afe_line_id, ");
            //sb.AppendLine(@"	m.afe_line_description, ");
            //sb.AppendLine(@"	m.parent_id, ");
            //sb.AppendLine(@"	m.parent_name, ");
            //sb.AppendLine(@"	m.uom_id, ");
            //sb.AppendLine(@"	m.uom_code, ");
            //sb.AppendLine(@"	m.material_type, ");
            //sb.AppendLine(@"	m.material_name,");
            //sb.AppendLine(@"	stock.material_id,");
            //sb.AppendLine(@"	stock.total_unit AS stock_total_unit,");
            //sb.AppendLine(@"	stock.total_price AS stock_total_price,");
            //sb.AppendLine(@"	d.total_unit AS daily_total_unit,");
            //sb.AppendLine(@"	d.total_price AS daily_total_price");
            //sb.AppendLine(@"	FROM stock ");
            //sb.AppendLine(@"	INNER JOIN m ON m.id=stock.material_id ");
            //sb.AppendLine(@"	LEFT OUTER JOIN daily_cost d ON m.id = d.material_id");
            //sb.AppendLine(@"            ");
            //sb.AppendLine(@") AS r ");

            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            DefaultViewColumns.Add("afe_line_id", "r.afe_line_id");
            DefaultViewColumns.Add("afe_line_description", "r.afe_line_description");
            DefaultViewColumns.Add("parent_id", "r.parent_id");
            DefaultViewColumns.Add("parent_name", "r.parent_name");
            DefaultViewColumns.Add("uom_id", "r.uom_id");
            DefaultViewColumns.Add("uom_code", "r.uom_code");
            DefaultViewColumns.Add("material_type", "r.material_type");
            DefaultViewColumns.Add("description", "r.material_name");
            DefaultViewColumns.Add("afe_unit", "r.afe_unit");
            DefaultViewColumns.Add("afe_currency_id", "r.afe_currency_id");
            DefaultViewColumns.Add("afe_currency_code", "r.afe_currency_code");
            DefaultViewColumns.Add("afe_currency_rate_value", "r.afe_currency_rate_value");
            DefaultViewColumns.Add("afe_actual_price", "r.afe_actual_price");
            DefaultViewColumns.Add("afe_unit_price", "r.afe_unit_price");
            DefaultViewColumns.Add("afe_total_price", "r.afe_total_price");


            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("material_name", "ASC");

            #endregion


            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

        public Dictionary<string, object> GetParameterized()
        {
            throw new NotImplementedException();
        }
    }
}
