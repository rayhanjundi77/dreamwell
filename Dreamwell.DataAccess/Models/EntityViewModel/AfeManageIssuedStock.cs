﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class vw_afe_manage_issued_stock
    {
        public string id { get; set; } //  '(nvarchar(50), not null)
        public string created_by { get; set; } //  '(nvarchar(50), null)
        public DateTime? created_on { get; set; } //  '(datetime2(7), null)
        public string modified_by { get; set; } //  '(nvarchar(50), null)
        public DateTime? modified_on { get; set; } //  '(datetime2(7), null)
        public string approved_by { get; set; } //  '(nvarchar(50), null)
        public DateTime? approved_on { get; set; } //  '(datetime2(7), null)
        public bool? is_active { get; set; } //  '(bit, null)
        public bool? is_locked { get; set; } //  '(bit, null)
        public bool? is_default { get; set; } //  '(bit, null)
        public string owner_id { get; set; } //  '(nvarchar(50), null)
        public string contract_id { get; set; } //  '(nvarchar(50), not null)
        public string contract_no { get; set; } //  '(varchar(255), null)
        public string afe_line_id { get; set; } //  '(nvarchar(50), null)
        public string afe_line_description { get; set; } //  '(varchar(256), null)
        public string material_id { get; set; } //  '(nvarchar(50), not null)
        public string material_name { get; set; } //  '(varchar(256), null)
        public string currency_id { get; set; } //  '(nvarchar(50), not null)
        public bool? base_rate { get; set; } //  '(bit, null)
        public string currency_code { get; set; } //  '(nvarchar(3), null)
        public Decimal? current_rate_value { get; set; } //  '(decimal(18,2), not null)
        public Decimal? unit { get; set; } //  '(decimal(8,0), not null)
        public Decimal? remaining_unit { get; set; } //  '(decimal(8,0), null)
        public Decimal? unit_available { get; set; } //  '(decimal(8,0), null)
        public string afe_id { get; set; } //  '(decimal(8,0), null)
        public string afe_locked_id { get; set; } //  '(nvarchar(50), null)
        public Decimal? unit_price { get; set; } //  '(decimal(18,2), not null)
        public Decimal? total_price { get; set; } //  '(decimal(18,2), not null)
        public Decimal? actual_price { get; set; } //  '(decimal(38,4), null)
        public string usage_afe_id { get; set; } //  '(decimal(8,0), null)
        public Decimal? unit_usage { get; set; } //  '(decimal(8,0), null)
        public string afe_contract_id { get; set; } //  '(decimal(8,0), null)
        public string record_created_by { get; set; } //  '(nvarchar(901), null)
        public string record_modified_by { get; set; } //  '(nvarchar(901), null)
        public string record_approved_by { get; set; } //  '(nvarchar(901), null)
        public string record_owner { get; set; } //  '(nvarchar(901), null)
        public string record_owning_team { get; set; } //  '(nvarchar(400), null)
    }


    public partial class vw_afe_manage_issued_stock : IEntity, IBaseRecord, IEntityParameterize
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        public static readonly Dictionary<string, object> Parameterize;

        #endregion



        static vw_afe_manage_issued_stock()
        {
            EntityName = contract_detail.GetTableName();
            EntityDisplayName = contract_detail.GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  r.id,");
            sb.AppendLine(@"  r.created_by,");
            sb.AppendLine(@"  r.created_on,");
            sb.AppendLine(@"  r.modified_by,");
            sb.AppendLine(@"  r.modified_on,");
            sb.AppendLine(@"  r.approved_by,");
            sb.AppendLine(@"  r.approved_on,");
            sb.AppendLine(@"  r.is_active,");
            sb.AppendLine(@"  r.is_locked,");
            sb.AppendLine(@"  r.is_default,");
            sb.AppendLine(@"  r.owner_id,");
            sb.AppendLine(@"  r.contract_id,");
            sb.AppendLine(@"  c.contract_no,");
            sb.AppendLine(@"  m.afe_line_id,");
            sb.AppendLine(@"  a.description AS afe_line_description,");
            sb.AppendLine(@"  r.material_id,");
            sb.AppendLine(@"  m.description AS material_name,");
            sb.AppendLine(@"  r.currency_id,");
            sb.AppendLine(@"  cr.base_rate,");
            sb.AppendLine(@"  cr.currency_code,");
            sb.AppendLine(@"  r.current_rate_value,");
            sb.AppendLine(@"  r.unit,");
            sb.AppendLine(@"  r.remaining_unit,");
            sb.AppendLine(@"  r.unit - COALESCE(usage.unit, 0) - COALESCE(usageOnAfe.unit, 0) AS unit_available,");
            sb.AppendLine(@"  c.afe_id,");
            sb.AppendLine(@"  r.afe_locked_id,");
            sb.AppendLine(@"  r.unit_price,");
            sb.AppendLine(@"  r.total_price,");
            sb.AppendLine(@"  COALESCE(r.actual_price, (r.current_rate_value * r.unit_price * r.unit)) AS actual_price,");
            sb.AppendLine(@"  usageOnAfe.afe_id AS usage_afe_id,");
            sb.AppendLine(@"  usageOnAfe.unit AS unit_usage,");
            sb.AppendLine(@"  usageOnAfe.id AS afe_contract_id,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,");
            sb.AppendLine(@"  t.team_name  AS record_owning_team");
            sb.AppendLine(@"FROM dbo.contract_detail AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.contract AS c");
            sb.AppendLine(@"  ON r.contract_id = c.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.material AS m");
            sb.AppendLine(@"  ON r.material_id = m.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.afe_line AS a");
            sb.AppendLine(@"  ON m.afe_line_id = a.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.currency AS cr");
            sb.AppendLine(@"  ON r.currency_id = cr.id");

            sb.AppendLine(@"OUTER APPLY (");
            sb.AppendLine(@"	SELECT ac.unit, ac.id, ac.afe_id");
            sb.AppendLine(@"	FROM afe_contract ac ");
            sb.AppendLine(@"	LEFT JOIN contract_detail cd ON cd.id = ac.contract_detail_id ");
            sb.AppendLine(@"	WHERE cd.id = r.id AND ");
            sb.AppendLine(@"	cd.material_id = @0 AND ac.afe_id != @1) AS usage");

            sb.AppendLine(@"OUTER APPLY (");
            sb.AppendLine(@"	SELECT ac.unit, ac.id, ac.afe_id");
            sb.AppendLine(@"	FROM afe_contract ac ");
            sb.AppendLine(@"	LEFT JOIN contract_detail cd ON cd.id = ac.contract_detail_id ");
            sb.AppendLine(@"	WHERE cd.id = r.id AND ");
            sb.AppendLine(@"	cd.material_id = @0 ");
            sb.AppendLine(@"	AND ac.afe_id = @1");
            sb.AppendLine(@"	) AS usageOnAfe");

            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");

            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion


            DefaultViewColumns.Add("contract_id", "r.contract_id");
            DefaultViewColumns.Add("contract_no", "c.contract_no");
            DefaultViewColumns.Add("afe_line_id", "m.afe_line_id");
            DefaultViewColumns.Add("afe_line_description", "a.description");
            DefaultViewColumns.Add("material_id", "r.material_id");
            DefaultViewColumns.Add("material_name", "m.description");
            DefaultViewColumns.Add("currency_id", "r.currency_id");
            DefaultViewColumns.Add("base_rate", "cr.base_rate");
            DefaultViewColumns.Add("currency_code", "cr.currency_code");
            DefaultViewColumns.Add("current_rate_value", "r.current_rate_value");
            DefaultViewColumns.Add("unit", "r.unit");
            DefaultViewColumns.Add("remaining_unit", "r.remaining_unit");
            DefaultViewColumns.Add("unit_available", "r.unit - COALESCE(usage.unit, 0) - COALESCE(usageOnAfe.unit, 0)");
            DefaultViewColumns.Add("afe_id", "c.afe_id");
            DefaultViewColumns.Add("afe_locked_id", "r.afe_locked_id");
            DefaultViewColumns.Add("unit_price", "r.unit_price");
            DefaultViewColumns.Add("total_price", "r.total_price");
            DefaultViewColumns.Add("actual_price", "COALESCE(r.actual_price, (r.current_rate_value * r.unit_price * r.unit))");
            DefaultViewColumns.Add("usage_afe_id", "usage.afe_id");
            DefaultViewColumns.Add("unit_usage", "usage.unit");
            DefaultViewColumns.Add("afe_contract_id", "usage.id");


            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("material_name", "ASC");
            
            #endregion


            #region Parameterized
            Parameterize = new Dictionary<string, object>();
            Parameterize.Add("@0", "");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }

        public Dictionary<string, object> GetParameterized()
        {
            throw new NotImplementedException();
        }
    }
}
