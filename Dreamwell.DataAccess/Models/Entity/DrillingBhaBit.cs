﻿using CommonTools;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public partial class drilling_bha_bit : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion

        static drilling_bha_bit()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"	r.id,");
            sb.AppendLine(@"	r.created_by,");
            sb.AppendLine(@"	r.created_on,");
            sb.AppendLine(@"	r.modified_by,");
            sb.AppendLine(@"	r.modified_on,");
            sb.AppendLine(@"	r.approved_by,");
            sb.AppendLine(@"	r.approved_on,");
            sb.AppendLine(@"	r.is_active,");
            sb.AppendLine(@"	r.is_locked,");
            sb.AppendLine(@"	r.is_default,");
            sb.AppendLine(@"	r.owner_id,");
            sb.AppendLine(@"	r.well_id,");
            sb.AppendLine(@"	r.drilling_id,");
            sb.AppendLine(@"	r.well_bha_id,");
            sb.AppendLine(@"	wbha.bha_no,");
            sb.AppendLine(@"	r.well_bit_id,");
            sb.AppendLine(@"	wbit.bit_number,");
            sb.AppendLine(@"	wbit.bit_size,");
            sb.AppendLine(@"	wbit.serial_number,");
            sb.AppendLine(@"	r.bit_run,");
            sb.AppendLine(@"	r.mudlogging_wob_min,");
            sb.AppendLine(@"	r.mudlogging_wob_max,");
            sb.AppendLine(@"	r.mudlogging_rpm_min,");
            sb.AppendLine(@"	r.mudlogging_rpm_max,");
            sb.AppendLine(@"	r.mudlogging_dhrpm_min,");
            sb.AppendLine(@"	r.mudlogging_dhrpm_max,");
            sb.AppendLine(@"	r.mudlogging_torque_min,");
            sb.AppendLine(@"	r.mudlogging_torque_max,");
            sb.AppendLine(@"	r.mudlogging_flowrate_min,");
            sb.AppendLine(@"	r.mudlogging_flowrate_max,");
            sb.AppendLine(@"	r.mudlogging_spp_min,");
            sb.AppendLine(@"	r.mudlogging_spp_max,");
            sb.AppendLine(@"	r.mudlogging_spm_min,");
            sb.AppendLine(@"	r.mudlogging_spm_max,");
            sb.AppendLine(@"	r.mudlogging_spmpress_min,");
            sb.AppendLine(@"	r.mudlogging_spmpress_max,");
            sb.AppendLine(@"	r.mudlogging_avg_rop_min,");
            sb.AppendLine(@"	r.mudlogging_avg_rop_max,");
            sb.AppendLine(@"	r.mudlogging_hkld,");
            sb.AppendLine(@"	r.mudlogging_mwi,");
            sb.AppendLine(@"	r.mudlogging_mwo,");
            sb.AppendLine(@"	r.weight_air,");
            sb.AppendLine(@"	r.weight_mud,");
            sb.AppendLine(@"	r.wt_below_jars_air,");
            sb.AppendLine(@"	r.wt_below_jars_mud,");
            sb.AppendLine(@"	r.string_weight,");
            sb.AppendLine(@"	r.weight_pick_up,");
            sb.AppendLine(@"	r.weight_slack_down,");
            sb.AppendLine(@"	r.weight_rotate,");
            sb.AppendLine(@"	r.dg_in_rows,");
            sb.AppendLine(@"	r.dg_out_rows,");
            sb.AppendLine(@"	r.dg_dull_char,");
            sb.AppendLine(@"	r.dg_loc_cone,");
            sb.AppendLine(@"	r.dg_seals,");
            sb.AppendLine(@"	r.dg_gauge,");
            sb.AppendLine(@"	r.dg_other_char,");
            sb.AppendLine(@"	r.dg_reason_pulled,");
            sb.AppendLine(@"	r.dg_footage,");
            sb.AppendLine(@"	r.dg_rop_overall,");
            sb.AppendLine(@"	r.depth_in,");
            sb.AppendLine(@"	r.depth_out,");
            sb.AppendLine(@"	r.duration,");


            sb.AppendLine(@"    we.well_name,");
            sb.AppendLine(@"	dr.drilling_date,");
            sb.AppendLine(@"	we.well_status,");
            sb.AppendLine(@"	we.field_id,");
            sb.AppendLine(@"	we.business_unit_id,");


            sb.AppendLine(@"	RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,");
            sb.AppendLine(@"	RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,");
            sb.AppendLine(@"	RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,");
            sb.AppendLine(@"	RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,");
            sb.AppendLine(@"	t.team_name  AS record_owning_team");
            sb.AppendLine(@"FROM dbo.drilling_bha_bit AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.well_bha AS wbha ");
            sb.AppendLine(@"  ON r.well_bha_id = wbha.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.well_bit AS wbit");
            sb.AppendLine(@"  ON r.well_bit_id = wbit.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id ");

            sb.AppendLine(@"LEFT OUTER JOIN dbo.well we ");
            sb.AppendLine(@"  ON we.id = wbit.well_id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.drilling dr");
            sb.AppendLine(@"  ON dr.id=r.drilling_id ");



            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("drilling_id", "r.drilling_id");
            DefaultViewColumns.Add("well_bha_id", "r.well_bha_id");
            DefaultViewColumns.Add("bha_no", "wbha.bha_no");
            DefaultViewColumns.Add("well_bit_id", "r.well_bit_id");
            DefaultViewColumns.Add("bit_number", "wbit.bit_number");
            DefaultViewColumns.Add("bit_run", "r.bit_run");
            DefaultViewColumns.Add("mudlogging_wob_min", "r.mudlogging_wob_min");
            DefaultViewColumns.Add("mudlogging_wob_max", "r.mudlogging_wob_max");
            DefaultViewColumns.Add("mudlogging_rpm_min", "r.mudlogging_rpm_min");
            DefaultViewColumns.Add("mudlogging_rpm_max", "r.mudlogging_rpm_max");
            DefaultViewColumns.Add("mudlogging_dhrpm_min", "r.mudlogging_dhrpm_min");
            DefaultViewColumns.Add("mudlogging_dhrpm_max", "r.mudlogging_dhrpm_max");
            DefaultViewColumns.Add("mudlogging_torque_min", "r.mudlogging_torque_min");
            DefaultViewColumns.Add("mudlogging_torque_max", "r.mudlogging_torque_max");
            DefaultViewColumns.Add("mudlogging_flowrate_min", "r.mudlogging_flowrate_min");
            DefaultViewColumns.Add("mudlogging_flowrate_max", "r.mudlogging_flowrate_max");
            DefaultViewColumns.Add("mudlogging_spp_min", "r.mudlogging_spp_min");
            DefaultViewColumns.Add("mudlogging_spp_max", "r.mudlogging_spp_max");
            DefaultViewColumns.Add("mudlogging_spm_min", "r.mudlogging_spm_min");
            DefaultViewColumns.Add("mudlogging_spm_max", "r.mudlogging_spm_max");
            DefaultViewColumns.Add("mudlogging_spmpress_min", "r.mudlogging_spmpress_min");
            DefaultViewColumns.Add("mudlogging_spmpress_max", "r.mudlogging_spmpress_max");
            DefaultViewColumns.Add("mudlogging_avg_rop_min", "r.mudlogging_avg_rop_min");
            DefaultViewColumns.Add("mudlogging_avg_rop_max", "r.mudlogging_avg_rop_max");
            DefaultViewColumns.Add("mudlogging_hkld", "r.mudlogging_hkld");
            DefaultViewColumns.Add("mudlogging_mwi", "r.mudlogging_mwi");
            DefaultViewColumns.Add("mudlogging_mwo", "r.mudlogging_mwo");

            DefaultViewColumns.Add("weight_air", "r.weight_air");
            DefaultViewColumns.Add("weight_mud", "r.weight_mud");
            DefaultViewColumns.Add("wt_below_jars_air", "r.wt_below_jars_air");
            DefaultViewColumns.Add("wt_below_jars_mud", "r.wt_below_jars_mud");
            DefaultViewColumns.Add("string_weight", "r.string_weight");
            DefaultViewColumns.Add("weight_pick_up", "r.weight_pick_up");
            DefaultViewColumns.Add("weight_slack_down", "r.weight_slack_down");
            DefaultViewColumns.Add("weight_rotate", "r.weight_rotate");

            DefaultViewColumns.Add("dg_in_rows", "r.dg_in_rows");
            DefaultViewColumns.Add("dg_out_rows", "r.dg_out_rows");
            DefaultViewColumns.Add("dg_dull_char", "r.dg_dull_char");
            DefaultViewColumns.Add("dg_loc_cone", "r.dg_loc_cone");
            DefaultViewColumns.Add("dg_seals", "r.dg_seals");
            DefaultViewColumns.Add("dg_gauge", "r.dg_gauge");
            DefaultViewColumns.Add("dg_other_char", "r.dg_other_char");
            DefaultViewColumns.Add("dg_reason_pulled", "r.dg_reason_pulled");
            DefaultViewColumns.Add("dg_footage", "r.dg_footage");
            DefaultViewColumns.Add("dg_rop_overall", "r.dg_rop_overall");

            DefaultViewColumns.Add("depth_in", "r.depth_in");
            DefaultViewColumns.Add("depth_out", "r.depth_out");
            DefaultViewColumns.Add("duration", "r.duration");


            DefaultViewColumns.Add("bit_size", "wbit.bit_size");
            DefaultViewColumns.Add("serial_number", "wbit.serial_number");
            DefaultViewColumns.Add("well_name", "we.well_name");
            DefaultViewColumns.Add("drilling_date", "dr.drilling_date");
            DefaultViewColumns.Add("well_status", "we.well_status");
            DefaultViewColumns.Add("field_id", "we.field_id");
            DefaultViewColumns.Add("business_unit_id", "we.business_unit_id");

            #region Default View Base Record by
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");
            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("id", "ASC");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
