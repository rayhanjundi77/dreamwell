﻿using CommonTools;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public partial class well_drilling_parameter : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion

        static well_drilling_parameter()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  r.id,");
            sb.AppendLine(@"  r.created_by,");
            sb.AppendLine(@"  r.created_on,");
            sb.AppendLine(@"  r.modified_by,");
            sb.AppendLine(@"  r.modified_on,");
            sb.AppendLine(@"  r.approved_by,");
            sb.AppendLine(@"  r.approved_on,");
            sb.AppendLine(@"  r.is_active,");
            sb.AppendLine(@"  r.is_locked,");
            sb.AppendLine(@"  r.is_default,");
            sb.AppendLine(@"  r.owner_id,");
            sb.AppendLine(@"  r.organization_id,");
            sb.AppendLine(@"  o1.organization_name AS parent_organization_name,");
            sb.AppendLine(@"  r.mudlogging_wob_min,");
            sb.AppendLine(@"  t.team_name AS record_owning_team,");
            sb.AppendLine(@"  r.well_id,");
            sb.AppendLine(@"  r.spud_date,");
            sb.AppendLine(@"  r.mudlogging_wob_max,");
            sb.AppendLine(@"  r.mudlogging_dhrpm_min,");
            sb.AppendLine(@"  r.mudlogging_dhrpm_max,");
            sb.AppendLine(@"  r.mudlogging_flowrate_max,");
            sb.AppendLine(@"  r.mudlogging_spm_min,");
            sb.AppendLine(@"  r.mudlogging_flowrate_min,");
            sb.AppendLine(@"  r.mudlogging_spm_max,");
            sb.AppendLine(@"  r.mudlogging_rpm_min,");
            sb.AppendLine(@"  r.mudlogging_rpm_max,");
            sb.AppendLine(@"  r.mudlogging_torque_min,");
            sb.AppendLine(@"  r.mudlogging_torque_max,");
            sb.AppendLine(@"  r.mudlogging_spp_min,");
            sb.AppendLine(@"  r.mudlogging_spp_max,");
            sb.AppendLine(@"  r.mudlogging_spmpress_min,");
            sb.AppendLine(@"  r.mudlogging_spmpress_max,");
            sb.AppendLine(@"  r.mudlogging_avg_rop_min,");
            sb.AppendLine(@"  r.mudlogging_avg_rop_max");
            sb.AppendLine(@"FROM dbo.well_drilling_parameter AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.organization AS o1");
            sb.AppendLine(@"  ON r.organization_id = o1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");
            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("spud_date", "r.spud_date");
            DefaultViewColumns.Add("mudlogging_wob_max", "r.mudlogging_wob_max");
            DefaultViewColumns.Add("mudlogging_dhrpm_min", "r.mudlogging_dhrpm_min");
            DefaultViewColumns.Add("mudlogging_dhrpm_max", "r.mudlogging_dhrpm_max");
            DefaultViewColumns.Add("mudlogging_flowrate_min", "r.mudlogging_flowrate_min");
            DefaultViewColumns.Add("mudlogging_flowrate_max", "r.mudlogging_flowrate_max");
            DefaultViewColumns.Add("mudlogging_spm_min", "r.mudlogging_spm_min");
            DefaultViewColumns.Add("mudlogging_spm_max", "r.mudlogging_spm_max");
            DefaultViewColumns.Add("mudlogging_rpm_min", "r.mudlogging_rpm_min");
            DefaultViewColumns.Add("mudlogging_rpm_max", "r.mudlogging_rpm_max");
            DefaultViewColumns.Add("mudlogging_torque_min", "r.mudlogging_torque_min");
            DefaultViewColumns.Add("mudlogging_torque_max", "r.mudlogging_torque_max");
            DefaultViewColumns.Add("mudlogging_spp_min", "r.mudlogging_spp_min");
            DefaultViewColumns.Add("mudlogging_spp_max", "r.mudlogging_spp_max");
            DefaultViewColumns.Add("mudlogging_spmpress_min", "r.mudlogging_spmpress_min");
            DefaultViewColumns.Add("mudlogging_spmpress_max", "r.mudlogging_spmpress_max");
            DefaultViewColumns.Add("mudlogging_avg_rop_min", "r.mudlogging_avg_rop_min");
            DefaultViewColumns.Add("mudlogging_avg_rop_max", "r.mudlogging_avg_rop_max");

            #region Default View Base Record by
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");
            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("created_on", "DESC");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
