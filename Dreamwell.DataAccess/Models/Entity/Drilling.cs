﻿using CommonTools;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public partial class vw_drilling
    {
        [ResultColumn]
        public decimal mudlogging_flowrate_avg
        {
            get
            {
                if (mudlogging_flowrate_min >= 0 && mudlogging_flowrate_max >= 0)
                    return (decimal)(mudlogging_flowrate_min + mudlogging_flowrate_max) / 2;
                else
                    return 0;
            }
        }

        [ResultColumn]
        public decimal mudlogging_rpm_avg
        {
            get
            {
                if (mudlogging_rpm_min >= 0 && mudlogging_rpm_max >= 0)
                    return (decimal)(mudlogging_rpm_min + mudlogging_rpm_max) / 2;
                else
                    return 0;
            }
        }

        [ResultColumn]
        public decimal mudlogging_avg_rop_avg
        {
            get
            {
                if (mudlogging_avg_rop_min >= 0 && mudlogging_avg_rop_max >= 0)
                    return (decimal)(mudlogging_avg_rop_min + mudlogging_avg_rop_max) / 2;
                else
                    return 0;
            }
        }

        [ResultColumn]
        public decimal mudlogging_torque_avg
        {
            get
            {
                if (mudlogging_torque_min >= 0 && mudlogging_torque_max >= 0)
                    return (decimal)(mudlogging_torque_min + mudlogging_torque_max) / 2;
                else
                    return 0;
            }
        }

        [ResultColumn]
        public decimal mudlogging_spp_avg
        {
            get
            {
                if (mudlogging_spp_min >= 0 && mudlogging_spp_max >= 0)
                    return (decimal)(mudlogging_spp_min + mudlogging_spp_max) / 2;
                else
                    return 0;
            }
        }

        [ResultColumn]
        public bool CanSubmit
        {
            get
            {
                bool ret = false;

                if (submitted_on == null)
                {
                    ret = true;
                }

                return ret;
            }
        }

        [ResultColumn]
        public bool CanApprove { get; set; }

    }

    public partial class drilling : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion


        static drilling()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"r.id,");
            sb.AppendLine(@"r.created_by,");
            sb.AppendLine(@"r.created_on,");
            sb.AppendLine(@"r.modified_by,");
            sb.AppendLine(@"r.modified_on,");
            sb.AppendLine(@"r.approved_by,");
            sb.AppendLine(@"r.approved_on,");
            sb.AppendLine(@"r.submitted_by,");
            sb.AppendLine(@"r.submitted_on,");
            sb.AppendLine(@"r.is_active,");
            sb.AppendLine(@"r.is_locked,");
            sb.AppendLine(@"r.is_default,");
            sb.AppendLine(@"r.owner_id,");
            sb.AppendLine(@"r.organization_id,");
            sb.AppendLine(@"o1.organization_name AS organization_name,");
            sb.AppendLine(@"c.country_name,");
            sb.AppendLine(@"r.well_id,");
            sb.AppendLine(@"w.well_name,");
            sb.AppendLine(@"w.well_classification,");
            sb.AppendLine(@"w.well_type,");
            sb.AppendLine(@"w.latitude,");
            sb.AppendLine(@"w.longitude,");
            sb.AppendLine(@"w.spud_date,");
            sb.AppendLine(@"w.target_direction,");
            sb.AppendLine(@"f.id AS field_id,");
            sb.AppendLine(@"f.field_name AS field_name,");
            sb.AppendLine(@"r.drilling_date,");
            sb.AppendLine(@"r.event,");
            sb.AppendLine(@"dc.contractor_name,");
            sb.AppendLine(@"r.report_no,");
            sb.AppendLine(@"w.rig_rating,");
            sb.AppendLine(@"r.rig_rating_uom,");
            sb.AppendLine(@"w.rig_type,");
            sb.AppendLine(@"r.water_depth,");
            sb.AppendLine(@"r.water_depth_uom,");
            sb.AppendLine(@"w.rkb_elevation,");
            sb.AppendLine(@"r.rkb_elevation_uom,");
            sb.AppendLine(@"w.rt_to_seabed,");
            sb.AppendLine(@"r.rt_to_seabed_uom,");
            sb.AppendLine(@"w.rig_heading,");
            sb.AppendLine(@"r.release_date,");
            sb.AppendLine(@"a.id as afe_id,");
            sb.AppendLine(@"rg.name as rig_name,");
            sb.AppendLine(@"a.afe_no,");
            sb.AppendLine(@"w.afe_cost,");
            sb.AppendLine(@"r.afe_cost_currency_id,");
            sb.AppendLine(@"daily.daily_cost,");
            sb.AppendLine(@"r.daily_mud_cost,");
            sb.AppendLine(@"r.daily_mud_cost_currency_id,");
            sb.AppendLine(@"r.cummulative_cost,");
            //sb.AppendLine(@"cmcc.cummulative_cost_count,");            
            sb.AppendLine(@"r.cummulative_cost_currency_id,");
            sb.AppendLine(@"r.cummulative_mud_cost,");
            sb.AppendLine(@"r.cummulative_mud_cost_currency_id,");
            sb.AppendLine(@"w.planned_td,");
            sb.AppendLine(@"r.planned_td_uom,");
            sb.AppendLine(@"w.planned_days,");
            sb.AppendLine(@"r.planned_days_uom,");
            sb.AppendLine(@"r.dol,");
            sb.AppendLine(@"r.dol_uom,");
            sb.AppendLine(@"r.dfs,");
            sb.AppendLine(@"r.dfs_uom,");
            sb.AppendLine(@"--r.current_hole_size,");
            sb.AppendLine(@"--r.current_hole_size_uom,");
            sb.AppendLine(@"COALESCE(d.previous_depth_md, 0) AS previous_depth_md,");
            sb.AppendLine(@"r.previous_depth_md_uom,");
            sb.AppendLine(@"COALESCE(d.previous_depth_tvd, 0) AS previous_depth_tvd,");
            sb.AppendLine(@"r.previous_depth_tvd_uom,");
            sb.AppendLine(@"COALESCE(r.current_depth_md, 0) AS current_depth_md,");
            sb.AppendLine(@"r.current_depth_md_uom,");
            sb.AppendLine(@"COALESCE(r.current_depth_tvd, 0) AS current_depth_tvd,");
            sb.AppendLine(@"r.current_depth_tvd_uom,");
            sb.AppendLine(@"r.hsse_incident_acident,");
            sb.AppendLine(@"r.hsse_environtmental_spills,");
            sb.AppendLine(@"r.hsse_safety_alert_received,");
            sb.AppendLine(@"r.hsse_proactive_safety,");
            sb.AppendLine(@"r.hsse_near_miss_report,");
            sb.AppendLine(@"r.hsse_exercise,");
            sb.AppendLine(@"r.hsse_social_issues,");
            sb.AppendLine(@"r.hsse_safety_meeting,");
            sb.AppendLine(@"r.hsse_stop_cards,");
            sb.AppendLine(@"r.hsse_tofs,");
            sb.AppendLine(@"r.hsse_jsa,");
            sb.AppendLine(@"r.hsse_inductions,");
            sb.AppendLine(@"r.hsse_audits,");
            sb.AppendLine(@"r.hsse_days_since_lti,");
            sb.AppendLine(@"r.hsse_tbop_press,");
            sb.AppendLine(@"r.hsse_tbop_func,");
            sb.AppendLine(@"r.hsse_dkick,");
            sb.AppendLine(@"r.hsse_dstrip,");
            sb.AppendLine(@"r.hsse_dfire,");
            sb.AppendLine(@"r.hsse_dmis_pers,");
            sb.AppendLine(@"r.hsse_aband_rig,");
            sb.AppendLine(@"r.hsse_dh2s,");
            sb.AppendLine(@"r.hsse_description,");
            sb.AppendLine(@"r.hsse_incident_description,");
            sb.AppendLine(@"r.hsse_type,");
            sb.AppendLine(@"r.hsse_lta,");
            sb.AppendLine(@"r.hsse_h2stest,");
            sb.AppendLine(@"r.hsse_mtg,");
            sb.AppendLine(@"r.hsse_kick_trip,");
            sb.AppendLine(@"r.hsse_kick_drill,");
            sb.AppendLine(@"");
            sb.AppendLine(@"r.mudlogging_wob_min,");
            sb.AppendLine(@"r.mudlogging_wob_max,");
            sb.AppendLine(@"r.mudlogging_wob_uom,");
            sb.AppendLine(@"r.mudlogging_rpm_min,");
            sb.AppendLine(@"r.mudlogging_rpm_max,");
            sb.AppendLine(@"r.mudlogging_rpm_uom,");
            sb.AppendLine(@"r.mudlogging_dhrpm_min,");
            sb.AppendLine(@"r.mudlogging_dhrpm_max,");
            sb.AppendLine(@"r.mudlogging_dhrpm_uom,");
            sb.AppendLine(@"r.mudlogging_torque_min,");
            sb.AppendLine(@"r.mudlogging_torque_max,");
            sb.AppendLine(@"r.mudlogging_torque_uom,");
            sb.AppendLine(@"r.mudlogging_flowrate_min,");
            sb.AppendLine(@"r.mudlogging_flowrate_max,");
            sb.AppendLine(@"r.mudlogging_flowrate_uom,");
            sb.AppendLine(@"r.mudlogging_spp_min,");
            sb.AppendLine(@"r.mudlogging_spp_max,");
            sb.AppendLine(@"r.mudlogging_spp_uom,");
            sb.AppendLine(@"r.mudlogging_spm_min,");
            sb.AppendLine(@"r.mudlogging_spm_max,");
            sb.AppendLine(@"r.mudlogging_spm_uom,");
            sb.AppendLine(@"r.mudlogging_spmpress_min,");
            sb.AppendLine(@"r.mudlogging_spmpress_max,");
            sb.AppendLine(@"r.mudlogging_spmpress_uom,");
            sb.AppendLine(@"r.mudlogging_avg_rop_min,");
            sb.AppendLine(@"r.mudlogging_avg_rop_max,");
            sb.AppendLine(@"r.mudlogging_avg_rop_uom,");
            sb.AppendLine(@"");
            sb.AppendLine(@"r.weather_general,");
            sb.AppendLine(@"r.weather_wind_speed,");
            sb.AppendLine(@"r.weather_wind_direction,");
            sb.AppendLine(@"r.weather_temperature,");
            sb.AppendLine(@"r.weather_temperature_low,");
            sb.AppendLine(@"r.weather_visibility,");
            sb.AppendLine(@"r.weather_cloud,");
            sb.AppendLine(@"r.weather_barometer,");
            sb.AppendLine(@"r.weather_wave,");
            sb.AppendLine(@"r.weather_wave_period,");
            sb.AppendLine(@"r.weather_wave_direction,");
            sb.AppendLine(@"r.weather_height,");
            sb.AppendLine(@"r.weather_current_speed,");
            sb.AppendLine(@"r.weather_pitch,");
            sb.AppendLine(@"r.weather_roll,");
            sb.AppendLine(@"r.weather_heave,");
            sb.AppendLine(@"r.weather_comments,");
            sb.AppendLine(@"r.weather_road_condition,");
            sb.AppendLine(@"r.weather_chill_factor,");
            sb.AppendLine(@"");
            sb.AppendLine(@"r.operation_data_period,");
            sb.AppendLine(@"r.operation_data_early,");
            sb.AppendLine(@"r.operation_data_planned,");
            sb.AppendLine(@"aps.approval_level,");
            sb.AppendLine(@"aps.approval_rejected_level,");
            sb.AppendLine(@"aps.approval_status,");
            sb.AppendLine(@"");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u4.last_name), N''), ' ', ISNULL(u4.first_name, N''))))  AS record_submitted_by,");
            sb.AppendLine(@"  t.team_name  AS record_owning_team");
            sb.AppendLine(@"FROM dbo.drilling AS r");
            sb.AppendLine(@"OUTER APPLY (");
            sb.AppendLine(@"	SELECT TOP 1 COALESCE(d.current_depth_tvd, 0) AS previous_depth_tvd, COALESCE(d.current_depth_md, 0) AS previous_depth_md ");
            sb.AppendLine(@"	FROM drilling d ");
            sb.AppendLine(@"	WHERE d.well_id = r.well_id AND d.drilling_date < r.drilling_date");
            sb.AppendLine(@"	ORDER BY d.drilling_date DESC");
            sb.AppendLine(@"	) AS d");
            sb.AppendLine(@"OUTER APPLY (SELECT daily_cost FROM [dbo].[udf_get_daily_cost](r.id)) AS daily");
            sb.AppendLine(@"OUTER APPLY(SELECT cummulative_cost_count FROM[dbo].[udf_get_cummulative_cost_count] (r.well_id, r.drilling_date)) AS cmcc");
            sb.AppendLine(@"OUTER APPLY (SELECT * FROM [dbo].[udf_get_approval_status](r.id)) AS aps");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.well AS w");
            sb.AppendLine(@"  ON r.well_id = w.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.field AS f");
            sb.AppendLine(@"  ON w.field_id = f.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.country AS c");
            sb.AppendLine(@"  ON w.country_id = c.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.drilling_contractor AS dc");
            sb.AppendLine(@"  ON w.drilling_contractor_id = dc.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.afe AS a");
            sb.AppendLine(@"  ON a.well_id = w.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.rig AS rg");
            sb.AppendLine(@"  ON w.rig_id = rg.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.organization AS o1");
            sb.AppendLine(@"  ON r.organization_id = o1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u4");
            sb.AppendLine(@"  ON r.submitted_by = u4.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");
            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("submitted_by", "r.submitted_by");
            DefaultViewColumns.Add("submitted_on", "r.submitted_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            #region Dynamic Record
            DefaultViewColumns.Add("organization_id", "r.organization_id");
            DefaultViewColumns.Add("organization_name", "o1.organization_name");
            DefaultViewColumns.Add("country_name", "c.country_name");
            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("well_name", "w.well_name");
            DefaultViewColumns.Add("well_classification", "w.well_classification");
            DefaultViewColumns.Add("well_type", "w.well_type");
            DefaultViewColumns.Add("latitude", "w.latitude");
            DefaultViewColumns.Add("longitude", "w.longitude");
            DefaultViewColumns.Add("spud_date", "w.spud_date");
            DefaultViewColumns.Add("target_direction", "w.target_direction");
            DefaultViewColumns.Add("field_id", "f.id");
            DefaultViewColumns.Add("field_name", "f.field_name");
            DefaultViewColumns.Add("drilling_date", "r.drilling_date");
            DefaultViewColumns.Add("event", "r.event");
            DefaultViewColumns.Add("contractor_name", "dc.contractor_name");
            DefaultViewColumns.Add("report_no", "r.report_no");
            DefaultViewColumns.Add("rig_rating", "w.rig_rating");
            DefaultViewColumns.Add("rig_rating_uom", "r.rig_rating_uom");
            DefaultViewColumns.Add("rig_type", "w.rig_type");
            DefaultViewColumns.Add("water_depth", "r.water_depth");
            DefaultViewColumns.Add("water_depth_uom", "r.water_depth_uom");
            DefaultViewColumns.Add("rkb_elevation", "w.rkb_elevation");
            DefaultViewColumns.Add("rkb_elevation_uom", "r.rkb_elevation_uom");
            DefaultViewColumns.Add("rt_to_seabed", "w.rt_to_seabed");
            DefaultViewColumns.Add("rt_to_seabed_uom", "r.rt_to_seabed_uom");
            DefaultViewColumns.Add("rig_heading", "w.rig_heading");
            DefaultViewColumns.Add("release_date", "r.release_date");
            DefaultViewColumns.Add("afe_id", "a.id");
            DefaultViewColumns.Add("rig_name", "rg.name");
            DefaultViewColumns.Add("afe_no", "a.afe_no");
            DefaultViewColumns.Add("afe_cost", "w.afe_cost");
            DefaultViewColumns.Add("afe_cost_currency_id", "r.afe_cost_currency_id");
            DefaultViewColumns.Add("daily_cost", "daily.daily_cost");
            DefaultViewColumns.Add("daily_mud_cost", "r.daily_mud_cost");
            DefaultViewColumns.Add("daily_mud_cost_currency_id", "r.daily_mud_cost_currency_id");
            DefaultViewColumns.Add("cummulative_cost", "r.cummulative_cost");
            DefaultViewColumns.Add("cummulative_cost_currency_id", "r.cummulative_cost_currency_id");
            DefaultViewColumns.Add("cummulative_mud_cost", "r.cummulative_mud_cost");
            DefaultViewColumns.Add("cummulative_mud_cost_currency_id", "r.cummulative_mud_cost_currency_id");
            DefaultViewColumns.Add("planned_td", "w.planned_td");
            DefaultViewColumns.Add("planned_td_uom", "r.planned_td_uom");
            DefaultViewColumns.Add("planned_days", "w.planned_days");
            DefaultViewColumns.Add("planned_days_uom", "r.planned_days_uom");
            DefaultViewColumns.Add("dol", "r.dol");
            DefaultViewColumns.Add("dol_uom", "r.dol_uom");
            DefaultViewColumns.Add("dfs", "r.dfs");
            DefaultViewColumns.Add("dfs_uom", "r.dfs_uom");
            DefaultViewColumns.Add("current_hole_size", "r.current_hole_size");
            DefaultViewColumns.Add("current_hole_size_uom", "r.current_hole_size_uom");

            DefaultViewColumns.Add("previous_depth_md", "COALESCE(d.previous_depth_md, 0)");
            DefaultViewColumns.Add("previous_depth_md_uom", "r.previous_depth_md_uom");
            DefaultViewColumns.Add("previous_depth_tvd", "COALESCE(d.previous_depth_tvd, 0)");
            DefaultViewColumns.Add("previous_depth_tvd_uom", "r.previous_depth_tvd_uom");
            DefaultViewColumns.Add("current_depth_md", "COALESCE(r.current_depth_md, 0)");
            DefaultViewColumns.Add("current_depth_md_uom", "r.current_depth_md_uom");
            DefaultViewColumns.Add("current_depth_tvd", "COALESCE(r.current_depth_tvd, 0)");
            DefaultViewColumns.Add("current_depth_tvd_uom", "r.current_depth_tvd_uom");

            DefaultViewColumns.Add("progress", "r.progress");
            DefaultViewColumns.Add("progress_uom", "r.progress_uom");

            DefaultViewColumns.Add("hsse_incident_acident", "r.hsse_incident_acident");
            DefaultViewColumns.Add("hsse_environtmental_spills", "r.hsse_environtmental_spills");
            DefaultViewColumns.Add("hsse_safety_alert_received", "r.hsse_safety_alert_received");
            DefaultViewColumns.Add("hsse_proactive_safety", "r.hsse_proactive_safety");
            DefaultViewColumns.Add("hsse_near_miss_report", "r.hsse_near_miss_report");
            DefaultViewColumns.Add("hsse_exercise", "r.hsse_exercise");
            DefaultViewColumns.Add("hsse_social_issues", "r.hsse_social_issues");
            DefaultViewColumns.Add("hsse_safety_meeting", "r.hsse_safety_meeting");
            DefaultViewColumns.Add("hsse_stop_cards", "r.hsse_stop_cards");
            DefaultViewColumns.Add("hsse_tofs", "r.hsse_tofs");
            DefaultViewColumns.Add("hsse_jsa", "r.hsse_jsa");
            DefaultViewColumns.Add("hsse_inductions", "r.hsse_inductions");
            DefaultViewColumns.Add("hsse_audits", "r.hsse_audits");
            DefaultViewColumns.Add("hsse_days_since_lti", "r.hsse_days_since_lti");
            DefaultViewColumns.Add("hsse_tbop_press", "r.hsse_tbop_press");
            DefaultViewColumns.Add("hsse_tbop_func", "r.hsse_tbop_func");
            DefaultViewColumns.Add("hsse_dkick", "r.hsse_dkick");
            DefaultViewColumns.Add("hsse_dstrip", "r.hsse_dstrip");
            DefaultViewColumns.Add("hsse_dfire", "r.hsse_dfire");
            DefaultViewColumns.Add("hsse_dmis_pers", "r.hsse_dmis_pers");
            DefaultViewColumns.Add("hsse_aband_rig", "r.hsse_aband_rig");
            DefaultViewColumns.Add("hsse_dh2s", "r.hsse_dh2s");
            DefaultViewColumns.Add("hsse_description", "r.hsse_description");
            DefaultViewColumns.Add("hsse_incident_description", "r.hsse_incident_description");
            DefaultViewColumns.Add("hsse_type", "r.hsse_type");
            DefaultViewColumns.Add("hsse_lta", "r.hsse_lta");
            DefaultViewColumns.Add("hsse_h2stest", "r.hsse_h2stest");
            DefaultViewColumns.Add("hsse_mtg", "r.hsse_mtg");
            DefaultViewColumns.Add("hsse_kick_trip", "r.hsse_kick_trip");
            DefaultViewColumns.Add("hsse_kick_drill", "r.hsse_kick_drill");

            DefaultViewColumns.Add("mudlogging_wob_min", "r.mudlogging_wob_min");
            DefaultViewColumns.Add("mudlogging_wob_max", "r.mudlogging_wob_max");
            DefaultViewColumns.Add("mudlogging_wob_uom", "r.mudlogging_wob_uom");
            DefaultViewColumns.Add("mudlogging_rpm_min", "r.mudlogging_rpm_min");
            DefaultViewColumns.Add("mudlogging_rpm_max", "r.mudlogging_rpm_max");
            DefaultViewColumns.Add("mudlogging_rpm_uom", "r.mudlogging_rpm_uom");
            DefaultViewColumns.Add("mudlogging_dhrpm_min", "r.mudlogging_dhrpm_min");
            DefaultViewColumns.Add("mudlogging_dhrpm_max", "r.mudlogging_dhrpm_max");
            DefaultViewColumns.Add("mudlogging_dhrpm_uom", "r.mudlogging_dhrpm_uom");
            DefaultViewColumns.Add("mudlogging_torque_min", "r.mudlogging_torque_min");
            DefaultViewColumns.Add("mudlogging_torque_max", "r.mudlogging_torque_max");
            DefaultViewColumns.Add("mudlogging_torque_uom", "r.mudlogging_torque_uom");
            DefaultViewColumns.Add("mudlogging_flowrate_min", "r.mudlogging_flowrate_min");
            DefaultViewColumns.Add("mudlogging_flowrate_max", "r.mudlogging_flowrate_max");
            DefaultViewColumns.Add("mudlogging_flowrate_uom", "r.mudlogging_flowrate_uom");
            DefaultViewColumns.Add("mudlogging_spp_min", "r.mudlogging_spp_min");
            DefaultViewColumns.Add("mudlogging_spp_max", "r.mudlogging_spp_max");
            DefaultViewColumns.Add("mudlogging_spp_uom", "r.mudlogging_spp_uom");
            DefaultViewColumns.Add("mudlogging_spm_min", "r.mudlogging_spm_min");
            DefaultViewColumns.Add("mudlogging_spm_max", "r.mudlogging_spm_max");
            DefaultViewColumns.Add("mudlogging_spm_uom", "r.mudlogging_spm_uom");
            DefaultViewColumns.Add("mudlogging_spmpress_min", "r.mudlogging_spmpress_min");
            DefaultViewColumns.Add("mudlogging_spmpress_max", "r.mudlogging_spmpress_max");
            DefaultViewColumns.Add("mudlogging_spmpress_uom", "r.mudlogging_spmpress_uom");
            DefaultViewColumns.Add("mudlogging_avg_rop_min", "r.mudlogging_avg_rop_min");
            DefaultViewColumns.Add("mudlogging_avg_rop_max", "r.mudlogging_avg_rop_max");
            DefaultViewColumns.Add("mudlogging_avg_rop_uom", "r.mudlogging_avg_rop_uom");

            DefaultViewColumns.Add("weather_general", "r.weather_general");
            DefaultViewColumns.Add("weather_wind_speed", "r.weather_wind_speed");
            DefaultViewColumns.Add("weather_wind_direction", "r.weather_wind_direction");
            DefaultViewColumns.Add("weather_temperature", "r.weather_temperature");
            DefaultViewColumns.Add("weather_temperature_low", "r.weather_temperature_low");
            DefaultViewColumns.Add("weather_visibility", "r.weather_visibility");
            DefaultViewColumns.Add("weather_cloud", "r.weather_cloud");
            DefaultViewColumns.Add("weather_barometer", "r.weather_barometer");
            DefaultViewColumns.Add("weather_wave", "r.weather_wave");
            DefaultViewColumns.Add("weather_wave_period", "r.weather_wave_period");
            DefaultViewColumns.Add("weather_wave_direction", "r.weather_wave_direction");
            DefaultViewColumns.Add("weather_height", "r.weather_height");
            DefaultViewColumns.Add("weather_current_speed", "r.weather_current_speed");
            DefaultViewColumns.Add("weather_pitch", "r.weather_pitch");
            DefaultViewColumns.Add("weather_roll", "r.weather_roll");
            DefaultViewColumns.Add("weather_heave", "r.weather_heave");
            DefaultViewColumns.Add("weather_comments", "r.weather_comments");
            DefaultViewColumns.Add("weather_road_condition", "r.weather_road_condition");
            DefaultViewColumns.Add("weather_chill_factor", "r.weather_chill_factor");

            DefaultViewColumns.Add("approval_level", "aps.approval_level");
            DefaultViewColumns.Add("approval_rejected_level", "aps.approval_rejected_level");
            DefaultViewColumns.Add("approval_status", "aps.approval_status");
            #endregion

            #region Default View Base Record by
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_submitted_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u4.last_name), N''), ' ', ISNULL(u4.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");

            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("drilling_date", "ASC");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
