﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Models.Entity
{
    [Table("casing")]
    public partial class Casing
    {
        [ExplicitKey]
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public bool? is_active { get; set; }
        public bool? is_locked { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public string organization_id { get; set; }
        public string name { get; set; }
        public string hole_id { get; set; }
    }

    public partial class CasingViewModel
    {
        [ExplicitKey]
        public string id { get; set; }
        public string name { get; set; }
        public string hole_id { get; set; }
        public string hole_name { get; set; }
    }

    public partial class Vw_Casing
    {
        [ExplicitKey]
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public bool? is_active { get; set; }
        public bool? is_locked { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public string organization_id { get; set; }
        public string name { get; set; }
        public string hole { get; set; }
        public bool? hole_type { get; set; }

    }
}
