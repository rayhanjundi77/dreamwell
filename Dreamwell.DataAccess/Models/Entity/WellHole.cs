﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Models.Entity
{
    [Table("well_hole")]
    public partial class WellHole
    {
        [ExplicitKey]
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public bool? is_active { get; set; }
        public bool? is_locked { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public string organization_id { get; set; }
        public string well_id { get; set; }
        public string hole_id { get; set; }
        public decimal? hole_depth { get; set; }
        public decimal? plan_cost { get; set; }
        public decimal? plan_volume { get; set; }
    }

    [Table("vw_well_hole_and_casing")]
    public partial class WellHoleCasing
    {
        [ExplicitKey]
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public bool? is_active { get; set; }
        public bool? is_locked { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string organization_id { get; set; }
        public string organization_name { get; set; }
        public string well_id { get; set; }
        public string hole_and_casing_id { get; set; }
        public int? seq { get; set; }
        public string hole_name { get; set; }
        public bool? hole_type { get; set; }
        public decimal? hole_depth { get; set; }
        public string casing_name { get; set; }
        public decimal? casing_setting_depth { get; set; }
        public string casing_type { get; set; }
        public decimal? casing_weight { get; set; }
        public string casing_grade { get; set; }
        public string casing_connection { get; set; }
        public string casing_top_of_liner { get; set; }
        public decimal? plan_cost { get; set; }
        public decimal? plan_volume { get; set; }
        public decimal? actual_depth { get; set; }
        public int? multicasing { get; set; }
        public string parent_well_hole_casing_id { get; set; }
        public string record_created_by { get; set; }
        public string record_modified_by { get; set; }
        public string record_approved_by { get; set; }
        public string record_owner { get; set; }
        public string record_owning_team { get; set; }
    }

    public partial class WellHoleRequest
    {
        public string id { get; set; }
        public string well_id { get; set; }
        public string hole_id { get; set; }
        public bool hole_type { get; set; }
        public decimal? hole_depth { get; set; }
        public decimal? plan_cost { get; set; }
        public decimal? plan_volume { get; set; }
        public List<WellCasing> casings { get; set; }
    }


    public partial class WellHoleResponse
    {
        public string id { get; set; }
        public string hole_id { get; set; }
        public string hole_name { get; set; }
        public bool? hole_type { get; set; }
        public decimal? hole_depth { get; set; }
        public decimal? plan_cost { get; set; }
        public decimal? plan_volume { get; set; }
        public List<WellCasingResponse> casings { get; set; }
        public List<well_deviation> deviations { get; set; }
    }

    public partial class WellHoleSchemanticResponse
    {
        public string id { get; set; }
        public string hole_id { get; set; }
        public string hole_name { get; set; }
        public bool? hole_type { get; set; }
        public decimal? hole_depth { get; set; }
        public decimal? plan_cost { get; set; }
        public decimal? plan_volume { get; set; }
        public string casing_id { get; set; }
        public string casing_name { get; set; }
        public decimal? casing_setting_depth { get; set; }
        public string casing_type { get; set; }
        public decimal? casing_weight { get; set; }
        public string casing_grade { get; set; }
        public string casing_connection { get; set; }
        public string casing_top_of_liner { get; set; }
    }
}
