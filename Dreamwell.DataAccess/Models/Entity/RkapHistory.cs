﻿using Dapper.Contrib.Extensions;
using System;

namespace Dreamwell.DataAccess.Models.Entity
{
    [Table("rkap_history")]
    public class rkap_history
    {
        [ExplicitKey]
        public string id { get; set; }
        public string header_id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public int status { get; set; }
        public string well_classification { get; set; }
        public string well_name { get; set; }
        public string business_unit_id { get; set; }
        public string asset_id { get; set; }
        public string field_id { get; set; }
        public DateTime? spud_date { get; set; }
        public DateTime? end_date { get; set; }
        public decimal? planned_days { get; set; }
        public string rig_id { get; set; }
        public decimal hp_no { get; set; }
        //public string horse_power_unit { get; set; }
        public decimal water_depth { get; set; }
        //public string water_depth_unit { get; set; }
        public decimal budget { get; set; }
        public string budget_currency { get; set; }
        public string well_id { get; set; }
    }

    [Table("vw_rkap_history")]
    public class vw_rkap_history
    {
        [ExplicitKey]
        public string id { get; set; }
        public int status { get; set; }
        public string well_classification { get; set; }
        public string well_name { get; set; }
        public string business_unit_id { get; set; }
        public string asset_id { get; set; }
        public string field_id { get; set; }
        public DateTime? spud_date { get; set; }
        public DateTime? end_date { get; set; }
        public decimal? planned_days { get; set; }
        public string rig_id { get; set; }
        public decimal hp_no { get; set; }
        //public string horse_power_unit { get; set; }
        //public string horse_power_unit_name { get; set; }
        public decimal water_depth { get; set; }
        public string rig_name { get; set; }
        public string business_unit_name { get; set; }
        //public string water_depth_unit { get; set; }
        //public string water_depth_unit_name { get; set; }
    }
}
