﻿using CommonTools;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public partial class well_bit : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion


        static well_bit()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"	r.id,");
            sb.AppendLine(@"	r.created_by,");
            sb.AppendLine(@"	r.created_on,");
            sb.AppendLine(@"	r.modified_by,");
            sb.AppendLine(@"	r.modified_on,");
            sb.AppendLine(@"	r.approved_by,");
            sb.AppendLine(@"	r.approved_on,");
            sb.AppendLine(@"	r.is_active,");
            sb.AppendLine(@"	r.is_locked,");
            sb.AppendLine(@"	r.is_default,");
            sb.AppendLine(@"	r.owner_id,");
            sb.AppendLine(@"	r.organization_id,");
            sb.AppendLine(@"	o1.organization_name AS organization_name,");
            sb.AppendLine(@"	r.well_id,");
            sb.AppendLine(@"	r.bit_number,");
            sb.AppendLine(@"	r.bit_size,");
            sb.AppendLine(@"	COALESCE(r.bit_run, 0) AS bit_run,");
            sb.AppendLine(@"	r.bit_type,");
            sb.AppendLine(@"	r.manufacturer,");
            sb.AppendLine(@"	r.iadc_code,");
            sb.AppendLine(@"	i.description AS iadc_description,");
            sb.AppendLine(@"	r.jets,");
            sb.AppendLine(@"	r.serial_number,");
            sb.AppendLine(@"	r.depth_out,");
            sb.AppendLine(@"	r.depth_in,");
            sb.AppendLine(@"	r.meterage,");
            sb.AppendLine(@"	r.bit_hour,");
            sb.AppendLine(@"	r.noozle_1,");
            sb.AppendLine(@"	r.noozle_2,");
            sb.AppendLine(@"	r.noozle_3,");
            sb.AppendLine(@"	r.noozle_4,");
            sb.AppendLine(@"	r.noozle_5,");
            sb.AppendLine(@"	r.count_1,");
            sb.AppendLine(@"	r.count_2,");
            sb.AppendLine(@"	r.count_3,");
            sb.AppendLine(@"	r.count_4,");
            sb.AppendLine(@"	r.count_5,");
            sb.AppendLine(@"	r.TFA,");
            sb.AppendLine(@"	r.dull_grade,");
            sb.AppendLine(@"	r.dg_in_rows,");
            sb.AppendLine(@"	r.dg_out_rows,");
            sb.AppendLine(@"	r.dg_dull_char,");
            sb.AppendLine(@"	r.dg_loc_cone,");
            sb.AppendLine(@"	r.dg_seals,");
            sb.AppendLine(@"	r.dg_gauge,");
            sb.AppendLine(@"	r.dg_other_char,");
            sb.AppendLine(@"	r.dg_reason_pulled,");
            sb.AppendLine(@"	r.dg_depth_in,");
            sb.AppendLine(@"	r.dg_depth_out,");
            sb.AppendLine(@"	r.dg_hours_on_bottom,");
            sb.AppendLine(@"	r.dg_footage,");
            sb.AppendLine(@"	r.dg_rop_overall,");
            sb.AppendLine(@"	RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,");
            sb.AppendLine(@"	RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,");
            sb.AppendLine(@"	RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,");
            sb.AppendLine(@"	RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,");
            sb.AppendLine(@"	t.team_name  AS record_owning_team");
            sb.AppendLine(@"FROM dbo.well_bit AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.organization AS o1");
            sb.AppendLine(@"  ON r.organization_id = o1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");
            sb.AppendLine(@"LEFT OUTER JOIN iadc as i");
            sb.AppendLine(@"  ON r.iadc_code = i.id");
            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            #region Dynamic Record
            DefaultViewColumns.Add("organization_id", "r.organization_id");
            DefaultViewColumns.Add("organization_name", "o1.organization_name"); 

            DefaultViewColumns.Add("well_id", "d.well_id");
            DefaultViewColumns.Add("bit_number", "r.bit_number");
            DefaultViewColumns.Add("bit_size", "r.bit_size");
            DefaultViewColumns.Add("bit_run", "COALESCE(r.bit_run, 0)");
            DefaultViewColumns.Add("bit_type", "r.bit_type"); 
            DefaultViewColumns.Add("manufacturer", "r.manufacturer");
            DefaultViewColumns.Add("iadc_code", "r.iadc_code");
            DefaultViewColumns.Add("iadc_description", "i.description AS iadc_description");
            DefaultViewColumns.Add("jets", "r.jets");
            DefaultViewColumns.Add("serial_number", "r.serial_number");
            DefaultViewColumns.Add("depth_out", "r.depth_out");
            DefaultViewColumns.Add("depth_in", "r.depth_in");
            DefaultViewColumns.Add("meterage", "r.meterage");
            DefaultViewColumns.Add("bit_hour", "r.bit_hour");
            DefaultViewColumns.Add("noozle_1", "r.noozle_1");
            DefaultViewColumns.Add("noozle_2", "r.noozle_2");
            DefaultViewColumns.Add("noozle_3", "r.noozle_3");
            DefaultViewColumns.Add("noozle_4", "r.noozle_4");
            DefaultViewColumns.Add("noozle_5", "r.noozle_5");
            DefaultViewColumns.Add("count_1", "r.count_1");
            DefaultViewColumns.Add("count_2", "r.count_2");
            DefaultViewColumns.Add("count_3", "r.count_3");
            DefaultViewColumns.Add("count_4", "r.count_4");
            DefaultViewColumns.Add("count_5", "r.count_5");
            DefaultViewColumns.Add("TFA", "r.TFA");
            DefaultViewColumns.Add("dull_grade", "r.dull_grade");
            DefaultViewColumns.Add("dg_in_rows", "r.dg_in_rows");
            DefaultViewColumns.Add("dg_out_rows", "r.dg_out_rows");
            DefaultViewColumns.Add("dg_dull_char", "r.dg_dull_char");
            DefaultViewColumns.Add("dg_loc_cone", "r.dg_loc_cone");
            DefaultViewColumns.Add("dg_seals", "r.dg_seals");
            DefaultViewColumns.Add("dg_gauge", "r.dg_gauge");
            DefaultViewColumns.Add("dg_other_char", "r.dg_other_char");
            DefaultViewColumns.Add("dg_reason_pulled", "r.dg_reason_pulled");
            DefaultViewColumns.Add("dg_depth_in", "r.dg_depth_in");
            DefaultViewColumns.Add("dg_depth_out", "r.dg_depth_out");
            DefaultViewColumns.Add("dg_hours_on_bottom", "r.dg_hours_on_bottom");
            DefaultViewColumns.Add("dg_footage", "r.dg_footage");
            DefaultViewColumns.Add("dg_rop_overall", "r.dg_rop_overall");

            #endregion

            #region Default View Base Record by
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");

            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("bit_number", "ASC");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
