﻿using CommonTools;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public partial class vw_drilling_operation_activity
    {
        [ResultColumn]
        public string Hours {
            get {
                var interval = operation_end_date - operation_start_date;
                return interval.Value.TotalHours.ToString("0.##");
            }
        }
    }

    public partial class drilling_operation_activity : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion


        static drilling_operation_activity()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"	r.id,");
            sb.AppendLine(@"	r.created_by,");
            sb.AppendLine(@"	r.created_on,");
            sb.AppendLine(@"	r.modified_by,");
            sb.AppendLine(@"	r.modified_on,");
            sb.AppendLine(@"	r.approved_by,");
            sb.AppendLine(@"	r.approved_on,");
            sb.AppendLine(@"	r.is_active,");
            sb.AppendLine(@"	r.is_locked,");
            sb.AppendLine(@"	r.is_default,");
            sb.AppendLine(@"	r.owner_id,");
            sb.AppendLine(@"	r.drilling_id,");
            sb.AppendLine(@"	r.drilling_hole_and_casing_id,");
            sb.AppendLine(@"	dh.hole_val,");
            sb.AppendLine(@"	dh.casing_val,");
            sb.AppendLine(@"	dh.lot_fit,");
            sb.AppendLine(@"	dh.lot_fit_val, ");
            sb.AppendLine(@"	dh.cement_val,");
            sb.AppendLine(@"	dh.seq,");
            sb.AppendLine(@"	d.drilling_date,");
            sb.AppendLine(@"	r.depth,");
            sb.AppendLine(@"	r.iadc_id,");
            sb.AppendLine(@"	i.iadc_code,");
            sb.AppendLine(@"	i.description as iadc_description,");
            sb.AppendLine(@"	i.type,");
            sb.AppendLine(@"	CASE i.type WHEN 1 THEN 'PT' WHEN 2 THEN 'NPT'  END AS type_desc,");
            sb.AppendLine(@"	i.category,");
            sb.AppendLine(@"	r.operation_start_date,");
            sb.AppendLine(@"	r.operation_end_date,");
            sb.AppendLine(@"	r.description,");
            sb.AppendLine(@"	wh.id AS well_hole_and_casing_id,");
            sb.AppendLine(@"	h.name hole_name,");
            sb.AppendLine(@"	wh.hole_depth,");
            sb.AppendLine(@"	c.name casing_name,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,");
            sb.AppendLine(@"  t.team_name  AS record_owning_team");
            sb.AppendLine(@"FROM dbo.drilling_operation_activity AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.drilling AS d");
            sb.AppendLine(@"  ON r.drilling_id = d.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.drilling_hole_and_casing AS dh");
            sb.AppendLine(@"  ON r.drilling_hole_and_casing_id = dh.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.well_casing AS wc");
            sb.AppendLine(@"    ON dh.well_hole_and_casing_id = wc.id"); 
            sb.AppendLine(@"LEFT OUTER JOIN dbo.well_hole AS wh");
            sb.AppendLine(@"    ON wc.well_hole_id = wh.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.hole AS h");
            sb.AppendLine(@"    ON wh.hole_id = h.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.casing AS c");
            sb.AppendLine(@"    ON c.hole_id = h.id and wc.casing_id = c.id ");
            //sb.AppendLine(@"LEFT OUTER JOIN dbo.well_hole_and_casing AS wh");
            //sb.AppendLine(@"  ON wh.id = dh.well_hole_and_casing_id");
            //sb.AppendLine(@"LEFT OUTER JOIN dbo.hole_and_casing AS h");
            //sb.AppendLine(@"  ON wh.hole_and_casing_id = h.id");

            sb.AppendLine(@"LEFT OUTER JOIN dbo.iadc AS i");
            sb.AppendLine(@"  ON r.iadc_id = i.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");
            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            DefaultViewColumns.Add("drilling_id", "r.drilling_id");
            DefaultViewColumns.Add("drilling_hole_and_casing_id", "r.drilling_hole_and_casing_id");

            DefaultViewColumns.Add("hole_val", "dh.hole_val");
            DefaultViewColumns.Add("casing_val", "dh.casing_val");
            DefaultViewColumns.Add("lot_fit", "dh.lot_fit");
            DefaultViewColumns.Add("lot_fit_val", "dh.lot_fit_val");
            DefaultViewColumns.Add("cement_val", "dh.cement_val");
            DefaultViewColumns.Add("seq", "dh.seq");
            DefaultViewColumns.Add("drilling_date", "d.drilling_date");

            DefaultViewColumns.Add("depth", "r.depth");
            DefaultViewColumns.Add("iadc_id", "r.iadc_id");
            DefaultViewColumns.Add("iadc_code", "i.iadc_code");
            DefaultViewColumns.Add("iadc_description", "i.description");
            DefaultViewColumns.Add("type", "i.type");
            DefaultViewColumns.Add("category", "i.category");
            DefaultViewColumns.Add("operation_start_date", "r.operation_start_date");
            DefaultViewColumns.Add("operation_end_date", "r.operation_end_date");
            DefaultViewColumns.Add("description", "r.description");

            DefaultViewColumns.Add("well_hole_and_casing_id", "wh.id");
            DefaultViewColumns.Add("hole_name", "h.hole_name");
            DefaultViewColumns.Add("hole_depth", "wh.hole_depth");
            DefaultViewColumns.Add("casing_name", "h.casing_name");




            #region Default View Base Record by
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");

            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("id", "ASC");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
