﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;

namespace Dreamwell.DataAccess.Models.Entity
{

    [Table("rkap")]
    public partial class rkap
    {
        [ExplicitKey]
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public bool? is_active { get; set; }
        public bool? is_locked { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public string organization_id { get; set; }
        public int status { get; set; }
        public string well_classification { get; set; }
        public string well_name { get; set; }
        public string business_unit_id { get; set; }
        public string region { get; set; }
        public string well_zona { get; set; }
        public string asset_id { get; set; }
        public string field_id { get; set; }
        public DateTime? spud_date { get; set; }
        public DateTime? end_date { get; set; }
        public decimal? planned_days { get; set; }
        public string rig_id { get; set; }
        public string hp_no { get; set; }
        //public string horse_power_unit { get; set; }
        public string water_depth { get; set; }
        //public string water_depth_unit { get; set; }
        public string budget { get; set; }
        public string budget_currency { get; set; }
        public string well_id { get; set; }
        public DateTime? realization_date { get; set; }
    }

    public partial class vw_rkap
    {
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public bool? is_active { get; set; }
        public bool? is_locked { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string owner_name { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public string organization_id { get; set; }
        public int status { get; set; }
        public string well_classification { get; set; }
        public string well_name { get; set; }
        public string business_unit_id { get; set; }
        public string unit_name { get; set; }
        public string region { get; set; }
        public string well_zona { get; set; }
        public string asset_id { get; set; }
        public string asset_code { get; set; }
        public string asset_name { get; set; }
        public string field_id { get; set; }
        public string field_name { get; set; }
        public DateTime spud_date { get; set; }
        public DateTime end_date { get; set; }
        public decimal? planned_days { get; set; }
        //public int planned_days { get; set; }
        public string rig_id { get; set; }
        public string rig_name { get; set; }
        public string hp_no { get; set; }
        //public string horse_power_unit { get; set; }
        //public string horse_power_unit_name { get; set; }
        public string water_depth { get; set; }
        //public string water_depth_unit { get; set; }
        //public string water_depth_unit_name { get; set; }
        public string budget { get; set; }
        public string budget_currency { get; set; }
        public string budget_currency_name { get; set; }
        public string well_id { get; set; }
        public string well_status { get; set; }
        public DateTime? realization_date { get; set; }
    }

    public partial class schedule_rkap
    {
        public string id { get; set; }
        public string title { get; set; }
        public DateTime? start { get; set; }
        public DateTime? end { get; set; }
        public bool allDay { get; set; }
        public int status { get; set; }
    }

    public partial class chart_rkap
    {
        public string unit_id { get; set; }
        public string unit_name { get; set; }
        public int type { get; set; }
        public int year { get; set; }
        public int status { get; set; }
        public int budget { get; set; }
        public int jumlah { get; set; }
        public int ongoing { get; set; }
        public int real { get; set; }
        public int total_co { get; set; }
        public int totalafe { get; set; }
        public object payload { get; set; }
    }

    public partial class rkap_chart_ds
    {
        public rkap_chart_ds(string name)
        {
            Name = name;
            Data = new List<int>();
        }
        public string Name { get; set; }
        public List<int> Data { get; set; }
    }

    public partial class rkap_timeline_resource
    {
        public string Id { get; set; }
        public string Area { get; set; }
        public string Well { get; set; }
        public string Jadwal { get; set; }
        public string Rig { get; set; }
        public string HpWd { get; set; }
        public string eventColor { get; set; }
    }

    public partial class rkap_timeline_event
    {
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string ResourceId { get; set; }
    }
    public partial class calendar_response
    {
        public List<rkap_timeline_resource> resource { get; set; }
        public List<rkap_timeline_event> events { get; set; }
    }
}