﻿using CommonTools;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public partial class tbl_pdf : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion

        static tbl_pdf()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  r.id,");
            sb.AppendLine(@"  r.name_file,");
            sb.AppendLine(@"  r.well_final_report_id,");
            sb.AppendLine(@"  r.well_id,");
            sb.AppendLine(@"  r.pdf_content_1,");
            sb.AppendLine(@"  r.pdf_content_2,");
            sb.AppendLine(@"  r.pdf_content_3,");
            sb.AppendLine(@"  r.pdf_content_4,");
            sb.AppendLine(@"  r.created_by,");
            sb.AppendLine(@"  r.created_on,");
            sb.AppendLine(@"  r.modified_by,");
            sb.AppendLine(@"  r.modified_on,");
            sb.AppendLine(@"  r.approved_by,");
            sb.AppendLine(@"  r.approved_on,");
            sb.AppendLine(@"  r.is_active,");
            sb.AppendLine(@"  r.is_locked,");
            sb.AppendLine(@"  r.is_default,");
            sb.AppendLine(@"  r.owner_id,");
            sb.AppendLine(@"  t.team_name  AS record_owning_team");
            sb.AppendLine(@"FROM dbo.tbl_pdf AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");
            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("name_file", "r.name_file");
            DefaultViewColumns.Add("well_final_report_id", "r.well_final_report_id");
            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("pdf_content", "r.pdf_content");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            #region Default View Base Record by
            DefaultViewColumns.Add("record_owning_team", "t.team_name");
            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("name_file", "ASC");  // Mengurutkan berdasarkan nama
            #endregion
        }


        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
