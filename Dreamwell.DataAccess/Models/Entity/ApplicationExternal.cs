﻿using Dapper.Contrib.Extensions;
using System;

namespace Dreamwell.DataAccess.Models.Entity
{
    [Table("application_external")]
    public class ApplicationExternal
    {
        [ExplicitKey]
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public bool? is_active { get; set; }
        public string client_id { get; set; }
        public string pass_phrase { get; set; }
        public DateTime? last_login { get; set; }
    }
}
