﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Models.Entity
{
    [Table("well_casing")]
    public partial class WellCasing
    {
        [ExplicitKey]
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public bool? is_active { get; set; }
        public bool? is_locked { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public string organization_id { get; set; }
        public string well_hole_id { get; set; }
        public string casing_id { get; set; }
        public decimal? casing_setting_depth { get; set; }
        public string casing_type { get; set; }
        public decimal? casing_weight { get; set; }
        public string casing_grade { get; set; }
        public string casing_connection { get; set; }
        public string casing_top_of_liner { get; set; }
    }

    public partial class WellCasingResponse
    {
        public string casing_id { get; set; }
        public string casing_name { get; set; }
        public decimal? casing_setting_depth { get; set; }
        public string casing_type { get; set; }
        public decimal? casing_weight { get; set; }
        public string casing_grade { get; set; }
        public string casing_connection { get; set; }
        public string casing_top_of_liner { get; set; }
    }
}
