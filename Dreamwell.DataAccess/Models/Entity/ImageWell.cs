﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Models.Entity
{
    public class DetailViewModel
    {
        public vw_well_hole_and_casing HoleAndCasingData { get; set; }
        public List<vw_well_deviation> WellDeviationData { get; set; }
    }

}
