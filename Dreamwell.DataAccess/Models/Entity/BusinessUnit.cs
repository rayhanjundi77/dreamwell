﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Models.Entity
{
    [Table("business_unit")]
    public class BusinessUnit
    {
        [ExplicitKey]
        public string id { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string organization_id { get; set; }
        public string unit_name { get; set; }
        public string unit_address { get; set; }
        public string parent_unit { get; set; }
        public string unit_fax { get; set; }
        public string unit_email { get; set; }
        public bool? is_locked { get; set; }
        public string unit_code { get; set; }
        public string unit_description { get; set; }
        public string unit_phone { get; set; }
        public bool? is_active { get; set; }
        public string modified_by { get; set; }
        public string approved_by { get; set; }
        public DateTime? modified_on { get; set; }
        public string default_team { get; set; }
        public DateTime? created_on { get; set; }
        public string created_by { get; set; }
        public DateTime? approved_on { get; set; }
        public int? order_index { get; set; }
    }
}
