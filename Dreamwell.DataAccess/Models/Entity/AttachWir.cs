﻿using CommonTools;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public class AttachWirPayload
    {
        public string well_id { get; set; }
        public string wli_comments { get; set; }
        public List<attach_wir> attachments { get; set; }
    }
    public partial class attach_wir : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion


        static attach_wir()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT  ");
            sb.AppendLine(@"  r.id,  ");
            sb.AppendLine(@"  r.created_by,  ");
            sb.AppendLine(@"  r.created_on,  ");
            sb.AppendLine(@"  r.modified_by,  ");
            sb.AppendLine(@"  r.modified_on,  ");
            sb.AppendLine(@"  r.approved_by,  ");
            sb.AppendLine(@"  r.approved_on,  ");
            sb.AppendLine(@"  r.is_active,  ");
            sb.AppendLine(@"  r.is_locked,  ");
            sb.AppendLine(@"  r.is_default,  ");
            sb.AppendLine(@"  r.owner_id,  ");
            sb.AppendLine(@"  r.well_id,  ");
            sb.AppendLine(@"  r.attach_type,  ");
            sb.AppendLine(@"  r.file_name,  ");
            sb.AppendLine(@"  r.description,  ");
            sb.AppendLine(@"  r.size,  ");
            sb.AppendLine(@"  r.comment_wir,  ");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,  ");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,  ");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,  ");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,  ");
            sb.AppendLine(@"  t.team_name  AS record_owning_team  ");
            sb.AppendLine(@"FROM dbo.attach_wir AS r  ");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.well AS d  ");
            sb.AppendLine(@"  ON r.well_id = d.id  ");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0  ");
            sb.AppendLine(@"  ON r.created_by = u0.id  ");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1  ");
            sb.AppendLine(@"  ON r.modified_by = u1.id  ");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2  ");
            sb.AppendLine(@"  ON r.approved_by = u2.id  ");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3  ");
            sb.AppendLine(@"  ON r.owner_id = u3.id  ");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t  ");
            sb.AppendLine(@"  ON r.owner_id = t.id  ");

            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("attach_type", "r.attach_type");
            DefaultViewColumns.Add("file_name", "r.file_name");
            DefaultViewColumns.Add("description", "r.description");
            DefaultViewColumns.Add("size", "r.size");
            DefaultViewColumns.Add("comment_wir", "r.comment_wir");

            #region Default View Base Record by
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");

            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("description", "ASC");
            #endregion
        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
