﻿using CommonTools;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    //public partial class vw_wir
    //{
    //    public List<vw_wellwi> wellwi { get; set; }
    //    public List<vw_wir> wir { get; set; }
    //}

    public partial class wir : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion


        static wir()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  r.id,");
            sb.AppendLine(@"  r.created_by,");
            sb.AppendLine(@"  r.created_on,");
            sb.AppendLine(@"  r.modified_by,");
            sb.AppendLine(@"  r.modified_on,");
            sb.AppendLine(@"  r.is_active,");
            sb.AppendLine(@"  r.is_locked,");
            sb.AppendLine(@"  r.is_default,");
            sb.AppendLine(@"  r.owner_id,");
            sb.AppendLine(@"  r.approved_by,");
            sb.AppendLine(@"  r.approved_on,");
            sb.AppendLine(@"  r.wir_no,");
            sb.AppendLine(@"  r.originator,");
            sb.AppendLine(@"  r.issues_date,");
            sb.AppendLine(@"  r.field_id,");
            sb.AppendLine(@"  r.well_id,");
            sb.AppendLine(@"  r.well_color,");
            sb.AppendLine(@"  r.priority,");
            sb.AppendLine(@"  r.main_job,");
            sb.AppendLine(@"  r.budget_ass,");
            sb.AppendLine(@"  r.request_status,");
            sb.AppendLine(@"  r.type_operation,");
            sb.AppendLine(@"  r.present_status,");
            sb.AppendLine(@"  r.main_lines,");
            sb.AppendLine(@"  r.reservoir_information,");
            sb.AppendLine(@"  r.organization_id,");
            sb.AppendLine(@"  r.business_unit_id,");
            sb.AppendLine(@"  r.main_job_id,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N'')))) AS record_approved_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N'')))) AS record_owner");
            sb.AppendLine(@"FROM dbo.wir AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0 ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1 ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2 ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3 ON r.owner_id = u3.id");

            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("wir_no", "r.wir_no");
            DefaultViewColumns.Add("originator", "r.originator");
            DefaultViewColumns.Add("issues_date", "r.issues_date");
            DefaultViewColumns.Add("field_id", "r.field_id");
            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("well_color", "r.well_color");
            DefaultViewColumns.Add("priority", "r.priority");
            DefaultViewColumns.Add("main_job", "r.main_job");
            DefaultViewColumns.Add("budget_ass", "r.budget_ass");
            DefaultViewColumns.Add("request_status", "r.request_status");
            DefaultViewColumns.Add("type_operation", "r.type_operation");
            DefaultViewColumns.Add("present_status", "r.present_status");
            DefaultViewColumns.Add("main_lines", "r.main_lines");
            DefaultViewColumns.Add("reservoir_information", "r.reservoir_information");
            DefaultViewColumns.Add("organization_id", "r.organization_id");
            DefaultViewColumns.Add("business_unit_id", "r.business_unit_id");
            DefaultViewColumns.Add("main_job_id", "r.main_job_id");

            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("wir_no", "ASC");
            #endregion
        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
