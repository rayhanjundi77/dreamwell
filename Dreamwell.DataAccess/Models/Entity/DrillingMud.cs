﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class drilling_mud: IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion


        static drilling_mud()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"	r.id,");
            sb.AppendLine(@"	r.created_by,");
            sb.AppendLine(@"	r.created_on,");
            sb.AppendLine(@"	r.modified_by,");
            sb.AppendLine(@"	r.modified_on,");
            sb.AppendLine(@"	r.approved_by,");
            sb.AppendLine(@"	r.approved_on,");
            sb.AppendLine(@"	r.is_active,");
            sb.AppendLine(@"	r.is_locked,");
            sb.AppendLine(@"	r.is_default,");
            sb.AppendLine(@"	r.owner_id,");
            sb.AppendLine(@"	r.drilling_id,");
            sb.AppendLine(@"	r.data_type,");
            sb.AppendLine(@"	r.mud_type,");
            sb.AppendLine(@"	mt.mud_name,");
            sb.AppendLine(@"	r.mud_time,");
            sb.AppendLine(@"	r.mw_in,");
            sb.AppendLine(@"	r.mw_out,");
            sb.AppendLine(@"	r.temp_in,");
            sb.AppendLine(@"	r.temp_out,");
            sb.AppendLine(@"	r.pres_grad,");
            sb.AppendLine(@"	r.visc,");
            sb.AppendLine(@"	r.pv,");
            sb.AppendLine(@"	r.yp,");
            sb.AppendLine(@"	r.gels_10sec,");
            sb.AppendLine(@"	r.gels_10min,");
            sb.AppendLine(@"	r.fluid_loss,");
            sb.AppendLine(@"	r.ph,");
            sb.AppendLine(@"	r.cake,");
            sb.AppendLine(@"	r.solids,");
            sb.AppendLine(@"	r.sand,");
            sb.AppendLine(@"	r.water,");
            sb.AppendLine(@"	r.oil,");
            sb.AppendLine(@"	r.hgs,");
            sb.AppendLine(@"	r.lgs,");
            sb.AppendLine(@"	r.ltlp,");
            sb.AppendLine(@"	r.hthp,");
            sb.AppendLine(@"	r.estb,");
            sb.AppendLine(@"	r.pf,");
            sb.AppendLine(@"	r.mf,");
            sb.AppendLine(@"    r.chloride,");
            sb.AppendLine(@"	r.pm,");
            sb.AppendLine(@"	r.ecd,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,");
            sb.AppendLine(@"  t.team_name  AS record_owning_team");
            sb.AppendLine(@"FROM dbo.drilling_mud AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.mud_type AS mt");
            sb.AppendLine(@"  ON r.mud_type = mt.id");

            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion            
            
            DefaultViewColumns.Add("drilling_id", "r.drilling_id");
            DefaultViewColumns.Add("data_type", "r.data_type");
            DefaultViewColumns.Add("mud_type", "r.mud_type");
            DefaultViewColumns.Add("mud_name", "mt.mud_name");
            DefaultViewColumns.Add("mud_time", "r.mud_time");
            DefaultViewColumns.Add("mw_in", "r.mw_in");
            DefaultViewColumns.Add("mw_out", "r.mw_out");
            DefaultViewColumns.Add("temp_in", "r.temp_in");
            DefaultViewColumns.Add("temp_out", "r.temp_out");
            DefaultViewColumns.Add("pres_grad", "r.pres_grad");
            DefaultViewColumns.Add("chloride", "r.chloride");
            DefaultViewColumns.Add("visc", "r.visc");
            DefaultViewColumns.Add("pv", "r.pv");
            DefaultViewColumns.Add("yp", "r.yp");
            DefaultViewColumns.Add("gels_10sec", "r.gels_10sec");
            DefaultViewColumns.Add("gels_10min", "r.gels_10min");
            DefaultViewColumns.Add("fluid_loss", "r.fluid_loss");
            DefaultViewColumns.Add("ph", "r.ph");
            DefaultViewColumns.Add("cake", "r.cake");
            DefaultViewColumns.Add("solids", "r.solids");
            DefaultViewColumns.Add("sand", "r.sand");
            DefaultViewColumns.Add("water", "r.water");
            DefaultViewColumns.Add("oil", "r.oil");
            DefaultViewColumns.Add("hgs", "r.hgs");
            DefaultViewColumns.Add("lgs", "r.lgs");
            DefaultViewColumns.Add("ltlp", "r.ltlp");
            DefaultViewColumns.Add("hthp", "r.hthp");
            DefaultViewColumns.Add("estb", "r.estb");
            DefaultViewColumns.Add("pf", "r.pf");
            DefaultViewColumns.Add("mf", "r.mf");
            DefaultViewColumns.Add("pm", "r.pm");
            DefaultViewColumns.Add("ecd", "r.ecd");

            #region Default View Base Record by
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");

            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("created_on", "ASC");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
