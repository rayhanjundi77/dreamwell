﻿using CommonTools;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Models.Entity
{
    [Table("hole")]
    public partial class Hole 
    {
        [ExplicitKey]
        public string id { get; set; }
        public string created_by { get; set; }
        public DateTime? created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? modified_on { get; set; }
        public bool? is_active { get; set; }
        public bool? is_locked { get; set; }
        public bool? is_default { get; set; }
        public string owner_id { get; set; }
        public string approved_by { get; set; }
        public DateTime? approved_on { get; set; }
        public string organization_id { get; set; }
        public string name { get; set; }
        public bool? hole_type { get; set; }
    }
}
