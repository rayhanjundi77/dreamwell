﻿using CommonTools;
using PetaPoco;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public partial class vw_well
    {
        [ResultColumn]
        public decimal daily_mud_cost { get; set; }
        [ResultColumn]
        public decimal cummulative_mud_cost { get; set; }
        [ResultColumn]
        public decimal actual_depth { get; set; }
        [ResultColumn]
        public decimal actual_days { get; set; }
        [ResultColumn]
        public decimal total_cost { get; set; }
        [ResultColumn]
        public decimal cummulative_cost_feet { get; set; }
        [ResultColumn]
        public decimal cummulative_cost_meter { get; set; }
        [ResultColumn]
        public decimal pt_hours { get; set; }
        [ResultColumn]
        public decimal npt_hours { get; set; }
        [ResultColumn]
        public decimal rop { get; set; }
        [ResultColumn]
        public decimal wob { get; set; }
        [ResultColumn]
        public decimal planned_md { get; set; }
        [ResultColumn]
        public decimal actual_md { get; set; }
        [ResultColumn]
        public decimal planned_cost { get; set; }
        [ResultColumn]
        public decimal actual_cost { get; set; }

    }

    public partial class well : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion


        static well()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  r.id,");
            sb.AppendLine(@"  r.created_by,");
            sb.AppendLine(@"  r.created_on,");
            sb.AppendLine(@"  r.modified_by,");
            sb.AppendLine(@"  r.modified_on,");
            sb.AppendLine(@"  r.approved_by,");
            sb.AppendLine(@"  r.approved_on,");
            sb.AppendLine(@"  r.submitted_by,");
            sb.AppendLine(@"  r.submitted_on,");
            sb.AppendLine(@"  r.closed_by,");
            sb.AppendLine(@"  r.closed_on,");
            sb.AppendLine(@"  r.is_active,");
            sb.AppendLine(@"  r.is_locked,");
            sb.AppendLine(@"  r.is_default,");
            sb.AppendLine(@"  r.owner_id,");
            sb.AppendLine(@"  r.organization_id,");
            sb.AppendLine(@"  o1.organization_name AS organization_name,");
            sb.AppendLine(@"  r.business_unit_id,");
            sb.AppendLine(@"  b.unit_name,");
            sb.AppendLine(@"  b.parent_unit,");
            sb.AppendLine(@"  r.field_id,");
            sb.AppendLine(@"  f.field_name,");
            sb.AppendLine(@"  r.well_name,");
            sb.AppendLine(@"  r.is_std_international,");
            sb.AppendLine(@"  CASE  ");
            sb.AppendLine(@"	WHEN  r.is_std_international IS NULL THEN 'MIXED'");
            sb.AppendLine(@"	WHEN  r.is_std_international = 1 THEN 'SI'");
            sb.AppendLine(@"	WHEN  r.is_std_international = 0 THEN 'API'");
            sb.AppendLine(@"  END AS metric_unit,");
            sb.AppendLine(@"");
            sb.AppendLine(@"    r.spud_date,");
            sb.AppendLine(@"    r.well_type,");
            sb.AppendLine(@"	r.well_status,");
            sb.AppendLine(@"	r.parent_well,");
            sb.AppendLine(@"    r.latitude,");
            sb.AppendLine(@"    r.longitude,");
            sb.AppendLine(@"    r.plan_depth,");
            sb.AppendLine(@"    r.well_operator,");
            sb.AppendLine(@"    r.well_head_elevation,");
            sb.AppendLine(@"    r.datum_elevation,");
            sb.AppendLine(@"    r.air_gap,");
            sb.AppendLine(@"    r.ground_elevation,");
            sb.AppendLine(@"    r.drilling_contractor_id,");
            sb.AppendLine(@"	dc.contractor_name,");
            sb.AppendLine(@"    r.rig_type,");
            sb.AppendLine(@"    r.rig_id,");
            sb.AppendLine(@"    r.rig_rating,");
            sb.AppendLine(@"    r.water_depth,");
            sb.AppendLine(@"    r.rkb_elevation,");
            sb.AppendLine(@"    r.rt_to_seabed,");
            sb.AppendLine(@"    r.rig_heading,");
            sb.AppendLine(@"    r.rig_heading_uom_id,");
            sb.AppendLine(@"    r.afe_id,");
            sb.AppendLine(@"	rg.name as rig_name,");
            sb.AppendLine(@"	a.afe_no,");
            sb.AppendLine(@"    r.afe_cost,");
            sb.AppendLine(@"    r.afe_cost_currency_id,");
            sb.AppendLine(@"    r.daily_cost,");
            sb.AppendLine(@"    r.daily_cost_currency_id,");
            sb.AppendLine(@"    r.cummulative_cost,");
            sb.AppendLine(@"    r.cummulative_cost_currency_id,");
            sb.AppendLine(@"    r.planned_td,");
            sb.AppendLine(@"    r.planned_days,");
            sb.AppendLine(@"    r.environment,");
            sb.AppendLine(@"    r.mud_pump_horse_power,");
            sb.AppendLine(@"    r.mud_pump_linear_size,");
            sb.AppendLine(@"    r.mud_pump_stroke_length,");
            sb.AppendLine(@"    r.mud_pump_qty,");
            sb.AppendLine(@"    r.mud_pump_pump_type,");
            sb.AppendLine(@"    r.top_drive_rating,");
            sb.AppendLine(@"    r.block_rating,");
            sb.AppendLine(@"    r.traveling_assembly_weight,");
            sb.AppendLine(@"    r.aerated_equipment,");
            sb.AppendLine(@"    r.mud_cap,");
            sb.AppendLine(@"	r.lat_deg,");
            sb.AppendLine(@"    r.lat_min,");
            sb.AppendLine(@"    r.lat_sec,");
            sb.AppendLine(@"    r.lon_deg,");
            sb.AppendLine(@"    r.lon_min,");
            sb.AppendLine(@"    r.lon_sec,");
            sb.AppendLine(@"    r.bppd,");
            sb.AppendLine(@"    r.mm,");
            sb.AppendLine(@"    r.kick_of_point,");
            sb.AppendLine(@"    r.kop_depth,");
            sb.AppendLine(@"    r.well_classification,");
            sb.AppendLine(@"    r.detail_well_classification,");
            sb.AppendLine(@"    r.country_id,");
            sb.AppendLine(@"	c.country_name,");
            sb.AppendLine(@"    r.target_direction,");
            sb.AppendLine(@"    r.fluid_production,");
            sb.AppendLine(@"    r.offset_well,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N'')))) AS record_created_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N'')))) AS record_modified_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))  AS record_approved_by,");
            sb.AppendLine(@"  RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))  AS record_owner,");
            sb.AppendLine(@"  t.team_name  AS record_owning_team");
            sb.AppendLine(@"FROM dbo.well AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.business_unit AS b");
            sb.AppendLine(@"  ON r.business_unit_id = b.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.field AS f");
            sb.AppendLine(@"  ON r.field_id = f.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.country AS c");
            sb.AppendLine(@"  ON r.country_id = c.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.drilling_contractor AS dc");
            sb.AppendLine(@"  ON r.drilling_contractor_id = dc.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.afe AS a");
            sb.AppendLine(@"  ON r.afe_id = a.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.rig AS rg");
            sb.AppendLine(@"  ON r.rig_id = rg.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.organization AS o1");
            sb.AppendLine(@"  ON r.organization_id = o1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");


            DefaultView = sb.ToString();

            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("submitted_by", "r.submitted_by");
            DefaultViewColumns.Add("submitted_on", "r.submitted_on");
            DefaultViewColumns.Add("closed_by", "r.closed_by");
            DefaultViewColumns.Add("closed_on", "r.closed_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            #endregion

            DefaultViewColumns.Add("organization_id", "r.organization_id");
            DefaultViewColumns.Add("organization_name", "o1.organization_name");
            DefaultViewColumns.Add("business_unit_id", "r.business_unit_id");
            DefaultViewColumns.Add("unit_name", "b.unit_name");
            DefaultViewColumns.Add("field_id", "r.field_id");
            DefaultViewColumns.Add("field_name", "f.field_name");
            DefaultViewColumns.Add("asset_name", "ass.asset_name");
            DefaultViewColumns.Add("well_name", "r.well_name");
            DefaultViewColumns.Add("spud_date", "r.spud_date");
            DefaultViewColumns.Add("well_type", "r.well_type");
            DefaultViewColumns.Add("well_status", "r.well_status");
            DefaultViewColumns.Add("parent_well", "r.parent_well");
            DefaultViewColumns.Add("latitude", "r.latitude");
            DefaultViewColumns.Add("longitude", "r.longitude");
            DefaultViewColumns.Add("plan_depth", "r.plan_depth");
            DefaultViewColumns.Add("well_operator", "r.well_operator");
            DefaultViewColumns.Add("well_head_elevation", "r.well_head_elevation");
            DefaultViewColumns.Add("datum_elevation", "r.datum_elevation");
            DefaultViewColumns.Add("air_gap", "r.air_gap");
            DefaultViewColumns.Add("ground_elevation", "r.ground_elevation");
            DefaultViewColumns.Add("drilling_contractor_id", "r.drilling_contractor_id");
            DefaultViewColumns.Add("contractor_name", "dc.contractor_name");
            DefaultViewColumns.Add("rig_type", "r.rig_type");
            DefaultViewColumns.Add("rig_id", "r.rig_id");
            DefaultViewColumns.Add("rig_name", "rg.name");
            DefaultViewColumns.Add("rig_rating", "r.rig_rating");
            DefaultViewColumns.Add("water_depth", "r.water_depth");
            DefaultViewColumns.Add("rkb_elevation", "r.rkb_elevation");
            DefaultViewColumns.Add("rt_to_seabed", "r.rt_to_seabed");
            DefaultViewColumns.Add("rig_heading", "r.rig_heading");;
            DefaultViewColumns.Add("afe_id", "r.afe_id");
            DefaultViewColumns.Add("afe_no", "a.afe_no");
            DefaultViewColumns.Add("afe_cost", "r.afe_cost");
            DefaultViewColumns.Add("afe_cost_currency_id", "r.afe_cost_currency_id");
            DefaultViewColumns.Add("daily_cost", "r.daily_cost");
            DefaultViewColumns.Add("daily_cost_currency_id", "r.daily_cost_currency_id");
            DefaultViewColumns.Add("cummulative_cost", "r.cummulative_cost");
            DefaultViewColumns.Add("cummulative_cost_currency_id", "r.cummulative_cost_currency_id");
            DefaultViewColumns.Add("planned_td", "r.planned_td");
            DefaultViewColumns.Add("planned_days", "r.planned_days");
            DefaultViewColumns.Add("environment", "r.environment");
            DefaultViewColumns.Add("mud_pump_horse_power", "r.mud_pump_horse_power");
            DefaultViewColumns.Add("mud_pump_linear_size", "r.mud_pump_linear_size");
            DefaultViewColumns.Add("mud_pump_stroke_length", "r.mud_pump_stroke_length");
            DefaultViewColumns.Add("mud_pump_qty", "r.mud_pump_qty");
            DefaultViewColumns.Add("mud_pump_pump_type", "r.mud_pump_pump_type");
            DefaultViewColumns.Add("top_drive_rating", "r.top_drive_rating");
            DefaultViewColumns.Add("block_rating", "r.block_rating");
            DefaultViewColumns.Add("traveling_assembly_weight", "r.traveling_assembly_weight");
            DefaultViewColumns.Add("aerated_equipment", "r.aerated_equipment");
            DefaultViewColumns.Add("mud_cap", "r.mud_cap");
            DefaultViewColumns.Add("lat_deg", "r.lat_deg");
            DefaultViewColumns.Add("lat_min", "r.lat_min");
            DefaultViewColumns.Add("lat_sec", "r.lat_sec");
            DefaultViewColumns.Add("lon_deg", "r.lon_deg");
            DefaultViewColumns.Add("lon_min", "r.lon_min");
            DefaultViewColumns.Add("lon_sec", "r.lon_sec");
            DefaultViewColumns.Add("bppd", "r.bppd");
            DefaultViewColumns.Add("mm", "r.mm");
            DefaultViewColumns.Add("kick_of_point", "r.kick_of_point");
            DefaultViewColumns.Add("kop_depth", "r.kop_depth");
            DefaultViewColumns.Add("well_classification", "r.well_classification");
            DefaultViewColumns.Add("detail_well_classification", "r.detail_well_classification");
            DefaultViewColumns.Add("fluid_production", "r.fluid_production");
            DefaultViewColumns.Add("offset_well", "r.offset_well");
            DefaultViewColumns.Add("country_id", "r.country_id");
            DefaultViewColumns.Add("country_name", "c.country_name");
            DefaultViewColumns.Add("target_direction", "r.target_direction");

            #region Default View Base Record by
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");

            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("well_name", "ASC");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {

            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }




    }
}
