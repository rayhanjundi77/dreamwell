﻿using CommonTools;
using System.Collections.Generic;
using System.Globalization;

namespace Dreamwell.DataAccess
{
    public partial class well_mud_data : IEntity, IBaseRecord
    {
        #region Default IBaseRecord

        public static readonly string EntityId;

        public static readonly string EntityName;

        public static readonly string EntityDisplayName;

        public static readonly string DefaultView;

        public static readonly Dictionary<string, string> DefaultViewColumns;

        public static readonly Dictionary<string, string> DefaultViewColumnTitles;

        public static readonly Dictionary<string, string> DefaultViewOrders;

        #endregion

        static well_mud_data()
        {
            EntityName = GetTableName();
            EntityDisplayName = GetTableName(true);
            EntityId = GuidHash.ConvertToMd5HashGUID(EntityName).ToString();

            #region View Table
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  r.id,");
            sb.AppendLine(@"  r.created_by,");
            sb.AppendLine(@"  r.created_on,");
            sb.AppendLine(@"  r.modified_by,");
            sb.AppendLine(@"  r.modified_on,");
            sb.AppendLine(@"  r.approved_by,");
            sb.AppendLine(@"  r.approved_on,");
            sb.AppendLine(@"  r.is_active,");
            sb.AppendLine(@"  r.is_locked,");
            sb.AppendLine(@"  r.is_default,");
            sb.AppendLine(@"  r.owner_id,");
            sb.AppendLine(@"  r.organization_id,");
            sb.AppendLine(@"  r.well_id,");
            sb.AppendLine(@"  r.mud_type,");
            sb.AppendLine(@"  r.visc,");
            sb.AppendLine(@"  r.cake,");
            sb.AppendLine(@"  r.ltlp,");
            sb.AppendLine(@"  r.mud_time,");
            sb.AppendLine(@"  r.pv,");
            sb.AppendLine(@"  r.solids,");
            sb.AppendLine(@"  r.hthp,");
            sb.AppendLine(@"  r.mw_in,");
            sb.AppendLine(@"  r.yp,");
            sb.AppendLine(@"  r.sand,");
            sb.AppendLine(@"  r.estb,");
            sb.AppendLine(@"  r.mw_out,");
            sb.AppendLine(@"  r.gels_10sec,");
            sb.AppendLine(@"  r.gels_10min,");
            sb.AppendLine(@"  r.water,");
            sb.AppendLine(@"  r.pf,");
            sb.AppendLine(@"  r.temp_in,");
            sb.AppendLine(@"  r.temp_out,");
            sb.AppendLine(@"  r.fluid_loss,");
            sb.AppendLine(@"  r.hgs,");
            sb.AppendLine(@"  r.oil,");
            sb.AppendLine(@"  r.mf,");
            sb.AppendLine(@"  r.pm,");
            sb.AppendLine(@"  r.pres_grad,");
            sb.AppendLine(@"  r.ph,");
            sb.AppendLine(@"  r.lgs,");
            sb.AppendLine(@"  r.ecd,");
            sb.AppendLine(@"  r.chloride");
            sb.AppendLine(@"FROM dbo.well_mud_data AS r");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.organization AS o1");
            sb.AppendLine(@"  ON r.organization_id = o1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u0");
            sb.AppendLine(@"  ON r.created_by = u0.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u1");
            sb.AppendLine(@"  ON r.modified_by = u1.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u2");
            sb.AppendLine(@"  ON r.approved_by = u2.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.application_user AS u3");
            sb.AppendLine(@"  ON r.owner_id = u3.id");
            sb.AppendLine(@"LEFT OUTER JOIN dbo.team AS t");
            sb.AppendLine(@"  ON r.owner_id = t.id");
            DefaultView = sb.ToString();
            #endregion

            #region View Columns
            DefaultViewColumns = new Dictionary<string, string>();

            #region Default View Base Record
            DefaultViewColumns.Add("id", "r.id");
            DefaultViewColumns.Add("created_by", "r.created_by");
            DefaultViewColumns.Add("created_on", "r.created_on");
            DefaultViewColumns.Add("modified_by", "r.modified_by");
            DefaultViewColumns.Add("modified_on", "r.modified_on");
            DefaultViewColumns.Add("approved_by", "r.approved_by");
            DefaultViewColumns.Add("approved_on", "r.approved_on");
            DefaultViewColumns.Add("is_active", "r.is_active");
            DefaultViewColumns.Add("is_locked", "r.is_locked");
            DefaultViewColumns.Add("is_default", "r.is_default");
            DefaultViewColumns.Add("owner_id", "r.owner_id");
            DefaultViewColumns.Add("organization_id", "r.organization_id");
            DefaultViewColumns.Add("well_id", "r.well_id");
            DefaultViewColumns.Add("mud_type", "r.mud_type");
            DefaultViewColumns.Add("visc", "r.visc");
            DefaultViewColumns.Add("cake", "r.cake");
            DefaultViewColumns.Add("ltlp", "r.ltlp");
            DefaultViewColumns.Add("mud_time", "r.mud_time");
            DefaultViewColumns.Add("pv", "r.pv");
            DefaultViewColumns.Add("solids", "r.solids");
            DefaultViewColumns.Add("hthp", "r.hthp");
            DefaultViewColumns.Add("mw_in", "r.mw_in");
            DefaultViewColumns.Add("yp", "r.yp");
            DefaultViewColumns.Add("sand", "r.sand");
            DefaultViewColumns.Add("estb", "r.estb");
            DefaultViewColumns.Add("mw_out", "r.mw_out");
            DefaultViewColumns.Add("gels_10sec", "r.gels_10sec");
            DefaultViewColumns.Add("gels_10min", "r.gels_10min");
            DefaultViewColumns.Add("water", "r.water");
            DefaultViewColumns.Add("pf", "r.pf");
            DefaultViewColumns.Add("oil", "r.oil");
            DefaultViewColumns.Add("mf", "r.mf");
            DefaultViewColumns.Add("temp_in", "r.temp_in");
            DefaultViewColumns.Add("temp_out", "r.temp_out");
            DefaultViewColumns.Add("fluid_loss", "r.fluid_loss");
            DefaultViewColumns.Add("hgs", "r.hgs");
            DefaultViewColumns.Add("pm", "r.pm");
            DefaultViewColumns.Add("pres_grad", "r.pres_grad");
            DefaultViewColumns.Add("ph", "r.ph");
            DefaultViewColumns.Add("lgs", "r.lgs");
            DefaultViewColumns.Add("ecd", "r.ecd");
            DefaultViewColumns.Add("chloride", "r.chloride");
            DefaultViewColumns.Add("record_created_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u0.last_name), N''), ' ', ISNULL(u0.first_name, N''))))");
            DefaultViewColumns.Add("record_modified_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u1.last_name), N''), ' ', ISNULL(u1.first_name, N''))))");
            DefaultViewColumns.Add("record_approved_by", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u2.last_name), N''), ' ', ISNULL(u2.first_name, N''))))");
            DefaultViewColumns.Add("record_owner", "RTRIM(LTRIM(CONCAT(ISNULL(UPPER(u3.last_name), N''), ' ', ISNULL(u3.first_name, N''))))");
            DefaultViewColumns.Add("record_owning_team", "t.team_name");
            #endregion

            #endregion

            #region Titles
            DefaultViewColumnTitles = new Dictionary<string, string>();
            TextInfo ti = new CultureInfo("en-US", false).TextInfo;

            foreach (var c in DefaultViewColumns)
            {
                DefaultViewColumnTitles.Add(c.Key, ti.ToTitleCase(c.Key.Replace("_", " ")));
            }
            #endregion

            #region Order By
            DefaultViewOrders = new Dictionary<string, string>();
            DefaultViewOrders.Add("created_on", "DESC");
            #endregion

        }

        public string GetEntityId()
        {
            return EntityId;
        }

        public string GetEntityName()
        {
            return EntityName;
        }

        public string GetEntityDisplayName()
        {
            return EntityDisplayName;
        }

        public string GetDefaultView()
        {
            return DefaultView;
        }

        public Dictionary<string, string> GetDefaultViewColumns()
        {
            return DefaultViewColumns;
        }

        public Dictionary<string, string> GetDefaultViewColumnTitles()
        {
            return DefaultViewColumnTitles;
        }

        public Dictionary<string, string> GetDefaultViewOrders()
        {
            return DefaultViewOrders;
        }
    }
}
