﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Dreamwell.DataAccess
{
    public interface IUserContext
    {
        DataContext dataContext { get;}
        dreamwellRepo db { get; }
    }
    public class UserContext : IUserContext
    {
        private readonly DataContext _userDataContext;
        public UserContext(DataContext userDataContext)
        {
            _userDataContext = userDataContext;
        }

        public DataContext dataContext
        {
            get
            {
                return _userDataContext;
            }
        }

        public dreamwellRepo db
        {
            get
            {
                return _userDataContext.GetInstance;
            }
        }
    }
}
