﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public interface IBaseRecord
    {
        string id { get; set; }

        string created_by { get; set; }

        DateTime? created_on { get; set; }

        string modified_by { get; set; }

        DateTime? modified_on { get; set; }

        string approved_by { get; set; }

        DateTime? approved_on { get; set; }

        bool? is_active { get; set; }

        bool? is_locked { get; set; }

        bool? is_default { get; set; }

        string owner_id { get; set; }


    }

    public interface IBaseRecordSubmit
    {
        string submitted_by { get; set; }

        DateTime? submitted_on { get; set; }

    }
}
