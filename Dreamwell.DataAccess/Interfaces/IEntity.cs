﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{

    public interface IEntity
    {
        string GetEntityId();

        string GetEntityName();

        string GetEntityDisplayName();

        string GetDefaultView();

        Dictionary<string, string> GetDefaultViewColumns();

        Dictionary<string, string> GetDefaultViewColumnTitles();

        Dictionary<string, string> GetDefaultViewOrders();

    }

}
