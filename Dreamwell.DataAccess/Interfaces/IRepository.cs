﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public interface IRepository<TEntity, TEntityView>
    {
        TEntity GetById(string id);
        TEntity GetFirstOrDefault(string sqlFilter, params object[] args);
        TEntity GetFirstOrDefault(Sql sqlFilter);

        TEntityView GetViewById(string id);
        TEntityView GetViewFirstOrDefault(string sqlFilter, params object[] args);
        TEntityView GetViewFirstOrDefault(Sql sqlFilter);

        #region Table

        List<TEntity> GetAll();
        List<TEntity> GetAll(Sql sqlFilter);
        List<TEntity> GetAll(string sqlFilter, params object[] args);
        Page<TEntity> GetAll(long page, long itemsPerPage);
        Page<TEntity> GetAll(long page, long itemsPerPage, string sqlFilter, params object[] args);
        Page<TEntity> GetAll(long page, long itemsPerPage, Sql sqlFilter);

        #endregion

        #region View
        List<TEntityView> GetViewAll();

        List<TEntityView> GetViewAll(Sql sqlFilter);
        List<TEntityView> GetViewAll(string sqlFilter, params object[] args);
        Page<TEntityView> GetViewPerPage(long page, long itemsPerPage);
        Page<TEntityView> GetViewPerPage(long page, long itemsPerPage, string sqlFilter, params object[] args);
        Page<TEntityView> GetViewAll(long page, long itemsPerPage, Sql sqlFilter);

        #endregion
        //bool SaveEntity(TEntity record, ref bool isNew);
        //bool SaveEntity(TEntity record, ref bool isNew, Action<TEntity> actionContinuation);


        bool RemoveAll(params string[] ids);

        bool Remove(string id);
    }

    public interface IRepositoryViewModel<TEntityViewModel>
    {
        TEntityViewModel GetViewFirstOrDefault(string sqlFilter, params object[] args);
        TEntityViewModel GetViewFirstOrDefault(Sql sqlFilter);

        List<TEntityViewModel> GetViewAll();

        List<TEntityViewModel> GetViewAll(Sql sqlFilter);
        List<TEntityViewModel> GetViewAll(string sqlFilter, params object[] args);
        Page<TEntityViewModel> GetViewPerPage(long page, long itemsPerPage);
        Page<TEntityViewModel> GetViewPerPage(long page, long itemsPerPage, string sqlFilter, params object[] args);
        Page<TEntityViewModel> GetViewAll(long page, long itemsPerPage, Sql sqlFilter);


    }
}
