﻿using Newtonsoft.Json;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class DataContext
    {
        public bool DeleteEntity<T>(string Id)
        {
            var success = false;
            string log = "";
            var appEntity = Activator.CreateInstance<T>();

            try
            {
                if (String.IsNullOrEmpty(Id) == true)
                {
                    appLogger.Error("Record Id is empty");
                    return success;
                }

                if (typeof(IEntity).IsAssignableFrom(typeof(T)))
                {
                    if (typeof(IBaseRecord).IsAssignableFrom(typeof(T)))
                    {

                        var qry = ((IEntity)appEntity).GetDefaultView();
                        qry += " WHERE r.id = @0 ";


                        using (var db = dreamwellRepo.GetInstance())
                        {
                            try
                            {
                                var oldRecord = db.FirstOrDefault<T>(qry, Id);
                                appLogger.Debug(db.LastCommand);

                                if (oldRecord != null)
                                {
                                    if (!IsSystemAdministrator)
                                    {
                                        #region Check role access

                                        var sql = Sql.Builder.Append("SELECT dbo.ufn_can_delete( @0, @1, @2, @3 )",
                                            AppUserId, ((IEntity)appEntity).GetEntityId(), ((IBaseRecord)oldRecord).id, ((IBaseRecord)oldRecord).owner_id);
                                        var hasAccess = db.ExecuteScalar<int>(sql);
                                        if (hasAccess <= 0) throw new Exception("User doesn't have permission to delete this record");

                                        #endregion

                                        if ((((IBaseRecord)oldRecord).is_default ?? false) == true)
                                        {
                                            log = $"Default record {Id} can not be deleted ";
                                            appLogger.Debug(log);
                                            throw new Exception(log);
                                        }
                                        else if ((((IBaseRecord)oldRecord).is_locked ?? false) == true)
                                        {
                                            log = $"Locked record {Id} is not editable";
                                            appLogger.Debug(log);
                                            throw new Exception(log);
                                        }

                                    }


                                    Guid audit_trail_id = Guid.NewGuid();
                                    using (var scope = db.GetTransaction())
                                    {
                                        try
                                        {
                                            db.Delete<T>(" WHERE id=@0 ", ((IBaseRecord)oldRecord).id);
                                            db.Insert(new audit_trail()
                                            {
                                                id = audit_trail_id.ToString(),
                                                created_by = AppUserId,
                                                created_on = DateTime.Now,
                                                owner_id = AppUserId,
                                                is_active = true,
                                                organization_id = OrganizationId,
                                                organization_name = OrganizationName,
                                                application_user_id = AppUserId,
                                                app_username = AppUsername,
                                                app_fullname = AppFullname,
                                                application_entity_id = ((IEntity)appEntity).GetEntityId(),
                                                user_action = DataAccessControl.ACTION_DELETE,
                                                record_id = ((IBaseRecord)oldRecord).id,
                                                new_record = JsonConvert.SerializeObject(oldRecord)
                                            });

                                            scope.Complete();
                                            success = true;
                                            log = string.Format("Record {0} with Id = {1} is deleted",
                                                ((IEntity)appEntity).GetEntityName(), Id);
                                            appLogger.Debug(log);
                                        }
                                        catch (Exception ex)
                                        {
                                            appLogger.Error(ex.ToString());
                                            appLogger.Debug(db.LastCommand);
                                            throw;
                                        }

                                    }
                                }
                                else
                                {
                                    log = string.Format("Record {0} with Id = {1} does not exist",
                                        ((IEntity)appEntity).GetEntityName(), Id);
                                    appLogger.Error(log);
                                    appLogger.Debug(db.LastCommand);
                                }
                            }
                            catch (Exception ex)
                            {
                                appLogger.Error(ex.ToString());
                                appLogger.Debug(db.LastCommand);
                                throw;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Record is not IBaseRecord");
                    }
                }
                else
                {
                    throw new Exception("Record is not IEntity");
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex.ToString());
                throw;
            }

            return success;
        }
    }
}
