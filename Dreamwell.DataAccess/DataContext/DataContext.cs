﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using PetaPoco;

namespace Dreamwell.DataAccess
{
    public partial class DataContext
    {
        private static readonly Logger appLogger = LogManager.GetCurrentClassLogger();

        #region User Information
        public String AppUserId { get; private set; }
        public String AppUsername { get; private set; }
        public String AppFullname { get; private set; }

        public String OrganizationId { get; private set; }
        public String OrganizationName { get; private set; }
        public bool IsDefaultUser { get; private set; }
        public bool IsSystemAdministrator { get; private set; }

        public String PrimaryBusinessUnitId { get; private set; }
        public String PrimaryBusinessUnit { get; private set; }

        public String PrimaryTeamId { get; private set; }
        public String PrimaryTeam { get; private set; }

        public dreamwellRepo GetInstance { get; private set; }
      

        #endregion


        public DataContext(String applicationUserId)
        {
            GetInstance = dreamwellRepo.GetInstance();
            using (var db = GetInstance)
            {
                try
                {
                    var qry = Sql.Builder.Append(application_user.DefaultView);
                    qry.Append("WHERE r.id=@0 ", applicationUserId);
                    var user = db.FirstOrDefault<vw_application_user>(qry);

                    if (user == null)
                        throw new Exception("User not found");
                    if (!(user.is_active ?? false))
                        throw new Exception("This user has disabled or not active, Please contact administrator");

                    AppUserId = user.id;
                    AppUsername = user.app_username;
                    //if (string.IsNullOrEmpty(user.last_name))
                    //    AppFullname = string.Format("{0}", user.first_name).Trim();
                    //else
                    //    AppFullname = string.Format("{0} {1}", user.last_name.ToUpper(), user.first_name).Trim();

                    AppFullname = user.app_fullname;
                    OrganizationId = user.organization_id;
                    OrganizationName = user.organization_name;
                    IsSystemAdministrator = user.is_sysadmin ?? false;

                    PrimaryBusinessUnitId = user.business_unit_id;
                    PrimaryBusinessUnit = user.unit_name ;

                    PrimaryTeamId = user.primary_team_id;
                    PrimaryTeam = user.team_name;

                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    throw;
                }
            }
        }
    }
}
