﻿using Newtonsoft.Json;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public partial class DataContext
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="record"> record is application entity</param>
        /// <param name="isNew"></param>
        /// <param name="ownershipId"> Ownership Id is optional, if you want set to specified owner_id to team or personal. 
        /// owner_id is default set to personal
        /// </param>
        public bool SaveEntity<T>(T record, bool isNew, string ownershipId = "")
        {
            var result = false;
            string log = string.Empty;

            try
            {
                var appEntity = Activator.CreateInstance<T>();
                if (!typeof(IEntity).IsAssignableFrom(typeof(T)))
                    throw new Exception("Record is not IEntity");

                if (!typeof(IBaseRecord).IsAssignableFrom(typeof(T)))
                    throw new Exception("Record is not IBaseRecord");

                using (var db = dreamwellRepo.GetInstance())
                {
                    try
                    {
                        if (isNew)
                        {
                            #region Insert
                            log = "Insert record ...";
                            appLogger.Debug(log);

                            if (string.IsNullOrEmpty(((IBaseRecord)record).id)
                                || string.IsNullOrWhiteSpace(((IBaseRecord)record).id))
                                ((IBaseRecord)record).id = Guid.NewGuid().ToString();

                            if (((IBaseRecord)record).is_active == null) ((IBaseRecord)record).is_active = true;
                            if (((IBaseRecord)record).created_by == null) ((IBaseRecord)record).created_by = AppUserId;
                            if (((IBaseRecord)record).created_on == null) ((IBaseRecord)record).created_on = DateTime.Now;

                            ((IBaseRecord)record).modified_by = null;
                            ((IBaseRecord)record).modified_on = null;

                            if (!string.IsNullOrEmpty(ownershipId))
                            {
                                ((IBaseRecord)record).owner_id = ownershipId;
                            }
                            else
                            {
                                ((IBaseRecord)record).owner_id = AppUserId;
                            }
                            



                            #region Check role access

                            // var entityId = ((IEntity)record).GetEntityId();
                            // var sql = Sql.Builder.Append("SELECT dbo.ufn_can_create( @0, @1 )",
                            //     AppUserId, entityId);
                            // var hasAccess = db.ExecuteScalar<int>(sql);
                            // if (hasAccess <= 0) throw new Exception("User doesn't have permission to create this entity");

                            #endregion

                            Guid audit_trail_id = Guid.NewGuid();
                            using (var scope = db.GetTransaction())
                            {
                                try
                                {
                                    db.Insert(record);
                                    db.Insert(new audit_trail()
                                    {
                                        id = audit_trail_id.ToString(),
                                        created_by = AppUserId,
                                        created_on = DateTime.Now,
                                        owner_id = AppUserId,
                                        is_active = true,
                                        organization_id = OrganizationId,
                                        organization_name = OrganizationName,
                                        application_user_id = AppUserId,
                                        app_username = AppUsername,
                                        app_fullname = AppFullname,
                                        application_entity_id = ((IEntity)appEntity).GetEntityId(),
                                        user_action = DataAccessControl.ACTION_CREATE,
                                        record_id = ((IBaseRecord)record).id,
                                        new_record = JsonConvert.SerializeObject(record)
                                    });

                                    scope.Complete();
                                    result = true;
                                }
                                catch (Exception ex)
                                {
                                    appLogger.Error(ex);
                                    appLogger.Debug(db.LastCommand);
                                    throw;
                                }
                            }

                            log = String.Format("Insert record = {0}", result);
                            appLogger.Debug(log);

                            #endregion
                        }
                        else
                        {
                            #region Update
                            log = "Update record ...";
                            appLogger.Debug(log);

                            var qry = ((IEntity)appEntity).GetDefaultView();
                            qry += " WHERE r.id = @0 ";


                            var oldRecord = db.FirstOrDefault<T>(qry, ((IBaseRecord)record).id);
                            appLogger.Debug(db.LastCommand);

                            if (oldRecord == null)
                            {
                                log = String.Format("Old record {0} with Id = {1} does not exists",
                                        typeof(T), ((IBaseRecord)appEntity).id);
                                throw new Exception(log);
                            }

                            #region Check role access

                            // var entityId = ((IEntity)record).GetEntityId();
                            // var sql = Sql.Builder.Append("SELECT dbo.ufn_can_update( @0, @1, @2, @3 )",
                            //     AppUserId, entityId, ((IBaseRecord)oldRecord).id, ((IBaseRecord)oldRecord).owner_id);
                            // var hasAccess = db.ExecuteScalar<int>(sql);
                            // if (hasAccess <= 0) throw new Exception("User doesn't have permission to update this entity");

                            #endregion

                            qry = ((IEntity)appEntity).GetDefaultView();
                            qry += " WHERE r.id = @0 ";

                            dynamic oldView = db.FirstOrDefault<dynamic>(qry, ((IBaseRecord)record).id);
                            appLogger.Debug(db.LastCommand);

                            log = String.Format("Old view = {0}", JsonConvert.SerializeObject(oldView));
                            appLogger.Debug(log);

                            /*enable modified is active, please remove this*/
                            /*((IBaseRecord)record).is_active = ((IBaseRecord)oldRecord).is_active;*/

                            ((IBaseRecord)record).created_by = ((IBaseRecord)oldRecord).created_by;
                            ((IBaseRecord)record).created_on = ((IBaseRecord)oldRecord).created_on;


                            ((IBaseRecord)record).modified_by = AppUserId;
                            ((IBaseRecord)record).modified_on = DateTime.Now;
                            //((IBaseRecord)Record).owner_id = ((IBaseRecord)oldRecord).owner_id;

                            var audit_trail_id = Guid.NewGuid();
                            using (var scope = db.GetTransaction())
                            {
                                try
                                {
                                    db.Update(record);
                                    db.Insert(new audit_trail()
                                    {
                                        id = audit_trail_id.ToString(),
                                        created_by = AppUserId,
                                        created_on = DateTime.Now,
                                        owner_id = AppUserId,
                                        is_active = true,
                                        organization_id = OrganizationId,
                                        organization_name = OrganizationName,
                                        application_user_id = AppUserId,
                                        app_username = AppUsername,
                                        app_fullname = AppFullname,
                                        application_entity_id = ((IEntity)appEntity).GetEntityId(),
                                        user_action = DataAccessControl.ACTION_UPDATE,
                                        record_id = ((IBaseRecord)record).id,
                                        new_record = JsonConvert.SerializeObject(record),
                                        old_record = JsonConvert.SerializeObject(oldRecord),
                                        old_view = JsonConvert.SerializeObject(oldView)
                                    });

                                    scope.Complete();
                                    result = true;
                                }
                                catch (Exception ex)
                                {
                                    appLogger.Error(ex);
                                    appLogger.Debug(db.LastCommand);
                                    throw;
                                }
                            }

                            log = String.Format("Update record = {0}", result);
                            appLogger.Debug(log);

                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                        appLogger.Debug(db.LastCommand);
                        throw;
                    }
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }


    }
}
