﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ExcelTableMapRepository : ServiceRespository<excel_table_map,vw_excel_table_map>
    {
        public ExcelTableMapRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
