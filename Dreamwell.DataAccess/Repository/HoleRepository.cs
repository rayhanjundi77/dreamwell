﻿using Dreamwell.DataAccess.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class HoleRepository : BaseRepository<Hole>
    {
        public HoleRepository(DataContext dataContext) : base(dataContext) { }
    }
}
