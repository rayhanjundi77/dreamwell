﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class AfePlanRepository : ServiceRespository<afe_plan, vw_afe_plan>
    {
        public AfePlanRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
