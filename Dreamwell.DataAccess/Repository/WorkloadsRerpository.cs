﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class WorkloadsRepository : ServiceRespository<workload_wir, vw_workload_wir>
    {
        public WorkloadsRepository(DataContext dataContext)
            : base(dataContext)
        {

        }
    }
}
