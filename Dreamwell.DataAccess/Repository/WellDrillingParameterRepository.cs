﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class WellDrillingParameterRepository : ServiceRespository<well_drilling_parameter, vw_well_drilling_parameter>
    {
        public WellDrillingParameterRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
