﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class CasingConnectionRepository : ServiceRespository<casing_connection, vw_casing_connection>
    {
        public CasingConnectionRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
