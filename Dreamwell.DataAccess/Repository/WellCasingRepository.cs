﻿using Dapper;
using Dreamwell.DataAccess.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class WellCasingRepository : BaseRepository<WellCasing>
    {
        public WellCasingRepository(DataContext dataContext) : base(dataContext) { }

        public void Clear(string id)
        {
            con.Execute($"Delete from well_casing where well_hole_id = '{id}'");
        }
    }

}
