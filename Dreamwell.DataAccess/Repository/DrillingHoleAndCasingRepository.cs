﻿namespace Dreamwell.DataAccess
{
    public class DrillingHoleAndCasingRepository : ServiceRespository<drilling_hole_and_casing, vw_drilling_hole_and_casing>
    {
        public DrillingHoleAndCasingRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
