﻿namespace Dreamwell.DataAccess
{
    public class DrillingContractorRepository : ServiceRespository<drilling_contractor, vw_drilling_contractor>
    {
        public DrillingContractorRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
