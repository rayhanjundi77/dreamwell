﻿namespace Dreamwell.DataAccess
{
    public class WellBhaComponentRepository : ServiceRespository<well_bha_component, vw_well_bha_component>
    {
        public WellBhaComponentRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
