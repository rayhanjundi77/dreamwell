﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ExcelTemplateRepository : ServiceRespository<excel_template,vw_excel_template>
    {
        public ExcelTemplateRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
