﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class CasingCatalogRepository : ServiceRespository<casing_catalog, vw_casing_catalog>
    {
        public CasingCatalogRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
