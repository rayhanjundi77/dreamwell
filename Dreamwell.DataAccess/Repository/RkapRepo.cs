﻿using Dreamwell.DataAccess.Models.Entity;

namespace Dreamwell.DataAccess
{
    public class RkapRepo : ServiceRespository<rkap, vw_rkap>
    {
        public RkapRepo(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}