﻿namespace Dreamwell.DataAccess
{
    public class WellProfileProgramRepository:ServiceRespository<well_profile_program, vw_well_profile_program>
    {
        public WellProfileProgramRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
