﻿namespace Dreamwell.DataAccess
{
    public class ImageRepository : ServiceRespository<tbl_image, vw_tbl_image>
    {
        public ImageRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
