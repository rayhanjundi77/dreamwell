﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ExcelDataValidationRepository : ServiceRespository<excel_data_validation, vw_excel_data_validation>
    {
        public ExcelDataValidationRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
