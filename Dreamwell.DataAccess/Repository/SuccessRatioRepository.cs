﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class SuccessRepository : ServiceRespository<status_ratio, vw_status_ratio>
    {
        public SuccessRepository(DataContext dataContext)
            : base(dataContext)
        {

        }
    }
}
