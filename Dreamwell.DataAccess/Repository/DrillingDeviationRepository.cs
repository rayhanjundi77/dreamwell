﻿namespace Dreamwell.DataAccess
{
    public class DrillingDeviationRepository:ServiceRespository<drilling_deviation, vw_drilling_deviation>
    {
        public DrillingDeviationRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
