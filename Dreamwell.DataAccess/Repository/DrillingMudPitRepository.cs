﻿namespace Dreamwell.DataAccess
{
    public class DrillingMudPitRepository : ServiceRespository<drilling_mud_pit, vw_drilling_mud_pit>
    {
        public DrillingMudPitRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
