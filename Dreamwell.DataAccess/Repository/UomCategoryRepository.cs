﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class UomCategoryRepository : ServiceRespository<uom_category, vw_uom_category>
    {
        public UomCategoryRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
