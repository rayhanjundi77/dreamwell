﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class WellItemRepository : ServiceRespository<well_item, vw_well_item>
    {
        public WellItemRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
