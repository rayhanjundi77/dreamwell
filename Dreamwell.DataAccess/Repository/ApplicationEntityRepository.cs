﻿
namespace Dreamwell.DataAccess
{
    public class ApplicationEntityRepository:ServiceRespository<application_entity, vw_application_entity>
    {
        public ApplicationEntityRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
