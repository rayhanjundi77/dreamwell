﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ApplicationMenuRepository : ServiceRespository<application_menu, vw_application_menu>
    {
        public ApplicationMenuRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
