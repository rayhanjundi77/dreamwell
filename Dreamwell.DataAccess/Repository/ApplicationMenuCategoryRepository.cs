﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ApplicationMenuCategoryRepository : ServiceRespository<application_menu_category, vw_application_menu_category>
    {
        public ApplicationMenuCategoryRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
