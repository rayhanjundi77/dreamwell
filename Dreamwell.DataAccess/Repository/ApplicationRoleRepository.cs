﻿
namespace Dreamwell.DataAccess
{
    public class ApplicationRoleRepository : ServiceRespository<application_role, vw_application_role>
    {
        public ApplicationRoleRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
