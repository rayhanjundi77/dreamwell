﻿namespace Dreamwell.DataAccess
{
    public class DrillingBhaComponentRepository : ServiceRespository<drilling_bha_comp, vw_drilling_bha_comp>
    {
        public DrillingBhaComponentRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
