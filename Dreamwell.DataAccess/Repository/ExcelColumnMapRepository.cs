﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ExcelColumnMapRepository : ServiceRespository<excel_column_map,vw_excel_column_map>
    {
        public ExcelColumnMapRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
