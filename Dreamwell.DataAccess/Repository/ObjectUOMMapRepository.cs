﻿namespace Dreamwell.DataAccess
{
    public class ObjectUOMMapRepository:ServiceRespository<object_uom_map, vw_object_uom_map>
    {
        public ObjectUOMMapRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
