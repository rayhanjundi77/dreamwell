﻿using Dapper;
using DataTables.Mvc;
using Dreamwell.DataAccess.ViewModels;
using Dreamwell.Infrastructure.DataTables;
using NLog.LayoutRenderers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class DatatablesRepository : IDisposable
    {
        SqlConnection con;
        public DatatablesRepository()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString);

        }
        public void Dispose()
        {
            con.Dispose();
        }

        public DataTablesResponse generate<T>(DataTableRequest dataTableRequest, string query = "")
        {
            DataTablesResponse result = new DataTablesResponse(0, new List<T>(), 0, 0);

            var columns = dataTableRequest.Columns.Select(x => x.Data).ToList();

            StringBuilder sb = new StringBuilder();
            StringBuilder where = new StringBuilder();
            where.Append(" where is_active=1 ");
            where.Append(query);

            sb.Append("select ").Append(string.Join(",", columns)).Append(" from ").Append(typeof(T).Name);

            if(!string.IsNullOrEmpty(dataTableRequest.Search.Value))
            {
                for (int i = 0; i < columns.Count; i++)
                {
                    if (i == 0)
                    {
                        where.Append(" and ").Append(columns[i]).Append(" LIKE '%").Append(dataTableRequest.Search.Value).Append("%' ");
                    }  
                    else
                    {
                        where.Append(" OR ").Append(columns[i]).Append(" LIKE '%").Append(dataTableRequest.Search.Value).Append("%' ");
                    }
                }
            }
            sb.Append(where.ToString());

            if (dataTableRequest.Order.Length > 0)
            {
                sb.Append(" order by ").Append(columns[dataTableRequest.Order[0].Column]).Append(" ").Append(dataTableRequest.Order[0].Dir);
            } else
            {
                sb.Append(" order by (SELECT NULL) desc ");
            }

            sb.Append(" OFFSET ").Append(dataTableRequest.Start).Append(" ROWS FETCH NEXT ").Append(dataTableRequest.Length).Append(" ROWS ONLY ");

            var queryResult = con.Query<T>(sb.ToString()).ToList();
            sb = new StringBuilder();
            sb.Append("select count (1) ").Append(" from ").Append(typeof(T).Name);
            sb.Append(where.ToString());

            var countResult = con.ExecuteScalar<int>(sb.ToString());
            result = new DataTablesResponse(dataTableRequest.Draw, queryResult, countResult, countResult);
            return result;
        }
    }
}
