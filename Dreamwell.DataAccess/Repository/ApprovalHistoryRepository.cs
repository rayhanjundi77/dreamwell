﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ApprovalHistoryRepository : ServiceRespository<approval_history, vw_approval_history>
    {
        public ApprovalHistoryRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
