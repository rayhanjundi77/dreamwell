﻿
namespace Dreamwell.DataAccess
{
    public class ApplicationUserRepository:ServiceRespository<application_user, vw_application_user>
    {
        public ApplicationUserRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
