﻿
namespace Dreamwell.DataAccess
{
    public class DrillFormationsActualDetailRepository : ServiceRespository<drill_formations_actual_detail, vw_drill_formations_actual>
    {
        public DrillFormationsActualDetailRepository(DataContext dataContext)
            : base(dataContext)
        {

        }
    }
}
