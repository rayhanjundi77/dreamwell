﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class DailyCostRepository : ServiceRespository<daily_cost, vw_daily_cost>
    {
        public DailyCostRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
