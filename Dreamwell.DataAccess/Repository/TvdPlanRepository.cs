﻿namespace Dreamwell.DataAccess
{
    public class TvdPlanRepository:ServiceRespository<tvd_plan, vw_tvd_plan>
    {
        public TvdPlanRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
