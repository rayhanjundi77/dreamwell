﻿namespace Dreamwell.DataAccess
{
    public class RoleAccessMenuRepository : ServiceRespository<role_access_menu, vw_role_access_menu>
    {
        public RoleAccessMenuRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
