﻿using Dapper;
using Dreamwell.DataAccess.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class WellDataRepository : BaseRepository<Base19Format>
    {
        public List<Base19Format> AfeContractByWellID(string wellID)
        {
            var parameters = new { wellID = wellID };

            //string sql = @"SELECT 
            //            subquery.afe_line_id,
            //            subquery.Currency,
            //            query2.total_price_plan,
            //            subquery.total_price_actual
            //        FROM (
            //            SELECT 
            //                afe_line_id,
            //                currency,
            //                SUM(total_actuals) AS total_price_actual
            //            FROM (
            //                SELECT 
            //                    afe_contract.unit,
            //                    contract_detail.unit_price,
            //                    material.afe_line_id,
            //                    currency.currency_symbol as currency,
            //                    SUM(COALESCE(contract_detail.unit_price, 0) / contract_detail.current_rate_value) * COALESCE(daily_cost.unit, 0) AS total_actuals
            //                FROM 
            //                    daily_cost
            //                JOIN 
            //                    afe_contract  ON afe_contract.id = daily_cost.afe_contract_id
            //                JOIN 
            //                    contract_detail  ON contract_detail.id = afe_contract.contract_detail_id
            //                JOIN 
            //                    material ON material.id = contract_detail.material_id
            //                JOIN
            //                    afe ON afe.id = afe_contract.afe_id
            //                INNER JOIN 
            //                    currency ON currency.id = contract_detail.currency_id
            //                WHERE 
            //                    afe.well_id = @wellID
            //                GROUP BY 
            //                    currency.currency_symbol,
            //                    material.afe_line_id, afe_contract.unit, contract_detail.unit_price, daily_cost.unit
            //            ) AS subquery
            //            GROUP BY 
            //                afe_line_id, currency
            //        ) AS subquery
            //        JOIN (
            //            SELECT 
            //                ap.afe_line_id,
            //                SUM(ap.total_price) AS total_price_plan
            //            FROM 
            //                vw_afe_plan ap
            //                JOIN afe af ON af.id = ap.afe_id
            //            WHERE 
            //                af.well_id = @wellID
            //            GROUP BY 
            //                ap.afe_line_id
            //        ) AS query2 ON subquery.afe_line_id = query2.afe_line_id;
            //        ";
            string sql = @"SELECT 
                        COALESCE(subquery.afe_line_id, query2.afe_line_id) AS afe_line_id,
                        COALESCE(subquery.Currency, query2.Currency) AS Currency,  
                        COALESCE(ROUND(query2.total_price_plan, 2), 0) AS total_price_plan,
                        COALESCE(ROUND(subquery.total_price_actual, 2), 0) AS total_price_actual
                    FROM (
                        SELECT 
                            material.afe_line_id,
                            currency.currency_symbol AS Currency,
                            SUM(ROUND((COALESCE(contract_detail.unit_price, 0) / contract_detail.current_rate_value) * COALESCE(daily_cost.unit, 0), 2)) AS total_price_actual
                        FROM 
                            daily_cost 
                        JOIN afe_contract ON afe_contract.id = daily_cost.afe_contract_id
                        JOIN contract_detail ON contract_detail.id = afe_contract.contract_detail_id
                        JOIN material ON material.id = contract_detail.material_id
                        JOIN afe ON afe.id = afe_contract.afe_id
                        JOIN currency ON currency.id = contract_detail.currency_id
                        WHERE afe.well_id = @wellID
                        GROUP BY 
                            material.afe_line_id, 
                            currency.currency_symbol
                    ) AS subquery
                    FULL OUTER JOIN (
                        SELECT 
                            ap.afe_line_id,
                            curr.currency_symbol AS Currency,  
                            SUM(ROUND(ap.total_price, 2)) AS total_price_plan
                        FROM 
                            vw_afe_plan ap
                        JOIN afe af ON af.id = ap.afe_id
                        JOIN currency curr ON curr.id = ap.currency_id  
                        WHERE af.well_id = @wellID
                        GROUP BY ap.afe_line_id, curr.currency_symbol  
                    ) AS query2 
                    ON subquery.afe_line_id = query2.afe_line_id;
                    ";
            var result = con.Query<Base19Format>(sql, parameters).ToList();
            return result;
        }
        public List<afe_line> AfeLine()
        {
            string sql = "SELECT * from afe_line order by line asc";
            var result = con.Query<afe_line>(sql).ToList();
            return result;
        }

      
    }
}
