﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class BusinessUnitFieldRepository : ServiceRespository<business_unit_field, vw_business_unit_field>
    {
        public BusinessUnitFieldRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
