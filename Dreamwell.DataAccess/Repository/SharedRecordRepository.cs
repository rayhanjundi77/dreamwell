﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class SharedRecordRepository : ServiceRespository<shared_record, vw_shared_record>
    {
        public SharedRecordRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
