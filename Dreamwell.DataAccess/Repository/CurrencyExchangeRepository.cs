﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class CurrencyExchangeRepository:ServiceRespository<currency_exchange, vw_currency_exchange>
    {
        public CurrencyExchangeRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
