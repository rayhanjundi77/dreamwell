﻿namespace Dreamwell.DataAccess
{
    public class DrillingChemicalUsedRepository : ServiceRespository<drilling_chemical_used, vw_drilling_chemical_used>
    {
        public DrillingChemicalUsedRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
