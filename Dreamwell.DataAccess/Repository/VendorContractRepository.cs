﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class VendorContractRepository : ServiceRespository<vendor_contract, vw_vendor_contract>
    {
        public VendorContractRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
