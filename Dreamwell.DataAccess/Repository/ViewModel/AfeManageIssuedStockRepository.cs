﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class AfeManageIssuedStockRepository : ServiceRespositoryViewModel<vw_afe_manage_issued_stock>
    {
        public AfeManageIssuedStockRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
