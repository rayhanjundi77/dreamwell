﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ApplicationMenuAccessRepository : ServiceRespositoryViewModel<vw_application_menu_access>
    {
        public ApplicationMenuAccessRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
