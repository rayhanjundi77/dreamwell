﻿namespace Dreamwell.DataAccess
{
    public class WellObjectUOMMapRepository:ServiceRespository<well_object_uom_map, vw_well_object_uom_map>
    {
        public WellObjectUOMMapRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
