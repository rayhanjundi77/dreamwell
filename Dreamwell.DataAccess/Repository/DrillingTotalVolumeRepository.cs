﻿namespace Dreamwell.DataAccess
{
    public class DrillingTotalVolumeRepository : ServiceRespository<drilling_total_volume, vw_drilling_total_volume>
    {
        public DrillingTotalVolumeRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
