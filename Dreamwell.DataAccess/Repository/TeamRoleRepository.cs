﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class TeamRoleRepository:ServiceRespository<team_role, vw_team_role>
    {
        public TeamRoleRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
