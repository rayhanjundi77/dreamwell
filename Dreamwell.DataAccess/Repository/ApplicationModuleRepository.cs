﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ApplicationModuleRepository : ServiceRespository<application_module, vw_application_module>
    {
        public ApplicationModuleRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
