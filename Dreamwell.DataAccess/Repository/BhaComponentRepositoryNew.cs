﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class BhaComponentRepositoryNew : BaseRepository<bha_component>
    {
        public string bha_update_file(string recordid, string filename,string ext,string mime, string filepath, int size)
        {
            string status="";
            try
            {
                var parameters = new
                {
                    recordid = recordid,
                    filename = filename,
                    ext = ext,
                    mime = mime,
                    filepath = filepath,
                    size = size
                };

                string sql = "UPDATE filemaster SET name = @filename, realname= @filename,extension = @ext, size=@size, mimetype = @mime, filepath= @filepath WHERE id = @recordid";
                var result = con.Query<bha_component>(sql);
                status = "succes";

                return status;
            } catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
           
        }

        public List<bha_component> ListBha()
        {
            string sql = "SELECT top 10 * from filemaster order by created_on desc";
            var result = con.Query<bha_component>(sql).ToList();
            return result;
        }
    }
}
