﻿namespace Dreamwell.DataAccess
{
    public class DrillFormationsPlanRepository:ServiceRespository<drill_formations_plan, vw_drill_formations_plan>
    {
        public DrillFormationsPlanRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
