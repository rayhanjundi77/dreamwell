﻿using Dreamwell.DataAccess.Models.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using Newtonsoft.Json.Linq;
using System.Data.Entity.Infrastructure;
using Dapper;

namespace Dreamwell.DataAccess.Repository
{
    public class BaseRepository<T>: IDisposable where T : class
    {
        public SqlConnection con;
        DataContext dataContext;
        public BaseRepository(DataContext context)
        {
            //Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString);
            dataContext = context;
        }

        public BaseRepository()
        {
            //Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString);
        }

        public void Dispose()
        {
            con.Dispose();
        }

        public async Task<T> Save(T model, bool isNew)
        {
            if (isNew)
            {
                foreach (var property in typeof(T).GetProperties())
                {
                    if (property.Name == "id")
                    {
                        property.SetValue(model, Guid.NewGuid().ToString());
                    }

                    if (property.Name == "created_by" || property.Name == "owner_id") 
                    {
                        property.SetValue(model, dataContext.AppUserId);
                    }

                    if (property.Name == "created_on")
                    {
                        property.SetValue(model, DateTime.Now);
                    }

                    if (property.Name == "is_active")
                    {
                        property.SetValue(model, true);
                    }

                    if (property.Name == "organization_id")
                    {
                        property.SetValue(model, dataContext.OrganizationId);
                    }
                }
                try
                {
                    await con.InsertAsync<T>(model);
                    return model;
                }
                catch (Exception)
                {
                    return null;
                }
            } else {
                var oldModel = await GetById(model.GetType().GetProperty("id").GetValue(model).ToString()); 
                foreach (var property in typeof(T).GetProperties())
                {
                    var currentProperty = property.GetValue(model);
                    if(currentProperty == null)
                    {
                        property.SetValue(model, property.GetValue(oldModel));
                    }

                    if (property.Name == "modified_by")
                    {
                        property.SetValue(model, dataContext.AppUserId);
                    }

                    if (property.Name == "modified_on")
                    {
                        property.SetValue(model, DateTime.Now);
                    }
                }
                try
                {
                    await con.UpdateAsync<T>(model);
                    return model;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public async Task<bool> Delete(string[] ids)
        {
            try
            {
                foreach (string id in ids)
                {
                    var model = await GetById(id);

                    foreach (var property in typeof(T).GetProperties())
                    {
                        if (property.Name == "is_active")
                        {
                            property.SetValue(model, false);
                        }
                    }
                    
                    await con.UpdateAsync<T>(model);
                }
                return true;
            } catch(Exception)
            {
                return false;
            }
        }

        public async Task<bool> Destroy(string id)
        {
            try
            {
                var model = await GetById(id);
                await con.DeleteAsync<T>(model);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public async Task<IEnumerable<T>> Get(string query = "")
        {
            var type = typeof(T);
            var table = (Dapper.Contrib.Extensions.TableAttribute)type.GetCustomAttributes(false).SingleOrDefault(attr => attr.GetType().Name == "TableAttribute");
            var sql = $"select * from {table.Name} where is_active = 1 and organization_id='{this.dataContext.OrganizationId}' {query} ";
            return await con.QueryAsync<T>(sql);
        }
        public async Task<T> GetById(string id)
        {
            return await con.GetAsync<T>(id);
        }

        public async Task<IEnumerable<T>> GetAll() 
        {
            return await con.GetAllAsync<T>();
        }
    }
}
