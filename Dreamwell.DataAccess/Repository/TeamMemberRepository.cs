﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class TeamMemberRepository:ServiceRespository<team_member, vw_team_member>
    {
        public TeamMemberRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
