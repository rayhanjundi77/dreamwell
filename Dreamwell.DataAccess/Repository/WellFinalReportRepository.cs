﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class WellFinalReportRepository : ServiceRespository<well_final_report, vw_well_final_report>
    {
        public WellFinalReportRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
