﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class IadcAnalysisRepository : ServiceRespositoryViewModel<vw_iadc_analysis>
    {
        public IadcAnalysisRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
