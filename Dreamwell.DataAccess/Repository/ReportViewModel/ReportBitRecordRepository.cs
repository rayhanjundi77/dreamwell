﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ReportBitRecordRepository : ServiceRespositoryViewModel<vw_report_bit_record>
    {
        public ReportBitRecordRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        { 
            
        }
    }
}
