﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ReportWellBitRepository : ServiceRespositoryViewModel<vw_report_well_bit>
    {
        public ReportWellBitRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
