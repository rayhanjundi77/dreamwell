﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ReportCummulativeCostRepository : ServiceRespositoryViewModel<vw_report_cummulative_cost>
    {
        public ReportCummulativeCostRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
