﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ReportHoleCasingActualRepository : ServiceRespositoryViewModel<vw_report_hole_casing_actual>
    {
        public ReportHoleCasingActualRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
