﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class BenchmarkReportIadcRepository : ServiceRespositoryViewModel<vw_benchmark_report_iadc>
    {
        public BenchmarkReportIadcRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
