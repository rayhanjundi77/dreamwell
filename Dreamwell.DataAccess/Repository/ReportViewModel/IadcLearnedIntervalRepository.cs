﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class IadcLearnedIntervalRepository : ServiceRespositoryViewModel<vw_iadc_learned_interval>
    {
        public IadcLearnedIntervalRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
