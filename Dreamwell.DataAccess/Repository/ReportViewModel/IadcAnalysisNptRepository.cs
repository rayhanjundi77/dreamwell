﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class IadcAnalysisNptRepository : ServiceRespositoryViewModel<vw_iadc_analysis_npt>
    {
        public IadcAnalysisNptRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
