﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ReportDrillingStuckPipeRepository : ServiceRespositoryViewModel<vw_report_drilling_stuck_pipe>
    {
        public ReportDrillingStuckPipeRepository(DataContext dataContext, params object[] parameterArgs)
            :base(dataContext, parameterArgs)
        {

        }

    }
}
