﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class WellDrillingPerformanceRepository : ServiceRespositoryViewModel<vw_well_drilling_performance>
    {
        public WellDrillingPerformanceRepository(DataContext dataContext, params object[] parameterArgs)
            : base(dataContext, parameterArgs)
        {

        }

    }
}
