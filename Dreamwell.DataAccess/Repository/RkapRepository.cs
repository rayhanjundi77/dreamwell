﻿using Autofac.Core;
using CommonTools.UnitOfMeasurement.Power;
using CommonTools.UnitOfMeasurement.Time;
using Dapper;
using Dapper.Contrib.Extensions;
using DataTables.Mvc;
using Dreamwell.DataAccess.Models.Entity;
using Dreamwell.Infrastructure.DataTables;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Dreamwell.DataAccess.Repository
{
    public class RkapRepository : IDisposable
    {
        SqlConnection sqlConnection;
        string defQuery;
        public RkapRepository()
        {
            sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString);
            defQuery = @"SELECT rk.*,
	                                rg.name rig_name,
	                                fd.field_name,
								    bu.unit_name
                                  FROM [dbo].[rkap] rk 
                                  inner join [dbo].[rig] rg on rk.rig_id = rg.id
                                  inner join [dbo].[field] fd on rk.field_id = fd.id
							      inner join [dbo].business_unit bu on rk.business_unit_id = bu.id";
        }

        public void Dispose()
        {
            sqlConnection.Dispose();
        }

        public async Task<string> RealRkap(rkap_history model)
        {
            // Mengambil data dari tabel well
            var dataWell = await sqlConnection.QueryFirstOrDefaultAsync<WellData>(
                "SELECT well_name, afe_cost, water_depth FROM well WHERE id = @WellId",
                new { WellId = model.well_id }
            );

            // Mengambil data dari tabel rkap
            var dataRkap = await sqlConnection.QueryFirstOrDefaultAsync<rkap>(
                "SELECT * FROM rkap WHERE id = @HeaderId",
                new { HeaderId = model.header_id }
            );

            // Memeriksa apakah data well ditemukan
            if (dataWell == null)
            {
                throw new Exception("Data well tidak ditemukan");
            }

            // Memeriksa apakah data RKAP ditemukan
            if (dataRkap == null)
            {
                throw new Exception("Data RKAP tidak ditemukan");
            }

            // Menetapkan tanggal realisasi RKAP
            dataRkap.realization_date = DateTime.Now;

            // Menginisialisasi properti model yang baru
            model.id = Guid.NewGuid().ToString();
            model.status = 2; // Set status to "Realisasi"

            // Assign business_unit_id from dataRkap
            model.business_unit_id = dataRkap.business_unit_id;

            // Set other fields based on spud_date and planned_days
            if (model.spud_date.HasValue && model.planned_days.HasValue)
            {
                model.end_date = model.spud_date.Value.AddDays(Convert.ToInt16(model.planned_days.Value));
            }

            model.created_on = DateTime.Now;
            model.well_name = dataWell.well_name;
            model.budget = dataWell.afe_cost;
            model.water_depth = dataWell.water_depth;

            // Insert rkap_history record
            await sqlConnection.InsertAsync<rkap_history>(model);

            // Update rkap record with the realization date
            await sqlConnection.UpdateAsync(dataRkap);

            return model.id;
        }


        // Kelas untuk menyimpan data well
        public class WellData
        {
            public string well_name { get; set; }
            public string afe_cost { get; set; }
            public string water_depth { get; set; }
        }


        public async Task<string> Insert(rkap model)
        {
            model.id = Guid.NewGuid().ToString();
            model.status = 0;
            //if (model.spud_date.HasValue && model.planned_days.HasValue)
            //    model.end_date = model.spud_date.Value.AddDays(Convert.ToInt16(model.planned_days.Value));
            if (model.spud_date.HasValue && model.planned_days.HasValue)
                model.end_date = model.spud_date.Value.AddDays(Convert.ToDouble(model.planned_days.Value));
            model.region = model.business_unit_id;
            model.well_zona = model.field_id;
            model.created_on = DateTime.Now;
            model.is_active = true;

            await sqlConnection.InsertAsync<rkap>(model);
            return model.id;
        }

        public async Task<bool> Update(rkap model)
        {
            model.modified_on = DateTime.Now;
            model.is_active = true;
            //if (model.spud_date.HasValue && model.planned_days.HasValue)
            //    model.end_date = model.spud_date.Value.AddDays(Convert.ToInt16(model.planned_days.Value));
            if (model.spud_date.HasValue && model.planned_days.HasValue)
                model.end_date = model.spud_date.Value.AddDays(Convert.ToDouble(model.planned_days.Value));
            return await sqlConnection.UpdateAsync<rkap>(model);
        }

        public async Task<bool> Delete(string id)
        {
            var existingRKAP = await sqlConnection.GetAsync<rkap>(id);
            if (existingRKAP != null)
            {
                return await sqlConnection.DeleteAsync(existingRKAP);
            }
            return false;
        }

        public async Task<bool> DeleteHistory(string id)
        {
            var models = sqlConnection.Query<rkap_history>($"select id from rkap_history where header_id ='{id}'");

            //var existingRKAP = await sqlConnection.GetAsync<rkap>(id);
            if (models != null)
            {
                return await sqlConnection.DeleteAsync(models);
            }
            return false;
        }


        public async Task<string> AddHistory(rkap rkap)
        {
            var models = sqlConnection.Query<rkap_history>($"select * from rkap_history where header_id ='{rkap.id}' and status='{rkap.status}'");
            if (models.Count() == 0)
            {
                string guid = Guid.NewGuid().ToString();
                var hist = new rkap_history()
                {
                    id = guid,
                    header_id = rkap.id,
                    created_on = DateTime.Now,
                    created_by = "",
                    well_classification = rkap.well_classification,
                    well_name = rkap.well_name,
                    asset_id = rkap.asset_id,
                    budget = rkap.budget,
                    budget_currency = rkap.budget_currency,
                    business_unit_id = rkap.business_unit_id,
                    end_date = rkap.end_date,
                    field_id = rkap.field_id,
                    planned_days = rkap.planned_days,
                    rig_id = rkap.rig_id,
                    spud_date = rkap.spud_date,
                    status = rkap.status,
                    hp_no = rkap.hp_no,
                    water_depth = rkap.water_depth,
                    well_id = rkap.well_id,
                };

                await sqlConnection.InsertAsync<rkap_history>(hist);
                return guid;
            }
            else
            {
                var model = models.First();
                model.header_id = rkap.id;
                model.created_on = DateTime.Now;
                model.created_by = "";
                model.well_classification = rkap.well_classification;
                model.well_name = rkap.well_name;
                model.asset_id = rkap.asset_id;
                model.budget = rkap.budget;
                model.budget_currency = rkap.budget_currency;
                model.business_unit_id = rkap.business_unit_id;
                model.end_date = rkap.end_date;
                model.field_id = rkap.field_id;
                model.planned_days = rkap.planned_days;
                model.rig_id = rkap.rig_id;
                model.spud_date = rkap.spud_date;
                model.status = rkap.status;
                model.hp_no = rkap.hp_no;
                model.water_depth = rkap.water_depth;
                model.well_id = rkap.well_id;
                await sqlConnection.UpdateAsync<rkap_history>(model);
            }

            return null;
        }

        public async Task<rkap> GetById(string id)
        {
            return await sqlConnection.GetAsync<rkap>(id);
        }

        public async Task<vw_rkap> GetViewById(string id)
        {
            return await sqlConnection.QueryFirstOrDefaultAsync<vw_rkap>($"select * from rkap rk WHERE rk.id=@id", new { id });
        }

        public async Task<IEnumerable<vw_rkap_history>> GetAllHistoryById(string id)
        {
            return await sqlConnection.QueryAsync<vw_rkap_history>("SELECT * FROM rkap_history WHERE header_id = @id", new { id });
        }

        public async Task<IEnumerable<vw_well>> GetWellRkap(string id)
        {
            return await sqlConnection.QueryAsync<vw_well>("SELECT * FROM well WHERE field_id = @id", new { id });
        }


        public async Task<IEnumerable<rkap>> GetAll()
        {
            return await sqlConnection.GetAllAsync<rkap>();
        }

        public async Task<IEnumerable<schedule_rkap>> GetSchedulesAsync()
        {
            var rkaps = await sqlConnection.GetAllAsync<rkap>();
            var schedules = rkaps.Select(s => new schedule_rkap()
            {
                id = s.id,
                title = s.well_name,
                start = s.spud_date,
                end = s.spud_date.Value.AddDays(s.planned_days.HasValue ? Convert.ToInt32(s.planned_days.Value) : 1),
                status = s.status,
                allDay = false
            }).ToList();

            return schedules;
        }

        public async Task<DataTablesResponse> GetDataTable(DataTableRequest dtRequest)
        {
            StringBuilder query = new StringBuilder();
            int total = sqlConnection.ExecuteScalar<int>($"SELECT count(*) FROM ({defQuery}) a");

            query.Append(defQuery);
            query.Append(" ORDER BY rk.well_name ASC ");
            query.Append($" OFFSET {dtRequest.Start} ROWS FETCH NEXT {dtRequest.Length} ROWS ONLY");

            var dataRkap = await sqlConnection.QueryAsync<vw_rkap>(query.ToString());
            return new DataTablesResponse(dtRequest.Draw, dataRkap, dataRkap.Count(), total);
        }



        public async Task<DataTablesResponse> GetDataTableNew(DataTableRequest dtRequest, string idRegion)
        {
            try
            {
                var totalQuery = "SELECT COUNT(*) FROM rkap WHERE status IS NOT NULL";
                if (!string.IsNullOrEmpty(idRegion))
                {
                    totalQuery += " AND business_unit_id = @IdRegion";
                }

                var total = await sqlConnection.ExecuteScalarAsync<int>(totalQuery, new { IdRegion = idRegion });
                string dataQuery;
                if (!string.IsNullOrEmpty(idRegion))
                {
                     dataQuery = $@"
                    SELECT * FROM (
                        SELECT *, ROW_NUMBER() OVER (ORDER BY well_name ASC) AS RowNum 
                        FROM rkap 
                        WHERE status IS NOT NULL
                        AND business_unit_id =@IdRegion
                    ) AS rk
                    WHERE RowNum BETWEEN @Start AND @End";

                }
                else
                {
                     dataQuery = $@"
                    SELECT * FROM (
                        SELECT *, ROW_NUMBER() OVER (ORDER BY well_name ASC) AS RowNum 
                        FROM rkap 
                        WHERE status IS NOT NULL
                    ) AS rk
                    WHERE RowNum BETWEEN @Start AND @End";
                }
                var parameters = new
                {
                    IdRegion = idRegion,
                    Start = dtRequest.Start + 1,
                    End = dtRequest.Start + dtRequest.Length
                };

                var dataRkap = await sqlConnection.QueryAsync<vw_rkap>(dataQuery, parameters);

                return new DataTablesResponse(dtRequest.Draw, dataRkap.ToList(), total, total);
            }
            catch (Exception ex)
            {  
                return new DataTablesResponse(0, new List<vw_rkap>(), 0, 0);
            }
        }


        public async Task<DataTablesResponse> GetRkapOptiones(DataTableRequest dtRequest)
        {
            try
            {
                var totalQuery = "SELECT COUNT(*) FROM rkap WHERE status IS NOT NULL";
                var total = await sqlConnection.ExecuteScalarAsync<int>(totalQuery);

                var dataQuery = $@"SELECT * FROM (
                                SELECT *, ROW_NUMBER() OVER (ORDER BY well_name ASC) AS RowNum 
                                FROM rkap 
                                WHERE status IS NOT NULL
                            ) AS rk
                            WHERE RowNum BETWEEN {dtRequest.Start + 1} AND {dtRequest.Start + dtRequest.Length}";

                var dataRkap = await sqlConnection.QueryAsync<vw_rkap>(dataQuery);

                return new DataTablesResponse(dtRequest.Draw, dataRkap.ToList(), dataRkap.Count(), total);
            }
            catch (Exception ex)
            {
                //appLogger.Error(ex);
                return new DataTablesResponse(0, new List<vw_rkap>(), 0, 0);
            }
        }
        public async Task<List<rkap_chart_ds>> GetChartDataDevelopment(int year, string idRegion)
        {
            try
            {
                string query = @"
                    SELECT 
                      rk.well_name,
                      bu.unit_name, 
                      rk.status, 
                      YEAR(rk.spud_date) AS year, 
                      COUNT(*) AS jumlah, 
                      SUM(CASE WHEN w.submitted_by IS NULL THEN 1 ELSE 0 END) AS ongoing, 
                      SUM(CASE WHEN w.submitted_by IS NOT NULL THEN 1 ELSE 0 END) AS real, 
                      SUM(CASE WHEN af.is_closed = 1 THEN 1 ELSE 0 END) AS total_co,
                      1 AS type 
                    FROM 
                        rkap_history rk 
                        INNER JOIN business_unit bu ON rk.business_unit_id = bu.id 
                        INNER JOIN field f ON f.id = rk.field_id 
                        LEFT JOIN well w ON rk.well_id = w.id
                        LEFT JOIN afe af ON af.well_id = w.id
                        LEFT JOIN (
                            SELECT 
                                well_id,
                                SUM(CASE WHEN w.well_name IS NULL THEN d.daily_cost ELSE 0 END) AS ongoing_cost,
                                SUM(CASE WHEN w.well_name IS NOT NULL THEN 1 ELSE 0 END) AS real_cost
                            FROM 
                                vw_drilling d
                                LEFT JOIN well w ON d.well_id = w.id
                            GROUP BY 
                                well_id
                        ) AS d ON d.well_id = w.id
                    WHERE 
                        YEAR(rk.spud_date) = @year 
                        AND rk.well_classification = 'Development'
                        {0}
                    GROUP BY 
                       rk.well_name,
                      bu.id, 
                      bu.unit_name, 
                      f.field_name, 
                      rk.status, 
                      YEAR(rk.spud_date)

                    UNION ALL 

                    SELECT 
                        NULL AS well_name,
                        bu.unit_name, 
                        rk.status, 
                        YEAR(rk.spud_date) AS year, 
                        COUNT(*) AS jumlah, 
                        0 AS ongoing, 
                        0 AS real, 
                        0 AS total_co,
                        2 AS type 
                    FROM 
                        rkap_history rk 
                        INNER JOIN business_unit bu ON rk.business_unit_id = bu.id 
                    WHERE 
                        EXISTS (
                            SELECT 
                                header_id 
                            FROM 
                                rkap_history rh 
                            WHERE 
                                YEAR(rh.spud_date) = @lastYear
                                AND YEAR(rh.end_date) = @lastYear
                                AND rk.header_id = rh.header_id
                        ) 
                        AND rk.status IN (1, 2) 
                        {1}
                    GROUP BY 
                        bu.id, 
                        bu.unit_name, 
                        rk.status, 
                        YEAR(rk.spud_date)
                ";

                string filterRegion = string.IsNullOrEmpty(idRegion) ? "" : "AND rk.business_unit_id = @idRegion";
                string finalQuery = string.Format(query, filterRegion, filterRegion);

                var lastYear = year - 1;
                var parameters = new { year, lastYear, idRegion };
                var dataGrp = await sqlConnection.QueryAsync<chart_rkap>(finalQuery, parameters);

                var unitNames = dataGrp.Select(s => s.unit_name).Distinct().ToList();
                var wellNames = dataGrp.Select(s => s.well_name).Distinct().ToList();

                List<rkap_chart_ds> data = new List<rkap_chart_ds>();
                int currYear = DateTime.Now.Year;

                // 'Original Rkap', 'CO 2019', 'Revised Rkap', 'Realization', 'Outlook Spud','Outlook Complete'
                foreach (var unitName in unitNames)
                {
                    foreach (var wellName in wellNames)
                    {
                        var dataUnit = dataGrp.Where(w => w.unit_name == unitName  && w.year == year);
                        rkap_chart_ds ds = new rkap_chart_ds(unitName, wellName);

                        // original
                        int ori = dataUnit.Where(w => w.status == 0 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName).Sum(s => s.jumlah);
                        int real = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName && w.well_name == wellName).Sum(s => s.jumlah);
                        int co = dataUnit.Where(w => w.total_co == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        int rev = dataUnit.Where(w => w.status == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        int realiz = dataUnit.Where(w => w.real == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.real);
                        int ongo = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.ongoing);
                        int oSpud = realiz + ongo;
                        int oComp = realiz;
                        // before year
                        int bef = dataGrp.Where(w => w.type == 2 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        //ds.Data = new List<int>() { ori, rev, real, bef, 0, 0 };
                        ds.Data = new List<int>() { ori, co, rev, realiz, ongo, oSpud, oComp };
                        data.Add(ds);

                    }
                }

                return data;

            }
            catch (Exception e)
            {
                // Mengembalikan pesan kesalahan bersama dengan daftar kosong
                string errorMessage = "Terjadi kesalahan saat mengambil data RKAP untuk tahun " + year + ": " + e.Message;
                Console.WriteLine(errorMessage); // Opsional: menampilkan pesan kesalahan ke konsol atau log
                return new List<rkap_chart_ds>();
            }
        }


        public async Task<List<rkap_chart_ds>> GetChartBudgetDevelopment(int year,string idRegion)
        {
            try
            {
                string query = @"
                           SELECT 
                            rk.well_name,
                            bu.unit_name, 
                            rk.status, 
                            YEAR(rk.spud_date) AS year, 
                            CAST(rk.budget AS DECIMAL(18, 2)) AS budget,
                            CASE 
                                WHEN COUNT(*) > 1 THEN 1 
                                ELSE COUNT(*) 
                            END AS jumlah, 
                            SUM(CASE WHEN w.submitted_by IS NULL THEN d.daily_cost ELSE 0 END) AS ongoing, 
                            SUM(CASE WHEN w.submitted_by IS NOT NULL THEN d.daily_cost ELSE 0 END) AS real, 
                            SUM(CASE WHEN af.is_closed = 1 THEN COALESCE(ap.actual_price, 0) ELSE 0 END) AS totalafe,
                            1 AS type 
                        FROM 
                            rkap_history rk 
                            INNER JOIN business_unit bu ON rk.business_unit_id = bu.id 
                            INNER JOIN field f ON f.id = rk.field_id 
                            LEFT JOIN well w ON rk.well_id = w.id
                            LEFT JOIN afe af ON af.well_id = w.id
                            LEFT JOIN afe_plan ap ON ap.afe_id = af.id
                            LEFT JOIN (
                                SELECT 
                                    well_id,
                                    d.daily_cost
                                FROM 
                                    vw_drilling d
                                    LEFT JOIN well w ON d.well_id = w.id
                                GROUP BY 
                                    well_id,
                                    d.daily_cost
                            ) AS d ON d.well_id = w.id
                        WHERE 
                            YEAR(rk.spud_date) = @year 
                            AND rk.well_classification = 'Development'
                            {0}
                        GROUP BY 
                            rk.well_name,
                            bu.id,
                            rk.budget,
                            bu.unit_name, 
                            f.field_name, 
                            rk.status, 
                            YEAR(rk.spud_date)

                        UNION ALL 

                        SELECT 
                            NULL AS well_name,
                            bu.unit_name, 
                            rk.status, 
                            YEAR(rk.spud_date) AS year, 
                            rk.budget AS budget,
                            CASE 
                                WHEN COUNT(*) > 1 THEN 1 
                                ELSE COUNT(*) 
                            END AS jumlah, 
                            0 AS ongoing_cost, 
                            0 AS real_cost, 
                            0 AS totalafe, 
                            2 AS type 
                        FROM 
                            rkap_history rk 
                            INNER JOIN business_unit bu ON rk.business_unit_id = bu.id 
                        WHERE 
                            EXISTS (
                                SELECT 
                                    header_id 
                                FROM 
                                    rkap_history rh 
                                WHERE 
                                    YEAR(rh.spud_date) = @lastYear
                                    AND YEAR(rh.end_date) = @lastYear
                                    AND rk.header_id = rh.header_id
                            ) 
                            AND rk.status IN (1, 2) 
                            {1}
                        GROUP BY 
                            bu.id, 
                            rk.budget,
                            bu.unit_name, 
                            rk.status,
                            YEAR(rk.spud_date)
                                            ";

                string filterRegion = string.IsNullOrEmpty(idRegion) ? "" : "AND rk.business_unit_id = @idRegion";
                string finalQuery = string.Format(query, filterRegion, filterRegion);

                var lastYear = year - 1;
                var parameters = new { year, lastYear, idRegion };
                var dataGrp = await sqlConnection.QueryAsync<chart_rkap>(finalQuery, parameters);

                var unitNames = dataGrp.Select(s => s.unit_name).Distinct().ToList();
                var wellNames = dataGrp.Select(s => s.well_name).Distinct().ToList();

                List<rkap_chart_ds> data = new List<rkap_chart_ds>();
                int currYear = DateTime.Now.Year;

                // 'Original Rkap', 'CO 2019', 'Revised Rkap', 'Realization', 'Outlook Spud','Outlook Complete'
                foreach (var unitName in unitNames)
                {
                    foreach (var wellName in wellNames)
                    {
                        var dataUnit = dataGrp.Where(w => w.unit_name == unitName  && w.year == year);
                        rkap_chart_ds ds = new rkap_chart_ds(unitName, wellName);

                        int ori = dataUnit.Where(w => w.status == 0 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.budget);
                        int co = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.totalafe);
                        int real = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.real);
                        int rev = dataUnit.Where(w => w.status == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.budget);
                        int ongo = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.ongoing);
                        int oComp = real;
                        int oSpud = real + ongo;
                        // before year
                        int bef = dataGrp.Where(w => w.type == 2 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        //ds.Data = new List<int>() { ori, rev, real, bef, 0, 0 };
                        ds.Data = new List<int>() { ori, co, rev, real, ongo, oSpud, oComp };
                        data.Add(ds);

                    }
                }

                return data;

            }
            catch (Exception e)
            {
                // Mengembalikan pesan kesalahan bersama dengan daftar kosong
                string errorMessage = "Terjadi kesalahan saat mengambil data RKAP untuk tahun " + year + ": " + e.Message;
                Console.WriteLine(errorMessage); // Opsional: menampilkan pesan kesalahan ke konsol atau log
                System.Diagnostics.Debug.WriteLine(errorMessage);
                return new List<rkap_chart_ds>();
            }

        }

        public async Task<List<rkap_chart_ds>> GetChartDataExploration(int year,string idRegion)
        {
            try
            {
                string query = @"
                    SELECT 
                      rk.well_name,
                      bu.unit_name, 
                      rk.status, 
                      YEAR(rk.spud_date) AS year, 
                      COUNT(*) AS jumlah, 
                      SUM(CASE WHEN w.submitted_by IS NULL THEN 1 ELSE 0 END) AS ongoing, 
                      SUM(CASE WHEN w.submitted_by IS NOT NULL THEN 1 ELSE 0 END) AS real, 
                      SUM(CASE WHEN af.is_closed = 1 THEN 1 ELSE 0 END) AS total_co,
                      1 AS type 
                    FROM 
                        rkap_history rk 
                        INNER JOIN business_unit bu ON rk.business_unit_id = bu.id 
                        INNER JOIN field f ON f.id = rk.field_id 
                        LEFT JOIN well w ON rk.well_id = w.id
                        LEFT JOIN afe af ON af.well_id = w.id
                        LEFT JOIN (
                            SELECT 
                                well_id,
                                SUM(CASE WHEN w.well_name IS NULL THEN d.daily_cost ELSE 0 END) AS ongoing_cost,
                                SUM(CASE WHEN w.well_name IS NOT NULL THEN 1 ELSE 0 END) AS real_cost
                            FROM 
                                vw_drilling d
                                LEFT JOIN well w ON d.well_id = w.id
                            GROUP BY 
                                well_id
                        ) AS d ON d.well_id = w.id
                    WHERE 
                        YEAR(rk.spud_date) = @year 
                        AND rk.well_classification = 'Exploration'
                        {0}
                    GROUP BY 
                       rk.well_name,
                      bu.id, 
                      bu.unit_name, 
                      f.field_name, 
                      rk.status, 
                      YEAR(rk.spud_date)

                    UNION ALL 

                    SELECT 
                        NULL AS well_name,
                        bu.unit_name, 
                        rk.status, 
                        YEAR(rk.spud_date) AS year, 
                        COUNT(*) AS jumlah, 
                        0 AS ongoing, 
                        0 AS real, 
                        0 AS total_co,
                        2 AS type 
                    FROM 
                        rkap_history rk 
                        INNER JOIN business_unit bu ON rk.business_unit_id = bu.id 
                    WHERE 
                        EXISTS (
                            SELECT 
                                header_id 
                            FROM 
                                rkap_history rh 
                            WHERE 
                                YEAR(rh.spud_date) = @lastYear
                                AND YEAR(rh.end_date) = @lastYear
                                AND rk.header_id = rh.header_id
                        ) 
                        AND rk.status IN (1, 2) 
                        {1}
                    GROUP BY 
                        bu.id, 
                        bu.unit_name, 
                        rk.status, 
                        YEAR(rk.spud_date)
                ";

                string filterRegion = string.IsNullOrEmpty(idRegion) ? "" : "AND rk.business_unit_id = @idRegion";
                string finalQuery = string.Format(query, filterRegion, filterRegion);

                var lastYear = year - 1;
                var parameters = new { year, lastYear, idRegion };
                var dataGrp = await sqlConnection.QueryAsync<chart_rkap>(finalQuery, parameters);

                var unitNames = dataGrp.Select(s => s.unit_name).Distinct().ToList();
                var wellNames = dataGrp.Select(s => s.well_name).Distinct().ToList();

                List<rkap_chart_ds> data = new List<rkap_chart_ds>();
                int currYear = DateTime.Now.Year;

                // 'Original Rkap', 'CO 2019', 'Revised Rkap', 'Realization', 'Outlook Spud','Outlook Complete'
                foreach (var unitName in unitNames)
                {
                    foreach (var wellName in wellNames)
                    {
                        var dataUnit = dataGrp.Where(w => w.unit_name == unitName  && w.year == year);
                        rkap_chart_ds ds = new rkap_chart_ds(unitName, wellName);

                        // original
                        int ori = dataUnit.Where(w => w.status == 0 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        int real = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        int co = dataUnit.Where(w => w.total_co == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        int rev = dataUnit.Where(w => w.status == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        int realiz = dataUnit.Where(w => w.real == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.real);
                        int ongo = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.ongoing);
                        int oSpud = realiz + ongo;
                        int oComp = realiz;
                        // before year
                        int bef = dataGrp.Where(w => w.type == 2 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);
                        //ds.Data = new List<int>() { ori, rev, real, bef, 0, 0 };
                        ds.Data = new List<int>() { ori, co, rev, realiz, ongo, oSpud, oComp };
                        data.Add(ds);

                    }
                }

                return data;

            }
            catch (Exception e)
            {
                return new List<rkap_chart_ds>();
            }
        }


        public async Task<List<rkap_chart_ds>> GetChartBudgetExploration(int year, string idRegion)
        {
            try
            {
                string query = @"
                           SELECT 
                            rk.well_name,
                            bu.unit_name, 
                            rk.status, 
                            YEAR(rk.spud_date) AS year, 
                            CAST(rk.budget AS DECIMAL(18, 2)) AS budget,
                            CASE 
                                WHEN COUNT(*) > 1 THEN 1 
                                ELSE COUNT(*) 
                            END AS jumlah, 
                            SUM(CASE WHEN w.submitted_by IS NULL THEN d.daily_cost ELSE 0 END) AS ongoing, 
                            SUM(CASE WHEN w.submitted_by IS NOT NULL THEN d.daily_cost ELSE 0 END) AS real, 
                            SUM(CASE WHEN af.is_closed = 1 THEN COALESCE(ap.actual_price, 0) ELSE 0 END) AS totalafe,
                            1 AS type 
                        FROM 
                            rkap_history rk 
                            INNER JOIN business_unit bu ON rk.business_unit_id = bu.id 
                            INNER JOIN field f ON f.id = rk.field_id 
                            LEFT JOIN well w ON rk.well_id = w.id
                            LEFT JOIN afe af ON af.well_id = w.id
                            LEFT JOIN afe_plan ap ON ap.afe_id = af.id
                            LEFT JOIN (
                                SELECT 
                                    well_id,
                                    d.daily_cost
                                FROM 
                                    vw_drilling d
                                    LEFT JOIN well w ON d.well_id = w.id
                                GROUP BY 
                                    well_id,
                                    d.daily_cost
                            ) AS d ON d.well_id = w.id
                        WHERE 
                            YEAR(rk.spud_date) = @year 
                            AND rk.well_classification = 'Exploration'
                            {0}
                        GROUP BY 
                            rk.well_name,
                            bu.id,
                            rk.budget,
                            bu.unit_name, 
                            f.field_name, 
                            rk.status, 
                            YEAR(rk.spud_date)

                        UNION ALL 

                        SELECT 
                            NULL AS well_name,
                            bu.unit_name, 
                            rk.status, 
                            YEAR(rk.spud_date) AS year, 
                            rk.budget AS budget,
                            CASE 
                                WHEN COUNT(*) > 1 THEN 1 
                                ELSE COUNT(*) 
                            END AS jumlah, 
                            0 AS ongoing_cost, 
                            0 AS real_cost, 
                            0 AS totalafe, 
                            2 AS type 
                        FROM 
                            rkap_history rk 
                            INNER JOIN business_unit bu ON rk.business_unit_id = bu.id 
                        WHERE 
                            EXISTS (
                                SELECT 
                                    header_id 
                                FROM 
                                    rkap_history rh 
                                WHERE 
                                    YEAR(rh.spud_date) = @lastYear
                                    AND YEAR(rh.end_date) = @lastYear
                                    AND rk.header_id = rh.header_id
                            ) 
                            AND rk.status IN (1, 2) 
                            {1}
                        GROUP BY 
                            bu.id, 
                            rk.budget,
                            bu.unit_name, 
                            rk.status,
                            YEAR(rk.spud_date)
                                            ";

                string filterRegion = string.IsNullOrEmpty(idRegion) ? "" : "AND rk.business_unit_id = @idRegion";
                string finalQuery = string.Format(query, filterRegion, filterRegion);

                var lastYear = year - 1;
                var parameters = new { year, lastYear, idRegion };
                var dataGrp = await sqlConnection.QueryAsync<chart_rkap>(finalQuery, parameters);

                var unitNames = dataGrp.Select(s => s.unit_name).Distinct().ToList();
                var wellNames = dataGrp.Select(s => s.well_name).Distinct().ToList();

                List<rkap_chart_ds> data = new List<rkap_chart_ds>();
                int currYear = DateTime.Now.Year;

                // 'Original Rkap', 'CO 2019', 'Revised Rkap', 'Realization', 'Outlook Spud','Outlook Complete'
                foreach (var unitName in unitNames)
                {
                    foreach (var wellName in wellNames)
                    {
                        var dataUnit = dataGrp.Where(w => w.unit_name == unitName && w.year == year);
                        rkap_chart_ds ds = new rkap_chart_ds(unitName, wellName);

                        // original
                        int ori = dataUnit.Where(w => w.status == 0 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.budget);
                        int co = dataUnit.Where(w => w.status == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.totalafe);
                        int real = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.real);
                        int rev = dataUnit.Where(w => w.status == 1 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.budget);
                        int ongo = dataUnit.Where(w => w.status == 2 && w.type == 1 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.ongoing);
                        int oSupd = real + ongo;
                        int oComp = real;
                        // before year
                        int bef = dataGrp.Where(w => w.type == 2 && w.unit_name == unitName  && w.well_name == wellName ).Sum(s => s.jumlah);

                        ds.Data = new List<int>() { ori, co, rev, real, ongo, oSupd ,oComp };
                        data.Add(ds);
                    }
                }

                return data;

            }
            catch (Exception e)
            {
                return new List<rkap_chart_ds>();
            }
        }

        public async Task<calendar_response> GetCalendarResource(int year, string IdRegion)
        {
            var result = new calendar_response();

            try
            {
                var query = @"
                SELECT rh.*, rig.name AS rig_name, bu.unit_name AS business_unit_name
                FROM rkap_history rh
                LEFT JOIN rig ON rh.rig_id = rig.id
                LEFT JOIN business_unit bu ON rh.business_unit_id = bu.id
                WHERE YEAR(rh.spud_date) = @year";

                if (!string.IsNullOrEmpty(IdRegion))
                {
                    query += " AND rh.business_unit_id = @IdRegion";
                }

                query += " ORDER BY rh.status";

                var data = await sqlConnection.QueryAsync<vw_rkap_history>(query, new { year, IdRegion });

                var resourceResult = new List<rkap_timeline_resource>(data.Count());
                var eventResult = new List<rkap_timeline_event>(data.Count());

                // Define status names and corresponding colors
                var statusMapping = new Dictionary<int, string>
                {
                    { 0, "RENCANA AWAL" },
                    { 1, "PROYEKSI" },
                    { 2, "REALISASI" }
                };

                var colorMapping = new Dictionary<int, string>
                {
                    { 0, "blue" },
                    { 1, "red" },
                    { 2, "green" }  // Green for "Realisasi"
                };

                foreach (var entry in data)
                {
                    // Add resources for the calendar
                    resourceResult.Add(new rkap_timeline_resource()
                    {
                        Id = entry.id,
                        Area = entry.business_unit_name,
                        HpWd = entry.hp_no + " / " + entry.water_depth,
                        Rig = entry.rig_name,
                        Jadwal = statusMapping.ContainsKey(entry.status) ? statusMapping[entry.status] : "UNKNOWN",
                        eventColor = colorMapping.ContainsKey(entry.status) ? colorMapping[entry.status] : "gray",
                        Well = entry.well_name
                    });

                    // Add events only for the selected year, including Realisasi (status = 2)
                    if (entry.spud_date.Value.Year == year)
                    {
                        eventResult.Add(new rkap_timeline_event()
                        {
                            ResourceId = entry.id,
                            Start = entry.spud_date.Value,
                            End = entry.end_date.Value,
                            Title = entry.well_name
                        });

                        // Log Realisasi to verify if it is being included
                        if (entry.status == 2)
                        {
                            Console.WriteLine($"Realisasi found for {year}: {entry.well_name} (spud_date: {entry.spud_date.Value})");
                        }
                    }
                }

                result.events = eventResult;
                result.resource = resourceResult;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Log exceptions
            }

            return result;
        }

    }
}
