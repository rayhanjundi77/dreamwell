﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class AttachWirRepository : ServiceRespository<attach_wir, vw_attach_wir>
    {
        public AttachWirRepository(DataContext dataContext)
            : base(dataContext)
        {

        }
    }
}
