﻿
namespace Dreamwell.DataAccess
{
    public class DrillFormationsActualRepository : ServiceRespository<drill_formations_actual, vw_drill_formations_actual>
    {
        public DrillFormationsActualRepository(DataContext dataContext)
            :base(dataContext)
        {

        }
    }
}
