﻿namespace Dreamwell.DataAccess
{
    public class CvdPlanRepository:ServiceRespository<cvd_plan, vw_cvd_plan>
    {
        public CvdPlanRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
