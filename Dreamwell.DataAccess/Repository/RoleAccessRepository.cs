﻿namespace Dreamwell.DataAccess
{
    public class RoleAccessRepository : ServiceRespository<role_access, vw_role_access>
    {
        public RoleAccessRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
