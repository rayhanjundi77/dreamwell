﻿namespace Dreamwell.DataAccess
{
    public class DrillingBhaComponentDetailRepository : ServiceRespository<drilling_bha_comp_detail, vw_drilling_bha_comp_detail>
    {
        public DrillingBhaComponentDetailRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
