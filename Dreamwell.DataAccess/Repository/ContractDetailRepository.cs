﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class ContractDetailRepository : ServiceRespository<contract_detail, vw_contract_detail>
    {
        public ContractDetailRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
