﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class BusinessUnitRepository:ServiceRespository<business_unit, vw_business_unit>
    {
        public BusinessUnitRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
