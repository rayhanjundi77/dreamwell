﻿using Dapper;
using Dreamwell.DataAccess.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class CasingRepository : BaseRepository<Casing>
    {
        public CasingRepository(DataContext dataContext) : base(dataContext) { }

        public Casing CheckIfExist(string hole_id, string name)
        {
            return con.QueryFirstOrDefault<Casing>($"SELECT TOP (1) * FROM casing where CONVERT(DECIMAL(7,3), name ) = {name} and hole_id = '{hole_id}'");
        }
    }

}
