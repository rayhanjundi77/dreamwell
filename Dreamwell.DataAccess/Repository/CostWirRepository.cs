﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class CostWirRepository : ServiceRespository<cost_wir, vw_cost_wir>
    {
        public CostWirRepository(DataContext dataContext)
            : base(dataContext)
        {

        }
    }
}
