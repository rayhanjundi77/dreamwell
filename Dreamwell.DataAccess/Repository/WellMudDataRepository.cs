﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class WellMudDataRepository : ServiceRespository<well_mud_data, vw_well_mud_data>
    {
        public WellMudDataRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
