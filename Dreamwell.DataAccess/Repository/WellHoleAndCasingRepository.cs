﻿namespace Dreamwell.DataAccess
{
    public class WellHoleAndCasingRepository : ServiceRespository<well_hole_and_casing, vw_well_hole_and_casing>
    {
        public WellHoleAndCasingRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
