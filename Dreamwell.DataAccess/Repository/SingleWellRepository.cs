﻿using CommonTools.UnitOfMeasurement.Mass;
using Dapper;
using Dreamwell.DataAccess.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class SingleWellRepository : BaseRepository<well>
    {
        public SingleWellRepository(DataContext dataContext) : base(dataContext) { }

        public async Task<List<vw_iadc_analysis_npt>> GetAnalysisByDrillingHoleCasingID(string ID)
        {

            //var sql = $" SELECT r.id, r.owner_id, r. description, r.type, r.total, r.interval " +
            //    $" 			FROM ( " +
            //    $" 	SELECT DISTINCT r.id, r.owner_id, r. description, type = 'NPT', m.total AS total, i.interval AS interval " +
            //    $" FROM iadc r" +
            //    $" INNER JOIN drilling_operation_activity do ON r.id = do.iadc_id" +
            //    $" INNER JOIN drilling_hole_and_casing dhc ON dhc.id = do.drilling_hole_and_casing_id" +
            //    $" INNER JOIN drilling d ON d.id = do.drilling_id" +
            //    $" OUTER APPLY (" +
            //    $" SELECT COUNT(*) AS total" +
            //    $" FROM iadc i" +
            //    $" INNER JOIN drilling_operation_activity do ON r.id = do.iadc_id" +
            //    $" WHERE i.type = 2 AND i.id = r.id AND do.drilling_hole_and_casing_id = dhc.id" +
            //    $" ) AS m" +
            //    $" OUTER APPLY (" +
            //    $" SELECT SUM(DATEDIFF(MINUTE, do.operation_start_date, do.operation_end_date) / 60.0) AS interval" +
            //    $" FROM drilling_operation_activity do" +
            //    $" INNER JOIN drilling AS d ON d.id = do.drilling_id" +
            //    $" WHERE do.iadc_id = r.id AND do.drilling_hole_and_casing_id = dhc.id" +
            //    $" ) AS i" +
            //    $" WHERE r.type = 2 AND dhc.id = '{ID}') AS r";
            var sql = $"select " +
                $"  sum(r.interval) interval, " +
                $"  count(1) total, " +
                $"  type, " +
                $"  description " +
                $"from " +
                $"  (" +
                $"    SELECT " +
                $"      i.description, " +
                $"      type = 'NPT', " +
                $"      case when CONVERT(time, do.operation_end_date) = '23:59:00' then (" +
                $"        DATEDIFF(" +
                $"          MINUTE, do.operation_start_date, " +
                $"          do.operation_end_date" +
                $"        )+ 1" +
                $"      )/ 60.0 else DATEDIFF(" +
                $"        MINUTE, do.operation_start_date, " +
                $"        do.operation_end_date" +
                $"      ) / 60.0 end as interval, " +
                $"      i.id " +
                $"    FROM " +
                $"      iadc i " +
                $"      INNER JOIN drilling_operation_activity do ON i.id = do.iadc_id " +
                $"      inner join drilling_hole_and_casing dhc on do.drilling_hole_and_casing_id = dhc.id" +
                $"    where " +
                $"      i.type = 2 " +
                $"      and dhc.id = '{ID}'" +
                $"  ) as r " +
                $"group by " +
                $"  r.description, " +
                $"  r.type";
            var result = await con.QueryAsync<vw_iadc_analysis_npt>(sql);
            return result.ToList();
        }
    }
}
