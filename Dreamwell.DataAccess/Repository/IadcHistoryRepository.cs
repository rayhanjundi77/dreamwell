﻿namespace Dreamwell.DataAccess
{
    public class IadcHistoryRepository : ServiceRespository<iadc_history, vw_iadc_history>
    {
        public IadcHistoryRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
