﻿namespace Dreamwell.DataAccess
{
    public class DrillingOperationActivityRepository : ServiceRespository<drilling_operation_activity, vw_drilling_operation_activity>
    {
        public DrillingOperationActivityRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
