﻿using Dapper;
using Dreamwell.DataAccess.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class WellHoleRepository : BaseRepository<WellHole>
    {
        public WellHoleRepository(DataContext dataContext) : base(dataContext) { }

        //public async Task<IEnumerable<WellHoleSchemanticResponse>> GetAllByWellId(string wellId)
        //{
        //    var sql = $"select wh.id, hole.id hole_id, hole.name hole_name, hole_type, hole_depth, plan_cost, plan_volume, casing.id casing_id, casing.name casing_name, wc.casing_setting_depth, wc.casing_type, wc.casing_weight, wc.casing_grade, wc.casing_connection, wc.casing_top_of_liner   from well_hole wh  inner join well_casing wc on wc.well_hole_id = wh.id  inner join hole on hole.id = wh.hole_id  inner join casing on casing.id = wc.casing_id where wh.well_id = '{wellId}' ";
        //    return await con.QueryAsync<WellHoleSchemanticResponse>(sql);
        //}
        public async Task<IEnumerable<WellHoleCasing>> GetAllDrilling(string where)
        {
            var sql = $"select * from vw_well_hole_and_casing {where} ";
            return await con.QueryAsync<WellHoleCasing>(sql);
        }
        /*
        public async Task<IEnumerable<vw_well_hole_and_casing>> GetAllByWellID(string wellID)
        {
            var sql = $"select * from vw_well_hole_and_casing where well_id = '{wellID}' ORDER BY created_on ASC";
            return await con.QueryAsync<vw_well_hole_and_casing>(sql);
        }
        */
        public async Task<IEnumerable<vw_well_hole_and_casing>> GetAllByWellID(string wellID)
        {
            var sql = @"
            WITH CTE AS (
                SELECT 
                    *,
                    ROW_NUMBER() OVER (PARTITION BY hole_and_casing_id ORDER BY (CASE WHEN actual_depth IS NOT NULL THEN 0 ELSE 1 END), actual_depth ASC) AS rn
                FROM 
                    vw_well_hole_and_casing
                WHERE 
                    well_id = @WellID
                    AND multicasing IS NULL
            )
            SELECT * 
            FROM CTE
            WHERE rn = 1
            ORDER BY created_on ASC;";

            return await con.QueryAsync<vw_well_hole_and_casing>(sql, new { WellID = wellID });
        }


        public async Task<IEnumerable<vw_well_hole_and_casing>> GetAllByWellId(string recordId)
        {
            var sql = $"select * from vw_well_hole_and_casing where parent_well_hole_casing_id='{recordId}' ORDER BY created_on ASC";
            return await con.QueryAsync<vw_well_hole_and_casing>(sql);
        }
    }
}