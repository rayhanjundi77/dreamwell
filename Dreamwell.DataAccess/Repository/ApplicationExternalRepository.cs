﻿using Dapper;
using Dapper.Contrib.Extensions;
using Dreamwell.DataAccess.Models.Entity;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess.Repository
{
    public class ApplicationExternalRepository : IDisposable
    {
        SqlConnection sqlConnection;
        string defQuery;

        public ApplicationExternalRepository()
        {
            sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connStr"].ConnectionString);
            defQuery = @"SELECT ae.* FROM [dbo].[application_external] ae ";
        }

        public void Dispose()
        {
            sqlConnection.Dispose();
        }

        public async Task<bool> Update(ApplicationExternal model)
        {
            model.modified_on = DateTime.Now;
            model.last_login = DateTime.Now;

            return await sqlConnection.UpdateAsync<ApplicationExternal>(model);
        }

        public async Task<ApplicationExternal> GetById(string clientId)
        {
            return await sqlConnection.QueryFirstOrDefaultAsync<ApplicationExternal>($"{defQuery} WHERE ae.client_id = @clientid", new { clientId });
        }
    }
}
