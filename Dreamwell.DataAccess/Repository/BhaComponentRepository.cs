﻿namespace Dreamwell.DataAccess
{
    public class BhaComponentRepository : ServiceRespository<bha_component, vw_bha_component>
    {
        public BhaComponentRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
