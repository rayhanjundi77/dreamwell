﻿namespace Dreamwell.DataAccess
{
    public class WellInformationRepository : ServiceRespository<well_information, vw_well_information>
    {
        public WellInformationRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
