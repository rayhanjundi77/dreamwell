﻿namespace Dreamwell.DataAccess
{
    public class DrillingOperationRepository : ServiceRespository<drilling_operation, vw_drilling_operation>
    {
        public DrillingOperationRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
