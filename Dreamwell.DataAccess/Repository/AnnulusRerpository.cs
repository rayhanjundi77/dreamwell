﻿using Dreamwell.DataAccess.Models.Entity;

namespace Dreamwell.DataAccess
{
    public class AnnulusRepository : ServiceRespository<annulus, vw_annulus>
    {
        public AnnulusRepository(DataContext dataContext)
            : base(dataContext)
        {

        }

    }
}
