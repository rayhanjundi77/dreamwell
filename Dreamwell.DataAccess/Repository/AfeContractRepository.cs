﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.DataAccess
{
    public class AfeContractRepository : ServiceRespository<afe_contract, vw_afe_contract>
    {
        public AfeContractRepository(DataContext dataContext)
            :base(dataContext)
        {

        }

    }
}
