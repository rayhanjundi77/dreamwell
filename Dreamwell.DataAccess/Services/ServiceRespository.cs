﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;
using NLog;
using System.Reflection;
using Newtonsoft.Json;
using System.Transactions;
using static CommonTools.Filterable;
using CommonTools;
using CommonTools.Helper;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;

namespace Dreamwell.DataAccess
{
    public class ServiceRespository<TEntity, TEntityView> : IRepository<TEntity, TEntityView>
    {
        protected static Logger appLogger = LogManager.GetCurrentClassLogger();
        protected UserContext _services;
        private TEntity appEntity;
        public ServiceRespository(DataContext dataContext)
        {
            try
            {
                _services = new UserContext(dataContext);
                appEntity = Activator.CreateInstance<TEntity>();
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }

        }


        public virtual List<TEntity> GetAll()
        {
            var sql = Sql.Builder.Append("WHERE");
            #region Check RoleAccess
            sql.Append(" dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            return _services.db.Fetch<TEntity>(sql);
        }

        public List<TEntity> GetAll(Sql sqlFilter)
        {
            var sql = Sql.Builder.Append("");
            if (string.IsNullOrEmpty(sqlFilter?.SQL))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion
            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
            }

            return _services.db.Fetch<TEntity>(sql);
        }

        public List<TEntity> GetAll(string sqlFilter, params object[] args)
        {
            var sql = Sql.Builder.Append("");

            if (string.IsNullOrEmpty(sqlFilter))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter, args);
            }

            return _services.db.Fetch<TEntity>(sql);
        }

        public virtual Page<TEntity> GetAll(long page, long itemsPerPage)
        {
            var sql = Sql.Builder.Append("WHERE");
            #region Check RoleAccess
            sql.Append(" dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            return _services.db.Page<TEntity>(page, itemsPerPage, sql);
        }

        public virtual Page<TEntity> GetAll(long page, long itemsPerPage, string sqlFilter, params object[] args)
        {
            var sql = Sql.Builder.Append("");
            if (string.IsNullOrEmpty(sqlFilter))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter, args);
            }

            return _services.db.Page<TEntity>(page, itemsPerPage, sql);
        }

        public virtual Page<TEntity> GetAll(long page, long itemsPerPage, Sql sqlFilter)
        {
            var sql = Sql.Builder.Append("");
            if (string.IsNullOrEmpty(sqlFilter?.SQL))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion
            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
            }
            return _services.db.Page<TEntity>(page, itemsPerPage, sql);
        }

        public virtual TEntity GetById(string id)
        {
            var sql = Sql.Builder.Append("WHERE id=@0 ", id);
            #region Check RoleAccess
            sql.Append(" AND dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            return _services.db.FirstOrDefault<TEntity>(sql);
        }

        public TEntity GetFirstOrDefault(string sqlFilter, params object[] args)
        {
            var sql = Sql.Builder.Append("");
            if (string.IsNullOrEmpty(sqlFilter))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter, args);
            }

            return _services.db.FirstOrDefault<TEntity>(sql);
        }

        public TEntity GetFirstOrDefault(Sql sqlFilter)
        {
            var sql = Sql.Builder.Append("");
            if (string.IsNullOrEmpty(sqlFilter?.SQL))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion
            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, id, owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
            }
            return _services.db.FirstOrDefault<TEntity>(sql);
        }

        public TEntityView GetViewFirstOrDefault(string sqlFilter, params object[] args)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
            if (string.IsNullOrEmpty(sqlFilter))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter, args);
            }
            return _services.db.FirstOrDefault<TEntityView>(sql);
        }


        public TEntityView GetViewFirstOrDefault(Sql sqlFilter)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
            if (string.IsNullOrEmpty(sqlFilter?.SQL))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion
            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
            }
            return _services.db.FirstOrDefault<TEntityView>(sql);
        }

        public virtual List<TEntityView> GetViewAll()
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
            #region Check RoleAccess
            sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            return _services.db.Fetch<TEntityView>(sql);
        }

        public virtual List<TEntityView> GetViewAll(Sql sqlFilter)
        {
            var result = new List<TEntityView>();
            Sql sql = Sql.Builder.Append("");
            try
            {
                sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
                if (sqlFilter == null || string.IsNullOrWhiteSpace(sqlFilter.SQL))
                {
                    sql.Append(" WHERE 1=1 ");
                    #region Check RoleAccess
                    sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                        this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                    #endregion
                }
                else
                {
                    #region Check RoleAccess
                    sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                        this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                    #endregion

                    sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
                }
                return _services.db.Fetch<TEntityView>(sql);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                appLogger.Debug(sql.SQL);
            }
            return result;
        }

        public virtual List<TEntityView> GetViewAll(string sqlFilter, params object[] args)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
            if (string.IsNullOrEmpty(sqlFilter))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion
                sql.Append(sqlFilter, args);
            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter, args);
            }
            return _services.db.Fetch<TEntityView>(sql);
        }

        public virtual Page<TEntityView> GetViewPerPage(long page, long itemsPerPage)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
            #region Check RoleAccess
            sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            return _services.db.Page<TEntityView>(page, itemsPerPage, sql);
        }


        /// <param name="sqlFilter">This parameter only WHERE to specify filtering data.</param>
        public virtual Page<TEntityView> GetViewPerPage(long page, long itemsPerPage, string sqlFilter, params object[] args)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
            try
            {
                if (string.IsNullOrEmpty(sqlFilter))
                {
                    sql.Append(" WHERE 1=1 ");
                    #region Check RoleAccess
                    sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                        this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                    #endregion

                }
                else
                {
                    #region Check RoleAccess
                    sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                        this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                    #endregion

                    sql.Append(sqlFilter, args);
                }

                if (itemsPerPage == 0)
                {
                    itemsPerPage = int.MaxValue;
                }
                appLogger.Debug(sql.SQL);
                appLogger.Debug(sql.Arguments);
                return _services.db.Page<TEntityView>(page, itemsPerPage, sql);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return null;

        }

        /// <param name="sqlFilter">This parameter only WHERE to specify filtering data.</param>
        public virtual Page<TEntityView> GetViewAll(long page, long itemsPerPage, Sql sqlFilter)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
            #region Check RoleAccess
            sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
            return _services.db.Page<TEntityView>(page, itemsPerPage, sql);
        }

        public virtual TEntityView GetViewById(string id)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
            sql.Append("WHERE r.id=@0 ", id);
            #region Check RoleAccess
            sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            return _services.db.FirstOrDefault<TEntityView>(sql);
        }

        public virtual bool Remove(string id)
        {
            var success = false;
            try
            {
                success = _services.dataContext.DeleteEntity<TEntity>(id);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return success;
        }

        public virtual bool RemoveAll(params string[] ids)
        {
            var success = false;
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    if (ids.Length == 0) return false;
                    success = (ids.Length > 0);
                    foreach (var id in ids)
                    {
                        success &= _services.dataContext.DeleteEntity<TEntity>(id);
                    }
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    appLogger.Error(ex);
                    throw;
                }
            }

            return success;
        }



        public virtual bool SaveEntity(TEntity record, ref bool isNew)
        {
            var success = false;
            try
            {
                TEntity recordMap = MapToEntity(record, ref isNew);
                success = _services.dataContext.SaveEntity<TEntity>(recordMap, isNew);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return success;
        }

        public virtual bool SaveEntity(TEntity record, ref bool isNew, string ownershipId)
        {
            var success = false;
            try
            {
                TEntity recordMap = MapToEntity(record, ref isNew);
                success = _services.dataContext.SaveEntity<TEntity>(recordMap, isNew, ownershipId);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return success;
        }



        public virtual bool SaveEntity(TEntity record, ref bool isNew, Action<TEntity> actionContinuation = null)
        {
            var success = false;
            try
            {
                TEntity recordMap = MapToEntity(record, ref isNew);
                success = _services.dataContext.SaveEntity<TEntity>(recordMap, isNew);
                actionContinuation?.Invoke(recordMap);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return success;
        }

        public virtual bool SaveEntity(TEntity record, ref bool isNew, string ownershipId, Action<TEntity> actionContinuation = null)
        {
            var success = false;
            try
            {
                TEntity recordMap = MapToEntity(record, ref isNew);
                success = _services.dataContext.SaveEntity<TEntity>(recordMap, isNew, ownershipId);
                actionContinuation?.Invoke(recordMap);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return success;
        }



        public TEntity MapToEntity(TEntity record, ref bool IsNew)
        {
            string log = "";
            var id = "";

            var result = Activator.CreateInstance<TEntity>();
            var source = ((IBaseRecord)record);
            if (source == null) return result;
            if (!string.IsNullOrEmpty(source.id))
            {
                id = source.id;
            }


            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    IsNew = true;
                    result = Activator.CreateInstance<TEntity>();
                    log = String.Format("New {0} record", typeof(TEntity).Name);
                    appLogger.Debug(log);

                }
                else
                {
                    result = _services.db.FirstOrDefault<TEntity>(" WHERE id = @0 ", id);
                    appLogger.Debug("Record Map Entity");
                    appLogger.Debug(JsonConvert.SerializeObject(result));
                    IsNew = (result == null);
                    if (IsNew)
                    {
                        result = Activator.CreateInstance<TEntity>();
                        log = String.Format("New {0} record", typeof(TEntity).Name);
                        appLogger.Debug(log);
                    }
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex.ToString());
            }
            finally
            {
                appLogger.Debug(_services.db.LastCommand);
            }


            PropertyInfo[] destinationProperties = result.GetType().GetProperties();

            if (IsNew)
            {
                foreach (PropertyInfo destinationPi in destinationProperties)
                {
                    PropertyInfo sourcePi = source.GetType().GetProperty(destinationPi.Name);

                    if (destinationPi.Name == "organization_id")
                    {
                        destinationPi.SetValue(result, _services.dataContext.OrganizationId, null);
                        continue;
                    }
                    if (sourcePi.GetValue(source, null) == null) continue;
                    destinationPi.SetValue(result, sourcePi.GetValue(source, null), null);

                }
            }
            else
            {
                foreach (PropertyInfo destinationPi in destinationProperties)
                {
                    PropertyInfo sourcePi = source.GetType().GetProperty(destinationPi.Name);
                    if (sourcePi.GetValue(source, null) == null) continue;

                    destinationPi.SetValue(result, sourcePi.GetValue(source, null), null);
                }
            }

            return result;
        }

        public virtual DataTablesResponse GetListDataTables(DataTableRequest DataTableRequest, Sql sqlOptionalFilter = null)
        {
            DataTablesResponse result = new DataTablesResponse(DataTableRequest.Draw, new List<TEntityView>(), 0, 0); ;
            var parameters = new List<String>();
            int unfilteredCount = 0;
            var log = "";

            try
            {
                Sql qry = null;
                object appEntity = Activator.CreateInstance<TEntity>();
                if (appEntity != null) qry = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView());
                if (qry == null) return result;

                if (sqlOptionalFilter == null)
                {
                    qry.Append(" WHERE r.id IS NOT NULL ");
                }
                else
                {
                    qry.Append(sqlOptionalFilter.SQL, sqlOptionalFilter.Arguments);
                }

                // Role access filter
                qry.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    _services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());

                #region Get unfiltered count

                using (var db = new dreamwellRepo())
                {
                    try
                    {
                        var qryUC = String.Format("SELECT COUNT(*) FROM ( {0} ) AS dt", qry.SQL);
                        unfilteredCount = db.ExecuteScalar<int>(qryUC, qry.Arguments);
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex.ToString());
                    }
                    finally
                    {
                        appLogger.Debug(db.LastCommand);
                    }
                }

                #endregion

                var sql = Sql.Builder.Append(qry.SQL, qry.Arguments);

                var viewColumns = ((IEntity)appEntity).GetDefaultViewColumns();
                if (!String.IsNullOrEmpty(DataTableRequest.Search.Value))
                {
                    #region Set single parameter search

                    parameters.Clear();
                    parameters.Add(String.Format("%{0}%", DataTableRequest.Search.Value));
                    var filteredColumnCount = 0;

                    foreach (Dreamwell.Infrastructure.DataTables.DataTableColumn col in DataTableRequest.Columns)
                    {
                        if (!col.Searchable) continue;
                        if (!viewColumns.ContainsKey(col.Data)) continue;

                        if (filteredColumnCount == 0)
                        {
                            sql.Append(" AND ( " + viewColumns[col.Data] + " LIKE @0 ",
                                String.Format("%{0}%", DataTableRequest.Search.Value));
                        }
                        else
                        {
                            sql.Append(" OR " + viewColumns[col.Data] + " LIKE @0 ",
                                String.Format("%{0}%", DataTableRequest.Search.Value));
                        }

                        filteredColumnCount++;
                    }

                    if (filteredColumnCount > 0)
                    {
                        sql.Append(" ) ");
                    }

                    #endregion
                }
                else
                {
                    #region Set multi parameters search

                    parameters.Clear();
                    var filteredColumnCount = 0;

                    var filteredColumns = DataTableRequest.Columns.Where(o => o.Searchable == true);
                    foreach (Dreamwell.Infrastructure.DataTables.DataTableColumn col in filteredColumns)
                    {
                        if (!col.Searchable || String.IsNullOrEmpty(col.Search.Value)) continue;
                        if (!viewColumns.ContainsKey(col.Data)) continue;

                        parameters.Add(String.Format("%{0}%", col.Search.Value));
                        sql.Append(" AND " + viewColumns[col.Data] + " LIKE @0 ",
                            String.Format("%{0}%", DataTableRequest.Search.Value));

                        filteredColumnCount++;
                    }

                    #endregion
                }

                #region Set row order

                sql.Append(" ORDER BY ");
                var viewOrders = ((IEntity)appEntity).GetDefaultViewOrders();
                if (viewOrders?.Count == 0)
                {
                    sql.Append(" r.id ASC ");
                }
                foreach (var col in viewOrders)
                {
                    log = String.Format("Default sorted columns = {0}", col.Key);
                    appLogger.Debug(log);

                    sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Key], col.Value));
                }

                var sortedColumns = DataTableRequest.Columns.Where(o => o.Orderable == true);
                if (sortedColumns != null && sortedColumns.Count() > 0)
                {
                    foreach (var col in sortedColumns.OrderBy(o => o.OrderNumber))
                    {
                        log = String.Format("DataTables sorted columns = {0}", col.Data);
                        appLogger.Debug(log);
                        var orderBy = DataTableRequest.Order.Where(o => o.Column == DataTableRequest.Columns.FindIndex(x => x.Data.Contains(col.Data))).FirstOrDefault();
                        if (orderBy != null)
                        {
                            if (!viewColumns.ContainsKey(col.Data)) continue;
                            sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Data],
                                orderBy.Dir));
                        }
                    }
                }
                else
                {
                    foreach (var col in viewOrders)
                    {
                        log = String.Format("Default sorted columns = {0}", col.Key);
                        appLogger.Debug(log);

                        sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Key], col.Value));
                    }
                }

                //sementara
                sql.Append(" r.id ASC ");

                #endregion

                using (var db = new dreamwellRepo())
                {
                    appLogger.Debug(sql.SQL);
                    appLogger.Debug(sql.Arguments);
                    var rowPerPage = DataTableRequest.Length < 1 ? 1 : DataTableRequest.Length;
                    var pageNumber = (DataTableRequest.Start / rowPerPage) + 1;


                    try
                    {
                        var rows = db.Page<TEntityView>(pageNumber, rowPerPage, sql);
                        appLogger.Debug(db.LastCommand);
                        result = new DataTablesResponse(DataTableRequest.Draw, rows.Items,
                            (int)rows.TotalItems, unfilteredCount);
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex.ToString());
                    }
                    finally
                    {
                        appLogger.Debug(db.LastCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex.ToString());
            }

            return result;
        }

        public virtual DataTablesResponse GetListDataTablesByQuery(DataTableRequest DataTableRequest, string sqlQuery, string sqlDefaultOrderByOptional, params object[] args)
        {
            DataTablesResponse result = new DataTablesResponse(DataTableRequest.Draw, new List<TEntityView>(), 0, 0);
            var parameters = new List<String>();
            int unfilteredCount = 0;
            var log = "";
            bool isOrderedByDatatables = false;

            try
            {
                Sql qry = null;
                object appEntity = Activator.CreateInstance<TEntity>();
                if (string.IsNullOrEmpty(sqlQuery)) throw new Exception(" Please Fill Sql Query View ");

                qry = Sql.Builder.Append(sqlQuery, args);
                if (qry == null) return result;

                // Role access filter
                qry.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    _services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());

                #region Get unfiltered count

                using (var db = new dreamwellRepo())
                {
                    try
                    {
                        var qryUC = String.Format("SELECT COUNT(*) FROM ( {0} ) AS dt", qry.SQL);
                        unfilteredCount = db.ExecuteScalar<int>(qryUC, qry.Arguments);
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex.ToString());
                    }
                    finally
                    {
                        appLogger.Debug(db.LastCommand);
                    }
                }

                #endregion

                var sql = Sql.Builder.Append(qry.SQL, qry.Arguments);

                var viewColumns = ((IEntity)appEntity).GetDefaultViewColumns();
                if (!String.IsNullOrEmpty(DataTableRequest.Search.Value))
                {
                    #region Set single parameter search

                    parameters.Clear();
                    parameters.Add(String.Format("%{0}%", DataTableRequest.Search.Value));
                    var filteredColumnCount = 0;

                    foreach (Dreamwell.Infrastructure.DataTables.DataTableColumn col in DataTableRequest.Columns)
                    {
                        if (!col.Searchable) continue;
                        if (!viewColumns.ContainsKey(col.Data)) continue;

                        if (filteredColumnCount == 0)
                        {
                            sql.Append(" AND ( " + viewColumns[col.Data] + " LIKE @0 ",
                                String.Format("%{0}%", DataTableRequest.Search.Value));
                        }
                        else
                        {
                            sql.Append(" OR " + viewColumns[col.Data] + " LIKE @0 ",
                                String.Format("%{0}%", DataTableRequest.Search.Value));
                        }

                        filteredColumnCount++;
                    }

                    if (filteredColumnCount > 0)
                    {
                        sql.Append(" ) ");
                    }

                    #endregion
                }
                else
                {
                    #region Set multi parameters search

                    parameters.Clear();
                    var filteredColumnCount = 0;

                    var filteredColumns = DataTableRequest.Columns.Where(o => o.Searchable == true);
                    foreach (Dreamwell.Infrastructure.DataTables.DataTableColumn col in filteredColumns)
                    {
                        if (!col.Searchable || String.IsNullOrEmpty(col.Search.Value)) continue;
                        if (!viewColumns.ContainsKey(col.Data)) continue;

                        parameters.Add(String.Format("%{0}%", col.Search.Value));
                        sql.Append(" AND " + viewColumns[col.Data] + " LIKE @0 ",
                            String.Format("%{0}%", DataTableRequest.Search.Value));

                        filteredColumnCount++;
                    }

                    #endregion
                }

                #region Set row order

                sql.Append(" ORDER BY ");



                if (DataTableRequest.Order == null || DataTableRequest.Order.Count() == 0)
                {
                    if (!string.IsNullOrEmpty(sqlDefaultOrderByOptional))
                    {
                        sql.Append(string.Format("{0} , ", sqlDefaultOrderByOptional));
                        //  sql.Append(string.Format("{0} ", sqlDefaultOrderByOptional));
                    }
                }


                var sortedColumns = DataTableRequest.Columns.Where(o => o.Orderable == true).ToList();
                if (sortedColumns != null && sortedColumns.Count() > 0)
                {

                    for (var i = 0; i < sortedColumns.Count(); i++)
                    {
                        var col = DataTableRequest.Columns[i];

                        var orderBy = DataTableRequest.Order.Where(o => o.Column == i).FirstOrDefault();
                        if (orderBy != null)
                        {
                            isOrderedByDatatables = true;
                            log = String.Format("DataTables sorted columns = {0}", col.Data);
                            appLogger.Debug(log);
                            if (!viewColumns.ContainsKey(col.Data)) continue;
                            sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Data],
                                orderBy.Dir));
                        }
                    }

                    //if (!isOrderedByDatatables && !string.IsNullOrEmpty(sqlOrderByOptional))
                    //    sql.Append(string.Format("{0} , ", sqlOrderByOptional));


                    //foreach (var col in sortedColumns)
                    //{
                    //    log = String.Format("DataTables sorted columns = {0}", col.Data);
                    //    appLogger.Debug(log);
                    //    var orderBy = DataTableRequest.Order.Where(o => o.Column == col.OrderNumber).FirstOrDefault();
                    //    if (orderBy != null)
                    //    {
                    //        if (!viewColumns.ContainsKey(col.Data)) continue;
                    //        sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Data],
                    //            orderBy.Dir));
                    //    }
                    //}
                }
                else
                {
                    var viewOrders = ((IEntity)appEntity).GetDefaultViewOrders();
                    foreach (var col in viewOrders)
                    {
                        log = String.Format("Default sorted columns [else condition] = {0}", col.Key);
                        appLogger.Debug(log);

                        sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Key], col.Value));
                    }
                }

                sql.Append(" r.id ASC ");



                #endregion

                using (var db = new dreamwellRepo())
                {
                    var rowPerPage = DataTableRequest.Length < 1 ? 1 : DataTableRequest.Length;
                    var pageNumber = (DataTableRequest.Start / rowPerPage) + 1;


                    try
                    {
                        var rows = db.Page<TEntityView>(pageNumber, rowPerPage, sql);
                        result = new DataTablesResponse(DataTableRequest.Draw, rows.Items,
                            (int)rows.TotalItems, unfilteredCount);
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex.ToString());
                    }
                    finally
                    {
                        appLogger.Debug(db.LastCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex.ToString());
            }

            return result;
        }

        public virtual DataTablesResponse GetListDataTablesByQueryFilter(DataTableRequest DataTableRequest, string sqlQuery, string sqlDefaultOrderByOptional, params object[] args)
        {
            DataTablesResponse result = new DataTablesResponse(DataTableRequest.Draw, new List<TEntityView>(), 0, 0);
            var parameters = new List<String>();
            int unfilteredCount = 0;
            var log = "";
            bool isOrderedByDatatables = false;

            try
            {
                Sql qry = null;
                object appEntity = Activator.CreateInstance<TEntity>();
                if (string.IsNullOrEmpty(sqlQuery)) throw new Exception(" Please Fill Sql Query View ");

                qry = Sql.Builder.Append(sqlQuery, args);
                if (qry == null) return result;

                // Role access filter
                qry.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    _services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());

                #region Get unfiltered count

                using (var db = new dreamwellRepo())
                {
                    try
                    {
                        var qryUC = String.Format("SELECT COUNT(*) FROM ( {0} ) AS dt", qry.SQL);
                        unfilteredCount = db.ExecuteScalar<int>(qryUC, qry.Arguments);
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex.ToString());
                    }
                    finally
                    {
                        appLogger.Debug(db.LastCommand);
                    }
                }

                #endregion

                var sql = Sql.Builder.Append(qry.SQL, qry.Arguments);

                var viewColumns = ((IEntity)appEntity).GetDefaultViewColumns();
                if (!String.IsNullOrEmpty(DataTableRequest.Search.Value))
                {
                    #region Set single parameter search

                    parameters.Clear();
                    parameters.Add(String.Format("%{0}%", DataTableRequest.Search.Value));
                    var filteredColumnCount = 0;

                    foreach (Dreamwell.Infrastructure.DataTables.DataTableColumn col in DataTableRequest.Columns)
                    {
                        if (!col.Searchable) continue;
                        if (!viewColumns.ContainsKey(col.Data)) continue;

                        if (filteredColumnCount == 0)
                        {
                            sql.Append(" AND ( " + viewColumns[col.Data] + " LIKE @0 ",
                                String.Format("%{0}%", DataTableRequest.Search.Value));
                        }
                        else
                        {
                            sql.Append(" OR " + viewColumns[col.Data] + " LIKE @0 ",
                                String.Format("%{0}%", DataTableRequest.Search.Value));
                        }

                        filteredColumnCount++;
                    }

                    if (filteredColumnCount > 0)
                    {
                        sql.Append(" ) ");
                    }

                    #endregion
                }
                else
                {
                    #region Set multi parameters search

                    parameters.Clear();
                    var filteredColumnCount = 0;

                    var filteredColumns = DataTableRequest.Columns.Where(o => o.Searchable == true);
                    foreach (Dreamwell.Infrastructure.DataTables.DataTableColumn col in filteredColumns)
                    {
                        if (!col.Searchable || String.IsNullOrEmpty(col.Search.Value)) continue;
                        if (!viewColumns.ContainsKey(col.Data)) continue;

                        parameters.Add(String.Format("%{0}%", col.Search.Value));
                        sql.Append(" AND " + viewColumns[col.Data] + " LIKE @0 ",
                            String.Format("%{0}%", DataTableRequest.Search.Value));

                        filteredColumnCount++;
                    }

                    #endregion
                }

                #region Set row order

                sql.Append(" ORDER BY ");



                if (DataTableRequest.Order == null || DataTableRequest.Order.Count() == 0)
                {
                    if (!string.IsNullOrEmpty(sqlDefaultOrderByOptional))
                    {
                        //sql.Append(string.Format("{0} , ", sqlDefaultOrderByOptional));
                        sql.Append(string.Format("{0} ", sqlDefaultOrderByOptional));
                    }
                }


                var sortedColumns = DataTableRequest.Columns.Where(o => o.Orderable == true).ToList();
                if (sortedColumns != null && sortedColumns.Count() > 0)
                {

                    for (var i = 0; i < sortedColumns.Count(); i++)
                    {
                        var col = DataTableRequest.Columns[i];

                        var orderBy = DataTableRequest.Order.Where(o => o.Column == i).FirstOrDefault();
                        if (orderBy != null)
                        {
                            isOrderedByDatatables = true;
                            log = String.Format("DataTables sorted columns = {0}", col.Data);
                            appLogger.Debug(log);
                            if (!viewColumns.ContainsKey(col.Data)) continue;
                            sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Data],
                                orderBy.Dir));
                        }
                    }

                    //if (!isOrderedByDatatables && !string.IsNullOrEmpty(sqlOrderByOptional))
                    //    sql.Append(string.Format("{0} , ", sqlOrderByOptional));


                    //foreach (var col in sortedColumns)
                    //{
                    //    log = String.Format("DataTables sorted columns = {0}", col.Data);
                    //    appLogger.Debug(log);
                    //    var orderBy = DataTableRequest.Order.Where(o => o.Column == col.OrderNumber).FirstOrDefault();
                    //    if (orderBy != null)
                    //    {
                    //        if (!viewColumns.ContainsKey(col.Data)) continue;
                    //        sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Data],
                    //            orderBy.Dir));
                    //    }
                    //}
                }
                else
                {
                    var viewOrders = ((IEntity)appEntity).GetDefaultViewOrders();
                    foreach (var col in viewOrders)
                    {
                        log = String.Format("Default sorted columns [else condition] = {0}", col.Key);
                        appLogger.Debug(log);

                        sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Key], col.Value));
                    }
                }

                sql.Append(" r.created_on,r.id ASC ");

                #endregion

                using (var db = new dreamwellRepo())
                {
                    var rowPerPage = DataTableRequest.Length < 1 ? 1 : DataTableRequest.Length;
                    var pageNumber = (DataTableRequest.Start / rowPerPage) + 1;


                    try
                    {
                        var rows = db.Page<TEntityView>(pageNumber, rowPerPage, sql);
                        result = new DataTablesResponse(DataTableRequest.Draw, rows.Items,
                            (int)rows.TotalItems, unfilteredCount);
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex.ToString());
                    }
                    finally
                    {
                        appLogger.Debug(db.LastCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex.ToString());
            }

            return result;
        }


        public Sql MultipleFilterable(FilterDataSource filter)
        {
            if (filter.Filters.Count == 0) return null;

            object appEntity = Activator.CreateInstance<TEntity>();
            if (appEntity == null) return null;
            var viewColumns = ((IEntity)appEntity).GetDefaultViewColumns();
            Sql result = Sql.Builder.Append("  ");
            if (filter.IsGroup() && filter.Filters.Count > 0)
            {
                var filterGroupLogic = filter.Logic.ToUpper();
                result.Append(" AND ("); //open ex: AND (
                var totalFilteredColumn = 0;
                foreach (var f in filter.Filters)
                {
                    if (f.IsGroup() && f.Filters.Count > 0)
                    {
                        var b = MultipleFilterableInner(f);
                        if (b != null)
                        {
                            if (totalFilteredColumn == 0)
                                result.Append(string.Format(" {0} ", b.SQL), b.Arguments);
                            else
                                result.Append(string.Format(" {0} {1} ", filterGroupLogic, b.SQL), b.Arguments);
                            totalFilteredColumn++;
                        }
                    }
                    else
                    {
                        if (!viewColumns.ContainsKey(f.Field)) continue;
                        var b = MultipleFilterableInner(f);
                        if (b != null)
                        {
                            if (totalFilteredColumn == 0)
                                result.Append(string.Format(" {0} ", b.SQL), b.Arguments);
                            else
                                result.Append(string.Format(" {0} {1} ", filterGroupLogic, b.SQL), b.Arguments);
                            totalFilteredColumn++;
                        }
                    }

                }
                result.Append(" ) ");//close open ex: AND ( col1=1 )
            }
            else
            {
                if (!viewColumns.ContainsKey(filter.Field)) return null;

                switch (filter.OperatorEnum())
                {
                    case EnumOperator.isempty:
                    case EnumOperator.isnull:
                    case EnumOperator.isnotempty:
                    case EnumOperator.isnotnull:
                        result.Append(string.Format(" {0} {1} {2} ", filter.Logic, viewColumns[filter.Field], filter.OperatorToSql()));
                        break;
                    default:
                        result.Append(string.Format(" {0} {1} {2} @0", filter.Logic, viewColumns[filter.Field], filter.OperatorToSql()),
                            filter.OperatorEnum() == EnumOperator.contains ? string.Format("%{0}%", filter.Value) : filter.Value);
                        break;
                }


            }
            return result;
        }



        private Sql MultipleFilterableInner(FilterDataSource filter)
        {
            object appEntity = Activator.CreateInstance<TEntity>();
            if (appEntity == null) return null;

            var viewColumns = ((IEntity)appEntity).GetDefaultViewColumns();
            Sql result = Sql.Builder.Append("  ");
            if (filter.IsGroup() && filter.Filters.Count > 0)
            {
                var filterGroupLogic = filter.Logic.ToUpper();
                result.Append(" ("); //open ex: AND (
                var totalFilteredColumn = 0;
                foreach (var f in filter.Filters)
                {
                    if (f.IsGroup() && f.Filters.Count > 0)
                    {
                        var b = MultipleFilterableInner(f);
                        if (b != null)
                        {
                            if (totalFilteredColumn == 0)
                                result.Append(string.Format(" {0} ", b.SQL), b.Arguments);
                            else
                                result.Append(string.Format(" {0} {1} ", filterGroupLogic, b.SQL), b.Arguments);
                            totalFilteredColumn++;
                        }
                    }
                    else
                    {
                        if (!viewColumns.ContainsKey(f.Field)) continue;
                        var b = MultipleFilterableInner(f);

                        if (b != null)
                        {
                            appLogger.Info("SQL :");
                            appLogger.Debug(b.SQL);
                            appLogger.Debug(string.Join(",", b.Arguments));

                            if (totalFilteredColumn == 0)
                                result.Append(string.Format(" {0} ", b.SQL), b.Arguments);
                            else
                                result.Append(string.Format(" {0} {1} ", filterGroupLogic, b.SQL), b.Arguments);
                            totalFilteredColumn++;
                        }
                    }

                }
                result.Append(" ) ");//close open ex: AND ( col1=1 )
            }
            else
            {
                if (!viewColumns.ContainsKey(filter.Field)) return null;
                switch (filter.OperatorEnum())
                {
                    case EnumOperator.isempty:
                    case EnumOperator.isnull:
                    case EnumOperator.isnotempty:
                    case EnumOperator.isnotnull:
                        result.Append(string.Format(" {0} {1} {2} ", filter.Logic, viewColumns[filter.Field], filter.OperatorToSql()));
                        break;
                    default:
                        result.Append(string.Format(" {0} {1} {2} @0", filter.Logic, viewColumns[filter.Field], filter.OperatorToSql()),
                            filter.OperatorEnum() == EnumOperator.contains ? string.Format("%{0}%", filter.Value) : filter.Value);
                        break;
                }


            }
            return result;
        }





    }
}
