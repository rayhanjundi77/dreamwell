﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;
using NLog;
using System.Reflection;
using Newtonsoft.Json;
using System.Transactions;
using static CommonTools.Filterable;
using CommonTools;
using CommonTools.Helper;
using DataTables.Mvc;
using Dreamwell.Infrastructure.DataTables;

namespace Dreamwell.DataAccess
{
    public class ServiceRespositoryViewModel<TEntityViewModel> : IRepositoryViewModel<TEntityViewModel>
    {
        protected static Logger appLogger = LogManager.GetCurrentClassLogger();
        protected UserContext _services;
        private TEntityViewModel appEntity;
        protected object[] parameterArgs;

        public ServiceRespositoryViewModel(DataContext dataContext, params object[] parameterArgs)
        {
            try
            {
                _services = new UserContext(dataContext);
                this.parameterArgs = parameterArgs;
                appEntity = Activator.CreateInstance<TEntityViewModel>();
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }

        }

        public List<TEntityViewModel> GetViewAll()
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(),parameterArgs);
            appLogger.Error(sql.SQL);
            appLogger.Error(sql.Arguments);
            #region Check RoleAccess
            sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            return _services.db.Fetch<TEntityViewModel>(sql);
        }

        public List<TEntityViewModel> GetViewAll(Sql sqlFilter)
        {
            var result = new List<TEntityViewModel>();
            Sql sql = Sql.Builder.Append("");
            try
            {
                sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(), parameterArgs);
                if (sqlFilter == null || string.IsNullOrWhiteSpace(sqlFilter.SQL))
                {
                    sql.Append(" WHERE 1=1 ");
                    #region Check RoleAccess
                    sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                        this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                    #endregion
                }
                else
                {
                    #region Check RoleAccess
                    sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                        this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                    #endregion

                    sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
                }
                return _services.db.Fetch<TEntityViewModel>(sql);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                appLogger.Debug(sql.SQL);
            }
            return result;
        }

        public List<TEntityViewModel> GetViewAll(string sqlFilter, params object[] args)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(), parameterArgs);
            if (string.IsNullOrEmpty(sqlFilter))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion
                sql.Append(sqlFilter, args);
            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter, args);
            }
            appLogger.Error(sql.SQL);
            appLogger.Error(sql.Arguments);
            appLogger.Error(args);
            return _services.db.Fetch<TEntityViewModel>(sql);
        }

        public Page<TEntityViewModel> GetViewAll(long page, long itemsPerPage, Sql sqlFilter)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(), parameterArgs);
            #region Check RoleAccess
            sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
            return _services.db.Page<TEntityViewModel>(page, itemsPerPage, sql);
        }

        public TEntityViewModel GetViewFirstOrDefault(string sqlFilter, params object[] args)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(), parameterArgs);
            if (string.IsNullOrEmpty(sqlFilter))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter, args);
            }
            return _services.db.FirstOrDefault<TEntityViewModel>(sql);
        }

        public TEntityViewModel GetViewFirstOrDefault(Sql sqlFilter)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(), parameterArgs);
            if (string.IsNullOrEmpty(sqlFilter?.SQL))
            {
                sql.Append(" WHERE 1=1 ");
                #region Check RoleAccess
                sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion
            }
            else
            {
                #region Check RoleAccess
                sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                    this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                #endregion

                sql.Append(sqlFilter.SQL, sqlFilter.Arguments);
            }
            return _services.db.FirstOrDefault<TEntityViewModel>(sql);
        }

        public Page<TEntityViewModel> GetViewPerPage(long page, long itemsPerPage)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(), parameterArgs);
            #region Check RoleAccess
            sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
            #endregion
            return _services.db.Page<TEntityViewModel>(page, itemsPerPage, sql);
        }

        public Page<TEntityViewModel> GetViewPerPage(long page, long itemsPerPage, string sqlFilter, params object[] args)
        {
            Sql sql = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(), parameterArgs);
            try
            {
                if (string.IsNullOrEmpty(sqlFilter))
                {
                    sql.Append(" WHERE 1=1 ");
                    #region Check RoleAccess
                    sql.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                        this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                    #endregion

                }
                else
                {
                    #region Check RoleAccess
                    sql.Append(" WHERE dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                        this._services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());
                    #endregion

                    sql.Append(sqlFilter, args);
                }

                if (itemsPerPage == 0)
                {
                    itemsPerPage = int.MaxValue;
                }
                appLogger.Debug(sql.SQL);
                appLogger.Debug(sql.Arguments);
                return _services.db.Page<TEntityViewModel>(page, itemsPerPage, sql);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return null;
        }

        public virtual DataTablesResponse GetListDataTables(DataTableRequest DataTableRequest, Sql sqlOptionalFilter = null)
        {
            DataTablesResponse result = new DataTablesResponse(DataTableRequest.Draw, new List<TEntityViewModel>(), 0, 0); ;
            var parameters = new List<String>();
            int unfilteredCount = 0;
            var log = "";

            try
            {
                Sql qry = null;
                
                if (appEntity != null) qry = Sql.Builder.Append(((IEntity)appEntity).GetDefaultView(), parameterArgs);
                if (qry == null) return result;

                if (sqlOptionalFilter == null)
                {
                    qry.Append(" WHERE 1=1 ");
                }
                else
                {
                    qry.Append(sqlOptionalFilter.SQL, sqlOptionalFilter.Arguments);
                }

                //This Function Does't support ;WITH
                if (qry.SQL.TrimStart().StartsWith(";WITH"))
                    throw new Exception("This Function Does't support ;WITH, or can't using pagination on ;WITH");

                // Role access filter
                //qry.Append(" AND dbo.ufn_can_read( @0, @1, r.id, r.owner_id ) > 0 ",
                //    _services.dataContext.AppUserId, ((IEntity)appEntity).GetEntityId());

                #region Get unfiltered count

                using (var db = new dreamwellRepo())
                {
                    try
                    {
                        var qryUC = String.Format("SELECT COUNT(*) FROM ( {0} ) AS dt", qry.SQL);
                        unfilteredCount = db.ExecuteScalar<int>(qryUC, qry.Arguments);
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex.ToString());
                    }
                    finally
                    {
                        appLogger.Debug(db.LastCommand);
                    }
                }

                #endregion

                var sql = Sql.Builder.Append(qry.SQL, qry.Arguments);

                var viewColumns = ((IEntity)appEntity).GetDefaultViewColumns();
                if (!String.IsNullOrEmpty(DataTableRequest.Search.Value))
                {
                    #region Set single parameter search

                    parameters.Clear();
                    parameters.Add(String.Format("%{0}%", DataTableRequest.Search.Value));
                    var filteredColumnCount = 0;

                    foreach (Dreamwell.Infrastructure.DataTables.DataTableColumn col in DataTableRequest.Columns)
                    {
                        if (!col.Searchable) continue;
                        if (!viewColumns.ContainsKey(col.Data)) continue;

                        if (filteredColumnCount == 0)
                        {
                            sql.Append(" AND ( " + viewColumns[col.Data] + " LIKE @0 ",
                                String.Format("%{0}%", DataTableRequest.Search.Value));
                        }
                        else
                        {
                            sql.Append(" OR " + viewColumns[col.Data] + " LIKE @0 ",
                                String.Format("%{0}%", DataTableRequest.Search.Value));
                        }

                        filteredColumnCount++;
                    }

                    if (filteredColumnCount > 0)
                    {
                        sql.Append(" ) ");
                    }

                    #endregion
                }
                else
                {
                    #region Set multi parameters search

                    parameters.Clear();
                    var filteredColumnCount = 0;

                    var filteredColumns = DataTableRequest.Columns.Where(o => o.Searchable == true);
                    foreach (Dreamwell.Infrastructure.DataTables.DataTableColumn col in filteredColumns)
                    {
                        if (!col.Searchable || String.IsNullOrEmpty(col.Search.Value)) continue;
                        if (!viewColumns.ContainsKey(col.Data)) continue;

                        parameters.Add(String.Format("%{0}%", col.Search.Value));
                        sql.Append(" AND " + viewColumns[col.Data] + " LIKE @0 ",
                            String.Format("%{0}%", DataTableRequest.Search.Value));

                        filteredColumnCount++;
                    }

                    #endregion
                }

                #region Set row order

                sql.Append(" ORDER BY ");

                //if (DataTableRequest.Order == null || DataTableRequest.Order.Count() == 0)
                //{

                //}
                var viewOrders = ((IEntity)appEntity).GetDefaultViewOrders();
                if (viewOrders?.Count == 0 )
                {
                    sql.Append(" r.id ASC ");
                }

                foreach (var col in viewOrders)
                {
                    log = String.Format("Default sorted columns = {0}", col.Key);
                    appLogger.Debug(log);

                    sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Key], col.Value));
                }

                var sortedColumns = DataTableRequest.Columns.Where(o => o.Orderable == true);
                if (sortedColumns != null && sortedColumns.Count() > 0 )
                {
                    foreach (var col in sortedColumns.OrderBy(o => o.OrderNumber))
                    {
                        log = String.Format("DataTables sorted columns = {0}", col.Data); 
                        appLogger.Debug(log);
                        var orderBy = DataTableRequest.Order.Where(o => o.Column == DataTableRequest.Columns.FindIndex(x => x.Data.Contains(col.Data))).FirstOrDefault();
                        if (orderBy != null)
                        {
                            if (!viewColumns.ContainsKey(col.Data)) continue;
                            sql.Append(String.Format(" {0} {1}, ", viewColumns[col.Data],
                                orderBy.Dir));
                        }
                    }
                }

                //sementara
                sql.Append(" r.id ASC ");

                #endregion

                using (var db = new dreamwellRepo())
                {
                    appLogger.Debug(sql.SQL.TrimEnd().TrimEnd(','));
                    appLogger.Debug(JsonConvert.SerializeObject(sql.Arguments));
                    var rowPerPage = DataTableRequest.Length < 1 ? 1 : DataTableRequest.Length;
                    var pageNumber = (DataTableRequest.Start / rowPerPage) + 1;

                    try
                    {
                        var rows = db.Page<TEntityViewModel>(pageNumber, rowPerPage, sql);
                        appLogger.Debug(db.LastCommand);
                        result = new DataTablesResponse(DataTableRequest.Draw, rows.Items,
                            (int)rows.TotalItems, unfilteredCount);

                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex.ToString());
                    }
                    finally
                    {
                        appLogger.Debug(db.LastCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex.ToString());
            }

            return result;
        }


    }
}
