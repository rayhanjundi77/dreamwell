﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.Infrastructure
{
    public static  class AppConstants
    {
        public const string ServerTypeAPI = "Dreamwell.ASP.NET Web API";
        public const string TokenSession = "Dreamwell.AppTokenSession";
        public const string SessionKey = "Dreamwell.UserSession";
        public const string TokenSessionKey = "Dreamwell.TokenSession";

    }
}
