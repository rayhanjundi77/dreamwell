﻿using Dreamwell.Infrastructure.Options;
using Dreamwell.Infrastructure.Session;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Infrastructure.WebApplication
{
	public class BaseController : Controller, IActionFilter
	{
		public Logger AppLogger = LogManager.GetCurrentClassLogger();
		public UserSession userSession { get; set; }

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			try
			{
				userSession = SessionManager.UserSession;
				filterContext.HttpContext.Response.Headers.Remove("X-Frame-Options");
				filterContext.HttpContext.Response.AddHeader("X-Frame-Options", "AllowAll");
				base.OnActionExecuting(filterContext);
				if (userSession == null)
				{
					var apiuri = Options.DreamwellSettings.WebServerUrl;
					Response.Redirect(string.Format("{0}/{1}", apiuri, "Identity/Account/Login"));
				}
				else
				{
					ViewBag.AppUserId = userSession.AppUserId;
					ViewBag.AppUsername = userSession.AppUsername;
					ViewBag.AppFullname = userSession.Fullname;

					ViewBag.PrimaryBusinessUnitId = userSession.PrimaryBusinessUnitId;
					ViewBag.PrimaryBusinessUnit = userSession.PrimaryBusinessUnit;

					ViewBag.PrimaryTeamId = userSession.PrimaryTeamId;
					ViewBag.PrimaryTeam = userSession.PrimaryTeam;

					ViewBag.AppEmail = userSession.Email;
					ViewBag.Gender = userSession.Gender;

					if (!userSession.SysAdmin)
					{
						ViewBag.BusinessUnitRoute = userSession.PrimaryBusinessUnitCode;
					}
					else
					{
						ViewBag.BusinessUnitRoute = "APH";
					}
				}
			}
			catch (Exception)
			{
			}

		}
	}
}
