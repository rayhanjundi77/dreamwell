﻿using Dreamwell.Infrastructure.Options;
using Dreamwell.Infrastructure.Session;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Dreamwell.Infrastructure.WebApplication
{
    public class BaseControllerUTC : Controller, IActionFilter
    {
        public Logger AppLogger = LogManager.GetCurrentClassLogger();
        public UserSession userSession { get; set; }

        public string BusinessUnitRoute { get; private set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            userSession = SessionManager.UserSession;
            filterContext.HttpContext.Response.Headers.Remove("X-Frame-Options");
            filterContext.HttpContext.Response.AddHeader("X-Frame-Options", "AllowAll");
            base.OnActionExecuting(filterContext);
            if (userSession == null)
            {
                var apiuri = Options.DreamwellSettings.WebServerUrl;
                Response.Redirect(string.Format("{0}/{1}", apiuri, "Identity/Account/Login/"));
            }

            ViewBag.AppUserId = userSession.AppUserId;
            ViewBag.AppUsername = userSession.AppUsername;
            ViewBag.AppFullname = userSession.Fullname;
            ViewBag.AppEmail = userSession.Email;
            ViewBag.Gender = userSession.Gender;


            BusinessUnitRoute = ControllerContext.RouteData.Values["unit"].ToString();

            if (!userSession.SysAdmin)
            {
                ViewBag.BusinessUnitRoute = userSession.PrimaryBusinessUnitCode;
                ViewBag.PrimaryBusinessUnit = userSession.PrimaryBusinessUnit;
            }
            else
            {
                if (BusinessUnitRoute != "APH")
                {
                    AppLogger.Debug($"Base controller started getting APH {BusinessUnitRoute}");
                    using (var _client = new HttpClient())
                    {
                        try
                        {
                            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userSession.Token);
                            _client.DefaultRequestHeaders.Add("Server-Type", Infrastructure.AppConstants.ServerTypeAPI);
                            _client.BaseAddress = new Uri(DreamwellSettings.ApiServerUrl);
                            var apiUri = Infrastructure.Options.DreamwellSettings.ApiServerUrl;
                            var task = _client.GetAsync(string.Format("{0}/core/BusinessUnit/GetId?aphName={1}", apiUri, BusinessUnitRoute)).Result;

                            if (task.Content != null)
                            {
                                var responseContent = task.Content.ReadAsStringAsync().Result;
                                AppLogger.Debug(responseContent);
                                var r = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResponse>(responseContent);
                                ViewBag.BusinessUnitRouteId = (string)r.Data;
                            }
                            else
                            {
                                AppLogger.Debug("responseContent is null");
                            }
                        }
                        catch (Exception ex)
                        {
                            AppLogger.Debug("Base Controller UTC GET Business unit ID by unit code from route");
                            AppLogger.Error(ex);
                            throw new Exception("Business unit route not found");
                        }
                    }

                }
                ViewBag.BusinessUnitRoute = BusinessUnitRoute;
            }


        }
    }
}
