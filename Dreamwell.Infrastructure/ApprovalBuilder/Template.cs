﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.Infrastructure.ApprovalBuilder
{
    public class Template
    {
        public string Title { get; set; }
        public string ApplicationEntityId { get; set; }
        public List<Approval> Approvals { get; set; }

        public Template()
        {
            Approvals = new List<Approval>();
        }
    }

    public class Approval {
        public int? Level { get; set; }
        public bool Condition { get; set; } /*FALSE = ANY | TRUE = ALL */
        public List<Approver> Approvers { get; set; }

        public Approval()
        {
            if (Level == null) {
                Level = 0;
            }
            Condition = false;
            Approvers = new List<Approver>();
        }

    }

    public class Approver {
        public string AssignTo { get; set; } /*USER OR TEAM*/
    }

}
