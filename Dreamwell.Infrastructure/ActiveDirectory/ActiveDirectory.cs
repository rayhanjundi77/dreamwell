﻿using CommonTools;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NLog;

namespace Dreamwell.Infrastructure
{
    public static class ActiveDirectory
    {
        static Logger appLogger = LogManager.GetCurrentClassLogger();
        public class UserAD
        {
            public string UserName { get; set; }
            public string Email { get; set; }
            public string DisplayName { get; set; }
            public string Company { get; set; }
            public string Department { get; set; }

        }


        public static bool Authentication(string username, string password, string domainPath)
        {
            bool Valid = false;
            try
            {
                DirectoryEntry objDirEntiry = new DirectoryEntry(domainPath, username, password);
                DirectorySearcher search = new DirectorySearcher(objDirEntiry);
                search.Filter = "(samaccountname=" + username + ")";
                SearchResult result = search.FindOne();

                Valid = (result != null);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return Valid;
        }

        public static List<UserAD> GetADUsers(MarvelDataSourceRequest marvelDataSourceRequest, string domainPath)
        {
            List<UserAD> lstADUsers = new List<UserAD>();
            try
            {

                string DomainPath = domainPath; //"LDAP://DC=xxxx,DC=com";
                DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
                DirectorySearcher search = new DirectorySearcher(searchRoot);


                if (marvelDataSourceRequest?.Filter != null)
                {
                    foreach (var item in marvelDataSourceRequest?.Filter.Filters)
                    {
                        if (string.IsNullOrEmpty(item.Value))
                        {
                            return lstADUsers;
                        }
                        search.Filter = "(&(objectClass=user)(objectCategory=person)(name=*" + item.Value + "*))";
                    }
                }

                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();

                for (int counter = 0; counter < resultCol?.Count; counter++)
                {
                    try
                    {
                        result = resultCol[counter];
                        if (result.Properties.Contains("samaccountname") &&
                                 result.Properties.Contains("mail") &&
                            result.Properties.Contains("displayname") && result.Properties.Contains("company"))
                        {
                            UserAD objSurveyUsers = new UserAD();
                            objSurveyUsers.Email = (String)result.Properties["mail"][0];
                            objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                            objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                            objSurveyUsers.Department = (String)result.Properties["department"][0];
                            objSurveyUsers.Company = (result.Properties["company"] != null ? (String)result.Properties["company"][0] : string.Empty);
                            lstADUsers.Add(objSurveyUsers);
                        }
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                    }
                }
                lstADUsers = lstADUsers.OrderBy(r => r.DisplayName).ToList();

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return lstADUsers;
        }

        public static List<UserAD> GetADUsers(string domainPath, string searchByName = null)
        {
            List<UserAD> lstADUsers = new List<UserAD>();
            try
            {
                string DomainPath = domainPath; //"LDAP://DC=xxxx,DC=com";
                DirectoryEntry searchRoot = new DirectoryEntry(DomainPath);
                searchRoot = new DirectoryEntry(domainPath, string.Format(@"{0}\{1}", "AXA-INSURANCE", "mikhael.alvin"), "Ax@t0wer12");
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(objectClass=user)(objectCategory=person))";
                //search.PropertiesToLoad.Add("samaccountname");
                //search.PropertiesToLoad.Add("mail");
                //search.PropertiesToLoad.Add("usergroup");
                //search.PropertiesToLoad.Add("company");
                //search.PropertiesToLoad.Add("displayname");//first name
                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();
                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        try
                        {
                            string UserNameEmailString = string.Empty;
                            result = resultCol[counter];
                            if (result.Properties.Contains("samaccountname") &&
                                     result.Properties.Contains("mail") &&
                                result.Properties.Contains("displayname") && result.Properties.Contains("company"))
                            {
                                UserAD objSurveyUsers = new UserAD();
                                objSurveyUsers.Email = (String)result.Properties["mail"][0] +
                                  "^" + (String)result.Properties["displayname"][0];
                                objSurveyUsers.UserName = (String)result.Properties["samaccountname"][0];
                                objSurveyUsers.DisplayName = (String)result.Properties["displayname"][0];
                                objSurveyUsers.Department = (String)result.Properties["department"][0];
                                objSurveyUsers.Company = (result.Properties["company"] != null ? (String)result.Properties["company"][0] : string.Empty);
                                lstADUsers.Add(objSurveyUsers);
                            }
                        }
                        catch (Exception ex)
                        {
                            appLogger.Error(ex);
                            continue;
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return lstADUsers;
        }
    }
}
