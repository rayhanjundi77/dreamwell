﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.Infrastructure.Session
{

    public class object_description
    {
        public string id { get; set; }
        public string description { get; set; }
    }

    [Serializable]
    public class UserSession
    {
        public string Token { get; set; }
        public string AppUserId { get; set; }
        public string AppUsername { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public bool SysAdmin { get; set; }

        public string OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationDomain { get; set; }

        public string PrimaryBusinessUnitId { get; set; }
        public string PrimaryBusinessUnit { get; set; }
        public string PrimaryBusinessUnitCode { get; set; }


        public string PrimaryTeamId { get; set; }
        public string PrimaryTeam { get; set; }

        public bool? Gender { get; set; }


        public List<object_description> Teams { get; set; }
        public List<object_description> Roles { get; set; }

        public string Fullname
        {
            get
            {
                if (string.IsNullOrEmpty(Lastname))
                    return String.Format("{0}", Firstname).Trim();
                else
                    return String.Format("{1},{0}", Firstname, Lastname.ToUpper()).Trim();
            }
        }

    }
}
