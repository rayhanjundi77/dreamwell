﻿using Dreamwell.BusinessLogic.Core.Common;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web;

namespace Dreamwell.Infrastructure.Session
{
    public static class SessionManager
    {
        static NLog.Logger appLogger = NLog.LogManager.GetCurrentClassLogger();
        public static UserSession FromToken(string token)
        {
            var tokenClaimData = JwtWrapper.GetPrincipal(token);
            var tokenValue = tokenClaimData?.Claims?.ToList()?[1]?.Value;
            return JsonConvert.DeserializeObject<UserSession>(tokenValue);
        }

        public static UserSession UserSession
        {
            get
            {
                var serverType = HttpContext.Current.Request.Headers["Server-Type"];
                //if (serverType.Equals(AppConstants.ServerTypeAPI) || string.IsNullOrEmpty(serverType)) throw new Exception("Server type is required!");

                if (string.IsNullOrEmpty(serverType))
                {
                    if (HttpContext.Current.Request.QueryString["x-Token"] != null ||
                        HttpContext.Current.Request.QueryString["X_TOKEN"] != null)
                    {
                        serverType = "X-TOKEN";
                    }
                }

                if (serverType == AppConstants.ServerTypeAPI)
                {
                    try
                    {
                        var token = HttpContext.Current.Request.Headers["Authorization"];
                        token = token.Substring(7);
                        var tokenClaimData = JwtWrapper.GetPrincipal(token);
                        var tokenValue = tokenClaimData?.Claims?.ToList()?[1]?.Value;
                        return JsonConvert.DeserializeObject<UserSession>(tokenValue);
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                        return null;
                    }
                }
                else if (serverType == "X-TOKEN")
                {
                    try
                    {
                        var token = HttpContext.Current.Request.QueryString["x-Token"];
                        if (string.IsNullOrEmpty(token))
                        {
                            token = HttpContext.Current.Request.QueryString["X_TOKEN"];
                        }

                        var tokenClaimData = JwtWrapper.GetPrincipal(token);
                        var tokenValue = tokenClaimData?.Claims?.ToList()?[1]?.Value;
                        var uSession = JsonConvert.DeserializeObject<UserSession>(tokenValue);
                        uSession.Token = token;
                        return uSession;
                    }
                    catch (Exception ex)
                    {
                        appLogger.Error(ex);
                        return null;
                    }
                }
                else
                {
                    return (UserSession)HttpContext.Current.Session[AppConstants.SessionKey];
                }
            }
        }
    }
}
