﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommonTools.UnitOfMeasurement;
namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Console.WriteLine($"Convert 1 ft to meter = {CommonTools.UnitOfMeasurement.MeasurementUnits.Convert(1, "ft", "m")}");
            var mleTosm = MeasurementUnits.Convert(1, "ft", "bbl");
            Assert.AreEqual(3.7854, mleTosm);            
        }
    }
}
