﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Hosting;
using CommonTools;
using PetaPoco;
using Dreamwell.Infrastructure;
using Dreamwell.DataAccess;
using NLog;
using System.Web;
using System.Dynamic;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;
using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess.Models.Entity;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class SingleWellServices
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();

        protected DataContext dataContext { get; private set; }
        //private readonly Database _database;
        public SingleWellServices(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }
        //public ApiResponsePage<vw_drilling_hole_and_casing> LookupByWellTrayek(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        //{
        //    var result = new ApiResponsePage<vw_drilling_hole_and_casing>();
        //    result.Items = this._services.db.Fetch<vw_drilling_hole_and_casing>(@"select * from vw_well_hole_and_casing as r where is_active=1 AND r.id IN (SELECT d.well_hole_and_casing_id FROM drilling_hole_and_casing d WHERE d.well_hole_and_casing_id = @0 )", wellId);
        //    return result;
        //}
        //public ApiResponsePage<vw_drilling_hole_and_casing> LookupByWellTrayek(MarvelDataSourceRequest marvelDataSourceRequest, string wellId)
        //{
        //    var result = new ApiResponsePage<vw_drilling_hole_and_casing>();
        //    try
        //    {
        //        appLogger.Info($"Executing LookupByWellTrayek with wellId: {wellId}");
        //        result.Items = this.db.Fetch<vw_drilling_hole_and_casing>(@"select * from vw_well_hole_and_casing as r where is_active=1 AND r.id IN (SELECT d.well_hole_and_casing_id FROM drilling_hole_and_casing d WHERE d.well_hole_and_casing_id = @0 )", wellId);
        //        appLogger.Info($"Fetched {result.Items.Count} items");
        //    }
        //    catch (Exception ex)
        //    {
        //        appLogger.Error(ex, "Error executing LookupByWellTrayek");
        //        result.Items = new List<vw_drilling_hole_and_casing>();
        //    }
        //    return result;
        //}

        public async Task<ApiResponse<List<vw_drilling_hole_and_casing>>> GetIadcAnalysis(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling_hole_and_casing>>();
            try
            {
                var holeAndCasingRecords = new List<vw_drilling_hole_and_casing>();
                var drillingHoleAndCasingRepo = new DrillingHoleAndCasingRepository(this.dataContext);

                var qry = Sql.Builder.Append(@"
                SELECT 
                    *
                FROM 
                    drilling_hole_and_casing t1
                INNER JOIN 
                    vw_drilling_hole_and_casing t2 ON t1.id = t2.id
                WHERE 
                    t1.well_id = @0 AND t1.multicasing IS NULL
                ORDER BY 
                    CAST(t2.hole_name AS FLOAT) DESC
            ", wellId);

                var records = vw_drilling_hole_and_casing.Fetch(qry).ToList();

                if (records.Count > 0)
                {
                    foreach (var item in records)
                    {
                        var service = new IadcAnalysisRepository(this.dataContext, wellId, item.id);
                        var trayekRecords = service.GetViewAll();

                        if (trayekRecords.Count > 0)
                        {
                            item.IadcAnalysis = trayekRecords;
                        }

                        var npt = await this.GetIadcAnalysisNpt(item.id);

                        if (npt.Data.Count > 0)
                        {
                            item.IadcAnalysisNpt = npt.Data;
                        }

                        holeAndCasingRecords.Add(item);
                    }
                }

                // Optional filter based on hole_name
                holeAndCasingRecords = holeAndCasingRecords
                    .Where(t => !string.IsNullOrEmpty(t.hole_name))
                    .ToList();

                result.Data = holeAndCasingRecords;
                result.Status.Success = true;
                result.Status.Message = "Successfully retrieved IADC analysis data.";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public async Task<ApiResponse<List<vw_iadc_analysis_npt>>> GetIadcAnalysisNpt(string drillingHoleCasingId)
        {
            var result = new ApiResponse<List<vw_iadc_analysis_npt>>();
            try
            {
                var service = new IadcAnalysisNptRepository(this.dataContext, drillingHoleCasingId);
                result.Data = await Task.Run(() => service.GetViewAll());
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling>> GetDrillingByWell(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling>>();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder.Append(drilling.DefaultView);
                    sql.Append(" WHERE r.well_id=@0", wellId);
                    sql.Append(" ORDER BY r.drilling_date ASC ");
                    result.Data = db.Fetch<vw_drilling>(sql);
                }
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<Report2DTrajectoryViewModel> Get2DTrajectory(string wellId)
        {
            var result = new ApiResponse<Report2DTrajectoryViewModel>();
            result.Data = new Report2DTrajectoryViewModel();
            try
            {
                var wellDeviationSql = Sql.Builder.Append(@"SELECT * FROM well_deviation WHERE well_id = @0 ORDER BY created_on ASC, seq ASC", wellId);
                result.Data.well_deviation = this.dataContext.GetInstance.Fetch<well_deviation>(wellDeviationSql);
                result.Data.well_id = wellId;


                var drillingDeviationSql = Sql.Builder.Append(@"SELECT  
                                                              r.id,
                                                              r.owner_id,
                                                              r.drilling_id,
                                                              r.seq,
                                                              r.measured_depth,
                                                              r.inclination,
                                                              r.azimuth,
                                                              r.tvd,
                                                              r.v_section,
                                                              r.n_s,
                                                              r.e_w,
                                                              r.dls
                                                            FROM drilling_deviation r
                                                            INNER JOIN drilling d ON d.id = r.drilling_id
                                                            WHERE d.well_id = @0 ORDER BY d.drilling_Date ASC, r.seq ASC ", wellId);
                result.Data.drilling_deviation = this.dataContext.GetInstance.Fetch<drilling_deviation>(drillingDeviationSql);

                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_report_drilling>> GetDrillingCostAndDepth(string wellId)
        {
            var result = new ApiResponse<List<vw_report_drilling>>();
            try
            {

                var service = new ReportDrillingRepository(this.dataContext, wellId);
                result.Data = service.GetViewAll(" AND r.well_id = @0 ORDER BY r.drilling_date ASC ", wellId);

                if (result.Data.Count > 0)
                {
                    var myData = new List<vw_report_drilling>();
                    var temp = result.Data;
                    myData = (temp.GroupBy(r => new { r.id, r.drilling_date, r.dfs, r.daily_cost, r.current_depth_md, r.current_depth_tvd, r.cummulative_cost }, (key, groupData) => new vw_report_drilling()
                    {
                        id = key.id,
                        drilling_date = key.drilling_date,
                        dfs = key.dfs,
                        cummulative_cost = key.cummulative_cost,
                        daily_cost = key.daily_cost,
                        current_depth_md = key.current_depth_md,
                        current_depth_tvd = key.current_depth_tvd,
                        operation_activity = (from r in groupData
                                              select new drilling_operation_activity()
                                              {
                                                  id = r.drilling_operation_activity_id,
                                                  description = r.description,
                                              }).ToList()
                    }).ToList());

                    result.Data = myData;
                }
                else
                {
                    result.Status.Message = "Eorrr gaes";
                }

                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling>> GetDrillingByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_drilling>>();
            try
            {

                var service = new DrillingRepository(this.dataContext);
                result.Data = service.GetViewAll(" AND r.well_id = @0 ORDER BY r.drilling_date ASC ", wellId);

                if (result.Data.Count > 0)
                {
                    var myData = new List<vw_drilling>();
                    var temp = result.Data;
                    myData = temp.ToList();

                    result.Data = myData;
                }

                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_report_drilling_stuck_pipe>> GetDrillingStuckPipe(string wellId)
        {
            var result = new ApiResponse<List<vw_report_drilling_stuck_pipe>>();
            try
            {
                var stuck_pipe_code = "21c";
                var service = new ReportDrillingStuckPipeRepository(this.dataContext, stuck_pipe_code);
                result.Data = service.GetViewAll(" AND r.well_id = @0 ORDER BY r.drilling_date ASC ", wellId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_report_well_iadc>> GetWellIadc(string wellId)
        {
            var result = new ApiResponse<List<vw_report_well_iadc>>();
            result.Data = new List<vw_report_well_iadc>();
            try
            {
                var service = new SingleWellReportIadcRepository(this.dataContext);
                result.Data = service.GetViewAll(" AND r.well_id = @0 ", wellId).OrderBy(r => r.drilling_date).ToList();
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse GetPTNPTChart(string wellId)
        {

            var result = new ApiResponse();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder.Append(@"
                            SELECT 
	                            r.drilling_date,
	                            r.type_desc,
	                            r.total as total_type,
	                            SUM(total) OVER (partition BY r.drilling_date) as total,
	                            CAST(CONVERT(DECIMAL,COALESCE(r.total,0)) / SUM(total) OVER (partition BY r.drilling_date) * 100 AS DECIMAL(3,0)) AS percentage 	
                            FROM (
                            SELECT
	                            d.drilling_date,
	                            CASE i.type WHEN 1 THEN 'PT' WHEN 2 THEN 'NPT'  END AS type_desc,
	                            COUNT(i.type) AS total

                            FROM dbo.drilling_operation_activity AS r
                            LEFT OUTER JOIN dbo.drilling AS d
                              ON r.drilling_id = d.id
                            LEFT OUTER JOIN dbo.iadc AS i
                              ON r.iadc_id = i.id
                            WHERE d.well_id=@0
                            GROUP BY d.drilling_date,i.type

                            ) as r ", wellId);

                    var d = db.Fetch<PTNPT_Hour_output>(sql);

                    var pt = d?.Where(x => x.type_desc.Equals("PT"))?.OrderBy(x => x.drilling_date).Select(n => n.percentage).ToArray();
                    var npt = d?.Where(x => x.type_desc.Equals("NPT"))?.OrderBy(x => x.drilling_date).Select(n => n.percentage).ToArray();

                    var r = new List<dynamic>();
                    r.Add(new
                    {
                        type_desc = "PT",
                        percentage = pt
                    });

                    r.Add(new
                    {
                        type_desc = "NPT",
                        percentage = npt
                    });

                    result.Status.Success = true;
                    result.Data = r;
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.StackTrace;
                appLogger.Error(ex);
            }
            return result;
        }

        public class PTNPT_Hour_output
        {
            public string drilling_date { get; set; }
            public string type_desc { get; set; }
            public int total_type { get; set; }
            public int total { get; set; }
            public decimal percentage { get; set; }
        }

    }
}
