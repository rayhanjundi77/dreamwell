﻿using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using NLog;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dreamwell.BusinessLogic.UTC.Services
{
    public class BitRecordServices
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();
        protected DataContext dataContext { get; private set; }
        public BitRecordServices(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }

        public ApiResponse<List<vw_report_bit_record>> GetBitByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_report_bit_record>>();
            try
            {
                var qry = Sql.Builder.Append(@"SELECT 
                                            w.id as well_id,
                                            w.well_name,
                                            rg.name AS rig_name,
                                            f.field_name,
                                            r.id,
                                            r.owner_id,
											r.created_by,
                                            r.bit_run,
                                            wh.bha_no,
                                            wb.manufacturer,
                                            wb.bit_size,
                                            wb.bit_type,
                                            i.description AS IADC,
                                            wb.serial_number,
                                            wb.noozle_1,
                                            r.depth_in,
                                            r.depth_out,
											d.drilling_date,
                                            wb.dg_hours_on_bottom,
                                            r.dg_footage,
                                            r.dg_rop_overall,
                                            CONCAT(r.mudlogging_wob_min, '-', r.mudlogging_wob_max) AS WOB,
                                            CONCAT(r.mudlogging_rpm_min, '-', r.mudlogging_rpm_max) AS RPM,
                                            CONCAT(r.mudlogging_dhrpm_min, '-', r.mudlogging_dhrpm_max) AS RPM_MTR,
                                            CONCAT(r.mudlogging_spp_min, '-', r.mudlogging_spp_max) AS SPP,
                                            CONCAT(r.mudlogging_flowrate_min, '-', r.mudlogging_flowrate_max) AS GPM,
                                            CONCAT(r.dg_in_rows,'/',r.dg_out_rows,'/',r.dg_dull_char,'/',r.dg_loc_cone,'/',r.dg_seals,'/',r.dg_gauge,'/',r.dg_other_char,'/',r.dg_reason_pulled) AS DULL_GRADE
                                            FROM drilling_bha_bit r
											LEFT OUTER JOIN drilling d ON r.drilling_id = d.id
                                            LEFT OUTER JOIN well_bit wb ON r.well_bit_id = wb.id
                                            LEFT OUTER JOIN well_bha wh ON r.well_bha_id = wh.id
											LEFT OUTER JOIN well w ON r.well_id = w.id
											LEFT OUTER JOIN (SELECT w.id, r.name FROM well w
														LEFT OUTER JOIN rig r ON w.rig_id = r.id
														where w.id = @0) rg ON w.id = rg.id
											LEFT OUTER JOIN field f ON w.field_id = f.id
                                            LEFT OUTER JOIN iadc i ON wb.iadc_code = i.id
                                            where r.well_id = @0", wellId);
                var data = this.dataContext.GetInstance.Fetch<vw_report_bit_record>(qry);
                result.Data = data;
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_report_bit_record>> getRecordByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_report_bit_record>>();
            try
            {
                var qry = Sql.Builder.Append(@"SELECT 
											r.created_by,
											RTRIM(LTRIM(CONCAT(ISNULL(UPPER(au.last_name), N''), ' ', ISNULL(au.first_name, N'')))) AS record_created_by,
											ao.role_name
                                            FROM drilling_bha_bit r
											LEFT OUTER JOIN user_role ur ON r.created_by = ur.application_user_id
											LEFT OUTER JOIN application_user au ON ur.application_user_id = au.id
											LEFT OUTER JOIN application_role ao ON ur.application_role_id = ao.id
                                            where r.well_id = @0
											GROUP BY r.created_by,au.last_name,au.first_name,ao.role_name", wellId);
                var data = this.dataContext.GetInstance.Fetch<vw_report_bit_record>(qry);
                result.Data = data;
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
