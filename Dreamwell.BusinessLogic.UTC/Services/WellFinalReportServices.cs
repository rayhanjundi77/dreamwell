﻿using System;
using CommonTools;
using PetaPoco;
using Dreamwell.DataAccess;
using Dreamwell.Infrastructure;
using System.Collections.Generic;
using OfficeOpenXml;
using System.IO;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellFinalReportServices : WellFinalReportRepository
    {
        public WellFinalReportServices(DataContext dataContext): base(dataContext){}

        public ApiResponse Save(well_final_report record) {
            var result = new ApiResponse();
            try
            {
                var isNew = false;
                var recordId = record.id;
                
                result.Status.Success = SaveEntity(record, ref isNew,(r)=> recordId = r.id);
                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                    result.Data = new
                    {
                        recordId
                    };
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse<vw_well_final_report> GetByWellId(string wellId)
        {
            var result = new ApiResponse<vw_well_final_report>();
            result.Status.Success = true; 
            try
            {
                var isNew = false;
                var qry = Sql.Builder.Append(well_final_report.DefaultView);
                qry.Append(" WHERE r.well_id = @0 ", wellId);
                var record = vw_well_final_report.FirstOrDefault(qry);
                var recordId = string.Empty;
                if (record == null)
                {
                    well_final_report newRecord = new well_final_report();
                    newRecord.well_id = wellId;
                    result.Status.Success = SaveEntity(newRecord, ref isNew, (r) => recordId = r.id);


                    result.Data = GetViewById(newRecord.id);
                }
                else {
                    //qry = Sql.Builder.Append(well_final_report.DefaultView);
                    //qry.Append(" AND r.id = @0 ", recordId);
                    result.Data = vw_well_final_report.FirstOrDefault(qry);
                }
                   
                if (result.Status.Success)
                {
                    if (isNew)
                        result.Status.Message = "The data has been added.";
                    else
                        result.Status.Message = "The data has been updated.";
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex);
            }
            return result;
        }

        public HttpResponseMessage GenerateLampiran(string filePath, string id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                FileInfo file = new FileInfo(filePath);

                if (!file.Exists)
                {
                    response.StatusCode = HttpStatusCode.NotFound;
                    response.ReasonPhrase = "File Not Found";
                    response.Content = new StringContent("File not found at the specified path.");
                    return response;
                }

                using (ExcelPackage excelPackage = new ExcelPackage(file))
                {
                    // Menyimpan paket Excel
                    excelPackage.Save();

                    // Menyiapkan respons HTTP
                    response.StatusCode = HttpStatusCode.OK;
                    response.ReasonPhrase = "OK";
                    MediaTypeHeaderValue mediaType = new MediaTypeHeaderValue("application/octet-stream");
                    string fileName = "Lampiran Final Well Report.xlsx";
                    MemoryStream memoryStream = new MemoryStream(excelPackage.GetAsByteArray());
                    response.Content = new StreamContent(memoryStream);
                    response.Content.Headers.ContentType = mediaType;
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName
                    };
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.InternalServerError;
                response.ReasonPhrase = "Internal Server Error";
                response.Content = new StringContent($"Error: {ex.Message}");
            }
            return response;
        }

        public ApiResponse<List<vw_report_well_bit>> GetBitByWellId(string wellId)
        {
            var result = new ApiResponse<List<vw_report_well_bit>>();
            try
            {

                var service = new ReportWellBitRepository(_services.dataContext, wellId);
                result.Data = service.GetViewAll(" AND r.well_id = @0 ORDER BY r.bha_no ASC ", wellId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_report_hole_casing_actual>> GetHoleCasingActual(string wellId)
        {
            var result = new ApiResponse<List<vw_report_hole_casing_actual>>();
            try
            {

                var service = new ReportHoleCasingActualRepository(_services.dataContext, wellId);
                result.Data = service.GetViewAll(" AND r.well_id = @0 AND r.well_hole_and_casing_id IS NOT NULL ORDER BY r.created_on ASC ", wellId);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
