﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Hosting;
using CommonTools;
using PetaPoco;
using Dreamwell.Infrastructure;
using Dreamwell.DataAccess;
using NLog;
using System.Dynamic;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class BenchmarkServices
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();

        protected DataContext dataContext { get; private set; }
        public BenchmarkServices(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }

        public ApiResponse<List<vw_well>> GetPlanVsActualDepth(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_well>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                            SELECT COALESCE(p.current_depth_md, 0) AS actual_depth, r.* 
                            FROM well r
                            OUTER APPLY (	SELECT TOP 1 d.current_depth_md
				                            FROM drilling d
				                            WHERE d.well_id = r.id
				                            ORDER BY d.drilling_date DESC
			                            ) AS p
                            where 1=1 ");
                var wellIds = "";
                if (record.well_id != null && record.well_id.Count > 0)
                {
                    foreach (var wellId in record.well_id.Select((value, i) => new { i, value }))
                    {
                        wellIds += "'" + wellId.value + "'" + (wellId.i != record.well_id.Count - 1 ? "," : "");
                    }
                    sql.Append($" AND r.id in ({wellIds}) ");
                    //if (record.well_id.Count == 1)
                    //    sql.Append(@" AND r.id = @0 ", record.well_id[0]);
                    //else
                    //{
                    //    for (int i = 0; i < record.well_id.Count; i++)
                    //    {
                    //        if (i == 0)
                    //            sql.Append(@" AND r.id = @0 ", record.well_id[i]);
                    //        else
                    //            sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                    //    }
                    //}
                }
                var item = this.dataContext.GetInstance.Fetch<vw_well>(sql);

                sql = Sql.Builder.Append(well_object_uom_map.DefaultView);
                sql.Append($" AND r.is_active = 1 and r.object_name in ('Measured Depth ', 'Planned TD') AND r.well_id in ({wellIds})");
                var dtuom = this.dataContext.GetInstance.Fetch<vw_well_object_uom_map>(sql);

                foreach (var well_id in record.well_id)
                {
                    var uom = dtuom.Where(x => x.well_id == well_id && x.object_name == "Measured Depth ").Select(x => x.uom_code).First();

                    item.Where(x => x.id == well_id).ToList().ForEach(x =>
                    {
                        if (uom == "ft" && record.uom_select == "m")
                        {
                            x.actual_depth = Math.Round(x.actual_depth / 3.281m, 2);
                        }
                        else if (uom == "m" && record.uom_select == "ft")
                        {
                            x.actual_depth = Math.Round(x.actual_depth * 3.281m, 2);
                        }
                    });

                    uom = dtuom.Where(x => x.well_id == well_id && x.object_name == "Planned TD").Select(x => x.uom_code).First();

                    item.Where(x => x.id == well_id).ToList().ForEach(x =>
                    {
                        if (uom == "ft" && record.uom_select == "m")
                        {
                            x.planned_td = Math.Round((x.planned_td ?? 0) / 3.281m, 2);
                        }
                        else if (uom == "m" && record.uom_select == "ft")
                        {
                            x.planned_td = Math.Round((x.planned_td ?? 0) * 3.281m, 2);
                        }
                    });
                }

                //foreach (var i in item)
                //{
                //    //sql = Sql.Builder.Append(well_object_uom_map.DefaultView);
                //    //sql.Append(@" WHERE r.well_id =@0", i.id);
                //    //var dtuom = this.dataContext.GetInstance.Fetch<vw_well_object_uom_map>(sql);

                //    //double dplanned_td = 0;
                //    //double dactual_depth = 0;
                //    //if (i.planned_td != null && i.actual_depth != 0)
                //    //    dplanned_td = Convert.ToDouble(i.planned_td);
                //    //dactual_depth = Convert.ToDouble(i.actual_depth);

                //    foreach (var u in dtuom)
                //    {
                //        switch (u.object_name)
                //        {
                //            case "Planned TD":
                //                if (u.uom_code.Equals("ft"))
                //                {
                //                    i.planned_td = Convert.ToDecimal(CommonTools.UnitOfMeasurement.MeasurementUnits.Convert((double)i.planned_td, "ft", record.uom_select));
                //                }
                //                break;
                //            case "Measured Depth ":
                //                if (u.uom_code.Equals("ft"))
                //                {
                //                    i.actual_depth = Convert.ToDecimal(CommonTools.UnitOfMeasurement.MeasurementUnits.Convert((double)i.actual_depth, "ft", record.uom_select));
                //                }
                //                break;
                //        }
                //    }
                //}
                result.Data = item;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<ReportTotalWellByYears>> getTotalWell(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<ReportTotalWellByYears>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                                SELECT 
                                r.id, 
                                r.owner_id, 
                                r.well_name,
                                r.spud_date,
                                COALESCE(r.well_status, 0) AS well_status,
                                r.business_unit_id,
	                            b.unit_name,
                                r.field_id,
	                            f.field_name
                                FROM well r
	                            INNER JOIN business_unit_field AS bf ON bf.field_id = r.field_id
	                            INNER JOIN business_unit AS b ON b.id = bf.business_unit_id
                                INNER JOIN field AS f ON f.id = r.field_id
                                WHERE 1=1 AND r.spud_date IS NOT NULL ");

                //if (record.field_id != "")
                //    sql.Append(@" AND r.field_id = @0 ", record.field_id);


                if (record.business_unit_id != "")
                    sql.Append(@" AND bf.business_unit_id = @0 ", record.business_unit_id);

                if (record.well_status.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_status.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" COALESCE(r.well_status, 0) = @0 ", record.well_status[i]);
                        else
                            sql.Append(@" OR COALESCE(r.well_status, 0) = @0 ", record.well_status[i]);
                    }
                    sql.Append(@" ) ");
                }

                if (record.well_year.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_year.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" year(r.spud_date) = @0 ", record.well_year[i]);
                        else
                            sql.Append(@" OR year(r.spud_date) = @0 ", record.well_year[i]);
                    }
                    sql.Append(@" ) ");
                }

                var wells = this.dataContext.GetInstance.Fetch<vw_well>(sql);

                if (wells.Count > 0)
                {
                    result.Data = wells.GroupBy(r => r.spud_date.Value.Year, (key, groupData) => new ReportTotalWellByYears
                    {
                        year = key,
                        field = groupData.GroupBy(f => f.field_name, (keyField, groupField) => new vw_field
                        {
                            field_name = keyField,
                            well = groupField.ToList()
                        }).ToList()
                    }).OrderBy(r => r.year).ToList();
                    result.Status.Message = wells.Count.ToString();
                }

                appLogger.Error(sql.SQL);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }


        public ApiResponse<List<vw_well>> getPlanVsCostFiltering(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_well>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                                            SELECT 
                                            r.id, 
                                            r.owner_id, 
                                            r.well_name,
                                            COALESCE([plan].depth, 0) AS planned_md,
                                            COALESCE(act_md.current_depth, 0) AS actual_md,
                                            COALESCE(r.planned_days, 0) AS planned_days,
                                            COALESCE(act_days.total_days, 0) AS actual_days,
                                            COALESCE(r.afe_cost, 0) planned_cost,
                                            --COALESCE([plan].cost, 0) planned_cost,
                                            COALESCE(act_cost.actual_cost, 0) AS actual_cost,
                                            b.unit_name,
                                            r.field_id,
                                            f.field_name,
                                            r.spud_date,
                                            COALESCE(r.well_status, 0) AS well_status,
                                            r.business_unit_id
                                            FROM well r
                                            INNER JOIN business_unit_field AS bf ON bf.field_id = r.field_id
                                            INNER JOIN business_unit AS b ON b.id = bf.business_unit_id
                                            INNER JOIN field AS f ON f.id = r.field_id");
                sql.Append(@"               OUTER APPLY (
	                                            SELECT COUNT(d.id) AS total_days
	                                            FROM drilling d
	                                            WHERE d.well_id = r.id");

                sql.Append(@"               ) AS act_days
                                            OUTER APPLY (
	                                            SELECT TOP 1 COALESCE(d.current_depth_md, 0) AS current_depth
		                                        FROM drilling d
		                                        WHERE d.well_id = r.id AND d.current_depth_md IS NOT NULL AND d.current_depth_md > 0");

                sql.Append(@"                   ORDER BY d.drilling_date DESC
                                            ) AS act_md
	                                        OUTER APPLY (
		                                        SELECT TOP 1 t.cumm_cost AS cost, t.depth AS depth
		                                        FROM tvd_plan t
		                                        WHERE t.well_id = r.id
		                                        ORDER BY t.[no] DESC
	                                        ) AS [plan]");
                sql.Append(@"               OUTER APPLY (
                                                                   
         	                                    SELECT  COALESCE(SUM(cd.actual_price / cd.unit * dc.unit), 0) AS actual_cost 
	                                            FROM daily_cost dc
	                                            INNER JOIN drilling d ON d.id = dc.drilling_id
	                                            INNER JOIN afe_contract ac ON ac.id = dc.afe_contract_id
	                                            INNER JOIN contract_detail cd ON cd.id = ac.contract_detail_id
	                                            WHERE d.well_id = r.id ");

                sql.Append(@") ");
                sql.Append(@"AS act_cost ");

                sql.Append(@" WHERE 1=1 AND r.spud_date IS NOT NULL ");
                if (record.well_status.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_status.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" COALESCE(r.well_status, 0) = @0 ", record.well_status[i]);
                        else
                            sql.Append(@" OR COALESCE(r.well_status, 0) = @0 ", record.well_status[i]);
                    }
                    sql.Append(@" ) ");
                }

                if (record.business_unit_id != "")
                    sql.Append(@" AND bf.business_unit_id = @0 ", record.business_unit_id);

                if (record.field_id != "")
                    sql.Append(@" AND f.id = @0", record.field_id);

                if (record.well_id.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_id.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" r.id = @0 ", record.well_id[i]);
                        else
                            sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                    }
                    sql.Append(@" ) ");
                }

                appLogger.Error(sql.SQL);
                result.Data = this.dataContext.GetInstance.Fetch<vw_well>(sql);
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_drilling_contractor>> getRigFiltering(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_drilling_contractor>>();
            try
            {
                var sql = Sql.Builder.Append(@"");
                sql.Append(@"
                    SELECT 
	                    r.[id],
	                    r.[created_by],
	                    r.[created_on],
	                    r.[modified_by],
	                    r.[modified_on],
	                    r.[is_active],
	                    r.[is_locked],
	                    r.[is_default],
	                    r.[owner_id],
	                    r.[approved_by],
	                    r.[approved_on],
	                    r.[contractor_name],
	                    r.[contractor_address],
	                    r.[contractor_email],
	                    p.total_well,
	                    p.planned_days, 
	                    a.actual_days,
                        a.dol,
	                    t.[0] AS original_well, 
	                    t.[1] AS side_track, 
	                    t.[2] AS work_over, 
	                    t.[3] AS well_services 
                    FROM drilling_contractor r
                    OUTER APPLY (
	                    SELECT 
		                    COALESCE(SUM(w.planned_days), 0) AS planned_days, 
		                    COUNT(w.id) AS total_well
	                    FROM well w
	                    INNER JOIN business_unit_field AS bf ON bf.field_id = w.field_id
                        INNER JOIN business_unit AS b ON b.id = bf.business_unit_id
                        INNER JOIN field AS f ON f.id = w.field_id
	                    WHERE bf.business_unit_id = @0
	                    AND w.drilling_contractor_id = r.id 
                        AND w.field_id = @1
                        AND w.spud_date IS NOT NULL ", record.business_unit_id, record.field_id);

                //-- Filtering by well_id
                if (record.well_id.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_id.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" w.id =@0 ", record.well_id[i]);
                        else
                            sql.Append(@" OR w.id =@0 ", record.well_id[i]);
                    }
                    sql.Append(@" ) ");
                }

                if (record.well_status.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_status.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" COALESCE(w.well_status, 0) = @0 ", record.well_status[i]);
                        else
                            sql.Append(@" OR COALESCE(w.well_status, 0) = @0 ", record.well_status[i]);
                    }
                    sql.Append(@" ) ");
                }
                sql.Append(@"
                    ) AS p
                    OUTER APPLY (
	                    SELECT *
	                    FROM
	                    (
		                    SELECT COALESCE(w.well_status, 0) AS well_status
		                    FROM well w
		                    INNER JOIN business_unit_field AS bf ON bf.field_id = w.field_id
		                    INNER JOIN business_unit AS b ON b.id = bf.business_unit_id
		                    INNER JOIN field AS f ON f.id = w.field_id
		                    WHERE bf.business_unit_id = @0
		                    AND w.drilling_contractor_id = r.id AND w.spud_date IS NOT NULL ", record.business_unit_id);
                if (record.well_id.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_id.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" w.id =@0 ", record.well_id[i]);
                        else
                            sql.Append(@" OR w.id =@0 ", record.well_id[i]);
                    }
                    sql.Append(@" ) ");
                }
                //-- Filtering by Year
                //if (record.well_year.Count > 0)
                //{
                //    sql.Append(@" AND ( ");
                //    for (int i = 0; i < record.well_year.Count; i++)
                //    {
                //        if (i == 0)
                //            sql.Append(@" year(w.spud_date) = @0 ", record.well_year[i]);
                //        else
                //            sql.Append(@" OR year(w.spud_date) = @0 ", record.well_year[i]);
                //    }
                //    sql.Append(@" ) ");
                //}

                if (record.well_status.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_status.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" COALESCE(w.well_status, 0) = @0 ", record.well_status[i]);
                        else
                            sql.Append(@" OR COALESCE(w.well_status, 0) = @0 ", record.well_status[i]);
                    }
                    sql.Append(@" ) ");
                }

                sql.Append(@"
                        ) d
	                    PIVOT (
		                    COUNT(well_status)
		                    FOR well_status IN (
			                    [0],
			                    [1],
			                    [2],
			                    [3]
		                    )
	                    ) AS p
                    ) AS t
                    OUTER APPLY (
	                    SELECT COUNT(d.id) AS actual_days, SUM(d.dol) AS dol
	                    FROM drilling d
	                    INNER JOIN well w ON w.id = d.well_id
	                    WHERE w.drilling_contractor_id = r.id AND w.spud_date IS NOT NULL ");
                // Nanti Cobain Untuk filtering query sum dolnya 
                //-- Filtering by well_id
                if (record.well_id.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_id.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" w.id =@0 ", record.well_id[i]);
                        else
                            sql.Append(@" OR w.id =@0 ", record.well_id[i]);
                    }
                    sql.Append(@" ) ");
                }
                if (record.well_status.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_status.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" COALESCE(w.well_status, 0) = @0 ", record.well_status[i]);
                        else
                            sql.Append(@" OR COALESCE(w.well_status, 0) = @0 ", record.well_status[i]);
                    }
                    sql.Append(@" ) ");
                }
                sql.Append(@"
                    ) AS a 
                    WHERE 1=1 ");
                //-- Filtering by well_id
                if (record.well_id.Count > 0)
                {
                    sql.Append(@" AND ( ");
                    for (int i = 0; i < record.well_id.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" r.id IN (SELECT w1.drilling_contractor_id FROM well w1 WHERE w1.id =@0 )", record.well_id[i]);
                        else
                            sql.Append(@" OR r.id IN (SELECT w1.drilling_contractor_id FROM well w1 WHERE w1.id =@0 )", record.well_id[i]);
                    }
                    sql.Append(@" ) ");
                }

                result.Data = this.dataContext.GetInstance.Fetch<vw_drilling_contractor>(sql);
                result.Status.Success = true;

                appLogger.Error(sql);
            }
            
            
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_well>> getPlanVsActualDrilledDays(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_well>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                            SELECT r.id, r.owner_id, r.well_name, r.planned_days, d.actual_days
                            FROM well r
                            OUTER APPLY (
	                            SELECT count(*) as actual_days
	                            FROM drilling d
	                            WHERE d.well_id = r.id
                            ) AS d ");
                if (record.well_id != null && record.well_id.Count > 0)
                {
                    if (record.well_id.Count == 1)
                        sql.Append(@" WHERE r.id = @0 ", record.well_id[0]);
                    else
                    {
                        for (int i = 0; i < record.well_id.Count; i++)
                        {
                            if (i == 0)
                                sql.Append(@" WHERE r.id = @0 ", record.well_id[i]);
                            else
                                sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                        }
                    }
                }
                var item = this.dataContext.GetInstance.Fetch<vw_well>(sql);
                result.Data = item;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
        public ApiResponse<List<vw_well>> getCummulativeCost(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_well>>();
            try
            {
                var sql = Sql.Builder.Append(vw_report_cummulative_cost.DefaultView);
                if (record.well_id != null && record.well_id.Count > 0)
                {
                    for (int i = 0; i < record.well_id.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" WHERE r.id = @0 ", record.well_id[i]);
                        else
                            sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                    }
                }
                result.Data = this.dataContext.GetInstance.Fetch<vw_well>(sql); 
                result.Status.Success = true;
                appLogger.Error(sql.SQL);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_report_well_cost_per_depth>> getCostPerDepth(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_report_well_cost_per_depth>>();
            try
            {
                var sql = Sql.Builder.Append(vw_report_well_cost_per_depth.DefaultView);
                if (record.well_id != null && record.well_id.Count > 0)
                {
                    for (int i = 0; i < record.well_id.Count; i++)
                    {
                        if (i == 0)
                            sql.Append(@" WHERE r.id = @0 ", record.well_id[i]);
                        else
                            sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                    }
                }
                result.Data = this.dataContext.GetInstance.Fetch<vw_report_well_cost_per_depth>(sql);
                result.Status.Success = true;
                appLogger.Error(sql.SQL);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_well>> getPTandNPThours(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_well>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                           SELECT r.well_name, COALESCE(t1.pt_hours, 0) AS pt_hours, COALESCE(t2.npt_hours, 0) AS npt_hours
                            FROM well r
                            OUTER APPLY (	
				                            SELECT SUM(DATEDIFF(HOUR, d.operation_start_date, d.operation_end_date)) AS pt_hours
				                            FROM drilling_operation_activity AS d
				                            LEFT JOIN drilling d1 ON d1.id = d.drilling_id
				                            LEFT JOIN iadc i ON i.id = d.iadc_id
				                            WHERE i.type = '1' AND d1.well_id = r.id
			                            ) AS t1
                            OUTER APPLY (	
				                            SELECT SUM(DATEDIFF(HOUR, d.operation_start_date, d.operation_end_date)) AS npt_hours
				                            FROM drilling_operation_activity AS d
				                            LEFT JOIN drilling d1 ON d1.id = d.drilling_id
				                            LEFT JOIN iadc i ON i.id = d.iadc_id
				                            WHERE i.type = '2' AND d1.well_id = r.id
			                            ) AS t2
                            where r.field_id = @0 ", record.field_id);
                if (record.well_id != null && record.well_id.Count > 0)
                {
                    if (record.well_id.Count == 1)
                        sql.Append(@" AND r.id = @0 ", record.well_id[0]);
                    else
                    {
                        for (int i = 0; i < record.well_id.Count; i++)
                        {
                            if (i == 0)
                                sql.Append(@" AND r.id = @0 ", record.well_id[i]);
                            else
                                sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                        }
                    }
                }
                var item = this.dataContext.GetInstance.Fetch<vw_well>(sql);
                result.Data = item;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_well>> getRop(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_well>>();
            try
            {
                var sql = Sql.Builder.Append(@"
                        SELECT COALESCE(p.rop, 0) AS rop, r.well_name
                        FROM well r
                        OUTER APPLY (	
				                        SELECT SUM(COALESCE(d.mudlogging_avg_rop_min, 0)) AS rop, r.* 
				                        FROM drilling d
				                        WHERE r.id = d.well_id
			                        ) AS p
                            where r.field_id = @0 ", record.field_id);
                if (record.well_id != null && record.well_id.Count > 0)
                {
                    if (record.well_id.Count == 1)
                        sql.Append(@" AND r.id = @0 ", record.well_id[0]);
                    else
                    {
                        for (int i = 0; i < record.well_id.Count; i++)
                        {
                            if (i == 0)
                                sql.Append(@" AND r.id = @0 ", record.well_id[i]);
                            else
                                sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                        }
                    }
                }
                var item = this.dataContext.GetInstance.Fetch<vw_well>(sql);
                result.Data = item;
                result.Status.Success = true;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_benchmark_report_iadc>> GetWellIadc(BenchmarkReportViewModel record)
        {
            var result = new ApiResponse<List<vw_benchmark_report_iadc>>();
            result.Data = new List<vw_benchmark_report_iadc>();
            try
            {
                if (record.well_id.Count > 0)
                {
                    var sql = Sql.Builder.Append(@" ");

                    var html = "";
                    var service = new BenchmarkReportIadcRepository(this.dataContext);

                    for (int i = 0; i < record.well_id.Count; i++)
                    {
                        //html += " OR r.id = " + record.well_id[i] + " ";
                        if (i == 0)
                            sql.Append(@" AND r.id = @0 ", record.well_id[i]);
                        else
                            sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                    }
                    var iadcResult = service.GetViewAll(sql);
                    result.Data = iadcResult;
                    result.Status.Success = true;
                    result.Status.Message = "Successfully";




                    //foreach (var item in record.well_id)
                    //{
                    //    //var _data = new ReportWellIadcViewModel();

                    //    //var iadcResult = service.GetViewAll().FirstOrDefault();
                    //    //if (iadcResult.total_pt + iadcResult.total_npt > 0)
                    //    //{
                    //    //    iadcResult.percentage_pt = (iadcResult.total_pt / (iadcResult.total_pt + iadcResult.total_npt)) * 100;
                    //    //    iadcResult.percentage_npt = (iadcResult.total_npt / (iadcResult.total_pt + iadcResult.total_npt)) * 100;
                    //    //}
                    //    //_data.well_iadc = iadcResult;

                    //    //var sql = Sql.Builder.Append(@"SELECT * FROM well r WHERE r.id = @0 ", item);
                    //    //var well = this.dataContext.GetInstance.First<well>(sql);
                    //    //if (well != null)
                    //    //{
                    //    //    _data.well_id = well.id;
                    //    //    _data.well_name = well.well_name;
                    //    //}

                    //    result.Data.Add(_data);
                    //}
                }
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        
    }
}