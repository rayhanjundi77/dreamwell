﻿
using CommonTools.Helper;
using Dreamwell.DataAccess;
using Dreamwell.DataAccess.Repository;
using Dreamwell.DataAccess.ViewModels;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dreamwell.BusinessLogic.UTC.Services
{
    public class WellDashboardServices
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();

        public Node<Base19Format> Bs19FormatByWellID(string wellID)
        {
            Node<Base19Format> data = new Node<Base19Format>(null);
            try
            {
                var service = new WellDataRepository();
                var afeLine = service.AfeLine();
                var afeContract = service.AfeContractByWellID(wellID);

                var afeLineParents = afeLine.Where(x => x.parent_id == null).OrderByDescending(x => x.description).ToList();
                foreach (var item in afeLineParents)
                {
                    var record = new Base19Format();
                    record.Description = item.description;
                    GetAfeLineTree(afeContract, item.id, afeLine, data.Add(record));
                }
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return data;
        }

        private void GetAfeLineTree(List<Base19Format> afeContract, string parentID, List<afe_line> data, Node<Base19Format> parent)
        {
            List<afe_line> seacrhResult = data.Where(x => x.parent_id == parentID).ToList();
            var children = parent;
            if (seacrhResult.Count > 0)
            {
                foreach (var result in seacrhResult)
                {
                    var record = new Base19Format();
                    record.Description = result.description;
                    var cost = afeContract.Where(x => x.Afe_Line_ID == result.id).FirstOrDefault();
                    if (cost != null)
                    {
                        record.Currency = cost.Currency;
                        record.total_price_actual = cost.total_price_actual;
                        record.total_price_plan = cost.total_price_plan;
                    }
                    GetAfeLineTree(afeContract, result.id, data, children.Add(record));
                }
            }
            //else
            //{
            //    afe_line afeLine = data.Where(x => x.id == parentID).First();
            //    Base19Format base19Format = afeContract.Where(x => x.AfeLineID == parentID).First();
            //    children.Add(new Base19Format()
            //    {
            //        Description = afeLine.description,
            //        Currency = base19Format.Currency,
            //        total_price_actual = base19Format.total_price_actual,
            //        total_price_plan = base19Format.total_price_plan,
            //    });
            //}
        }
    }
}
