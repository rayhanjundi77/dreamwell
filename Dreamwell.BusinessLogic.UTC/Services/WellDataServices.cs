﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Hosting;
using CommonTools;
using PetaPoco;
using Dreamwell.Infrastructure;
using Dreamwell.DataAccess;
using NLog;
using System.Dynamic;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class WellDataServices
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();

        protected DataContext dataContext { get; private set; }
        public WellDataServices(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }


        public ApiResponse GetActualDepth(string wellId)
        {
            
            var result = new ApiResponse();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder.Append(drilling.DefaultView);
                    sql.Append(" WHERE r.well_id=@0", wellId);
                    sql.Append(" ORDER BY r.drilling_date ASC ");
                    var d = db.Fetch<vw_drilling>(sql);

                    var r = new List<dynamic>();
                    var idx = 1;
                    foreach (var i in d)
                    {
                        Dictionary<string, object> p = new Dictionary<string, object>();
                        p.Add("drilling_id", i.id);
                        var operationData = DataAccess.Models.StoreFunction.udf_drilling_operation.FirstOrDefault(p);

                        dynamic record = new ExpandoObject();
                        record.x = idx;
                        record.y = i.current_depth_md;
                        record.operation = string.Format("Detail Operation:<br/> {0}",
                                operationData?.operation_period_desc?.Replace("\n", "<br/>").Replace("\r", "<br/>").Replace("'", "\'").Replace('"', '\"'));

                        r.Add(record);
                        idx++;
                    }
                    result.Status.Success = true;
                    result.Data = r;
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.StackTrace;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse GetActualCost(string wellId)
        {
            var result = new ApiResponse();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var sql = Sql.Builder.Append(drilling.DefaultView);
                    sql.Append(" WHERE r.well_id=@0", wellId);
                    sql.Append(" ORDER BY r.drilling_date ASC ");
                    var d = db.Fetch<vw_drilling>(sql);

                    var r = new List<dynamic>();
                    var idx = 1;
                    foreach (var i in d)
                    {
                        Dictionary<string, object> p = new Dictionary<string, object>();
                        p.Add("drilling_id", i.id);
                        var operationData = DataAccess.Models.StoreFunction.udf_drilling_operation.FirstOrDefault(p);

                        dynamic record = new ExpandoObject();
                        record.x = idx;
                        record.y = i.cummulative_cost;
                        record.operation = string.Format("Detail Operation:<br/> {0}",
                                operationData?.operation_period_desc?.Replace("\n", "<br/>").Replace("\r", "<br/>").Replace("'", "\'").Replace('"', '\"'));

                        r.Add(record);
                        idx++;
                    }
                    result.Status.Success = true;
                    result.Data = r;
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.StackTrace;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse GetPlanningDepth(string wellId)
        {
            var result = new ApiResponse();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var d = db.Fetch<tvd_plan>(" WHERE well_id=@0",wellId);

                    var r = new List<dynamic>();
                    var idx = 1;
                    foreach (var i in d)
                    {
                        dynamic record = new ExpandoObject();
                        record.x = i.days;
                        record.y = i.depth;
                        record.operation = string.Format("{0}",
                                i?.activity?.Replace("\n", "<br/>").Replace("\r", "<br/>").Replace("'", "\'").Replace('"', '\"'));

                        r.Add(record);
                        idx++;
                    }
                    result.Status.Success = true;
                    result.Data = r;
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.StackTrace;
                appLogger.Error(ex);
            }
            return result;
        }

        public ApiResponse GetPlanningCost(string wellId)
        {
            var result = new ApiResponse();
            try
            {
                using (var db = new dreamwellRepo())
                {
                    var d = db.Fetch<tvd_plan>(" WHERE well_id=@0", wellId);

                    var r = new List<dynamic>();
                    var idx = 1;
                    foreach (var i in d)
                    {
                        dynamic record = new ExpandoObject();
                        record.x = i.days;
                        record.y = i.cumm_cost;
                        record.operation = string.Format("{0}",
                                i?.activity?.Replace("\n", "<br/>").Replace("\r", "<br/>").Replace("'", "\'").Replace('"', '\"'));

                        r.Add(record);
                        idx++;
                    }
                    result.Status.Success = true;
                    result.Data = r;
                }
            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.StackTrace;
                appLogger.Error(ex);
            }
            return result;
        }

    }
}
