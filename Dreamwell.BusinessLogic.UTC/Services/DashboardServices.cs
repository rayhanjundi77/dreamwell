﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Hosting;
using CommonTools;
using PetaPoco;
using Dreamwell.Infrastructure;
using Dreamwell.DataAccess;
using NLog;
namespace Dreamwell.BusinessLogic.Core.Services
{
    public class DashboardServices
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();

        protected DataContext dataContext { get; private set; }
        public DashboardServices(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }


        public ApiResponse GetTotalProjectArea()
        {
            var result = new ApiResponse();
            try
            {
                int? totalProjectArea;
                int? totalSumur;
                int? totalDDRUpload;

                #region Total 
                //var sql = Sql.Builder.Append(@"SELECT COUNT(r.field_id) AS total FROM dbo.well as r
                //        INNER JOIN dbo.field f ON r.field_id=f.id
                //        WHERE r.is_active=@0
                //        GROUP BY r.field_id ", true);

                var sql = Sql.Builder.Append(@"
                            SELECT 
	                            COUNT(r.field_id) OVER (PARTITION BY field_id) AS total_project_area,
	                            COUNT(r.field_id)  OVER (ORDER BY field_id) AS total_sumur
                            FROM dbo.well as r
                            WHERE r.is_active=1");

                var data = this.dataContext.GetInstance.FirstOrDefault<dynamic>(sql);
                totalProjectArea = data?.total_project_area;
                totalSumur = data?.total_sumur;

                #endregion 


                result.Data = new
                {
                    total = new
                    {
                        projectArea = totalProjectArea ?? 0,
                        totalSumur = totalSumur ?? 0
                    }
                };
                result.Status.Success = true;

            }
            catch (Exception ex)
            {
                result.Status.Success = false;
                result.Status.Message = ex.Message;
                appLogger.Error(ex.Message);
            }
            return result;
        }



    }
}
