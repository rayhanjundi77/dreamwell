﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Hosting;
using CommonTools;
using PetaPoco;
using Dreamwell.Infrastructure;
using Dreamwell.DataAccess;
using NLog;
using System.Web;
using System.Dynamic;
using Dreamwell.DataAccess.ViewModels;
using Omu.ValueInjecter;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class LeasonLearnedServices
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();

        protected DataContext dataContext { get; private set; }
        public LeasonLearnedServices(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }

        public ApiResponse<List<vw_iadc_learned_interval>> GetAnalyze(LeasonLearnedViewModel record)
        {
            var result = new ApiResponse<List<vw_iadc_learned_interval>>();
            try
            {
                var sql = Sql.Builder.Append(@" AND r.field_id = @0", record.field_id);
                if (record.well_id != null && record.well_id.Count > 0)
                {
                    if (record.well_id.Count == 1)
                        sql.Append(@" AND r.id = @0 ", record.well_id[0]);
                    else
                    {
                        sql.Append(@" AND ( ");
                        for (int i = 0; i < record.well_id.Count; i++)
                        {
                            if (i == 0)
                                sql.Append(@" r.id = @0 ", record.well_id[i]);
                            else
                                sql.Append(@" OR r.id = @0 ", record.well_id[i]);
                        }
                        sql.Append(@" ) ");
                    }
                }
                sql.Append(@" ORDER BY r.well_name ASC ");

                var service = new IadcLearnedIntervalRepository(this.dataContext, record.iadc_id, record.uom_select);
                //result.Data = service.GetViewAll(" AND r.field_id = @0 ORDER BY r.well_name ASC ", field_id);
                result.Data = service.GetViewAll(sql);
                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public ApiResponse<List<vw_time_versus_cost>> GetIadcLearnedDetail(string wellId)
        {
            var result = new ApiResponse<List<vw_time_versus_cost>>();
            try
            {

                var service = new TimeVersusCostRepository(this.dataContext);
                //result.Data = service.GetViewAll(" AND r.well_id = @0 ORDER BY r.drilling_date ASC ", wellId);
                result.Data = service.GetViewAll($" AND r.well_id in ({wellId}) ORDER BY r.drilling_date ASC ");

                result.Status.Success = true;
                result.Status.Message = "Successfully";
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }
    }
}
