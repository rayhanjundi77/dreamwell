﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Hosting;
using CommonTools;
using PetaPoco;
using Dreamwell.Infrastructure;
using Dreamwell.DataAccess;
using NLog;
using System.Dynamic;
using Dreamwell.DataAccess.ViewModels;

namespace Dreamwell.BusinessLogic.Core.Services
{
    public class ServicePerformanceServices
    {
        protected Logger appLogger = LogManager.GetCurrentClassLogger();

        protected DataContext dataContext { get; private set; }
        public ServicePerformanceServices(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }

        public ApiResponse<List<vw_service_performance_group>> getServicePerformancetrayek(string fieldId, string[] wellList)
        {
            var result = new ApiResponse<List<vw_service_performance_group>>();
            try
            {
                var sqlWell = Sql.Builder.Append(well.DefaultView);
                sqlWell.Append(@" WHERE r.field_id=@0", fieldId);
                if (wellList.Length > 0)
                {
                    sqlWell.Append(@" and r.id in ('"+ string.Join("','", wellList) + "') ");
                }
                var datawell = well.Fetch(sqlWell);
                var listwell = datawell;
                var sql = Sql.Builder.Append(vw_service_performance.DefaultView);

                sql.Append(@"WHERE w.field_id = @0 ", fieldId);
                sql.Append(@"AND r.id IN ( ");
                sql.Append(@"	SELECT whc.id ");
                sql.Append(@"	FROM well_hole_and_casing whc ");
                sql.Append(@") ");
                if (wellList.Length > 0)
                {
                    sql.Append(@" and r.well_id in ('" + string.Join("','", wellList) + "') ");
                }

                sqlWell.Append(@" WHERE r.field_id=@0", fieldId);
                var datatrayek = this.dataContext.GetInstance.Fetch<vw_service_performance>(sql);

                //var listdatatrayek = (from a in datatrayek.DistinctBy(x => x.hole_name)
                //                      select new vw_service_performance_group()
                //                      {
                //                          hole_name = a.hole_name,
                //                          data = (from b in datatrayek.DistinctBy(x => x.well_name)
                //                                  join p in datatrayek on new
                //                                  {
                //                                      b.well_name
                //                                  } equals new { p.well_name, a.hole_name }
                //                                  into ps
                //                                  from p in ps
                //                               select new vw_service_performance()
                //                               {
                //                                   hole_depth = b.hole_depth,

                //                               }
                //                                 ).ToList()

                //                      }
                //                      )

                var listdatatrayek = new List<vw_service_performance_group>();


                foreach (var item in datatrayek.DistinctBy(x => x.hole_name).Select(s => s.hole_name).ToList())
                {
                    var record = new vw_service_performance_group();

                    record.hole_name = item;
                    record.data = new List<vw_service_performance>();
                    foreach (var well in datawell)
                    {
                        var wellhole = new vw_service_performance();
                        var data = datatrayek.FirstOrDefault(w => w.well_name == well.well_name && w.hole_name == item);

                        if (data != null)
                        {
                            wellhole.hole_name = item;
                            wellhole.well_name = data.well_name;
                            wellhole.uom_depth = data.uom_depth;
                            wellhole.plan_depth = data.plan_depth;
                            wellhole.actual_depth = data.actual_depth;
                            wellhole.uom_volume = data.uom_volume;
                            wellhole.plan_volume = data.plan_volume;
                            wellhole.actual_volume = data.actual_volume;
                            wellhole.plan_cost = data.plan_cost;
                            wellhole.actual_mud_cost = data.actual_mud_cost;
                            wellhole.actual_build = data.actual_build;
                        }
                        else
                        {
                            wellhole.hole_name = item;
                            wellhole.well_name = well.well_name;
                            wellhole.uom_depth = "ft";
                            wellhole.plan_depth = 0;
                            wellhole.actual_depth = 0;
                            wellhole.uom_volume = "bbl";
                            wellhole.plan_volume = 0;
                            wellhole.actual_volume = 0;
                            wellhole.plan_cost = 0;
                            wellhole.actual_mud_cost = 0;
                            wellhole.actual_build = 0;
                        }

                        record.data.Add(wellhole);
                    }

                    listdatatrayek.Add(record);
                }


                result.Data = listdatatrayek.OrderByDescending(o => o.dhole_name).ToList();
                result.Status.Success = true;
                appLogger.Error(sql.SQL);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                result.Status.Success = false;
                result.Status.Message = ex.Message;
            }
            return result;
        }

        public List<vw_iadc_analysis_npt> GetPTNPTDetailsByAPH(string aphId, string[] syear)
        {
            string ssyear = string.Empty;

            if (syear.Length > 0)
            {
                for (int i = 0; i < syear.Length; i++)
                {
                    if (!string.IsNullOrEmpty(syear[i]))
                    {
                        ssyear += syear[i] + ",";
                    }
                }
                if (ssyear.Length > 0)
                {
                    ssyear = ssyear.Substring(0, ssyear.Length - 1);
                }
            }
            else
            {
                ssyear = "0";
            }

            var result = new List<vw_iadc_analysis_npt>();
            try
            {
                var qry = Sql.Builder.Append(@"SELECT r.id,r.description, m.type, m.interval 
                            FROM iadc r
                            INNER JOIN iadc parent ON r.parent_id=parent.id
                            OUTER APPLY (
                                SELECT i.id, i.type, ISNULL(SUM(DATEDIFF(MINUTE, do.operation_start_date, do.operation_end_date) / 60.0),0) AS interval
                                FROM iadc i
                                INNER JOIN drilling_operation_activity do ON r.id = do.iadc_id
	                            INNER JOIN drilling_hole_and_casing dhc ON dhc.id = do.drilling_hole_and_casing_id
								INNER JOIN well w ON dhc.well_id=w.id
								INNER JOIN business_unit_field buf ON buf.field_id=w.field_id
								INNER JOIN drilling dr ON do.drilling_id=dr.id
                                WHERE i.id = r.id AND 
								buf.business_unit_id=@0 ", aphId);

                qry = qry.Append(" AND YEAR(dr.drilling_date) IN (" + ssyear + ") ");

                qry = qry.Append(@" GROUP BY i.id, i.type
                          ) AS m
							WHERE ISNULL(m.type,0) > 0 
                            ORDER BY r.description ASC ");
                result = this.dataContext.GetInstance.Fetch<vw_iadc_analysis_npt>(qry);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }

        public List<vw_iadc_analysis_npt> GetPTNPTDetailsYearByAPH(string aphId, string[] syear)
        {
            string ssyear = string.Empty;

            if (syear.Length > 0)
            {
                for (int i = 0; i < syear.Length; i++)
                {
                    if (!string.IsNullOrEmpty(syear[i]))
                    {
                        ssyear += syear[i] + ",";
                    }
                }
                if (ssyear.Length > 0)
                {
                    ssyear = ssyear.Substring(0, ssyear.Length - 1);
                }
            }
            else
            {
                ssyear = "0";
            }

            var result = new List<vw_iadc_analysis_npt>();
            try
            {
                var qry = Sql.Builder.Append(@"SELECT r.id,r.description, m.type, iyear, m.interval 
                            FROM iadc r
                            INNER JOIN iadc parent ON r.parent_id=parent.id
                            OUTER APPLY (
                                SELECT i.id, i.type, YEAR(dr.drilling_date) as iyear, ISNULL(SUM(DATEDIFF(MINUTE, do.operation_start_date, do.operation_end_date) / 60.0),0) AS interval
                                FROM iadc i
                                INNER JOIN drilling_operation_activity do ON r.id = do.iadc_id
	                            INNER JOIN drilling_hole_and_casing dhc ON dhc.id = do.drilling_hole_and_casing_id
								INNER JOIN well w ON dhc.well_id=w.id
								INNER JOIN business_unit_field buf ON buf.field_id=w.field_id
								INNER JOIN drilling dr ON do.drilling_id=dr.id
                                WHERE i.id = r.id AND 
								buf.business_unit_id=@0 ", aphId);

                qry = qry.Append(" AND YEAR(dr.drilling_date) IN (" + ssyear + ") ");

                qry = qry.Append(@" GROUP BY i.id, i.type, YEAR(dr.drilling_date)
                          ) AS m
							WHERE ISNULL(m.type,0) > 0 
                            ORDER BY iyear, r.description ");
                result = this.dataContext.GetInstance.Fetch<vw_iadc_analysis_npt>(qry);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
                throw;
            }
            return result;
        }


        public List<vw_drilling_performance> GetDrillingPerformance(string aphId, string[] years)
        {
            var result = new List<vw_drilling_performance>();
            try
            {
                var services = new DrillingPerformanceRepository(dataContext, new { years = years });
                var fieldIds = business_unit_field.Fetch("WHERE business_unit_id=@0 AND field_id IN (SELECT distinct field_id FROM well WHERE YEAR(spud_date) IN (@years)) ", 
                    aphId, new { years = years })?.Select(o => o.field_id).ToArray();
                if (fieldIds == null)
                {
                    throw new Exception("Fields / Area not found");
                }
                var sql = Sql.Builder.Append(" AND r.id IN (@fields) ", new { fields = fieldIds });
                result = services.GetViewAll(sql);
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return result;
        }
        public List<vw_drilling_performance> GetWellDrillingPerformance(string aphId, string[] years)
        {
            var result = new List<vw_drilling_performance>();
            try
            {
                var services = new DrillingPerformanceRepository(dataContext, new { years = years });
                var fieldIds = business_unit_field.Fetch("WHERE business_unit_id=@0 AND field_id IN (SELECT distinct field_id FROM well WHERE YEAR(spud_date) IN (@years)) ",
                    aphId, new { years = years })?.Select(o => o.field_id).ToArray();
                if (fieldIds == null)
                {
                    throw new Exception("Fields / Area not found");
                }
                var sql = Sql.Builder.Append(" AND r.id IN (@fields) ", new { fields = fieldIds });
                var data = services.GetViewAll(sql);
                foreach (var item in data)
                {
                    var tmp = item;
                    var sqlWell = Sql.Builder.Append(vw_well_drilling_performance.DefaultView, new { years = years });
                    sqlWell.Append(" where r.id =@0 ", item.field_id);
                    sqlWell.Append(" and w.is_active =@0 ", true);
                    if (!dataContext.IsSystemAdministrator)
                    {
                        sqlWell.Append(" and w.business_unit_id=@0 ", dataContext.PrimaryBusinessUnitId);
                    }
                    var dataWell = this.dataContext.GetInstance.Fetch<vw_well_drilling_performance>(sqlWell);
                    tmp.well_list = dataWell;
                    result.Add(tmp);
                }
                return data;
            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return result;
        }

        public dynamic GetMaterialUssagePerformance(string aphId, string[] years)
        {
            var data = new List<vw_material_ussage_by_field>();
            dynamic result = new ExpandoObject();
            try
            {
                var services = new MaterialUssageByFieldRepository(dataContext, new { years = years });
                var fieldIds = business_unit_field.Fetch("WHERE business_unit_id=@0 AND field_id IN (SELECT distinct field_id FROM well WHERE YEAR(spud_date) IN (@years)) ",
                    aphId, new { years = years })?.Select(o => o.field_id).ToArray();
                if (fieldIds == null)
                {
                    throw new Exception("Fields / Area not found");
                }
                var sql = Sql.Builder.Append(" AND r.field_id IN (@fields) ", new { fields = fieldIds });
                data = services.GetViewAll(sql);

                result.Fields = new List<string>();
                result.PivotData = new List<dynamic>();


                result.Fields = data?.DistinctBy(x => x.field_name).Select(s => s.field_name).OrderBy(o => o).ToList();
                result.PivotData = data?.GroupBy(v => new { v.material_name})
                                .Select(g => new {
                                    material_name = g.Key.material_name,
                                    Fields = from f in data?.DistinctBy(x => x.field_name).Select(s => s.field_name).OrderBy(o => o).ToList()
                                             join gv in g on f equals gv.field_name into ps
                                             from gv in ps.DefaultIfEmpty()
                                             orderby f
                                             select new {
                                                 name = f,
                                                 total_unit = (gv == null ? 0:gv.total_unit),
                                                 total_price = (gv == null ? 0 : gv.total_price)
                                             }
                                             
                                    //Fields = g.GroupBy(f => f.field_name,(mKey,mValue)=>new { 
                                    //        name = mKey,
                                    //        total_unit = mValue?.FirstOrDefault().total_unit,
                                    //        total_price = mValue?.FirstOrDefault().total_price
                                    //})
                                });

            }
            catch (Exception ex)
            {
                appLogger.Error(ex);
            }
            return result;
        }


    }
}
